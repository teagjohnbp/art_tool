# ART_Tool
Require:
- JDK8: https://www.oracle.com/java/technologies/downloads/
- Z3 SMT Solver: https://github.com/Z3Prover/z3/releases/tag/z3-4.13.0 (select your suitable OS version)

To run program:
 
Make sure that your java version 1.8 has been set.

Open CommandLine/Terminal in folder: "java -jar art_tool-1.0.0-jar-with-dependencies.jar"

Step 1: File > Set Z3 > Choose path to 'z3_path/bin' 

Step 2: Choose File > Create new environment > Choose Compiler and select source code under test folders and Build.

Step 3: Choose Test case navigation > select function under test > insert test case/ generate test cases automatically.

Data set in: /art_tool/art_prototype_tool/datatest/hieu