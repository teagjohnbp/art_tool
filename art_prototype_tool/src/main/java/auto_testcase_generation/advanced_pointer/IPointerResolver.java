package auto_testcase_generation.advanced_pointer;

public interface IPointerResolver {
    TypeMap solve();
}
