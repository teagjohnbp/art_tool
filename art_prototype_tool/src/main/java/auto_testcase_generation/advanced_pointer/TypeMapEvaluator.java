package auto_testcase_generation.advanced_pointer;

import com.dse.parser.object.FunctionPointerTypeNode;
import com.dse.parser.object.ICommonFunctionNode;
import com.dse.parser.object.INode;
import com.dse.parser.object.IVariableNode;
import com.dse.search.Search;
import com.dse.util.VariableTypeUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TypeMapEvaluator {

    private static boolean searchInProject = true;

    public static TypeMap evaluate(ICommonFunctionNode fn) {
        TypeMap typeMap;

        TypeMap internal = new InternalPtrResolver(fn).solve();
        TypeMap external = new ExternalPtrResolver(fn).solve();
        typeMap = merge(new TypeMap[]{external, internal});
        typeMap.setFunctionUsage(internal);

        List<IVariableNode> arguments = fn.getArguments();
        // add all matches function
        if (searchInProject) {
            for (IVariableNode argument : arguments) {
                if (VariableTypeUtils.isFunctionPointer(argument.getRealType())) {
                    String name = argument.getName();
                    FunctionPointerTypeNode typeNode = (FunctionPointerTypeNode) argument.resolveCoreType();

                    List<String> values = typeMap.get(name);
                    if (values == null)
                        values = new ArrayList<>();

                    if (typeNode != null) {
                        List<INode> matches = Search.searchAllMatchFunctions(typeNode);
                        for (INode f : matches) {
                            if (!values.contains(f.getName()))
                                values.add(f.getName());
                        }
                    }

                    typeMap.put(name, values);
                }
            }
        }

        for (IVariableNode argument : arguments) {
            if (VariableTypeUtils.isFunctionPointer(argument.getRealType())) {
                String name = argument.getName();
                typeMap.removeButKeepOne(name);
            }
        }

        return typeMap;
    }

    private static TypeMap merge(TypeMap[] typeMaps) {
        TypeMap merge = new TypeMap();
        merge.putAll(typeMaps[0]);
        for (int i = 1; i < typeMaps.length; i++) {
            for (Map.Entry<String, List<String>> e : typeMaps[i].entrySet()) {
                String key = e.getKey();
                List<String> value = e.getValue();

                if (merge.containsKey(key)) {
                    List<String> oldValue = merge.get(key);
                    oldValue.addAll(value);
                    value = oldValue.stream()
                            .distinct()
                            .collect(Collectors.toList());
                }

                merge.put(key, value);
            }
        }
        return merge;
    }
}
