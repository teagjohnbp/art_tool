package auto_testcase_generation.advanced_pointer;

import com.dse.parser.FunctionCallParser;
import com.dse.parser.dependency.FunctionCallDependency;
import com.dse.parser.dependency.finder.Level;
import com.dse.parser.dependency.finder.MethodFinder;
import com.dse.parser.dependency.finder.VariableSearchingSpace;
import com.dse.parser.object.*;
import com.dse.resolver.NewTypeResolver;
import com.dse.search.Search;
import com.dse.search.SearchCondition;
import com.dse.search.condition.AbstractFunctionNodeCondition;
import com.dse.search.condition.DefinitionFunctionNodeCondition;
import com.dse.util.FunctionPointerUtils;
import com.dse.util.SpecialCharacter;
import com.dse.util.VariableTypeUtils;
import org.eclipse.cdt.core.dom.ast.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ExternalPtrResolver extends PointerResolver {

    private static final int IS_VOID_POINTER = 1;
    private static final int IS_FUNCTION_POINTER = 2;

    private final int[] parameterMark;
    private final Map<IVariableNode, List<ICommonFunctionNode>> matchesMap = new HashMap<>();

    public ExternalPtrResolver(ICommonFunctionNode sut) {
        super(sut);

        List<IVariableNode> arguments = sut.getArguments();
        parameterMark = new int[arguments.size()];

        // Find all function in space
        VariableSearchingSpace searchingSpace = new VariableSearchingSpace(sut);
        List<ICommonFunctionNode> functionNodesInSpace = new ArrayList<>();
        List<Level> space = searchingSpace.getSpaces();
        for (Level level : space) {
            for (INode node : level) {
                List<SearchCondition> conditions = new ArrayList<>();
                conditions.add(new AbstractFunctionNodeCondition());
                conditions.add(new DefinitionFunctionNodeCondition());
                List<ICommonFunctionNode> functionNodes = Search.searchNodes(node, conditions);
                functionNodesInSpace.addAll(functionNodes);
            }
        }
        functionNodesInSpace = functionNodesInSpace.stream()
                .distinct()
                .collect(Collectors.toList());

        for (int i = 0; i < arguments.size(); i++) {
            IVariableNode argument = arguments.get(i);
            String type = argument.getRealType();
            if (VariableTypeUtils.isVoidPointer(type))
                parameterMark[i] = IS_VOID_POINTER;
            else if (VariableTypeUtils.isFunctionPointer(type)) {
                parameterMark[i] = IS_FUNCTION_POINTER;
                INode typeNode = argument.resolveCoreType();
                List<ICommonFunctionNode> matches;
                if (typeNode instanceof FunctionPointerTypeNode) {
                    matches = functionNodesInSpace.stream()
                            .filter(f -> FunctionPointerUtils.match((IFunctionPointerTypeNode) typeNode, f))
                            .collect(Collectors.toList());
                } else {
                    matches = new ArrayList<>();
                }
                matchesMap.put(argument, matches);
            } else
                parameterMark[i] = 0;
        }
    }

    @Override
    public TypeMap solve() {
        if (functionNode instanceof IFunctionNode) {
            ((IFunctionNode) functionNode).getDependencies().stream()
                    .filter(d -> d instanceof FunctionCallDependency && d.getEndArrow().equals(functionNode))
                    .map(d -> (FunctionCallDependency) d)
                    .forEach(this::handleExpression);
        }

        return super.solve();
    }

    private void handleExpression(FunctionCallDependency d) {
        if (!(d.getStartArrow() instanceof IFunctionNode))
            return;

        IFunctionNode callee = (IFunctionNode) d.getStartArrow();
        IASTNode ast = callee.getAST();
        FunctionCallParser visitor = new FunctionCallParser();
        ast.accept(visitor);

        for (IASTFunctionCallExpression expr : visitor.getExpressions()) {
            String name = new MethodFinder(callee).getFunctionSimpleName(expr);
            String[] nameItems = name.split(SpecialCharacter.STRUCTURE_OR_NAMESPACE_ACCESS);
            name = nameItems[nameItems.length - 1];
            if (functionNode.getSingleSimpleName().equals(name)) {
                IASTInitializerClause[] arguments = expr.getArguments();
                for (int i = 0; i < parameterMark.length; i++) {
                    IVariableNode parameter = functionNode.getArguments().get(i);
                    if (parameterMark[i] == IS_VOID_POINTER) {
                        handleVoidPointer(callee, arguments[i], parameter);
                    } else if (parameterMark[i] == IS_FUNCTION_POINTER && arguments[i] instanceof IASTExpression) {
                        handleFuncPointer(callee, arguments[i], parameter);
                    }
                }
            }
        }
    }

    private void handleFuncPointer(ICommonFunctionNode callee, IASTInitializerClause argument, IVariableNode parameter) {
        IASTFunctionDefinition ast = null;

        if (callee instanceof IFunctionNode)
            ast = ((IFunctionNode) callee).getAST();

        if (ast != null) {
            List<ICommonFunctionNode> functionNodesInSpace = matchesMap.get(parameter);
            PriorityFunctionAssignmentVisitor visitor = new PriorityFunctionAssignmentVisitor(functionNodesInSpace);
            ast.accept(visitor);
            List<ICommonFunctionNode> priority = visitor.getPriority();
            List<String> values = priority.stream()
                    .map(ICommonFunctionNode::getName)
                    .collect(Collectors.toList());
            String name = parameter.getName();
            typeMap.put(name, values);
        }
    }

    private void handleVoidPointer(ICommonFunctionNode callee, IASTInitializerClause argument, IVariableNode parameter) {
        String type = new NewTypeResolver(callee).solve(argument);
        String name = parameter.getName();
        typeMap.append(name, type);
    }

    private static class PriorityFunctionAssignmentVisitor extends ASTVisitor {

        private final List<ICommonFunctionNode> functionNodes;

        public PriorityFunctionAssignmentVisitor(List<ICommonFunctionNode> functionNodesInSpace) {
            this.functionNodes = new ArrayList<>(functionNodesInSpace);
            shouldVisitExpressions = true;
        }

        @Override
        public int visit(IASTExpression expression) {
            if (expression instanceof IASTIdExpression) {
                for (ICommonFunctionNode f : functionNodes) {
                    if (f.getSingleSimpleName().equals(expression.getRawSignature())) {
                        functionNodes.remove(f);
                        functionNodes.add(0, f);
                        break;
                    }
                }
            }

            return PROCESS_CONTINUE;
        }

        public List<ICommonFunctionNode> getPriority() {
            return functionNodes;
        }
    }
}
