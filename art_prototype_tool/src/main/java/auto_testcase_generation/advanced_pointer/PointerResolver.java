package auto_testcase_generation.advanced_pointer;

import com.dse.parser.object.ICommonFunctionNode;

public abstract class PointerResolver implements IPointerResolver {

    protected TypeMap typeMap = new TypeMap();
    protected ICommonFunctionNode functionNode;

    public PointerResolver(ICommonFunctionNode sut) {
        this.functionNode = sut;
    }

    @Override
    public TypeMap solve() {
        return typeMap;
    }
}
