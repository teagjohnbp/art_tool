package auto_testcase_generation.testdatagen;

import com.dse.logger.AkaLogger;
import com.dse.parser.object.ICommonFunctionNode;
import com.dse.testcase_manager.TestCase;

import java.util.ArrayList;
import java.util.List;

public class MixedAutomatedTestdataGeneration extends CFDSAutomatedTestdataGeneration {

    private final static AkaLogger logger = AkaLogger.get(MixedAutomatedTestdataGeneration.class);

    public MixedAutomatedTestdataGeneration(ICommonFunctionNode fn, String coverageType) {
        super(fn, coverageType);
    }

    @Override
    protected List<TestCase> generateInitialTestCases() {
        logger.debug("Concolic DFS test data generation");
        DFSAutomatedTestdataGeneration gen = new DFSAutomatedTestdataGeneration(fn, coverageType);
        gen.setMaxIteration(10);
        gen.setShowReport(false);
        gen.getAllPrototypes().addAll(getAllPrototypes());

        try {
            gen.generateTestdata(fn);
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.cfg = gen.cfg;

        return new ArrayList<>(gen.getTestCases());
    }
}
