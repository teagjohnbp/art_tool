package auto_testcase_generation.testdatagen.se.memory.pointer;

/**
 * Represent one level pointer
 *
 * @author lamnt
 */
public class VoidPointerSymbolicVariable extends PointerSymbolicVariable {
    public VoidPointerSymbolicVariable(String name, String type, int scopeLevel) {
        super(name, type, scopeLevel);
    }
}
