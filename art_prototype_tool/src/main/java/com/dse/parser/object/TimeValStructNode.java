package com.dse.parser.object;

import com.dse.util.Utils;
import org.eclipse.cdt.core.dom.ast.IASTCompositeTypeSpecifier;
import org.eclipse.cdt.core.dom.ast.IASTDeclaration;
import org.eclipse.cdt.core.dom.ast.IASTDeclarationStatement;
import org.eclipse.cdt.core.dom.ast.IASTSimpleDeclaration;

public class TimeValStructNode extends StructNode {
    public TimeValStructNode() {
        try {
            String code = "struct timeval {\n" +
                    "   long tv_sec;    /* seconds */\n" +
                    "   long tv_usec;   /* microseconds */\n" +
                    "};";
            IASTDeclarationStatement declarationStm = (IASTDeclarationStatement) Utils.convertToIAST(code);
            IASTSimpleDeclaration declaration = (IASTSimpleDeclaration) declarationStm.getDeclaration();
            setAST(declaration);

            IASTCompositeTypeSpecifier declSpec = (IASTCompositeTypeSpecifier) declaration.getDeclSpecifier();
            for (IASTDeclaration decItem : declSpec.getMembers()) {
                AttributeOfStructureVariableNode var = new AttributeOfStructureVariableNode();
                var.setAST(decItem);
                var.setPrivate(false);
                var.setParent(this);
                getChildren().add(var);
            }
        } catch (Exception ignored) {

        }
    }

    @Override
    public String getAbsolutePath() {
        setAbsolutePath("/timeval");
        return super.getAbsolutePath();
    }
}
