package com.dse.parser.dependency;

import com.dse.logger.AkaLogger;
import com.dse.parser.dependency.finder.VariableSearchingSpace;
import com.dse.parser.object.*;
import com.dse.search.Search;
import com.dse.search.condition.FunctionNodeCondition;
import com.dse.testdata.object.UnitNode;
import com.dse.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class FunctionDefinitionDependencyGeneration extends AbstractDependencyGeneration{

    final static AkaLogger logger = AkaLogger.get(FunctionDefinitionDependencyGeneration.class);
    @Override
    public void dependencyGeneration(INode root) {
        logger.debug("Start definition dependency generation for function " + root.getNewType());
        if (root instanceof DefinitionFunctionNode) {
            if (!((DefinitionFunctionNode) root).isFunctionDefinitionDependencyState()) {
                INode unit = Utils.getSourcecodeFile(root);
                List<INode> includedNodes = new ArrayList<>();
                for (Dependency d : unit.getDependencies()) {
                    INode sourceCodeFileNode = null;
                    if (d instanceof IncludeHeaderDependency && d.getEndArrow().equals(unit)) {
                        includedNodes.add(d.getStartArrow());
                        sourceCodeFileNode = d.getStartArrow();
                    }
                    if (sourceCodeFileNode != null) {
                        INode bodyFunctionNode = getBodyFunctionNode((DefinitionFunctionNode) root, (ISourcecodeFileNode) sourceCodeFileNode);
                        if (bodyFunctionNode != null) {
                            FunctionDefinitionDependency dependency = new FunctionDefinitionDependency(root, bodyFunctionNode);
                            if (!root.getDependencies().contains(dependency)) root.getDependencies().add(dependency);
                            if (!bodyFunctionNode.getDependencies().contains(dependency)) bodyFunctionNode.getDependencies().add(dependency);
                            break;
                        }
                    }
                }
                for (INode include : includedNodes) {
                    System.out.println(include.getAbsolutePath());
                }
                ((DefinitionFunctionNode) root).setFunctionDefinitionDependencyState(true);
            }
            else logger.debug(root.getAbsolutePath() + " is analyzed function call dependency before");
        }

    }

    public INode getBodyFunctionNode(DefinitionFunctionNode definition, ISourcecodeFileNode sourceCodeFileNode) {
        // CPP file node

        List<IFunctionNode> functionNodes = Search.searchNodes(sourceCodeFileNode, new FunctionNodeCondition());
        for (IFunctionNode functionNode : functionNodes) {
            String definitionRealName = (definition.getParent() instanceof StructureNode) ?
                    definition.getParent().getName() + "::" + definition.getName() :
                    definition.getName();
            if (functionNode.getName().equals(definitionRealName)) {
                return functionNode;
            }
        }
        return null;
    }
}
