package com.dse.parser.dependency;

import com.dse.parser.dependency.Dependency;
import com.dse.parser.object.INode;

public class FunctionDefinitionDependency extends Dependency {
    public FunctionDefinitionDependency(INode startArrow, INode endArrow) {
        super(startArrow, endArrow);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FunctionDefinitionDependency) {
            return startArrow.equals(((FunctionDefinitionDependency) obj).startArrow)
                    && endArrow.equals(((FunctionDefinitionDependency) obj).endArrow);
        }
        return false;
    }
}
