package com.dse.testdata.object;

import com.dse.environment.Environment;
import com.dse.parser.dependency.Dependency;
import com.dse.parser.dependency.TypeDependency;
import com.dse.parser.object.ClassNode;
import com.dse.parser.object.ConstructorNode;
import com.dse.parser.object.INode;
import com.dse.search.Search2;
import com.dse.util.SpecialCharacter;
import com.dse.util.VariableTypeUtils;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent variable as pointer (one level, two level, etc.)
 *
 * @author ducanhnguyen
 */
public class PointerStructureDataNode extends PointerDataNode {
    @Override
    public String getInputForGoogleTest(boolean isDeclared) throws Exception {
        String s = super.getInputForGoogleTest(isDeclared);
        int i = s.indexOf(SpecialCharacter.END_OF_STATEMENT);
        if (getRawType().contains("json_object") && i >= 0) {
            s = s.substring(0, i + 1);
            s = s.replaceAll("malloc.+;", "json_object_new_object();");
        }
        return s;
    }
    //    @Override
//    public String getInputForGoogleTest() throws Exception {
//        if (Environment.getInstance().isC()) {
//            String input = "";
//
//            String type = VariableTypeUtils
//                    .deleteStorageClasses(getType().replace(IDataNode.REFERENCE_OPERATOR, ""));
//
//            String coreType = "";
//            if (getChildren() != null && !getChildren().isEmpty())
//                coreType = ((ValueDataNode) getChildren().get(0)).getType();
//            else
//                coreType = type.substring(0, type.lastIndexOf('*'));
//
//            if (getCorrespondingType() instanceof StructNode) {
//                type = "struct " + type;
//                coreType = "struct " + coreType;
//            }
//
//            if (isExternel())
//                type = "";
//
//            String allocation = "";
//
//            String tempName = getVituralName() + "_temp";
//            tempName = tempName.replaceAll("[^\\w]", "_");
//
//            if (isPassingVariable() || isSTLListBaseElement() || isInConstructor() || isGlobalExpectedValue() || isSutExpectedArgument()) {
//
//                if (this.isNotNull()) {
//                    allocation = String.format("%s %s;\n", coreType, tempName);
//                    allocation += String.format("%s %s = &%s" + SpecialCharacter.END_OF_STATEMENT, type,
//                            this.getVituralName(), tempName);
//                } else {
//                    allocation = String.format("%s %s = " + IDataNode.NULL_POINTER_IN_C + SpecialCharacter.END_OF_STATEMENT,
//                            type, this.getVituralName());
//                }
//                input += allocation;
//            } else if (isArrayElement() || isAttribute()) {
//                if (this.isNotNull()) {
//                    allocation = String.format("%s %s;\n", coreType, tempName);
//                    allocation += String.format("%s = &%s" + SpecialCharacter.END_OF_STATEMENT,
//                            this.getVituralName(), tempName);
//                } else
//                    allocation = String.format("%s = " + IDataNode.NULL_POINTER_IN_C + SpecialCharacter.END_OF_STATEMENT
//                            , this.getVituralName());
//                input += allocation;
//            } else {
//                if (this.isNotNull()) {
//                    allocation = String.format("%s %s;\n", coreType, tempName);
//                    allocation += String.format("%s = &%s" + SpecialCharacter.END_OF_STATEMENT,
//                            this.getVituralName(), tempName);
//                } else
//                    allocation = String.format("%s = " + IDataNode.NULL_POINTER_IN_C + SpecialCharacter.END_OF_STATEMENT
//                            , this.getVituralName());
//                input += allocation;
//            }
//
//            input = input.replace(SpecialCharacter.STRUCTURE_OR_NAMESPACE_ACCESS, SpecialCharacter.EMPTY);
//
//            return input + SpecialCharacter.LINE_BREAK + superSuperInputGTest();
//        } else
//            return super.getInputForGoogleTest();
//    }


    //	@Override
//	public String getInputForGoogleTest() throws Exception {
//		String input = "";
//		if (this.isAttribute()) {
//			input = "// does not support this struct initialization";
//
//		} else if (this.isPassingVariable()) {
//			// create one dimensional array
//			String type = VariableTypeUtils
//					.deleteStorageClasses(this.getType().replace(IDataNode.REFERENCE_OPERATOR, ""));
//
//			String coreType = type.replace(IDataNode.ONE_LEVEL_POINTER_OPERATOR, "");
//			if (this.isNotNull())
//				input += coreType + " " + getVituralName() + "[" + getAllocatedSize() + "]" + SpecialCharacter.END_OF_STATEMENT;
//			else
//				input += type + " " + getVituralName()  + " = NULL" + SpecialCharacter.END_OF_STATEMENT;
//		} else {
//			input = "// does not support this struct initialization";
//		}
//
//		return input + SpecialCharacter.LINE_BREAK + super.getInputForGoogleTest();
//	}

    private boolean isPointerClass() {
        return getCorrespondingVar().getCorrespondingNode() instanceof ClassNode;
    }
}
