package com.dse.testdata.object;

import com.dse.environment.Environment;
import com.dse.parser.dependency.finder.Level;
import com.dse.parser.dependency.finder.VariableSearchingSpace;
import com.dse.parser.object.EmptyStructNode;
import com.dse.parser.object.INode;
import com.dse.parser.object.ISourcecodeFileNode;
import com.dse.parser.object.StructNode;
import com.dse.project_init.ProjectClone;
import com.dse.search.Search;
import com.dse.search.condition.StructNodeCondition;
import com.dse.testdata.comparable.*;
import com.dse.util.SourceConstant;
import com.dse.util.SpecialCharacter;
import com.dse.util.Utils;
import com.dse.util.VariableTypeUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represent variable as pointer (one level, two level, etc.)
 *
 * @author ducanhnguyen
 */
public abstract class PointerDataNode extends ValueDataNode implements INullableComparable {
	public static Map<String, String> specialAllocationMap = new HashMap<>(); // key: data type, value: content
	public static final int NULL_VALUE = -1;

	protected int level;

	/**
	 * The allocated size, including '\0'.
	 *
	 * Ex1: node="xyz" ---> allocatedSize = 4 <br/>
	 * Ex2: node="" ---> allocatedSize = 1
	 */
	private int allocatedSize;

	private boolean sizeIsSet = false;

	public boolean isSetSize() {
		return sizeIsSet;
	}

	public void setSizeIsSet(boolean sizeIsSet) {
		this.sizeIsSet = sizeIsSet;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getAllocatedSize() {
		return this.allocatedSize;
	}

	public void setAllocatedSize(int allocatedSize) {
		this.allocatedSize = allocatedSize;
	}

	public boolean isNotNull() {
		return this.allocatedSize >= 1;
	}

	@Override
	public String generareSourcecodetoReadInputFromFile() throws Exception {
		StringBuilder output = new StringBuilder();
		for (IDataNode child : this.getChildren())
			output.append(child.generareSourcecodetoReadInputFromFile());
		return output.toString();
	}

	@Override
	public String getInputForGoogleTest(boolean isDeclared) throws Exception {
		if (isUseUserCode()) {
			if (isPassingVariable() && !isDeclared)
				return SpecialCharacter.EMPTY;
			else
				return getUserCodeContent();
		}

		String code;

		if (Environment.getInstance().isC())
			code = getCInput(isDeclared);
		else
			code = getCppInput(isDeclared);

		String virtualName = getVituralName();
		if (virtualName.startsWith(SourceConstant.STUB_PREFIX)
				&& !virtualName.contains("RETURN")
				&& !virtualName.contains(SourceConstant.EXPECTED_PREFIX)) {
			IDataNode parent = this;
			while (!(parent.getParent() instanceof IterationSubprogramNode)) {
				parent = parent.getParent();
			}
			if (parent instanceof ValueDataNode && parent != this) {
				if (!VariableTypeUtils.isReference(((ValueDataNode) parent).getRealType())) {
					int idx = code.indexOf("=");
					if (idx >= 0) {
						code = code.substring(0, idx + 1) + virtualName.substring(SourceConstant.STUB_PREFIX.length())
								+ SpecialCharacter.END_OF_STATEMENT;
					}
				}
			}
		}

		code += super.getInputForGoogleTest(isDeclared);

		return code;
	}

	/**
	 * Reformat memory allocation for incomplete built-in data types
	 * 
	 * @param allocation the previous allocation
	 * @param type       the incomplete built-in data type
	 * @return the reformatted allocation
	 */
	protected String reformatAllocationForIncompleteTypes(String allocation, String type) {
		if (type.equals("DIR*")) {
			allocation = allocation.replaceAll("malloc\\(.*\\)", "opendir(\".\")");
		}
		if (type.equals("FILE*")) {
			allocation = allocation.replaceAll("malloc\\(.*\\)", "fopen(\"temp.txt\", \"w+\")");
		}
		return allocation;
	}

	private String getCInput(boolean isDeclared) throws Exception {
		String input = "";
		String allocation = "";

		if (!isPassingVariable() || (isPassingVariable() && isDeclared)) {
			String type = VariableTypeUtils
					.deleteStorageClassesExceptConst(getRawType().replace(IDataNode.REFERENCE_OPERATOR, ""));

			String coreType = "";
			if (getChildren() != null && !getChildren().isEmpty())
				coreType = ((ValueDataNode) getChildren().get(0)).getRawType();
			else {
				int index = type.lastIndexOf('*');
				if (index < 0) {
					String realType = getRealType();
					index = realType.lastIndexOf('*');
					if (index >= 0) {
						coreType = realType.substring(0, index).trim();
					}
				} else {
					coreType = type.substring(0, index).trim();
				}
			}

			if (this instanceof PointerStructureDataNode) {
				type = type.replace(SpecialCharacter.STRUCTURE_OR_NAMESPACE_ACCESS, SpecialCharacter.EMPTY);
				coreType = coreType.replace(SpecialCharacter.STRUCTURE_OR_NAMESPACE_ACCESS, SpecialCharacter.EMPTY);
			}

			if (isExternel())
				type = "";

			if (isPassingVariable() || isSTLListBaseElement() || isInConstructor() || isGlobalExpectedValue()
					|| isSutExpectedArgument()) {
				if (this.isNotNull())
					allocation = getSpecialAllocation(type, this, true);
				else {
					allocation = String.format(
							"%s %s = " + IDataNode.NULL_POINTER_IN_C + SpecialCharacter.END_OF_STATEMENT,
							type, this.getVituralName());
				}
			} else if (isArrayElement() || isAttribute()) {
				if (this.isNotNull()) {
					allocation = getSpecialAllocation(type, this, false);
				}
				else
					allocation = String.format(
							"%s = " + IDataNode.NULL_POINTER_IN_C + SpecialCharacter.END_OF_STATEMENT,
							this.getVituralName());
			} else if (isVoidPointerValue()) {
				if (this.isNotNull())
					allocation = getSpecialAllocation(type, this, true);
				else {
					allocation = String.format(
							"%s %s = " + IDataNode.NULL_POINTER_IN_C + SpecialCharacter.END_OF_STATEMENT,
							type, this.getVituralName());
				}
			} else {
				if (this.isNotNull())
					allocation = getSpecialAllocation(type, this, false);
				else
					allocation = String.format(
							"%s = " + IDataNode.NULL_POINTER_IN_C + SpecialCharacter.END_OF_STATEMENT,
							this.getVituralName());
			}

			input = reformatAllocationForIncompleteTypes(allocation, type);
		}

		return input + SpecialCharacter.LINE_BREAK + super.getInputForGoogleTest(isDeclared);
	}

	private String getCppInput(boolean isDeclared) throws Exception {
		String input = "";
		String allocation = "";
		if (!isPassingVariable() || (isPassingVariable() && isDeclared)) {
			String type = VariableTypeUtils
					.deleteStorageClassesExceptConst(getRawType().replace(IDataNode.REFERENCE_OPERATOR, ""));

			String coreType = "";
			if (getChildren() != null && !getChildren().isEmpty())
				coreType = ((ValueDataNode) getChildren().get(0)).getRawType();
			else
				coreType = type.substring(0, type.lastIndexOf('*'));

			if (isExternel())
				type = "";

			String name = getVituralName();
			int size = getAllocatedSize();

			if (isPassingVariable() || isSTLListBaseElement() || isInConstructor() || isGlobalExpectedValue()
					|| isSutExpectedArgument()) {
				if (this.isNotNull()) {
					allocation = String.format("%s %s = (%s) malloc(%d * sizeof(%s));", type, name, type, size,
							coreType);
				} else {
					allocation = String.format("%s %s = "
							+ IDataNode.NULL_POINTER_IN_CPP
							+ SpecialCharacter.END_OF_STATEMENT, type, name);
				}
			} else if (isArrayElement() || isAttribute()) {
				if (this.isNotNull())
					allocation = String.format("%s = (%s) malloc(%d * sizeof(%s));", name, type, size, coreType);
				else
					allocation = String.format("%s = "
							+ IDataNode.NULL_POINTER_IN_CPP
							+ SpecialCharacter.END_OF_STATEMENT, name);
			} else {
				if (this.isNotNull())
					allocation = String.format("%s = (%s) malloc(%d * sizeof(%s));", name, type, size, coreType);
				else
					allocation = name + " = " + IDataNode.NULL_POINTER_IN_CPP + SpecialCharacter.END_OF_STATEMENT;
			}

			input = reformatAllocationForIncompleteTypes(allocation, type);
		}

		return input + SpecialCharacter.LINE_BREAK + super.getInputForGoogleTest(isDeclared);
	}

	@Override
	public String assertNull(String name) {
		return new NullableStatementGenerator(this).assertNull(name);
	}

	@Override
	public String assertNotNull(String name) {
		return new NullableStatementGenerator(this).assertNotNull(name);
	}

	@Override
	public String getAssertion() {
		if (isVoidPointerValue()) {
			return SpecialCharacter.EMPTY;
		}

		String actualName = getActualName();

		String output = SpecialCharacter.EMPTY;

		String assertMethod = getAssertMethod();
		if (assertMethod != null) {
			switch (assertMethod) {
				case AssertMethod.ASSERT_NULL:
					output = assertNull(actualName);
					break;

				case AssertMethod.ASSERT_NOT_NULL:
					output = assertNotNull(actualName);
					break;

				case AssertMethod.USER_CODE:
					output = getAssertUserCode().normalize();
					break;
			}
		}

		return output + super.getAssertion();
	}

	public static String getSpecialAllocation (String type, PointerDataNode dataNode, boolean isExternal) {
//		if (!specialAllocationMap.containsKey(type)) return "";
//		String content = specialAllocationMap.get(type);
		String allocation = "";
		String coreType = "";
		if (dataNode.getChildren() != null && !dataNode.getChildren().isEmpty())
			coreType = ((ValueDataNode) dataNode.getChildren().get(0)).getRawType();
		else
			coreType = type.substring(0, type.lastIndexOf('*'));
		switch (dataNode.getRawType()) {
			case "GLFWwindow*": {
				allocation = String.format("%s = glfwCreateWindow(640, 480, \"My Title\", NULL, NULL);",
						dataNode.getVituralName());
				if (isExternal)
					allocation = dataNode.getRawType() + " " + allocation;
				break;
			}
			case "sqlite3*": {
				allocation = String.format("sqlite3_open_v2(\"test.db\", &%s, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);",
						dataNode.getVituralName());
				if (!isExternal) {
					allocation = dataNode.getRawType() + " " + dataNode.getVituralName() + ";\n" + allocation;
				}
				break;
			}

			case "sqlite3_stmt*": {
				allocation = String.format("sqlite3_prepare_v2(db, \"SELECT 2012\", -1, &%s, NULL);",
						dataNode.getVituralName());
				if (!isExternal) {
					allocation = dataNode.getRawType() + " " + dataNode.getVituralName() + ";\n" + allocation;
				}
				break;
			}
			default:
				allocation = String.format("%s = malloc(%d * sizeof(%s))" + SpecialCharacter.END_OF_STATEMENT,
						dataNode.getVituralName(), dataNode.getAllocatedSize(), coreType);
				if (isExternal)
					allocation = dataNode.getRawType() + " " + allocation;
		}
		return allocation;
	}

	@Override
	public PointerDataNode clone() {
		PointerDataNode clone = (PointerDataNode) super.clone();
		clone.level = level;
		return clone;
	}

}
