package com.dse.coverage;


/**
 *  This class is used to contains data needed to display when view coverages
 */

public class CombinedCoverageDataObject extends CoverageDataObject {
    private float funcProgress;
    private int funcTotal;
    private int funcVisited;

    public float getFuncProgress() {
        return funcProgress;
    }

    public void setFuncProgress(float funcProgress) {
        this.funcProgress = funcProgress;
    }

    public int getFuncTotal() {
        return funcTotal;
    }

    public void setFuncTotal(int funcTotal) {
        this.funcTotal = funcTotal;
    }

    public int getFuncVisited() {
        return funcVisited;
    }

    public void setFuncVisited(int funcVisited) {
        this.funcVisited = funcVisited;
    }
}
