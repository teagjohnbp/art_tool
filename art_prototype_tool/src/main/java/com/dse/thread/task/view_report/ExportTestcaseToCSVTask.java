package com.dse.thread.task.view_report;

import com.dse.guifx_v3.controllers.object.LoadingPopupController;
import com.dse.guifx_v3.helps.UIController;
import com.dse.report.regression_report.RegressionReport;
import com.dse.testcasescript.object.ITestcaseNode;
import com.dse.testcasescript.object.TestcaseRootNode;
import com.dse.thread.AbstractAkaTask;

public class ExportTestcaseToCSVTask extends AbstractAkaTask<Boolean> {
    private ITestcaseNode rootNode = null;
    public static LoadingPopupController popup = LoadingPopupController.newInstance("Exporting...");

    public ExportTestcaseToCSVTask(ITestcaseNode rootNode) {
        this.rootNode = rootNode;
    }

    @Override
    protected Boolean call() throws Exception {
        if (rootNode == null) return false;
        RegressionReport report = new RegressionReport();
        report.initialTestcaseMap(rootNode);
        report.exportToCSV();
        return true;
    }

    @Override
    protected void succeeded() {
        super.succeeded();
        popup.close();
        UIController.showSuccessDialog("Successfully!", "Test cases Exporter CSV", "");
    }
}
