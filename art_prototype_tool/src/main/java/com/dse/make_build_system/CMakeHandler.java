package com.dse.make_build_system;

import com.dse.cmake.CMakeBuilder;
import com.dse.compiler.Terminal;
import com.dse.config.WorkspaceConfig;
import com.dse.environment.Environment;
import com.dse.environment.object.EnviroMakeBuildSystemNode;
import com.dse.guifx_v3.about_us.Test;
import com.dse.guifx_v3.helps.UIController;
import com.dse.parser.object.CFileNode;
import com.dse.parser.object.CppFileNode;
import com.dse.parser.object.HeaderNode;
import com.dse.parser.object.INode;
import com.dse.testcase_manager.TestCase;
import com.dse.util.PathUtils;
import com.dse.util.SpecialCharacter;
import com.dse.util.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CMakeHandler extends BuildSystemHandler {
    public static String CMAKE_CHECK_CMD = "cmake --version";
    public static String CMAKE_BUILD_FOLDER_NAME = "aka_cmake_build";
    public static String AKAIGNORE_CMAKE_LIST = "CMakeLists.akaignore.txt";
    public static String DEFAULT_CMAKE_LIST = "CMakeLists.txt";
    public static String CMAKE_CACHE_FILE_NAME = "CMakeCache.txt";
    public static String CMAKE_FILES_FOLDER_NAME = "CMakeFiles";
    private String cmakeGenerator = "";

    class GlobVariable {
        public int index;
        public String line;
        public String name;
    }

    public CMakeHandler() {
        buildSystem = "CMake";
        buildFile = "CMakeLists.txt";
    }

    public void importConfigFromNode(EnviroMakeBuildSystemNode node) {
        super.importConfigFromNode(node);
        buildFile = "CMakeLists.txt";
        cmakeGenerator = node.getCMakeGenerator();
    }

    public void reset() {
        super.reset();
        cmakeGenerator = "";
    }

    @Override
    public boolean verifyBuildSystemExist() {
//        try {
//            Terminal terminal = new Terminal(CMAKE_CHECK_CMD);
//
//            if (terminal.getStdout().contains("cmake version")) {
//                return true;
//            } else {
//                logger.error("Cannot find cmake in your PATH environment variable!");
//                UIController.showErrorDialog("Cannot find cmake in your PATH", "Error", "CMake Handler");
//                return false;
//            }
//
//        } catch (InterruptedException | IOException e) {
//            logger.error(e.getMessage());
//            UIController.showErrorDialog(e.getMessage(), "Error", "CMake Handler");
//            return false;
//        }
        // temporal fix
        return true;
    }

    public void cloneCurrentProjectToInstrumentDirectory() {
        try {
            String instrumentDirectory = new WorkspaceConfig().fromJson().getInstrumentDirectory();
            String makeSourceDirectory = new WorkspaceConfig().fromJson().getMakeSourceFolder();
            Utils.copy(new File(projectDirectory), new File(instrumentDirectory));
            Utils.copy(new File(projectDirectory), new File(makeSourceDirectory));
            logger.debug("Project cloned to instrument directory");

            logger.info("Starting cloning CMakeLists files");
            cloneAllCMakeListsFiles(makeSourceDirectory);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void cloneAllCMakeListsFiles(String directoryPath) throws IOException {
        File directory = new File(directoryPath);
        for (File file : Objects.requireNonNull(directory.listFiles())) {
            if (file.isDirectory()) {
                cloneAllCMakeListsFiles(file.getAbsolutePath());
            } else {
                if (file.getName().equals(DEFAULT_CMAKE_LIST)) {
                    String cmakelistsCopyPath = directoryPath + File.separator + AKAIGNORE_CMAKE_LIST;

                    Utils.copy(file, new File(cmakelistsCopyPath));
                    logger.debug("CMakeLists.txt copied to " + cmakelistsCopyPath);
                }
            }
        }
    }

    public void doNecessaryModify() {
        String makeSourceDirectory = new WorkspaceConfig().fromJson().getMakeSourceFolder();
        //modifyAllCMakeListsFileInDirectory(new File(instrumentDirectory));
        setExecutableFilePathFromCMakeLists(new File(makeSourceDirectory + File.separator + CMakeBuilder.AKAIGNORE_CMAKE_LIST));
    }

    public void prepareForBuild() {}

    /**
     * Replace these file with akaignore.ext (ex:
     *
     * .h => .akaignore.h,
     * .hpp => .akaignore.hpp,
     * .c => .akaignore.c,
     * .cpp => .akaignore.cpp,
     * )
     */
    private void modifyCMakeListsFile(String cmakeListsPath) {
        File cmakelistsFile = new File(cmakeListsPath);
        String cmakelistsContent = Utils.readFileContent(cmakelistsFile);
        StringBuilder finalContent = new StringBuilder();

        for (int i = 0; i < cmakelistsContent.length(); i++) {
            if (cmakelistsContent.charAt(i) == '.') {
                if (cmakelistsContent.substring(i, i + 2).equals(".h")) {
                    if (!cmakelistsContent.substring(i, i + 4).equals(".hpp")) {
                        finalContent.append(".akaignore.h");
                        i += 1;
                    } else {
                        finalContent.append(".akaignore.hpp");
                        i += 3;
                    }
                } else if (cmakelistsContent.substring(i, i + 2).equals(".c")) {
                    if (cmakelistsContent.substring(i, i + 6).equals(".cmake")) {
                        finalContent.append(".cmake");
                        i += 5;
                        continue;
                    }

                    if (!cmakelistsContent.substring(i, i + 4).equals(".cpp")) {
                        finalContent.append(".akaignore.c");
                        i += 1;
                    } else {
                        finalContent.append(".akaignore.cpp");
                        i += 3;
                    }
                } else {
                    finalContent.append(cmakelistsContent.charAt(i));
                }
            } else
                finalContent.append(cmakelistsContent.charAt(i));
        }

        Utils.writeContentToFile(finalContent.toString(), cmakeListsPath);
    }

    public void modifyAllCMakeListsFileInDirectory(File currentDirectory) {
        File[] files = currentDirectory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    modifyAllCMakeListsFileInDirectory(file);
                } else {
                    if (file.getName().endsWith(".h.in")) {
                        file.renameTo(new File(file.getAbsolutePath().replace(".h.in", ".akaignore.h.in")));
                    } else if (file.getName().equals(AKAIGNORE_CMAKE_LIST)) {
                        modifyCMakeListsFile(file.getAbsolutePath());
                    }
                }
            }
        }
    }

    public void setExecutableFilePathFromCMakeLists(File cmakeListsFile) {
        String cmakeListsContent = Utils.readFileContent(cmakeListsFile);
        String projectName = "";

        // Get project name from CMakeLists.txt
        for (int i = cmakeListsContent.indexOf("project(") + 9; cmakeListsContent.charAt(i) != ')'; i++) {
            projectName += cmakeListsContent.charAt(i);
        }

        String executableFileName = "";
        // Get executable file name from CMakeLists.txt
        if (cmakeListsContent.contains("add_executable")) {
            int index = cmakeListsContent.indexOf("add_executable(") + 15;
            while (cmakeListsContent.charAt(index) == ' ' || cmakeListsContent.charAt(index) == '\t'
                    || cmakeListsContent.charAt(index) == '\n') {
                index++;
            }
            for (; cmakeListsContent.charAt(index) != ' ' && cmakeListsContent.charAt(index) != '\t'
                    && cmakeListsContent.charAt(index) != '\n'; index++) {
                executableFileName += cmakeListsContent.charAt(index);
            }

            if (executableFileName.equals("${PROJECT_NAME}")) {
                executableFileName = projectName;
            }

            executableFilePath = new WorkspaceConfig().fromJson().getMakeSourceFolder()
                    + File.separator + CMAKE_BUILD_FOLDER_NAME + File.separator + executableFileName;

            if (Utils.isWindows()) {
                executableFilePath += ".exe";
            }

            logger.info("Executable file path: " + executableFilePath);
        }
    }

    public String buildProject(boolean cleanBuild) {
        return buildProject(cleanBuild, this.projectDirectory);
    }

    public String buildProject(boolean cleanBuild, String projectDirectory) {
        try {
            if (cleanBuild) {
                Utils.deleteFileOrFolder(
                        new File(projectDirectory + File.separator + CMAKE_BUILD_FOLDER_NAME + File.separator + CMAKE_CACHE_FILE_NAME));

                Utils.deleteFileOrFolder(
                        new File(projectDirectory + File.separator + CMAKE_BUILD_FOLDER_NAME + File.separator + CMAKE_FILES_FOLDER_NAME));

                Utils.deleteFileOrFolder(
                        new File(projectDirectory + File.separator + CMAKE_BUILD_FOLDER_NAME));
            }

            String buildDirectory = new WorkspaceConfig().fromJson().getMakeSourceFolder() + File.separator + CMAKE_BUILD_FOLDER_NAME;
            String[] buildScripts = {"cmake", "-G", cmakeGenerator, "-S", projectDirectory, "-B", buildDirectory};

            logger.debug("Build directory: " + buildDirectory);
            logger.debug("Build script: " + Arrays.stream(buildScripts).reduce("", (a, b) -> a + " " + b));

            Terminal terminal = new Terminal(buildScripts);

            if (terminal.getStderr().contains("CMake Error")) {
                logger.error(terminal.getStderr());
//                UIController.showErrorDialog("Project builded failed!", "Error", "CMake Builder");
                return terminal.get();
            } else {
                logger.debug("Project builded successfully");
                return terminal.get();
            }
        } catch (InterruptedException | IOException e) {
            logger.error(e.getMessage());
            UIController.showErrorDialog(e.getMessage(), "Error", "CMake Builder");
            return e.getMessage();
        }
    }

    public String generateExecutableFile(String containerDirectory) {
        try {
            String buildDirectory = containerDirectory + File.separator + CMAKE_BUILD_FOLDER_NAME;
            String[] compileScripts = {"cmake", "--build", buildDirectory};

            logger.debug(
                    "Generate executable file command: "
                            + Arrays.stream(compileScripts).reduce("", (a, b) -> a + " " + b));

            Terminal terminal = new Terminal(compileScripts);

            if (terminal.getStderr().contains("CMake Error")) {
                logger.error(terminal.getStderr());
                UIController.showErrorDialog("Compile project failed!", "Error", "CMake Builder");
                return terminal.get();
            } else {
                logger.debug("Project compiled successfully");
                return terminal.get();
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            UIController.showErrorDialog("Cannot compile project!", "Error", "CMake Builder");
            return e.getMessage();
        }
    }

    public String linkTestDriverToMakeBuildFile(TestCase testcase) throws IOException {
        String makeSourceFolder = new WorkspaceConfig().fromJson().getMakeSourceFolder();
        String cmakelistCopyPath = makeSourceFolder + File.separator + AKAIGNORE_CMAKE_LIST;
        String cmakelistClonePath = makeSourceFolder + File.separator + DEFAULT_CMAKE_LIST;

        String content = Utils.readFileContent(cmakelistCopyPath);
        String defineFlagContent = String.format("add_compile_definitions(AKA_TC_%s)\n", testcase.getName().toUpperCase()
                .replace(SpecialCharacter.DOT, SpecialCharacter.UNDERSCORE_CHAR));
        content += defineFlagContent;
        Utils.writeContentToFile(content, cmakelistClonePath);

//        Utils.copy(new File(cmakelistCopyPath), new File(cmakelistClonePath));
        return linkTestDriverToEachCMakeListsFile(makeSourceFolder, testcase);
    }

    private List<GlobVariable> getGlobVariableFromCMakeLists(String content) {
        // regex pattern to check if content contains FILE(GLOB_RECURSE variableName ) or file(GLOB_RECURSE variableName ) or File(GLOB_RECURSE variableName ) or file(GLOB variableName ) or File(GLOB variableName )
        String regex = "(?i)file(?-i)\\s*\\((\\s*|\\t*|\\n*)GLOB(_RECURSE)?(\\s*|\\t*|\\n*)([a-zA-Z0-9_]+)(\\s*|\\t*|\\n*).*\\)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);
        // iterate through all matches and get the variable name and return the start pos of that match

        List<GlobVariable> lists = new ArrayList<>();

        while (matcher.find()) {
            GlobVariable globVariable = new GlobVariable();
            globVariable.name = matcher.group(4);
            globVariable.index = matcher.start();
            globVariable.line = matcher.group();
            lists.add(globVariable);
        }

        return lists;
    }

    private String linkTestDriver(TestCase testcase, String cmakelistsPath) {
        File cmakeListFile = new File(cmakelistsPath);
        if (!cmakeListFile.exists()) {
            logger.error("CMakeLists.txt does not exist in " + cmakelistsPath);
            return "Error: CMakeLists.txt does not exist in " + cmakelistsPath;
        }

        String cmakeListsContent = Utils.readFileContent(cmakeListFile);

        List<GlobVariable> lists = getGlobVariableFromCMakeLists(cmakeListsContent);

        String modifiedCMakeListsContent = cmakeListsContent;
        String fileName = "";
        INode parent = testcase.getFunctionNode().getParent();
        while (!(parent instanceof CFileNode || parent instanceof CppFileNode || parent instanceof HeaderNode)) {
            parent = parent.getParent();
        }

        if (parent instanceof CFileNode) {
            fileName = ((CFileNode) parent).getName();
        } else if (parent instanceof CppFileNode) {
            fileName = ((CppFileNode) parent).getName();
        } else if (parent instanceof HeaderNode) {
            fileName = ((HeaderNode) parent).getName();
        }

        //fileName = Utils.insertExtIntoFileName(fileName, ".akaignore");
        modifiedCMakeListsContent = modifiedCMakeListsContent.replaceAll(fileName, "");

        if (!lists.isEmpty()) {

            for (GlobVariable variable: lists) {
                String filter = "LIST(FILTER " + variable.name + " EXCLUDE REGEX \".*" + fileName.replace(".", "\\\\.") + "$\")";
                int index = modifiedCMakeListsContent.indexOf(variable.line) + variable.line.length();
                modifiedCMakeListsContent = modifiedCMakeListsContent.substring(0, index) + "\n" + filter +
                        modifiedCMakeListsContent.substring(index) + "\n";
            }
        }

        if (modifiedCMakeListsContent.contains("add_executable")) {
            int index = modifiedCMakeListsContent.indexOf("add_executable(") + 15;
            while (modifiedCMakeListsContent.charAt(index) == ' ' || modifiedCMakeListsContent.charAt(index) == '\t'
                    || modifiedCMakeListsContent.charAt(index) == '\n') {
                index++;
            }

            // keep increase index through project name or ${PROJECT_NAME}
            while (modifiedCMakeListsContent.charAt(index) != ' ' && modifiedCMakeListsContent.charAt(index) != '\t'
                    && modifiedCMakeListsContent.charAt(index) != '\n') {
                index++;
            }

            modifiedCMakeListsContent = modifiedCMakeListsContent.substring(0, index)
                    + " \"" + PathUtils.replaceBackslashWithSlash(testcase.getSourceCodeFile()) + "\""
                    + modifiedCMakeListsContent.substring(index);

            logger.debug("Modified CMakeLists.txt: " + modifiedCMakeListsContent);

            Utils.writeContentToFile(modifiedCMakeListsContent, cmakelistsPath);
            logger.debug("Test driver linked to CMakeLists.txt in " + cmakelistsPath);
            return "Test driver linked to CMakeLists.txt in " + cmakelistsPath;
        }

        return "";
    }

    private String linkTestDriverToEachCMakeListsFile(String directoryPath, TestCase testcase) throws IOException {
        StringBuilder result = new StringBuilder();
        File directory = new File(directoryPath);
        for (File file : Objects.requireNonNull(directory.listFiles())) {
            if (file.isDirectory()) {
                //result.append(linkTestDriverToEachCMakeListsFile(file.getAbsolutePath(), testcase));
                //result.append("\n");
            } else {
                if (file.getName().equals(DEFAULT_CMAKE_LIST)) {
                    String content = Utils.readFileContent(directoryPath + File.separator + AKAIGNORE_CMAKE_LIST);
                    String defineFlagContent = String.format("add_compile_definitions(AKA_TC_%s)\n", testcase.getName().toUpperCase()
                            .replace(SpecialCharacter.DOT, SpecialCharacter.UNDERSCORE_CHAR));
                    content += defineFlagContent;
                    Utils.writeContentToFile(content, file.getAbsolutePath());
//                    Utils.copy(new File(directoryPath + File.separator + AKAIGNORE_CMAKE_LIST), file);
                    result.append(linkTestDriver(testcase, file.getAbsolutePath()));
                    result.append("\n");
                } else if (file.getName().contains("lcov.akaignore")) {
                    Utils.deleteFileOrFolder(file);
                }
            }
        }

        return result.toString();
    }

    public String getCmakeGenerator() {
        return cmakeGenerator;
    }

    public void setCmakeGenerator(String cmakeGenerator) {
        this.cmakeGenerator = cmakeGenerator;
    }

    @Override
    public String getExecutableFilePath() {
        if (executableFilePath.equals("")) {
            String cmakeListsPath =
                    new WorkspaceConfig().fromJson().getInstrumentDirectory() + File.separator + DEFAULT_CMAKE_LIST;
            File cmakeListsFile = new File(cmakeListsPath);

            if (cmakeListsFile.exists()) {
                setExecutableFilePathFromCMakeLists(cmakeListsFile);
            } else {
                logger.error("Cannot get executable file path from /instrument/CMakeLists.txt");
            }
        }

        return executableFilePath;
    }
}
