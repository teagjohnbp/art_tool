package com.dse.report;

import com.dse.config.WorkspaceConfig;
import com.dse.coverage.CoverageDataObject;
import com.dse.coverage.CoverageManager;
import com.dse.report.element.*;
import com.dse.testcase_manager.*;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CoverageReport extends ReportView {

    private final List<TestCase> testCases;

    private final String fileName;

    private final String[] coverageTypes;

    public CoverageReport(String fileName, List<TestCase> testCases, String[] coverageTypes, LocalDateTime creationDateTime) {
        // set report name
        super(String.format("Coverage Report %s", fileName));

        // set report attributes
        this.testCases = testCases;
        this.creationDateTime = creationDateTime;
        this.fileName = fileName;
        this.coverageTypes = coverageTypes;

        // set report location path to default
        setPathDefault();

        // generate test case report
        generate();
    }

    @Override
    protected void generate() {
        // STEP 1: generate table of contents section
        sections.add(generateTableOfContents());

        // STEP 2: generate configuration data section
        sections.add(generateConfigurationData());

        // STEP 3: coverage view
        for (String coverageType : coverageTypes) {
            sections.add(generateCoverageView(coverageType));
        }
    }

    private IElement generateCoverageView(String coverageType) {
        Section section = new Section(coverageType.toLowerCase());

        section.getBody().add(new Section.CenteredLine(coverageType, COLOR.MEDIUM));

        CoverageDataObject dataObject = CoverageManager.getCoverageOfMultiTestCaseAtSourcecodeFileLevel(testCases, coverageType);

        if (dataObject != null) {
            Table overall = new Table();
            overall.getRows().add(new Table.Row("Coverage:", String.format("%.2f%%", dataObject.getProgress() * 100)));

            section.getBody().add(overall);

            section.getBody().add(new CodeView(dataObject.getContent()));
        }

        section.getBody().add(new Section.BlankLine());

        return section;
    }

    @Override
    protected TableOfContents generateTableOfContents() {
        TableOfContents tableOfContents = new TableOfContents();

        tableOfContents.getBody().add(new TableOfContents.Item("Configuration Data", "config-data"));

        for (String coverageType : coverageTypes) {
            tableOfContents.getBody().add(
                    new TableOfContents.Item(coverageType, coverageType.toLowerCase()));
        }

        return tableOfContents;
    }

    protected ConfigurationData generateConfigurationData() {
        ConfigurationData configData = new ConfigurationData();

        List<ITestCase> elements = new ArrayList<>(testCases);

        TestCaseTable table = new TestCaseTable(elements);

        configData.setCreationDate(getCreationDate());
        configData.setCreationTime(getCreationTime());
        configData.setTable(table);

        configData.generate();

        return configData;
    }

    @Override
    protected void setPathDefault() {
        this.path = new WorkspaceConfig().fromJson().getReportDirectory()
                + File.separator + "coverage" + File.separator + fileName + ".html";
    }
}
