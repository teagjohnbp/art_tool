package com.dse.report.expr;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

public class Recorder {

    private static Recorder instance;

    public static Recorder getInstance() {
        if (instance == null)
            instance = new Recorder();
        return instance;
    }


    private Line current;

    private String file;

    private final MemoryClock memClk = new MemoryClock();


    private Recorder() {

    }

    public void initialize() {
        this.file = String.format("%s/aut-%s.csv", new File("").getAbsolutePath(), LocalDateTime.now());
        try {
            new File(file).getParentFile().mkdirs();
            new File(file).createNewFile();
            FileWriter fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("File,Function,Iteration,Time,Memory,Coverage");
            bw.newLine();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void register(String file, String function) {
        this.current = new Line();
        this.current.function = function;
        this.current.file = file;
        this.memClk.reset();
    }

    public void appendTime(long time) {
        this.current.time += time;
    }

    public void memClkStart() {
        memClk.start();
    }

    public void memClkEnd() {
        memClk.end();
    }

    public void increaseIter() {
        current.iteration++;
    }

    public void setCoverage(float coverage) {
        current.coverage = coverage;
    }

    public void export() {
        current.memory = memClk.estimate();

        try {
            FileWriter fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(current.toString());
            bw.newLine();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class MemoryClock extends Timer {

        private long prevUsed;
        private long totalUsed;
        private int count;

        private final AtomicBoolean pause = new AtomicBoolean(true);

        private MemoryClock() {
            super("Memory Clock");

            TimerTask task = new TimerTask() {
                public void run() {
                    if (!pause.get()) {
                        long used = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
                        if (used > prevUsed) {
                            totalUsed += used - prevUsed;
                            count++;
                        }
                        prevUsed = used;
                    }
                }
            };

            schedule(task, 100L, 1L);
        }

        public void reset() {
            prevUsed = -1;
            totalUsed = 0;
        }

        public void start() {
            pause.set(false);
        }

        public void end() {
            pause.set(true);
        }

        public long estimate() {
            return totalUsed / count;
        }
    }

    private static class Line {
        private String function, file;
        private int iteration;
        private long time;
        private long memory;
        private float coverage;

        @Override
        public String toString() {
            return String.format("%s,%s,%d,%d,%d,%f", file, function, iteration, time, memory, coverage);
        }
    }
}
