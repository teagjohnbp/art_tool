package com.dse.report.regression_report;

import com.dse.config.WorkspaceConfig;
import com.dse.coverage.CoverageManager;
import com.dse.environment.Environment;
import com.dse.exception.FunctionNodeNotFoundException;
import com.dse.guifx_v3.helps.UIController;
import com.dse.logger.AkaLogger;
import com.dse.parser.object.ICommonFunctionNode;
import com.dse.parser.object.IFunctionNode;
import com.dse.testcase_manager.TestCase;
import com.dse.testcase_manager.TestCaseManager;
import com.dse.testcasescript.object.ITestcaseNode;
import com.dse.testcasescript.object.TestNewNode;
import com.dse.testcasescript.object.TestNormalSubprogramNode;
import com.dse.util.SpecialCharacter;
import com.dse.util.Utils;

import java.io.File;
import java.util.*;

public class RegressionReport {
    AkaLogger logger = AkaLogger.get(RegressionReport.class);
    private Map<IFunctionNode, List<TestCase>> testCaseMap = new HashMap<>();

    public void exportToCSV() {
        logger.debug("Generating report ...");
        String fileName = "reportAllTestcaseInProject";
        String extension = ".csv";
        String exportPath = new WorkspaceConfig().fromJson().getRegressionScriptDirectory() + File.separator + fileName + extension;
        logger.debug("CSV PATH: " + exportPath);
        StringBuilder content = new StringBuilder("Function path, Test case name, Functional Coverage, Sourcecode File Coverage\n");
        for (IFunctionNode functionNode : testCaseMap.keySet()) {
            List<TestCase> testCases = testCaseMap.get(functionNode);
            String functionPath = functionNode.getAbsolutePath();

            for (TestCase tc : testCases) {
                String tcName = tc.getName();
                float tcFunctionalCoverage = tc.getFunctionalCoverage();
                if (tcFunctionalCoverage < 0) {
                    if (tc.getStatus().equals(TestCase.STATUS_SUCCESS) || tc.getStatus().equals(TestCase.STATUS_RUNTIME_ERR))
                        tcFunctionalCoverage = CoverageManager.getFunctionProgress(tc, Environment.getInstance().getTypeofCoverage());
                    else tcFunctionalCoverage = 0;
                }
                float tcSourceCoverage = tc.getSourceCoverage();
                if (tcSourceCoverage < 0) {
                    if (tc.getStatus().equals(TestCase.STATUS_SUCCESS) || tc.getStatus().equals(TestCase.STATUS_RUNTIME_ERR))
                        tcSourceCoverage = CoverageManager.getProgress(tc, Environment.getInstance().getTypeofCoverage());
                    else tcSourceCoverage = 0;
                }

                TestCaseManager.exportTestcaseCoverage(tc, tcFunctionalCoverage, tcSourceCoverage);

                content.append("\"").append(functionPath).append("\"").append(",").append(tcName).append(",")
                        .append(Utils.round(tcFunctionalCoverage * 100, 4) + "%").append(",")
                        .append(Utils.round(tcSourceCoverage * 100, 4) + "%").append("\n");
            }
        }
        Utils.writeContentToFile(String.valueOf(content), exportPath);
        logger.debug("Finish");
    }

    public void initialTestcaseMap(ITestcaseNode root) {
        logger.debug("Initializing map testcases...");
        testCaseMap.clear();
        Stack<ITestcaseNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            ITestcaseNode item = stack.pop();
            if (item instanceof TestNormalSubprogramNode) {
                ICommonFunctionNode functionNode = null;
                try {
                    functionNode = UIController.searchFunctionNodeByPath(((TestNormalSubprogramNode) item).getName());
                    if (functionNode instanceof IFunctionNode) {
                        List<TestCase> testCases = TestCaseManager.getTestCasesByFunctionWithoutData(functionNode);
//                        List<TestCase> testCases = TestCaseManager.getTestCasesByFunction(functionNode);
                        logger.debug("Found " + testCases.size()   + " test cases in function " + functionNode.getRelativePathToRoot());
                        if (!testCases.isEmpty())
                            testCaseMap.put((IFunctionNode) functionNode, testCases);
                    }
                } catch (FunctionNodeNotFoundException e) {
                    e.printStackTrace();
                }

            }
            else {
                for (int i = item.getChildren().size() - 1; i >= 0; i--) {
                    stack.push(item.getChildren().get(i));
                }
            }
        }
    }
}
