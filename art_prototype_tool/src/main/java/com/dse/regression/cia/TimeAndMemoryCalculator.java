package com.dse.regression.cia;

import com.dse.logger.AkaLogger;
import com.dse.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class TimeAndMemoryCalculator {
    AkaLogger logger = AkaLogger.get(TimeAndMemoryCalculator.class);
    CalculateThread thread;
    long timePerSecond;
    long beforeMemory;
    long afterMemory;
    long usedMemory;

    List<History> histories = new ArrayList<>();

    int status = RUNNING;
    final static int RUNNING = 1;
    final static int PAUSED = 0;
    final static int FINISHED = -1;

    long sleepStep = 3000; // milliseconds
    private static TimeAndMemoryCalculator instance = null;

    private TimeAndMemoryCalculator() {

    }

    public static TimeAndMemoryCalculator getInstance() {
        if (instance == null) instance = new TimeAndMemoryCalculator();
        return instance;
    }

    public void reset() {
        timePerSecond = 0;
        usedMemory = 0;
        beforeMemory = 0;
        afterMemory = 0;
        histories.clear();
//        thread = new CalculateThread();
    }

    public void start() {
        logger.debug("Start calculator!");
        status = RUNNING;
        thread = new CalculateThread();
        thread.start();
    }

    public void pause() {
        logger.debug("Pause calculator!");
        status = PAUSED;
    }

    public void continued() {
        logger.debug("Continue calculator!");
        status = RUNNING;
    }

    public void finish() {
        logger.debug("Finish calculator!");
        status = FINISHED;
    }

    public void export(String toPath) {
        StringBuilder content = new StringBuilder("Time, Real Memory, Used Memory,\n");
        for (History history : histories) {
            content.append(history.second).append(",").append(history.realMemory).append(",").append(history.usedMemory).append(",\n");
        }
        Utils.writeContentToFile(String.valueOf(content), toPath);
    }


    class CalculateThread extends Thread {

        @Override
        public void run() {
            reset();
//            Runtime runtime = Runtime.getRuntime();
            Runtime.getRuntime().gc();
//        calculator.beforeMemory = runtime.totalMemory() - runtime.freeMemory();
//        calculator.afterMemory = runtime.totalMemory() - runtime.freeMemory();
//        calculator.usedMemory = calculator.afterMemory - calculator.beforeMemory;
            beforeMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
            while (true) {
                if (status == RUNNING) {
                    afterMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
                    usedMemory = afterMemory - beforeMemory;
//                    logger.debug("Add history: " + timePerSecond + " --- " + usedMemory);
                    histories.add(new History(timePerSecond, afterMemory, usedMemory));
                    timePerSecond += sleepStep / 1000;
                    try {
                        Thread.sleep(sleepStep);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else if (status == FINISHED) break;
            }
        }
    }

    class History {
        long second;
        long realMemory;
        long usedMemory;

        public History(long second, long realMemory, long usedMemory) {
            this.second = second;
            this.realMemory = realMemory;
            this.usedMemory = usedMemory;
        }
    }

}
