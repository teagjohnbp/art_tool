/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_METHOD_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_METHOD_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <microhttpd.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_METHOD_H_
#define AKA_INCLUDE__AFB_METHOD_H_
#include "afb-method.akaignore.h"
#endif


/** Instrumented function get_method(const char*) */
enum afb_method get_method(const char *method)
{AKA_mark("Calling: ./app-framework-binder/src/afb-method.c/get_method(const char*)");AKA_fCall++;
		AKA_mark("lis===25###sois===753###eois===769###lif===2###soif===58###eoif===74###ins===true###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");switch(method[0] & ~' '){
			case 'C': if(method[0] & ~' ' == 'C')AKA_mark("lis===26###sois===774###eois===783###lif===3###soif===79###eoif===88###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");

				AKA_mark("lis===27###sois===786###eois===812###lif===4###soif===91###eoif===117###ins===true###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");return afb_method_connect;

			case 'D': if(method[0] & ~' ' == 'D')AKA_mark("lis===28###sois===814###eois===823###lif===5###soif===119###eoif===128###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");

				AKA_mark("lis===29###sois===826###eois===851###lif===6###soif===131###eoif===156###ins===true###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");return afb_method_delete;

			case 'G': if(method[0] & ~' ' == 'G')AKA_mark("lis===30###sois===853###eois===862###lif===7###soif===158###eoif===167###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");

				AKA_mark("lis===31###sois===865###eois===887###lif===8###soif===170###eoif===192###ins===true###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");return afb_method_get;

			case 'H': if(method[0] & ~' ' == 'H')AKA_mark("lis===32###sois===889###eois===898###lif===9###soif===194###eoif===203###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");

				AKA_mark("lis===33###sois===901###eois===924###lif===10###soif===206###eoif===229###ins===true###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");return afb_method_head;

			case 'O': if(method[0] & ~' ' == 'O')AKA_mark("lis===34###sois===926###eois===935###lif===11###soif===231###eoif===240###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");

				AKA_mark("lis===35###sois===938###eois===964###lif===12###soif===243###eoif===269###ins===true###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");return afb_method_options;

			case 'P': if(method[0] & ~' ' == 'P')AKA_mark("lis===36###sois===966###eois===975###lif===13###soif===271###eoif===280###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");

				AKA_mark("lis===37###sois===986###eois===1002###lif===14###soif===291###eoif===307###ins===true###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");switch(method[1] & ~' '){
					case 'A': if(method[1] & ~' ' == 'A')AKA_mark("lis===38###sois===1008###eois===1017###lif===15###soif===313###eoif===322###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");

						AKA_mark("lis===39###sois===1021###eois===1045###lif===16###soif===326###eoif===350###ins===true###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");return afb_method_patch;

					case 'O': if(method[1] & ~' ' == 'O')AKA_mark("lis===40###sois===1048###eois===1057###lif===17###soif===353###eoif===362###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");

						AKA_mark("lis===41###sois===1061###eois===1084###lif===18###soif===366###eoif===389###ins===true###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");return afb_method_post;

					case 'U': if(method[1] & ~' ' == 'U')AKA_mark("lis===42###sois===1087###eois===1096###lif===19###soif===392###eoif===401###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");

						AKA_mark("lis===43###sois===1100###eois===1122###lif===20###soif===405###eoif===427###ins===true###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");return afb_method_put;

		}

				AKA_mark("lis===45###sois===1129###eois===1135###lif===22###soif===434###eoif===440###ins===true###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");break;

			case 'T': if(method[0] & ~' ' == 'T')AKA_mark("lis===46###sois===1137###eois===1146###lif===23###soif===442###eoif===451###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");

				AKA_mark("lis===47###sois===1149###eois===1173###lif===24###soif===454###eoif===478###ins===true###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");return afb_method_trace;

	}

		AKA_mark("lis===49###sois===1178###eois===1201###lif===26###soif===483###eoif===506###ins===true###function===./app-framework-binder/src/afb-method.c/get_method(const char*)");return afb_method_none;

}

#if !defined(MHD_HTTP_METHOD_PATCH)
#define MHD_HTTP_METHOD_PATCH "PATCH"
#endif
/** Instrumented function get_method_name(enum afb_method) */
const char *get_method_name(enum afb_method method)
{AKA_mark("Calling: ./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");AKA_fCall++;
		AKA_mark("lis===57###sois===1349###eois===1355###lif===2###soif===63###eoif===69###ins===true###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");switch(method){
			case afb_method_get: if(method == afb_method_get)AKA_mark("lis===58###sois===1360###eois===1380###lif===3###soif===74###eoif===94###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");

				AKA_mark("lis===59###sois===1383###eois===1410###lif===4###soif===97###eoif===124###ins===true###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");return MHD_HTTP_METHOD_GET;

			case afb_method_post: if(method == afb_method_post)AKA_mark("lis===60###sois===1412###eois===1433###lif===5###soif===126###eoif===147###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");

				AKA_mark("lis===61###sois===1436###eois===1464###lif===6###soif===150###eoif===178###ins===true###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");return MHD_HTTP_METHOD_POST;

			case afb_method_head: if(method == afb_method_head)AKA_mark("lis===62###sois===1466###eois===1487###lif===7###soif===180###eoif===201###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");

				AKA_mark("lis===63###sois===1490###eois===1518###lif===8###soif===204###eoif===232###ins===true###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");return MHD_HTTP_METHOD_HEAD;

			case afb_method_connect: if(method == afb_method_connect)AKA_mark("lis===64###sois===1520###eois===1544###lif===9###soif===234###eoif===258###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");

				AKA_mark("lis===65###sois===1547###eois===1578###lif===10###soif===261###eoif===292###ins===true###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");return MHD_HTTP_METHOD_CONNECT;

			case afb_method_delete: if(method == afb_method_delete)AKA_mark("lis===66###sois===1580###eois===1603###lif===11###soif===294###eoif===317###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");

				AKA_mark("lis===67###sois===1606###eois===1636###lif===12###soif===320###eoif===350###ins===true###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");return MHD_HTTP_METHOD_DELETE;

			case afb_method_options: if(method == afb_method_options)AKA_mark("lis===68###sois===1638###eois===1662###lif===13###soif===352###eoif===376###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");

				AKA_mark("lis===69###sois===1665###eois===1696###lif===14###soif===379###eoif===410###ins===true###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");return MHD_HTTP_METHOD_OPTIONS;

			case afb_method_patch: if(method == afb_method_patch)AKA_mark("lis===70###sois===1698###eois===1720###lif===15###soif===412###eoif===434###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");

				AKA_mark("lis===71###sois===1723###eois===1752###lif===16###soif===437###eoif===466###ins===true###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");return MHD_HTTP_METHOD_PATCH;

			case afb_method_put: if(method == afb_method_put)AKA_mark("lis===72###sois===1754###eois===1774###lif===17###soif===468###eoif===488###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");

				AKA_mark("lis===73###sois===1777###eois===1804###lif===18###soif===491###eoif===518###ins===true###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");return MHD_HTTP_METHOD_PUT;

			case afb_method_trace: if(method == afb_method_trace)AKA_mark("lis===74###sois===1806###eois===1828###lif===19###soif===520###eoif===542###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");

				AKA_mark("lis===75###sois===1831###eois===1860###lif===20###soif===545###eoif===574###ins===true###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");return MHD_HTTP_METHOD_TRACE;

			default: if(method != afb_method_get && method != afb_method_post && method != afb_method_head && method != afb_method_connect && method != afb_method_delete && method != afb_method_options && method != afb_method_patch && method != afb_method_put && method != afb_method_trace)AKA_mark("lis===76###sois===1862###eois===1870###lif===21###soif===576###eoif===584###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");

				AKA_mark("lis===77###sois===1873###eois===1885###lif===22###soif===587###eoif===599###ins===true###function===./app-framework-binder/src/afb-method.c/get_method_name(enum afb_method)");return NULL;

	}

}



#endif

