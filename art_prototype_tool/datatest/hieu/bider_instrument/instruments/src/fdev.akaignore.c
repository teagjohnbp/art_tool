/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_FDEV_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_FDEV_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#define FDEV_PROVIDER
/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_H_
#define AKA_INCLUDE__FDEV_H_
#include "fdev.akaignore.h"
#endif

#ifndef FDEV_STRUCT
#define FDEV_STRUCT

struct fdev
{
	int fd;
	uint32_t events;
	unsigned refcount;
	struct fdev_itf *itf;
	void *closure_itf;
	void (*callback)(void*,uint32_t,struct fdev*);
	void *closure_callback;
};

#endif

/** Instrumented function fdev_create(int) */
struct fdev *fdev_create(int fd)
{AKA_mark("Calling: ./app-framework-binder/src/fdev.c/fdev_create(int)");AKA_fCall++;
		AKA_mark("lis===39###sois===981###eois===999###lif===2###soif===36###eoif===54###ins===true###function===./app-framework-binder/src/fdev.c/fdev_create(int)");struct fdev *fdev;


		AKA_mark("lis===41###sois===1002###eois===1033###lif===4###soif===57###eoif===88###ins===true###function===./app-framework-binder/src/fdev.c/fdev_create(int)");fdev = calloc(1, sizeof *fdev);

		if (AKA_mark("lis===42###sois===1039###eois===1044###lif===5###soif===94###eoif===99###ifc===true###function===./app-framework-binder/src/fdev.c/fdev_create(int)") && (AKA_mark("lis===42###sois===1039###eois===1044###lif===5###soif===94###eoif===99###isc===true###function===./app-framework-binder/src/fdev.c/fdev_create(int)")&&!fdev)) {
		AKA_mark("lis===43###sois===1048###eois===1063###lif===6###soif===103###eoif===118###ins===true###function===./app-framework-binder/src/fdev.c/fdev_create(int)");errno = ENOMEM;
	}
	else {
				AKA_mark("lis===45###sois===1074###eois===1088###lif===8###soif===129###eoif===143###ins===true###function===./app-framework-binder/src/fdev.c/fdev_create(int)");fdev->fd = fd;

				AKA_mark("lis===46###sois===1091###eois===1110###lif===9###soif===146###eoif===165###ins===true###function===./app-framework-binder/src/fdev.c/fdev_create(int)");fdev->refcount = 3;
 /* set autoclose by default */
	}

		AKA_mark("lis===48###sois===1146###eois===1158###lif===11###soif===201###eoif===213###ins===true###function===./app-framework-binder/src/fdev.c/fdev_create(int)");return fdev;

}

/** Instrumented function fdev_set_itf(struct fdev*,struct fdev_itf*,void*) */
void fdev_set_itf(struct fdev *fdev, struct fdev_itf *itf, void *closure_itf)
{AKA_mark("Calling: ./app-framework-binder/src/fdev.c/fdev_set_itf(struct fdev*,struct fdev_itf*,void*)");AKA_fCall++;
		AKA_mark("lis===53###sois===1243###eois===1259###lif===2###soif===81###eoif===97###ins===true###function===./app-framework-binder/src/fdev.c/fdev_set_itf(struct fdev*,struct fdev_itf*,void*)");fdev->itf = itf;

		AKA_mark("lis===54###sois===1261###eois===1293###lif===3###soif===99###eoif===131###ins===true###function===./app-framework-binder/src/fdev.c/fdev_set_itf(struct fdev*,struct fdev_itf*,void*)");fdev->closure_itf = closure_itf;

}

/** Instrumented function fdev_dispatch(struct fdev*,uint32_t) */
void fdev_dispatch(struct fdev *fdev, uint32_t events)
{AKA_mark("Calling: ./app-framework-binder/src/fdev.c/fdev_dispatch(struct fdev*,uint32_t)");AKA_fCall++;
		if (AKA_mark("lis===59###sois===1359###eois===1373###lif===2###soif===62###eoif===76###ifc===true###function===./app-framework-binder/src/fdev.c/fdev_dispatch(struct fdev*,uint32_t)") && (AKA_mark("lis===59###sois===1359###eois===1373###lif===2###soif===62###eoif===76###isc===true###function===./app-framework-binder/src/fdev.c/fdev_dispatch(struct fdev*,uint32_t)")&&fdev->callback)) {
		AKA_mark("lis===60###sois===1377###eois===1430###lif===3###soif===80###eoif===133###ins===true###function===./app-framework-binder/src/fdev.c/fdev_dispatch(struct fdev*,uint32_t)");fdev->callback(fdev->closure_callback, events, fdev);
	}
	else {AKA_mark("lis===-59-###sois===-1359-###eois===-135914-###lif===-2-###soif===-###eoif===-76-###ins===true###function===./app-framework-binder/src/fdev.c/fdev_dispatch(struct fdev*,uint32_t)");}

}

/** Instrumented function fdev_addref(struct fdev*) */
struct fdev *fdev_addref(struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/fdev.c/fdev_addref(struct fdev*)");AKA_fCall++;
		if (AKA_mark("lis===65###sois===1485###eois===1489###lif===2###soif===51###eoif===55###ifc===true###function===./app-framework-binder/src/fdev.c/fdev_addref(struct fdev*)") && (AKA_mark("lis===65###sois===1485###eois===1489###lif===2###soif===51###eoif===55###isc===true###function===./app-framework-binder/src/fdev.c/fdev_addref(struct fdev*)")&&fdev)) {
		AKA_mark("lis===66###sois===1493###eois===1550###lif===3###soif===59###eoif===116###ins===true###function===./app-framework-binder/src/fdev.c/fdev_addref(struct fdev*)");__atomic_add_fetch(&fdev->refcount, 2, __ATOMIC_RELAXED);
	}
	else {AKA_mark("lis===-65-###sois===-1485-###eois===-14854-###lif===-2-###soif===-###eoif===-55-###ins===true###function===./app-framework-binder/src/fdev.c/fdev_addref(struct fdev*)");}

		AKA_mark("lis===67###sois===1552###eois===1564###lif===4###soif===118###eoif===130###ins===true###function===./app-framework-binder/src/fdev.c/fdev_addref(struct fdev*)");return fdev;

}

/** Instrumented function fdev_unref(struct fdev*) */
void fdev_unref(struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)");AKA_fCall++;
		if (AKA_mark("lis===72###sois===1610###eois===1679###lif===2###soif===42###eoif===111###ifc===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)") && ((AKA_mark("lis===72###sois===1610###eois===1614###lif===2###soif===42###eoif===46###isc===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)")&&fdev)	&&(AKA_mark("lis===72###sois===1618###eois===1679###lif===2###soif===50###eoif===111###isc===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)")&&__atomic_sub_fetch(&fdev->refcount, 2, __ATOMIC_RELAXED) <= 1))) {
				if (AKA_mark("lis===73###sois===1689###eois===1698###lif===3###soif===121###eoif===130###ifc===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)") && (AKA_mark("lis===73###sois===1689###eois===1698###lif===3###soif===121###eoif===130###isc===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)")&&fdev->itf)) {
						AKA_mark("lis===74###sois===1705###eois===1749###lif===4###soif===137###eoif===181###ins===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)");fdev->itf->disable(fdev->closure_itf, fdev);

						if (AKA_mark("lis===75###sois===1757###eois===1773###lif===5###soif===189###eoif===205###ifc===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)") && (AKA_mark("lis===75###sois===1757###eois===1773###lif===5###soif===189###eoif===205###isc===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)")&&fdev->itf->unref)) {
				AKA_mark("lis===76###sois===1779###eois===1815###lif===6###soif===211###eoif===247###ins===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)");fdev->itf->unref(fdev->closure_itf);
			}
			else {AKA_mark("lis===-75-###sois===-1757-###eois===-175716-###lif===-5-###soif===-###eoif===-205-###ins===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)");}

		}
		else {AKA_mark("lis===-73-###sois===-1689-###eois===-16899-###lif===-3-###soif===-###eoif===-130-###ins===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)");}

				if (AKA_mark("lis===78###sois===1826###eois===1840###lif===8###soif===258###eoif===272###ifc===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)") && (AKA_mark("lis===78###sois===1826###eois===1840###lif===8###soif===258###eoif===272###isc===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)")&&fdev->refcount)) {
			AKA_mark("lis===79###sois===1845###eois===1861###lif===9###soif===277###eoif===293###ins===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)");close(fdev->fd);
		}
		else {AKA_mark("lis===-78-###sois===-1826-###eois===-182614-###lif===-8-###soif===-###eoif===-272-###ins===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)");}

				AKA_mark("lis===80###sois===1864###eois===1875###lif===10###soif===296###eoif===307###ins===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)");free(fdev);

	}
	else {AKA_mark("lis===-72-###sois===-1610-###eois===-161069-###lif===-2-###soif===-###eoif===-111-###ins===true###function===./app-framework-binder/src/fdev.c/fdev_unref(struct fdev*)");}

}

/** Instrumented function fdev_fd(const struct fdev*) */
int fdev_fd(const struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/fdev.c/fdev_fd(const struct fdev*)");AKA_fCall++;
		AKA_mark("lis===86###sois===1922###eois===1938###lif===2###soif===40###eoif===56###ins===true###function===./app-framework-binder/src/fdev.c/fdev_fd(const struct fdev*)");return fdev->fd;

}

/** Instrumented function fdev_events(const struct fdev*) */
uint32_t fdev_events(const struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/fdev.c/fdev_events(const struct fdev*)");AKA_fCall++;
		AKA_mark("lis===91###sois===1991###eois===2011###lif===2###soif===49###eoif===69###ins===true###function===./app-framework-binder/src/fdev.c/fdev_events(const struct fdev*)");return fdev->events;

}

/** Instrumented function fdev_autoclose(const struct fdev*) */
int fdev_autoclose(const struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/fdev.c/fdev_autoclose(const struct fdev*)");AKA_fCall++;
		AKA_mark("lis===96###sois===2062###eois===2088###lif===2###soif===47###eoif===73###ins===true###function===./app-framework-binder/src/fdev.c/fdev_autoclose(const struct fdev*)");return 1 & fdev->refcount;

}

/** Instrumented function is_active(struct fdev*) */
static inline int is_active(struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/fdev.c/is_active(struct fdev*)");AKA_fCall++;
		AKA_mark("lis===101###sois===2142###eois===2166###lif===2###soif===50###eoif===74###ins===true###function===./app-framework-binder/src/fdev.c/is_active(struct fdev*)");return !!fdev->callback;

}

/** Instrumented function update_activity(struct fdev*,int) */
static inline void update_activity(struct fdev *fdev, int old_active)
{AKA_mark("Calling: ./app-framework-binder/src/fdev.c/update_activity(struct fdev*,int)");AKA_fCall++;
		if (AKA_mark("lis===106###sois===2247###eois===2262###lif===2###soif===77###eoif===92###ifc===true###function===./app-framework-binder/src/fdev.c/update_activity(struct fdev*,int)") && (AKA_mark("lis===106###sois===2247###eois===2262###lif===2###soif===77###eoif===92###isc===true###function===./app-framework-binder/src/fdev.c/update_activity(struct fdev*,int)")&&is_active(fdev))) {
				if (AKA_mark("lis===107###sois===2272###eois===2283###lif===3###soif===102###eoif===113###ifc===true###function===./app-framework-binder/src/fdev.c/update_activity(struct fdev*,int)") && (AKA_mark("lis===107###sois===2272###eois===2283###lif===3###soif===102###eoif===113###isc===true###function===./app-framework-binder/src/fdev.c/update_activity(struct fdev*,int)")&&!old_active)) {
			AKA_mark("lis===108###sois===2288###eois===2331###lif===4###soif===118###eoif===161###ins===true###function===./app-framework-binder/src/fdev.c/update_activity(struct fdev*,int)");fdev->itf->enable(fdev->closure_itf, fdev);
		}
		else {AKA_mark("lis===-107-###sois===-2272-###eois===-227211-###lif===-3-###soif===-###eoif===-113-###ins===true###function===./app-framework-binder/src/fdev.c/update_activity(struct fdev*,int)");}

	}
	else {
				if (AKA_mark("lis===110###sois===2348###eois===2358###lif===6###soif===178###eoif===188###ifc===true###function===./app-framework-binder/src/fdev.c/update_activity(struct fdev*,int)") && (AKA_mark("lis===110###sois===2348###eois===2358###lif===6###soif===178###eoif===188###isc===true###function===./app-framework-binder/src/fdev.c/update_activity(struct fdev*,int)")&&old_active)) {
			AKA_mark("lis===111###sois===2363###eois===2407###lif===7###soif===193###eoif===237###ins===true###function===./app-framework-binder/src/fdev.c/update_activity(struct fdev*,int)");fdev->itf->disable(fdev->closure_itf, fdev);
		}
		else {AKA_mark("lis===-110-###sois===-2348-###eois===-234810-###lif===-6-###soif===-###eoif===-188-###ins===true###function===./app-framework-binder/src/fdev.c/update_activity(struct fdev*,int)");}

	}

}

/** Instrumented function fdev_set_callback(struct fdev*,void(*callback)(void*,uint32_t,struct fdev*),void*) */
void fdev_set_callback(struct fdev *fdev, void (*callback)(void*,uint32_t,struct fdev*), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/fdev.c/fdev_set_callback(struct fdev*,void(*callback)(void*,uint32_t,struct fdev*),void*)");AKA_fCall++;
		AKA_mark("lis===117###sois===2521###eois===2528###lif===2###soif===107###eoif===114###ins===true###function===./app-framework-binder/src/fdev.c/fdev_set_callback(struct fdev*,void(*callback)(void*,uint32_t,struct fdev*),void*)");int oa;


		AKA_mark("lis===119###sois===2531###eois===2552###lif===4###soif===117###eoif===138###ins===true###function===./app-framework-binder/src/fdev.c/fdev_set_callback(struct fdev*,void(*callback)(void*,uint32_t,struct fdev*),void*)");oa = is_active(fdev);

		AKA_mark("lis===120###sois===2554###eois===2580###lif===5###soif===140###eoif===166###ins===true###function===./app-framework-binder/src/fdev.c/fdev_set_callback(struct fdev*,void(*callback)(void*,uint32_t,struct fdev*),void*)");fdev->callback = callback;

		AKA_mark("lis===121###sois===2582###eois===2615###lif===6###soif===168###eoif===201###ins===true###function===./app-framework-binder/src/fdev.c/fdev_set_callback(struct fdev*,void(*callback)(void*,uint32_t,struct fdev*),void*)");fdev->closure_callback = closure;

		AKA_mark("lis===122###sois===2617###eois===2643###lif===7###soif===203###eoif===229###ins===true###function===./app-framework-binder/src/fdev.c/fdev_set_callback(struct fdev*,void(*callback)(void*,uint32_t,struct fdev*),void*)");update_activity(fdev, oa);

}

/** Instrumented function fdev_set_events(struct fdev*,uint32_t) */
void fdev_set_events(struct fdev *fdev, uint32_t events)
{AKA_mark("Calling: ./app-framework-binder/src/fdev.c/fdev_set_events(struct fdev*,uint32_t)");AKA_fCall++;
		if (AKA_mark("lis===127###sois===2711###eois===2733###lif===2###soif===64###eoif===86###ifc===true###function===./app-framework-binder/src/fdev.c/fdev_set_events(struct fdev*,uint32_t)") && (AKA_mark("lis===127###sois===2711###eois===2733###lif===2###soif===64###eoif===86###isc===true###function===./app-framework-binder/src/fdev.c/fdev_set_events(struct fdev*,uint32_t)")&&events != fdev->events)) {
				AKA_mark("lis===128###sois===2739###eois===2761###lif===3###soif===92###eoif===114###ins===true###function===./app-framework-binder/src/fdev.c/fdev_set_events(struct fdev*,uint32_t)");fdev->events = events;

				if (AKA_mark("lis===129###sois===2768###eois===2783###lif===4###soif===121###eoif===136###ifc===true###function===./app-framework-binder/src/fdev.c/fdev_set_events(struct fdev*,uint32_t)") && (AKA_mark("lis===129###sois===2768###eois===2783###lif===4###soif===121###eoif===136###isc===true###function===./app-framework-binder/src/fdev.c/fdev_set_events(struct fdev*,uint32_t)")&&is_active(fdev))) {
			AKA_mark("lis===130###sois===2788###eois===2831###lif===5###soif===141###eoif===184###ins===true###function===./app-framework-binder/src/fdev.c/fdev_set_events(struct fdev*,uint32_t)");fdev->itf->update(fdev->closure_itf, fdev);
		}
		else {AKA_mark("lis===-129-###sois===-2768-###eois===-276815-###lif===-4-###soif===-###eoif===-136-###ins===true###function===./app-framework-binder/src/fdev.c/fdev_set_events(struct fdev*,uint32_t)");}

	}
	else {AKA_mark("lis===-127-###sois===-2711-###eois===-271122-###lif===-2-###soif===-###eoif===-86-###ins===true###function===./app-framework-binder/src/fdev.c/fdev_set_events(struct fdev*,uint32_t)");}

}

/** Instrumented function fdev_set_autoclose(struct fdev*,int) */
void fdev_set_autoclose(struct fdev *fdev, int autoclose)
{AKA_mark("Calling: ./app-framework-binder/src/fdev.c/fdev_set_autoclose(struct fdev*,int)");AKA_fCall++;
		if (AKA_mark("lis===136###sois===2903###eois===2912###lif===2###soif===65###eoif===74###ifc===true###function===./app-framework-binder/src/fdev.c/fdev_set_autoclose(struct fdev*,int)") && (AKA_mark("lis===136###sois===2903###eois===2912###lif===2###soif===65###eoif===74###isc===true###function===./app-framework-binder/src/fdev.c/fdev_set_autoclose(struct fdev*,int)")&&autoclose)) {
		AKA_mark("lis===137###sois===2916###eois===2946###lif===3###soif===78###eoif===108###ins===true###function===./app-framework-binder/src/fdev.c/fdev_set_autoclose(struct fdev*,int)");fdev->refcount |= (unsigned)1;
	}
	else {
		AKA_mark("lis===139###sois===2955###eois===2986###lif===5###soif===117###eoif===148###ins===true###function===./app-framework-binder/src/fdev.c/fdev_set_autoclose(struct fdev*,int)");fdev->refcount &= ~(unsigned)1;
	}

}


#endif

