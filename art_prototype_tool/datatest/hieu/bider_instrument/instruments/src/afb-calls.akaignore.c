/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_CALLS_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_CALLS_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <json-c/json.h>

#define AFB_BINDING_VERSION 0
#include <afb/afb-binding.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CALLS_H_
#define AKA_INCLUDE__AFB_CALLS_H_
#include "afb-calls.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_EVT_H_
#define AKA_INCLUDE__AFB_EVT_H_
#include "afb-evt.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_EXPORT_H_
#define AKA_INCLUDE__AFB_EXPORT_H_
#include "afb-export.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HOOK_H_
#define AKA_INCLUDE__AFB_HOOK_H_
#include "afb-hook.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_MSG_JSON_H_
#define AKA_INCLUDE__AFB_MSG_JSON_H_
#include "afb-msg-json.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SESSION_H_
#define AKA_INCLUDE__AFB_SESSION_H_
#include "afb-session.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_XREQ_H_
#define AKA_INCLUDE__AFB_XREQ_H_
#include "afb-xreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_ERROR_TEXT_H_
#define AKA_INCLUDE__AFB_ERROR_TEXT_H_
#include "afb-error-text.akaignore.h"
#endif


/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__JOBS_H_
#define AKA_INCLUDE__JOBS_H_
#include "jobs.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif


#define CALLFLAGS            (afb_req_x2_subcall_api_session|afb_req_x2_subcall_catch_events)
#define LEGACY_SUBCALLFLAGS  (afb_req_x2_subcall_pass_events|afb_req_x2_subcall_on_behalf)


/************************************************************************/

struct modes
{
	unsigned hooked: 1;
	unsigned sync: 1;
	unsigned legacy: 1;
};

#define mode_sync  ((struct modes){ .hooked=0, .sync=1, .legacy=0 })
#define mode_async  ((struct modes){ .hooked=0, .sync=0, .legacy=0 })
#define mode_legacy_sync  ((struct modes){ .hooked=0, .sync=1, .legacy=1 })
#define mode_legacy_async  ((struct modes){ .hooked=0, .sync=0, .legacy=1 })

#if WITH_AFB_HOOK
#define mode_hooked_sync  ((struct modes){ .hooked=1, .sync=1, .legacy=0 })
#define mode_hooked_async  ((struct modes){ .hooked=1, .sync=0, .legacy=0 })
#define mode_hooked_legacy_sync  ((struct modes){ .hooked=1, .sync=1, .legacy=1 })
#define mode_hooked_legacy_async  ((struct modes){ .hooked=1, .sync=0, .legacy=1 })
#endif

union callback {
	void *any;
	union {
		void (*legacy_v1)(void*, int, struct json_object*);
		void (*legacy_v2)(void*, int, struct json_object*, struct afb_req_x1);
		void (*legacy_v3)(void*, int, struct json_object*, struct afb_req_x2*);
		void (*x3)(void*, struct json_object*, const char*, const char *, struct afb_req_x2*);
	} subcall;
	union {
		void (*legacy_v12)(void*, int, struct json_object*);
		void (*legacy_v3)(void*, int, struct json_object*, struct afb_api_x3*);
		void (*x3)(void*, struct json_object*, const char*, const char*, struct afb_api_x3*);
	} call;
};

struct callreq
{
	struct afb_xreq xreq;

	struct afb_export *export;

	struct modes mode;

	int flags;

	union {
		struct {
			struct jobloop *jobloop;
			int returned;
			int status;
			struct json_object **object;
			char **error;
			char **info;
		};
		struct {
			union callback callback;
			void *closure;
			union {
				void (*final)(void*, struct json_object*, const char*, const char*, union callback, struct afb_export*,struct afb_xreq*);
				void (*legacy_final)(void*, int, struct json_object*, union callback, struct afb_export*,struct afb_xreq*);
			};
		};
	};
};

/******************************************************************************/

/** Instrumented function store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**) */
static int store_reply(
		struct json_object *iobject, const char *ierror, const char *iinfo,
		struct json_object **sobject, char **serror, char **sinfo)
{AKA_mark("Calling: ./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");AKA_fCall++;
		if (AKA_mark("lis===117###sois===3427###eois===3433###lif===4###soif===162###eoif===168###ifc===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)") && (AKA_mark("lis===117###sois===3427###eois===3433###lif===4###soif===162###eoif===168###isc===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)")&&serror)) {
				if (AKA_mark("lis===118###sois===3443###eois===3450###lif===5###soif===178###eoif===185###ifc===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)") && (AKA_mark("lis===118###sois===3443###eois===3450###lif===5###soif===178###eoif===185###isc===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)")&&!ierror)) {
			AKA_mark("lis===119###sois===3455###eois===3470###lif===6###soif===190###eoif===205###ins===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");*serror = NULL;
		}
		else {
			if (AKA_mark("lis===120###sois===3482###eois===3509###lif===7###soif===217###eoif===244###ifc===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)") && (AKA_mark("lis===120###sois===3482###eois===3509###lif===7###soif===217###eoif===244###isc===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)")&&!(*serror = strdup(ierror)))) {
							AKA_mark("lis===121###sois===3516###eois===3555###lif===8###soif===251###eoif===290###ins===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");ERROR("can't report error %s", ierror);

							AKA_mark("lis===122###sois===3559###eois===3584###lif===9###soif===294###eoif===319###ins===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");json_object_put(iobject);

							AKA_mark("lis===123###sois===3588###eois===3603###lif===10###soif===323###eoif===338###ins===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");iobject = NULL;

							AKA_mark("lis===124###sois===3607###eois===3620###lif===11###soif===342###eoif===355###ins===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");iinfo = NULL;

		}
			else {AKA_mark("lis===-120-###sois===-3482-###eois===-348227-###lif===-7-###soif===-###eoif===-244-###ins===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");}
		}

	}
	else {AKA_mark("lis===-117-###sois===-3427-###eois===-34276-###lif===-4-###soif===-###eoif===-168-###ins===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");}


		if (AKA_mark("lis===128###sois===3634###eois===3641###lif===15###soif===369###eoif===376###ifc===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)") && (AKA_mark("lis===128###sois===3634###eois===3641###lif===15###soif===369###eoif===376###isc===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)")&&sobject)) {
		AKA_mark("lis===129###sois===3645###eois===3664###lif===16###soif===380###eoif===399###ins===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");*sobject = iobject;
	}
	else {
		AKA_mark("lis===131###sois===3673###eois===3698###lif===18###soif===408###eoif===433###ins===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");json_object_put(iobject);
	}


		if (AKA_mark("lis===133###sois===3705###eois===3710###lif===20###soif===440###eoif===445###ifc===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)") && (AKA_mark("lis===133###sois===3705###eois===3710###lif===20###soif===440###eoif===445###isc===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)")&&sinfo)) {
				if (AKA_mark("lis===134###sois===3720###eois===3726###lif===21###soif===455###eoif===461###ifc===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)") && (AKA_mark("lis===134###sois===3720###eois===3726###lif===21###soif===455###eoif===461###isc===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)")&&!iinfo)) {
			AKA_mark("lis===135###sois===3731###eois===3745###lif===22###soif===466###eoif===480###ins===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");*sinfo = NULL;
		}
		else {
			if (AKA_mark("lis===136###sois===3757###eois===3782###lif===23###soif===492###eoif===517###ifc===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)") && (AKA_mark("lis===136###sois===3757###eois===3782###lif===23###soif===492###eoif===517###isc===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)")&&!(*sinfo = strdup(iinfo)))) {
				AKA_mark("lis===137###sois===3787###eois===3824###lif===24###soif===522###eoif===559###ins===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");ERROR("can't report info %s", iinfo);
			}
			else {AKA_mark("lis===-136-###sois===-3757-###eois===-375725-###lif===-23-###soif===-###eoif===-517-###ins===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");}
		}

	}
	else {AKA_mark("lis===-133-###sois===-3705-###eois===-37055-###lif===-20-###soif===-###eoif===-445-###ins===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");}


		AKA_mark("lis===140###sois===3830###eois===3847###lif===27###soif===565###eoif===582###ins===true###function===./app-framework-binder/src/afb-calls.c/store_reply(struct json_object*,const char*,const char*,struct json_object**,char**,char**)");return -!!ierror;

}

/******************************************************************************/

/** Instrumented function sync_leave(struct callreq*) */
static void sync_leave(struct callreq *callreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-calls.c/sync_leave(struct callreq*)");AKA_fCall++;
		AKA_mark("lis===147###sois===3984###eois===4073###lif===2###soif===51###eoif===140###ins===true###function===./app-framework-binder/src/afb-calls.c/sync_leave(struct callreq*)");struct jobloop *jobloop = __atomic_exchange_n(&callreq->jobloop, NULL, __ATOMIC_RELAXED);

		if (AKA_mark("lis===148###sois===4079###eois===4086###lif===3###soif===146###eoif===153###ifc===true###function===./app-framework-binder/src/afb-calls.c/sync_leave(struct callreq*)") && (AKA_mark("lis===148###sois===4079###eois===4086###lif===3###soif===146###eoif===153###isc===true###function===./app-framework-binder/src/afb-calls.c/sync_leave(struct callreq*)")&&jobloop)) {
		AKA_mark("lis===149###sois===4090###eois===4110###lif===4###soif===157###eoif===177###ins===true###function===./app-framework-binder/src/afb-calls.c/sync_leave(struct callreq*)");jobs_leave(jobloop);
	}
	else {AKA_mark("lis===-148-###sois===-4079-###eois===-40797-###lif===-3-###soif===-###eoif===-153-###ins===true###function===./app-framework-binder/src/afb-calls.c/sync_leave(struct callreq*)");}

}

/** Instrumented function sync_enter(int,void*,struct jobloop*) */
static void sync_enter(int signum, void *closure, struct jobloop *jobloop)
{AKA_mark("Calling: ./app-framework-binder/src/afb-calls.c/sync_enter(int,void*,struct jobloop*)");AKA_fCall++;
		AKA_mark("lis===154###sois===4192###eois===4226###lif===2###soif===78###eoif===112###ins===true###function===./app-framework-binder/src/afb-calls.c/sync_enter(int,void*,struct jobloop*)");struct callreq *callreq = closure;

		if (AKA_mark("lis===155###sois===4232###eois===4239###lif===3###soif===118###eoif===125###ifc===true###function===./app-framework-binder/src/afb-calls.c/sync_enter(int,void*,struct jobloop*)") && (AKA_mark("lis===155###sois===4232###eois===4239###lif===3###soif===118###eoif===125###isc===true###function===./app-framework-binder/src/afb-calls.c/sync_enter(int,void*,struct jobloop*)")&&!signum)) {
				AKA_mark("lis===156###sois===4245###eois===4272###lif===4###soif===131###eoif===158###ins===true###function===./app-framework-binder/src/afb-calls.c/sync_enter(int,void*,struct jobloop*)");callreq->jobloop = jobloop;

		/* Cant instrument this following code */
afb_export_process_xreq(callreq->export, &callreq->xreq);
	}
	else {
				AKA_mark("lis===159###sois===4345###eois===4419###lif===7###soif===231###eoif===305###ins===true###function===./app-framework-binder/src/afb-calls.c/sync_enter(int,void*,struct jobloop*)");afb_xreq_reply(&callreq->xreq, NULL, afb_error_text_internal_error, NULL);

	}

}

/******************************************************************************/

/** Instrumented function callreq_destroy_cb(struct afb_xreq*) */
static void callreq_destroy_cb(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-calls.c/callreq_destroy_cb(struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===167###sois===4565###eois===4579###lif===2###soif===57###eoif===71###ins===true###function===./app-framework-binder/src/afb-calls.c/callre/* Cant instrument this following code */
q_destroy_cb(struct afb_xreq*)");struct callreq
 /* Cant instrument this following code */
*callreq = CONTAINER_OF_XREQ(struct callreq, xreq);

		AKA_mark("lis===169###sois===4634###eois===4681###lif===4###soif===126###eoif===173###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_destroy_cb(struct afb_xreq*)");afb_context_disconnect(&callreq->xreq.context);

		AKA_mark("lis===170###sois===4683###eois===4719###lif===5###soif===175###eoif===211###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_destroy_cb(struct afb_xreq*)");json_object_put(callreq->xreq.json);

		AKA_mark("lis===171###sois===4721###eois===4735###lif===6###soif===213###eoif===227###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_destroy_cb(struct afb_xreq*)");free(callreq);

}

/** Instrumented function callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*) */
static void callreq_reply_cb(struct afb_xreq *xreq, struct json_object *object, const char *error, const char *info)
{AKA_mark("Calling: ./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===176###sois===4859###eois===4873###lif===2###soif===120###eoif===134###ins===true###function===./app-framework-binder/src/afb-calls.c/callre/* Cant instrument this following code */
q_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)");struct callreq
 /* Cant instrument this following code */
*callreq = CONTAINER_OF_XREQ(struct callreq, xreq);

#if WITH_AFB_HOOK
	/* centralized hooking */
	if (callreq->mode.hooked) {
		if (callreq->mode.sync) {
			if (callreq->xreq.caller)
				afb_hook_xreq_subcallsync_result(callreq->xreq.caller, -!!error, object, error, info);
			else
				afb_hook_api_callsync_result(callreq->export, -!!error, object, error, info);
		} else {
			if (callreq->xreq.caller)
				afb_hook_xreq_subcall_result(callreq->xreq.caller, object, error, info);
			else
				afb_hook_api_call_result(callreq->export, object, error, info);
		}
	}
#endif

	/* true report of the result */
		if (AKA_mark("lis===196###sois===5485###eois===5503###lif===22###soif===746###eoif===764###ifc===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)") && (AKA_mark("lis===196###sois===5485###eois===5503###lif===22###soif===746###eoif===764###isc===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)")&&callreq->mode.sync)) {
				AKA_mark("lis===197###sois===5509###eois===5531###lif===23###soif===770###eoif===792###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)");callreq->returned = 1;

				if (AKA_mark("lis===198###sois===5538###eois===5558###lif===24###soif===799###eoif===819###ifc===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)") && (AKA_mark("lis===198###sois===5538###eois===5558###lif===24###soif===799###eoif===819###isc===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)")&&callreq->mode.legacy)) {
						AKA_mark("lis===199###sois===5565###eois===5592###lif===25###soif===826###eoif===853###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)");callreq->status = -!!error;

						if (AKA_mark("lis===200###sois===5600###eois===5615###lif===26###soif===861###eoif===876###ifc===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)") && (AKA_mark("lis===200###sois===5600###eois===5615###lif===26###soif===861###eoif===876###isc===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)")&&callreq->object)) {
				AKA_mark("lis===201###sois===5621###eois===5686###lif===27###soif===882###eoif===947###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)");*callreq->object = afb_msg_json_reply(object, error, info, NULL);
			}
			else {
				AKA_mark("lis===203###sois===5699###eois===5723###lif===29###soif===960###eoif===984###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)");json_object_put(object);
			}

		}
		else {
						AKA_mark("lis===205###sois===5738###eois===5842###lif===31###soif===999###eoif===1103###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)");callreq->status = store_reply(object, error, info,
					callreq->object, callreq->error, callreq->info);

		}

				AKA_mark("lis===208###sois===5849###eois===5869###lif===34###soif===1110###eoif===1130###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)");sync_leave(callreq);

	}
	else {
				if (AKA_mark("lis===210###sois===5886###eois===5906###lif===36###soif===1147###eoif===1167###ifc===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)") && (AKA_mark("lis===210###sois===5886###eois===5906###lif===36###soif===1147###eoif===1167###isc===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)")&&callreq->mode.legacy)) {
						AKA_mark("lis===211###sois===5913###eois===5968###lif===37###soif===1174###eoif===1229###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)");object = afb_msg_json_reply(object, error, info, NULL);

			/* Cant instrument this following code */
callreq->legacy_final(callreq->closure, -!!error, object, callreq->callback, callreq->export, callreq->xreq.caller);
		}
		else {
			/* Cant instrument this following code */
callreq->final(callreq->closure, object, error, info, callreq->callback, callreq->export, callreq->xreq.caller);
		}

				AKA_mark("lis===216###sois===6222###eois===6246###lif===42###soif===1483###eoif===1507###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_reply_cb(struct afb_xreq*,struct json_object*,const char*,const char*)");json_object_put(object);

	}

}

/** Instrumented function callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*) */
static int callreq_subscribe_cb(struct afb_xreq *xreq, struct afb_event_x2 *event)
{AKA_mark("Calling: ./app-framework-binder/src/afb-calls.c/callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)");AKA_fCall++;
		AKA_mark("lis===222###sois===6339###eois===6355###lif===2###soif===86###eoif===102###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)");int rc = 0, rc2;

		AKA_mark("lis===223###sois===6357###eois===6371###lif===3###soif===104###eoif===118###ins===true###function===./app-framework-binder/src/afb-calls.c/callre/* Cant instrument this following code */
q_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)");struct callreq
 /* Cant instrument this following code */
*callreq = CONTAINER_OF_XREQ(struct callreq, xreq);

		if (AKA_mark("lis===225###sois===6430###eois===6477###lif===5###soif===177###eoif===224###ifc===true###function===./app-framework-binder/src/afb-calls.c/callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)") && (AKA_mark("lis===225###sois===6430###eois===6477###lif===5###soif===177###eoif===224###isc===true###function===./app-framework-binder/src/afb-calls.c/callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)")&&callreq->flags & afb_req_x2_subcall_pass_events)) {
		AKA_mark("lis===226###sois===6481###eois===6534###lif===6###soif===228###eoif===281###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)");rc = afb_xreq_subscribe(callreq->xreq.caller, event);
	}
	else {AKA_mark("lis===-225-###sois===-6430-###eois===-643047-###lif===-5-###soif===-###eoif===-224-###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)");}

		if (AKA_mark("lis===227###sois===6540###eois===6588###lif===7###soif===287###eoif===335###ifc===true###function===./app-framework-binder/src/afb-calls.c/callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)") && (AKA_mark("lis===227###sois===6540###eois===6588###lif===7###soif===287###eoif===335###isc===true###function===./app-framework-binder/src/afb-calls.c/callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)")&&callreq->flags & afb_req_x2_subcall_catch_events)) {
		/* Cant instrument this following code */
rc2 = afb_export_subscribe(callreq->export, event);
				if (AKA_mark("lis===229###sois===6652###eois===6659###lif===9###soif===399###eoif===406###ifc===true###function===./app-framework-binder/src/afb-calls.c/callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)") && (AKA_mark("lis===229###sois===6652###eois===6659###lif===9###soif===399###eoif===406###isc===true###function===./app-framework-binder/src/afb-calls.c/callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)")&&rc2 < 0)) {
			AKA_mark("lis===230###sois===6664###eois===6673###lif===10###soif===411###eoif===420###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)");rc = rc2;
		}
		else {AKA_mark("lis===-229-###sois===-6652-###eois===-66527-###lif===-9-###soif===-###eoif===-406-###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)");}

	}
	else {AKA_mark("lis===-227-###sois===-6540-###eois===-654048-###lif===-7-###soif===-###eoif===-335-###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)");}

		AKA_mark("lis===232###sois===6678###eois===6688###lif===12###soif===425###eoif===435###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_subscribe_cb(struct afb_xreq*,struct afb_event_x2*)");return rc;

}

/** Instrumented function callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*) */
static int callreq_unsubscribe_cb(struct afb_xreq *xreq, struct afb_event_x2 *event)
{AKA_mark("Calling: ./app-framework-binder/src/afb-calls.c/callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)");AKA_fCall++;
		AKA_mark("lis===237###sois===6780###eois===6796###lif===2###soif===88###eoif===104###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)");int rc = 0, rc2;

		AKA_mark("lis===238###sois===6798###eois===6812###lif===3###soif===106###eoif===120###ins===true###function===./app-framework-binder/src/afb-calls.c/callre/* Cant instrument this following code */
q_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)");struct callreq
 /* Cant instrument this following code */
*callreq = CONTAINER_OF_XREQ(struct callreq, xreq);

		if (AKA_mark("lis===240###sois===6871###eois===6918###lif===5###soif===179###eoif===226###ifc===true###function===./app-framework-binder/src/afb-calls.c/callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)") && (AKA_mark("lis===240###sois===6871###eois===6918###lif===5###soif===179###eoif===226###isc===true###function===./app-framework-binder/src/afb-calls.c/callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)")&&callreq->flags & afb_req_x2_subcall_pass_events)) {
		AKA_mark("lis===241###sois===6922###eois===6977###lif===6###soif===230###eoif===285###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)");rc = afb_xreq_unsubscribe(callreq->xreq.caller, event);
	}
	else {AKA_mark("lis===-240-###sois===-6871-###eois===-687147-###lif===-5-###soif===-###eoif===-226-###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)");}

		if (AKA_mark("lis===242###sois===6983###eois===7031###lif===7###soif===291###eoif===339###ifc===true###function===./app-framework-binder/src/afb-calls.c/callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)") && (AKA_mark("lis===242###sois===6983###eois===7031###lif===7###soif===291###eoif===339###isc===true###function===./app-framework-binder/src/afb-calls.c/callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)")&&callreq->flags & afb_req_x2_subcall_catch_events)) {
		/* Cant instrument this following code */
rc2 = afb_export_unsubscribe(callreq->export, event);
				if (AKA_mark("lis===244###sois===7097###eois===7104###lif===9###soif===405###eoif===412###ifc===true###function===./app-framework-binder/src/afb-calls.c/callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)") && (AKA_mark("lis===244###sois===7097###eois===7104###lif===9###soif===405###eoif===412###isc===true###function===./app-framework-binder/src/afb-calls.c/callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)")&&rc2 < 0)) {
			AKA_mark("lis===245###sois===7109###eois===7118###lif===10###soif===417###eoif===426###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)");rc = rc2;
		}
		else {AKA_mark("lis===-244-###sois===-7097-###eois===-70977-###lif===-9-###soif===-###eoif===-412-###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)");}

	}
	else {AKA_mark("lis===-242-###sois===-6983-###eois===-698348-###lif===-7-###soif===-###eoif===-339-###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)");}

		AKA_mark("lis===247###sois===7123###eois===7133###lif===12###soif===431###eoif===441###ins===true###function===./app-framework-binder/src/afb-calls.c/callreq_unsubscribe_cb(struct afb_xreq*,struct afb_event_x2*)");return rc;

}

/******************************************************************************/

const struct afb_xreq_query_itf afb_calls_xreq_itf = {
	.unref = callreq_destroy_cb,
	.reply = callreq_reply_cb,
	.subscribe = callreq_subscribe_cb,
	.unsubscribe = callreq_unsubscribe_cb
};

/******************************************************************************/

static struct callreq *callreq_create(
		struct afb_export *export,
		struct afb_xreq *caller,
		const char *api,
		const char *verb,
		struct json_object *args,
		int flags,
		struct modes mode)
{
	struct callreq *callreq;
	size_t lenapi, lenverb;
	char *api2, *verb2;

	lenapi = 1 + strlen(api);
	lenverb = 1 + strlen(verb);
	callreq = malloc(lenapi + lenverb + sizeof *callreq);
	if (!callreq) {
		ERROR("out of memory");
		json_object_put(args);
		errno = ENOMEM;
	} else {
		afb_xreq_init(&callreq->xreq, &afb_calls_xreq_itf);
		api2 = (char*)&callreq[1];
		callreq->xreq.request.called_api = memcpy(api2, api, lenapi);;
		verb2 = &api2[lenapi];
		callreq->xreq.request.called_verb = memcpy(verb2, verb, lenverb);
		callreq->xreq.json = args;
		callreq->mode = mode;
		if (!caller)
			afb_export_context_init(export, &callreq->xreq.context);
		else {
			if (flags & afb_req_x2_subcall_api_session)
				afb_export_context_init(export, &callreq->xreq.context);
			else
				afb_context_subinit(&callreq->xreq.context, &caller->context);
			if (flags & afb_req_x2_subcall_on_behalf)
				afb_context_on_behalf_other_context(&callreq->xreq.context, &caller->context);
			callreq->xreq.caller = caller;
			afb_xreq_unhooked_addref(caller);
			export = afb_export_from_api_x3(caller->request.api);
		}
		callreq->export = export;
		callreq->flags = flags;
	}
	return callreq;
}

/******************************************************************************/

static int do_sync(
		struct afb_export *export,
		struct afb_xreq *caller,
		const char *api,
		const char *verb,
		struct json_object *args,
		int flags,
		struct json_object **object,
		char **error,
		char **info,
		struct modes mode)
{
	struct callreq *callreq;
	int rc;

	/* allocates the request */
	callreq = callreq_create(export, caller, api, verb, args, flags, mode);
	if (!callreq)
		goto interr;

	/* initializes the request */
	callreq->jobloop = NULL;
	callreq->returned = 0;
	callreq->status = 0;
	callreq->object = object;
	callreq->error = error;
	callreq->info = info;

	afb_xreq_unhooked_addref(&callreq->xreq); /* avoid early callreq destruction */

	rc = jobs_enter(NULL, 0, sync_enter, callreq);
	if (rc >= 0 && callreq->returned) {
		rc = callreq->status;
		afb_xreq_unhooked_unref(&callreq->xreq);
		return rc;
	}

	afb_xreq_unhooked_unref(&callreq->xreq);
interr:
	return store_reply(NULL, afb_error_text_internal_error, NULL, object, error, info);
}

/******************************************************************************/

static void do_async(
		struct afb_export *export,
		struct afb_xreq *caller,
		const char *api,
		const char *verb,
		struct json_object *args,
		int flags,
		void *callback,
		void *closure,
		void (*final)(void*, struct json_object*, const char*, const char*, union callback, struct afb_export*,struct afb_xreq*),
		struct modes mode)
{
	struct callreq *callreq;

	callreq = callreq_create(export, caller, api, verb, args, flags, mode);

	if (!callreq)
		final(closure, NULL, afb_error_text_internal_error, NULL, (union callback){ .any = callback }, export, caller);
	else {
		callreq->callback.any = callback;
		callreq->closure = closure;
		callreq->final = final;

		afb_export_process_xreq(callreq->export, &callreq->xreq);
	}
}

/******************************************************************************/

static void final_call(
	void *closure,
	struct json_object *object,
	const char *error,
	const char *info,
	union callback callback,
	struct afb_export *export,
	struct afb_xreq *caller)
{
	if (callback.call.x3)
		callback.call.x3(closure, object, error, info, afb_export_to_api_x3(export));
}

static void final_subcall(
	void *closure,
	struct json_object *object,
	const char *error,
	const char *info,
	union callback callback,
	struct afb_export *export,
	struct afb_xreq *caller)
{
	if (callback.subcall.x3)
		callback.subcall.x3(closure, object, error, info, xreq_to_req_x2(caller));
}

/******************************************************************************/

void afb_calls_call(
		struct afb_export *export,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, struct json_object*, const char *error, const char *info, struct afb_api_x3*),
		void *closure)
{
	do_async(export, NULL, api, verb, args, CALLFLAGS, callback, closure, final_call, mode_async);
}

int afb_calls_call_sync(
		struct afb_export *export,
		const char *api,
		const char *verb,
		struct json_object *args,
		struct json_object **object,
		char **error,
		char **info)
{
	return do_sync(export, NULL, api, verb, args, CALLFLAGS, object, error, info, mode_sync);
}

/** Instrumented function afb_calls_subcall(struct afb_xreq*,const char*,const char*,struct json_object*,int,void(*callback)(void*closure, struct json_object*object, const char*error, const char*info, struct afb_req_x2*req),void*) */
void afb_calls_subcall(
			struct afb_xreq *xreq,
			const char *api,
			const char *verb,
			struct json_object *args,
			int flags,
			void (*callback)(void *closure, struct json_object *object, const char *error, const char * info, struct afb_req_x2 *req),
			void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-calls.c/afb_calls_subcall(struct afb_xreq*,const char*,const char*,struct json_object*,int,void(*callback)(void*closure, struct json_object*object, const char*error, const char*info, struct afb_req_x2*req),void*)");AKA_fCall++;
	/* Cant instrument this following code */
do_async(NULL, xreq, api, verb, args, flags, callback, closure, final_subcall, /* Cant instrument this following code */
mode_async);
}

/** Instrumented function afb_calls_subcall_sync(struct afb_xreq*,const char*,const char*,struct json_object*,int,struct json_object**,char**,char**) */
int afb_calls_subcall_sync(
			struct afb_xreq *xreq,
			const char *api,
			const char *verb,
			struct json_object *args,
			int flags,
			struct json_object **object,
			char **error,
			char **info)
{AKA_mark("Calling: ./app-framework-binder/src/afb-calls.c/afb_calls_subcall_sync(struct afb_xreq*,const char*,const char*,struct json_object*,int,struct json_object**,char**,char**)");AKA_fCall++;
	/* Cant instrument this following code */
return do_sync(NULL, xreq, api, verb, args, flags, object, error, info, /* Cant instrument this following code */
mode_sync);
}

#if WITH_AFB_HOOK
void afb_calls_hooked_call(
		struct afb_export *export,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, struct json_object*, const char *error, const char *info, struct afb_api_x3*),
		void *closure)
{
	afb_hook_api_call(export, api, verb, args);
	do_async(export, NULL, api, verb, args, CALLFLAGS, callback, closure, final_call, mode_hooked_async);
}

int afb_calls_hooked_call_sync(
		struct afb_export *export,
		const char *api,
		const char *verb,
		struct json_object *args,
		struct json_object **object,
		char **error,
		char **info)
{
	afb_hook_api_callsync(export, api, verb, args);
	return do_sync(export, NULL, api, verb, args, CALLFLAGS, object, error, info, mode_hooked_sync);
}

void afb_calls_hooked_subcall(
			struct afb_xreq *xreq,
			const char *api,
			const char *verb,
			struct json_object *args,
			int flags,
			void (*callback)(void *closure, struct json_object *object, const char *error, const char * info, struct afb_req_x2 *req),
			void *closure)
{
	afb_hook_xreq_subcall(xreq, api, verb, args, flags);
	do_async(NULL, xreq, api, verb, args, flags, callback, closure, final_subcall, mode_hooked_async);
}

int afb_calls_hooked_subcall_sync(
			struct afb_xreq *xreq,
			const char *api,
			const char *verb,
			struct json_object *args,
			int flags,
			struct json_object **object,
			char **error,
			char **info)
{
	afb_hook_xreq_subcallsync(xreq, api, verb, args, flags);
	return do_sync(NULL, xreq, api, verb, args, flags, object, error, info, mode_hooked_sync);
}
#endif

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

static int do_legacy_sync(
		struct afb_export *export,
		struct afb_xreq *caller,
		const char *api,
		const char *verb,
		struct json_object *args,
		int flags,
		struct json_object **object,
		struct modes mode)
{
	struct callreq *callreq;
	int rc;

	/* allocates the request */
	callreq = callreq_create(export, caller, api, verb, args, flags, mode);
	if (!callreq)
		goto interr;

	/* initializes the request */
	callreq->jobloop = NULL;
	callreq->returned = 0;
	callreq->status = 0;
	callreq->object = object;

	afb_xreq_unhooked_addref(&callreq->xreq); /* avoid early callreq destruction */

	rc = jobs_enter(NULL, 0, sync_enter, callreq);
	if (rc >= 0 && callreq->returned) {
		rc = callreq->status;
		afb_xreq_unhooked_unref(&callreq->xreq);
		return rc;
	}

	afb_xreq_unhooked_unref(&callreq->xreq);
interr:
	if (object)
		*object = afb_msg_json_reply(NULL, afb_error_text_internal_error, NULL, NULL);
	return -1;
}

/******************************************************************************/

static void do_legacy_async(
		struct afb_export *export,
		struct afb_xreq *caller,
		const char *api,
		const char *verb,
		struct json_object *args,
		int flags,
		void *callback,
		void *closure,
		void (*final)(void*, int, struct json_object*, union callback, struct afb_export*,struct afb_xreq*),
		struct modes mode)
{
	struct callreq *callreq;
	struct json_object *ie;

	callreq = callreq_create(export, caller, api, verb, args, flags, mode);

	if (!callreq) {
		ie = afb_msg_json_reply(NULL, afb_error_text_internal_error, NULL, NULL);
		final(closure, -1, ie, (union callback){ .any = callback }, export, caller);
		json_object_put(ie);
	} else {
		callreq->callback.any = callback;
		callreq->closure = closure;
		callreq->legacy_final = final;

		afb_export_process_xreq(callreq->export, &callreq->xreq);
	}
}

/******************************************************************************/

static void final_legacy_call_v12(
	void *closure,
	int status,
	struct json_object *object,
	union callback callback,
	struct afb_export *export,
	struct afb_xreq *caller)
{
	if (callback.call.legacy_v12)
		callback.call.legacy_v12(closure, status, object);
}

static void final_legacy_call_v3(
	void *closure,
	int status,
	struct json_object *object,
	union callback callback,
	struct afb_export *export,
	struct afb_xreq *caller)
{
	if (callback.call.legacy_v3)
		callback.call.legacy_v3(closure, status, object, afb_export_to_api_x3(export));
}

/******************************************************************************/

void afb_calls_legacy_call_v12(
		struct afb_export *export,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, int, struct json_object*),
		void *closure)
{
	do_legacy_async(export, NULL, api, verb, args, CALLFLAGS, callback, closure, final_legacy_call_v12, mode_legacy_async);
}

void afb_calls_legacy_call_v3(
		struct afb_export *export,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, int, struct json_object*, struct afb_api_x3 *),
		void *closure)
{
	do_legacy_async(export, NULL, api, verb, args, CALLFLAGS, callback, closure, final_legacy_call_v3, mode_legacy_async);
}

int afb_calls_legacy_call_sync(
		struct afb_export *export,
		const char *api,
		const char *verb,
		struct json_object *args,
		struct json_object **result)
{
	return do_legacy_sync(export, NULL, api, verb, args, CALLFLAGS, result, mode_legacy_sync);
}

#if WITH_AFB_HOOK
void afb_calls_legacy_hooked_call_v12(
		struct afb_export *export,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, int, struct json_object*),
		void *closure)
{
	afb_hook_api_call(export, api, verb, args);
	do_legacy_async(export, NULL, api, verb, args, CALLFLAGS, callback, closure, final_legacy_call_v12, mode_hooked_legacy_async);
}

void afb_calls_legacy_hooked_call_v3(
		struct afb_export *export,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, int, struct json_object*, struct afb_api_x3 *),
		void *closure)
{
	afb_hook_api_call(export, api, verb, args);
	do_legacy_async(export, NULL, api, verb, args, CALLFLAGS, callback, closure, final_legacy_call_v3, mode_hooked_legacy_async);
}

int afb_calls_legacy_hooked_call_sync(
		struct afb_export *export,
		const char *api,
		const char *verb,
		struct json_object *args,
		struct json_object **result)
{
	int rc;
	struct json_object *object;

	afb_hook_api_callsync(export, api, verb, args);
	rc = do_legacy_sync(export, NULL, api, verb, args, CALLFLAGS, &object, mode_hooked_legacy_sync);
	if (result)
		*result = object;
	else
		json_object_put(object);
	return rc;
}
#endif

/******************************************************************************/

static void final_legacy_subcall_v1(
	void *closure,
	int status,
	struct json_object *object,
	union callback callback,
	struct afb_export *export,
	struct afb_xreq *caller)
{
	if (callback.subcall.legacy_v1)
		callback.subcall.legacy_v1(closure, status, object);
}

static void final_legacy_subcall_v2(
	void *closure,
	int status,
	struct json_object *object,
	union callback callback,
	struct afb_export *export,
	struct afb_xreq *caller)
{
	if (callback.subcall.legacy_v2)
		callback.subcall.legacy_v2(closure, status, object, xreq_to_req_x1(caller));
}

static void final_legacy_subcall_v3(
	void *closure,
	int status,
	struct json_object *object,
	union callback callback,
	struct afb_export *export,
	struct afb_xreq *caller)
{
	if (callback.subcall.legacy_v3)
		callback.subcall.legacy_v3(closure, status, object, xreq_to_req_x2(caller));
}

/******************************************************************************/

/** Instrumented function afb_calls_legacy_subcall_v1(struct afb_xreq*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*),void*) */
void afb_calls_legacy_subcall_v1(
		struct afb_xreq *caller,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, int, struct json_object*),
		void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-calls.c/afb_calls_legacy_subcall_v1(struct afb_xreq*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*),void*)");AKA_fCall++;
	/* Cant instrument this following code */
do_legacy_async(NULL, caller, api, verb, args, LEGACY_SUBCALLFLAGS, callback, closure, final_legacy_subcall_v1, /* Cant instrument this following code */
mode_legacy_async);
}

/** Instrumented function afb_calls_legacy_subcall_v2(struct afb_xreq*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x1),void*) */
void afb_calls_legacy_subcall_v2(
		struct afb_xreq *caller,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, int, struct json_object*, struct afb_req_x1),
		void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-calls.c/afb_calls_legacy_subcall_v2(struct afb_xreq*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x1),void*)");AKA_fCall++;
	/* Cant instrument this following code */
do_legacy_async(NULL, caller, api, verb, args, LEGACY_SUBCALLFLAGS, callback, closure, final_legacy_subcall_v2, /* Cant instrument this following code */
mode_legacy_async);
}

/** Instrumented function afb_calls_legacy_subcall_v3(struct afb_xreq*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x2*),void*) */
void afb_calls_legacy_subcall_v3(
		struct afb_xreq *caller,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, int, struct json_object*, struct afb_req_x2 *),
		void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-calls.c/afb_calls_legacy_subcall_v3(struct afb_xreq*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x2*),void*)");AKA_fCall++;
	/* Cant instrument this following code */
do_legacy_async(NULL, caller, api, verb, args, LEGACY_SUBCALLFLAGS, callback, closure, final_legacy_subcall_v3, /* Cant instrument this following code */
mode_legacy_async);
}

/** Instrumented function afb_calls_legacy_subcall_sync(struct afb_xreq*,const char*,const char*,struct json_object*,struct json_object**) */
int afb_calls_legacy_subcall_sync(
		struct afb_xreq *caller,
		const char *api,
		const char *verb,
		struct json_object *args,
		struct json_object **result)
{AKA_mark("Calling: ./app-framework-binder/src/afb-calls.c/afb_calls_legacy_subcall_sync(struct afb_xreq*,const char*,const char*,struct json_object*,struct json_object**)");AKA_fCall++;
	/* Cant instrument this following code */
return do_legacy_sync(NULL, caller, api, verb, args, LEGACY_SUBCALLFLAGS, result, /* Cant instrument this following code */
mode_legacy_sync);
}

#if WITH_AFB_HOOK
void afb_calls_legacy_hooked_subcall_v1(
		struct afb_xreq *caller,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, int, struct json_object*),
		void *closure)
{
	afb_hook_xreq_subcall(caller, api, verb, args, LEGACY_SUBCALLFLAGS);
	do_legacy_async(NULL, caller, api, verb, args, LEGACY_SUBCALLFLAGS, callback, closure, final_legacy_subcall_v1, mode_hooked_legacy_async);
}

void afb_calls_legacy_hooked_subcall_v2(
		struct afb_xreq *caller,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, int, struct json_object*, struct afb_req_x1),
		void *closure)
{
	afb_hook_xreq_subcall(caller, api, verb, args, LEGACY_SUBCALLFLAGS);
	do_legacy_async(NULL, caller, api, verb, args, LEGACY_SUBCALLFLAGS, callback, closure, final_legacy_subcall_v2, mode_hooked_legacy_async);
}

void afb_calls_legacy_hooked_subcall_v3(
		struct afb_xreq *caller,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, int, struct json_object*, struct afb_req_x2 *),
		void *closure)
{
	afb_hook_xreq_subcall(caller, api, verb, args, LEGACY_SUBCALLFLAGS);
	do_legacy_async(NULL, caller, api, verb, args, LEGACY_SUBCALLFLAGS, callback, closure, final_legacy_subcall_v3, mode_hooked_legacy_async);
}

int afb_calls_legacy_hooked_subcall_sync(
		struct afb_xreq *caller,
		const char *api,
		const char *verb,
		struct json_object *args,
		struct json_object **result)
{
	afb_hook_xreq_subcallsync(caller, api, verb, args, LEGACY_SUBCALLFLAGS);
	return do_legacy_sync(NULL, caller, api, verb, args, LEGACY_SUBCALLFLAGS, result, mode_hooked_legacy_sync);
}
#endif


#endif

