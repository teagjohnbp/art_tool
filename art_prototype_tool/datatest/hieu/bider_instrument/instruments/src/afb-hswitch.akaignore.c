/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_HSWITCH_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_HSWITCH_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author "Fulup Ar Foll"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>

#include <microhttpd.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CONTEXT_H_
#define AKA_INCLUDE__AFB_CONTEXT_H_
#include "afb-context.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HREQ_H_
#define AKA_INCLUDE__AFB_HREQ_H_
#include "afb-hreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_APISET_H_
#define AKA_INCLUDE__AFB_APISET_H_
#include "afb-apiset.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SESSION_H_
#define AKA_INCLUDE__AFB_SESSION_H_
#include "afb-session.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_WEBSOCK_H_
#define AKA_INCLUDE__AFB_WEBSOCK_H_
#include "afb-websock.akaignore.h"
#endif


/** Instrumented function afb_hswitch_apis(struct afb_hreq*,void*) */
int afb_hswitch_apis(struct afb_hreq *hreq, void *data)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");AKA_fCall++;
		AKA_mark("lis===34###sois===937###eois===964###lif===2###soif===59###eoif===86###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");const char *api, *verb, *i;

		AKA_mark("lis===35###sois===966###eois===989###lif===3###soif===88###eoif===111###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");size_t lenapi, lenverb;

		AKA_mark("lis===36###sois===991###eois===1024###lif===4###soif===113###eoif===146###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");struct afb_apiset *apiset = data;


	/* api is the first hierarchical item */
		AKA_mark("lis===39###sois===1069###eois===1084###lif===7###soif===191###eoif===206###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");i = hreq->tail;

		while (AKA_mark("lis===40###sois===1093###eois===1102###lif===8###soif===215###eoif===224###ifc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)") && (AKA_mark("lis===40###sois===1093###eois===1102###lif===8###soif===215###eoif===224###isc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)")&&*i == '/')) {
		AKA_mark("lis===41###sois===1106###eois===1110###lif===9###soif===228###eoif===232###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");i++;
	}

		if (AKA_mark("lis===42###sois===1116###eois===1119###lif===10###soif===238###eoif===241###ifc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)") && (AKA_mark("lis===42###sois===1116###eois===1119###lif===10###soif===238###eoif===241###isc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)")&&!*i)) {
		AKA_mark("lis===43###sois===1123###eois===1132###lif===11###soif===245###eoif===254###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");return 0;
	}
	else {AKA_mark("lis===-42-###sois===-1116-###eois===-11163-###lif===-10-###soif===-###eoif===-241-###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");}
 /* no API */
		AKA_mark("lis===44###sois===1147###eois===1155###lif===12###soif===269###eoif===277###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");api = i;


	/* search end of the api and get its length */
		while (AKA_mark("lis===47###sois===1213###eois===1230###lif===15###soif===335###eoif===352###ifc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)") && ((AKA_mark("lis===47###sois===1213###eois===1217###lif===15###soif===335###eoif===339###isc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)")&&*++i)	&&(AKA_mark("lis===47###sois===1221###eois===1230###lif===15###soif===343###eoif===352###isc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)")&&*i != '/'))) {
		;
	}

		AKA_mark("lis===48###sois===1234###eois===1261###lif===16###soif===356###eoif===383###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");lenapi = (size_t)(i - api);


	/* search the verb */
		while (AKA_mark("lis===51###sois===1294###eois===1303###lif===19###soif===416###eoif===425###ifc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)") && (AKA_mark("lis===51###sois===1294###eois===1303###lif===19###soif===416###eoif===425###isc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)")&&*i == '/')) {
		AKA_mark("lis===52###sois===1307###eois===1311###lif===20###soif===429###eoif===433###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");i++;
	}

		if (AKA_mark("lis===53###sois===1317###eois===1320###lif===21###soif===439###eoif===442###ifc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)") && (AKA_mark("lis===53###sois===1317###eois===1320###lif===21###soif===439###eoif===442###isc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)")&&!*i)) {
		AKA_mark("lis===54###sois===1324###eois===1333###lif===22###soif===446###eoif===455###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");return 0;
	}
	else {AKA_mark("lis===-53-###sois===-1317-###eois===-13173-###lif===-21-###soif===-###eoif===-442-###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");}
 /* no verb */
		AKA_mark("lis===55###sois===1349###eois===1358###lif===23###soif===471###eoif===480###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");verb = i;


	/* get the verb length */
		while (AKA_mark("lis===58###sois===1395###eois===1399###lif===26###soif===517###eoif===521###ifc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)") && (AKA_mark("lis===58###sois===1395###eois===1399###lif===26###soif===517###eoif===521###isc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)")&&*++i)) {
		;
	}

		AKA_mark("lis===59###sois===1403###eois===1432###lif===27###soif===525###eoif===554###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");lenverb = (size_t)(i - verb);


	/* found api + verb so process the call */
		AKA_mark("lis===62###sois===1479###eois===1535###lif===30###soif===601###eoif===657###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");afb_hreq_call(hreq, apiset, api, lenapi, verb, lenverb);

		AKA_mark("lis===63###sois===1537###eois===1546###lif===31###soif===659###eoif===668###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_apis(struct afb_hreq*,void*)");return 1;

}

/** Instrumented function afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*) */
int afb_hswitch_one_page_api_redirect(struct afb_hreq *hreq, void *data)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)");AKA_fCall++;
		AKA_mark("lis===68###sois===1626###eois===1638###lif===2###soif===76###eoif===88###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)");size_t plen;

		AKA_mark("lis===69###sois===1640###eois===1650###lif===3###soif===90###eoif===100###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)");char *url;


		if (AKA_mark("lis===71###sois===1657###eois===1699###lif===5###soif===107###eoif===149###ifc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)") && ((AKA_mark("lis===71###sois===1657###eois===1675###lif===5###soif===107###eoif===125###isc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)")&&hreq->lentail >= 2)	&&(AKA_mark("lis===71###sois===1679###eois===1699###lif===5###soif===129###eoif===149###isc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)")&&hreq->tail[1] == '#'))) {
		AKA_mark("lis===72###sois===1703###eois===1712###lif===6###soif===153###eoif===162###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)");return 0;
	}
	else {AKA_mark("lis===-71-###sois===-1657-###eois===-165742-###lif===-5-###soif===-###eoif===-149-###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)");}

	/*
	 * Here we have for example:
	 *    url  = "/pre/dir/page"   lenurl = 13
	 *    tail =     "/dir/page"   lentail = 9
	 *
	 * We will produce "/pre/#!dir/page"
	 *
	 * Let compute plen that include the / at end (for "/pre/")
	 */
		AKA_mark("lis===82###sois===1948###eois===1988###lif===16###soif===398###eoif===438###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)");plen = hreq->lenurl - hreq->lentail + 1;

		AKA_mark("lis===83###sois===1990###eois===2021###lif===17###soif===440###eoif===471###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)");url = alloca(hreq->lenurl + 3);

		AKA_mark("lis===84###sois===2023###eois===2052###lif===18###soif===473###eoif===502###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)");memcpy(url, hreq->url, plen);

		AKA_mark("lis===85###sois===2054###eois===2072###lif===19###soif===504###eoif===522###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)");url[plen++] = '#';

		AKA_mark("lis===86###sois===2074###eois===2092###lif===20###soif===524###eoif===542###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)");url[plen++] = '!';

		AKA_mark("lis===87###sois===2094###eois===2144###lif===21###soif===544###eoif===594###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)");memcpy(&url[plen], &hreq->tail[1], hreq->lentail);

		AKA_mark("lis===88###sois===2146###eois===2181###lif===22###soif===596###eoif===631###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)");afb_hreq_redirect_to(hreq, url, 1);

		AKA_mark("lis===89###sois===2183###eois===2192###lif===23###soif===633###eoif===642###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_one_page_api_redirect(struct afb_hreq*,void*)");return 1;

}

/** Instrumented function afb_hswitch_websocket_switch(struct afb_hreq*,void*) */
int afb_hswitch_websocket_switch(struct afb_hreq *hreq, void *data)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hswitch.c/afb_hswitch_websocket_switch(struct afb_hreq*,void*)");AKA_fCall++;
		AKA_mark("lis===94###sois===2267###eois===2300###lif===2###soif===71###eoif===104###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_websocket_switch(struct afb_hreq*,void*)");struct afb_apiset *apiset = data;


		if (AKA_mark("lis===96###sois===2307###eois===2325###lif===4###soif===111###eoif===129###ifc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_websocket_switch(struct afb_hreq*,void*)") && (AKA_mark("lis===96###sois===2307###eois===2325###lif===4###soif===111###eoif===129###isc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_websocket_switch(struct afb_hreq*,void*)")&&hreq->lentail != 0)) {
		AKA_mark("lis===97###sois===2329###eois===2338###lif===5###soif===133###eoif===142###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_websocket_switch(struct afb_hreq*,void*)");return 0;
	}
	else {AKA_mark("lis===-96-###sois===-2307-###eois===-230718-###lif===-4-###soif===-###eoif===-129-###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_websocket_switch(struct afb_hreq*,void*)");}


		if (AKA_mark("lis===99###sois===2345###eois===2376###lif===7###soif===149###eoif===180###ifc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_websocket_switch(struct afb_hreq*,void*)") && (AKA_mark("lis===99###sois===2345###eois===2376###lif===7###soif===149###eoif===180###isc===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_websocket_switch(struct afb_hreq*,void*)")&&afb_hreq_init_context(hreq) < 0)) {
				AKA_mark("lis===100###sois===2382###eois===2441###lif===8###soif===186###eoif===245###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_websocket_switch(struct afb_hreq*,void*)");afb_hreq_reply_error(hreq, MHD_HTTP_INTERNAL_SERVER_ERROR);

				AKA_mark("lis===101###sois===2444###eois===2453###lif===9###soif===248###eoif===257###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_websocket_switch(struct afb_hreq*,void*)");return 1;

	}
	else {AKA_mark("lis===-99-###sois===-2345-###eois===-234531-###lif===-7-###soif===-###eoif===-180-###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_websocket_switch(struct afb_hreq*,void*)");}


		AKA_mark("lis===104###sois===2459###eois===2506###lif===12###soif===263###eoif===310###ins===true###function===./app-framework-binder/src/afb-hswitch.c/afb_hswitch_websocket_switch(struct afb_hreq*,void*)");return afb_websock_check_upgrade(hreq, apiset);

}



#endif

