/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_SUBPATH_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_SUBPATH_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 Copyright (C) 2015-2020 "IoT.bzh"

 author: José Bollo <jose.bollo@iot.bzh>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#include <limits.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__SUBPATH_H_
#define AKA_INCLUDE__SUBPATH_H_
#include "subpath.akaignore.h"
#endif


/* a valid subpath is a relative path not looking deeper than root using .. */
/** Instrumented function subpath_is_valid(const char*) */
int subpath_is_valid(const char *path)
{AKA_mark("Calling: ./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");AKA_fCall++;
		AKA_mark("lis===39###sois===1024###eois===1041###lif===2###soif===42###eoif===59###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");int l = 0, i = 0;


	/* absolute path is not valid */
		if (AKA_mark("lis===42###sois===1082###eois===1096###lif===5###soif===100###eoif===114###ifc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)") && (AKA_mark("lis===42###sois===1082###eois===1096###lif===5###soif===100###eoif===114###isc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)")&&path[i] == '/')) {
		AKA_mark("lis===43###sois===1100###eois===1109###lif===6###soif===118###eoif===127###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");return 0;
	}
	else {AKA_mark("lis===-42-###sois===-1082-###eois===-108214-###lif===-5-###soif===-###eoif===-114-###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");}


	/* inspect the path */
		while (AKA_mark("lis===46###sois===1142###eois===1149###lif===9###soif===160###eoif===167###ifc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)") && (AKA_mark("lis===46###sois===1142###eois===1149###lif===9###soif===160###eoif===167###isc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)")&&path[i])) {
				AKA_mark("lis===47###sois===1162###eois===1171###lif===10###soif===180###eoif===189###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");switch(path[i++]){
					case '.': if(path[i++] == '.')AKA_mark("lis===48###sois===1177###eois===1186###lif===11###soif===195###eoif===204###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");

						if (AKA_mark("lis===49###sois===1194###eois===1202###lif===12###soif===212###eoif===220###ifc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)") && (AKA_mark("lis===49###sois===1194###eois===1202###lif===12###soif===212###eoif===220###isc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)")&&!path[i])) {
				AKA_mark("lis===50###sois===1208###eois===1214###lif===13###soif===226###eoif===232###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");break;
			}
			else {AKA_mark("lis===-49-###sois===-1194-###eois===-11948-###lif===-12-###soif===-###eoif===-220-###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");}

						if (AKA_mark("lis===51###sois===1222###eois===1236###lif===14###soif===240###eoif===254###ifc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)") && (AKA_mark("lis===51###sois===1222###eois===1236###lif===14###soif===240###eoif===254###isc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)")&&path[i] == '/')) {
								AKA_mark("lis===52###sois===1244###eois===1248###lif===15###soif===262###eoif===266###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");i++;

								AKA_mark("lis===53###sois===1253###eois===1259###lif===16###soif===271###eoif===277###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");break;

			}
			else {AKA_mark("lis===-51-###sois===-1222-###eois===-122214-###lif===-14-###soif===-###eoif===-254-###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");}

						if (AKA_mark("lis===55###sois===1272###eois===1288###lif===18###soif===290###eoif===306###ifc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)") && (AKA_mark("lis===55###sois===1272###eois===1288###lif===18###soif===290###eoif===306###isc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)")&&path[i++] == '.')) {
								if (AKA_mark("lis===56###sois===1300###eois===1308###lif===19###soif===318###eoif===326###ifc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)") && (AKA_mark("lis===56###sois===1300###eois===1308###lif===19###soif===318###eoif===326###isc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)")&&!path[i])) {
										AKA_mark("lis===57###sois===1317###eois===1321###lif===20###soif===335###eoif===339###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");l--;

										AKA_mark("lis===58###sois===1327###eois===1333###lif===21###soif===345###eoif===351###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");break;

				}
				else {AKA_mark("lis===-56-###sois===-1300-###eois===-13008-###lif===-19-###soif===-###eoif===-326-###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");}

								if (AKA_mark("lis===60###sois===1348###eois===1364###lif===23###soif===366###eoif===382###ifc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)") && (AKA_mark("lis===60###sois===1348###eois===1364###lif===23###soif===366###eoif===382###isc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)")&&path[i++] == '/')) {
										AKA_mark("lis===61###sois===1373###eois===1377###lif===24###soif===391###eoif===395###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");l--;

										AKA_mark("lis===62###sois===1383###eois===1389###lif===25###soif===401###eoif===407###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");break;

				}
				else {AKA_mark("lis===-60-###sois===-1348-###eois===-134816-###lif===-23-###soif===-###eoif===-382-###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");}

			}
			else {AKA_mark("lis===-55-###sois===-1272-###eois===-127216-###lif===-18-###soif===-###eoif===-306-###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");}

					default: if(path[i++] != '.' && path[i++] != '/')AKA_mark("lis===65###sois===1403###eois===1411###lif===28###soif===421###eoif===429###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");

						while (AKA_mark("lis===66###sois===1421###eois===1446###lif===29###soif===439###eoif===464###ifc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)") && ((AKA_mark("lis===66###sois===1421###eois===1428###lif===29###soif===439###eoif===446###isc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)")&&path[i])	&&(AKA_mark("lis===66###sois===1432###eois===1446###lif===29###soif===450###eoif===464###isc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)")&&path[i] != '/'))) {
				AKA_mark("lis===67###sois===1452###eois===1456###lif===30###soif===470###eoif===474###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");i++;
			}

						if (AKA_mark("lis===68###sois===1464###eois===1470###lif===31###soif===482###eoif===488###ifc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)") && (AKA_mark("lis===68###sois===1464###eois===1470###lif===31###soif===482###eoif===488###isc===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)")&&l >= 0)) {
				AKA_mark("lis===69###sois===1476###eois===1480###lif===32###soif===494###eoif===498###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");l++;
			}
			else {AKA_mark("lis===-68-###sois===-1464-###eois===-14646-###lif===-31-###soif===-###eoif===-488-###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");}

					case '/': if(path[i++] == '/')AKA_mark("lis===70###sois===1483###eois===1492###lif===33###soif===501###eoif===510###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");

						AKA_mark("lis===71###sois===1496###eois===1502###lif===34###soif===514###eoif===520###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");break;

		}

	}

		AKA_mark("lis===74###sois===1511###eois===1525###lif===37###soif===529###eoif===543###ins===true###function===./app-framework-binder/src/subpath.c/subpath_is_valid(const char*)");return l >= 0;

}

/*
 * Return the path or NULL is not valid.
 * Ensure that the path doesn't start with '/' and that
 * it does not contains sequence of '..' going deeper than
 * root.
 * Returns the path or NULL in case of
 * invalid path.
 */
/** Instrumented function subpath(const char*) */
const char *subpath(const char *path)
{AKA_mark("Calling: ./app-framework-binder/src/subpath.c/subpath(const char*)");AKA_fCall++;
		AKA_mark("lis===87###sois===1798###eois===1868###lif===2###soif===41###eoif===111###ins===true###function===./app-framework-binder/src/subpath.c/subpath(const char*)");return path && subpath_is_valid(path) ? (path[0] ? path : ".") : NULL;

}

/*
 * Normalizes and checks the 'path'.
 * Removes any starting '/' and checks that 'path'
 * does not contains sequence of '..' going deeper than
 * root.
 * Returns the normalized path or NULL in case of
 * invalid path.
 */
/** Instrumented function subpath_force(const char*) */
const char *subpath_force(const char *path)
{AKA_mark("Calling: ./app-framework-binder/src/subpath.c/subpath_force(const char*)");AKA_fCall++;
		while (AKA_mark("lis===100###sois===2152###eois===2172###lif===2###soif===53###eoif===73###ifc===true###function===./app-framework-binder/src/subpath.c/subpath_force(const char*)") && ((AKA_mark("lis===100###sois===2152###eois===2156###lif===2###soif===53###eoif===57###isc===true###function===./app-framework-binder/src/subpath.c/subpath_force(const char*)")&&path)	&&(AKA_mark("lis===100###sois===2160###eois===2172###lif===2###soif===61###eoif===73###isc===true###function===./app-framework-binder/src/subpath.c/subpath_force(const char*)")&&*path == '/'))) {
		AKA_mark("lis===101###sois===2176###eois===2183###lif===3###soif===77###eoif===84###ins===true###function===./app-framework-binder/src/subpath.c/subpath_force(const char*)");path++;
	}

		AKA_mark("lis===102###sois===2185###eois===2206###lif===4###soif===86###eoif===107###ins===true###function===./app-framework-binder/src/subpath.c/subpath_force(const char*)");return subpath(path);

}

#if defined(TEST_subpath)
#include <stdio.h>
void t(const char *subpath, int validity) {
  printf("%s -> %d = %d, %s\n", subpath, validity, subpath_is_valid(subpath), subpath_is_valid(subpath)==validity ? "ok" : "NOT OK");
}
int main() {
  t("/",0);
  t("..",0);
  t(".",1);
  t("../a",0);
  t("a/..",1);
  t("a/../////..",0);
  t("a/../b/..",1);
  t("a/b/c/..",1);
  t("a/b/c/../..",1);
  t("a/b/c/../../..",1);
  t("a/b/c/../../../.",1);
  t("./..a/././..b/..c/./.././.././../.",1);
  t("./..a/././..b/..c/./.././.././.././..",0);
  t("./..a//.//./..b/..c/./.././/./././///.././.././a/a/a/a/a",1);
  return 0;
}
#endif


#endif

