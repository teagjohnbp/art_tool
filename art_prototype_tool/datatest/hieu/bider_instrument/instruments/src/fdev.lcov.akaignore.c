/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_FDEV_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_FDEV_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#define FDEV_PROVIDER
/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_H_
#define AKA_INCLUDE__FDEV_H_
#include "fdev.lcov.akaignore.h"
#endif


struct fdev
{
	int fd;
	uint32_t events;
	unsigned refcount;
	struct fdev_itf *itf;
	void *closure_itf;
	void (*callback)(void*,uint32_t,struct fdev*);
	void *closure_callback;
};

struct fdev *fdev_create(int fd)
{AKA_fCall++;
	struct fdev *fdev;

	fdev = calloc(1, sizeof *fdev);
	if (!fdev)
		errno = ENOMEM;
	else {
		fdev->fd = fd;
		fdev->refcount = 3; /* set autoclose by default */
	}
	return fdev;
}

void fdev_set_itf(struct fdev *fdev, struct fdev_itf *itf, void *closure_itf)
{AKA_fCall++;
	fdev->itf = itf;
	fdev->closure_itf = closure_itf;
}

void fdev_dispatch(struct fdev *fdev, uint32_t events)
{AKA_fCall++;
	if (fdev->callback)
		fdev->callback(fdev->closure_callback, events, fdev);
}

struct fdev *fdev_addref(struct fdev *fdev)
{AKA_fCall++;
	if (fdev)
		__atomic_add_fetch(&fdev->refcount, 2, __ATOMIC_RELAXED);
	return fdev;
}

void fdev_unref(struct fdev *fdev)
{AKA_fCall++;
	if (fdev && __atomic_sub_fetch(&fdev->refcount, 2, __ATOMIC_RELAXED) <= 1) {
		if (fdev->itf) {
			fdev->itf->disable(fdev->closure_itf, fdev);
			if (fdev->itf->unref)
				fdev->itf->unref(fdev->closure_itf);
		}
		if (fdev->refcount)
			close(fdev->fd);
		free(fdev);
	}
}

int fdev_fd(const struct fdev *fdev)
{AKA_fCall++;
	return fdev->fd;
}

uint32_t fdev_events(const struct fdev *fdev)
{AKA_fCall++;
	return fdev->events;
}

int fdev_autoclose(const struct fdev *fdev)
{AKA_fCall++;
	return 1 & fdev->refcount;
}

static inline int is_active(struct fdev *fdev)
{AKA_fCall++;
	return !!fdev->callback;
}

static inline void update_activity(struct fdev *fdev, int old_active)
{AKA_fCall++;
	if (is_active(fdev)) {
		if (!old_active)
			fdev->itf->enable(fdev->closure_itf, fdev);
	} else {
		if (old_active)
			fdev->itf->disable(fdev->closure_itf, fdev);
	}
}

void fdev_set_callback(struct fdev *fdev, void (*callback)(void*,uint32_t,struct fdev*), void *closure)
{AKA_fCall++;
	int oa;

	oa = is_active(fdev);
	fdev->callback = callback;
	fdev->closure_callback = closure;
	update_activity(fdev, oa);
}

void fdev_set_events(struct fdev *fdev, uint32_t events)
{AKA_fCall++;
	if (events != fdev->events) {
		fdev->events = events;
		if (is_active(fdev))
			fdev->itf->update(fdev->closure_itf, fdev);
	}
}

void fdev_set_autoclose(struct fdev *fdev, int autoclose)
{AKA_fCall++;
	if (autoclose)
		fdev->refcount |= (unsigned)1;
	else
		fdev->refcount &= ~(unsigned)1;
}


#endif

