/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_COMMON_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_COMMON_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_COMMON_H_
#define AKA_INCLUDE__AFB_COMMON_H_
#include "afb-common.lcov.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__LOCALE_ROOT_H_
#define AKA_INCLUDE__LOCALE_ROOT_H_
#include "locale-root.lcov.akaignore.h"
#endif


static const char *default_locale = NULL;
static struct locale_root *rootdir = NULL;

void afb_common_default_locale_set(const char *locale)
{AKA_fCall++;
	default_locale = locale;
}

const char *afb_common_default_locale_get()
{AKA_fCall++;
	return default_locale;
}

int afb_common_rootdir_set(const char *dirname)
{AKA_fCall++;
	int dirfd, rc;
	struct locale_root *root;
	struct locale_search *search;

	rc = -1;
	dirfd = openat(AT_FDCWD, dirname, O_PATH|O_DIRECTORY);
	if (dirfd < 0) {
		/* TODO message */
	} else {
		root = locale_root_create(dirfd);
		if (root == NULL) {
			/* TODO message */
			close(dirfd);
		} else {
			rc = 0;
			if (default_locale != NULL) {
				search = locale_root_search(root, default_locale, 0);
				if (search == NULL) {
					/* TODO message */
				} else {
					locale_root_set_default_search(root, search);
					locale_search_unref(search);
				}
			}
			locale_root_unref(rootdir);
			rootdir = root;
		}
	}
	return rc;
}

int afb_common_rootdir_get_fd()
{AKA_fCall++;
	return locale_root_get_dirfd(rootdir);
}

int afb_common_rootdir_open_locale(const char *filename, int flags, const char *locale)
{AKA_fCall++;
	return locale_root_open(rootdir, filename, flags, locale);
}



#endif

