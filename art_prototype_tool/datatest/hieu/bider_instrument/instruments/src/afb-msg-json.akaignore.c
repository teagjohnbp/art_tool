/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_MSG_JSON_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_MSG_JSON_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <json-c/json.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_MSG_JSON_H_
#define AKA_INCLUDE__AFB_MSG_JSON_H_
#include "afb-msg-json.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CONTEXT_H_
#define AKA_INCLUDE__AFB_CONTEXT_H_
#include "afb-context.akaignore.h"
#endif


static const char _success_[] = "success";

/** Instrumented function afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*) */
struct json_object *afb_msg_json_reply(struct json_object *resp, const char *error, const char *info, struct afb_context *context)
{AKA_mark("Calling: ./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)");AKA_fCall++;
		AKA_mark("lis===29###sois===921###eois===948###lif===2###soif===134###eoif===161###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)");json_object *msg, *request;

		AKA_mark("lis===30###sois===950###eois===981###lif===3###soif===163###eoif===194###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)");json_object *type_reply = NULL;


		AKA_mark("lis===32###sois===984###eois===1015###lif===5###soif===197###eoif===228###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)");msg = json_object_new_object();

		if (AKA_mark("lis===33###sois===1021###eois===1033###lif===6###soif===234###eoif===246###ifc===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)") && (AKA_mark("lis===33###sois===1021###eois===1033###lif===6###soif===234###eoif===246###isc===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)")&&resp != NULL)) {
		AKA_mark("lis===34###sois===1037###eois===1083###lif===7###soif===250###eoif===296###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)");json_object_object_add(msg, "response", resp);
	}
	else {AKA_mark("lis===-33-###sois===-1021-###eois===-102112-###lif===-6-###soif===-###eoif===-246-###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)");}


		AKA_mark("lis===36###sois===1086###eois===1135###lif===9###soif===299###eoif===348###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)");type_reply = json_object_new_string("afb-reply");

		AKA_mark("lis===37###sois===1137###eois===1186###lif===10###soif===350###eoif===399###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)");json_object_object_add(msg, "jtype", type_reply);


		AKA_mark("lis===39###sois===1189###eois===1224###lif===12###soif===402###eoif===437###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)");request = json_object_new_object();

		AKA_mark("lis===40###sois===1226###eois===1274###lif===13###soif===439###eoif===487###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)");json_object_object_add(msg, "request", request);

		AKA_mark("lis===41###sois===1276###eois===1362###lif===14###soif===489###eoif===575###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)");json_object_object_add(request, "status", json_object_new_string(error ?: _success_));


		if (AKA_mark("lis===43###sois===1369###eois===1381###lif===16###soif===582###eoif===594###ifc===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)") && (AKA_mark("lis===43###sois===1369###eois===1381###lif===16###soif===582###eoif===594###isc===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)")&&info != NULL)) {
		AKA_mark("lis===44###sois===1385###eois===1455###lif===17###soif===598###eoif===668###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)");json_object_object_add(request, "info", json_object_new_string(info));
	}
	else {AKA_mark("lis===-43-###sois===-1369-###eois===-136912-###lif===-16-###soif===-###eoif===-594-###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)");}


		AKA_mark("lis===46###sois===1458###eois===1469###lif===19###soif===671###eoif===682###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_reply(struct json_object*,const char*,const char*,struct afb_context*)");return msg;

}

/** Instrumented function afb_msg_json_event(const char*,struct json_object*) */
struct json_object *afb_msg_json_event(const char *event, struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-msg-json.c/afb_msg_json_event(const char*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===51###sois===1562###eois===1579###lif===2###soif===89###eoif===106###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_event(const char*,struct json_object*)");json_object *msg;

		AKA_mark("lis===52###sois===1581###eois===1612###lif===3###soif===108###eoif===139###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_event(const char*,struct json_object*)");json_object *type_event = NULL;


		AKA_mark("lis===54###sois===1615###eois===1646###lif===5###soif===142###eoif===173###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_event(const char*,struct json_object*)");msg = json_object_new_object();


		AKA_mark("lis===56###sois===1649###eois===1717###lif===7###soif===176###eoif===244###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_event(const char*,struct json_object*)");json_object_object_add(msg, "event", json_object_new_string(event));


		if (AKA_mark("lis===58###sois===1724###eois===1738###lif===9###soif===251###eoif===265###ifc===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_event(const char*,struct json_object*)") && (AKA_mark("lis===58###sois===1724###eois===1738###lif===9###soif===251###eoif===265###isc===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_event(const char*,struct json_object*)")&&object != NULL)) {
		AKA_mark("lis===59###sois===1742###eois===1786###lif===10###soif===269###eoif===313###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_event(const char*,struct json_object*)");json_object_object_add(msg, "data", object);
	}
	else {AKA_mark("lis===-58-###sois===-1724-###eois===-172414-###lif===-9-###soif===-###eoif===-265-###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_event(const char*,struct json_object*)");}


		AKA_mark("lis===61###sois===1789###eois===1838###lif===12###soif===316###eoif===365###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_event(const char*,struct json_object*)");type_event = json_object_new_string("afb-event");

		AKA_mark("lis===62###sois===1840###eois===1889###lif===13###soif===367###eoif===416###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_event(const char*,struct json_object*)");json_object_object_add(msg, "jtype", type_event);


		AKA_mark("lis===64###sois===1892###eois===1903###lif===15###soif===419###eoif===430###ins===true###function===./app-framework-binder/src/afb-msg-json.c/afb_msg_json_event(const char*,struct json_object*)");return msg;

}



#endif

