/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_WS_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_WS_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <assert.h>
#include <errno.h>
#include <sys/uio.h>
#include <string.h>
#include <stdarg.h>
#include <poll.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__WEBSOCK_H_
#define AKA_INCLUDE__WEBSOCK_H_
#include "websock.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_WS_H_
#define AKA_INCLUDE__AFB_WS_H_
#include "afb-ws.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_H_
#define AKA_INCLUDE__FDEV_H_
#include "fdev.akaignore.h"
#endif


/*
 * declaration of the websock interface for afb-ws
 */
static ssize_t aws_writev(struct afb_ws *ws, const struct iovec *iov, int iovcnt);
static ssize_t aws_readv(struct afb_ws *ws, const struct iovec *iov, int iovcnt);
static void aws_on_close(struct afb_ws *ws, uint16_t code, size_t size);
static void aws_on_text(struct afb_ws *ws, int last, size_t size);
static void aws_on_binary(struct afb_ws *ws, int last, size_t size);
static void aws_on_continue(struct afb_ws *ws, int last, size_t size);
static void aws_on_readable(struct afb_ws *ws);
static void aws_on_error(struct afb_ws *ws, uint16_t code, const void *data, size_t size);

static struct websock_itf aws_itf = {
	.writev = (void*)aws_writev,
	.readv = (void*)aws_readv,

	.on_ping = NULL,
	.on_pong = NULL,
	.on_close = (void*)aws_on_close,
	.on_text = (void*)aws_on_text,
	.on_binary = (void*)aws_on_binary,
	.on_continue = (void*)aws_on_continue,
	.on_extension = NULL,

	.on_error = (void*)aws_on_error
};

/*
 * a common scheme of buffer handling
 */
struct buf
{
	char *buffer;
	size_t size;
};

/*
 * the state
 */
enum state
{
	waiting,
	reading_text,
	reading_binary
};

/*
 * the afb_ws structure
 */
struct afb_ws
{
	int fd;			/* the socket file descriptor */
	enum state state;	/* current state */
	const struct afb_ws_itf *itf; /* the callback interface */
	void *closure;		/* closure when calling the callbacks */
	struct websock *ws;	/* the websock handler */
	struct fdev *fdev;	/* the fdev for the socket */
	struct buf buffer;	/* the last read fragment */
};

/*
 * Returns the current buffer of 'ws' that is reset.
 */
/** Instrumented function aws_pick_buffer(struct afb_ws*) */
static inline struct buf aws_pick_buffer(struct afb_ws *ws)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/aws_pick_buffer(struct afb_ws*)");AKA_fCall++;
		AKA_mark("lis===98###sois===2572###eois===2603###lif===2###soif===63###eoif===94###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_pick_buffer(struct afb_ws*)");struct buf result = ws->buffer;

		if (AKA_mark("lis===99###sois===2609###eois===2622###lif===3###soif===100###eoif===113###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_pick_buffer(struct afb_ws*)") && (AKA_mark("lis===99###sois===2609###eois===2622###lif===3###soif===100###eoif===113###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_pick_buffer(struct afb_ws*)")&&result.buffer)) {
		AKA_mark("lis===100###sois===2626###eois===2657###lif===4###soif===117###eoif===148###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_pick_buffer(struct afb_ws*)");result.buffer[result.size] = 0;
	}
	else {AKA_mark("lis===-99-###sois===-2609-###eois===-260913-###lif===-3-###soif===-###eoif===-113-###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_pick_buffer(struct afb_ws*)");}

		AKA_mark("lis===101###sois===2659###eois===2684###lif===5###soif===150###eoif===175###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_pick_buffer(struct afb_ws*)");ws->buffer.buffer = NULL;

		AKA_mark("lis===102###sois===2686###eois===2706###lif===6###soif===177###eoif===197###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_pick_buffer(struct afb_ws*)");ws->buffer.size = 0;

		AKA_mark("lis===103###sois===2708###eois===2722###lif===7###soif===199###eoif===213###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_pick_buffer(struct afb_ws*)");return result;

}

/*
 * Clear the current buffer
 */
/** Instrumented function aws_clear_buffer(struct afb_ws*) */
static inline void aws_clear_buffer(struct afb_ws *ws)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/aws_clear_buffer(struct afb_ws*)");AKA_fCall++;
		AKA_mark("lis===111###sois===2819###eois===2839###lif===2###soif===58###eoif===78###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_clear_buffer(struct afb_ws*)");ws->buffer.size = 0;

}

/*
 * Disconnect the websocket 'ws' and calls on_hangup if
 * 'call_on_hangup' is not null.
 */
/** Instrumented function aws_disconnect(struct afb_ws*,int) */
static void aws_disconnect(struct afb_ws *ws, int call_on_hangup)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)");AKA_fCall++;
		AKA_mark("lis===120###sois===3008###eois===3037###lif===2###soif===69###eoif===98###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)");struct websock *wsi = ws->ws;

		if (AKA_mark("lis===121###sois===3043###eois===3054###lif===3###soif===104###eoif===115###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)") && (AKA_mark("lis===121###sois===3043###eois===3054###lif===3###soif===104###eoif===115###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)")&&wsi != NULL)) {
				AKA_mark("lis===122###sois===3060###eois===3074###lif===4###soif===121###eoif===135###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)");ws->ws = NULL;

				AKA_mark("lis===123###sois===3077###eois===3114###lif===5###soif===138###eoif===175###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)");fdev_set_callback(ws->fdev, NULL, 0);

				AKA_mark("lis===124###sois===3117###eois===3138###lif===6###soif===178###eoif===199###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)");fdev_unref(ws->fdev);

				AKA_mark("lis===125###sois===3141###eois===3162###lif===7###soif===202###eoif===223###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)");websock_destroy(wsi);

				AKA_mark("lis===126###sois===3165###eois===3189###lif===8###soif===226###eoif===250###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)");free(ws->buffer.buffer);

				AKA_mark("lis===127###sois===3192###eois===3212###lif===9###soif===253###eoif===273###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)");ws->state = waiting;

				if (AKA_mark("lis===128###sois===3219###eois===3255###lif===10###soif===280###eoif===316###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)") && ((AKA_mark("lis===128###sois===3219###eois===3233###lif===10###soif===280###eoif===294###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)")&&call_on_hangup)	&&(AKA_mark("lis===128###sois===3237###eois===3255###lif===10###soif===298###eoif===316###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)")&&ws->itf->on_hangup))) {
			AKA_mark("lis===129###sois===3260###eois===3292###lif===11###soif===321###eoif===353###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)");ws->itf->on_hangup(ws->closure);
		}
		else {AKA_mark("lis===-128-###sois===-3219-###eois===-321936-###lif===-10-###soif===-###eoif===-316-###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)");}

	}
	else {AKA_mark("lis===-121-###sois===-3043-###eois===-304311-###lif===-3-###soif===-###eoif===-115-###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_disconnect(struct afb_ws*,int)");}

}

/** Instrumented function fdevcb(void*,uint32_t,struct fdev*) */
static void fdevcb(void *ws, uint32_t revents, struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/fdevcb(void*,uint32_t,struct fdev*)");AKA_fCall++;
		if (AKA_mark("lis===135###sois===3372###eois===3397###lif===2###soif===73###eoif===98###ifc===true###function===./app-framework-binder/src/afb-ws.c/fdevcb(void*,uint32_t,struct fdev*)") && (AKA_mark("lis===135###sois===3372###eois===3397###lif===2###soif===73###eoif===98###isc===true###function===./app-framework-binder/src/afb-ws.c/fdevcb(void*,uint32_t,struct fdev*)")&&(revents & EPOLLHUP) != 0)) {
		AKA_mark("lis===136###sois===3401###eois===3419###lif===3###soif===102###eoif===120###ins===true###function===./app-framework-binder/src/afb-ws.c/fdevcb(void*,uint32_t,struct fdev*)");afb_ws_hangup(ws);
	}
	else {
		if (AKA_mark("lis===137###sois===3430###eois===3454###lif===4###soif===131###eoif===155###ifc===true###function===./app-framework-binder/src/afb-ws.c/fdevcb(void*,uint32_t,struct fdev*)") && (AKA_mark("lis===137###sois===3430###eois===3454###lif===4###soif===131###eoif===155###isc===true###function===./app-framework-binder/src/afb-ws.c/fdevcb(void*,uint32_t,struct fdev*)")&&(revents & EPOLLIN) != 0)) {
			AKA_mark("lis===138###sois===3458###eois===3478###lif===5###soif===159###eoif===179###ins===true###function===./app-framework-binder/src/afb-ws.c/fdevcb(void*,uint32_t,struct fdev*)");aws_on_readable(ws);
		}
		else {AKA_mark("lis===-137-###sois===-3430-###eois===-343024-###lif===-4-###soif===-###eoif===-155-###ins===true###function===./app-framework-binder/src/afb-ws.c/fdevcb(void*,uint32_t,struct fdev*)");}
	}

}

/*
 * Creates the afb_ws structure for the file descritor
 * 'fd' and the callbacks described by the interface 'itf'
 * and its 'closure'.
 * When the creation is a success, the systemd event loop 'eloop' is
 * used for handling event for 'fd'.
 *
 * Returns the handle for the afb_ws created or NULL on error.
 */
/** Instrumented function afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*) */
struct afb_ws *afb_ws_create(struct fdev *fdev, const struct afb_ws_itf *itf, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");AKA_fCall++;
		AKA_mark("lis===152###sois===3893###eois===3915###lif===2###soif===96###eoif===118###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");struct afb_ws *result;


		AKA_mark("lis===154###sois===3918###eois===3931###lif===4###soif===121###eoif===134###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");assert(fdev);


	/* allocation */
		AKA_mark("lis===157###sois===3952###eois===3985###lif===7###soif===155###eoif===188###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");result = malloc(sizeof * result);

		if (AKA_mark("lis===158###sois===3991###eois===4005###lif===8###soif===194###eoif===208###ifc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)") && (AKA_mark("lis===158###sois===3991###eois===4005###lif===8###soif===194###eoif===208###isc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)")&&result == NULL)) {
		AKA_mark("lis===159###sois===4009###eois===4020###lif===9###soif===212###eoif===223###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");goto error;
	}
	else {AKA_mark("lis===-158-###sois===-3991-###eois===-399114-###lif===-8-###soif===-###eoif===-208-###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");}


	/* init */
		AKA_mark("lis===162###sois===4035###eois===4055###lif===12###soif===238###eoif===258###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");result->fdev = fdev;

		AKA_mark("lis===163###sois===4057###eois===4084###lif===13###soif===260###eoif===287###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");result->fd = fdev_fd(fdev);

		AKA_mark("lis===164###sois===4086###eois===4110###lif===14###soif===289###eoif===313###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");result->state = waiting;

		AKA_mark("lis===165###sois===4112###eois===4130###lif===15###soif===315###eoif===333###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");result->itf = itf;

		AKA_mark("lis===166###sois===4132###eois===4158###lif===16###soif===335###eoif===361###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");result->closure = closure;

		AKA_mark("lis===167###sois===4160###eois===4189###lif===17###soif===363###eoif===392###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");result->buffer.buffer = NULL;

		AKA_mark("lis===168###sois===4191###eois===4215###lif===18###soif===394###eoif===418###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");result->buffer.size = 0;


	/* creates the websocket */
		AKA_mark("lis===171###sois===4247###eois===4297###lif===21###soif===450###eoif===500###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");result->ws = websock_create_v13(&aws_itf, result);

		if (AKA_mark("lis===172###sois===4303###eois===4321###lif===22###soif===506###eoif===524###ifc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)") && (AKA_mark("lis===172###sois===4303###eois===4321###lif===22###soif===506###eoif===524###isc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)")&&result->ws == NULL)) {
		AKA_mark("lis===173###sois===4325###eois===4337###lif===23###soif===528###eoif===540###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");goto error2;
	}
	else {AKA_mark("lis===-172-###sois===-4303-###eois===-430318-###lif===-22-###soif===-###eoif===-524-###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");}


	/* finalize */
		AKA_mark("lis===176###sois===4356###eois===4387###lif===26###soif===559###eoif===590###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");fdev_set_events(fdev, EPOLLIN);

		AKA_mark("lis===177###sois===4389###eois===4429###lif===27###soif===592###eoif===632###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");fdev_set_callback(fdev, fdevcb, result);

		AKA_mark("lis===178###sois===4431###eois===4445###lif===28###soif===634###eoif===648###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");return result;


	error2:
	AKA_mark("lis===181###sois===4456###eois===4469###lif===31###soif===659###eoif===672###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");free(result);

	error:
	AKA_mark("lis===183###sois===4478###eois===4495###lif===33###soif===681###eoif===698###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");fdev_unref(fdev);

		AKA_mark("lis===184###sois===4497###eois===4509###lif===34###soif===700###eoif===712###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_create(struct fdev*,const struct afb_ws_itf*,void*)");return NULL;

}

/*
 * Destroys the websocket 'ws'
 * It first hangup (but without calling on_hangup for safety reasons)
 * if needed.
 */
/** Instrumented function afb_ws_destroy(struct afb_ws*) */
void afb_ws_destroy(struct afb_ws *ws)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/afb_ws_destroy(struct afb_ws*)");AKA_fCall++;
		AKA_mark("lis===194###sois===4677###eois===4699###lif===2###soif===42###eoif===64###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_destroy(struct afb_ws*)");aws_disconnect(ws, 0);

		AKA_mark("lis===195###sois===4701###eois===4710###lif===3###soif===66###eoif===75###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_destroy(struct afb_ws*)");free(ws);

}

/*
 * Hangup the websocket 'ws'
 */
/** Instrumented function afb_ws_hangup(struct afb_ws*) */
void afb_ws_hangup(struct afb_ws *ws)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/afb_ws_hangup(struct afb_ws*)");AKA_fCall++;
		AKA_mark("lis===203###sois===4791###eois===4813###lif===2###soif===41###eoif===63###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_hangup(struct afb_ws*)");aws_disconnect(ws, 1);

}

/*
 * Is the websocket 'ws' still connected ?
 */
/** Instrumented function afb_ws_is_connected(struct afb_ws*) */
int afb_ws_is_connected(struct afb_ws *ws)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/afb_ws_is_connected(struct afb_ws*)");AKA_fCall++;
		AKA_mark("lis===211###sois===4913###eois===4935###lif===2###soif===46###eoif===68###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_is_connected(struct afb_ws*)");return ws->ws != NULL;

}

/*
 * Sends a 'close' command to the endpoint of 'ws' with the 'code' and the
 * 'reason' (that can be NULL and that else should not be greater than 123
 * characters).
 * Returns 0 on success or -1 in case of error.
 */
/** Instrumented function afb_ws_close(struct afb_ws*,uint16_t,const char*) */
int afb_ws_close(struct afb_ws *ws, uint16_t code, const char *reason)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/afb_ws_close(struct afb_ws*,uint16_t,const char*)");AKA_fCall++;
		if (AKA_mark("lis===222###sois===5238###eois===5252###lif===2###soif===78###eoif===92###ifc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_close(struct afb_ws*,uint16_t,const char*)") && (AKA_mark("lis===222###sois===5238###eois===5252###lif===2###soif===78###eoif===92###isc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_close(struct afb_ws*,uint16_t,const char*)")&&ws->ws == NULL)) {
		/* disconnected */
				AKA_mark("lis===224###sois===5279###eois===5293###lif===4###soif===119###eoif===133###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_close(struct afb_ws*,uint16_t,const char*)");errno = EPIPE;

				AKA_mark("lis===225###sois===5296###eois===5306###lif===5###soif===136###eoif===146###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_close(struct afb_ws*,uint16_t,const char*)");return -1;

	}
	else {AKA_mark("lis===-222-###sois===-5238-###eois===-523814-###lif===-2-###soif===-###eoif===-92-###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_close(struct afb_ws*,uint16_t,const char*)");}

		AKA_mark("lis===227###sois===5311###eois===5391###lif===7###soif===151###eoif===231###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_close(struct afb_ws*,uint16_t,const char*)");return websock_close(ws->ws, code, reason, reason == NULL ? 0 : strlen(reason));

}

/*
 * Sends a 'close' command to the endpoint of 'ws' with the 'code' and the
 * 'reason' (that can be NULL and that else should not be greater than 123
 * characters).
 * Raise an error after 'close' command is sent.
 * Returns 0 on success or -1 in case of error.
 */
/** Instrumented function afb_ws_error(struct afb_ws*,uint16_t,const char*) */
int afb_ws_error(struct afb_ws *ws, uint16_t code, const char *reason)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/afb_ws_error(struct afb_ws*,uint16_t,const char*)");AKA_fCall++;
		if (AKA_mark("lis===239###sois===5743###eois===5757###lif===2###soif===78###eoif===92###ifc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_error(struct afb_ws*,uint16_t,const char*)") && (AKA_mark("lis===239###sois===5743###eois===5757###lif===2###soif===78###eoif===92###isc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_error(struct afb_ws*,uint16_t,const char*)")&&ws->ws == NULL)) {
		/* disconnected */
				AKA_mark("lis===241###sois===5784###eois===5798###lif===4###soif===119###eoif===133###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_error(struct afb_ws*,uint16_t,const char*)");errno = EPIPE;

				AKA_mark("lis===242###sois===5801###eois===5811###lif===5###soif===136###eoif===146###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_error(struct afb_ws*,uint16_t,const char*)");return -1;

	}
	else {AKA_mark("lis===-239-###sois===-5743-###eois===-574314-###lif===-2-###soif===-###eoif===-92-###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_error(struct afb_ws*,uint16_t,const char*)");}

		AKA_mark("lis===244###sois===5816###eois===5896###lif===7###soif===151###eoif===231###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_error(struct afb_ws*,uint16_t,const char*)");return websock_error(ws->ws, code, reason, reason == NULL ? 0 : strlen(reason));

}

/*
 * Sends a 'text' of 'length' to the endpoint of 'ws'.
 * Returns 0 on success or -1 in case of error.
 */
/** Instrumented function afb_ws_text(struct afb_ws*,const char*,size_t) */
int afb_ws_text(struct afb_ws *ws, const char *text, size_t length)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/afb_ws_text(struct afb_ws*,const char*,size_t)");AKA_fCall++;
		if (AKA_mark("lis===253###sois===6085###eois===6099###lif===2###soif===75###eoif===89###ifc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_text(struct afb_ws*,const char*,size_t)") && (AKA_mark("lis===253###sois===6085###eois===6099###lif===2###soif===75###eoif===89###isc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_text(struct afb_ws*,const char*,size_t)")&&ws->ws == NULL)) {
		/* disconnected */
				AKA_mark("lis===255###sois===6126###eois===6140###lif===4###soif===116###eoif===130###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_text(struct afb_ws*,const char*,size_t)");errno = EPIPE;

				AKA_mark("lis===256###sois===6143###eois===6153###lif===5###soif===133###eoif===143###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_text(struct afb_ws*,const char*,size_t)");return -1;

	}
	else {AKA_mark("lis===-253-###sois===-6085-###eois===-608514-###lif===-2-###soif===-###eoif===-89-###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_text(struct afb_ws*,const char*,size_t)");}

		AKA_mark("lis===258###sois===6158###eois===6203###lif===7###soif===148###eoif===193###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_text(struct afb_ws*,const char*,size_t)");return websock_text(ws->ws, 1, text, length);

}

/*
 * Sends a variable list of texts to the endpoint of 'ws'.
 * Returns 0 on success or -1 in case of error.
 */
/** Instrumented function afb_ws_texts(struct afb_ws*) */
int afb_ws_texts(struct afb_ws *ws, ...)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");AKA_fCall++;
		AKA_mark("lis===267###sois===6365###eois===6378###lif===2###soif===44###eoif===57###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");va_list args;

		AKA_mark("lis===268###sois===6380###eois===6401###lif===3###soif===59###eoif===80###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");struct iovec ios[32];

		AKA_mark("lis===269###sois===6403###eois===6413###lif===4###soif===82###eoif===92###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");int count;

		AKA_mark("lis===270###sois===6415###eois===6429###lif===5###soif===94###eoif===108###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");const char *s;


		if (AKA_mark("lis===272###sois===6436###eois===6450###lif===7###soif===115###eoif===129###ifc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)") && (AKA_mark("lis===272###sois===6436###eois===6450###lif===7###soif===115###eoif===129###isc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)")&&ws->ws == NULL)) {
		/* disconnected */
				AKA_mark("lis===274###sois===6477###eois===6491###lif===9###soif===156###eoif===170###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");errno = EPIPE;

				AKA_mark("lis===275###sois===6494###eois===6504###lif===10###soif===173###eoif===183###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");return -1;

	}
	else {AKA_mark("lis===-272-###sois===-6436-###eois===-643614-###lif===-7-###soif===-###eoif===-129-###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");}


		AKA_mark("lis===278###sois===6510###eois===6520###lif===13###soif===189###eoif===199###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");count = 0;

		AKA_mark("lis===279###sois===6522###eois===6541###lif===14###soif===201###eoif===220###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");va_start(args, ws);

	/* Cant instrument this following code */
s = va_arg(args, const char *);
		while (AKA_mark("lis===281###sois===6583###eois===6592###lif===16###soif===262###eoif===271###ifc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)") && (AKA_mark("lis===281###sois===6583###eois===6592###lif===16###soif===262###eoif===271###isc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)")&&s != NULL)) {
				if (AKA_mark("lis===282###sois===6602###eois===6613###lif===17###soif===281###eoif===292###ifc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)") && (AKA_mark("lis===282###sois===6602###eois===6613###lif===17###soif===281###eoif===292###isc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)")&&count == 32)) {
						AKA_mark("lis===283###sois===6620###eois===6635###lif===18###soif===299###eoif===314###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");errno = EINVAL;

						AKA_mark("lis===284###sois===6639###eois===6649###lif===19###soif===318###eoif===328###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");return -1;

		}
		else {AKA_mark("lis===-282-###sois===-6602-###eois===-660211-###lif===-17-###soif===-###eoif===-292-###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");}

				AKA_mark("lis===286###sois===6656###eois===6687###lif===21###soif===335###eoif===366###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");ios[count].iov_base = (void*)s;

				AKA_mark("lis===287###sois===6690###eois===6721###lif===22###soif===369###eoif===400###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");ios[count].iov_len = strlen(s);

				AKA_mark("lis===288###sois===6724###eois===6732###lif===23###soif===403###eoif===411###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");count++;

		/* Cant instrument this following code */
s = va_arg(args, const char *);
	}

		AKA_mark("lis===291###sois===6771###eois===6784###lif===26###soif===450###eoif===463###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");va_end(args);

		AKA_mark("lis===292###sois===6786###eois===6831###lif===27###soif===465###eoif===510###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_texts(struct afb_ws*)");return websock_text_v(ws->ws, 1, ios, count);

}

/*
 * Sends a text data described in the 'count' 'iovec' to the endpoint of 'ws'.
 * Returns 0 on success or -1 in case of error.
 */
/** Instrumented function afb_ws_text_v(struct afb_ws*,const struct iovec*,int) */
int afb_ws_text_v(struct afb_ws *ws, const struct iovec *iovec, int count)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/afb_ws_text_v(struct afb_ws*,const struct iovec*,int)");AKA_fCall++;
		if (AKA_mark("lis===301###sois===7051###eois===7065###lif===2###soif===82###eoif===96###ifc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_text_v(struct afb_ws*,const struct iovec*,int)") && (AKA_mark("lis===301###sois===7051###eois===7065###lif===2###soif===82###eoif===96###isc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_text_v(struct afb_ws*,const struct iovec*,int)")&&ws->ws == NULL)) {
		/* disconnected */
				AKA_mark("lis===303###sois===7092###eois===7106###lif===4###soif===123###eoif===137###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_text_v(struct afb_ws*,const struct iovec*,int)");errno = EPIPE;

				AKA_mark("lis===304###sois===7109###eois===7119###lif===5###soif===140###eoif===150###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_text_v(struct afb_ws*,const struct iovec*,int)");return -1;

	}
	else {AKA_mark("lis===-301-###sois===-7051-###eois===-705114-###lif===-2-###soif===-###eoif===-96-###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_text_v(struct afb_ws*,const struct iovec*,int)");}

		AKA_mark("lis===306###sois===7124###eois===7171###lif===7###soif===155###eoif===202###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_text_v(struct afb_ws*,const struct iovec*,int)");return websock_text_v(ws->ws, 1, iovec, count);

}

/*
 * Sends a binary 'data' of 'length' to the endpoint of 'ws'.
 * Returns 0 on success or -1 in case of error.
 */
/** Instrumented function afb_ws_binary(struct afb_ws*,const void*,size_t) */
int afb_ws_binary(struct afb_ws *ws, const void *data, size_t length)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/afb_ws_binary(struct afb_ws*,const void*,size_t)");AKA_fCall++;
		if (AKA_mark("lis===315###sois===7369###eois===7383###lif===2###soif===77###eoif===91###ifc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_binary(struct afb_ws*,const void*,size_t)") && (AKA_mark("lis===315###sois===7369###eois===7383###lif===2###soif===77###eoif===91###isc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_binary(struct afb_ws*,const void*,size_t)")&&ws->ws == NULL)) {
		/* disconnected */
				AKA_mark("lis===317###sois===7410###eois===7424###lif===4###soif===118###eoif===132###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_binary(struct afb_ws*,const void*,size_t)");errno = EPIPE;

				AKA_mark("lis===318###sois===7427###eois===7437###lif===5###soif===135###eoif===145###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_binary(struct afb_ws*,const void*,size_t)");return -1;

	}
	else {AKA_mark("lis===-315-###sois===-7369-###eois===-736914-###lif===-2-###soif===-###eoif===-91-###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_binary(struct afb_ws*,const void*,size_t)");}

		AKA_mark("lis===320###sois===7442###eois===7489###lif===7###soif===150###eoif===197###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_binary(struct afb_ws*,const void*,size_t)");return websock_binary(ws->ws, 1, data, length);

}

/*
 * Sends a binary data described in the 'count' 'iovec' to the endpoint of 'ws'.
 * Returns 0 on success or -1 in case of error.
 */
/** Instrumented function afb_ws_binary_v(struct afb_ws*,const struct iovec*,int) */
int afb_ws_binary_v(struct afb_ws *ws, const struct iovec *iovec, int count)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/afb_ws_binary_v(struct afb_ws*,const struct iovec*,int)");AKA_fCall++;
		if (AKA_mark("lis===329###sois===7713###eois===7727###lif===2###soif===84###eoif===98###ifc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_binary_v(struct afb_ws*,const struct iovec*,int)") && (AKA_mark("lis===329###sois===7713###eois===7727###lif===2###soif===84###eoif===98###isc===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_binary_v(struct afb_ws*,const struct iovec*,int)")&&ws->ws == NULL)) {
		/* disconnected */
				AKA_mark("lis===331###sois===7754###eois===7768###lif===4###soif===125###eoif===139###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_binary_v(struct afb_ws*,const struct iovec*,int)");errno = EPIPE;

				AKA_mark("lis===332###sois===7771###eois===7781###lif===5###soif===142###eoif===152###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_binary_v(struct afb_ws*,const struct iovec*,int)");return -1;

	}
	else {AKA_mark("lis===-329-###sois===-7713-###eois===-771314-###lif===-2-###soif===-###eoif===-98-###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_binary_v(struct afb_ws*,const struct iovec*,int)");}

		AKA_mark("lis===334###sois===7786###eois===7835###lif===7###soif===157###eoif===206###ins===true###function===./app-framework-binder/src/afb-ws.c/afb_ws_binary_v(struct afb_ws*,const struct iovec*,int)");return websock_binary_v(ws->ws, 1, iovec, count);

}

/*
 * callback for writing data
 */
/** Instrumented function aws_writev(struct afb_ws*,const struct iovec*,int) */
static ssize_t aws_writev(struct afb_ws *ws, const struct iovec *iov, int iovcnt)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");AKA_fCall++;
		AKA_mark("lis===342###sois===7960###eois===7966###lif===2###soif===85###eoif===91###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");int i;

		AKA_mark("lis===343###sois===7968###eois===7988###lif===3###soif===93###eoif===113###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");ssize_t rc, sz, dsz;

		AKA_mark("lis===344###sois===7990###eois===8009###lif===4###soif===115###eoif===134###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");struct iovec *iov2;

		AKA_mark("lis===345###sois===8011###eois===8029###lif===5###soif===136###eoif===154###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");struct pollfd pfd;


	/* compute the size */
		AKA_mark("lis===348###sois===8056###eois===8064###lif===8###soif===181###eoif===189###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");dsz = 0;

		AKA_mark("lis===349###sois===8066###eois===8072###lif===9###soif===191###eoif===197###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");i = 0;

		while (AKA_mark("lis===350###sois===8081###eois===8091###lif===10###soif===206###eoif===216###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)") && (AKA_mark("lis===350###sois===8081###eois===8091###lif===10###soif===206###eoif===216###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)")&&i < iovcnt)) {
				AKA_mark("lis===351###sois===8097###eois===8121###lif===11###soif===222###eoif===246###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");dsz += iov[i++].iov_len;

				if (AKA_mark("lis===352###sois===8128###eois===8135###lif===12###soif===253###eoif===260###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)") && (AKA_mark("lis===352###sois===8128###eois===8135###lif===12###soif===253###eoif===260###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)")&&dsz < 0)) {
						AKA_mark("lis===353###sois===8142###eois===8157###lif===13###soif===267###eoif===282###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");errno = EINVAL;

						AKA_mark("lis===354###sois===8161###eois===8171###lif===14###soif===286###eoif===296###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");return -1;

		}
		else {AKA_mark("lis===-352-###sois===-8128-###eois===-81287-###lif===-12-###soif===-###eoif===-260-###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");}

	}

		if (AKA_mark("lis===357###sois===8184###eois===8192###lif===17###soif===309###eoif===317###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)") && (AKA_mark("lis===357###sois===8184###eois===8192###lif===17###soif===309###eoif===317###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)")&&dsz == 0)) {
		AKA_mark("lis===358###sois===8196###eois===8205###lif===18###soif===321###eoif===330###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");return 0;
	}
	else {AKA_mark("lis===-357-###sois===-8184-###eois===-81848-###lif===-17-###soif===-###eoif===-317-###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");}


	/* write the data */
		AKA_mark("lis===361###sois===8230###eois===8256###lif===21###soif===355###eoif===381###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");iov2 = (struct iovec*)iov;

		AKA_mark("lis===362###sois===8258###eois===8267###lif===22###soif===383###eoif===392###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");sz = dsz;

		for (;;) {
				AKA_mark("lis===364###sois===8282###eois===8316###lif===24###soif===407###eoif===441###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");rc = writev(ws->fd, iov2, iovcnt);

				if (AKA_mark("lis===365###sois===8323###eois===8329###lif===25###soif===448###eoif===454###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)") && (AKA_mark("lis===365###sois===8323###eois===8329###lif===25###soif===448###eoif===454###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)")&&rc < 0)) {
						if (AKA_mark("lis===366###sois===8340###eois===8354###lif===26###soif===465###eoif===479###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)") && (AKA_mark("lis===366###sois===8340###eois===8354###lif===26###soif===465###eoif===479###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)")&&errno == EINTR)) {
				AKA_mark("lis===367###sois===8360###eois===8369###lif===27###soif===485###eoif===494###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");continue;
			}
			else {AKA_mark("lis===-366-###sois===-8340-###eois===-834014-###lif===-26-###soif===-###eoif===-479-###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");}

						if (AKA_mark("lis===368###sois===8377###eois===8392###lif===28###soif===502###eoif===517###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)") && (AKA_mark("lis===368###sois===8377###eois===8392###lif===28###soif===502###eoif===517###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)")&&errno != EAGAIN)) {
				AKA_mark("lis===369###sois===8398###eois===8408###lif===29###soif===523###eoif===533###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");return -1;
			}
			else {AKA_mark("lis===-368-###sois===-8377-###eois===-837715-###lif===-28-###soif===-###eoif===-517-###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");}

		}
		else {
						AKA_mark("lis===371###sois===8423###eois===8433###lif===31###soif===548###eoif===558###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");dsz -= rc;

						if (AKA_mark("lis===372###sois===8441###eois===8449###lif===32###soif===566###eoif===574###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)") && (AKA_mark("lis===372###sois===8441###eois===8449###lif===32###soif===566###eoif===574###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)")&&dsz == 0)) {
				AKA_mark("lis===373###sois===8455###eois===8465###lif===33###soif===580###eoif===590###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");return sz;
			}
			else {AKA_mark("lis===-372-###sois===-8441-###eois===-84418-###lif===-32-###soif===-###eoif===-574-###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");}


						AKA_mark("lis===375###sois===8470###eois===8476###lif===35###soif===595###eoif===601###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");i = 0;

						while (AKA_mark("lis===376###sois===8487###eois===8517###lif===36###soif===612###eoif===642###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)") && (AKA_mark("lis===376###sois===8487###eois===8517###lif===36###soif===612###eoif===642###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)")&&rc >= (ssize_t)iov2[i].iov_len)) {
				AKA_mark("lis===377###sois===8523###eois===8556###lif===37###soif===648###eoif===681###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");rc -= (ssize_t)iov2[i++].iov_len;
			}


						AKA_mark("lis===379###sois===8561###eois===8573###lif===39###soif===686###eoif===698###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");iovcnt -= i;

						if (AKA_mark("lis===380###sois===8581###eois===8592###lif===40###soif===706###eoif===717###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)") && (AKA_mark("lis===380###sois===8581###eois===8592###lif===40###soif===706###eoif===717###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)")&&iov2 != iov)) {
				AKA_mark("lis===381###sois===8598###eois===8608###lif===41###soif===723###eoif===733###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");iov2 += i;
			}
			else {
								AKA_mark("lis===383###sois===8623###eois===8632###lif===43###soif===748###eoif===757###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");iov += i;

								AKA_mark("lis===384###sois===8637###eois===8674###lif===44###soif===762###eoif===799###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");iov2 = alloca(iovcnt * sizeof *iov2);

								AKA_mark("lis===385###sois===8684###eois===8691###lif===45###soif===809###eoif===816###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");for (i = 0 ;AKA_mark("lis===385###sois===8692###eois===8702###lif===45###soif===817###eoif===827###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)") && AKA_mark("lis===385###sois===8692###eois===8702###lif===45###soif===817###eoif===827###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)")&&i < iovcnt;({AKA_mark("lis===385###sois===8705###eois===8708###lif===45###soif===830###eoif===833###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");i++;})) {
					AKA_mark("lis===386###sois===8715###eois===8732###lif===46###soif===840###eoif===857###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");iov2[i] = iov[i];
				}

			}

						AKA_mark("lis===388###sois===8741###eois===8762###lif===48###soif===866###eoif===887###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");iov2->iov_base += rc;

						AKA_mark("lis===389###sois===8766###eois===8786###lif===49###soif===891###eoif===911###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");iov2->iov_len -= rc;

		}

				AKA_mark("lis===391###sois===8793###eois===8809###lif===51###soif===918###eoif===934###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");pfd.fd = ws->fd;

				AKA_mark("lis===392###sois===8812###eois===8833###lif===52###soif===937###eoif===958###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");pfd.events = POLLOUT;

				AKA_mark("lis===393###sois===8836###eois===8854###lif===53###soif===961###eoif===979###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_writev(struct afb_ws*,const struct iovec*,int)");poll(&pfd, 1, 10);

	}

}

/*
 * callback for reading data
 */
/** Instrumented function aws_readv(struct afb_ws*,const struct iovec*,int) */
static ssize_t aws_readv(struct afb_ws *ws, const struct iovec *iov, int iovcnt)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/aws_readv(struct afb_ws*,const struct iovec*,int)");AKA_fCall++;
		AKA_mark("lis===402###sois===8981###eois===8992###lif===2###soif===84###eoif===95###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_readv(struct afb_ws*,const struct iovec*,int)");ssize_t rc;

		do {
				AKA_mark("lis===404###sois===9001###eois===9033###lif===4###soif===104###eoif===136###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_readv(struct afb_ws*,const struct iovec*,int)");rc = readv(ws->fd, iov, iovcnt);

	}
	while (AKA_mark("lis===405###sois===9043###eois===9069###lif===5###soif===146###eoif===172###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_readv(struct afb_ws*,const struct iovec*,int)") && ((AKA_mark("lis===405###sois===9043###eois===9051###lif===5###soif===146###eoif===154###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_readv(struct afb_ws*,const struct iovec*,int)")&&rc == -1)	&&(AKA_mark("lis===405###sois===9055###eois===9069###lif===5###soif===158###eoif===172###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_readv(struct afb_ws*,const struct iovec*,int)")&&errno == EINTR)));

		if (AKA_mark("lis===406###sois===9077###eois===9084###lif===6###soif===180###eoif===187###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_readv(struct afb_ws*,const struct iovec*,int)") && (AKA_mark("lis===406###sois===9077###eois===9084###lif===6###soif===180###eoif===187###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_readv(struct afb_ws*,const struct iovec*,int)")&&rc == 0)) {
				AKA_mark("lis===407###sois===9090###eois===9104###lif===7###soif===193###eoif===207###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_readv(struct afb_ws*,const struct iovec*,int)");errno = EPIPE;

				AKA_mark("lis===408###sois===9107###eois===9115###lif===8###soif===210###eoif===218###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_readv(struct afb_ws*,const struct iovec*,int)");rc = -1;

	}
	else {AKA_mark("lis===-406-###sois===-9077-###eois===-90777-###lif===-6-###soif===-###eoif===-187-###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_readv(struct afb_ws*,const struct iovec*,int)");}

		AKA_mark("lis===410###sois===9120###eois===9130###lif===10###soif===223###eoif===233###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_readv(struct afb_ws*,const struct iovec*,int)");return rc;

}

/*
 * callback on incoming data
 */
/** Instrumented function aws_on_readable(struct afb_ws*) */
static void aws_on_readable(struct afb_ws *ws)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/aws_on_readable(struct afb_ws*)");AKA_fCall++;
		AKA_mark("lis===418###sois===9220###eois===9227###lif===2###soif===50###eoif===57###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_readable(struct afb_ws*)");int rc;


		AKA_mark("lis===420###sois===9230###eois===9253###lif===4###soif===60###eoif===83###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_readable(struct afb_ws*)");assert(ws->ws != NULL);

		AKA_mark("lis===421###sois===9255###eois===9288###lif===5###soif===85###eoif===118###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_readable(struct afb_ws*)");rc = websock_dispatch(ws->ws, 0);

		if (AKA_mark("lis===422###sois===9294###eois===9318###lif===6###soif===124###eoif===148###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_readable(struct afb_ws*)") && ((AKA_mark("lis===422###sois===9294###eois===9300###lif===6###soif===124###eoif===130###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_readable(struct afb_ws*)")&&rc < 0)	&&(AKA_mark("lis===422###sois===9304###eois===9318###lif===6###soif===134###eoif===148###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_readable(struct afb_ws*)")&&errno == EPIPE))) {
		AKA_mark("lis===423###sois===9322###eois===9340###lif===7###soif===152###eoif===170###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_readable(struct afb_ws*)");afb_ws_hangup(ws);
	}
	else {AKA_mark("lis===-422-###sois===-9294-###eois===-929424-###lif===-6-###soif===-###eoif===-148-###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_readable(struct afb_ws*)");}

}

/*
 * Reads from the websocket handled by 'ws' data of length 'size'
 * and append it to the current buffer of 'ws'.
 * Returns 0 in case of error or 1 in case of success.
 */
/** Instrumented function aws_read(struct afb_ws*,size_t) */
static int aws_read(struct afb_ws *ws, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");AKA_fCall++;
		AKA_mark("lis===433###sois===9575###eois===9593###lif===2###soif===55###eoif===73###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");struct pollfd pfd;

		AKA_mark("lis===434###sois===9595###eois===9606###lif===3###soif===75###eoif===86###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");ssize_t sz;

		AKA_mark("lis===435###sois===9608###eois===9621###lif===4###soif===88###eoif===101###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");char *buffer;


		if (AKA_mark("lis===437###sois===9628###eois===9666###lif===6###soif===108###eoif===146###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)") && ((AKA_mark("lis===437###sois===9628###eois===9637###lif===6###soif===108###eoif===117###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)")&&size != 0)	||(AKA_mark("lis===437###sois===9641###eois===9666###lif===6###soif===121###eoif===146###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)")&&ws->buffer.buffer == NULL))) {
				AKA_mark("lis===438###sois===9672###eois===9736###lif===7###soif===152###eoif===216###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");buffer = realloc(ws->buffer.buffer, ws->buffer.size + size + 1);

				if (AKA_mark("lis===439###sois===9743###eois===9757###lif===8###soif===223###eoif===237###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)") && (AKA_mark("lis===439###sois===9743###eois===9757###lif===8###soif===223###eoif===237###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)")&&buffer == NULL)) {
			AKA_mark("lis===440###sois===9762###eois===9771###lif===9###soif===242###eoif===251###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");return 0;
		}
		else {AKA_mark("lis===-439-###sois===-9743-###eois===-974314-###lif===-8-###soif===-###eoif===-237-###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");}

				AKA_mark("lis===441###sois===9774###eois===9801###lif===10###soif===254###eoif===281###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");ws->buffer.buffer = buffer;

				while (AKA_mark("lis===442###sois===9811###eois===9820###lif===11###soif===291###eoif===300###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)") && (AKA_mark("lis===442###sois===9811###eois===9820###lif===11###soif===291###eoif===300###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)")&&size != 0)) {
						AKA_mark("lis===443###sois===9827###eois===9885###lif===12###soif===307###eoif===365###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");sz = websock_read(ws->ws, &buffer[ws->buffer.size], size);

						if (AKA_mark("lis===444###sois===9893###eois===9899###lif===13###soif===373###eoif===379###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)") && (AKA_mark("lis===444###sois===9893###eois===9899###lif===13###soif===373###eoif===379###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)")&&sz < 0)) {
								if (AKA_mark("lis===445###sois===9911###eois===9926###lif===14###soif===391###eoif===406###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)") && (AKA_mark("lis===445###sois===9911###eois===9926###lif===14###soif===391###eoif===406###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)")&&errno != EAGAIN)) {
					AKA_mark("lis===446###sois===9933###eois===9942###lif===15###soif===413###eoif===422###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");return 0;
				}
				else {AKA_mark("lis===-445-###sois===-9911-###eois===-991115-###lif===-14-###soif===-###eoif===-406-###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");}

								AKA_mark("lis===447###sois===9947###eois===9963###lif===16###soif===427###eoif===443###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");pfd.fd = ws->fd;

								AKA_mark("lis===448###sois===9968###eois===9988###lif===17###soif===448###eoif===468###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");pfd.events = POLLIN;

								AKA_mark("lis===449###sois===9993###eois===10011###lif===18###soif===473###eoif===491###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");poll(&pfd, 1, 10);
 /* TODO: make fully asynchronous websockets */
			}
			else {
								AKA_mark("lis===451###sois===10075###eois===10105###lif===20###soif===555###eoif===585###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");ws->buffer.size += (size_t)sz;

								AKA_mark("lis===452###sois===10110###eois===10129###lif===21###soif===590###eoif===609###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");size -= (size_t)sz;

			}

		}

	}
	else {AKA_mark("lis===-437-###sois===-9628-###eois===-962838-###lif===-6-###soif===-###eoif===-146-###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");}

		AKA_mark("lis===456###sois===10143###eois===10152###lif===25###soif===623###eoif===632###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_read(struct afb_ws*,size_t)");return 1;

}

/*
 * Callback when 'close' command received from 'ws' with 'code' and 'size'.
 */
/** Instrumented function aws_on_close(struct afb_ws*,uint16_t,size_t) */
static void aws_on_close(struct afb_ws *ws, uint16_t code, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/aws_on_close(struct afb_ws*,uint16_t,size_t)");AKA_fCall++;
		AKA_mark("lis===464###sois===10314###eois===10327###lif===2###soif===75###eoif===88###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_close(struct afb_ws*,uint16_t,size_t)");struct buf b;


		AKA_mark("lis===466###sois===10330###eois===10350###lif===4###soif===91###eoif===111###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_close(struct afb_ws*,uint16_t,size_t)");ws->state = waiting;

		AKA_mark("lis===467###sois===10352###eois===10373###lif===5###soif===113###eoif===134###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_close(struct afb_ws*,uint16_t,size_t)");aws_clear_buffer(ws);

		if (AKA_mark("lis===468###sois===10379###eois===10404###lif===6###soif===140###eoif===165###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_close(struct afb_ws*,uint16_t,size_t)") && (AKA_mark("lis===468###sois===10379###eois===10404###lif===6###soif===140###eoif===165###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_close(struct afb_ws*,uint16_t,size_t)")&&ws->itf->on_close == NULL)) {
				AKA_mark("lis===469###sois===10410###eois===10431###lif===7###soif===171###eoif===192###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_close(struct afb_ws*,uint16_t,size_t)");websock_drop(ws->ws);

				AKA_mark("lis===470###sois===10434###eois===10452###lif===8###soif===195###eoif===213###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_close(struct afb_ws*,uint16_t,size_t)");afb_ws_hangup(ws);

	}
	else {
		if (AKA_mark("lis===471###sois===10465###eois===10484###lif===9###soif===226###eoif===245###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_close(struct afb_ws*,uint16_t,size_t)") && (AKA_mark("lis===471###sois===10465###eois===10484###lif===9###soif===226###eoif===245###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_close(struct afb_ws*,uint16_t,size_t)")&&!aws_read(ws, size))) {
			AKA_mark("lis===472###sois===10488###eois===10534###lif===10###soif===249###eoif===295###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_close(struct afb_ws*,uint16_t,size_t)");ws->itf->on_close(ws->closure, code, NULL, 0);
		}
		else {
					AKA_mark("lis===474###sois===10545###eois===10569###lif===12###soif===306###eoif===330###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_close(struct afb_ws*,uint16_t,size_t)");b = aws_pick_buffer(ws);

					AKA_mark("lis===475###sois===10572###eois===10627###lif===13###soif===333###eoif===388###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_close(struct afb_ws*,uint16_t,size_t)");ws->itf->on_close(ws->closure, code, b.buffer, b.size);

	}
	}

}

/*
 * Drops any incoming data and send an error of 'code'
 */
/** Instrumented function aws_drop_error(struct afb_ws*,uint16_t) */
static void aws_drop_error(struct afb_ws *ws, uint16_t code)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/aws_drop_error(struct afb_ws*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===484###sois===10760###eois===10780###lif===2###soif===64###eoif===84###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_drop_error(struct afb_ws*,uint16_t)");ws->state = waiting;

		AKA_mark("lis===485###sois===10782###eois===10803###lif===3###soif===86###eoif===107###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_drop_error(struct afb_ws*,uint16_t)");aws_clear_buffer(ws);

		AKA_mark("lis===486###sois===10805###eois===10826###lif===4###soif===109###eoif===130###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_drop_error(struct afb_ws*,uint16_t)");websock_drop(ws->ws);

		AKA_mark("lis===487###sois===10828###eois===10865###lif===5###soif===132###eoif===169###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_drop_error(struct afb_ws*,uint16_t)");websock_error(ws->ws, code, NULL, 0);

}

/*
 * Reads either text or binary data of 'size' from 'ws' eventually 'last'.
 */
/** Instrumented function aws_continue(struct afb_ws*,int,size_t) */
static void aws_continue(struct afb_ws *ws, int last, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/aws_continue(struct afb_ws*,int,size_t)");AKA_fCall++;
		AKA_mark("lis===495###sois===11021###eois===11034###lif===2###soif===70###eoif===83###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_continue(struct afb_ws*,int,size_t)");struct buf b;

		AKA_mark("lis===496###sois===11036###eois===11046###lif===3###soif===85###eoif===95###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_continue(struct afb_ws*,int,size_t)");int istxt;


		if (AKA_mark("lis===498###sois===11053###eois===11072###lif===5###soif===102###eoif===121###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_continue(struct afb_ws*,int,size_t)") && (AKA_mark("lis===498###sois===11053###eois===11072###lif===5###soif===102###eoif===121###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_continue(struct afb_ws*,int,size_t)")&&!aws_read(ws, size))) {
		AKA_mark("lis===499###sois===11076###eois===11120###lif===6###soif===125###eoif===169###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_continue(struct afb_ws*,int,size_t)");aws_drop_error(ws, WEBSOCKET_CODE_ABNORMAL);
	}
	else {
		if (AKA_mark("lis===500###sois===11131###eois===11135###lif===7###soif===180###eoif===184###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_continue(struct afb_ws*,int,size_t)") && (AKA_mark("lis===500###sois===11131###eois===11135###lif===7###soif===180###eoif===184###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_continue(struct afb_ws*,int,size_t)")&&last)) {
					AKA_mark("lis===501###sois===11141###eois===11175###lif===8###soif===190###eoif===224###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_continue(struct afb_ws*,int,size_t)");istxt = ws->state == reading_text;

					AKA_mark("lis===502###sois===11178###eois===11198###lif===9###soif===227###eoif===247###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_continue(struct afb_ws*,int,size_t)");ws->state = waiting;

					AKA_mark("lis===503###sois===11201###eois===11225###lif===10###soif===250###eoif===274###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_continue(struct afb_ws*,int,size_t)");b = aws_pick_buffer(ws);

					AKA_mark("lis===504###sois===11228###eois===11307###lif===11###soif===277###eoif===356###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_continue(struct afb_ws*,int,size_t)");(istxt ? ws->itf->on_text : ws->itf->on_binary)(ws->closure, b.buffer, b.size);

	}
		else {AKA_mark("lis===-500-###sois===-11131-###eois===-111314-###lif===-7-###soif===-###eoif===-184-###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_continue(struct afb_ws*,int,size_t)");}
	}

}

/*
 * Callback when 'text' message received from 'ws' with 'size' and possibly 'last'.
 */
/** Instrumented function aws_on_text(struct afb_ws*,int,size_t) */
static void aws_on_text(struct afb_ws *ws, int last, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/aws_on_text(struct afb_ws*,int,size_t)");AKA_fCall++;
		if (AKA_mark("lis===513###sois===11478###eois===11498###lif===2###soif===73###eoif===93###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_text(struct afb_ws*,int,size_t)") && (AKA_mark("lis===513###sois===11478###eois===11498###lif===2###soif===73###eoif===93###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_text(struct afb_ws*,int,size_t)")&&ws->state != waiting)) {
		AKA_mark("lis===514###sois===11502###eois===11552###lif===3###soif===97###eoif===147###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_text(struct afb_ws*,int,size_t)");aws_drop_error(ws, WEBSOCKET_CODE_PROTOCOL_ERROR);
	}
	else {
		if (AKA_mark("lis===515###sois===11563###eois===11587###lif===4###soif===158###eoif===182###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_text(struct afb_ws*,int,size_t)") && (AKA_mark("lis===515###sois===11563###eois===11587###lif===4###soif===158###eoif===182###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_text(struct afb_ws*,int,size_t)")&&ws->itf->on_text == NULL)) {
			AKA_mark("lis===516###sois===11591###eois===11638###lif===5###soif===186###eoif===233###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_text(struct afb_ws*,int,size_t)");aws_drop_error(ws, WEBSOCKET_CODE_CANT_ACCEPT);
		}
		else {
					AKA_mark("lis===518###sois===11649###eois===11674###lif===7###soif===244###eoif===269###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_text(struct afb_ws*,int,size_t)");ws->state = reading_text;

					AKA_mark("lis===519###sois===11677###eois===11706###lif===8###soif===272###eoif===301###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_text(struct afb_ws*,int,size_t)");aws_continue(ws, last, size);

	}
	}

}

/*
 * Callback when 'binary' message received from 'ws' with 'size' and possibly 'last'.
 */
/** Instrumented function aws_on_binary(struct afb_ws*,int,size_t) */
static void aws_on_binary(struct afb_ws *ws, int last, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/aws_on_binary(struct afb_ws*,int,size_t)");AKA_fCall++;
		if (AKA_mark("lis===528###sois===11881###eois===11901###lif===2###soif===75###eoif===95###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_binary(struct afb_ws*,int,size_t)") && (AKA_mark("lis===528###sois===11881###eois===11901###lif===2###soif===75###eoif===95###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_binary(struct afb_ws*,int,size_t)")&&ws->state != waiting)) {
		AKA_mark("lis===529###sois===11905###eois===11955###lif===3###soif===99###eoif===149###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_binary(struct afb_ws*,int,size_t)");aws_drop_error(ws, WEBSOCKET_CODE_PROTOCOL_ERROR);
	}
	else {
		if (AKA_mark("lis===530###sois===11966###eois===11992###lif===4###soif===160###eoif===186###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_binary(struct afb_ws*,int,size_t)") && (AKA_mark("lis===530###sois===11966###eois===11992###lif===4###soif===160###eoif===186###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_binary(struct afb_ws*,int,size_t)")&&ws->itf->on_binary == NULL)) {
			AKA_mark("lis===531###sois===11996###eois===12043###lif===5###soif===190###eoif===237###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_binary(struct afb_ws*,int,size_t)");aws_drop_error(ws, WEBSOCKET_CODE_CANT_ACCEPT);
		}
		else {
					AKA_mark("lis===533###sois===12054###eois===12081###lif===7###soif===248###eoif===275###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_binary(struct afb_ws*,int,size_t)");ws->state = reading_binary;

					AKA_mark("lis===534###sois===12084###eois===12113###lif===8###soif===278###eoif===307###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_binary(struct afb_ws*,int,size_t)");aws_continue(ws, last, size);

	}
	}

}

/*
 * Callback when 'continue' command received from 'ws' with 'code' and 'size'.
 */
/** Instrumented function aws_on_continue(struct afb_ws*,int,size_t) */
static void aws_on_continue(struct afb_ws *ws, int last, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/aws_on_continue(struct afb_ws*,int,size_t)");AKA_fCall++;
		if (AKA_mark("lis===543###sois===12283###eois===12303###lif===2###soif===77###eoif===97###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_continue(struct afb_ws*,int,size_t)") && (AKA_mark("lis===543###sois===12283###eois===12303###lif===2###soif===77###eoif===97###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_continue(struct afb_ws*,int,size_t)")&&ws->state == waiting)) {
		AKA_mark("lis===544###sois===12307###eois===12357###lif===3###soif===101###eoif===151###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_continue(struct afb_ws*,int,size_t)");aws_drop_error(ws, WEBSOCKET_CODE_PROTOCOL_ERROR);
	}
	else {
		AKA_mark("lis===546###sois===12366###eois===12395###lif===5###soif===160###eoif===189###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_continue(struct afb_ws*,int,size_t)");aws_continue(ws, last, size);
	}

}

/*
 * Callback when 'close' command is sent to 'ws' with 'code' and 'size'.
 */
/** Instrumented function aws_on_error(struct afb_ws*,uint16_t,const void*,size_t) */
static void aws_on_error(struct afb_ws *ws, uint16_t code, const void *data, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws.c/aws_on_error(struct afb_ws*,uint16_t,const void*,size_t)");AKA_fCall++;
		if (AKA_mark("lis===554###sois===12576###eois===12601###lif===2###soif===97###eoif===122###ifc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_error(struct afb_ws*,uint16_t,const void*,size_t)") && (AKA_mark("lis===554###sois===12576###eois===12601###lif===2###soif===97###eoif===122###isc===true###function===./app-framework-binder/src/afb-ws.c/aws_on_error(struct afb_ws*,uint16_t,const void*,size_t)")&&ws->itf->on_error != NULL)) {
		AKA_mark("lis===555###sois===12605###eois===12654###lif===3###soif===126###eoif===175###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_error(struct afb_ws*,uint16_t,const void*,size_t)");ws->itf->on_error(ws->closure, code, data, size);
	}
	else {
		AKA_mark("lis===557###sois===12663###eois===12681###lif===5###soif===184###eoif===202###ins===true###function===./app-framework-binder/src/afb-ws.c/aws_on_error(struct afb_ws*,uint16_t,const void*,size_t)");afb_ws_hangup(ws);
	}

}



#endif

