/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_VERBOSE_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_VERBOSE_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 Copyright (C) 2015-2020 "IoT.bzh"

 author: José Bollo <jose.bollo@iot.bzh>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

#include <stdio.h>
#include <stdarg.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif


/** Instrumented function MASKOF(x) */
#define MASKOF(x)		(1 << (x))

#if !defined(DEFAULT_LOGLEVEL)
# define DEFAULT_LOGLEVEL	Log_Level_Warning
#endif

#if !defined(DEFAULT_LOGMASK)
# define DEFAULT_LOGMASK	(MASKOF((DEFAULT_LOGLEVEL) + 1) - 1)
#endif

#if !defined(MINIMAL_LOGLEVEL)
# define MINIMAL_LOGLEVEL	Log_Level_Error
#endif

#if !defined(MINIMAL_LOGMASK)
# define MINIMAL_LOGMASK	(MASKOF((MINIMAL_LOGLEVEL) + 1) - 1)
#endif

static const char *names[] = {
	"emergency",
	"alert",
	"critical",
	"error",
	"warning",
	"notice",
	"info",
	"debug"
};

static const char asort[] = { 1, 2, 7, 0, 3, 6, 5, 4 };

int logmask = DEFAULT_LOGMASK | MINIMAL_LOGMASK;

void (*verbose_observer)(int loglevel, const char *file, int line, const char *function, const char *fmt, va_list args);

/** Instrumented function CROP_LOGLEVEL(x) */
#define CROP_LOGLEVEL(x) \
	((x) < Log_Level_Emergency ? Log_Level_Emergency \
	                           : (x) > Log_Level_Debug ? Log_Level_Debug : (x))

#if defined(VERBOSE_WITH_SYSLOG)

#include <syslog.h>

static void _vverbose_(int loglevel, const char *file, int line, const char *function, const char *fmt, va_list args)
{
	char *p;

	if (file == NULL || vasprintf(&p, fmt, args) < 0)
		vsyslog(loglevel, fmt, args);
	else {
		syslog(CROP_LOGLEVEL(loglevel), "%s [%s:%d, %s]", p, file, line, function?:"?");
		free(p);
	}
}

void verbose_set_name(const char *name, int authority)
{
	openlog(name, LOG_PERROR, authority ? LOG_AUTH : LOG_USER);
}

#elif defined(VERBOSE_WITH_SYSTEMD)

#define SD_JOURNAL_SUPPRESS_LOCATION

#include <systemd/sd-journal.h>

static const char *appname;

static int appauthority;

static void _vverbose_(int loglevel, const char *file, int line, const char *function, const char *fmt, va_list args)
{
	char lino[20];

	if (file == NULL) {
		sd_journal_printv(loglevel, fmt, args);
	} else {
		sprintf(lino, "%d", line);
		sd_journal_printv_with_location(loglevel, file, lino, function, fmt, args);
	}
}

/** Instrumented function verbose_set_name(const char*,int) */
void verbose_set_name(const char *name, int authority)
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbose_set_name(const char*,int)");AKA_fCall++;
		AKA_mark("lis===260###sois===6080###eois===6095###lif===2###soif===58###eoif===73###ins===true###function===./app-framework-binder/src/verbose.c/verbose_set_name(const char*,int)");appname = name;

		AKA_mark("lis===261###sois===6097###eois===6122###lif===3###soif===75###eoif===100###ins===true###function===./app-framework-binder/src/verbose.c/verbose_set_name(const char*,int)");appauthority = authority;

}

#else

#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/uio.h>
#include <pthread.h>

static const char *appname;

static int appauthority;

static const char *prefixes[] = {
	"<0> EMERGENCY",
	"<1> ALERT",
	"<2> CRITICAL",
	"<3> ERROR",
	"<4> WARNING",
	"<5> NOTICE",
	"<6> INFO",
	"<7> DEBUG"
};

static const char *prefixes_colorized[] = {
	"<0> " COLOR_EMERGENCY	"EMERGENCY"	COLOR_DEFAULT,
	"<1> " COLOR_ALERT	"ALERT"		COLOR_DEFAULT,
	"<2> " COLOR_CRITICAL	"CRITICAL"	COLOR_DEFAULT,
	"<3> " COLOR_ERROR	"ERROR"		COLOR_DEFAULT,
	"<4> " COLOR_WARNING	"WARNING"	COLOR_DEFAULT,
	"<5> " COLOR_NOTICE	"NOTICE"	COLOR_DEFAULT,
	"<6> " COLOR_INFO	"INFO"		COLOR_DEFAULT,
	"<7> " COLOR_DEBUG	"DEBUG"		COLOR_DEFAULT
};

static int colorize = 0;

static int tty;

static const char chars[] = { '\n', '?', ':', ' ', '[', ',', ']' };

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

/** Instrumented function _vverbose_(int,const char*,int,const char*,const char*,va_list) */
static void _vverbose_(int loglevel, const char *file, int line, const char *function, const char *fmt, va_list args)
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");AKA_fCall++;
		AKA_mark("lis===156###sois===3699###eois===3717###lif===2###soif===121###eoif===139###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");char buffer[4000];

		AKA_mark("lis===157###sois===3719###eois===3733###lif===3###soif===141###eoif===155###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");char lino[40];

		AKA_mark("lis===158###sois===3735###eois===3753###lif===4###soif===157###eoif===175###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");int saverr, n, rc;

		AKA_mark("lis===159###sois===3755###eois===3776###lif===5###soif===177###eoif===198###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");struct iovec iov[20];


	/* save errno */
		AKA_mark("lis===162###sois===3797###eois===3812###lif===8###soif===219###eoif===234###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");saverr = errno;


	/* check if tty (2) or not (1) */
		if (AKA_mark("lis===165###sois===3854###eois===3858###lif===11###soif===276###eoif===280###ifc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)") && (AKA_mark("lis===165###sois===3854###eois===3858###lif===11###soif===276###eoif===280###isc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)")&&!tty)) {
		AKA_mark("lis===166###sois===3862###eois===3894###lif===12###soif===284###eoif===316###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");tty = 1 + isatty(STDERR_FILENO);
	}
	else {AKA_mark("lis===-165-###sois===-3854-###eois===-38544-###lif===-11-###soif===-###eoif===-280-###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");}


	/* prefix */
		if (AKA_mark("lis===169###sois===3915###eois===3923###lif===15###soif===337###eoif===345###ifc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)") && (AKA_mark("lis===169###sois===3915###eois===3923###lif===15###soif===337###eoif===345###isc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)")&&colorize)) {
		AKA_mark("lis===170###sois===3927###eois===4016###lif===16###soif===349###eoif===438###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[0].iov_base = (void*)prefixes_colorized[CROP_LOGLEVEL(loglevel)] + (tty - 1 ? 4 : 0);
	}
	else {
		AKA_mark("lis===172###sois===4025###eois===4104###lif===18###soif===447###eoif===526###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[0].iov_base = (void*)prefixes[CROP_LOGLEVEL(loglevel)] + (tty - 1 ? 4 : 0);
	}

		AKA_mark("lis===173###sois===4106###eois===4147###lif===19###soif===528###eoif===569###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[0].iov_len = strlen(iov[0].iov_base);


	/* " " */
		AKA_mark("lis===176###sois===4161###eois===4196###lif===22###soif===583###eoif===618###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[1].iov_base = (void*)&chars[2];

		AKA_mark("lis===177###sois===4198###eois===4217###lif===23###soif===620###eoif===639###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[1].iov_le	AKA_mark("lis===179###sois===4220###eois===4226###lif===25###soif===642###eoif===648###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");n = 2;



	n = 2;
		if (AKA_mark("lis===180###sois===4232###eois===4235###lif===26###soif===654###eoif===657###ifc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)") && (AKA_mark("lis===180###sois===4232###eois===4235###lif===26###soif===654###eoif===657###isc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)")&&fmt)) {
				AKA_mark("lis===181###sois===4241###eois===4266###lif===27###soif===663###eoif===688###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n].iov_base = buffer;

				AKA_mark("lis===182###sois===4269###eois===4284###lif===28###soif===691###eoif===706###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");errno = saverr;

				AKA_mark("lis===183###sois===4287###eois===4336###lif===29###soif===709###eoif===758###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");rc = vsnprintf(buffer, sizeof buffer, fmt, args);

				if (AKA_mark("lis===184###sois===4343###eois===4349###lif===30###soif===765###eoif===771###ifc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)") && (AKA_mark("lis===184###sois===4343###eois===4349###lif===30###soif===765###eoif===771###isc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)")&&rc < 0)) {
			AKA_mark("lis===185###sois===4354###eois===4361###lif===31###soif===776###eoif===783###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");rc = 0;
		}
		else {
			if (AKA_mark("lis===186###sois===4373###eois===4399###lif===32###soif===795###eoif===821###ifc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)") && (AKA_mark("lis===186###sois===4373###eois===4399###lif===32###soif===795###eoif===821###isc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)")&&(size_t)rc > sizeof buffer)) {
			/* if too long, ellipsis the end with ... */
							AKA_mark("lis===188###sois===4454###eois===4478###lif===34###soif===876###eoif===900###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");rc = (int)sizeof buffer;

							AKA_mark("lis===189###sois===4482###eois===4538###lif===35###soif===904###eoif===960###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");buffer[rc - 1] = buffer[rc - 2]  = buffer[rc - 3] = '.';

		}
			else {AKA_mark("lis===-186-###sois===-4373-###eois===-437326-###lif===-32-###soif===-###eoif===-821-###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");}
		}

				AKA_mark("lis===191###sois===4545###eois===4575###lif===37###soif===967###eoif===997###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n++].iov_len = (size_t)rc;

	}
	else {AKA_mark("lis===-180-###sois===-4232-###eois===-42323-###lif===-26-###soif===-###eoif===-657-###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");}

		if (AKA_mark("lis===193###sois===4584###eois===4643###lif===39###soif===1006###eoif===1065###ifc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)") && ((AKA_mark("lis===193###sois===4584###eois===4588###lif===39###soif===1006###eoif===1010###isc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)")&&file)	&&(((AKA_mark("lis===193###sois===4593###eois===4597###lif===39###soif===1015###eoif===1019###isc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)")&&!fmt)	||(AKA_mark("lis===193###sois===4601###eois===4609###lif===39###soif===1023###eoif===1031###isc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)")&&tty == 1))	||(AKA_mark("lis===193###sois===4613###eois===4642###lif===39###soif===1035###eoif===1064###isc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)")&&loglevel <= Log_Level_Warning)))) {

				if (AKA_mark("lis===195###sois===4654###eois===4662###lif===41###soif===1076###eoif===1084###ifc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)") && (AKA_mark("lis===195###sois===4654###eois===4662###lif===41###soif===1076###eoif===1084###isc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)")&&colorize)) {
						AKA_mark("lis===197###sois===4671###eois===4707###lif===43###soif===1093###eoif===1129###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n].iov_base = (void*)COLOR_FILE;

						AKA_mark("lis===198###sois===4711###eois===4749###lif===44###soif===1133###eoif===1171###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n++].iov_len = strlen(COLOR_FILE);

		}
		else {AKA_mark("lis===-195-###sois===-4654-###eois===-46548-###lif===-41-###soif===-###eoif===-1084-###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");}


		/* "[" (!fmt) or " [" (fmt) */
				AKA_mark("lis===202###sois===4790###eois===4832###lif===48###soif===1212###eoif===1254###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n].iov_base = (void*)&chars[3 + !fmt];

				AKA_mark("lis===203###sois===4835###eois===4863###lif===49###soif===1257###eoif===1285###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n++].iov_len = 2 - !fmt;

		/* file */
				AKA_mark("lis===205###sois===4879###eois===4909###lif===51###soif===1301###eoif===1331###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n].iov_base = (void*)file;

				AKA_mark("lis===206###sois===4912###eois===4944###lif===52###soif===1334###eoif===1366###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n++].iov_len = strlen(file);

		/* ":" */
				AKA_mark("lis===208###sois===4959###eois===4994###lif===54###soif===1381###eoif===1416###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n].iov_base = (void*)&chars[2];

				AKA_mark("lis===209###sois===4997###eois===5018###lif===55###soif===1419###eoif===1440###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n++].iov_len = 1;

				if (AKA_mark("lis===210###sois===5025###eois===5029###lif===56###soif===1447###eoif===1451###ifc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)") && (AKA_mark("lis===210###sois===5025###eois===5029###lif===56###soif===1447###eoif===1451###isc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)")&&line)) {
			/* line number */
						AKA_mark("lis===212###sois===5057###eois===5080###lif===58###soif===1479###eoif===1502###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n].iov_base = lino;

						AKA_mark("lis===213###sois===5084###eois===5143###lif===59###soif===1506###eoif===1565###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n++].iov_len = snprintf(lino, sizeof lino, "%d", line);

		}
		else {
			/* "?" */
						AKA_mark("lis===216###sois===5171###eois===5206###lif===62###soif===1593###eoif===1628###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n].iov_base = (void*)&chars[1];

						AKA_mark("lis===217###sois===5210###eois===5231###lif===63###soif===1632###eoif===1653###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n++].iov_len = 1;

		}

		/* "," */
				AKA_mark("lis===220###sois===5250###eois===5285###lif===66###soif===1672###eoif===1707###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n].iov_base = (void*)&chars[5];

				AKA_mark("lis===221###sois===5288###eois===5309###lif===67###soif===1710###eoif===1731###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n++].iov_len = 1;

				if (AKA_mark("lis===222###sois===5316###eois===5324###lif===68###soif===1738###eoif===1746###ifc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)") && (AKA_mark("lis===222###sois===5316###eois===5324###lif===68###soif===1738###eoif===1746###isc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)")&&function)) {
			/* function name */
						AKA_mark("lis===224###sois===5354###eois===5388###lif===70###soif===1776###eoif===1810###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n].iov_base = (void*)function;

						AKA_mark("lis===225###sois===5392###eois===5428###lif===71###soif===1814###eoif===1850###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n++].iov_len = strlen(function);

		}
		else {
			/* "?" */
						AKA_mark("lis===228###sois===5456###eois===5491###lif===74###soif===1878###eoif===1913###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n].iov_base = (void*)&chars[1];

						AKA_mark("lis===229###sois===5495###eois===5516###lif===75###soif===1917###eoif===1938###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n++].iov_len = 1;

		}

				AKA_mark("lis===231###sois===5523###eois===5558###lif===77###soif===1945###eoif===1980###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n].iov_base = (void*)&chars[6];

				AKA_mark("lis===232###sois===5561###eois===5582###lif===78###soif===1983###eoif===2004###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n++].iov_len = 1;


				if (AKA_mark("lis===234###sois===5590###eois===5598###lif===80###soif===2012###eoif===2020###ifc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)") && (AKA_mark("lis===234###sois===5590###eois===5598###lif===80###soif===2012###eoif===2020###isc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)")&&colorize)) {
						AKA_mark("lis===236###sois===5607###eois===5646###lif===82###soif===2029###eoif===2068###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n].iov_base = (void*)COLOR_DEFAULT;

						AKA_mark("lis===237###sois===5650###eois===5691###lif===83###soif===2072###eoif===2113###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n++].iov_len = strlen(COLOR_DEFAULT);

		}
		else {AKA_mark("lis===-234-###sois===-5590-###eois===-55908-###lif===-80-###soif===-###eoif===-2020-###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");}

	}
	else {AKA_mark("lis===-193-###sois===-4584-###eois===-458459-###lif===-39-###soif===-###eoif===-1065-###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");}

		if (AKA_mark("lis===240###sois===5704###eois===5710###lif===86###soif===2126###eoif===2132###ifc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)") && (AKA_mark("lis===240###sois===5704###eois===5710###lif===86###soif===2126###eoif===2132###isc===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)")&&n == 2)) {
		/* "?" */
				AKA_mark("lis===242###sois===5728###eois===5763###lif===88###soif===2150###eoif===2185###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n].iov_base = (void*)&chars[1];

				AKA_mark("lis===243###sois===5766###eois===5787###lif===89###soif===2188###eoif===2209###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n++].iov_len = 1;

	}
	else {AKA_mark("lis===-240-###sois===-5704-###eois===-57046-###lif===-86-###soif===-###eoif===-2132-###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");}

	/* "\n" */
		AKA_mark("lis===246###sois===5804###eois===5839###lif===92###soif===2226###eoif===2261###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n].iov_base = (void*)&chars[0];

		AKA_mark("lis===247###sois===5841###eois===5862###lif===93###soif===2263###eoif===2284###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");iov[n++].iov_len = 1;


	/* emit the message */
		AKA_mark("lis===250###sois===5889###eois===5916###lif===96###soif===2311###eoif===2338###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");pthread_mutex_lock(&mutex);

		AKA_mark("lis===251###sois===5918###eois===5948###lif===97###soif===2340###eoif===2370###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");writev(STDERR_FILENO, iov, n);

		AKA_mark("lis===252###sois===5950###eois===5979###lif===98###soif===2372###eoif===2401###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");pthread_mutex_unlock(&mutex);


	/* restore errno */
		AKA_mark("lis===255###sois===6003###eois===6018###lif===101###soif===2425###eoif===2440###ins===true###function===./app-framework-binder/src/verbose.c/_vverbose_(int,const char*,int,const char*,const char*,va_list)");errno = saverr;

}

/** Instrumented function verbose_set_name(const char*,int) */
void verbose_set_name(const char *name, int authority)
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbose_set_name(const char*,int)");AKA_fCall++;
		AKA_mark("lis===260###sois===6080###eois===6095###lif===2###soif===58###eoif===73###ins===true###function===./app-framework-binder/src/verbose.c/verbose_set_name(const char*,int)");appname = name;

		AKA_mark("lis===261###sois===6097###eois===6122###lif===3###soif===75###eoif===100###ins===true###function===./app-framework-binder/src/verbose.c/verbose_set_name(const char*,int)");appauthority = authority;

}

#endif

/** Instrumented function vverbose(int,const char*,int,const char*,const char*,va_list) */
void vverbose(int loglevel, const char *file, int line, const char *function, const char *fmt, va_list args)
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/vverbose(int,const char*,int,const char*,const char*,va_list)");AKA_fCall++;
		AKA_mark("lis===268###sois===6246###eois===6377###lif===2###soif===112###eoif===243###ins===true###function===./app-framework-binder/src/verbose.c/vverbose(int,const char*,int,const char*,const char*,va_list)");void (*observer)(int loglevel, const char *file, int line, const char *function, const char *fmt, va_list args) = verbose_observer;


		if (AKA_mark("lis===270###sois===6384###eois===6393###lif===4###soif===250###eoif===259###ifc===true###function===./app-framework-binder/src/verbose.c/vverbose(int,const char*,int,const char*,const char*,va_list)") && (AKA_mark("lis===270###sois===6384###eois===6393###lif===4###soif===250###eoif===259###isc===true###function===./app-framework-binder/src/verbose.c/vverbose(int,const char*,int,const char*,const char*,va_list)")&&!observer)) {
		AKA_mark("lis===271###sois===6397###eois===6451###lif===5###soif===263###eoif===317###ins===true###function===./app-framework-binder/src/verbose.c/vverbose(int,const char*,int,const char*,const char*,va_list)");_vverbose_(loglevel, file, line, function, fmt, args);
	}
	else {
				AKA_mark("lis===273###sois===6462###eois===6473###lif===7###soif===328###eoif===339###ins===true###function===./app-framework-binder/src/verbose.c/vverbose(int,const char*,int,const char*,const char*,va_list)");va_list ap;

				AKA_mark("lis===274###sois===6476###eois===6494###lif===8###soif===342###eoif===360###ins===true###function===./app-framework-binder/src/verbose.c/vverbose(int,const char*,int,const char*,const char*,va_list)");va_copy(ap, args);

				AKA_mark("lis===275###sois===6497###eois===6551###lif===9###soif===363###eoif===417###ins===true###function===./app-framework-binder/src/verbose.c/vverbose(int,const char*,int,const char*,const char*,va_list)");_vverbose_(loglevel, file, line, function, fmt, args);

				AKA_mark("lis===276###sois===6554###eois===6604###lif===10###soif===420###eoif===470###ins===true###function===./app-framework-binder/src/verbose.c/vverbose(int,const char*,int,const char*,const char*,va_list)");observer(loglevel, file, line, function, fmt, ap);

				AKA_mark("lis===277###sois===6607###eois===6618###lif===11###soif===473###eoif===484###ins===true###function===./app-framework-binder/src/verbose.c/vverbose(int,const char*,int,const char*,const char*,va_list)");va_end(ap);

	}

}

/** Instrumented function verbose(int,const char*,int,const char*,const char*) */
void verbose(int loglevel, const char *file, int line, const char *function, const char *fmt, ...)
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbose(int,const char*,int,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===283###sois===6727###eois===6738###lif===2###soif===102###eoif===113###ins===true###function===./app-framework-binder/src/verbose.c/verbose(int,const char*,int,const char*,const char*)");va_list ap;


		AKA_mark("lis===285###sois===6741###eois===6759###lif===4###soif===116###eoif===134###ins===true###function===./app-framework-binder/src/verbose.c/verbose(int,const char*,int,const char*,const char*)");va_start(ap, fmt);

		AKA_mark("lis===286###sois===6761###eois===6811###lif===5###soif===136###eoif===186###ins===true###function===./app-framework-binder/src/verbose.c/verbose(int,const char*,int,const char*,const char*)");vverbose(loglevel, file, line, function, fmt, ap);

		AKA_mark("lis===287###sois===6813###eois===6824###lif===6###soif===188###eoif===199###ins===true###function===./app-framework-binder/src/verbose.c/verbose(int,const char*,int,const char*,const char*)");va_end(ap);

}

/** Instrumented function set_logmask(int) */
void set_logmask(int lvl)
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/set_logmask(int)");AKA_fCall++;
		AKA_mark("lis===292###sois===6857###eois===6889###lif===2###soif===29###eoif===61###ins===true###function===./app-framework-binder/src/verbose.c/set_logmask(int)");logmask = lvl | MINIMAL_LOGMASK;

}

/** Instrumented function verbose_add(int) */
void verbose_add(int level)
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbose_add(int)");AKA_fCall++;
		AKA_mark("lis===297###sois===6924###eois===6961###lif===2###soif===31###eoif===68###ins===true###function===./app-framework-binder/src/verbose.c/verbose_add(int)");set_logmask(logmask | MASKOF(level));

}

/** Instrumented function verbose_sub(int) */
void verbose_sub(int level)
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbose_sub(int)");AKA_fCall++;
		AKA_mark("lis===302###sois===6996###eois===7034###lif===2###soif===31###eoif===69###ins===true###function===./app-framework-binder/src/verbose.c/verbose_sub(int)");set_logmask(logmask & ~MASKOF(level));

}

/** Instrumented function verbose_clear() */
void verbose_clear()
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbose_clear()");AKA_fCall++;
		AKA_mark("lis===307###sois===7062###eois===7077###lif===2###soif===24###eoif===39###ins===true###function===./app-framework-binder/src/verbose.c/verbose_clear()");set_logmask(0);

}

/** Instrumented function verbose_dec() */
void verbose_dec()
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbose_dec()");AKA_fCall++;
		AKA_mark("lis===312###sois===7103###eois===7138###lif===2###soif===22###eoif===57###ins===true###function===./app-framework-binder/src/verbose.c/verbose_dec()");verbosity_set(verbosity_get() - 1);

}

/** Instrumented function verbose_inc() */
void verbose_inc()
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbose_inc()");AKA_fCall++;
		AKA_mark("lis===317###sois===7164###eois===7199###lif===2###soif===22###eoif===57###ins===true###function===./app-framework-binder/src/verbose.c/verbose_inc()");verbosity_set(verbosity_get() + 1);

}

/** Instrumented function verbosity_to_mask(int) */
int verbosity_to_mask(int verbo)
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbosity_to_mask(int)");AKA_fCall++;
		AKA_mark("lis===322###sois===7239###eois===7271###lif===2###soif===36###eoif===68###ins===true###function===./app-framework-binder/src/verbose.c/verbosity_to_mask(int)");int x = verbo + Log_Level_Error;

		AKA_mark("lis===323###sois===7273###eois===7298###lif===3###soif===70###eoif===95###ins===true###function===./app-framework-binder/src/verbose.c/verbosity_to_mask(int)");int l = CROP_LOGLEVEL(x);

		AKA_mark("lis===324###sois===7300###eois===7326###lif===4###soif===97###eoif===123###ins===true###function===./app-framework-binder/src/verbose.c/verbosity_to_mask(int)");return (1 << (l + 1)) - 1;

}

/** Instrumented function verbosity_from_mask(int) */
int verbosity_from_mask(int mask)
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbosity_from_mask(int)");AKA_fCall++;
		AKA_mark("lis===329###sois===7367###eois===7377###lif===2###soif===37###eoif===47###ins===true###function===./app-framework-binder/src/verbose.c/verbosity_from_mask(int)");int v = 0;

		while (AKA_mark("lis===330###sois===7386###eois===7413###lif===3###soif===56###eoif===83###ifc===true###function===./app-framework-binder/src/verbose.c/verbosity_from_mask(int)") && (AKA_mark("lis===330###sois===7386###eois===7413###lif===3###soif===56###eoif===83###isc===true###function===./app-framework-binder/src/verbose.c/verbosity_from_mask(int)")&&mask > verbosity_to_mask(v))) {
		AKA_mark("lis===331###sois===7417###eois===7421###lif===4###soif===87###eoif===91###ins===true###function===./app-framework-binder/src/verbose.c/verbosity_from_mask(int)");v++;
	}

		AKA_mark("lis===332###sois===7423###eois===7432###lif===5###soif===93###eoif===102###ins===true###function===./app-framework-binder/src/verbose.c/verbosity_from_mask(int)");return v;

}

/** Instrumented function verbosity_set(int) */
void verbosity_set(int verbo)
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbosity_set(int)");AKA_fCall++;
		AKA_mark("lis===337###sois===7469###eois===7507###lif===2###soif===33###eoif===71###ins===true###function===./app-framework-binder/src/verbose.c/verbosity_set(int)");set_logmask(verbosity_to_mask(verbo));

}

/** Instrumented function verbosity_get() */
int verbosity_get()
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbosity_get()");AKA_fCall++;
		AKA_mark("lis===342###sois===7534###eois===7570###lif===2###soif===23###eoif===59###ins===true###function===./app-framework-binder/src/verbose.c/verbosity_get()");return verbosity_from_mask(logmask);

}

/** Instrumented function verbose_level_of_name(const char*) */
int verbose_level_of_name(const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)");AKA_fCall++;
		AKA_mark("lis===347###sois===7621###eois===7675###lif===2###soif===47###eoif===101###ins===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)");int c, i, r, l = 0, u = sizeof names / sizeof * names;

		while (AKA_mark("lis===348###sois===7684###eois===7689###lif===3###soif===110###eoif===115###ifc===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)") && (AKA_mark("lis===348###sois===7684###eois===7689###lif===3###soif===110###eoif===115###isc===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)")&&l < u)) {
				AKA_mark("lis===349###sois===7695###eois===7712###lif===4###soif===121###eoif===138###ins===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)");i = (l + u) >> 1;

				AKA_mark("lis===350###sois===7715###eois===7733###lif===5###soif===141###eoif===159###ins===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)");r = (int)asort[i];

				AKA_mark("lis===351###sois===7736###eois===7767###lif===6###soif===162###eoif===193###ins===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)");c = strcasecmp(names[r], name);

				if (AKA_mark("lis===352###sois===7774###eois===7776###lif===7###soif===200###eoif===202###ifc===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)") && (AKA_mark("lis===352###sois===7774###eois===7776###lif===7###soif===200###eoif===202###isc===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)")&&!c)) {
			AKA_mark("lis===353###sois===7781###eois===7790###lif===8###soif===207###eoif===216###ins===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)");return r;
		}
		else {AKA_mark("lis===-352-###sois===-7774-###eois===-77742-###lif===-7-###soif===-###eoif===-202-###ins===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)");}

				if (AKA_mark("lis===354###sois===7797###eois===7802###lif===9###soif===223###eoif===228###ifc===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)") && (AKA_mark("lis===354###sois===7797###eois===7802###lif===9###soif===223###eoif===228###isc===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)")&&c < 0)) {
			AKA_mark("lis===355###sois===7807###eois===7817###lif===10###soif===233###eoif===243###ins===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)");l = i + 1;
		}
		else {
			AKA_mark("lis===357###sois===7828###eois===7834###lif===12###soif===254###eoif===260###ins===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)");u = i;
		}

	}

		AKA_mark("lis===359###sois===7839###eois===7849###lif===14###soif===265###eoif===275###ins===true###function===./app-framework-binder/src/verbose.c/verbose_level_of_name(const char*)");return -1;

}

/** Instrumented function verbose_name_of_level(int) */
const char *verbose_name_of_level(int level)
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbose_name_of_level(int)");AKA_fCall++;
		AKA_mark("lis===364###sois===7901###eois===7960###lif===2###soif===48###eoif===107###ins===true###function===./app-framework-binder/src/verbose.c/verbose_name_of_level(int)");return level == CROP_LOGLEVEL(level) ? names[level] : NULL;

}

/** Instrumented function verbose_colorize() */
void verbose_colorize()
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbose_colorize()");AKA_fCall++;
		AKA_mark("lis===369###sois===7991###eois===8004###lif===2###soif===27###eoif===40###ins===true###function===./app-framework-binder/src/verbose.c/verbose_colorize()");colorize = 1;

}

/** Instrumented function verbose_is_colorized() */
int verbose_is_colorized()
{AKA_mark("Calling: ./app-framework-binder/src/verbose.c/verbose_is_colorized()");AKA_fCall++;
		AKA_mark("lis===374###sois===8038###eois===8054###lif===2###soif===30###eoif===46###ins===true###function===./app-framework-binder/src/verbose.c/verbose_is_colorized()");return colorize;

}

#endif

