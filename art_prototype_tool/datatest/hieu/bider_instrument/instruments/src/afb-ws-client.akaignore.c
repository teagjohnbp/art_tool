/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_WS_CLIENT_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_WS_CLIENT_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_WSJ1_H_
#define AKA_INCLUDE__AFB_WSJ1_H_
#include "afb-wsj1.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_SYSTEMD_H_
#define AKA_INCLUDE__FDEV_SYSTEMD_H_
#include "fdev-systemd.akaignore.h"
#endif


/**************** WebSocket handshake ****************************/

static const char *compkeys[32] = {
	"lYKr2sn9+ILcLpkqdrE2VQ==", "G5J7ncQnmS/MubIYcqKWM+E6k8I=",
	"gjN6eOU/6Yy7dBTJ+EaQSw==", "P5QzN7mRt4DeRWxKdG7s4/NCEwk=",
	"ziLin6OQ0/a1+cGaI9Mupg==", "yvpxcFJAGam6huL77vz34CdShyU=",
	"KMfd2bHKah0U5mk2Kg/LIg==", "lyYxfDP5YunhkBF+nAWb/w6K4yg=",
	"fQ/ISF1mNCPRMyAj3ucqNg==", "91YY1EUelb4eMU24Z8WHhJ9cHmc=",
	"RHlfiVVE1lM1AJnErI8dFg==", "UdZQc0JaihQJV5ETCZ84Av88pxQ=",
	"NVy3L2ujXN7v3KEJwK92ww==", "+dE7iITxhExjBtf06VYNWChHqx8=",
	"cCNAgttlgELfbDDIfhujww==", "W2JiswqbTAXx5u84EtjbtqAW2Bg=",
	"K+oQvEDWJP+WXzRS5BJDFw==", "szgW10a9AuD+HtfS4ylaqWfzWAs=",
	"nmg43S4DpVaxye+oQv9KTw==", "8XK74jB9xFfTzzl0wTqW04k3tPE=",
	"LIqZ23sEppbF4YJR9LQ4/w==", "f8lJBQEbR8QmmvPHZpA0smlIeeA=",
	"WY1vvvY2j/3V9DAGW3ZZcA==", "lROlE4vL4cjU1Vnk6rISc9gVKN0=",
	"Ia+dgHnA9QaBrbxuqh4wgQ==", "GiGjxFdSaF0EGTl2cjvFsVmJnfM=",
	"MfpIVG082jFTV7SxTNNijQ==", "f5I2h53hBsT5ES3EHhnxAJ2nqsw=",
	"kFumnAw5d/WctG0yAUHPiQ==", "aQQmOjoABl7mrbliTPS1bOkndOs=",
	"MHiEc+Qc8w/SJ3zMHEM8pA==", "FVCxLBmoil3gY0jSX3aNJ6kR/t4="
};

/* get randomly a pair of key/accept value */
/** Instrumented function getkeypair(const char**,const char**) */
static void getkeypair(const char **key, const char **ack)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-client.c/getkeypair(const char**,const char**)");AKA_fCall++;
		AKA_mark("lis===58###sois===2110###eois===2116###lif===2###soif===62###eoif===68###ins===true###function===./app-framework-binder/src/afb-ws-client.c/getkeypair(const char**,const char**)");int r;

		AKA_mark("lis===59###sois===2118###eois===2129###lif===3###soif===70###eoif===81###ins===true###function===./app-framework-binder/src/afb-ws-client.c/getkeypair(const char**,const char**)");r = rand();

		while (AKA_mark("lis===60###sois===2138###eois===2144###lif===4###soif===90###eoif===96###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/getkeypair(const char**,const char**)") && (AKA_mark("lis===60###sois===2138###eois===2144###lif===4###soif===90###eoif===96###isc===true###function===./app-framework-binder/src/afb-ws-client.c/getkeypair(const char**,const char**)")&&r > 15)) {
		AKA_mark("lis===61###sois===2148###eois===2172###lif===5###soif===100###eoif===124###ins===true###function===./app-framework-binder/src/afb-ws-client.c/getkeypair(const char**,const char**)");r = (r & 15) + (r >> 4);
	}

		AKA_mark("lis===62###sois===2174###eois===2192###lif===6###soif===126###eoif===144###ins===true###function===./app-framework-binder/src/afb-ws-client.c/getkeypair(const char**,const char**)");r = (r & 15) << 1;

		AKA_mark("lis===63###sois===2194###eois===2213###lif===7###soif===146###eoif===165###ins===true###function===./app-framework-binder/src/afb-ws-client.c/getkeypair(const char**,const char**)");*key = compkeys[r];

		AKA_mark("lis===64###sois===2215###eois===2236###lif===8###soif===167###eoif===188###ins===true###function===./app-framework-binder/src/afb-ws-client.c/getkeypair(const char**,const char**)");*ack = compkeys[r+1];

}

/* joins the strings using the separator */
/** Instrumented function strjoin(int,const char**,const char*) */
static char *strjoin(int count, const char **strings, const char *separ)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");AKA_fCall++;
		AKA_mark("lis===70###sois===2360###eois===2380###lif===2###soif===76###eoif===96###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");char *result, *iter;

		AKA_mark("lis===71###sois===2382###eois===2396###lif===3###soif===98###eoif===112###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");size_t length;

		AKA_mark("lis===72###sois===2398###eois===2406###lif===4###soif===114###eoif===122###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");int idx;


	/* creates the count if needed */
		if (AKA_mark("lis===75###sois===2448###eois===2457###lif===7###soif===164###eoif===173###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)") && (AKA_mark("lis===75###sois===2448###eois===2457###lif===7###soif===164###eoif===173###isc===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)")&&count < 0)) {
		AKA_mark("lis===76###sois===2465###eois===2476###lif===8###soif===181###eoif===192###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");for (count = 0 ;AKA_mark("lis===76###sois===2477###eois===2499###lif===8###soif===193###eoif===215###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)") && AKA_mark("lis===76###sois===2477###eois===2499###lif===8###soif===193###eoif===215###isc===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)")&&strings[count] != NULL;({AKA_mark("lis===76###sois===2502###eois===2509###lif===8###soif===218###eoif===225###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");count++;})) {
			;
		}
	}
	else {AKA_mark("lis===-75-###sois===-2448-###eois===-24489-###lif===-7-###soif===-###eoif===-173-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");}


	/* compute the length of the result */
		if (AKA_mark("lis===79###sois===2558###eois===2568###lif===11###soif===274###eoif===284###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)") && (AKA_mark("lis===79###sois===2558###eois===2568###lif===11###soif===274###eoif===284###isc===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)")&&count == 0)) {
		AKA_mark("lis===80###sois===2572###eois===2583###lif===12###soif===288###eoif===299###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");length = 0;
	}
	else {
				AKA_mark("lis===82###sois===2594###eois===2641###lif===14###soif===310###eoif===357###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");length = (unsigned)(count - 1) * strlen(separ);

				AKA_mark("lis===83###sois===2649###eois===2658###lif===15###soif===365###eoif===374###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");for (idx = 0 ;AKA_mark("lis===83###sois===2659###eois===2670###lif===15###soif===375###eoif===386###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)") && AKA_mark("lis===83###sois===2659###eois===2670###lif===15###soif===375###eoif===386###isc===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)")&&idx < count;({AKA_mark("lis===83###sois===2673###eois===2679###lif===15###soif===389###eoif===395###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");idx ++;})) {
			AKA_mark("lis===84###sois===2684###eois===2715###lif===16###soif===400###eoif===431###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");length += strlen(strings[idx]);
		}

	}


	/* allocates the result */
		AKA_mark("lis===88###sois===2749###eois===2777###lif===20###soif===465###eoif===493###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");result = malloc(length + 1);

		if (AKA_mark("lis===89###sois===2783###eois===2797###lif===21###soif===499###eoif===513###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)") && (AKA_mark("lis===89###sois===2783###eois===2797###lif===21###soif===499###eoif===513###isc===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)")&&result == NULL)) {
		AKA_mark("lis===90###sois===2801###eois===2816###lif===22###soif===517###eoif===532###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");errno = ENOMEM;
	}
	else {
		/* create the result */
				if (AKA_mark("lis===93###sois===2857###eois===2867###lif===25###soif===573###eoif===583###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)") && (AKA_mark("lis===93###sois===2857###eois===2867###lif===25###soif===573###eoif===583###isc===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)")&&count != 0)) {
						AKA_mark("lis===94###sois===2874###eois===2914###lif===26###soif===590###eoif===630###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");iter = stpcpy(result, strings[idx = 0]);

						while (AKA_mark("lis===95###sois===2925###eois===2938###lif===27###soif===641###eoif===654###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)") && (AKA_mark("lis===95###sois===2925###eois===2938###lif===27###soif===641###eoif===654###isc===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)")&&++idx < count)) {
				AKA_mark("lis===96###sois===2944###eois===2993###lif===28###soif===660###eoif===709###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");iter = stpcpy(stpcpy(iter, separ), strings[idx]);
			}

			// assert(iter - result == length);
		}
		else {AKA_mark("lis===-93-###sois===-2857-###eois===-285710-###lif===-25-###soif===-###eoif===-583-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");}

				AKA_mark("lis===99###sois===3039###eois===3058###lif===31###soif===755###eoif===774###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");result[length] = 0;

	}

		AKA_mark("lis===101###sois===3063###eois===3077###lif===33###soif===779###eoif===793###ins===true###function===./app-framework-binder/src/afb-ws-client.c/strjoin(int,const char**,const char*)");return result;

}

/* creates the http message for the request */
/** Instrumented function make_request(char**,const char*,const char*,const char*,const char*) */
static int make_request(char **request, const char *path, const char *host, const char *key, const char *protocols)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-client.c/make_request(char**,const char*,const char*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===107###sois===3247###eois===3561###lif===2###soif===119###eoif===433###ins===true###function===./app-framework-binder/src/afb-ws-client.c/make_request(char**,const char*,const char*,const char*,const char*)");int rc = asprintf(request,
			"GET %s HTTP/1.1\r\n"
			"Host: %s\r\n"
			"Upgrade: websocket\r\n"
			"Connection: Upgrade\r\n"
			"Sec-WebSocket-Version: 13\r\n"
			"Sec-WebSocket-Key: %s\r\n"
			"Sec-WebSocket-Protocol: %s\r\n"
			"Content-Length: 0\r\n"
			"\r\n"
			, path
			, host
			, key
			, protocols
		);

		if (AKA_mark("lis===122###sois===3567###eois===3573###lif===17###soif===439###eoif===445###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/make_request(char**,const char*,const char*,const char*,const char*)") && (AKA_mark("lis===122###sois===3567###eois===3573###lif===17###soif===439###eoif===445###isc===true###function===./app-framework-binder/src/afb-ws-client.c/make_request(char**,const char*,const char*,const char*,const char*)")&&rc < 0)) {
				AKA_mark("lis===123###sois===3579###eois===3594###lif===18###soif===451###eoif===466###ins===true###function===./app-framework-binder/src/afb-ws-client.c/make_request(char**,const char*,const char*,const char*,const char*)");errno = ENOMEM;

				AKA_mark("lis===124###sois===3597###eois===3613###lif===19###soif===469###eoif===485###ins===true###function===./app-framework-binder/src/afb-ws-client.c/make_request(char**,const char*,const char*,const char*,const char*)");*request = NULL;

				AKA_mark("lis===125###sois===3616###eois===3626###lif===20###soif===488###eoif===498###ins===true###function===./app-framework-binder/src/afb-ws-client.c/make_request(char**,const char*,const char*,const char*,const char*)");return -1;

	}
	else {AKA_mark("lis===-122-###sois===-3567-###eois===-35676-###lif===-17-###soif===-###eoif===-445-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/make_request(char**,const char*,const char*,const char*,const char*)");}

		AKA_mark("lis===127###sois===3631###eois===3641###lif===22###soif===503###eoif===513###ins===true###function===./app-framework-binder/src/afb-ws-client.c/make_request(char**,const char*,const char*,const char*,const char*)");return rc;

}

/* create the request and send it to fd, returns the expected accept string */
/** Instrumented function send_request(int,const char**,const char*,const char*) */
static const char *send_request(int fd, const char **protocols, const char *path, const char *host)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===133###sois===3827###eois===3849###lif===2###soif===103###eoif===125###ins===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");const char *key, *ack;

		AKA_mark("lis===134###sois===3851###eois===3877###lif===3###soif===127###eoif===153###ins===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");char *protolist, *request;

		AKA_mark("lis===135###sois===3879###eois===3894###lif===4###soif===155###eoif===170###ins===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");int length, rc;


	/* make the list of accepted protocols */
		AKA_mark("lis===138###sois===3940###eois===3981###lif===7###soif===216###eoif===257###ins===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");protolist = strjoin(-1, protocols, ", ");

		if (AKA_mark("lis===139###sois===3987###eois===4004###lif===8###soif===263###eoif===280###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)") && (AKA_mark("lis===139###sois===3987###eois===4004###lif===8###soif===263###eoif===280###isc===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)")&&protolist == NULL)) {
		AKA_mark("lis===140###sois===4008###eois===4020###lif===9###soif===284###eoif===296###ins===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");return NULL;
	}
	else {AKA_mark("lis===-139-###sois===-3987-###eois===-398717-###lif===-8-###soif===-###eoif===-280-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");}


	/* create the request */
		AKA_mark("lis===143###sois===4049###eois===4072###lif===12###soif===325###eoif===348###ins===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");getkeypair(&key, &ack);

		AKA_mark("lis===144###sois===4074###eois===4134###lif===13###soif===350###eoif===410###ins===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");length = make_request(&request, path, host, key, protolist);

		AKA_mark("lis===145###sois===4136###eois===4152###lif===14###soif===412###eoif===428###ins===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");free(protolist);

		if (AKA_mark("lis===146###sois===4158###eois===4168###lif===15###soif===434###eoif===444###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)") && (AKA_mark("lis===146###sois===4158###eois===4168###lif===15###soif===434###eoif===444###isc===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)")&&length < 0)) {
		AKA_mark("lis===147###sois===4172###eois===4184###lif===16###soif===448###eoif===460###ins===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");return NULL;
	}
	else {AKA_mark("lis===-146-###sois===-4158-###eois===-415810-###lif===-15-###soif===-###eoif===-444-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");}


	/* send the request */
		do { 		AKA_mark("lis===150###sois===4216###eois===4253###lif===19###soif===492###eoif===529###ins===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");rc = (int)write(fd, request, length);
 }
	while (AKA_mark("lis===150###sois===4262###eois===4286###lif===19###soif===538###eoif===562###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)") && ((AKA_mark("lis===150###sois===4262###eois===4268###lif===19###soif===538###eoif===544###isc===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)")&&rc < 0)	&&(AKA_mark("lis===150###sois===4272###eois===4286###lif===19###soif===548###eoif===562###isc===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)")&&errno == EINTR)));

		AKA_mark("lis===151###sois===4290###eois===4304###lif===20###soif===566###eoif===580###ins===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");free(request);

		AKA_mark("lis===152###sois===4306###eois===4333###lif===21###soif===582###eoif===609###ins===true###function===./app-framework-binder/src/afb-ws-client.c/send_request(int,const char**,const char*,const char*)");return rc < 0 ? NULL : ack;

}

/* read a line not efficiently but without buffering */
/** Instrumented function receive_line(int,char*,int) */
static int receive_line(int fd, char *line, int size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)");AKA_fCall++;
		AKA_mark("lis===158###sois===4450###eois===4477###lif===2###soif===57###eoif===84###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)");int rc, length = 0, cr = 0;

		for (;;) {
				if (AKA_mark("lis===160###sois===4495###eois===4509###lif===4###soif===102###eoif===116###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)") && (AKA_mark("lis===160###sois===4495###eois===4509###lif===4###soif===102###eoif===116###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)")&&length >= size)) {
						AKA_mark("lis===161###sois===4516###eois===4530###lif===5###soif===123###eoif===137###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)");errno = EFBIG;

						AKA_mark("lis===162###sois===4534###eois===4544###lif===6###soif===141###eoif===151###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)");return -1;

		}
		else {AKA_mark("lis===-160-###sois===-4495-###eois===-449514-###lif===-4-###soif===-###eoif===-116-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)");}

				do { 			AKA_mark("lis===164###sois===4556###eois===4593###lif===8###soif===163###eoif===200###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)");rc = (int)read(fd, line + length, 1);
 }
		while (AKA_mark("lis===164###sois===4603###eois===4627###lif===8###soif===210###eoif===234###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)") && ((AKA_mark("lis===164###sois===4603###eois===4609###lif===8###soif===210###eoif===216###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)")&&rc < 0)	&&(AKA_mark("lis===164###sois===4613###eois===4627###lif===8###soif===220###eoif===234###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)")&&errno == EINTR)));

				if (AKA_mark("lis===165###sois===4636###eois===4642###lif===9###soif===243###eoif===249###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)") && (AKA_mark("lis===165###sois===4636###eois===4642###lif===9###soif===243###eoif===249###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)")&&rc < 0)) {
			AKA_mark("lis===166###sois===4647###eois===4657###lif===10###soif===254###eoif===264###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)");return -1;
		}
		else {AKA_mark("lis===-165-###sois===-4636-###eois===-46366-###lif===-9-###soif===-###eoif===-249-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)");}

				if (AKA_mark("lis===167###sois===4664###eois===4684###lif===11###soif===271###eoif===291###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)") && (AKA_mark("lis===167###sois===4664###eois===4684###lif===11###soif===271###eoif===291###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)")&&line[length] == '\r')) {
			AKA_mark("lis===168###sois===4689###eois===4696###lif===12###soif===296###eoif===303###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)");cr = 1;
		}
		else {
			if (AKA_mark("lis===169###sois===4708###eois===4739###lif===13###soif===315###eoif===346###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)") && ((AKA_mark("lis===169###sois===4708###eois===4715###lif===13###soif===315###eoif===322###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)")&&cr != 0)	&&(AKA_mark("lis===169###sois===4719###eois===4739###lif===13###soif===326###eoif===346###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)")&&line[length] == '\n'))) {
							AKA_mark("lis===170###sois===4746###eois===4765###lif===14###soif===353###eoif===372###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)");line[--length] = 0;

							AKA_mark("lis===171###sois===4769###eois===4783###lif===15###soif===376###eoif===390###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)");return length;

		}
			else {
				AKA_mark("lis===173###sois===4796###eois===4803###lif===17###soif===403###eoif===410###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)");cr = 0;
			}
		}

				AKA_mark("lis===174###sois===4806###eois===4815###lif===18###soif===413###eoif===422###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_line(int,char*,int)");length++;

	}

}

/* check a header */
/** Instrumented function isheader(const char*,size_t,const char*) */
static inline int isheader(const char *head, size_t klen, const char *key)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-client.c/isheader(const char*,size_t,const char*)");AKA_fCall++;
		AKA_mark("lis===181###sois===4921###eois===4980###lif===2###soif===78###eoif===137###ins===true###function===./app-framework-binder/src/afb-ws-client.c/isheader(const char*,size_t,const char*)");return strncasecmp(head, key, klen) == 0 && key[klen] == 0;

}

/* receives and scan the response */
/** Instrumented function receive_response(int,const char**,const char*) */
static int receive_response(int fd, const char **protocols, const char *ack)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");AKA_fCall++;
		AKA_mark("lis===187###sois===5101###eois===5122###lif===2###soif===80###eoif===101###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");char line[4096], *it;

		AKA_mark("lis===188###sois===5124###eois===5152###lif===3###soif===103###eoif===131###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");int rc, haserr, result = -1;

		AKA_mark("lis===189###sois===5154###eois===5171###lif===4###soif===133###eoif===150###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");size_t len, clen;


	/* check the header line to be something like: "HTTP/1.1 101 Switching Protocols" */
		AKA_mark("lis===192###sois===5260###eois===5307###lif===7###soif===239###eoif===286###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");rc = receive_line(fd, line, (int)sizeof(line));

		if (AKA_mark("lis===193###sois===5313###eois===5319###lif===8###soif===292###eoif===298###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && (AKA_mark("lis===193###sois===5313###eois===5319###lif===8###soif===292###eoif===298###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&rc < 0)) {
		AKA_mark("lis===194###sois===5323###eois===5334###lif===9###soif===302###eoif===313###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");goto error;
	}
	else {AKA_mark("lis===-193-###sois===-5313-###eois===-53136-###lif===-8-###soif===-###eoif===-298-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");}

		AKA_mark("lis===195###sois===5336###eois===5361###lif===10###soif===315###eoif===340###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");len = strcspn(line, " ");

		if (AKA_mark("lis===196###sois===5367###eois===5412###lif===11###soif===346###eoif===391###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && ((AKA_mark("lis===196###sois===5367###eois===5375###lif===11###soif===346###eoif===354###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&len != 8)	||(AKA_mark("lis===196###sois===5379###eois===5412###lif===11###soif===358###eoif===391###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&0 != strncmp(line, "HTTP/1.1", 8)))) {
		AKA_mark("lis===197###sois===5416###eois===5427###lif===12###soif===395###eoif===406###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");goto abort;
	}
	else {AKA_mark("lis===-196-###sois===-5367-###eois===-536745-###lif===-11-###soif===-###eoif===-391-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");}

		AKA_mark("lis===198###sois===5429###eois===5445###lif===13###soif===408###eoif===424###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");it = line + len;

		AKA_mark("lis===199###sois===5447###eois===5469###lif===14###soif===426###eoif===448###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");len = strspn(it, " ");

		if (AKA_mark("lis===200###sois===5475###eois===5483###lif===15###soif===454###eoif===462###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && (AKA_mark("lis===200###sois===5475###eois===5483###lif===15###soif===454###eoif===462###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&len == 0)) {
		AKA_mark("lis===201###sois===5487###eois===5498###lif===16###soif===466###eoif===477###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");goto abort;
	}
	else {AKA_mark("lis===-200-###sois===-5475-###eois===-54758-###lif===-15-###soif===-###eoif===-462-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");}

		AKA_mark("lis===202###sois===5500###eois===5510###lif===17###soif===479###eoif===489###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");it += len;

		AKA_mark("lis===203###sois===5512###eois===5535###lif===18###soif===491###eoif===514###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");len = strcspn(it, " ");

		if (AKA_mark("lis===204###sois===5541###eois===5579###lif===19###soif===520###eoif===558###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && ((AKA_mark("lis===204###sois===5541###eois===5549###lif===19###soif===520###eoif===528###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&len != 3)	||(AKA_mark("lis===204###sois===5553###eois===5579###lif===19###soif===532###eoif===558###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&0 != strncmp(it, "101", 3)))) {
		AKA_mark("lis===205###sois===5583###eois===5594###lif===20###soif===562###eoif===573###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");goto abort;
	}
	else {AKA_mark("lis===-204-###sois===-5541-###eois===-554138-###lif===-19-###soif===-###eoif===-558-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");}


	/* reads the rest of the response until empty line */
		AKA_mark("lis===208###sois===5652###eois===5661###lif===23###soif===631###eoif===640###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");clen = 0;

		AKA_mark("lis===209###sois===5663###eois===5674###lif===24###soif===642###eoif===653###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");haserr = 0;

		for (;;) {
				AKA_mark("lis===211###sois===5688###eois===5735###lif===26###soif===667###eoif===714###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");rc = receive_line(fd, line, (int)sizeof(line));

				if (AKA_mark("lis===212###sois===5742###eois===5748###lif===27###soif===721###eoif===727###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && (AKA_mark("lis===212###sois===5742###eois===5748###lif===27###soif===721###eoif===727###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&rc < 0)) {
			AKA_mark("lis===213###sois===5753###eois===5764###lif===28###soif===732###eoif===743###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");goto error;
		}
		else {AKA_mark("lis===-212-###sois===-5742-###eois===-57426-###lif===-27-###soif===-###eoif===-727-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");}

				if (AKA_mark("lis===214###sois===5771###eois===5778###lif===29###soif===750###eoif===757###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && (AKA_mark("lis===214###sois===5771###eois===5778###lif===29###soif===750###eoif===757###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&rc == 0)) {
			AKA_mark("lis===215###sois===5783###eois===5789###lif===30###soif===762###eoif===768###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");break;
		}
		else {AKA_mark("lis===-214-###sois===-5771-###eois===-57717-###lif===-29-###soif===-###eoif===-757-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");}

				AKA_mark("lis===216###sois===5792###eois===5818###lif===31###soif===771###eoif===797###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");len = strcspn(line, ": ");

				if (AKA_mark("lis===217###sois===5825###eois===5853###lif===32###soif===804###eoif===832###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && ((AKA_mark("lis===217###sois===5825###eois===5833###lif===32###soif===804###eoif===812###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&len != 0)	&&(AKA_mark("lis===217###sois===5837###eois===5853###lif===32###soif===816###eoif===832###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&line[len] == ':'))) {
			/* checks the headers values */
						AKA_mark("lis===219###sois===5895###eois===5915###lif===34###soif===874###eoif===894###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");it = line + len + 1;

						AKA_mark("lis===220###sois===5919###eois===5942###lif===35###soif===898###eoif===921###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");it += strspn(it, " ,");

						AKA_mark("lis===221###sois===5946###eois===5972###lif===36###soif===925###eoif===951###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");it[strcspn(it, " ,")] = 0;

						if (AKA_mark("lis===222###sois===5980###eois===6023###lif===37###soif===959###eoif===1002###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && (AKA_mark("lis===222###sois===5980###eois===6023###lif===37###soif===959###eoif===1002###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&isheader(line, len, "Sec-WebSocket-Accept"))) {
								if (AKA_mark("lis===223###sois===6035###eois===6055###lif===38###soif===1014###eoif===1034###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && (AKA_mark("lis===223###sois===6035###eois===6055###lif===38###soif===1014###eoif===1034###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&strcmp(it, ack) != 0)) {
					AKA_mark("lis===224###sois===6062###eois===6073###lif===39###soif===1041###eoif===1052###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");haserr = 1;
				}
				else {AKA_mark("lis===-223-###sois===-6035-###eois===-603520-###lif===-38-###soif===-###eoif===-1034-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");}

			}
			else {
				if (AKA_mark("lis===225###sois===6088###eois===6133###lif===40###soif===1067###eoif===1112###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && (AKA_mark("lis===225###sois===6088###eois===6133###lif===40###soif===1067###eoif===1112###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&isheader(line, len, "Sec-WebSocket-Protocol"))) {
									AKA_mark("lis===226###sois===6141###eois===6152###lif===41###soif===1120###eoif===1131###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");result = 0;

									while (AKA_mark("lis===227###sois===6163###eois===6226###lif===42###soif===1142###eoif===1205###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && ((AKA_mark("lis===227###sois===6163###eois===6188###lif===42###soif===1142###eoif===1167###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&protocols[result] != NULL)	&&(AKA_mark("lis===227###sois===6192###eois===6226###lif===42###soif===1171###eoif===1205###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&strcmp(it, protocols[result]) != 0))) {
						AKA_mark("lis===228###sois===6233###eois===6242###lif===43###soif===1212###eoif===1221###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");result++;
					}

			}
				else {
					if (AKA_mark("lis===229###sois===6257###eois===6287###lif===44###soif===1236###eoif===1266###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && (AKA_mark("lis===229###sois===6257###eois===6287###lif===44###soif===1236###eoif===1266###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&isheader(line, len, "Upgrade"))) {
										if (AKA_mark("lis===230###sois===6299###eois===6327###lif===45###soif===1278###eoif===1306###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && (AKA_mark("lis===230###sois===6299###eois===6327###lif===45###soif===1278###eoif===1306###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&strcmp(it, "websocket") != 0)) {
							AKA_mark("lis===231###sois===6334###eois===6345###lif===46###soif===1313###eoif===1324###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");haserr = 1;
						}
						else {AKA_mark("lis===-230-###sois===-6299-###eois===-629928-###lif===-45-###soif===-###eoif===-1306-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");}

			}
					else {
						if (AKA_mark("lis===232###sois===6360###eois===6397###lif===47###soif===1339###eoif===1376###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && (AKA_mark("lis===232###sois===6360###eois===6397###lif===47###soif===1339###eoif===1376###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&isheader(line, len, "Content-Length"))) {
											AKA_mark("lis===233###sois===6405###eois===6421###lif===48###soif===1384###eoif===1400###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");clen = atol(it);

			}
						else {AKA_mark("lis===-232-###sois===-6360-###eois===-636037-###lif===-47-###soif===-###eoif===-1376-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");}
					}
				}
			}

		}
		else {AKA_mark("lis===-217-###sois===-5825-###eois===-582528-###lif===-32-###soif===-###eoif===-832-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");}

	}


	/* skips the remaining of the message */
		while (AKA_mark("lis===239###sois===6485###eois===6504###lif===54###soif===1464###eoif===1483###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && (AKA_mark("lis===239###sois===6485###eois===6504###lif===54###soif===1464###eoif===1483###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&clen >= sizeof line)) {
				while (AKA_mark("lis===240###sois===6517###eois===6566###lif===55###soif===1496###eoif===1545###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && ((AKA_mark("lis===240###sois===6517###eois===6548###lif===55###soif===1496###eoif===1527###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&read(fd, line, sizeof line) < 0)	&&(AKA_mark("lis===240###sois===6552###eois===6566###lif===55###soif===1531###eoif===1545###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&errno == EINTR))) {
			;
		}

				AKA_mark("lis===241###sois===6571###eois===6591###lif===56###soif===1550###eoif===1570###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");clen -= sizeof line;

	}

		if (AKA_mark("lis===243###sois===6600###eois===6608###lif===58###soif===1579###eoif===1587###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && (AKA_mark("lis===243###sois===6600###eois===6608###lif===58###soif===1579###eoif===1587###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&clen > 0)) {
				while (AKA_mark("lis===244###sois===6621###eois===6662###lif===59###soif===1600###eoif===1641###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && ((AKA_mark("lis===244###sois===6621###eois===6644###lif===59###soif===1600###eoif===1623###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&read(fd, line, len) < 0)	&&(AKA_mark("lis===244###sois===6648###eois===6662###lif===59###soif===1627###eoif===1641###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&errno == EINTR))) {
			;
		}

	}
	else {AKA_mark("lis===-243-###sois===-6600-###eois===-66008-###lif===-58-###soif===-###eoif===-1587-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");}

		if (AKA_mark("lis===246###sois===6673###eois===6698###lif===61###soif===1652###eoif===1677###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)") && ((AKA_mark("lis===246###sois===6673###eois===6684###lif===61###soif===1652###eoif===1663###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&haserr != 0)	||(AKA_mark("lis===246###sois===6688###eois===6698###lif===61###soif===1667###eoif===1677###isc===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)")&&result < 0))) {
		AKA_mark("lis===247###sois===6702###eois===6713###lif===62###soif===1681###eoif===1692###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");goto abort;
	}
	else {AKA_mark("lis===-246-###sois===-6673-###eois===-667325-###lif===-61-###soif===-###eoif===-1677-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");}

		AKA_mark("lis===248###sois===6715###eois===6729###lif===63###soif===1694###eoif===1708###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");return result;

	abort:
	AKA_mark("lis===250###sois===6738###eois===6759###lif===65###soif===1717###eoif===1738###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");errno = ECONNABORTED;

	error:
	AKA_mark("lis===252###sois===6768###eois===6778###lif===67###soif===1747###eoif===1757###ins===true###function===./app-framework-binder/src/afb-ws-client.c/receive_response(int,const char**,const char*)");return -1;

}

/** Instrumented function negociate(int,const char**,const char*,const char*) */
static int negociate(int fd, const char **protocols, const char *path, const char *host)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-client.c/negociate(int,const char**,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===257###sois===6874###eois===6932###lif===2###soif===92###eoif===150###ins===true###function===./app-framework-binder/src/afb-ws-client.c/negociate(int,const char**,const char*,const char*)");const char *ack = send_request(fd, protocols, path, host);

		AKA_mark("lis===258###sois===6934###eois===6997###lif===3###soif===152###eoif===215###ins===true###function===./app-framework-binder/src/afb-ws-client.c/negociate(int,const char**,const char*,const char*)");return ack == NULL ? -1 : receive_response(fd, protocols, ack);

}

/* tiny parse a "standard" websock uri ws://host:port/path... */
/** Instrumented function parse_uri(const char*,char**,char**,const char**) */
static int parse_uri(const char *uri, char **host, char **service, const char **path)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");AKA_fCall++;
		AKA_mark("lis===264###sois===7155###eois===7173###lif===2###soif===89###eoif===107###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");const char *h, *p;

		AKA_mark("lis===265###sois===7175###eois===7193###lif===3###soif===109###eoif===127###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");size_t hlen, plen;


	/* the scheme */
		if (AKA_mark("lis===268###sois===7218###eois===7247###lif===6###soif===152###eoif===181###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)") && (AKA_mark("lis===268###sois===7218###eois===7247###lif===6###soif===152###eoif===181###isc===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)")&&strncmp(uri, "ws://", 5) == 0)) {
		AKA_mark("lis===269###sois===7251###eois===7260###lif===7###soif===185###eoif===194###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");uri += 5;
	}
	else {AKA_mark("lis===-268-###sois===-7218-###eois===-721829-###lif===-6-###soif===-###eoif===-181-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");}


	/* the host */
		AKA_mark("lis===272###sois===7279###eois===7287###lif===10###soif===213###eoif===221###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");h = uri;

		AKA_mark("lis===273###sois===7289###eois===7313###lif===11###soif===223###eoif===247###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");hlen = strcspn(h, ":/");

		if (AKA_mark("lis===274###sois===7319###eois===7328###lif===12###soif===253###eoif===262###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)") && (AKA_mark("lis===274###sois===7319###eois===7328###lif===12###soif===253###eoif===262###isc===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)")&&hlen == 0)) {
		AKA_mark("lis===275###sois===7332###eois===7345###lif===13###soif===266###eoif===279###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");goto invalid;
	}
	else {AKA_mark("lis===-274-###sois===-7319-###eois===-73199-###lif===-12-###soif===-###eoif===-262-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");}

		AKA_mark("lis===276###sois===7347###eois===7359###lif===14###soif===281###eoif===293###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");uri += hlen;


	/* the port (optional) */
		if (AKA_mark("lis===279###sois===7393###eois===7404###lif===17###soif===327###eoif===338###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)") && (AKA_mark("lis===279###sois===7393###eois===7404###lif===17###soif===327###eoif===338###isc===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)")&&*uri == ':')) {
				AKA_mark("lis===280###sois===7410###eois===7420###lif===18###soif===344###eoif===354###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");p = ++uri;

				AKA_mark("lis===281###sois===7423###eois===7446###lif===19###soif===357###eoif===380###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");plen = strcspn(p, "/");

				if (AKA_mark("lis===282###sois===7453###eois===7462###lif===20###soif===387###eoif===396###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)") && (AKA_mark("lis===282###sois===7453###eois===7462###lif===20###soif===387###eoif===396###isc===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)")&&plen == 0)) {
			AKA_mark("lis===283###sois===7467###eois===7480###lif===21###soif===401###eoif===414###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");goto invalid;
		}
		else {AKA_mark("lis===-282-###sois===-7453-###eois===-74539-###lif===-20-###soif===-###eoif===-396-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");}

				AKA_mark("lis===284###sois===7483###eois===7495###lif===22###soif===417###eoif===429###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");uri += plen;

	}
	else {
				AKA_mark("lis===286###sois===7508###eois===7517###lif===24###soif===442###eoif===451###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");p = NULL;

				AKA_mark("lis===287###sois===7520###eois===7529###lif===25###soif===454###eoif===463###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");plen = 0;

	}


	/* the path */
		if (AKA_mark("lis===291###sois===7555###eois===7566###lif===29###soif===489###eoif===500###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)") && (AKA_mark("lis===291###sois===7555###eois===7566###lif===29###soif===489###eoif===500###isc===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)")&&*uri != '/')) {
		AKA_mark("lis===292###sois===7570###eois===7583###lif===30###soif===504###eoif===517###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");goto invalid;
	}
	else {AKA_mark("lis===-291-###sois===-7555-###eois===-755511-###lif===-29-###soif===-###eoif===-500-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");}


	/* make the result */
		AKA_mark("lis===295###sois===7609###eois===7634###lif===33###soif===543###eoif===568###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");*host = strndup(h, hlen);

		if (AKA_mark("lis===296###sois===7640###eois===7653###lif===34###soif===574###eoif===587###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)") && (AKA_mark("lis===296###sois===7640###eois===7653###lif===34###soif===574###eoif===587###isc===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)")&&*host != NULL)) {
				AKA_mark("lis===297###sois===7659###eois===7711###lif===35###soif===593###eoif===645###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");*service = plen ? strndup(p, plen) : strdup("http");

				if (AKA_mark("lis===298###sois===7718###eois===7734###lif===36###soif===652###eoif===668###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)") && (AKA_mark("lis===298###sois===7718###eois===7734###lif===36###soif===652###eoif===668###isc===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)")&&*service != NULL)) {
						AKA_mark("lis===299###sois===7741###eois===7753###lif===37###soif===675###eoif===687###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");*path = uri;

						AKA_mark("lis===300###sois===7757###eois===7766###lif===38###soif===691###eoif===700###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");return 0;

		}
		else {AKA_mark("lis===-298-###sois===-7718-###eois===-771816-###lif===-36-###soif===-###eoif===-668-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");}

				AKA_mark("lis===302###sois===7773###eois===7785###lif===40###soif===707###eoif===719###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");free(*host);

	}
	else {AKA_mark("lis===-296-###sois===-7640-###eois===-764013-###lif===-34-###soif===-###eoif===-587-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");}

		AKA_mark("lis===304###sois===7790###eois===7805###lif===42###soif===724###eoif===739###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");errno = ENOMEM;

		AKA_mark("lis===305###sois===7807###eois===7818###lif===43###soif===741###eoif===752###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");goto error;

	invalid:
	AKA_mark("lis===307###sois===7829###eois===7844###lif===45###soif===763###eoif===778###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");errno = EINVAL;

	error:
	AKA_mark("lis===309###sois===7853###eois===7863###lif===47###soif===787###eoif===797###ins===true###function===./app-framework-binder/src/afb-ws-client.c/parse_uri(const char*,char**,char**,const char**)");return -1;

}




static const char *proto_json1[2] = { "x-afb-ws-json1",	NULL };

/** Instrumented function afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*) */
struct afb_wsj1 *afb_ws_client_connect_wsj1(struct sd_event *eloop, const char *uri, struct afb_wsj1_itf *itf, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");AKA_fCall++;
		AKA_mark("lis===319###sois===8064###eois===8075###lif===2###soif===129###eoif===140###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");int rc, fd;

		AKA_mark("lis===320###sois===8077###eois===8109###lif===3###soif===142###eoif===174###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");char *host, *service, xhost[32];

		AKA_mark("lis===321###sois===8111###eois===8128###lif===4###soif===176###eoif===193###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");const char *path;

		AKA_mark("lis===322###sois===8130###eois===8163###lif===5###soif===195###eoif===228###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");struct addrinfo hint, *rai, *iai;

		AKA_mark("lis===323###sois===8165###eois===8189###lif===6###soif===230###eoif===254###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");struct afb_wsj1 *result;

		AKA_mark("lis===324###sois===8191###eois===8209###lif===7###soif===256###eoif===274###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");struct fdev *fdev;


	/* scan the uri */
		AKA_mark("lis===327###sois===8232###eois===8276###lif===10###soif===297###eoif===341###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");rc = parse_uri(uri, &host, &service, &path);

		if (AKA_mark("lis===328###sois===8282###eois===8288###lif===11###soif===347###eoif===353###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)") && (AKA_mark("lis===328###sois===8282###eois===8288###lif===11###soif===347###eoif===353###isc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)")&&rc < 0)) {
		AKA_mark("lis===329###sois===8292###eois===8304###lif===12###soif===357###eoif===369###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");return NULL;
	}
	else {AKA_mark("lis===-328-###sois===-8282-###eois===-82826-###lif===-11-###soif===-###eoif===-353-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");}


	/* get addr */
		AKA_mark("lis===332###sois===8323###eois===8353###lif===15###soif===388###eoif===418###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");memset(&hint, 0, sizeof hint);

		AKA_mark("lis===333###sois===8355###eois===8380###lif===16###soif===420###eoif===445###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");hint.ai_family = AF_INET;

		AKA_mark("lis===334###sois===8382###eois===8413###lif===17###soif===447###eoif===478###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");hint.ai_socktype = SOCK_STREAM;

		AKA_mark("lis===335###sois===8415###eois===8460###lif===18###soif===480###eoif===525###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");rc = getaddrinfo(host, service, &hint, &rai);

		AKA_mark("lis===336###sois===8462###eois===8473###lif===19###soif===527###eoif===538###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");free(host);

		AKA_mark("lis===337###sois===8475###eois===8489###lif===20###soif===540###eoif===554###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");free(service);

		if (AKA_mark("lis===338###sois===8495###eois===8502###lif===21###soif===560###eoif===567###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)") && (AKA_mark("lis===338###sois===8495###eois===8502###lif===21###soif===560###eoif===567###isc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)")&&rc != 0)) {
				AKA_mark("lis===339###sois===8508###eois===8523###lif===22###soif===573###eoif===588###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");errno = EINVAL;

				AKA_mark("lis===340###sois===8526###eois===8538###lif===23###soif===591###eoif===603###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");return NULL;

	}
	else {AKA_mark("lis===-338-###sois===-8495-###eois===-84957-###lif===-21-###soif===-###eoif===-567-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");}


	/* get the socket */
		AKA_mark("lis===344###sois===8566###eois===8580###lif===27###soif===631###eoif===645###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");result = NULL;

		AKA_mark("lis===345###sois===8582###eois===8592###lif===28###soif===647###eoif===657###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");iai = rai;

		while (AKA_mark("lis===346###sois===8601###eois===8612###lif===29###soif===666###eoif===677###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)") && (AKA_mark("lis===346###sois===8601###eois===8612###lif===29###soif===666###eoif===677###isc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)")&&iai != NULL)) {
				AKA_mark("lis===347###sois===8618###eois===8678###lif===30###soif===683###eoif===743###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");struct sockaddr_in *a = (struct sockaddr_in*)(iai->ai_addr);

				AKA_mark("lis===348###sois===8681###eois===8741###lif===31###soif===746###eoif===806###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");unsigned char *ipv4 = (unsigned char*)&(a->sin_addr.s_addr);

				AKA_mark("lis===349###sois===8744###eois===8797###lif===32###soif===809###eoif===862###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");unsigned char *port = (unsigned char*)&(a->sin_port);

				AKA_mark("lis===350###sois===8800###eois===8931###lif===33###soif===865###eoif===996###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");sprintf(xhost, "%d.%d.%d.%d:%d",
			(int)ipv4[0], (int)ipv4[1], (int)ipv4[2], (int)ipv4[3],
			(((int)port[0]) << 8)|(int)port[1]);

				AKA_mark("lis===353###sois===8934###eois===8998###lif===36###soif===999###eoif===1063###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");fd = socket(iai->ai_family, iai->ai_socktype, iai->ai_protocol);

				if (AKA_mark("lis===354###sois===9005###eois===9012###lif===37###soif===1070###eoif===1077###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)") && (AKA_mark("lis===354###sois===9005###eois===9012###lif===37###soif===1070###eoif===1077###isc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)")&&fd >= 0)) {
						AKA_mark("lis===355###sois===9019###eois===9067###lif===38###soif===1084###eoif===1132###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");rc = connect(fd, iai->ai_addr, iai->ai_addrlen);

						if (AKA_mark("lis===356###sois===9075###eois===9082###lif===39###soif===1140###eoif===1147###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)") && (AKA_mark("lis===356###sois===9075###eois===9082###lif===39###soif===1140###eoif===1147###isc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)")&&rc == 0)) {
								AKA_mark("lis===357###sois===9090###eois===9135###lif===40###soif===1155###eoif===1200###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");rc = negociate(fd, proto_json1, path, xhost);

								if (AKA_mark("lis===358###sois===9144###eois===9151###lif===41###soif===1209###eoif===1216###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)") && (AKA_mark("lis===358###sois===9144###eois===9151###lif===41###soif===1209###eoif===1216###isc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)")&&rc == 0)) {
										AKA_mark("lis===359###sois===9160###eois===9198###lif===42###soif===1225###eoif===1263###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");fdev = fdev_systemd_create(eloop, fd);

										if (AKA_mark("lis===360###sois===9208###eois===9212###lif===43###soif===1273###eoif===1277###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)") && (AKA_mark("lis===360###sois===9208###eois===9212###lif===43###soif===1273###eoif===1277###isc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)")&&fdev)) {
												AKA_mark("lis===361###sois===9222###eois===9267###lif===44###soif===1287###eoif===1332###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");result = afb_wsj1_create(fdev, itf, closure);

												if (AKA_mark("lis===362###sois===9278###eois===9292###lif===45###soif===1343###eoif===1357###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)") && (AKA_mark("lis===362###sois===9278###eois===9292###lif===45###soif===1343###eoif===1357###isc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)")&&result != NULL)) {
														AKA_mark("lis===363###sois===9303###eois===9334###lif===46###soif===1368###eoif===1399###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");fcntl(fd, F_SETFL, O_NONBLOCK);

														AKA_mark("lis===364###sois===9342###eois===9348###lif===47###soif===1407###eoif===1413###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");break;

						}
						else {AKA_mark("lis===-362-###sois===-9278-###eois===-927814-###lif===-45-###soif===-###eoif===-1357-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");}

					}
					else {AKA_mark("lis===-360-###sois===-9208-###eois===-92084-###lif===-43-###soif===-###eoif===-1277-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");}

				}
				else {AKA_mark("lis===-358-###sois===-9144-###eois===-91447-###lif===-41-###soif===-###eoif===-1216-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");}

			}
			else {AKA_mark("lis===-356-###sois===-9075-###eois===-90757-###lif===-39-###soif===-###eoif===-1147-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");}

						AKA_mark("lis===369###sois===9378###eois===9388###lif===52###soif===1443###eoif===1453###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");close(fd);

		}
		else {AKA_mark("lis===-354-###sois===-9005-###eois===-90057-###lif===-37-###soif===-###eoif===-1077-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");}

				AKA_mark("lis===371###sois===9395###eois===9414###lif===54###soif===1460###eoif===1479###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");iai = iai->ai_next;

	}

		AKA_mark("lis===373###sois===9419###eois===9437###lif===56###soif===1484###eoif===1502###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");freeaddrinfo(rai);

		AKA_mark("lis===374###sois===9439###eois===9453###lif===57###soif===1504###eoif===1518###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_wsj1(struct sd_event*,const char*,struct afb_wsj1_itf*,void*)");return result;

}

#if 0
/* compute the queried path */
static char *makequery(const char *path, const char *uuid, const char *token)
{
	char *result;
	int rc;

	while(*path == '/')
		path++;
	if (uuid == NULL) {
		if (token == NULL)
			rc = asprintf(&result, "/%s", path);
		else
			rc = asprintf(&result, "/%s?x-afb-token=%s", path, token);
	} else {
		if (token == NULL)
			rc = asprintf(&result, "/%s?x-afb-uuid=%s", path, uuid);
		else
			rc = asprintf(&result, "/%s?x-afb-uuid=%s&x-afb-token=%s", path, uuid, token);
	}
	if (rc < 0) {
		errno = ENOMEM;
		return NULL;
	}
	return result;
}
#endif

/*****************************************************************************************************************************/

#include <sys/un.h>
/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_PROTO_WS_H_
#define AKA_INCLUDE__AFB_PROTO_WS_H_
#include "afb-proto-ws.akaignore.h"
#endif


/** Instrumented function get_socket_unix(const char*) */
static int get_socket_unix(const char *uri)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");AKA_fCall++;
		AKA_mark("lis===412###sois===10264###eois===10275###lif===2###soif===47###eoif===58###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");int fd, rc;

		AKA_mark("lis===413###sois===10277###eois===10301###lif===3###soif===60###eoif===84###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");struct sockaddr_un addr;

		AKA_mark("lis===414###sois===10303###eois===10317###lif===4###soif===86###eoif===100###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");size_t length;


		AKA_mark("lis===416###sois===10320###eois===10341###lif===6###soif===103###eoif===124###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");length = strlen(uri);

		if (AKA_mark("lis===417###sois===10347###eois===10360###lif===7###soif===130###eoif===143###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)") && (AKA_mark("lis===417###sois===10347###eois===10360###lif===7###soif===130###eoif===143###isc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)")&&length >= 108)) {
				AKA_mark("lis===418###sois===10366###eois===10387###lif===8###soif===149###eoif===170###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");errno = ENAMETOOLONG;

				AKA_mark("lis===419###sois===10390###eois===10400###lif===9###soif===173###eoif===183###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");return -1;

	}
	else {AKA_mark("lis===-417-###sois===-10347-###eois===-1034713-###lif===-7-###soif===-###eoif===-143-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");}


		AKA_mark("lis===422###sois===10406###eois===10443###lif===12###soif===189###eoif===226###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");fd = socket(AF_UNIX, SOCK_STREAM, 0);

		if (AKA_mark("lis===423###sois===10449###eois===10455###lif===13###soif===232###eoif===238###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)") && (AKA_mark("lis===423###sois===10449###eois===10455###lif===13###soif===232###eoif===238###isc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)")&&fd < 0)) {
		AKA_mark("lis===424###sois===10459###eois===10469###lif===14###soif===242###eoif===252###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");return fd;
	}
	else {AKA_mark("lis===-423-###sois===-10449-###eois===-104496-###lif===-13-###soif===-###eoif===-238-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");}


		AKA_mark("lis===426###sois===10472###eois===10502###lif===16###soif===255###eoif===285###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");memset(&addr, 0, sizeof addr);

		AKA_mark("lis===427###sois===10504###eois===10530###lif===17###soif===287###eoif===313###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");addr.sun_family = AF_UNIX;

		AKA_mark("lis===428###sois===10532###eois===10559###lif===18###soif===315###eoif===342###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");strcpy(addr.sun_path, uri);

		if (AKA_mark("lis===429###sois===10565###eois===10588###lif===19###soif===348###eoif===371###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)") && (AKA_mark("lis===429###sois===10565###eois===10588###lif===19###soif===348###eoif===371###isc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)")&&addr.sun_path[0] == '@')) {
		AKA_mark("lis===430###sois===10592###eois===10613###lif===20###soif===375###eoif===396###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");addr.sun_path[0] = 0;
	}
	else {AKA_mark("lis===-429-###sois===-10565-###eois===-1056523-###lif===-19-###soif===-###eoif===-371-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");}
 /* implement abstract sockets */
		AKA_mark("lis===431###sois===10648###eois===10718###lif===21###soif===431###eoif===501###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");rc = connect(fd, (struct sockaddr *) &addr, (socklen_t)(sizeof addr));

		if (AKA_mark("lis===432###sois===10724###eois===10730###lif===22###soif===507###eoif===513###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)") && (AKA_mark("lis===432###sois===10724###eois===10730###lif===22###soif===507###eoif===513###isc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)")&&rc < 0)) {
				AKA_mark("lis===433###sois===10736###eois===10746###lif===23###soif===519###eoif===529###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");close(fd);

				AKA_mark("lis===434###sois===10749###eois===10759###lif===24###soif===532###eoif===542###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");return rc;

	}
	else {AKA_mark("lis===-432-###sois===-10724-###eois===-107246-###lif===-22-###soif===-###eoif===-513-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");}

		AKA_mark("lis===436###sois===10764###eois===10774###lif===26###soif===547###eoif===557###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_unix(const char*)");return fd;

}

/** Instrumented function get_socket_inet(const char*) */
static int get_socket_inet(const char *uri)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");AKA_fCall++;
		AKA_mark("lis===441###sois===10825###eois===10836###lif===2###soif===47###eoif===58###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");int rc, fd;

		AKA_mark("lis===442###sois===10838###eois===10871###lif===3###soif===60###eoif===93###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");const char *service, *host, *api;

		AKA_mark("lis===443###sois===10873###eois===10906###lif===4###soif===95###eoif===128###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");struct addrinfo hint, *rai, *iai;


	/* scan the uri */
		AKA_mark("lis===446###sois===10929###eois===10957###lif===7###soif===151###eoif===179###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");service = strrchr(uri, ':');

		if (AKA_mark("lis===447###sois===10963###eois===10978###lif===8###soif===185###eoif===200###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)") && (AKA_mark("lis===447###sois===10963###eois===10978###lif===8###soif===185###eoif===200###isc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)")&&service == NULL)) {
				AKA_mark("lis===448###sois===10984###eois===10999###lif===9###soif===206###eoif===221###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");errno = EINVAL;

				AKA_mark("lis===449###sois===11002###eois===11012###lif===10###soif===224###eoif===234###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");return -1;

	}
	else {AKA_mark("lis===-447-###sois===-10963-###eois===-1096315-###lif===-8-###soif===-###eoif===-200-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");}

		AKA_mark("lis===451###sois===11017###eois===11047###lif===12###soif===239###eoif===269###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");api = strchrnul(service, '/');

		AKA_mark("lis===452###sois===11049###eois===11087###lif===13###soif===271###eoif===309###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");host = strndupa(uri, service++ - uri);

		AKA_mark("lis===453###sois===11089###eois===11132###lif===14###soif===311###eoif===354###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");service = strndupa(service, api - service);


	/* get addr */
		AKA_mark("lis===456###sois===11151###eois===11181###lif===17###soif===373###eoif===403###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");memset(&hint, 0, sizeof hint);

		AKA_mark("lis===457###sois===11183###eois===11208###lif===18###soif===405###eoif===430###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");hint.ai_family = AF_INET;

		AKA_mark("lis===458###sois===11210###eois===11241###lif===19###soif===432###eoif===463###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");hint.ai_socktype = SOCK_STREAM;

		AKA_mark("lis===459###sois===11243###eois===11288###lif===20###soif===465###eoif===510###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");rc = getaddrinfo(host, service, &hint, &rai);

		if (AKA_mark("lis===460###sois===11294###eois===11301###lif===21###soif===516###eoif===523###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)") && (AKA_mark("lis===460###sois===11294###eois===11301###lif===21###soif===516###eoif===523###isc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)")&&rc != 0)) {
				AKA_mark("lis===461###sois===11307###eois===11322###lif===22###soif===529###eoif===544###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");errno = EINVAL;

				AKA_mark("lis===462###sois===11325###eois===11335###lif===23###soif===547###eoif===557###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");return -1;

	}
	else {AKA_mark("lis===-460-###sois===-11294-###eois===-112947-###lif===-21-###soif===-###eoif===-523-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");}


	/* get the socket */
		AKA_mark("lis===466###sois===11363###eois===11373###lif===27###soif===585###eoif===595###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");iai = rai;

		while (AKA_mark("lis===467###sois===11382###eois===11393###lif===28###soif===604###eoif===615###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)") && (AKA_mark("lis===467###sois===11382###eois===11393###lif===28###soif===604###eoif===615###isc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)")&&iai != NULL)) {
				AKA_mark("lis===468###sois===11399###eois===11463###lif===29###soif===621###eoif===685###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");fd = socket(iai->ai_family, iai->ai_socktype, iai->ai_protocol);

				if (AKA_mark("lis===469###sois===11470###eois===11477###lif===30###soif===692###eoif===699###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)") && (AKA_mark("lis===469###sois===11470###eois===11477###lif===30###soif===692###eoif===699###isc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)")&&fd >= 0)) {
						AKA_mark("lis===470###sois===11484###eois===11532###lif===31###soif===706###eoif===754###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");rc = connect(fd, iai->ai_addr, iai->ai_addrlen);

						if (AKA_mark("lis===471###sois===11540###eois===11547###lif===32###soif===762###eoif===769###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)") && (AKA_mark("lis===471###sois===11540###eois===11547###lif===32###soif===762###eoif===769###isc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)")&&rc == 0)) {
								AKA_mark("lis===472###sois===11555###eois===11573###lif===33###soif===777###eoif===795###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");	AKA_mark("lis===479###sois===11638###eois===11656###lif===40###soif===860###eoif===878###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");freeaddrinfo(rai);


								AKA_mark("lis===473###sois===11578###eois===11588###lif===34###soif===800###eoif===810###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");return fd;

			}
			else {AKA_mark("lis===-471-###sois===-11540-###eois===-115407-###lif===-32-###soif===-###eoif===-769-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");}

						AKA_mark("lis===475###sois===11597###eois===11607###lif===36###soif===819###eoif===829###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");close(fd);

		}
		else {AKA_mark("lis===-469-###sois===-11470-###eois===-114707-###lif===-30-###soif===-###eoif===-699-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");}

				AKA_mark("lis===477###sois===11614###eois===11633###lif===38###soif===836###eoif===855###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");iai = iai->ai_next;

	}

	freeaddrinfo(rai);
		AKA_mark("lis===480###sois===11658###eois===11668###lif===41###soif===880###eoif===890###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket_inet(const char*)");return -1;

}

/** Instrumented function get_socket(const char*) */
static int get_socket(const char *uri)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)");AKA_fCall++;
		AKA_mark("lis===485###sois===11714###eois===11721###lif===2###soif===42###eoif===49###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)");int fd;


	/* check for unix socket */
		if (AKA_mark("lis===488###sois===11757###eois===11786###lif===5###soif===85###eoif===114###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)") && (AKA_mark("lis===488###sois===11757###eois===11786###lif===5###soif===85###eoif===114###isc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)")&&0 == strncmp(uri, "unix:", 5))) {
		AKA_mark("lis===490###sois===11810###eois===11840###lif===7###soif===138###eoif===168###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)");fd = get_socket_unix(uri + 5);
	}
	else {
		if (AKA_mark("lis===491###sois===11851###eois===11879###lif===8###soif===179###eoif===207###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)") && (AKA_mark("lis===491###sois===11851###eois===11879###lif===8###soif===179###eoif===207###isc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)")&&0 == strncmp(uri, "tcp:", 4))) {
			AKA_mark("lis===493###sois===11903###eois===11933###lif===10###soif===231###eoif===261###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)");fd = get_socket_inet(uri + 4);
		}
		else {
			AKA_mark("lis===496###sois===11962###eois===11988###lif===13###soif===290###eoif===316###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)");fd = get_socket_inet(uri);
		}
	}


	/* configure the socket */
		if (AKA_mark("lis===499###sois===12023###eois===12030###lif===16###soif===351###eoif===358###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)") && (AKA_mark("lis===499###sois===12023###eois===12030###lif===16###soif===351###eoif===358###isc===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)")&&fd >= 0)) {
				AKA_mark("lis===500###sois===12036###eois===12067###lif===17###soif===364###eoif===395###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)");fcntl(fd, F_SETFD, FD_CLOEXEC);

				AKA_mark("lis===501###sois===12070###eois===12101###lif===18###soif===398###eoif===429###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)");fcntl(fd, F_SETFL, O_NONBLOCK);

	}
	else {AKA_mark("lis===-499-###sois===-12023-###eois===-120237-###lif===-16-###soif===-###eoif===-358-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)");}

		AKA_mark("lis===503###sois===12106###eois===12116###lif===20###soif===434###eoif===444###ins===true###function===./app-framework-binder/src/afb-ws-client.c/get_socket(const char*)");return fd;

}

/*
 * Establish a websocket-like client connection to the API of 'uri' and if successful
 * instantiate a client afb_proto_ws websocket for this API using 'itf' and 'closure'.
 * (see afb_proto_ws_create_client).
 * The systemd event loop 'eloop' is used to handle the websocket.
 * Returns NULL in case of failure with errno set appropriately.
 */
/** Instrumented function afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*) */
struct afb_proto_ws *afb_ws_client_connect_api(struct sd_event *eloop, const char *uri, struct afb_proto_ws_client_itf *itf, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)");AKA_fCall++;
		AKA_mark("lis===515###sois===12612###eois===12619###lif===2###soif===143###eoif===150###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)");int fd;

		AKA_mark("lis===516###sois===12621###eois===12646###lif===3###soif===152###eoif===177###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)");struct afb_proto_ws *pws;

		AKA_mark("lis===517###sois===12648###eois===12666###lif===4###soif===179###eoif===197###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)");struct fdev *fdev;


		AKA_mark("lis===519###sois===12669###eois===12690###lif===6###soif===200###eoif===221###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)");fd = get_socket(uri);

		if (AKA_mark("lis===520###sois===12696###eois===12703###lif===7###soif===227###eoif===234###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)") && (AKA_mark("lis===520###sois===12696###eois===12703###lif===7###soif===227###eoif===234###isc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)")&&fd >= 0)) {
				AKA_mark("lis===521###sois===12709###eois===12747###lif===8###soif===240###eoif===278###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)");fdev = fdev_systemd_create(eloop, fd);

				if (AKA_mark("lis===522###sois===12754###eois===12758###lif===9###soif===285###eoif===289###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)") && (AKA_mark("lis===522###sois===12754###eois===12758###lif===9###soif===285###eoif===289###isc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)")&&fdev)) {
						AKA_mark("lis===523###sois===12765###eois===12818###lif===10###soif===296###eoif===349###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)");pws = afb_proto_ws_create_client(fdev, itf, closure);

						if (AKA_mark("lis===524###sois===12826###eois===12829###lif===11###soif===357###eoif===360###ifc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)") && (AKA_mark("lis===524###sois===12826###eois===12829###lif===11###soif===357###eoif===360###isc===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)")&&pws)) {
				AKA_mark("lis===525###sois===12835###eois===12846###lif===12###soif===366###eoif===377###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)");return pws;
			}
			else {AKA_mark("lis===-524-###sois===-12826-###eois===-128263-###lif===-11-###soif===-###eoif===-360-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)");}

		}
		else {AKA_mark("lis===-522-###sois===-12754-###eois===-127544-###lif===-9-###soif===-###eoif===-289-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)");}

				AKA_mark("lis===527###sois===12853###eois===12863###lif===14###soif===384###eoif===394###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)");close(fd);

	}
	else {AKA_mark("lis===-520-###sois===-12696-###eois===-126967-###lif===-7-###soif===-###eoif===-234-###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)");}

		AKA_mark("lis===529###sois===12868###eois===12880###lif===16###soif===399###eoif===411###ins===true###function===./app-framework-binder/src/afb-ws-client.c/afb_ws_client_connect_api(struct sd_event*,const char*,struct afb_proto_ws_client_itf*,void*)");return NULL;

}


#endif

