/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_WSJ1_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_WSJ1_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>

#include <json-c/json.h>
#if !defined(JSON_C_TO_STRING_NOSLASHESCAPE)
#define JSON_C_TO_STRING_NOSLASHESCAPE 0
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_WS_H_
#define AKA_INCLUDE__AFB_WS_H_
#include "afb-ws.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_WSJ1_H_
#define AKA_INCLUDE__AFB_WSJ1_H_
#include "afb-wsj1.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_H_
#define AKA_INCLUDE__FDEV_H_
#include "fdev.akaignore.h"
#endif


#define CALL 2
#define RETOK 3
#define RETERR 4
#define EVENT 5

#define WEBSOCKET_CODE_POLICY_VIOLATION  1008
#define WEBSOCKET_CODE_INTERNAL_ERROR    1011

static void wsj1_on_hangup(struct afb_wsj1 *wsj1);
static void wsj1_on_text(struct afb_wsj1 *wsj1, char *text, size_t size);
static struct afb_wsj1_msg *wsj1_msg_make(struct afb_wsj1 *wsj1, char *text, size_t size);

static struct afb_ws_itf wsj1_itf = {
	.on_hangup = (void*)wsj1_on_hangup,
	.on_text = (void*)wsj1_on_text
};

struct wsj1_call
{
	struct wsj1_call *next;
	void (*callback)(void *, struct afb_wsj1_msg *);
	void *closure;
	char id[16];
};

struct afb_wsj1_msg
{
	int refcount;
	struct afb_wsj1 *wsj1;
	struct afb_wsj1_msg *next, *previous;
	char *text;
	int code;
	const char *id;
	const char *api;
	const char *verb;
	const char *event;
	const char *object_s;
	size_t object_s_length;
	const char *token;
	struct json_object *object_j;
};

struct afb_wsj1
{
	int refcount;
	int genid;
	struct afb_wsj1_itf *itf;
	void *closure;
	struct json_tokener *tokener;
	struct afb_ws *ws;
	struct afb_wsj1_msg *messages;
	struct wsj1_call *calls;
	pthread_mutex_t mutex;
};

/** Instrumented function afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*) */
struct afb_wsj1 *afb_wsj1_create(struct fdev *fdev, struct afb_wsj1_itf *itf, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");AKA_fCall++;
		AKA_mark("lis===94###sois===2221###eois===2245###lif===2###soif===96###eoif===120###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");struct afb_wsj1 *result;


		AKA_mark("lis===96###sois===2248###eois===2261###lif===4###soif===123###eoif===136###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");assert(fdev);

		AKA_mark("lis===97###sois===2263###eois===2275###lif===5###soif===138###eoif===150###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");assert(itf);

		AKA_mark("lis===98###sois===2277###eois===2298###lif===6###soif===152###eoif===173###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");assert(itf->on_call);


		AKA_mark("lis===100###sois===2301###eois===2337###lif===8###soif===176###eoif===212###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");result = calloc(1, sizeof * result);

		if (AKA_mark("lis===101###sois===2343###eois===2357###lif===9###soif===218###eoif===232###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)") && (AKA_mark("lis===101###sois===2343###eois===2357###lif===9###soif===218###eoif===232###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)")&&result == NULL)) {
		AKA_mark("lis===102###sois===2361###eois===2372###lif===10###soif===236###eoif===247###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");goto error;
	}
	else {AKA_mark("lis===-101-###sois===-2343-###eois===-234314-###lif===-9-###soif===-###eoif===-232-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");}


		AKA_mark("lis===104###sois===2375###eois===2396###lif===12###soif===250###eoif===271###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");result->refcount = 1;

		AKA_mark("lis===105###sois===2398###eois===2416###lif===13###soif===273###eoif===291###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");result->itf = itf;

		AKA_mark("lis===106###sois===2418###eois===2444###lif===14###soif===293###eoif===319###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");result->closure = closure;

		AKA_mark("lis===107###sois===2446###eois===2487###lif===15###soif===321###eoif===362###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");pthread_mutex_init(&result->mutex, NULL);


		AKA_mark("lis===109###sois===2490###eois===2527###lif===17###soif===365###eoif===402###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");result->tokener = json_tokener_new();

		if (AKA_mark("lis===110###sois===2533###eois===2556###lif===18###soif===408###eoif===431###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)") && (AKA_mark("lis===110###sois===2533###eois===2556###lif===18###soif===408###eoif===431###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)")&&result->tokener == NULL)) {
		AKA_mark("lis===111###sois===2560###eois===2572###lif===19###soif===435###eoif===447###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");goto error2;
	}
	else {AKA_mark("lis===-110-###sois===-2533-###eois===-253323-###lif===-18-###soif===-###eoif===-431-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");}


		AKA_mark("lis===113###sois===2575###eois===2627###lif===21###soif===450###eoif===502###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");result->ws = afb_ws_create(fdev, &wsj1_itf, result);

		if (AKA_mark("lis===114###sois===2633###eois===2651###lif===22###soif===508###eoif===526###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)") && (AKA_mark("lis===114###sois===2633###eois===2651###lif===22###soif===508###eoif===526###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)")&&result->ws == NULL)) {
		AKA_mark("lis===115###sois===2655###eois===2667###lif===23###soif===530###eoif===542###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");goto error3;
	}
	else {AKA_mark("lis===-114-###sois===-2633-###eois===-263318-###lif===-22-###soif===-###eoif===-526-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");}


		AKA_mark("lis===117###sois===2670###eois===2684###lif===25###soif===545###eoif===559###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");return result;


	error3:
	AKA_mark("lis===120###sois===2695###eois===2730###lif===28###soif===570###eoif===605###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");json_tokener_free(result->tokener);

	error2:
	AKA_mark("lis===122###sois===2740###eois===2753###lif===30###soif===615###eoif===628###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");free(result);

	error:
	AKA_mark("lis===124###sois===2762###eois===2779###lif===32###soif===637###eoif===654###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");fdev_unref(fdev);

		AKA_mark("lis===125###sois===2781###eois===2793###lif===33###soif===656###eoif===668###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_create(struct fdev*,struct afb_wsj1_itf*,void*)");return NULL;

}

/** Instrumented function afb_wsj1_addref(struct afb_wsj1*) */
void afb_wsj1_addref(struct afb_wsj1 *wsj1)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_addref(struct afb_wsj1*)");AKA_fCall++;
		if (AKA_mark("lis===130###sois===2848###eois===2852###lif===2###soif===51###eoif===55###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_addref(struct afb_wsj1*)") && (AKA_mark("lis===130###sois===2848###eois===2852###lif===2###soif===51###eoif===55###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_addref(struct afb_wsj1*)")&&wsj1)) {
		AKA_mark("lis===131###sois===2856###eois===2913###lif===3###soif===59###eoif===116###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_addref(struct afb_wsj1*)");__atomic_add_fetch(&wsj1->refcount, 1, __ATOMIC_RELAXED);
	}
	else {AKA_mark("lis===-130-###sois===-2848-###eois===-28484-###lif===-2-###soif===-###eoif===-55-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_addref(struct afb_wsj1*)");}

}

/** Instrumented function afb_wsj1_unref(struct afb_wsj1*) */
void afb_wsj1_unref(struct afb_wsj1 *wsj1)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_unref(struct afb_wsj1*)");AKA_fCall++;
		if (AKA_mark("lis===136###sois===2967###eois===3032###lif===2###soif===50###eoif===115###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_unref(struct afb_wsj1*)") && ((AKA_mark("lis===136###sois===2967###eois===2971###lif===2###soif===50###eoif===54###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_unref(struct afb_wsj1*)")&&wsj1)	&&(AKA_mark("lis===136###sois===2975###eois===3032###lif===2###soif===58###eoif===115###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_unref(struct afb_wsj1*)")&&!__atomic_sub_fetch(&wsj1->refcount, 1, __ATOMIC_RELAXED)))) {
				AKA_mark("lis===137###sois===3038###eois===3063###lif===3###soif===121###eoif===146###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_unref(struct afb_wsj1*)");afb_ws_destroy(wsj1->ws);

				AKA_mark("lis===138###sois===3066###eois===3099###lif===4###soif===149###eoif===182###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_unref(struct afb_wsj1*)");json_tokener_free(wsj1->tokener);

				AKA_mark("lis===139###sois===3102###eois===3113###lif===5###soif===185###eoif===196###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_unref(struct afb_wsj1*)");free(wsj1);

	}
	else {AKA_mark("lis===-136-###sois===-2967-###eois===-296765-###lif===-2-###soif===-###eoif===-115-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_unref(struct afb_wsj1*)");}

}

/** Instrumented function wsj1_on_hangup(struct afb_wsj1*) */
static void wsj1_on_hangup(struct afb_wsj1 *wsj1)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");AKA_fCall++;
		AKA_mark("lis===145###sois===3173###eois===3204###lif===2###soif===53###eoif===84###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");struct wsj1_call *call, *ncall;

		AKA_mark("lis===146###sois===3206###eois===3231###lif===3###soif===86###eoif===111###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");struct afb_wsj1_msg *msg;

		AKA_mark("lis===147###sois===3233###eois===3244###lif===4###soif===113###eoif===124###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");char *text;

		AKA_mark("lis===148###sois===3246###eois===3254###lif===5###soif===126###eoif===134###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");int len;


		AKA_mark("lis===150###sois===3257###eois===3416###lif===7###soif===137###eoif===296###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");static const char error_object_str[] = "{"
		"\"jtype\":\"afb-reply\","
		"\"request\":{"
			"\"status\":\"disconnected\","
			"\"info\":\"server hung up\"}}";


		AKA_mark("lis===156###sois===3419###eois===3485###lif===13###soif===299###eoif===365###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");ncall = __atomic_exchange_n(&wsj1->calls, NULL, __ATOMIC_RELAXED);

		while (AKA_mark("lis===157###sois===3494###eois===3499###lif===14###soif===374###eoif===379###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)") && (AKA_mark("lis===157###sois===3494###eois===3499###lif===14###soif===374###eoif===379###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)")&&ncall)) {
				AKA_mark("lis===158###sois===3505###eois===3518###lif===15###soif===385###eoif===398###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");call = ncall;

				AKA_mark("lis===159###sois===3521###eois===3540###lif===16###soif===401###eoif===420###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");ncall = call->next;

				AKA_mark("lis===160###sois===3543###eois===3619###lif===17###soif===423###eoif===499###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");len = asprintf(&text, "[%d,\"%s\",%s]", RETERR, call->id, error_object_str);

				if (AKA_mark("lis===161###sois===3626###eois===3633###lif===18###soif===506###eoif===513###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)") && (AKA_mark("lis===161###sois===3626###eois===3633###lif===18###soif===506###eoif===513###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)")&&len > 0)) {
						AKA_mark("lis===162###sois===3640###eois===3685###lif===19###soif===520###eoif===565###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");msg = wsj1_msg_make(wsj1, text, (size_t)len);

						if (AKA_mark("lis===163###sois===3693###eois===3704###lif===20###soif===573###eoif===584###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)") && (AKA_mark("lis===163###sois===3693###eois===3704###lif===20###soif===573###eoif===584###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)")&&msg != NULL)) {
								AKA_mark("lis===164###sois===3712###eois===3747###lif===21###soif===592###eoif===627###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");call->callback(call->closure, msg);

								AKA_mark("lis===165###sois===3752###eois===3776###lif===22###soif===632###eoif===656###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");afb_wsj1_msg_unref(msg);

			}
			else {AKA_mark("lis===-163-###sois===-3693-###eois===-369311-###lif===-20-###soif===-###eoif===-584-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");}

		}
		else {AKA_mark("lis===-161-###sois===-3626-###eois===-36267-###lif===-18-###soif===-###eoif===-513-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");}

				AKA_mark("lis===168###sois===3788###eois===3799###lif===25###soif===668###eoif===679###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");free(call);

	}


		if (AKA_mark("lis===171###sois===3809###eois===3837###lif===28###soif===689###eoif===717###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)") && (AKA_mark("lis===171###sois===3809###eois===3837###lif===28###soif===689###eoif===717###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)")&&wsj1->itf->on_hangup != NULL)) {
		AKA_mark("lis===172###sois===3841###eois===3883###lif===29###soif===721###eoif===763###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");wsj1->itf->on_hangup(wsj1->closure, wsj1);
	}
	else {AKA_mark("lis===-171-###sois===-3809-###eois===-380928-###lif===-28-###soif===-###eoif===-717-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_hangup(struct afb_wsj1*)");}

}


/** Instrumented function wsj1_locked_call_search(struct afb_wsj1*,const char*,int) */
static struct wsj1_call *wsj1_locked_call_search(struct afb_wsj1 *wsj1, const char *id, int remove)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===178###sois===3991###eois===4016###lif===2###soif===103###eoif===128###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)");struct wsj1_call *r, **p;


		AKA_mark("lis===180###sois===4019###eois===4036###lif===4###soif===131###eoif===148###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)");p = &wsj1->calls;

		while (AKA_mark("lis===181###sois===4044###eois===4060###lif===5###soif===156###eoif===172###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)") && (AKA_mark("lis===181###sois===4044###eois===4060###lif===5###soif===156###eoif===172###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)")&&(r = *p) != NULL)) {
				if (AKA_mark("lis===182###sois===4070###eois===4092###lif===6###soif===182###eoif===204###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)") && (AKA_mark("lis===182###sois===4070###eois===4092###lif===6###soif===182###eoif===204###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)")&&strcmp(r->id, id) == 0)) {
						if (AKA_mark("lis===183###sois===4103###eois===4109###lif===7###soif===215###eoif===221###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)") && (AKA_mark("lis===183###sois===4103###eois===4109###lif===7###soif===215###eoif===221###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)")&&remove)) {
				AKA_mark("lis===184###sois===4115###eois===4128###lif===8###soif===227###eoif===240###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)");*p = r->next;
			}
			else {AKA_mark("lis===-183-###sois===-4103-###eois===-41036-###lif===-7-###soif===-###eoif===-221-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)");}

						AKA_mark("lis===185###sois===4132###eois===4138###lif===9###soif===244###eoif===250###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)");break;

		}
		else {AKA_mark("lis===-182-###sois===-4070-###eois===-407022-###lif===-6-###soif===-###eoif===-204-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)");}

				AKA_mark("lis===187###sois===4145###eois===4158###lif===11###soif===257###eoif===270###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)");p = &r->next;

	}


		AKA_mark("lis===190###sois===4164###eois===4173###lif===14###soif===276###eoif===285###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_locked_call_search(struct afb_wsj1*,const char*,int)");return r;

}

/** Instrumented function wsj1_call_search(struct afb_wsj1*,const char*,int) */
static struct wsj1_call *wsj1_call_search(struct afb_wsj1 *wsj1, const char *id, int remove)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/wsj1_call_search(struct afb_wsj1*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===195###sois===4273###eois===4293###lif===2###soif===96###eoif===116###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_search(struct afb_wsj1*,const char*,int)");struct wsj1_call *r;


		AKA_mark("lis===197###sois===4296###eois===4329###lif===4###soif===119###eoif===152###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_search(struct afb_wsj1*,const char*,int)");pthread_mutex_lock(&wsj1->mutex);

		AKA_mark("lis===198###sois===4331###eois===4377###lif===5###soif===154###eoif===200###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_search(struct afb_wsj1*,const char*,int)");r = wsj1_locked_call_search(wsj1, id, remove);

		AKA_mark("lis===199###sois===4379###eois===4414###lif===6###soif===202###eoif===237###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_search(struct afb_wsj1*,const char*,int)");pthread_mutex_unlock(&wsj1->mutex);


		AKA_mark("lis===201###sois===4417###eois===4426###lif===8###soif===240###eoif===249###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_search(struct afb_wsj1*,const char*,int)");return r;

}

/** Instrumented function wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*) */
static struct wsj1_call *wsj1_call_create(struct afb_wsj1 *wsj1, void (*on_reply)(void*,struct afb_wsj1_msg*), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)");AKA_fCall++;
		AKA_mark("lis===206###sois===4559###eois===4605###lif===2###soif===129###eoif===175###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)");struct wsj1_call *call = malloc(sizeof *call);

		if (AKA_mark("lis===207###sois===4611###eois===4623###lif===3###soif===181###eoif===193###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)") && (AKA_mark("lis===207###sois===4611###eois===4623###lif===3###soif===181###eoif===193###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)")&&call == NULL)) {
		AKA_mark("lis===208###sois===4627###eois===4642###lif===4###soif===197###eoif===212###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)");errno = ENOMEM;
	}
	else {
				AKA_mark("lis===210###sois===4653###eois===4686###lif===6###soif===223###eoif===256###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)");pthread_mutex_lock(&wsj1->mutex);

				do {
						if (AKA_mark("lis===212###sois===4701###eois===4717###lif===8###soif===271###eoif===287###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)") && (AKA_mark("lis===212###sois===4701###eois===4717###lif===8###soif===271###eoif===287###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)")&&wsj1->genid == 0)) {
				AKA_mark("lis===213###sois===4723###eois===4744###lif===9###soif===293###eoif===314###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)");wsj1->genid = 999999;
			}
			else {AKA_mark("lis===-212-###sois===-4701-###eois===-470116-###lif===-8-###soif===-###eoif===-287-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)");}

						AKA_mark("lis===214###sois===4748###eois===4787###lif===10###soif===318###eoif===357###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)");sprintf(call->id, "%d", wsj1->genid--);

		}
		while (AKA_mark("lis===215###sois===4799###eois===4849###lif===11###soif===369###eoif===419###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)") && (AKA_mark("lis===215###sois===4799###eois===4849###lif===11###soif===369###eoif===419###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)")&&wsj1_locked_call_search(wsj1, call->id, 0) != NULL));

				AKA_mark("lis===216###sois===4854###eois===4880###lif===12###soif===424###eoif===450###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)");call->callback = on_reply;

				AKA_mark("lis===217###sois===4883###eois===4907###lif===13###soif===453###eoif===477###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)");call->closure = closure;

				AKA_mark("lis===218###sois===4910###eois===4935###lif===14###soif===480###eoif===505###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)");call->next = wsj1->calls;

				AKA_mark("lis===219###sois===4938###eois===4957###lif===15###soif===508###eoif===527###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)");wsj1->calls = call;

				AKA_mark("lis===220###sois===4960###eois===4995###lif===16###soif===530###eoif===565###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)");pthread_mutex_unlock(&wsj1->mutex);

	}

		AKA_mark("lis===222###sois===5000###eois===5012###lif===18###soif===570###eoif===582###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_call_create(struct afb_wsj1*,void(*on_reply)(void*,struct afb_wsj1_msg*),void*)");return call;

}


/** Instrumented function wsj1_msg_scan(char*,size_t[10][2]) */
static int wsj1_msg_scan(char *text, size_t items[10][2])
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");AKA_fCall++;
		AKA_mark("lis===228###sois===5078###eois===5103###lif===2###soif===61###eoif===86###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");char *pos, *beg, *end, c;

		AKA_mark("lis===229###sois===5105###eois===5120###lif===3###soif===88###eoif===103###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");int aux, n = 0;


	/* scan */
		AKA_mark("lis===232###sois===5135###eois===5146###lif===6###soif===118###eoif===129###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");pos = text;


	/* scans: [ */
		while (AKA_mark("lis===235###sois===5171###eois===5182###lif===9###soif===154###eoif===165###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])") && (AKA_mark("lis===235###sois===5171###eois===5182###lif===9###soif===154###eoif===165###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&*pos == ' ')) {
		AKA_mark("lis===235###sois===5184###eois===5190###lif===9###soif===167###eoif===173###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");pos++;
	}

		if (AKA_mark("lis===236###sois===5196###eois===5209###lif===10###soif===179###eoif===192###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])") && (AKA_mark("lis===236###sois===5196###eois===5209###lif===10###soif===179###eoif===192###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&*pos++ != '[')) {
		AKA_mark("lis===236###sois===5211###eois===5225###lif===10###soif===194###eoif===208###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");goto bad_scan;
	}
	else {AKA_mark("lis===-236-###sois===-5196-###eois===-519613-###lif===-10-###soif===-###eoif===-192-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");}


	/* scans list */
		while (AKA_mark("lis===239###sois===5252###eois===5263###lif===13###soif===235###eoif===246###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])") && (AKA_mark("lis===239###sois===5252###eois===5263###lif===13###soif===235###eoif===246###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&*pos == ' ')) {
		AKA_mark("lis===239###sois===5265###eois===5271###lif===13###soif===248###eoif===254###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");pos++;
	}

		if (AKA_mark("lis===240###sois===5277###eois===5288###lif===14###soif===260###eoif===271###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])") && (AKA_mark("lis===240###sois===5277###eois===5288###lif===14###soif===260###eoif===271###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&*pos != ']')) {
				for (;;) {
						if (AKA_mark("lis===242###sois===5312###eois===5319###lif===16###soif===295###eoif===302###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])") && (AKA_mark("lis===242###sois===5312###eois===5319###lif===16###soif===295###eoif===302###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&n == 10)) {
				AKA_mark("lis===243###sois===5325###eois===5339###lif===17###soif===308###eoif===322###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");goto bad_scan;
			}
			else {AKA_mark("lis===-242-###sois===-5312-###eois===-53127-###lif===-16-###soif===-###eoif===-302-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");}

						AKA_mark("lis===244###sois===5343###eois===5353###lif===18###soif===326###eoif===336###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");beg = pos;

						AKA_mark("lis===245###sois===5357###eois===5365###lif===19###soif===340###eoif===348###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");aux = 0;

						while (AKA_mark("lis===246###sois===5376###eois===5416###lif===20###soif===359###eoif===399###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])") && ((AKA_mark("lis===246###sois===5376###eois===5384###lif===20###soif===359###eoif===367###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&aux != 0)	||((AKA_mark("lis===246###sois===5389###eois===5400###lif===20###soif===372###eoif===383###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&*pos != ',')	&&(AKA_mark("lis===246###sois===5404###eois===5415###lif===20###soif===387###eoif===398###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&*pos != ']')))) {
								AKA_mark("lis===247###sois===5431###eois===5437###lif===21###soif===414###eoif===420###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");switch(*pos++){
									case '{': if(*pos++ == '{')AKA_mark("lis===248###sois===5445###eois===5454###lif===22###soif===428###eoif===437###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");
 					case '[': if(*pos++ == '[')AKA_mark("lis===248###sois===5455###eois===5464###lif===22###soif===438###eoif===447###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");
 					AKA_mark("lis===248###sois===5465###eois===5471###lif===22###soif===448###eoif===454###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");aux++;
 					AKA_mark("lis===248###sois===5472###eois===5478###lif===22###soif===455###eoif===461###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");break;

									case '}': if(*pos++ == '}')AKA_mark("lis===249###sois===5483###eois===5492###lif===23###soif===466###eoif===475###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");
 					case ']': if(*pos++ == ']')AKA_mark("lis===249###sois===5493###eois===5502###lif===23###soif===476###eoif===485###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");
 					if (AKA_mark("lis===249###sois===5507###eois===5512###lif===23###soif===490###eoif===495###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])") && (AKA_mark("lis===249###sois===5507###eois===5512###lif===23###soif===490###eoif===495###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&aux--)) {
						AKA_mark("lis===249###sois===5514###eois===5520###lif===23###soif===497###eoif===503###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");break;
					}
					else {AKA_mark("lis===-249-###sois===-5507-###eois===-55075-###lif===-23-###soif===-###eoif===-495-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");}

									case 0: if(*pos++ == 0)AKA_mark("lis===250###sois===5525###eois===5532###lif===24###soif===508###eoif===515###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");
 					AKA_mark("lis===250###sois===5533###eois===5547###lif===24###soif===516###eoif===530###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");goto bad_scan;

									case '"': if(*pos++ == '"')AKA_mark("lis===251###sois===5552###eois===5561###lif===25###soif===535###eoif===544###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");

										do {
												AKA_mark("lis===253###sois===5585###eois===5595###lif===27###soif===568###eoif===578###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");switch(c = *pos++){
													case '\\': if(c = *pos++ == '\\')AKA_mark("lis===254###sois===5605###eois===5615###lif===28###soif===588###eoif===598###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");
 							if (AKA_mark("lis===254###sois===5620###eois===5626###lif===28###soif===603###eoif===609###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])") && (AKA_mark("lis===254###sois===5620###eois===5626###lif===28###soif===603###eoif===609###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&*pos++)) {
								AKA_mark("lis===254###sois===5628###eois===5634###lif===28###soif===611###eoif===617###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");break;
							}
							else {AKA_mark("lis===-254-###sois===-5620-###eois===-56206-###lif===-28-###soif===-###eoif===-609-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");}

													case 0: if(c = *pos++ == 0)AKA_mark("lis===255###sois===5641###eois===5648###lif===29###soif===624###eoif===631###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");
 							AKA_mark("lis===255###sois===5649###eois===5663###lif===29###soif===632###eoif===646###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");goto bad_scan;

						}

					}
					while (AKA_mark("lis===257###sois===5685###eois===5693###lif===31###soif===668###eoif===676###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])") && (AKA_mark("lis===257###sois===5685###eois===5693###lif===31###soif===668###eoif===676###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&c != '"'));

				}

			}

						AKA_mark("lis===260###sois===5710###eois===5720###lif===34###soif===693###eoif===703###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");end = pos;

						while (AKA_mark("lis===261###sois===5731###eois===5758###lif===35###soif===714###eoif===741###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])") && ((AKA_mark("lis===261###sois===5731###eois===5740###lif===35###soif===714###eoif===723###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&end > beg)	&&(AKA_mark("lis===261###sois===5744###eois===5758###lif===35###soif===727###eoif===741###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&end[-1] == ' '))) {
				AKA_mark("lis===262###sois===5764###eois===5770###lif===36###soif===747###eoif===753###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");end--;
			}

						AKA_mark("lis===263###sois===5774###eois===5799###lif===37###soif===757###eoif===782###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");items[n][0] = beg - text;
 /* start offset */
						AKA_mark("lis===264###sois===5822###eois===5846###lif===38###soif===805###eoif===829###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");items[n][1] = end - beg;
  /* length */
						AKA_mark("lis===265###sois===5864###eois===5868###lif===39###soif===847###eoif===851###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");n++;

						if (AKA_mark("lis===266###sois===5876###eois===5887###lif===40###soif===859###eoif===870###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])") && (AKA_mark("lis===266###sois===5876###eois===5887###lif===40###soif===859###eoif===870###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&*pos == ']')) {
				AKA_mark("lis===267###sois===5893###eois===5899###lif===41###soif===876###eoif===882###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");break;
			}
			else {AKA_mark("lis===-266-###sois===-5876-###eois===-587611-###lif===-40-###soif===-###eoif===-870-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");}

						while (AKA_mark("lis===268###sois===5909###eois===5922###lif===42###soif===892###eoif===905###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])") && (AKA_mark("lis===268###sois===5909###eois===5922###lif===42###soif===892###eoif===905###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&*++pos == ' ')) {
				;
			}

		}

	}
	else {AKA_mark("lis===-240-###sois===-5277-###eois===-527711-###lif===-14-###soif===-###eoif===-271-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");}

		while (AKA_mark("lis===271###sois===5939###eois===5952###lif===45###soif===922###eoif===935###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])") && (AKA_mark("lis===271###sois===5939###eois===5952###lif===45###soif===922###eoif===935###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&*++pos == ' ')) {
		;
	}

		if (AKA_mark("lis===272###sois===5960###eois===5964###lif===46###soif===943###eoif===947###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])") && (AKA_mark("lis===272###sois===5960###eois===5964###lif===46###soif===943###eoif===947###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])")&&*pos)) {
		AKA_mark("lis===272###sois===5966###eois===5980###lif===46###soif===949###eoif===963###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");goto bad_scan;
	}
	else {AKA_mark("lis===-272-###sois===-5960-###eois===-59604-###lif===-46-###soif===-###eoif===-947-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");}

		AKA_mark("lis===273###sois===5982###eois===5991###lif===47###soif===965###eoif===974###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");return n;


	bad_scan:
	AKA_mark("lis===276###sois===6004###eois===6014###lif===50###soif===987###eoif===997###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_scan(char*,size_t[10][2])");return -1;

}

/** Instrumented function wsj1_msg_parse_extract(char*,size_t,size_t) */
static char *wsj1_msg_parse_extract(char *text, size_t offset, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/wsj1_msg_parse_extract(char*,size_t,size_t)");AKA_fCall++;
		AKA_mark("lis===281###sois===6097###eois===6121###lif===2###soif===79###eoif===103###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_parse_extract(char*,size_t,size_t)");text[offset + size] = 0;

		AKA_mark("lis===282###sois===6123###eois===6144###lif===3###soif===105###eoif===126###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_parse_extract(char*,size_t,size_t)");return text + offset;

}

/** Instrumented function wsj1_msg_parse_string(char*,size_t,size_t) */
static char *wsj1_msg_parse_string(char *text, size_t offset, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/wsj1_msg_parse_string(char*,size_t,size_t)");AKA_fCall++;
		if (AKA_mark("lis===287###sois===6230###eois===6261###lif===2###soif===82###eoif===113###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_parse_string(char*,size_t,size_t)") && ((AKA_mark("lis===287###sois===6230###eois===6238###lif===2###soif===82###eoif===90###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_parse_string(char*,size_t,size_t)")&&size > 1)	&&(AKA_mark("lis===287###sois===6242###eois===6261###lif===2###soif===94###eoif===113###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_parse_string(char*,size_t,size_t)")&&text[offset] == '"'))) {
				AKA_mark("lis===288###sois===6267###eois===6279###lif===3###soif===119###eoif===131###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_parse_string(char*,size_t,size_t)");offset += 1;

				AKA_mark("lis===289###sois===6282###eois===6292###lif===4###soif===134###eoif===144###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_parse_string(char*,size_t,size_t)");size -= 2;

	}
	else {AKA_mark("lis===-287-###sois===-6230-###eois===-623031-###lif===-2-###soif===-###eoif===-113-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_parse_string(char*,size_t,size_t)");}

		AKA_mark("lis===291###sois===6297###eois===6347###lif===6###soif===149###eoif===199###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_parse_string(char*,size_t,size_t)");return wsj1_msg_parse_extract(text, offset, size);

}

/** Instrumented function wsj1_msg_make(struct afb_wsj1*,char*,size_t) */
static struct afb_wsj1_msg *wsj1_msg_make(struct afb_wsj1 *wsj1, char *text, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");AKA_fCall++;
		AKA_mark("lis===296###sois===6444###eois===6464###lif===2###soif===93###eoif===113###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");size_t items[10][2];

		AKA_mark("lis===297###sois===6466###eois===6472###lif===3###soif===115###eoif===121###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");int n;

		AKA_mark("lis===298###sois===6474###eois===6499###lif===4###soif===123###eoif===148###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");struct afb_wsj1_msg *msg;

		AKA_mark("lis===299###sois===6501###eois===6512###lif===5###soif===150###eoif===161###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");char *verb;


	/* allocate */
		AKA_mark("lis===302###sois===6531###eois===6560###lif===8###soif===180###eoif===209###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg = calloc(1, sizeof *msg);

		if (AKA_mark("lis===303###sois===6566###eois===6577###lif===9###soif===215###eoif===226###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)") && (AKA_mark("lis===303###sois===6566###eois===6577###lif===9###soif===215###eoif===226###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)")&&msg == NULL)) {
				AKA_mark("lis===304###sois===6583###eois===6598###lif===10###soif===232###eoif===247###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");errno = ENOMEM;

				AKA_mark("lis===305###sois===6601###eois===6618###lif===11###soif===250###eoif===267###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");goto alloc_error;

	}
	else {AKA_mark("lis===-303-###sois===-6566-###eois===-656611-###lif===-9-###soif===-###eoif===-226-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");}


	/* scan */
		AKA_mark("lis===309###sois===6636###eois===6667###lif===15###soif===285###eoif===316###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");n = wsj1_msg_scan(text, items);

		if (AKA_mark("lis===310###sois===6673###eois===6679###lif===16###soif===322###eoif===328###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)") && (AKA_mark("lis===310###sois===6673###eois===6679###lif===16###soif===322###eoif===328###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)")&&n <= 0)) {
		AKA_mark("lis===311###sois===6683###eois===6699###lif===17###soif===332###eoif===348###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");goto bad_header;
	}
	else {AKA_mark("lis===-310-###sois===-6673-###eois===-66736-###lif===-16-###soif===-###eoif===-328-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");}


	/* scans code: 2|3|4|5 */
		if (AKA_mark("lis===314###sois===6733###eois===6749###lif===20###soif===382###eoif===398###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)") && (AKA_mark("lis===314###sois===6733###eois===6749###lif===20###soif===382###eoif===398###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)")&&items[0][1] != 1)) {
		AKA_mark("lis===315###sois===6753###eois===6769###lif===21###soif===402###eoif===418###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");goto bad_header;
	}
	else {AKA_mark("lis===-314-###sois===-6733-###eois===-673316-###lif===-20-###soif===-###eoif===-398-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");}


		AKA_mark("lis===317###sois===6780###eois===6797###lif===23###soif===429###eoif===446###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");switch(text[items[0][0]]){
			case '2': if(text[items[0][0]] == '2')AKA_mark("lis===318###sois===6802###eois===6811###lif===24###soif===451###eoif===460###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");
 		AKA_mark("lis===318###sois===6812###eois===6829###lif===24###soif===461###eoif===478###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->code = CALL;
 		AKA_mark("lis===318###sois===6830###eois===6836###lif===24###soif===479###eoif===485###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");break;

			case '3': if(text[items[0][0]] == '3')AKA_mark("lis===319###sois===6838###eois===6847###lif===25###soif===487###eoif===496###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");
 		AKA_mark("lis===319###sois===6848###eois===6866###lif===25###soif===497###eoif===515###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->code = RETOK;
 		AKA_mark("lis===319###sois===6867###eois===6873###lif===25###soif===516###eoif===522###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");break;

			case '4': if(text[items[0][0]] == '4')AKA_mark("lis===320###sois===6875###eois===6884###lif===26###soif===524###eoif===533###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");
 		AKA_mark("lis===320###sois===6885###eois===6904###lif===26###soif===534###eoif===553###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->code = RETERR;
 		AKA_mark("lis===320###sois===6905###eois===6911###lif===26###soif===554###eoif===560###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");break;

			case '5': if(text[items[0][0]] == '5')AKA_mark("lis===321###sois===6913###eois===6922###lif===27###soif===562###eoif===571###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");
 		AKA_mark("lis===321###sois===6923###eois===6941###lif===27###soif===572###eoif===590###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->code = EVENT;
 		AKA_mark("lis===321###sois===6942###eois===6948###lif===27###soif===591###eoif===597###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");break;

			default: if(text[items[0][0]] != '2' && text[items[0][0]] != '3' && text[items[0][0]] != '4' && text[items[0][0]] != '5')AKA_mark("lis===322###sois===6950###eois===6958###lif===28###soif===599###eoif===607###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");
 		AKA_mark("lis===322###sois===6959###eois===6975###lif===28###soif===608###eoif===624###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");goto bad_header;

	}


	/* fills the message */
		AKA_mark("lis===326###sois===7014###eois===7023###lif===32###soif===663###eoif===672###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");switch(msg->code){
			case CALL: if(msg->code == CALL)AKA_mark("lis===327###sois===7028###eois===7038###lif===33###soif===677###eoif===687###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");

				if (AKA_mark("lis===328###sois===7045###eois===7061###lif===34###soif===694###eoif===710###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)") && ((AKA_mark("lis===328###sois===7045###eois===7051###lif===34###soif===694###eoif===700###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)")&&n != 4)	&&(AKA_mark("lis===328###sois===7055###eois===7061###lif===34###soif===704###eoif===710###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)")&&n != 5))) {
			AKA_mark("lis===328###sois===7063###eois===7079###lif===34###soif===712###eoif===728###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");goto bad_header;
		}
		else {AKA_mark("lis===-328-###sois===-7045-###eois===-704516-###lif===-34-###soif===-###eoif===-710-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");}

				AKA_mark("lis===329###sois===7082###eois===7146###lif===35###soif===731###eoif===795###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->id = wsj1_msg_parse_string(text, items[1][0], items[1][1]);

				AKA_mark("lis===330###sois===7149###eois===7214###lif===36###soif===798###eoif===863###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->api = wsj1_msg_parse_string(text, items[2][0], items[2][1]);

				AKA_mark("lis===331###sois===7217###eois===7246###lif===37###soif===866###eoif===895###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");verb = strchr(msg->api, '/');

				if (AKA_mark("lis===332###sois===7253###eois===7265###lif===38###soif===902###eoif===914###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)") && (AKA_mark("lis===332###sois===7253###eois===7265###lif===38###soif===902###eoif===914###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)")&&verb == NULL)) {
			AKA_mark("lis===332###sois===7267###eois===7283###lif===38###soif===916###eoif===932###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");goto bad_header;
		}
		else {AKA_mark("lis===-332-###sois===-7253-###eois===-725312-###lif===-38-###soif===-###eoif===-914-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");}

				AKA_mark("lis===333###sois===7286###eois===7298###lif===39###soif===935###eoif===947###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");*verb++ = 0;

				if (AKA_mark("lis===334###sois===7305###eois===7327###lif===40###soif===954###eoif===976###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)") && ((AKA_mark("lis===334###sois===7305###eois===7311###lif===40###soif===954###eoif===960###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)")&&!*verb)	||(AKA_mark("lis===334###sois===7315###eois===7327###lif===40###soif===964###eoif===976###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)")&&*verb == '/'))) {
			AKA_mark("lis===334###sois===7329###eois===7345###lif===40###soif===978###eoif===994###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");goto bad_header;
		}
		else {AKA_mark("lis===-334-###sois===-7305-###eois===-730522-###lif===-40-###soif===-###eoif===-976-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");}

				AKA_mark("lis===335###sois===7348###eois===7365###lif===41###soif===997###eoif===1014###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->verb = verb;

				AKA_mark("lis===336###sois===7368###eois===7439###lif===42###soif===1017###eoif===1088###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->object_s = wsj1_msg_parse_extract(text, items[3][0], items[3][1]);

				AKA_mark("lis===337###sois===7442###eois===7477###lif===43###soif===1091###eoif===1126###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->object_s_length = items[3][1];

				AKA_mark("lis===338###sois===7480###eois===7563###lif===44###soif===1129###eoif===1212###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->token = n == 5 ? wsj1_msg_parse_string(text, items[4][0], items[4][1]) : NULL;

				AKA_mark("lis===339###sois===7566###eois===7572###lif===45###soif===1215###eoif===1221###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");break;

			case RETOK: if(msg->code == RETOK)AKA_mark("lis===340###sois===7574###eois===7585###lif===46###soif===1223###eoif===1234###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");

			case RETERR: if(msg->code == RETERR)AKA_mark("lis===341###sois===7587###eois===7599###lif===47###soif===1236###eoif===1248###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");

				if (AKA_mark("lis===342###sois===7606###eois===7622###lif===48###soif===1255###eoif===1271###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)") && ((AKA_mark("lis===342###sois===7606###eois===7612###lif===48###soif===1255###eoif===1261###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)")&&n != 3)	&&(AKA_mark("lis===342###sois===7616###eois===7622###lif===48###soif===1265###eoif===1271###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)")&&n != 4))) {
			AKA_mark("lis===342###sois===7624###eois===7640###lif===48###soif===1273###eoif===1289###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");goto bad_header;
		}
		else {AKA_mark("lis===-342-###sois===-7606-###eois===-760616-###lif===-48-###soif===-###eoif===-1271-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");}

				AKA_mark("lis===343###sois===7643###eois===7707###lif===49###soif===1292###eoif===1356###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->id = wsj1_msg_parse_string(text, items[1][0], items[1][1]);

				AKA_mark("lis===344###sois===7710###eois===7781###lif===50###soif===1359###eoif===1430###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->object_s = wsj1_msg_parse_extract(text, items[2][0], items[2][1]);

				AKA_mark("lis===345###sois===7784###eois===7819###lif===51###soif===1433###eoif===1468###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->object_s_length = items[2][1];

				AKA_mark("lis===346###sois===7822###eois===7905###lif===52###soif===1471###eoif===1554###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->token = n == 5 ? wsj1_msg_parse_string(text, items[3][0], items[3][1]) : NULL;

				AKA_mark("lis===347###sois===7908###eois===7914###lif===53###soif===1557###eoif===1563###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");break;

			case EVENT: if(msg->code == EVENT)AKA_mark("lis===348###sois===7916###eois===7927###lif===54###soif===1565###eoif===1576###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");

				if (AKA_mark("lis===349###sois===7934###eois===7940###lif===55###soif===1583###eoif===1589###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)") && (AKA_mark("lis===349###sois===7934###eois===7940###lif===55###soif===1583###eoif===1589###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)")&&n != 3)) {
			AKA_mark("lis===349###sois===7942###eois===7958###lif===55###soif===1591###eoif===1607###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");goto bad_header;
		}
		else {AKA_mark("lis===-349-###sois===-7934-###eois===-79346-###lif===-55-###soif===-###eoif===-1589-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");}

				AKA_mark("lis===350###sois===7961###eois===8028###lif===56###soif===1610###eoif===1677###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->event = wsj1_msg_parse_string(text, items[1][0], items[1][1]);

				AKA_mark("lis===351###sois===8031###eois===8102###lif===57###soif===1680###eoif===1751###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->object_s = wsj1_msg_parse_extract(text, items[2][0], items[2][1]);

				AKA_mark("lis===352###sois===8105###eois===8140###lif===58###soif===1754###eoif===1789###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->object_s_length = items[2][1];

				AKA_mark("lis===353###sois===8143###eois===8149###lif===59###soif===1792###eoif===1798###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");break;

	}

	/* done */
		AKA_mark("lis===356###sois===8166###eois===8183###lif===62###soif===1815###eoif===1832###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->text = text;


	/* fill and record the request */
		AKA_mark("lis===359###sois===8221###eois===8239###lif===65###soif===1870###eoif===1888###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->refcount = 1;

		AKA_mark("lis===360###sois===8241###eois===8263###lif===66###soif===1890###eoif===1912###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");afb_wsj1_addref(wsj1);

		AKA_mark("lis===361###sois===8265###eois===8282###lif===67###soif===1914###eoif===1931###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->wsj1 = wsj1;

		AKA_mark("lis===362###sois===8284###eois===8317###lif===68###soif===1933###eoif===1966###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");pthread_mutex_lock(&wsj1->mutex);

		AKA_mark("lis===363###sois===8319###eois===8346###lif===69###soif===1968###eoif===1995###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->next = wsj1->messages;

		if (AKA_mark("lis===364###sois===8352###eois===8369###lif===70###soif===2001###eoif===2018###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)") && (AKA_mark("lis===364###sois===8352###eois===8369###lif===70###soif===2001###eoif===2018###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)")&&msg->next != NULL)) {
		AKA_mark("lis===365###sois===8373###eois===8399###lif===71###soif===2022###eoif===2048###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");msg->next->previous = msg;
	}
	else {AKA_mark("lis===-364-###sois===-8352-###eois===-835217-###lif===-70-###soif===-###eoif===-2018-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");}

		AKA_mark("lis===366###sois===8401###eois===8422###lif===72###soif===2050###eoif===2071###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");wsj1->messages = msg;

		AKA_mark("lis===367###sois===8424###eois===8459###lif===73###soif===2073###eoif===2108###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");pthread_mutex_unlock(&wsj1->mutex);


		AKA_mark("lis===369###sois===8462###eois===8473###lif===75###soif===2111###eoif===2122###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");return msg;


	bad_header:
	AKA_mark("lis===372###sois===8488###eois===8504###lif===78###soif===2137###eoif===2153###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");errno = EBADMSG;

		AKA_mark("lis===373###sois===8506###eois===8516###lif===79###soif===2155###eoif===2165###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");free(msg);


	alloc_error:
	AKA_mark("lis===376###sois===8532###eois===8543###lif===82###soif===2181###eoif===2192###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");free(text);

		AKA_mark("lis===377###sois===8545###eois===8557###lif===83###soif===2194###eoif===2206###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_msg_make(struct afb_wsj1*,char*,size_t)");return NULL;

}

/** Instrumented function wsj1_on_text(struct afb_wsj1*,char*,size_t) */
static void wsj1_on_text(struct afb_wsj1 *wsj1, char *text, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");AKA_fCall++;
		AKA_mark("lis===382###sois===8637###eois===8660###lif===2###soif===76###eoif===99###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");struct wsj1_call *call;

		AKA_mark("lis===383###sois===8662###eois===8687###lif===3###soif===101###eoif===126###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");struct afb_wsj1_msg *msg;


	/* allocate */
		AKA_mark("lis===386###sois===8706###eois===8744###lif===6###soif===145###eoif===183###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");msg = wsj1_msg_make(wsj1, text, size);

		if (AKA_mark("lis===387###sois===8750###eois===8761###lif===7###soif===189###eoif===200###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)") && (AKA_mark("lis===387###sois===8750###eois===8761###lif===7###soif===189###eoif===200###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)")&&msg == NULL)) {
				AKA_mark("lis===388###sois===8767###eois===8886###lif===8###soif===206###eoif===325###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");afb_ws_close(wsj1->ws, errno == EBADMSG
			? WEBSOCKET_CODE_POLICY_VIOLATION
			: WEBSOCKET_CODE_INTERNAL_ERROR, NULL);

				AKA_mark("lis===391###sois===8889###eois===8896###lif===11###soif===328###eoif===335###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");return;

	}
	else {AKA_mark("lis===-387-###sois===-8750-###eois===-875011-###lif===-7-###soif===-###eoif===-200-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");}


	/* handle the message */
		AKA_mark("lis===395###sois===8936###eois===8945###lif===15###soif===375###eoif===384###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");switch(msg->code){
			case CALL: if(msg->code == CALL)AKA_mark("lis===396###sois===8950###eois===8960###lif===16###soif===389###eoif===399###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");

				AKA_mark("lis===397###sois===8963###eois===9023###lif===17###soif===402###eoif===462###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");wsj1->itf->on_call(wsj1->closure, msg->api, msg->verb, msg);

				AKA_mark("lis===398###sois===9026###eois===9032###lif===18###soif===465###eoif===471###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");break;

			case RETOK: if(msg->code == RETOK)AKA_mark("lis===399###sois===9034###eois===9045###lif===19###soif===473###eoif===484###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");

			case RETERR: if(msg->code == RETERR)AKA_mark("lis===400###sois===9047###eois===9059###lif===20###soif===486###eoif===498###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");

				AKA_mark("lis===401###sois===9062###eois===9104###lif===21###soif===501###eoif===543###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");call = wsj1_call_search(wsj1, msg->id, 1);

				if (AKA_mark("lis===402###sois===9111###eois===9123###lif===22###soif===550###eoif===562###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)") && (AKA_mark("lis===402###sois===9111###eois===9123###lif===22###soif===550###eoif===562###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)")&&call == NULL)) {
			AKA_mark("lis===403###sois===9128###eois===9190###lif===23###soif===567###eoif===629###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");afb_ws_close(wsj1->ws, WEBSOCKET_CODE_POLICY_VIOLATION, NULL);
		}
		else {
			AKA_mark("lis===405###sois===9201###eois===9236###lif===25###soif===640###eoif===675###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");call->callback(call->closure, msg);
		}

				AKA_mark("lis===406###sois===9239###eois===9250###lif===26###soif===678###eoif===689###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");free(call);

				AKA_mark("lis===407###sois===9253###eois===9259###lif===27###soif===692###eoif===698###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");break;

			case EVENT: if(msg->code == EVENT)AKA_mark("lis===408###sois===9261###eois===9272###lif===28###soif===700###eoif===711###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");

				if (AKA_mark("lis===409###sois===9279###eois===9306###lif===29###soif===718###eoif===745###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)") && (AKA_mark("lis===409###sois===9279###eois===9306###lif===29###soif===718###eoif===745###isc===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)")&&wsj1->itf->on_event != NULL)) {
			AKA_mark("lis===410###sois===9311###eois===9363###lif===30###soif===750###eoif===802###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");wsj1->itf->on_event(wsj1->closure, msg->event, msg);
		}
		else {AKA_mark("lis===-409-###sois===-9279-###eois===-927927-###lif===-29-###soif===-###eoif===-745-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");}

				AKA_mark("lis===411###sois===9366###eois===9372###lif===31###soif===805###eoif===811###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");break;

	}

		AKA_mark("lis===413###sois===9377###eois===9401###lif===33###soif===816###eoif===840###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_on_text(struct afb_wsj1*,char*,size_t)");afb_wsj1_msg_unref(msg);

}

/** Instrumented function afb_wsj1_msg_addref(struct afb_wsj1_msg*) */
void afb_wsj1_msg_addref(struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_addref(struct afb_wsj1_msg*)");AKA_fCall++;
		if (AKA_mark("lis===418###sois===9463###eois===9474###lif===2###soif===58###eoif===69###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_addref(struct afb_wsj1_msg*)") && (AKA_mark("lis===418###sois===9463###eois===9474###lif===2###soif===58###eoif===69###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_addref(struct afb_wsj1_msg*)")&&msg != NULL)) {
		AKA_mark("lis===419###sois===9478###eois===9534###lif===3###soif===73###eoif===129###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_addref(struct afb_wsj1_msg*)");__atomic_add_fetch(&msg->refcount, 1, __ATOMIC_RELAXED);
	}
	else {AKA_mark("lis===-418-###sois===-9463-###eois===-946311-###lif===-2-###soif===-###eoif===-69-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_addref(struct afb_wsj1_msg*)");}

}

/** Instrumented function afb_wsj1_msg_unref(struct afb_wsj1_msg*) */
void afb_wsj1_msg_unref(struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)");AKA_fCall++;
		if (AKA_mark("lis===424###sois===9595###eois===9666###lif===2###soif===57###eoif===128###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)") && ((AKA_mark("lis===424###sois===9595###eois===9606###lif===2###soif===57###eoif===68###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)")&&msg != NULL)	&&(AKA_mark("lis===424###sois===9610###eois===9666###lif===2###soif===72###eoif===128###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)")&&!__atomic_sub_fetch(&msg->refcount, 1, __ATOMIC_RELAXED)))) {
		/* unlink the message */
				AKA_mark("lis===426###sois===9699###eois===9737###lif===4###soif===161###eoif===199###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)");pthread_mutex_lock(&msg->wsj1->mutex);

				if (AKA_mark("lis===427###sois===9744###eois===9761###lif===5###soif===206###eoif===223###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)") && (AKA_mark("lis===427###sois===9744###eois===9761###lif===5###soif===206###eoif===223###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)")&&msg->next != NULL)) {
			AKA_mark("lis===428###sois===9766###eois===9802###lif===6###soif===228###eoif===264###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)");msg->next->previous = msg->previous;
		}
		else {AKA_mark("lis===-427-###sois===-9744-###eois===-974417-###lif===-5-###soif===-###eoif===-223-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)");}

				if (AKA_mark("lis===429###sois===9809###eois===9830###lif===7###soif===271###eoif===292###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)") && (AKA_mark("lis===429###sois===9809###eois===9830###lif===7###soif===271###eoif===292###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)")&&msg->previous == NULL)) {
			AKA_mark("lis===430###sois===9835###eois===9867###lif===8###soif===297###eoif===329###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)");msg->wsj1->messages = msg->next;
		}
		else {
			AKA_mark("lis===432###sois===9878###eois===9910###lif===10###soif===340###eoif===372###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)");msg->previous->next = msg->next;
		}

				AKA_mark("lis===433###sois===9913###eois===9953###lif===11###soif===375###eoif===415###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)");pthread_mutex_unlock(&msg->wsj1->mutex);

		/* free ressources */
				AKA_mark("lis===435###sois===9980###eois===10006###lif===13###soif===442###eoif===468###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)");afb_wsj1_unref(msg->wsj1);

				AKA_mark("lis===436###sois===10009###eois===10040###lif===14###soif===471###eoif===502###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)");json_object_put(msg->object_j);

				AKA_mark("lis===437###sois===10043###eois===10059###lif===15###soif===505###eoif===521###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)");free(msg->text);

				AKA_mark("lis===438###sois===10062###eois===10072###lif===16###soif===524###eoif===534###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)");free(msg);

	}
	else {AKA_mark("lis===-424-###sois===-9595-###eois===-959571-###lif===-2-###soif===-###eoif===-128-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_unref(struct afb_wsj1_msg*)");}

}

/** Instrumented function afb_wsj1_msg_object_s(struct afb_wsj1_msg*) */
const char *afb_wsj1_msg_object_s(struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_s(struct afb_wsj1_msg*)");AKA_fCall++;
		AKA_mark("lis===444###sois===10142###eois===10163###lif===2###soif===63###eoif===84###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_s(struct afb_wsj1_msg*)");return msg->object_s;

}

/** Instrumented function afb_wsj1_msg_object_j(struct afb_wsj1_msg*) */
struct json_object *afb_wsj1_msg_object_j(struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)");AKA_fCall++;
		AKA_mark("lis===449###sois===10238###eois===10267###lif===2###soif===71###eoif===100###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)");enum json_tokener_error jerr;

		AKA_mark("lis===450###sois===10269###eois===10312###lif===3###soif===102###eoif===145###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)");struct json_object *object = msg->object_j;

		if (AKA_mark("lis===451###sois===10318###eois===10332###lif===4###soif===151###eoif===165###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)") && (AKA_mark("lis===451###sois===10318###eois===10332###lif===4###soif===151###eoif===165###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)")&&object == NULL)) {
				AKA_mark("lis===452###sois===10338###eois===10376###lif===5###soif===171###eoif===209###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)");pthread_mutex_lock(&msg->wsj1->mutex);

				AKA_mark("lis===453###sois===10379###eois===10418###lif===6###soif===212###eoif===251###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)");json_tokener_reset(msg->wsj1->tokener);

				AKA_mark("lis===454###sois===10421###eois===10518###lif===7###soif===254###eoif===351###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)");object = json_tokener_parse_ex(msg->wsj1->tokener, msg->object_s, 1 + (int)msg->object_s_length);

				AKA_mark("lis===455###sois===10521###eois===10571###lif===8###soif===354###eoif===404###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)");jerr = json_tokener_get_error(msg->wsj1->tokener);

				AKA_mark("lis===456###sois===10574###eois===10614###lif===9###soif===407###eoif===447###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)");pthread_mutex_unlock(&msg->wsj1->mutex);

				if (AKA_mark("lis===457###sois===10621###eois===10649###lif===10###soif===454###eoif===482###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)") && (AKA_mark("lis===457###sois===10621###eois===10649###lif===10###soif===454###eoif===482###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)")&&jerr != json_tokener_success)) {
			/* lazy error detection of json request. Is it to improve? */
						AKA_mark("lis===459###sois===10721###eois===10799###lif===12###soif===554###eoif===632###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)");object = json_object_new_string_len(msg->object_s, (int)msg->object_s_length);

		}
		else {AKA_mark("lis===-457-###sois===-10621-###eois===-1062128-###lif===-10-###soif===-###eoif===-482-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)");}

				AKA_mark("lis===461###sois===10806###eois===10829###lif===14###soif===639###eoif===662###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)");msg->object_j = object;

	}
	else {AKA_mark("lis===-451-###sois===-10318-###eois===-1031814-###lif===-4-###soif===-###eoif===-165-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)");}

		AKA_mark("lis===463###sois===10834###eois===10848###lif===16###soif===667###eoif===681###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_object_j(struct afb_wsj1_msg*)");return object;

}

int afb_wsj1_msg_is_call(struct afb_wsj1_msg *msg)
{
	return msg->code == CALL;
}

/** Instrumented function afb_wsj1_msg_is_reply(struct afb_wsj1_msg*) */
int afb_wsj1_msg_is_reply(struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_is_reply(struct afb_wsj1_msg*)");AKA_fCall++;
		AKA_mark("lis===473###sois===10990###eois===11039###lif===2###soif===55###eoif===104###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_is_reply(struct afb_wsj1_msg*)");return msg->code == RETOK || msg->code == RETERR;

}

/** Instrumented function afb_wsj1_msg_is_reply_ok(struct afb_wsj1_msg*) */
int afb_wsj1_msg_is_reply_ok(struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_is_reply_ok(struct afb_wsj1_msg*)");AKA_fCall++;
		AKA_mark("lis===478###sois===11101###eois===11127###lif===2###soif===58###eoif===84###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_is_reply_ok(struct afb_wsj1_msg*)");return msg->code == RETOK;

}

/** Instrumented function afb_wsj1_msg_is_reply_error(struct afb_wsj1_msg*) */
int afb_wsj1_msg_is_reply_error(struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_is_reply_error(struct afb_wsj1_msg*)");AKA_fCall++;
		AKA_mark("lis===483###sois===11192###eois===11219###lif===2###soif===61###eoif===88###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_is_reply_error(struct afb_wsj1_msg*)");return msg->code == RETERR;

}

/** Instrumented function afb_wsj1_msg_is_event(struct afb_wsj1_msg*) */
int afb_wsj1_msg_is_event(struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_is_event(struct afb_wsj1_msg*)");AKA_fCall++;
		AKA_mark("lis===488###sois===11278###eois===11304###lif===2###soif===55###eoif===81###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_is_event(struct afb_wsj1_msg*)");return msg->code == EVENT;

}

/** Instrumented function afb_wsj1_msg_api(struct afb_wsj1_msg*) */
const char *afb_wsj1_msg_api(struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_api(struct afb_wsj1_msg*)");AKA_fCall++;
		AKA_mark("lis===493###sois===11366###eois===11382###lif===2###soif===58###eoif===74###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_api(struct afb_wsj1_msg*)");return msg->api;

}

const char *afb_wsj1_msg_verb(struct afb_wsj1_msg *msg)
{
	return msg->verb;
}

/** Instrumented function afb_wsj1_msg_event(struct afb_wsj1_msg*) */
const char *afb_wsj1_msg_event(struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_event(struct afb_wsj1_msg*)");AKA_fCall++;
		AKA_mark("lis===503###sois===11526###eois===11544###lif===2###soif===60###eoif===78###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_event(struct afb_wsj1_msg*)");return msg->event;

}

/** Instrumented function afb_wsj1_msg_token(struct afb_wsj1_msg*) */
const char *afb_wsj1_msg_token(struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_token(struct afb_wsj1_msg*)");AKA_fCall++;
		AKA_mark("lis===508###sois===11608###eois===11626###lif===2###soif===60###eoif===78###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_token(struct afb_wsj1_msg*)");return msg->token;

}

/** Instrumented function afb_wsj1_msg_wsj1(struct afb_wsj1_msg*) */
struct afb_wsj1 *afb_wsj1_msg_wsj1(struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_wsj1(struct afb_wsj1_msg*)");AKA_fCall++;
		AKA_mark("lis===513###sois===11694###eois===11711###lif===2###soif===64###eoif===81###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_msg_wsj1(struct afb_wsj1_msg*)");return msg->wsj1;

}

/** Instrumented function afb_wsj1_close(struct afb_wsj1*,uint16_t,const char*) */
int afb_wsj1_close(struct afb_wsj1 *wsj1, uint16_t code, const char *text)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_close(struct afb_wsj1*,uint16_t,const char*)");AKA_fCall++;
		AKA_mark("lis===518###sois===11793###eois===11835###lif===2###soif===78###eoif===120###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_close(struct afb_wsj1*,uint16_t,const char*)");return afb_ws_close(wsj1->ws, code, text);

}

/** Instrumented function wsj1_send_isot(struct afb_wsj1*,int,const char*,const char*,const char*) */
static int wsj1_send_isot(struct afb_wsj1 *wsj1, int i1, const char *s1, const char *o1, const char *t1)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/wsj1_send_isot(struct afb_wsj1*,int,const char*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===523###sois===11947###eois===11986###lif===2###soif===108###eoif===147###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_send_isot(struct afb_wsj1*,int,const char*,const char*,const char*)");char code[2] = { (char)('0' + i1), 0 };

		AKA_mark("lis===524###sois===11988###eois===12116###lif===3###soif===149###eoif===277###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_send_isot(struct afb_wsj1*,int,const char*,const char*,const char*)");return afb_ws_texts(wsj1->ws, "[", code, ",\"", s1, "\",", o1 == NULL ? "null" : o1, t1 != NULL ? ",\"" : "]", t1, "\"]", NULL);

}

/** Instrumented function wsj1_send_issot(struct afb_wsj1*,int,const char*,const char*,const char*,const char*) */
static int wsj1_send_issot(struct afb_wsj1 *wsj1, int i1, const char *s1, const char *s2, const char *o1, const char *t1)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/wsj1_send_issot(struct afb_wsj1*,int,const char*,const char*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===529###sois===12245###eois===12284###lif===2###soif===125###eoif===164###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_send_issot(struct afb_wsj1*,int,const char*,const char*,const char*,const char*)");char code[2] = { (char)('0' + i1), 0 };

		AKA_mark("lis===530###sois===12286###eois===12427###lif===3###soif===166###eoif===307###ins===true###function===./app-framework-binder/src/afb-wsj1.c/wsj1_send_issot(struct afb_wsj1*,int,const char*,const char*,const char*,const char*)");return afb_ws_texts(wsj1->ws, "[", code, ",\"", s1, "\",\"", s2, "\",", o1 == NULL ? "null" : o1, t1 != NULL ? ",\"" : "]", t1, "\"]", NULL);

}

/** Instrumented function afb_wsj1_send_event_j(struct afb_wsj1*,const char*,struct json_object*) */
int afb_wsj1_send_event_j(struct afb_wsj1 *wsj1, const char *event, struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_send_event_j(struct afb_wsj1*,const char*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===535###sois===12530###eois===12645###lif===2###soif===99###eoif===214###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_send_event_j(struct afb_wsj1*,const char*,struct json_object*)");const char *objstr = json_object_to_json_string_ext(object, JSON_C_TO_STRING_PLAIN|JSON_C_TO_STRING_NOSLASHESCAPE);

		AKA_mark("lis===536###sois===12647###eois===12699###lif===3###soif===216###eoif===268###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_send_event_j(struct afb_wsj1*,const char*,struct json_object*)");int rc = afb_wsj1_send_event_s(wsj1, event, objstr);

		AKA_mark("lis===537###sois===12701###eois===12725###lif===4###soif===270###eoif===294###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_send_event_j(struct afb_wsj1*,const char*,struct json_object*)");json_object_put(object);

		AKA_mark("lis===538###sois===12727###eois===12737###lif===5###soif===296###eoif===306###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_send_event_j(struct afb_wsj1*,const char*,struct json_object*)");return rc;

}

/** Instrumented function afb_wsj1_send_event_s(struct afb_wsj1*,const char*,const char*) */
int afb_wsj1_send_event_s(struct afb_wsj1 *wsj1, const char *event, const char *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_send_event_s(struct afb_wsj1*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===543###sois===12832###eois===12888###lif===2###soif===91###eoif===147###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_send_event_s(struct afb_wsj1*,const char*,const char*)");return wsj1_send_isot(wsj1, EVENT, event, object, NULL);

}

/** Instrumented function afb_wsj1_call_j(struct afb_wsj1*,const char*,const char*,struct json_object*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*) */
int afb_wsj1_call_j(struct afb_wsj1 *wsj1, const char *api, const char *verb, struct json_object *object, void (*on_reply)(void *closure, struct afb_wsj1_msg *msg), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_j(struct afb_wsj1*,const char*,const char*,struct json_object*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");AKA_fCall++;
		AKA_mark("lis===548###sois===13075###eois===13190###lif===2###soif===183###eoif===298###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_j(struct afb_wsj1*,const char*,const char*,struct json_object*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");const char *objstr = json_object_to_json_string_ext(object, JSON_C_TO_STRING_PLAIN|JSON_C_TO_STRING_NOSLASHESCAPE);

		AKA_mark("lis===549###sois===13192###eois===13261###lif===3###soif===300###eoif===369###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_j(struct afb_wsj1*,const char*,const char*,struct json_object*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");int rc = afb_wsj1_call_s(wsj1, api, verb, objstr, on_reply, closure);

		AKA_mark("lis===550###sois===13263###eois===13287###lif===4###soif===371###eoif===395###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_j(struct afb_wsj1*,const char*,const char*,struct json_object*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");json_object_put(object);

		AKA_mark("lis===551###sois===13289###eois===13299###lif===5###soif===397###eoif===407###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_j(struct afb_wsj1*,const char*,const char*,struct json_object*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");return rc;

}

/** Instrumented function afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*) */
int afb_wsj1_call_s(struct afb_wsj1 *wsj1, const char *api, const char *verb, const char *object, void (*on_reply)(void *closure, struct afb_wsj1_msg *msg), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");AKA_fCall++;
		AKA_mark("lis===556###sois===13478###eois===13485###lif===2###soif===175###eoif===182###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");int rc;

		AKA_mark("lis===557###sois===13487###eois===13510###lif===3###soif===184###eoif===207###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");struct wsj1_call *call;

		AKA_mark("lis===558###sois===13512###eois===13522###lif===4###soif===209###eoif===219###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");char *tag;


	/* allocates the call */
		AKA_mark("lis===561###sois===13551###eois===13600###lif===7###soif===248###eoif===297###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");call = wsj1_call_create(wsj1, on_reply, closure);

		if (AKA_mark("lis===562###sois===13606###eois===13618###lif===8###soif===303###eoif===315###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)") && (AKA_mark("lis===562###sois===13606###eois===13618###lif===8###soif===303###eoif===315###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)")&&call == NULL)) {
				AKA_mark("lis===563###sois===13624###eois===13639###lif===9###soif===321###eoif===336###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");errno = ENOMEM;

				AKA_mark("lis===564###sois===13642###eois===13652###lif===10###soif===339###eoif===349###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");return -1;

	}
	else {AKA_mark("lis===-562-###sois===-13606-###eois===-1360612-###lif===-8-###soif===-###eoif===-315-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");}


	/* makes the tag */
		AKA_mark("lis===568###sois===13679###eois===13724###lif===14###soif===376###eoif===421###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");tag = alloca(2 + strlen(api) + strlen(verb));

		AKA_mark("lis===569###sois===13726###eois===13770###lif===15###soif===423###eoif===467###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");stpcpy(stpcpy(stpcpy(tag, api), "/"), verb);


	/* makes the call */
		AKA_mark("lis===572###sois===13795###eois===13857###lif===18###soif===492###eoif===554###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");rc = wsj1_send_issot(wsj1, CALL, call->id, tag, object, NULL);

		if (AKA_mark("lis===573###sois===13863###eois===13869###lif===19###soif===560###eoif===566###ifc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)") && (AKA_mark("lis===573###sois===13863###eois===13869###lif===19###soif===560###eoif===566###isc===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)")&&rc < 0)) {
				AKA_mark("lis===574###sois===13875###eois===13911###lif===20###soif===572###eoif===608###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");wsj1_call_search(wsj1, call->id, 1);

				AKA_mark("lis===575###sois===13914###eois===13925###lif===21###soif===611###eoif===622###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");free(call);

	}
	else {AKA_mark("lis===-573-###sois===-13863-###eois===-138636-###lif===-19-###soif===-###eoif===-566-###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");}

		AKA_mark("lis===577###sois===13930###eois===13940###lif===23###soif===627###eoif===637###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_call_s(struct afb_wsj1*,const char*,const char*,const char*,void(*on_reply)(void*closure, struct afb_wsj1_msg*msg),void*)");return rc;

}

/** Instrumented function afb_wsj1_reply_j(struct afb_wsj1_msg*,struct json_object*,const char*,int) */
int afb_wsj1_reply_j(struct afb_wsj1_msg *msg, struct json_object *object, const char *token, int iserror)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_reply_j(struct afb_wsj1_msg*,struct json_object*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===582###sois===14054###eois===14169###lif===2###soif===110###eoif===225###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_reply_j(struct afb_wsj1_msg*,struct json_object*,const char*,int)");const char *objstr = json_object_to_json_string_ext(object, JSON_C_TO_STRING_PLAIN|JSON_C_TO_STRING_NOSLASHESCAPE);

		AKA_mark("lis===583###sois===14171###eois===14226###lif===3###soif===227###eoif===282###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_reply_j(struct afb_wsj1_msg*,struct json_object*,const char*,int)");int rc = afb_wsj1_reply_s(msg, objstr, token, iserror);

		AKA_mark("lis===584###sois===14228###eois===14252###lif===4###soif===284###eoif===308###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_reply_j(struct afb_wsj1_msg*,struct json_object*,const char*,int)");json_object_put(object);

		AKA_mark("lis===585###sois===14254###eois===14264###lif===5###soif===310###eoif===320###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_reply_j(struct afb_wsj1_msg*,struct json_object*,const char*,int)");return rc;

}

/** Instrumented function afb_wsj1_reply_s(struct afb_wsj1_msg*,const char*,const char*,int) */
int afb_wsj1_reply_s(struct afb_wsj1_msg *msg, const char *object, const char *token, int iserror)
{AKA_mark("Calling: ./app-framework-binder/src/afb-wsj1.c/afb_wsj1_reply_s(struct afb_wsj1_msg*,const char*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===590###sois===14370###eois===14453###lif===2###soif===102###eoif===185###ins===true###function===./app-framework-binder/src/afb-wsj1.c/afb_wsj1_reply_s(struct afb_wsj1_msg*,const char*,const char*,int)");return wsj1_send_isot(msg->wsj1, iserror ? RETERR : RETOK, msg->id, object, token);

}


#endif

