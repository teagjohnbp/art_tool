/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_DEBUG_H
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_DEBUG_H
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 Copyright (C) 2015-2020 "IoT.bzh"

 author: José Bollo <jose.bollo@iot.bzh>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

#pragma once

#if AGL_DEVEL && !defined(AFB_INSERT_DEBUG_FEATURES)
# define AFB_INSERT_DEBUG_FEATURES
#endif

#if defined(AFB_INSERT_DEBUG_FEATURES)
extern void afb_debug(const char *key);
extern void afb_debug_wait(const char *key);
extern void afb_debug_break(const char *key);
#else
/** Instrumented function afb_debug(x) */
#define afb_debug(x)       ((void)0)
/** Instrumented function afb_debug_wait(x) */
#define afb_debug_wait(x)  ((void)0)
/** Instrumented function afb_debug_break(x) */
#define afb_debug_break(x) ((void)0)
#endif

#endif

