/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_CRED_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_CRED_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CRED_H_
#define AKA_INCLUDE__AFB_CRED_H_
#include "afb-cred.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif


#define MAX_LABEL_LENGTH  1024

#if !defined(NO_DEFAULT_PEERCRED) && !defined(ADD_DEFAULT_PEERCRED)
#  define NO_DEFAULT_PEERCRED
#endif

#if !defined(DEFAULT_PEERSEC_LABEL)
#  define DEFAULT_PEERSEC_LABEL "NoLabel"
#endif
#if !defined(DEFAULT_PEERCRED_UID)
#  define DEFAULT_PEERCRED_UID 99 /* nobody */
#endif
#if !defined(DEFAULT_PEERCRED_GID)
#  define DEFAULT_PEERCRED_GID 99 /* nobody */
#endif
#if !defined(DEFAULT_PEERCRED_PID)
#  define DEFAULT_PEERCRED_PID 0  /* no process */
#endif

static char export_format[] = "%x:%x:%x-%s";
static char import_format[] = "%x:%x:%x-%n";

static struct afb_cred *current;

/** Instrumented function mkcred(uid_t,gid_t,pid_t,const char*,size_t) */
static struct afb_cred *mkcred(uid_t uid, gid_t gid, pid_t pid, const char *label, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");AKA_fCall++;
		AKA_mark("lis===59###sois===1613###eois===1635###lif===2###soif===99###eoif===121###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");struct afb_cred *cred;

		AKA_mark("lis===60###sois===1637###eois===1658###lif===3###soif===123###eoif===144###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");char *dest, user[64];

		AKA_mark("lis===61###sois===1660###eois===1669###lif===4###soif===146###eoif===155###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");size_t i;

		AKA_mark("lis===62###sois===1671###eois===1679###lif===5###soif===157###eoif===165###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");uid_t u;


		AKA_mark("lis===64###sois===1682###eois===1688###lif===7###soif===168###eoif===174###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");i = 0;

		AKA_mark("lis===65###sois===1690###eois===1698###lif===8###soif===176###eoif===184###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");u = uid;

		do {
				AKA_mark("lis===67###sois===1707###eois===1740###lif===10###soif===193###eoif===226###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");user[i++] = (char)('0' + u % 10);

				AKA_mark("lis===68###sois===1743###eois===1754###lif===11###soif===229###eoif===240###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");u = u / 10;

	}
	while (AKA_mark("lis===69###sois===1764###eois===1784###lif===12###soif===250###eoif===270###ifc===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)") && ((AKA_mark("lis===69###sois===1764###eois===1765###lif===12###soif===250###eoif===251###isc===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)")&&u)	&&(AKA_mark("lis===69###sois===1769###eois===1784###lif===12###soif===255###eoif===270###isc===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)")&&i < sizeof user)));


		AKA_mark("lis===71###sois===1789###eois===1832###lif===14###soif===275###eoif===318###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");cred = malloc(2 + i + size + sizeof *cred);

		if (AKA_mark("lis===72###sois===1838###eois===1843###lif===15###soif===324###eoif===329###ifc===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)") && (AKA_mark("lis===72###sois===1838###eois===1843###lif===15###soif===324###eoif===329###isc===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)")&&!cred)) {
		AKA_mark("lis===73###sois===1847###eois===1862###lif===16###soif===333###eoif===348###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");errno = ENOMEM;
	}
	else {
				AKA_mark("lis===75###sois===1873###eois===1892###lif===18###soif===359###eoif===378###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");cred->refcount = 1;

				AKA_mark("lis===76###sois===1895###eois===1911###lif===19###soif===381###eoif===397###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");cred->uid = uid;

				AKA_mark("lis===77###sois===1914###eois===1930###lif===20###soif===400###eoif===416###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");cred->gid = gid;

				AKA_mark("lis===78###sois===1933###eois===1949###lif===21###soif===419###eoif===435###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");cred->pid = pid;

				AKA_mark("lis===79###sois===1952###eois===1974###lif===22###soif===438###eoif===460###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");cred->exported = NULL;

				AKA_mark("lis===80###sois===1977###eois===2002###lif===23###soif===463###eoif===488###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");dest = (char*)(&cred[1]);

				AKA_mark("lis===81###sois===2005###eois===2023###lif===24###soif===491###eoif===509###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");cred->user = dest;

				while (AKA_mark("lis===82###sois===2032###eois===2033###lif===25###soif===518###eoif===519###ifc===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)") && (AKA_mark("lis===82###sois===2032###eois===2033###lif===25###soif===518###eoif===519###isc===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)")&&i)) {
			AKA_mark("lis===83###sois===2038###eois===2058###lif===26###soif===524###eoif===544###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");*dest++ = user[--i];
		}

				AKA_mark("lis===84###sois===2061###eois===2073###lif===27###soif===547###eoif===559###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");*dest++ = 0;

				AKA_mark("lis===85###sois===2076###eois===2095###lif===28###soif===562###eoif===581###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");cred->label = dest;

				AKA_mark("lis===86###sois===2098###eois===2114###lif===29###soif===584###eoif===600###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");cred->id = dest;

				AKA_mark("lis===87###sois===2117###eois===2143###lif===30###soif===603###eoif===629###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");memcpy(dest, label, size);

				AKA_mark("lis===88###sois===2146###eois===2161###lif===31###soif===632###eoif===647###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");dest[size] = 0;

				AKA_mark("lis===89###sois===2164###eois===2190###lif===32###soif===650###eoif===676###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");dest = strrchr(dest, ':');

				if (AKA_mark("lis===90###sois===2197###eois===2201###lif===33###soif===683###eoif===687###ifc===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)") && (AKA_mark("lis===90###sois===2197###eois===2201###lif===33###soif===683###eoif===687###isc===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)")&&dest)) {
			AKA_mark("lis===91###sois===2206###eois===2226###lif===34###soif===692###eoif===712###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");cred->id = &dest[1];
		}
		else {AKA_mark("lis===-90-###sois===-2197-###eois===-21974-###lif===-33-###soif===-###eoif===-687-###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");}

	}

		AKA_mark("lis===93###sois===2231###eois===2243###lif===36###soif===717###eoif===729###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcred(uid_t,gid_t,pid_t,const char*,size_t)");return cred;

}

/** Instrumented function mkcurrent() */
static struct afb_cred *mkcurrent()
{AKA_mark("Calling: ./app-framework-binder/src/afb-cred.c/mkcurrent()");AKA_fCall++;
		AKA_mark("lis===98###sois===2286###eois===2315###lif===2###soif===39###eoif===68###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcurrent()");char label[MAX_LABEL_LENGTH];

		AKA_mark("lis===99###sois===2317###eois===2324###lif===3###soif===70###eoif===77###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcurrent()");int fd;

		AKA_mark("lis===100###sois===2326###eois===2337###lif===4###soif===79###eoif===90###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcurrent()");ssize_t rc;


		AKA_mark("lis===102###sois===2340###eois===2387###lif===6###soif===93###eoif===140###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcurrent()");fd = open("/proc/self/attr/current", O_RDONLY);

		if (AKA_mark("lis===103###sois===2393###eois===2399###lif===7###soif===146###eoif===152###ifc===true###function===./app-framework-binder/src/afb-cred.c/mkcurrent()") && (AKA_mark("lis===103###sois===2393###eois===2399###lif===7###soif===146###eoif===152###isc===true###function===./app-framework-binder/src/afb-cred.c/mkcurrent()")&&fd < 0)) {
		AKA_mark("lis===104###sois===2403###eois===2410###lif===8###soif===156###eoif===163###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcurrent()");rc = 0;
	}
	else {
				AKA_mark("lis===106###sois===2421###eois===2456###lif===10###soif===174###eoif===209###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcurrent()");rc = read(fd, label, sizeof label);

				if (AKA_mark("lis===107###sois===2463###eois===2469###lif===11###soif===216###eoif===222###ifc===true###function===./app-framework-binder/src/afb-cred.c/mkcurrent()") && (AKA_mark("lis===107###sois===2463###eois===2469###lif===11###soif===216###eoif===222###isc===true###function===./app-framework-binder/src/afb-cred.c/mkcurrent()")&&rc < 0)) {
			AKA_mark("lis===108###sois===2474###eois===2481###lif===12###soif===227###eoif===234###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcurrent()");rc = 0;
		}
		else {AKA_mark("lis===-107-###sois===-2463-###eois===-24636-###lif===-11-###soif===-###eoif===-222-###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcurrent()");}

				AKA_mark("lis===109###sois===2484###eois===2494###lif===13###soif===237###eoif===247###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcurrent()");close(fd);

	}


		AKA_mark("lis===112###sois===2500###eois===2563###lif===16###soif===253###eoif===316###ins===true###function===./app-framework-binder/src/afb-cred.c/mkcurrent()");return mkcred(getuid(), getgid(), getpid(), label, (size_t)rc);

}

/** Instrumented function afb_cred_create(uid_t,gid_t,pid_t,const char*) */
struct afb_cred *afb_cred_create(uid_t uid, gid_t gid, pid_t pid, const char *label)
{AKA_mark("Calling: ./app-framework-binder/src/afb-cred.c/afb_cred_create(uid_t,gid_t,pid_t,const char*)");AKA_fCall++;
		AKA_mark("lis===117###sois===2655###eois===2695###lif===2###soif===88###eoif===128###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create(uid_t,gid_t,pid_t,const char*)");label = label ? : DEFAULT_PEERSEC_LABEL;

		AKA_mark("lis===118###sois===2697###eois===2748###lif===3###soif===130###eoif===181###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create(uid_t,gid_t,pid_t,const char*)");return mkcred(uid, gid, pid, label, strlen(label));

}

/** Instrumented function afb_cred_create_for_socket(int) */
struct afb_cred *afb_cred_create_for_socket(int fd)
{AKA_mark("Calling: ./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");AKA_fCall++;
		AKA_mark("lis===123###sois===2807###eois===2814###lif===2###soif===55###eoif===62###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");int rc;

		AKA_mark("lis===124###sois===2816###eois===2833###lif===3###soif===64###eoif===81###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");socklen_t length;

		AKA_mark("lis===125###sois===2835###eois===2854###lif===4###soif===83###eoif===102###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");struct ucred ucred;

		AKA_mark("lis===126###sois===2856###eois===2885###lif===5###soif===104###eoif===133###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");char label[MAX_LABEL_LENGTH];


	/* get the credentials */
		AKA_mark("lis===129###sois===2915###eois===2950###lif===8###soif===163###eoif===198###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");length = (socklen_t)(sizeof ucred);

		AKA_mark("lis===130###sois===2952###eois===3014###lif===9###soif===200###eoif===262###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");rc = getsockopt(fd, SOL_SOCKET, SO_PEERCRED, &ucred, &length);

		if (AKA_mark("lis===131###sois===3020###eois===3080###lif===10###soif===268###eoif===328###ifc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)") && (((AKA_mark("lis===131###sois===3020###eois===3026###lif===10###soif===268###eoif===274###isc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)")&&rc < 0)	||(AKA_mark("lis===131###sois===3030###eois===3065###lif===10###soif===278###eoif===313###isc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)")&&length != (socklen_t)(sizeof ucred)))	||(AKA_mark("lis===131###sois===3069###eois===3080###lif===10###soif===317###eoif===328###isc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)")&&!~ucred.uid))) {
#if !defined(NO_DEFAULT_PEERCRED)
		ucred.uid = DEFAULT_PEERCRED_UID;
		ucred.gid = DEFAULT_PEERCRED_GID;
		ucred.pid = DEFAULT_PEERCRED_PID;
#else
				if (AKA_mark("lis===137###sois===3238###eois===3241###lif===16###soif===486###eoif===489###ifc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)") && (AKA_mark("lis===137###sois===3238###eois===3241###lif===16###soif===486###eoif===489###isc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)")&&!rc)) {
			AKA_mark("lis===138###sois===3246###eois===3261###lif===17###soif===494###eoif===509###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");errno = EINVAL;
		}
		else {AKA_mark("lis===-137-###sois===-3238-###eois===-32383-###lif===-16-###soif===-###eoif===-489-###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");}

				AKA_mark("lis===139###sois===3264###eois===3276###lif===18###soif===512###eoif===524###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");return NULL;

#endif
	}
	else {AKA_mark("lis===-131-###sois===-3020-###eois===-302060-###lif===-10-###soif===-###eoif===-328-###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");}


	/* get the security label */
		AKA_mark("lis===144###sois===3319###eois===3354###lif===23###soif===567###eoif===602###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");length = (socklen_t)(sizeof label);

		AKA_mark("lis===145###sois===3356###eois===3416###lif===24###soif===604###eoif===664###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");rc = getsockopt(fd, SOL_SOCKET, SO_PEERSEC, label, &length);

		if (AKA_mark("lis===146###sois===3422###eois===3466###lif===25###soif===670###eoif===714###ifc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)") && ((AKA_mark("lis===146###sois===3422###eois===3428###lif===25###soif===670###eoif===676###isc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)")&&rc < 0)	||(AKA_mark("lis===146###sois===3432###eois===3466###lif===25###soif===680###eoif===714###isc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)")&&length > (socklen_t)(sizeof label)))) {
#if !defined(NO_DEFAULT_PEERSEC)
				AKA_mark("lis===148###sois===3505###eois===3555###lif===27###soif===753###eoif===803###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");length = (socklen_t)strlen(DEFAULT_PEERSEC_LABEL);

				AKA_mark("lis===149###sois===3558###eois===3596###lif===28###soif===806###eoif===844###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");strcpy (label, DEFAULT_PEERSEC_LABEL);

#else
		if (!rc)
			errno = EINVAL;
		return NULL;
#endif
	}
	else {AKA_mark("lis===-146-###sois===-3422-###eois===-342244-###lif===-25-###soif===-###eoif===-714-###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");}


	/* makes the result */
		AKA_mark("lis===158###sois===3684###eois===3754###lif===37###soif===932###eoif===1002###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_create_for_socket(int)");return mkcred(ucred.uid, ucred.gid, ucred.pid, label, (size_t)length);

}

/** Instrumented function afb_cred_addref(struct afb_cred*) */
struct afb_cred *afb_cred_addref(struct afb_cred *cred)
{AKA_mark("Calling: ./app-framework-binder/src/afb-cred.c/afb_cred_addref(struct afb_cred*)");AKA_fCall++;
		if (AKA_mark("lis===163###sois===3821###eois===3825###lif===2###soif===63###eoif===67###ifc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_addref(struct afb_cred*)") && (AKA_mark("lis===163###sois===3821###eois===3825###lif===2###soif===63###eoif===67###isc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_addref(struct afb_cred*)")&&cred)) {
		AKA_mark("lis===164###sois===3829###eois===3886###lif===3###soif===71###eoif===128###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_addref(struct afb_cred*)");__atomic_add_fetch(&cred->refcount, 1, __ATOMIC_RELAXED);
	}
	else {AKA_mark("lis===-163-###sois===-3821-###eois===-38214-###lif===-2-###soif===-###eoif===-67-###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_addref(struct afb_cred*)");}

		AKA_mark("lis===165###sois===3888###eois===3900###lif===4###soif===130###eoif===142###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_addref(struct afb_cred*)");return cred;

}

/** Instrumented function afb_cred_unref(struct afb_cred*) */
void afb_cred_unref(struct afb_cred *cred)
{AKA_mark("Calling: ./app-framework-binder/src/afb-cred.c/afb_cred_unref(struct afb_cred*)");AKA_fCall++;
		if (AKA_mark("lis===170###sois===3954###eois===4019###lif===2###soif===50###eoif===115###ifc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_unref(struct afb_cred*)") && ((AKA_mark("lis===170###sois===3954###eois===3958###lif===2###soif===50###eoif===54###isc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_unref(struct afb_cred*)")&&cred)	&&(AKA_mark("lis===170###sois===3962###eois===4019###lif===2###soif===58###eoif===115###isc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_unref(struct afb_cred*)")&&!__atomic_sub_fetch(&cred->refcount, 1, __ATOMIC_RELAXED)))) {
				if (AKA_mark("lis===171###sois===4029###eois===4044###lif===3###soif===125###eoif===140###ifc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_unref(struct afb_cred*)") && (AKA_mark("lis===171###sois===4029###eois===4044###lif===3###soif===125###eoif===140###isc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_unref(struct afb_cred*)")&&cred == current)) {
			AKA_mark("lis===172###sois===4049###eois===4068###lif===4###soif===145###eoif===164###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_unref(struct afb_cred*)");cred->refcount = 1;
		}
		else {
						AKA_mark("lis===174###sois===4081###eois===4109###lif===6###soif===177###eoif===205###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_unref(struct afb_cred*)");free((void*)cred->exported);

						AKA_mark("lis===175###sois===4113###eois===4124###lif===7###soif===209###eoif===220###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_unref(struct afb_cred*)");free(cred);

		}

	}
	else {AKA_mark("lis===-170-###sois===-3954-###eois===-395465-###lif===-2-###soif===-###eoif===-115-###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_unref(struct afb_cred*)");}

}

/** Instrumented function afb_cred_current() */
struct afb_cred *afb_cred_current()
{AKA_mark("Calling: ./app-framework-binder/src/afb-cred.c/afb_cred_current()");AKA_fCall++;
		if (AKA_mark("lis===182###sois===4178###eois===4186###lif===2###soif===43###eoif===51###ifc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_current()") && (AKA_mark("lis===182###sois===4178###eois===4186###lif===2###soif===43###eoif===51###isc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_current()")&&!current)) {
		AKA_mark("lis===183###sois===4190###eois===4212###lif===3###soif===55###eoif===77###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_current()");current = mkcurrent();
	}
	else {AKA_mark("lis===-182-###sois===-4178-###eois===-41788-###lif===-2-###soif===-###eoif===-51-###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_current()");}

		AKA_mark("lis===184###sois===4214###eois===4246###lif===4###soif===79###eoif===111###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_current()");return afb_cred_addref(current);

}

/** Instrumented function afb_cred_export(struct afb_cred*) */
const char *afb_cred_export(struct afb_cred *cred)
{AKA_mark("Calling: ./app-framework-binder/src/afb-cred.c/afb_cred_export(struct afb_cred*)");AKA_fCall++;
		AKA_mark("lis===189###sois===4304###eois===4311###lif===2###soif===54###eoif===61###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_export(struct afb_cred*)");int rc;


		if (AKA_mark("lis===191###sois===4318###eois===4333###lif===4###soif===68###eoif===83###ifc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_export(struct afb_cred*)") && (AKA_mark("lis===191###sois===4318###eois===4333###lif===4###soif===68###eoif===83###isc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_export(struct afb_cred*)")&&!cred->exported)) {
				AKA_mark("lis===192###sois===4339###eois===4473###lif===5###soif===89###eoif===223###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_export(struct afb_cred*)");rc = asprintf((char**)&cred->exported,
			export_format,
				(int)cred->uid,
				(int)cred->gid,
				(int)cred->pid,
				cred->label);

				if (AKA_mark("lis===198###sois===4480###eois===4486###lif===11###soif===230###eoif===236###ifc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_export(struct afb_cred*)") && (AKA_mark("lis===198###sois===4480###eois===4486###lif===11###soif===230###eoif===236###isc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_export(struct afb_cred*)")&&rc < 0)) {
						AKA_mark("lis===199###sois===4493###eois===4508###lif===12###soif===243###eoif===258###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_export(struct afb_cred*)");errno = ENOMEM;

						AKA_mark("lis===200###sois===4512###eois===4534###lif===13###soif===262###eoif===284###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_export(struct afb_cred*)");cred->exported = NULL;

		}
		else {AKA_mark("lis===-198-###sois===-4480-###eois===-44806-###lif===-11-###soif===-###eoif===-236-###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_export(struct afb_cred*)");}

	}
	else {AKA_mark("lis===-191-###sois===-4318-###eois===-431815-###lif===-4-###soif===-###eoif===-83-###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_export(struct afb_cred*)");}

		AKA_mark("lis===203###sois===4543###eois===4565###lif===16###soif===293###eoif===315###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_export(struct afb_cred*)");return cred->exported;

}

/** Instrumented function afb_cred_import(const char*) */
struct afb_cred *afb_cred_import(const char *string)
{AKA_mark("Calling: ./app-framework-binder/src/afb-cred.c/afb_cred_import(const char*)");AKA_fCall++;
		AKA_mark("lis===208###sois===4625###eois===4647###lif===2###soif===56###eoif===78###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_import(const char*)");struct afb_cred *cred;

		AKA_mark("lis===209###sois===4649###eois===4676###lif===3###soif===80###eoif===107###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_import(const char*)");int rc, uid, gid, pid, pos;


		AKA_mark("lis===211###sois===4679###eois===4738###lif===5###soif===110###eoif===169###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_import(const char*)");rc = sscanf(string, import_format, &uid, &gid, &pid, &pos);

		if (AKA_mark("lis===212###sois===4744###eois===4751###lif===6###soif===175###eoif===182###ifc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_import(const char*)") && (AKA_mark("lis===212###sois===4744###eois===4751###lif===6###soif===175###eoif===182###isc===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_import(const char*)")&&rc == 3)) {
		AKA_mark("lis===213###sois===4755###eois===4828###lif===7###soif===186###eoif===259###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_import(const char*)");cred = afb_cred_create((uid_t)uid, (gid_t)gid, (pid_t)pid, &string[pos]);
	}
	else {
				AKA_mark("lis===215###sois===4839###eois===4854###lif===9###soif===270###eoif===285###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_import(const char*)");errno = EINVAL;

				AKA_mark("lis===216###sois===4857###eois===4869###lif===10###soif===288###eoif===300###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_import(const char*)");cred = NULL;

	}

		AKA_mark("lis===218###sois===4874###eois===4886###lif===12###soif===305###eoif===317###ins===true###function===./app-framework-binder/src/afb-cred.c/afb_cred_import(const char*)");return cred;

}

#endif

