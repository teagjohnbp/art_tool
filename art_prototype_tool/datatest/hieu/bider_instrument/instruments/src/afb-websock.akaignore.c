/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_WEBSOCK_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_WEBSOCK_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <string.h>

#include <openssl/sha.h>
#include <microhttpd.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_METHOD_H_
#define AKA_INCLUDE__AFB_METHOD_H_
#include "afb-method.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CONTEXT_H_
#define AKA_INCLUDE__AFB_CONTEXT_H_
#include "afb-context.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HREQ_H_
#define AKA_INCLUDE__AFB_HREQ_H_
#include "afb-hreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_WEBSOCK_H_
#define AKA_INCLUDE__AFB_WEBSOCK_H_
#include "afb-websock.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_WS_JSON1_H_
#define AKA_INCLUDE__AFB_WS_JSON1_H_
#include "afb-ws-json1.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_FDEV_H_
#define AKA_INCLUDE__AFB_FDEV_H_
#include "afb-fdev.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_H_
#define AKA_INCLUDE__FDEV_H_
#include "fdev.akaignore.h"
#endif


/**************** WebSocket connection upgrade ****************************/

static const char websocket_s[] = "websocket";
static const char sec_websocket_key_s[] = "Sec-WebSocket-Key";
static const char sec_websocket_version_s[] = "Sec-WebSocket-Version";
static const char sec_websocket_accept_s[] = "Sec-WebSocket-Accept";
static const char sec_websocket_protocol_s[] = "Sec-WebSocket-Protocol";
static const char websocket_guid[] = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

/** Instrumented function enc64(unsigned char*,char*) */
static void enc64(unsigned char *in, char *out)
{AKA_mark("Calling: ./app-framework-binder/src/afb-websock.c/enc64(unsigned char*,char*)");AKA_fCall++;
		AKA_mark("lis===47###sois===1488###eois===1595###lif===2###soif===51###eoif===158###ins===true###function===./app-framework-binder/src/afb-websock.c/enc64(unsigned char*,char*)");static const char tob64[] =
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz"
		"0123456789+/";

		AKA_mark("lis===51###sois===1597###eois===1624###lif===6###soif===160###eoif===187###ins===true###function===./app-framework-binder/src/afb-websock.c/enc64(unsigned char*,char*)");out[0] = tob64[in[0] >> 2];

		AKA_mark("lis===52###sois===1626###eois===1688###lif===7###soif===189###eoif===251###ins===true###function===./app-framework-binder/src/afb-websock.c/enc64(unsigned char*,char*)");out[1] = tob64[((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4)];

		AKA_mark("lis===53###sois===1690###eois===1752###lif===8###soif===253###eoif===315###ins===true###function===./app-framework-binder/src/afb-websock.c/enc64(unsigned char*,char*)");out[2] = tob64[((in[1] & 0x0f) << 2) | ((in[2] & 0xc0) >> 6)];

		AKA_mark("lis===54###sois===1754###eois===1783###lif===9###soif===317###eoif===346###ins===true###function===./app-framework-binder/src/afb-websock.c/enc64(unsigned char*,char*)");out[3] = tob64[in[2] & 0x3f];

}

/** Instrumented function make_accept_value(const char*,char[29]) */
static void make_accept_value(const char *key, char result[29])
{AKA_mark("Calling: ./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");AKA_fCall++;
		AKA_mark("lis===59###sois===1854###eois===1892###lif===2###soif===67###eoif===105###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");unsigned char md[SHA_DIGEST_LENGTH+1];

		AKA_mark("lis===60###sois===1894###eois===1919###lif===3###soif===107###eoif===132###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");size_t len = strlen(key);

		AKA_mark("lis===61###sois===1921###eois===1976###lif===4###soif===134###eoif===189###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");char *buffer = alloca(len + sizeof websocket_guid - 1);

		AKA_mark("lis===62###sois===1978###eois===2003###lif===5###soif===191###eoif===216###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");memcpy(buffer, key, len);

		AKA_mark("lis===63###sois===2005###eois===2069###lif===6###soif===218###eoif===282###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");memcpy(buffer + len, websocket_guid, sizeof websocket_guid - 1);

		AKA_mark("lis===64###sois===2071###eois===2161###lif===7###soif===284###eoif===374###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");SHA1((const unsigned char *)buffer, (unsigned long)(len + sizeof websocket_guid - 1), md);

		AKA_mark("lis===65###sois===2163###eois===2195###lif===8###soif===376###eoif===408###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");assert(SHA_DIGEST_LENGTH == 20);

		AKA_mark("lis===66###sois===2197###eois===2208###lif===9###soif===410###eoif===421###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");md[20] = 0;

		AKA_mark("lis===67###sois===2210###eois===2236###lif===10###soif===423###eoif===449###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");enc64(&md[0], &result[0]);

		AKA_mark("lis===68###sois===2238###eois===2264###lif===11###soif===451###eoif===477###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");enc64(&md[3], &result[4]);

		AKA_mark("lis===69###sois===2266###eois===2292###lif===12###soif===479###eoif===505###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");enc64(&md[6], &result[8]);

		AKA_mark("lis===70###sois===2294###eois===2321###lif===13###soif===507###eoif===534###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");enc64(&md[9], &result[12]);

		AKA_mark("lis===71###sois===2323###eois===2351###lif===14###soif===536###eoif===564###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");enc64(&md[12], &result[16]);

		AKA_mark("lis===72###sois===2353###eois===2381###lif===15###soif===566###eoif===594###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");enc64(&md[15], &result[20]);

		AKA_mark("lis===73###sois===2383###eois===2411###lif===16###soif===596###eoif===624###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");enc64(&md[18], &result[24]);

		AKA_mark("lis===74###sois===2413###eois===2430###lif===17###soif===626###eoif===643###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");result[27] = '=';

		AKA_mark("lis===75###sois===2432###eois===2447###lif===18###soif===645###eoif===660###ins===true###function===./app-framework-binder/src/afb-websock.c/make_accept_value(const char*,char[29])");result[28] = 0;

}

static const char vseparators[] = " \t,";

/** Instrumented function headerhas(const char*,const char*) */
static int headerhas(const char *header, const char *needle)
{AKA_mark("Calling: ./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===82###sois===2558###eois===2572###lif===2###soif===64###eoif===78###ins===true###function===./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)");size_t len, n;


		AKA_mark("lis===84###sois===2575###eois===2594###lif===4###soif===81###eoif===100###ins===true###function===./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)");n = strlen(needle);

		for (;;) {
				AKA_mark("lis===86###sois===2608###eois===2646###lif===6###soif===114###eoif===152###ins===true###function===./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)");header += strspn(header, vseparators);

				if (AKA_mark("lis===87###sois===2653###eois===2661###lif===7###soif===159###eoif===167###ifc===true###function===./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)") && (AKA_mark("lis===87###sois===2653###eois===2661###lif===7###soif===159###eoif===167###isc===true###function===./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)")&&!*header)) {
			AKA_mark("lis===88###sois===2666###eois===2675###lif===8###soif===172###eoif===181###ins===true###function===./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)");return 0;
		}
		else {AKA_mark("lis===-87-###sois===-2653-###eois===-26538-###lif===-7-###soif===-###eoif===-167-###ins===true###function===./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)");}

				AKA_mark("lis===89###sois===2678###eois===2713###lif===9###soif===184###eoif===219###ins===true###function===./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)");len = strcspn(header, vseparators);

				if (AKA_mark("lis===90###sois===2720###eois===2767###lif===10###soif===226###eoif===273###ifc===true###function===./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)") && ((AKA_mark("lis===90###sois===2720###eois===2728###lif===10###soif===226###eoif===234###isc===true###function===./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)")&&n == len)	&&(AKA_mark("lis===90###sois===2732###eois===2767###lif===10###soif===238###eoif===273###isc===true###function===./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)")&&0 == strncasecmp(needle, header, n)))) {
			AKA_mark("lis===91###sois===2772###eois===2781###lif===11###soif===278###eoif===287###ins===true###function===./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)");return 1;
		}
		else {AKA_mark("lis===-90-###sois===-2720-###eois===-272047-###lif===-10-###soif===-###eoif===-273-###ins===true###function===./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)");}

				AKA_mark("lis===92###sois===2784###eois===2798###lif===12###soif===290###eoif===304###ins===true###function===./app-framework-binder/src/afb-websock.c/headerhas(const char*,const char*)");header += len;

	}

}

struct protodef
{
	const char *name;
	void *(*create)(struct fdev *fdev, struct afb_apiset *apiset, struct afb_context *context, void (*cleanup)(void*), void *cleanup_closure);
};

/** Instrumented function search_proto(const struct protodef*,const char*) */
static const struct protodef *search_proto(const struct protodef *protodefs, const char *protocols)
{AKA_mark("Calling: ./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)");AKA_fCall++;
		AKA_mark("lis===104###sois===3089###eois===3095###lif===2###soif===103###eoif===109###ins===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)");int i;

		AKA_mark("lis===105###sois===3097###eois===3108###lif===3###soif===111###eoif===122###ins===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)");size_t len;


		if (AKA_mark("lis===107###sois===3115###eois===3132###lif===5###soif===129###eoif===146###ifc===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)") && (AKA_mark("lis===107###sois===3115###eois===3132###lif===5###soif===129###eoif===146###isc===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)")&&protocols == NULL)) {
		/* return NULL; */
				AKA_mark("lis===109###sois===3159###eois===3230###lif===7###soif===173###eoif===244###ins===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)");return protodefs != NULL && protodefs->name != NULL ? protodefs : NULL;

	}
	else {AKA_mark("lis===-107-###sois===-3115-###eois===-311517-###lif===-5-###soif===-###eoif===-146-###ins===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)");}

		for (;;) {
				AKA_mark("lis===112###sois===3247###eois===3291###lif===10###soif===261###eoif===305###ins===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)");protocols += strspn(protocols, vseparators);

				if (AKA_mark("lis===113###sois===3298###eois===3309###lif===11###soif===312###eoif===323###ifc===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)") && (AKA_mark("lis===113###sois===3298###eois===3309###lif===11###soif===312###eoif===323###isc===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)")&&!*protocols)) {
			AKA_mark("lis===114###sois===3314###eois===3326###lif===12###soif===328###eoif===340###ins===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)");return NULL;
		}
		else {AKA_mark("lis===-113-###sois===-3298-###eois===-329811-###lif===-11-###soif===-###eoif===-323-###ins===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)");}

				AKA_mark("lis===115###sois===3329###eois===3367###lif===13###soif===343###eoif===381###ins===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)");len = strcspn(protocols, vseparators);

				AKA_mark("lis===116###sois===3375###eois===3382###lif===14###soif===389###eoif===396###ins===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)");for (i = 0 ;AKA_mark("lis===116###sois===3383###eois===3408###lif===14###soif===397###eoif===422###ifc===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)") && AKA_mark("lis===116###sois===3383###eois===3408###lif===14###soif===397###eoif===422###isc===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)")&&protodefs[i].name != NULL;({AKA_mark("lis===116###sois===3411###eois===3414###lif===14###soif===425###eoif===428###ins===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)");i++;})) {
			if (AKA_mark("lis===117###sois===3423###eois===3501###lif===15###soif===437###eoif===515###ifc===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)") && ((AKA_mark("lis===117###sois===3423###eois===3470###lif===15###soif===437###eoif===484###isc===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)")&&!strncasecmp(protodefs[i].name, protocols, len))	&&(AKA_mark("lis===118###sois===3478###eois===3501###lif===16###soif===492###eoif===515###isc===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)")&&!protodefs[i].name[len]))) {
				AKA_mark("lis===119###sois===3507###eois===3528###lif===17###soif===521###eoif===542###ins===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)");return &protodefs[i];
			}
			else {AKA_mark("lis===-117-###sois===-3423-###eois===-342378-###lif===-15-###soif===-###eoif===-515-###ins===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)");}
		}

				AKA_mark("lis===120###sois===3531###eois===3548###lif===18###soif===545###eoif===562###ins===true###function===./app-framework-binder/src/afb-websock.c/search_proto(const struct protodef*,const char*)");protocols += len;

	}

}

struct memo_websocket {
	const struct protodef *proto;
	struct afb_hreq *hreq;
	struct afb_apiset *apiset;
};

/** Instrumented function close_websocket(void*) */
static void close_websocket(void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-websock.c/close_websocket(void*)");AKA_fCall++;
		AKA_mark("lis===132###sois===3712###eois===3760###lif===2###soif===46###eoif===94###ins===true###function===./app-framework-binder/src/afb-websock.c/close_websocket(void*)");struct MHD_UpgradeResponseHandle *urh = closure;

		AKA_mark("lis===133###sois===3762###eois===3813###lif===3###soif===96###eoif===147###ins===true###function===./app-framework-binder/src/afb-websock.c/close_websocket(void*)");MHD_upgrade_action (urh, MHD_UPGRADE_ACTION_CLOSE);

}

/** Instrumented function upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*) */
static void upgrade_to_websocket(
			void *cls,
			struct MHD_Connection *connection,
			void *con_cls,
			const char *extra_in,
			size_t extra_in_size,
			MHD_socket sock,
			struct MHD_UpgradeResponseHandle *urh)
{AKA_mark("Calling: ./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)");AKA_fCall++;
		AKA_mark("lis===145###sois===4036###eois===4070###lif===9###soif===219###eoif===253###ins===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)");struct memo_websocket *memo = cls;

		AKA_mark("lis===146###sois===4072###eois===4081###lif===10###soif===255###eoif===264###ins===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)");void *ws;

		AKA_mark("lis===147###sois===4083###eois===4101###lif===11###soif===266###eoif===284###ins===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)");struct fdev *fdev;


		AKA_mark("lis===149###sois===4104###eois===4133###lif===13###soif===287###eoif===316###ins===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)");fdev = afb_fdev_create(sock);

		if (AKA_mark("lis===150###sois===4139###eois===4144###lif===14###soif===322###eoif===327###ifc===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)") && (AKA_mark("lis===150###sois===4139###eois===4144###lif===14###soif===322###eoif===327###isc===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)")&&!fdev)) {
		/* TODO */
				AKA_mark("lis===152###sois===4163###eois===4184###lif===16###soif===346###eoif===367###ins===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)");close_websocket(urh);

	}
	else {
				AKA_mark("lis===154###sois===4197###eois===4225###lif===18###soif===380###eoif===408###ins===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)");fdev_set_autoclose(fdev, 0);

				AKA_mark("lis===155###sois===4228###eois===4322###lif===19###soif===411###eoif===505###ins===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)");ws = memo->proto->create(fdev, memo->apiset, &memo->hreq->xreq.context, close_websocket, urh);

				if (AKA_mark("lis===156###sois===4329###eois===4339###lif===20###soif===512###eoif===522###ifc===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)") && (AKA_mark("lis===156###sois===4329###eois===4339###lif===20###soif===512###eoif===522###isc===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)")&&ws == NULL)) {
			/* TODO */
						AKA_mark("lis===158###sois===4360###eois===4381###lif===22###soif===543###eoif===564###ins===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)");close_websocket(urh);

		}
		else {AKA_mark("lis===-156-###sois===-4329-###eois===-432910-###lif===-20-###soif===-###eoif===-522-###ins===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)");}

	}

#if MHD_VERSION <= 0x00095900
		AKA_mark("lis===162###sois===4420###eois===4447###lif===26###soif===603###eoif===630###ins===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)");afb_hreq_unref(memo->hreq);

#endif
		AKA_mark("lis===164###sois===4456###eois===4467###lif===28###soif===639###eoif===650###ins===true###function===./app-framework-binder/src/afb-websock.c/upgrade_to_websocket(void*,struct MHD_Connection*,void*,const char*,size_t,MHD_socket,struct MHD_UpgradeResponseHandle*)");free(memo);

}

/** Instrumented function check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*) */
static int check_websocket_upgrade(struct MHD_Connection *con, const struct protodef *protodefs, struct afb_hreq *hreq, struct afb_apiset *apiset)
{AKA_mark("Calling: ./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===169###sois===4621###eois===4649###lif===2###soif===150###eoif===178###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");struct memo_websocket *memo;

		AKA_mark("lis===170###sois===4651###eois===4681###lif===3###soif===180###eoif===210###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");struct MHD_Response *response;

		AKA_mark("lis===171###sois===4683###eois===4744###lif===4###soif===212###eoif===273###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");const char *connection, *upgrade, *key, *version, *protocols;

		AKA_mark("lis===172###sois===4746###eois===4765###lif===5###soif===275###eoif===294###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");char acceptval[29];

		AKA_mark("lis===173###sois===4767###eois===4778###lif===6###soif===296###eoif===307###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");int vernum;

		AKA_mark("lis===174###sois===4780###eois===4809###lif===7###soif===309###eoif===338###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");const struct protodef *proto;


	/* is an upgrade to websocket ? */
		AKA_mark("lis===177###sois===4848###eois===4933###lif===10###soif===377###eoif===462###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");upgrade = MHD_lookup_connection_value(con, MHD_HEADER_KIND, MHD_HTTP_HEADER_UPGRADE);

		if (AKA_mark("lis===178###sois===4939###eois===4990###lif===11###soif===468###eoif===519###ifc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)") && ((AKA_mark("lis===178###sois===4939###eois===4954###lif===11###soif===468###eoif===483###isc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)")&&upgrade == NULL)	||(AKA_mark("lis===178###sois===4958###eois===4990###lif===11###soif===487###eoif===519###isc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)")&&strcasecmp(upgrade, websocket_s)))) {
		AKA_mark("lis===179###sois===4994###eois===5003###lif===12###soif===523###eoif===532###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");return 0;
	}
	else {AKA_mark("lis===-178-###sois===-4939-###eois===-493951-###lif===-11-###soif===-###eoif===-519-###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");}


	/* is a connection for upgrade ? */
		AKA_mark("lis===182###sois===5043###eois===5134###lif===15###soif===572###eoif===663###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");connection = MHD_lookup_connection_value(con, MHD_HEADER_KIND, MHD_HTTP_HEADER_CONNECTION);

		if (AKA_mark("lis===183###sois===5140###eois===5212###lif===16###soif===669###eoif===741###ifc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)") && ((AKA_mark("lis===183###sois===5140###eois===5158###lif===16###soif===669###eoif===687###isc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)")&&connection == NULL)	||(AKA_mark("lis===184###sois===5164###eois===5212###lif===17###soif===693###eoif===741###isc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)")&&!headerhas (connection, MHD_HTTP_HEADER_UPGRADE)))) {
		AKA_mark("lis===185###sois===5216###eois===5225###lif===18###soif===745###eoif===754###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");return 0;
	}
	else {AKA_mark("lis===-183-###sois===-5140-###eois===-514072-###lif===-16-###soif===-###eoif===-741-###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");}


	/* has a key and a version ? */
		AKA_mark("lis===188###sois===5261###eois===5338###lif===21###soif===790###eoif===867###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");key = MHD_lookup_connection_value(con, MHD_HEADER_KIND, sec_websocket_key_s);

		AKA_mark("lis===189###sois===5340###eois===5425###lif===22###soif===869###eoif===954###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");version = MHD_lookup_connection_value(con, MHD_HEADER_KIND, sec_websocket_version_s);

		if (AKA_mark("lis===190###sois===5431###eois===5461###lif===23###soif===960###eoif===990###ifc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)") && ((AKA_mark("lis===190###sois===5431###eois===5442###lif===23###soif===960###eoif===971###isc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)")&&key == NULL)	||(AKA_mark("lis===190###sois===5446###eois===5461###lif===23###soif===975###eoif===990###isc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)")&&version == NULL))) {
		AKA_mark("lis===191###sois===5465###eois===5474###lif===24###soif===994###eoif===1003###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");return 0;
	}
	else {AKA_mark("lis===-190-###sois===-5431-###eois===-543130-###lif===-23-###soif===-###eoif===-990-###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");}


	/* is a supported version ? */
		AKA_mark("lis===194###sois===5509###eois===5532###lif===27###soif===1038###eoif===1061###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");vernum = atoi(version);

		if (AKA_mark("lis===195###sois===5538###eois===5550###lif===28###soif===1067###eoif===1079###ifc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)") && (AKA_mark("lis===195###sois===5538###eois===5550###lif===28###soif===1067###eoif===1079###isc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)")&&vernum != 13)) {
				AKA_mark("lis===196###sois===5556###eois===5632###lif===29###soif===1085###eoif===1161###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");response = MHD_create_response_from_buffer(0, NULL, MHD_RESPMEM_PERSISTENT);

				AKA_mark("lis===197###sois===5635###eois===5700###lif===30###soif===1164###eoif===1229###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");MHD_add_response_header(response, sec_websocket_version_s, "13");

				AKA_mark("lis===198###sois===5703###eois===5764###lif===31###soif===1232###eoif===1293###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");MHD_queue_response(con, MHD_HTTP_UPGRADE_REQUIRED, response);

				AKA_mark("lis===199###sois===5767###eois===5798###lif===32###soif===1296###eoif===1327###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");MHD_destroy_response(response);

				AKA_mark("lis===200###sois===5801###eois===5810###lif===33###soif===1330###eoif===1339###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");return 1;

	}
	else {AKA_mark("lis===-195-###sois===-5538-###eois===-553812-###lif===-28-###soif===-###eoif===-1079-###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");}


	/* is the protocol supported ? */
		AKA_mark("lis===204###sois===5851###eois===5939###lif===37###soif===1380###eoif===1468###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");protocols = MHD_lookup_connection_value(con, MHD_HEADER_KIND, sec_websocket_protocol_s);

		AKA_mark("lis===205###sois===5941###eois===5984###lif===38###soif===1470###eoif===1513###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");proto = search_proto(protodefs, protocols);

		if (AKA_mark("lis===206###sois===5990###eois===6003###lif===39###soif===1519###eoif===1532###ifc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)") && (AKA_mark("lis===206###sois===5990###eois===6003###lif===39###soif===1519###eoif===1532###isc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)")&&proto == NULL)) {
				AKA_mark("lis===207###sois===6009###eois===6085###lif===40###soif===1538###eoif===1614###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");response = MHD_create_response_from_buffer(0, NULL, MHD_RESPMEM_PERSISTENT);

				AKA_mark("lis===208###sois===6088###eois===6152###lif===41###soif===1617###eoif===1681###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");MHD_queue_response(con, MHD_HTTP_PRECONDITION_FAILED, response);

				AKA_mark("lis===209###sois===6155###eois===6186###lif===42###soif===1684###eoif===1715###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");MHD_destroy_response(response);

				AKA_mark("lis===210###sois===6189###eois===6198###lif===43###soif===1718###eoif===1727###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");return 1;

	}
	else {AKA_mark("lis===-206-###sois===-5990-###eois===-599013-###lif===-39-###soif===-###eoif===-1532-###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");}


	/* record context */
		AKA_mark("lis===214###sois===6226###eois===6254###lif===47###soif===1755###eoif===1783###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");memo = malloc(sizeof *memo);

		if (AKA_mark("lis===215###sois===6260###eois===6272###lif===48###soif===1789###eoif===1801###ifc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)") && (AKA_mark("lis===215###sois===6260###eois===6272###lif===48###soif===1789###eoif===1801###isc===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)")&&memo == NULL)) {
				AKA_mark("lis===216###sois===6278###eois===6354###lif===49###soif===1807###eoif===1883###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");response = MHD_create_response_from_buffer(0, NULL, MHD_RESPMEM_PERSISTENT);

				AKA_mark("lis===217###sois===6357###eois===6423###lif===50###soif===1886###eoif===1952###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");MHD_queue_response(con, MHD_HTTP_INTERNAL_SERVER_ERROR, response);

				AKA_mark("lis===218###sois===6426###eois===6457###lif===51###soif===1955###eoif===1986###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");MHD_destroy_response(response);

				AKA_mark("lis===219###sois===6460###eois===6469###lif===52###soif===1989###eoif===1998###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");return 1;

	}
	else {AKA_mark("lis===-215-###sois===-6260-###eois===-626012-###lif===-48-###soif===-###eoif===-1801-###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");}

		AKA_mark("lis===221###sois===6474###eois===6494###lif===54###soif===2003###eoif===2023###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");memo->proto = proto;

		AKA_mark("lis===222###sois===6496###eois===6514###lif===55###soif===2025###eoif===2043###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");memo->hreq = hreq;

		AKA_mark("lis===223###sois===6516###eois===6538###lif===56###soif===2045###eoif===2067###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");memo->apiset = apiset;


	/* send the accept connection */
		AKA_mark("lis===226###sois===6575###eois===6646###lif===59###soif===2104###eoif===2175###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");response = MHD_create_response_for_upgrade(upgrade_to_websocket, memo);

		AKA_mark("lis===227###sois===6648###eois===6682###lif===60###soif===2177###eoif===2211###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");make_accept_value(key, acceptval);

		AKA_mark("lis===228###sois===6684###eois===6753###lif===61###soif===2213###eoif===2282###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");MHD_add_response_header(response, sec_websocket_accept_s, acceptval);

		AKA_mark("lis===229###sois===6755###eois===6828###lif===62###soif===2284###eoif===2357###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");MHD_add_response_header(response, sec_websocket_protocol_s, proto->name);

		AKA_mark("lis===230###sois===6830###eois===6902###lif===63###soif===2359###eoif===2431###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");MHD_add_response_header(response, MHD_HTTP_HEADER_UPGRADE, websocket_s);

		AKA_mark("lis===231###sois===6904###eois===6968###lif===64###soif===2433###eoif===2497###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");MHD_queue_response(con, MHD_HTTP_SWITCHING_PROTOCOLS, response);

		AKA_mark("lis===232###sois===6970###eois===7001###lif===65###soif===2499###eoif===2530###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");MHD_destroy_response(response);


		AKA_mark("lis===234###sois===7004###eois===7013###lif===67###soif===2533###eoif===2542###ins===true###function===./app-framework-binder/src/afb-websock.c/check_websocket_upgrade(struct MHD_Connection*,const struct protodef*,struct afb_hreq*,struct afb_apiset*)");return 1;

}

static const struct protodef protodefs[] = {
	{ "x-afb-ws-json1",	(void*)afb_ws_json1_create },
	{ NULL, NULL }
};

/** Instrumented function afb_websock_check_upgrade(struct afb_hreq*,struct afb_apiset*) */
int afb_websock_check_upgrade(struct afb_hreq *hreq, struct afb_apiset *apiset)
{AKA_mark("Calling: ./app-framework-binder/src/afb-websock.c/afb_websock_check_upgrade(struct afb_hreq*,struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===244###sois===7216###eois===7223###lif===2###soif===83###eoif===90###ins===true###function===./app-framework-binder/src/afb-websock.c/afb_websock_check_upgrade(struct afb_hreq*,struct afb_apiset*)");int rc;


	/* is a get ? */
		if (AKA_mark("lis===247###sois===7248###eois===7331###lif===5###soif===115###eoif===198###ifc===true###function===./app-framework-binder/src/afb-websock.c/afb_websock_check_upgrade(struct afb_hreq*,struct afb_apiset*)") && ((AKA_mark("lis===247###sois===7248###eois===7278###lif===5###soif===115###eoif===145###isc===true###function===./app-framework-binder/src/afb-websock.c/afb_websock_check_upgrade(struct afb_hreq*,struct afb_apiset*)")&&hreq->method != afb_method_get)	||(AKA_mark("lis===248###sois===7284###eois===7331###lif===6###soif===151###eoif===198###isc===true###function===./app-framework-binder/src/afb-websock.c/afb_websock_check_upgrade(struct afb_hreq*,struct afb_apiset*)")&&strcasecmp(hreq->version, MHD_HTTP_VERSION_1_1)))) {
		AKA_mark("lis===249###sois===7335###eois===7344###lif===7###soif===202###eoif===211###ins===true###function===./app-framework-binder/src/afb-websock.c/afb_websock_check_upgrade(struct afb_hreq*,struct afb_apiset*)");return 0;
	}
	else {AKA_mark("lis===-247-###sois===-7248-###eois===-724883-###lif===-5-###soif===-###eoif===-198-###ins===true###function===./app-framework-binder/src/afb-websock.c/afb_websock_check_upgrade(struct afb_hreq*,struct afb_apiset*)");}


		AKA_mark("lis===251###sois===7347###eois===7419###lif===9###soif===214###eoif===286###ins===true###function===./app-framework-binder/src/afb-websock.c/afb_websock_check_upgrade(struct afb_hreq*,struct afb_apiset*)");rc = check_websocket_upgrade(hreq->connection, protodefs, hreq, apiset);

		if (AKA_mark("lis===252###sois===7425###eois===7432###lif===10###soif===292###eoif===299###ifc===true###function===./app-framework-binder/src/afb-websock.c/afb_websock_check_upgrade(struct afb_hreq*,struct afb_apiset*)") && (AKA_mark("lis===252###sois===7425###eois===7432###lif===10###soif===292###eoif===299###isc===true###function===./app-framework-binder/src/afb-websock.c/afb_websock_check_upgrade(struct afb_hreq*,struct afb_apiset*)")&&rc == 1)) {
				AKA_mark("lis===253###sois===7438###eois===7456###lif===11###soif===305###eoif===323###ins===true###function===./app-framework-binder/src/afb-websock.c/afb_websock_check_upgrade(struct afb_hreq*,struct afb_apiset*)");hreq->replied = 1;

	}
	else {AKA_mark("lis===-252-###sois===-7425-###eois===-74257-###lif===-10-###soif===-###eoif===-299-###ins===true###function===./app-framework-binder/src/afb-websock.c/afb_websock_check_upgrade(struct afb_hreq*,struct afb_apiset*)");}

		AKA_mark("lis===255###sois===7461###eois===7471###lif===13###soif===328###eoif===338###ins===true###function===./app-framework-binder/src/afb-websock.c/afb_websock_check_upgrade(struct afb_hreq*,struct afb_apiset*)");return rc;

}


#endif

