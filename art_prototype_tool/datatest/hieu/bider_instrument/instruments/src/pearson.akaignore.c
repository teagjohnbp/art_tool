/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_PEARSON_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_PEARSON_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>

/*
 * Returns a tiny hash value for the 'text'.
 *
 * Tiny hash function inspired from pearson
 */
/** Instrumented function pearson4(const char*) */
uint8_t pearson4(const char *text)
{AKA_mark("Calling: ./app-framework-binder/src/pearson.c/pearson4(const char*)");AKA_fCall++;
		AKA_mark("lis===27###sois===802###eois===897###lif===2###soif===38###eoif===133###ins===true###function===./app-framework-binder/src/pearson.c/pearson4(const char*)");static uint8_t T[16] = {
		 4,  1,  6,  0,  9, 14, 11,  5,
		 2,  3, 12, 15, 10,  7,  8, 13
	};

		AKA_mark("lis===31###sois===899###eois===912###lif===6###soif===135###eoif===148###ins===true###function===./app-framework-binder/src/pearson.c/pearson4(const char*)");uint8_t r, c;


		AKA_mark("lis===33###sois===920###eois===926###lif===8###soif===156###eoif===162###ins===true###function===./app-framework-binder/src/pearson.c/pearson4(const char*)");for (r = 0;AKA_mark("lis===33###sois===928###eois===946###lif===8###soif===164###eoif===182###ifc===true###function===./app-framework-binder/src/pearson.c/pearson4(const char*)") && c=((uint8_t)*text) && AKA_mark("lis===33###sois===928###eois===946###lif===8###soif===164###eoif===182###isc===true###function===./app-framework-binder/src/pearson.c/pearson4(const char*)");({AKA_mark("lis===33###sois===950###eois===956###lif===8###soif===186###eoif===192###ins===true###function===./app-framework-binder/src/pearson.c/pearson4(const char*)");text++;})) {
				AKA_mark("lis===34###sois===962###eois===982###lif===9###soif===198###eoif===218###ins===true###function===./app-framework-binder/src/pearson.c/pearson4(const char*)");r = T[r ^ (15 & c)];

				AKA_mark("lis===35###sois===985###eois===1005###lif===10###soif===221###eoif===241###ins===true###function===./app-framework-binder/src/pearson.c/pearson4(const char*)");r = T[r ^ (c >> 4)];

	}

		AKA_mark("lis===37###sois===1010###eois===1019###lif===12###soif===246###eoif===255###ins===true###function===./app-framework-binder/src/pearson.c/pearson4(const char*)");return r;
 // % HEADCOUNT;
}


#endif

