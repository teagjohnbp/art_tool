/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_MAIN_AFB_CLIENT_DEMO_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_MAIN_AFB_CLIENT_DEMO_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author "Fulup Ar Foll"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>

#include <systemd/sd-event.h>
#include <json-c/json.h>
#if !defined(JSON_C_TO_STRING_NOSLASHESCAPE)
#define JSON_C_TO_STRING_NOSLASHESCAPE 0
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_WSJ1_H_
#define AKA_INCLUDE__AFB_WSJ1_H_
#include "afb-wsj1.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_WS_CLIENT_H_
#define AKA_INCLUDE__AFB_WS_CLIENT_H_
#include "afb-ws-client.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_PROTO_WS_H_
#define AKA_INCLUDE__AFB_PROTO_WS_H_
#include "afb-proto-ws.akaignore.h"
#endif


enum {
	Exit_Success      = 0,
	Exit_Error        = 1,
	Exit_HangUp       = 2,
	Exit_Input_Fail   = 3,
	Exit_Bad_Arg      = 4,
	Exit_Cant_Connect = 5
};


/* declaration of functions */
static void on_wsj1_hangup(void *closure, struct afb_wsj1 *wsj1);
static void on_wsj1_call(void *closure, const char *api, const char *verb, struct afb_wsj1_msg *msg);
static void on_wsj1_event(void *closure, const char *event, struct afb_wsj1_msg *msg);

static void on_pws_hangup(void *closure);
static void on_pws_reply(void *closure, void *request, struct json_object *result, const char *error, const char *info);
static void on_pws_event_create(void *closure, uint16_t event_id, const char *event_name);
static void on_pws_event_remove(void *closure, uint16_t event_id);
static void on_pws_event_subscribe(void *closure, void *request, uint16_t event_id);
static void on_pws_event_unsubscribe(void *closure, void *request, uint16_t event_id);
static void on_pws_event_push(void *closure, uint16_t event_id, struct json_object *data);
static void on_pws_event_broadcast(void *closure, const char *event_name, struct json_object *data, const afb_proto_ws_uuid_t uuid, uint8_t hop);

static void idle();
static int process_stdin();
static int on_stdin(sd_event_source *src, int fd, uint32_t revents, void *closure);

static void wsj1_emit(const char *api, const char *verb, const char *object);
static void pws_call(const char *verb, const char *object);

/* the callback interface for wsj1 */
static struct afb_wsj1_itf wsj1_itf = {
	.on_hangup = on_wsj1_hangup,
	.on_call = on_wsj1_call,
	.on_event = on_wsj1_event
};

/* the callback interface for pws */
static struct afb_proto_ws_client_itf pws_itf = {
	.on_reply = on_pws_reply,
	.on_event_create = on_pws_event_create,
	.on_event_remove = on_pws_event_remove,
	.on_event_subscribe = on_pws_event_subscribe,
	.on_event_unsubscribe = on_pws_event_unsubscribe,
	.on_event_push = on_pws_event_push,
	.on_event_broadcast = on_pws_event_broadcast,
};

/* global variables */
static struct afb_wsj1 *wsj1;
static struct afb_proto_ws *pws;
static int breakcon;
static int exonrep;
static int callcount;
static int human;
static int raw;
static int keeprun;
static int direct;
static int echo;
static int synchro;
static int usein;
static sd_event *loop;
static sd_event_source *evsrc;
static char *uuid;
static char *token;
static uint16_t numuuid;
static uint16_t numtoken;
static char *url;
static int exitcode = 0;

/* print usage of the program */
/** Instrumented function usage(int,char*) */
static void usage(int status, char *arg0)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/usage(int,char*)");AKA_fCall++;
		AKA_mark("lis===115###sois===3633###eois===3665###lif===2###soif===45###eoif===77###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/usage(int,char*)");char *name = strrchr(arg0, '/');

		AKA_mark("lis===116###sois===3667###eois===3697###lif===3###soif===79###eoif===109###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/usage(int,char*)");name = name ? name + 1 : arg0;

		AKA_mark("lis===117###sois===3699###eois===3789###lif===4###soif===111###eoif===201###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/usage(int,char*)");fprintf(status ? stderr : stdout, "usage: %s [options]... uri [api verb [data]]\n", name);

		AKA_mark("lis===118###sois===3791###eois===3880###lif===5###soif===203###eoif===292###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/usage(int,char*)");fprintf(status ? stderr : stdout, "       %s -d [options]... uri [verb [data]]\n", name);

		AKA_mark("lis===119###sois===3882###eois===4675###lif===6###soif===294###eoif===1087###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/usage(int,char*)");fprintf(status ? stderr : stdout, "\n"
		"allowed options\n"
		"  -b, --break         Break connection just after event/call has been emitted.\n"
		"  -d, --direct        Direct api\n"
		"  -e, --echo          Echo inputs\n"
		"  -h, --help          Display this help\n"
		"  -H, --human         Display human readable JSON\n"
		"  -k, --keep-running  Keep running until disconnect, even if input closed\n"
		"  -p, --pipe COUNT    Allow to pipe COUNT requests\n"
		"  -r, --raw           Raw output (default)\n"
		"  -s, --sync          Synchronous: wait for answers (like -p 1)\n"
		"  -t, --token TOKEN   The token to use\n"
		"  -u, --uuid UUID     The identifier of session to use\n"
		"Example:\n"
		" %s --human 'localhost:1234/api?token=HELLO&uuid=magic' hello ping\n"
		"\n", name
	);


		AKA_mark("lis===137###sois===4678###eois===4691###lif===24###soif===1090###eoif===1103###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/usage(int,char*)");exit(status);

}

/* entry function */
/** Instrumented function main(int,char**,char**) */
int AKA_MAIN(int ac, char **av, char **env)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");AKA_fCall++;
		AKA_mark("lis===143###sois===4759###eois===4766###lif===2###soif===43###eoif===50###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");int rc;

		AKA_mark("lis===144###sois===4768###eois===4782###lif===3###soif===52###eoif===66###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");char *a0, *an;


	/* get the program name */
		AKA_mark("lis===147###sois===4813###eois===4824###lif===6###soif===97###eoif===108###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");a0 = av[0];


	/* check options */
		while (AKA_mark("lis===150###sois===4855###eois===4887###lif===9###soif===139###eoif===171###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && ((AKA_mark("lis===150###sois===4855###eois===4861###lif===9###soif===139###eoif===145###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&ac > 1)	&&(AKA_mark("lis===150###sois===4865###eois===4887###lif===9###soif===149###eoif===171###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&(an = av[1])[0] == '-'))) {
				if (AKA_mark("lis===151###sois===4897###eois===4909###lif===10###soif===181###eoif===193###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===151###sois===4897###eois===4909###lif===10###soif===181###eoif===193###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&an[1] == '-')) {
			/* long option */

						if (AKA_mark("lis===154###sois===4942###eois===4964###lif===13###soif===226###eoif===248###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===154###sois===4942###eois===4964###lif===13###soif===226###eoif===248###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&!strcmp(an, "--human"))) {
				AKA_mark("lis===155###sois===5001###eois===5011###lif===14###soif===285###eoif===295###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");human = 1;
			}
			else {
				if (AKA_mark("lis===157###sois===5025###eois===5045###lif===16###soif===309###eoif===329###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===157###sois===5025###eois===5045###lif===16###soif===309###eoif===329###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&!strcmp(an, "--raw"))) {
					AKA_mark("lis===158###sois===5080###eois===5088###lif===17###soif===364###eoif===372###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");raw = 1;
				}
				else {
					if (AKA_mark("lis===160###sois===5102###eois===5125###lif===19###soif===386###eoif===409###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===160###sois===5102###eois===5125###lif===19###soif===386###eoif===409###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&!strcmp(an, "--direct"))) {
						AKA_mark("lis===161###sois===5160###eois===5171###lif===20###soif===444###eoif===455###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");direct = 1;
					}
					else {
						if (AKA_mark("lis===163###sois===5185###eois===5207###lif===22###soif===469###eoif===491###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===163###sois===5185###eois===5207###lif===22###soif===469###eoif===491###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&!strcmp(an, "--break"))) {
							AKA_mark("lis===164###sois===5247###eois===5260###lif===23###soif===531###eoif===544###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");breakcon = 1;
						}
						else {
							if (AKA_mark("lis===166###sois===5274###eois===5303###lif===25###soif===558###eoif===587###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===166###sois===5274###eois===5303###lif===25###soif===558###eoif===587###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&!strcmp(an, "--keep-running"))) {
								AKA_mark("lis===167###sois===5343###eois===5355###lif===26###soif===627###eoif===639###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");keeprun = 1;
							}
							else {
								if (AKA_mark("lis===169###sois===5369###eois===5390###lif===28###soif===653###eoif===674###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===169###sois===5369###eois===5390###lif===28###soif===653###eoif===674###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&!strcmp(an, "--sync"))) {
									AKA_mark("lis===170###sois===5430###eois===5442###lif===29###soif===714###eoif===726###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");synchro = 1;
								}
								else {
									if (AKA_mark("lis===172###sois===5456###eois===5477###lif===31###soif===740###eoif===761###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===172###sois===5456###eois===5477###lif===31###soif===740###eoif===761###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&!strcmp(an, "--echo"))) {
										AKA_mark("lis===173###sois===5512###eois===5521###lif===32###soif===796###eoif===805###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");echo = 1;
									}
									else {
										if (AKA_mark("lis===175###sois===5535###eois===5584###lif===34###soif===819###eoif===868###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (((AKA_mark("lis===175###sois===5535###eois===5556###lif===34###soif===819###eoif===840###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&!strcmp(an, "--pipe"))	&&(AKA_mark("lis===175###sois===5560###eois===5565###lif===34###soif===844###eoif===849###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&av[2]))	&&(AKA_mark("lis===175###sois===5569###eois===5584###lif===34###soif===853###eoif===868###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&atoi(av[2]) > 0))) {
															AKA_mark("lis===176###sois===5592###eois===5614###lif===35###soif===876###eoif===898###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");synchro = atoi(av[2]);

															AKA_mark("lis===177###sois===5619###eois===5624###lif===36###soif===903###eoif===908###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");		AKA_mark("lis===212###sois===6662###eois===6667###lif===71###soif===1946###eoif===1951###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");av++;


															AKA_mark("lis===178###sois===5629###eois===5634###lif===37###soif===913###eoif===918###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");		AKA_mark("lis===213###sois===6670###eois===6675###lif===72###soif===1954###eoif===1959###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");ac--;


			}
										else {
											if (AKA_mark("lis===180###sois===5652###eois===5683###lif===39###soif===936###eoif===967###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && ((AKA_mark("lis===180###sois===5652###eois===5674###lif===39###soif===936###eoif===958###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&!strcmp(an, "--token"))	&&(AKA_mark("lis===180###sois===5678###eois===5683###lif===39###soif===962###eoif===967###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&av[2]))) { /* token to use */
																AKA_mark("lis===181###sois===5710###eois===5724###lif===40###soif===994###eoif===1008###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");token = av[2];

																AKA_mark("lis===182###sois===5729###eois===5734###lif===41###soif===1013###eoif===1018###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");av++;

																AKA_mark("lis===183###sois===5739###eois===5744###lif===42###soif===1023###eoif===1028###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");ac--;

			}
											else {
												if (AKA_mark("lis===185###sois===5762###eois===5792###lif===44###soif===1046###eoif===1076###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && ((AKA_mark("lis===185###sois===5762###eois===5783###lif===44###soif===1046###eoif===1067###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&!strcmp(an, "--uuid"))	&&(AKA_mark("lis===185###sois===5787###eois===5792###lif===44###soif===1071###eoif===1076###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&av[2]))) { /* session id to join */
																	AKA_mark("lis===186###sois===5825###eois===5838###lif===45###soif===1109###eoif===1122###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");uuid = av[2];

																	AKA_mark("lis===187###sois===5843###eois===5848###lif===46###soif===1127###eoif===1132###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");av++;

																	AKA_mark("lis===188###sois===5853###eois===5858###lif===47###soif===1137###eoif===1142###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");ac--;

			}
												else {
													AKA_mark("lis===192###sois===5905###eois===5967###lif===51###soif===1189###eoif===1251###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");usage(strcmp(an, "--help") ? Exit_Bad_Arg : Exit_Success, a0);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}

		}
		else {
			/* short option(s) */
						AKA_mark("lis===195###sois===6012###eois===6020###lif===54###soif===1296###eoif===1304###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");for (rc = 1 ;AKA_mark("lis===195###sois===6021###eois===6027###lif===54###soif===1305###eoif===1311###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && AKA_mark("lis===195###sois===6021###eois===6027###lif===54###soif===1305###eoif===1311###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&an[rc];({AKA_mark("lis===195###sois===6030###eois===6034###lif===54###soif===1314###eoif===1318###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");rc++;})) {
				AKA_mark("lis===196###sois===6048###eois===6054###lif===55###soif===1332###eoif===1338###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");switch(an[rc]){
									case 'H': if(an[rc] == 'H')AKA_mark("lis===197###sois===6062###eois===6071###lif===56###soif===1346###eoif===1355###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");
 					AKA_mark("lis===197###sois===6072###eois===6082###lif===56###soif===1356###eoif===1366###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");human = 1;
 					AKA_mark("lis===197###sois===6083###eois===6089###lif===56###soif===1367###eoif===1373###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");break;

									case 'r': if(an[rc] == 'r')AKA_mark("lis===198###sois===6094###eois===6103###lif===57###soif===1378###eoif===1387###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");
 					AKA_mark("lis===198###sois===6104###eois===6112###lif===57###soif===1388###eoif===1396###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");raw = 1;
 					AKA_mark("lis===198###sois===6113###eois===6119###lif===57###soif===1397###eoif===1403###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");break;

									case 'd': if(an[rc] == 'd')AKA_mark("lis===199###sois===6124###eois===6133###lif===58###soif===1408###eoif===1417###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");
 					AKA_mark("lis===199###sois===6134###eois===6145###lif===58###soif===1418###eoif===1429###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");direct = 1;
 					AKA_mark("lis===199###sois===6146###eois===6152###lif===58###soif===1430###eoif===1436###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");break;

									case 'b': if(an[rc] == 'b')AKA_mark("lis===200###sois===6157###eois===6166###lif===59###soif===1441###eoif===1450###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");
 					AKA_mark("lis===200###sois===6167###eois===6180###lif===59###soif===1451###eoif===1464###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");breakcon = 1;
 					AKA_mark("lis===200###sois===6181###eois===6187###lif===59###soif===1465###eoif===1471###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");break;

									case 'k': if(an[rc] == 'k')AKA_mark("lis===201###sois===6192###eois===6201###lif===60###soif===1476###eoif===1485###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");
 					AKA_mark("lis===201###sois===6202###eois===6214###lif===60###soif===1486###eoif===1498###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");keeprun = 1;
 					AKA_mark("lis===201###sois===6215###eois===6221###lif===60###soif===1499###eoif===1505###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");break;

									case 's': if(an[rc] == 's')AKA_mark("lis===202###sois===6226###eois===6235###lif===61###soif===1510###eoif===1519###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");
 					AKA_mark("lis===202###sois===6236###eois===6248###lif===61###soif===1520###eoif===1532###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");synchro = 1;
 					AKA_mark("lis===202###sois===6249###eois===6255###lif===61###soif===1533###eoif===1539###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");break;

									case 'e': if(an[rc] == 'e')AKA_mark("lis===203###sois===6260###eois===6269###lif===62###soif===1544###eoif===1553###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");
 					AKA_mark("lis===203###sois===6270###eois===6279###lif===62###soif===1554###eoif===1563###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");echo = 1;
 					AKA_mark("lis===203###sois===6280###eois===6286###lif===62###soif===1564###eoif===1570###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");break;

									case 't': if(an[rc] == 't')AKA_mark("lis===204###sois===6291###eois===6300###lif===63###soif===1575###eoif===1584###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");
 					if (AKA_mark("lis===204###sois===6305###eois===6311###lif===63###soif===1589###eoif===1595###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===204###sois===6305###eois===6311###lif===63###soif===1589###eoif===1595###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&!av[2])) {
						AKA_mark("lis===204###sois===6313###eois===6337###lif===63###soif===1597###eoif===1621###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");usage(Exit_Bad_Arg, a0);
					}
					else {AKA_mark("lis===-204-###sois===-6305-###eois===-63056-###lif===-63-###soif===-###eoif===-1595-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");}
 					AKA_mark("lis===204###sois===6338###eois===6352###lif===63###soif===1622###eoif===1636###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");token = av[2];
 					AKA_mark("lis===204###sois===6353###eois===6358###lif===63###soif===1637###eoif===1642###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");av++;
 					AKA_mark("lis===204###sois===6359###eois===6364###lif===63###soif===1643###eoif===1648###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");ac--;
 					AKA_mark("lis===204###sois===6365###eois===6371###lif===63###soif===1649###eoif===1655###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");break;

									case 'u': if(an[rc] == 'u')AKA_mark("lis===205###sois===6376###eois===6385###lif===64###soif===1660###eoif===1669###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");
 					if (AKA_mark("lis===205###sois===6390###eois===6396###lif===64###soif===1674###eoif===1680###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===205###sois===6390###eois===6396###lif===64###soif===1674###eoif===1680###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&!av[2])) {
						AKA_mark("lis===205###sois===6398###eois===6422###lif===64###soif===1682###eoif===1706###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");usage(Exit_Bad_Arg, a0);
					}
					else {AKA_mark("lis===-205-###sois===-6390-###eois===-63906-###lif===-64-###soif===-###eoif===-1680-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");}
 					AKA_mark("lis===205###sois===6423###eois===6436###lif===64###soif===1707###eoif===1720###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");uuid = av[2];
 					AKA_mark("lis===205###sois===6437###eois===6442###lif===64###soif===1721###eoif===1726###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");av++;
 					AKA_mark("lis===205###sois===6443###eois===6448###lif===64###soif===1727###eoif===1732###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");ac--;
 					AKA_mark("lis===205###sois===6449###eois===6455###lif===64###soif===1733###eoif===1739###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");break;

									case 'p': if(an[rc] == 'p')AKA_mark("lis===206###sois===6460###eois===6469###lif===65###soif===1744###eoif===1753###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");
 					if (AKA_mark("lis===206###sois===6474###eois===6498###lif===65###soif===1758###eoif===1782###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && ((AKA_mark("lis===206###sois===6474###eois===6479###lif===65###soif===1758###eoif===1763###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&av[2])	&&(AKA_mark("lis===206###sois===6483###eois===6498###lif===65###soif===1767###eoif===1782###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&atoi(av[2]) > 0))) { 						AKA_mark("lis===206###sois===6502###eois===6524###lif===65###soif===1786###eoif===1808###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");synchro = atoi(av[2]);
 						AKA_mark("lis===206###sois===6525###eois===6530###lif===65###soif===1809###eoif===1814###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");av++;
 						AKA_mark("lis===206###sois===6531###eois===6536###lif===65###soif===1815###eoif===1820###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");ac--;
 						AKA_mark("lis===206###sois===6537###eois===6543###lif===65###soif===1821###eoif===1827###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");break;
 }
					else {AKA_mark("lis===-206-###sois===-6474-###eois===-647424-###lif===-65-###soif===-###eoif===-1782-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");}
 /*@fallthrough@*/
									default: if(an[rc] != 'H' && an[rc] != 'r' && an[rc] != 'd' && an[rc] != 'b' && an[rc] != 'k' && an[rc] != 's' && an[rc] != 'e' && an[rc] != 't' && an[rc] != 'u' && an[rc] != 'p')AKA_mark("lis===207###sois===6568###eois===6576###lif===66###soif===1852###eoif===1860###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");

										AKA_mark("lis===208###sois===6582###eois===6637###lif===67###soif===1866###eoif===1921###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");usage(an[rc] != 'h' ? Exit_Bad_Arg : Exit_Success, a0);

										AKA_mark("lis===209###sois===6643###eois===6649###lif===68###soif===1927###eoif===1933###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");break;

				}
			}

		}

		av++;
		ac--;
	}


	/* check the argument count */
		if (AKA_mark("lis===217###sois===6717###eois===6746###lif===76###soif===2001###eoif===2030###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (((AKA_mark("lis===217###sois===6717###eois===6724###lif===76###soif===2001###eoif===2008###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&ac != 2)	&&(AKA_mark("lis===217###sois===6728###eois===6735###lif===76###soif===2012###eoif===2019###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&ac != 4))	&&(AKA_mark("lis===217###sois===6739###eois===6746###lif===76###soif===2023###eoif===2030###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&ac != 5))) {
		AKA_mark("lis===218###sois===6750###eois===6763###lif===77###soif===2034###eoif===2047###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");usage(1, a0);
	}
	else {AKA_mark("lis===-217-###sois===-6717-###eois===-671729-###lif===-76-###soif===-###eoif===-2030-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");}


	/* set raw by default */
		if (AKA_mark("lis===221###sois===6796###eois===6802###lif===80###soif===2080###eoif===2086###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===221###sois===6796###eois===6802###lif===80###soif===2080###eoif===2086###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&!human)) {
		AKA_mark("lis===222###sois===6806###eois===6814###lif===81###soif===2090###eoif===2098###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");raw = 1;
	}
	else {AKA_mark("lis===-221-###sois===-6796-###eois===-67966-###lif===-80-###soif===-###eoif===-2086-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");}


	/* get the default event loop */
		AKA_mark("lis===225###sois===6851###eois===6880###lif===84###soif===2135###eoif===2164###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");rc = sd_event_default(&loop);

		if (AKA_mark("lis===226###sois===6886###eois===6892###lif===85###soif===2170###eoif===2176###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===226###sois===6886###eois===6892###lif===85###soif===2170###eoif===2176###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&rc < 0)) {
				AKA_mark("lis===227###sois===6898###eois===6978###lif===86###soif===2182###eoif===2262###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");fprintf(stderr, "connection to default event loop failed: %s\n", strerror(-rc));

				AKA_mark("lis===228###sois===6981###eois===6990###lif===87###soif===2265###eoif===2274###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");return 1;

	}
	else {AKA_mark("lis===-226-###sois===-6886-###eois===-68866-###lif===-85-###soif===-###eoif===-2176-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");}


	/* connect the websocket wsj1 to the uri given by the first argument */
		if (AKA_mark("lis===232###sois===7073###eois===7079###lif===91###soif===2357###eoif===2363###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===232###sois===7073###eois===7079###lif===91###soif===2357###eoif===2363###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&direct)) {
				AKA_mark("lis===233###sois===7085###eois===7146###lif===92###soif===2369###eoif===2430###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");pws = afb_ws_client_connect_api(loop, av[1], &pws_itf, NULL);

				if (AKA_mark("lis===234###sois===7153###eois===7164###lif===93###soif===2437###eoif===2448###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===234###sois===7153###eois===7164###lif===93###soif===2437###eoif===2448###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&pws == NULL)) {
						AKA_mark("lis===235###sois===7171###eois===7227###lif===94###soif===2455###eoif===2511###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");fprintf(stderr, "connection to %s failed: %m\n", av[1]);

						AKA_mark("lis===236###sois===7231###eois===7256###lif===95###soif===2515###eoif===2540###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");return Exit_Cant_Connect;

		}
		else {AKA_mark("lis===-234-###sois===-7153-###eois===-715311-###lif===-93-###soif===-###eoif===-2448-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");}

				AKA_mark("lis===238###sois===7263###eois===7306###lif===97###soif===2547###eoif===2590###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");afb_proto_ws_on_hangup(pws, on_pws_hangup);

				if (AKA_mark("lis===239###sois===7313###eois===7317###lif===98###soif===2597###eoif===2601###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===239###sois===7313###eois===7317###lif===98###soif===2597###eoif===2601###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&uuid)) {
						AKA_mark("lis===240###sois===7324###eois===7336###lif===99###soif===2608###eoif===2620###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");numuuid = 1;

						AKA_mark("lis===241###sois===7340###eois===7395###lif===100###soif===2624###eoif===2679###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");afb_proto_ws_client_session_create(pws, numuuid, uuid);

		}
		else {AKA_mark("lis===-239-###sois===-7313-###eois===-73134-###lif===-98-###soif===-###eoif===-2601-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");}

				if (AKA_mark("lis===243###sois===7406###eois===7411###lif===102###soif===2690###eoif===2695###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===243###sois===7406###eois===7411###lif===102###soif===2690###eoif===2695###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&token)) {
						AKA_mark("lis===244###sois===7418###eois===7431###lif===103###soif===2702###eoif===2715###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");numtoken = 1;

						AKA_mark("lis===245###sois===7435###eois===7490###lif===104###soif===2719###eoif===2774###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");afb_proto_ws_client_token_create(pws, numtoken, token);

		}
		else {AKA_mark("lis===-243-###sois===-7406-###eois===-74065-###lif===-102-###soif===-###eoif===-2695-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");}

	}
	else {
				AKA_mark("lis===248###sois===7507###eois===7697###lif===107###soif===2791###eoif===2981###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");rc = asprintf(&url, "%s%s%s%s%s%s%s",
			av[1],
			uuid || token ? "?" : "",
			uuid ? "uuid=" : "",
			uuid ?: "",
			uuid && token ? "&" : "",
			token ? "token=" : "",
			token ?: ""
		);

				AKA_mark("lis===257###sois===7700###eois===7762###lif===116###soif===2984###eoif===3046###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");wsj1 = afb_ws_client_connect_wsj1(loop, url, &wsj1_itf, NULL);

				if (AKA_mark("lis===258###sois===7769###eois===7781###lif===117###soif===3053###eoif===3065###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===258###sois===7769###eois===7781###lif===117###soif===3053###eoif===3065###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&wsj1 == NULL)) {
						AKA_mark("lis===259###sois===7788###eois===7844###lif===118###soif===3072###eoif===3128###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");fprintf(stderr, "connection to %s failed: %m\n", av[1]);

						AKA_mark("lis===260###sois===7848###eois===7873###lif===119###soif===3132###eoif===3157###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");return Exit_Cant_Connect;

		}
		else {AKA_mark("lis===-258-###sois===-7769-###eois===-776912-###lif===-117-###soif===-###eoif===-3065-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");}

	}


	/* test the behaviour */
		if (AKA_mark("lis===265###sois===7913###eois===7920###lif===124###soif===3197###eoif===3204###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===265###sois===7913###eois===7920###lif===124###soif===3197###eoif===3204###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&ac == 2)) {
		/* get requests from stdin */
				AKA_mark("lis===267###sois===7958###eois===7968###lif===126###soif===3242###eoif===3252###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");usein = 1;

				AKA_mark("lis===268###sois===7971###eois===8001###lif===127###soif===3255###eoif===3285###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");fcntl(0, F_SETFL, O_NONBLOCK);

				if (AKA_mark("lis===269###sois===8008###eois===8069###lif===128###soif===3292###eoif===3353###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===269###sois===8008###eois===8069###lif===128###soif===3292###eoif===3353###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&sd_event_add_io(loop, &evsrc, 0, EPOLLIN, on_stdin, NULL) < 0)) {
			AKA_mark("lis===270###sois===8074###eois===8087###lif===129###soif===3358###eoif===3371###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");evsrc = NULL;
		}
		else {AKA_mark("lis===-269-###sois===-8008-###eois===-800861-###lif===-128-###soif===-###eoif===-3353-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");}

	}
	else {
		/* the request is defined by the arguments */
				AKA_mark("lis===273###sois===8148###eois===8158###lif===132###soif===3432###eoif===3442###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");usein = 0;

				AKA_mark("lis===274###sois===8161###eois===8180###lif===133###soif===3445###eoif===3464###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");exonrep = !keeprun;

				if (AKA_mark("lis===275###sois===8187###eois===8193###lif===134###soif===3471###eoif===3477###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)") && (AKA_mark("lis===275###sois===8187###eois===8193###lif===134###soif===3471###eoif===3477###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)")&&direct)) {
			AKA_mark("lis===276###sois===8198###eois===8221###lif===135###soif===3482###eoif===3505###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");pws_call(av[2], av[3]);
		}
		else {
			AKA_mark("lis===278###sois===8232###eois===8263###lif===137###soif===3516###eoif===3547###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");wsj1_emit(av[2], av[3], av[4]);
		}

	}


	/* loop until end */
		AKA_mark("lis===282###sois===8291###eois===8298###lif===141###soif===3575###eoif===3582###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");idle();

		AKA_mark("lis===283###sois===8300###eois===8309###lif===142###soif===3584###eoif===3593###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/main(int,char**,char**)");return 0;

}

/** Instrumented function idle() */
static void idle()
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/idle()");AKA_fCall++;
		for (;;) {
				if (AKA_mark("lis===289###sois===8351###eois===8357###lif===3###soif===38###eoif===44###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()") && (AKA_mark("lis===289###sois===8351###eois===8357###lif===3###soif===38###eoif===44###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()")&&!usein)) {
						if (AKA_mark("lis===290###sois===8368###eois===8390###lif===4###soif===55###eoif===77###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()") && ((AKA_mark("lis===290###sois===8368###eois===8376###lif===4###soif===55###eoif===63###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()")&&!keeprun)	&&(AKA_mark("lis===290###sois===8380###eois===8390###lif===4###soif===67###eoif===77###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()")&&!callcount))) {
				AKA_mark("lis===291###sois===8396###eois===8411###lif===5###soif===83###eoif===98###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()");exit(exitcode);
			}
			else {AKA_mark("lis===-290-###sois===-8368-###eois===-836822-###lif===-4-###soif===-###eoif===-77-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()");}

						AKA_mark("lis===292###sois===8415###eois===8444###lif===6###soif===102###eoif===131###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()");sd_event_run(loop, 30000000);

		}
		else {
			if (AKA_mark("lis===294###sois===8460###eois===8491###lif===8###soif===147###eoif===178###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()") && ((AKA_mark("lis===294###sois===8460###eois===8468###lif===8###soif===147###eoif===155###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()")&&!synchro)	||(AKA_mark("lis===294###sois===8472###eois===8491###lif===8###soif===159###eoif===178###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()")&&callcount < synchro))) {
							if (AKA_mark("lis===295###sois===8502###eois===8527###lif===9###soif===189###eoif===214###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()") && ((AKA_mark("lis===295###sois===8502###eois===8518###lif===9###soif===189###eoif===205###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()")&&!process_stdin())	&&(AKA_mark("lis===295###sois===8522###eois===8527###lif===9###soif===209###eoif===214###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()")&&usein))) {
					AKA_mark("lis===296###sois===8533###eois===8560###lif===10###soif===220###eoif===247###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()");sd_event_run(loop, 100000);
				}
				else {AKA_mark("lis===-295-###sois===-8502-###eois===-850225-###lif===-9-###soif===-###eoif===-214-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()");}

		}
			else {
							AKA_mark("lis===298###sois===8575###eois===8604###lif===12###soif===262###eoif===291###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/idle()");sd_event_run(loop, 30000000);

		}
		}

	}

}

/* decrement the count of calls */
/** Instrumented function dec_callcount() */
static void dec_callcount()
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/dec_callcount()");AKA_fCall++;
		AKA_mark("lis===306###sois===8681###eois===8693###lif===2###soif===31###eoif===43###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/dec_callcount()");callcount--;

		if (AKA_mark("lis===307###sois===8699###eois===8720###lif===3###soif===49###eoif===70###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/dec_callcount()") && ((AKA_mark("lis===307###sois===8699###eois===8706###lif===3###soif===49###eoif===56###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/dec_callcount()")&&exonrep)	&&(AKA_mark("lis===307###sois===8710###eois===8720###lif===3###soif===60###eoif===70###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/dec_callcount()")&&!callcount))) {
		AKA_mark("lis===308###sois===8724###eois===8739###lif===4###soif===74###eoif===89###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/dec_callcount()");exit(exitcode);
	}
	else {AKA_mark("lis===-307-###sois===-8699-###eois===-869921-###lif===-3-###soif===-###eoif===-70-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/dec_callcount()");}

}

/* called when wsj1 hangsup */
/** Instrumented function on_wsj1_hangup(void*,struct afb_wsj1*) */
static void on_wsj1_hangup(void *closure, struct afb_wsj1 *wsj1)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_hangup(void*,struct afb_wsj1*)");AKA_fCall++;
		AKA_mark("lis===314###sois===8842###eois===8864###lif===2###soif===68###eoif===90###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_hangup(void*,struct afb_wsj1*)");printf("ON-HANGUP\n");

		AKA_mark("lis===315###sois===8866###eois===8881###lif===3###soif===92###eoif===107###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_hangup(void*,struct afb_wsj1*)");fflush(stdout);

		AKA_mark("lis===316###sois===8883###eois===8901###lif===4###soif===109###eoif===127###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_hangup(void*,struct afb_wsj1*)");exit(Exit_HangUp);

}

/* called when wsj1 receives a method invocation */
/** Instrumented function on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*) */
static void on_wsj1_call(void *closure, const char *api, const char *verb, struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)");AKA_fCall++;
		AKA_mark("lis===322###sois===9061###eois===9068###lif===2###soif===104###eoif===111###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)");int rc;

		if (AKA_mark("lis===323###sois===9074###eois===9077###lif===3###soif===117###eoif===120###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)") && (AKA_mark("lis===323###sois===9074###eois===9077###lif===3###soif===117###eoif===120###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)")&&raw)) {
		AKA_mark("lis===324###sois===9081###eois===9124###lif===4###soif===124###eoif===167###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)");printf("%s\n", afb_wsj1_msg_object_s(msg));
	}
	else {AKA_mark("lis===-323-###sois===-9074-###eois===-90743-###lif===-3-###soif===-###eoif===-120-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)");}

		if (AKA_mark("lis===325###sois===9130###eois===9135###lif===5###soif===173###eoif===178###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)") && (AKA_mark("lis===325###sois===9130###eois===9135###lif===5###soif===173###eoif===178###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)")&&human)) {
		AKA_mark("lis===326###sois===9139###eois===9308###lif===6###soif===182###eoif===351###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)");printf("ON-CALL %s/%s:\n%s\n", api, verb,
				json_object_to_json_string_ext(afb_wsj1_msg_object_j(msg),
							JSON_C_TO_STRING_PRETTY|JSON_C_TO_STRING_NOSLASHESCAPE));
	}
	else {AKA_mark("lis===-325-###sois===-9130-###eois===-91305-###lif===-5-###soif===-###eoif===-178-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)");}

		AKA_mark("lis===329###sois===9310###eois===9325###lif===9###soif===353###eoif===368###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)");fflush(stdout);

		AKA_mark("lis===330###sois===9327###eois===9387###lif===10###soif===370###eoif===430###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)");rc = afb_wsj1_reply_error_s(msg, "\"unimplemented\"", NULL);

		if (AKA_mark("lis===331###sois===9393###eois===9399###lif===11###soif===436###eoif===442###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)") && (AKA_mark("lis===331###sois===9393###eois===9399###lif===11###soif===436###eoif===442###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)")&&rc < 0)) {
		AKA_mark("lis===332###sois===9403###eois===9444###lif===12###soif===446###eoif===487###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)");fprintf(stderr, "replying failed: %m\n");
	}
	else {AKA_mark("lis===-331-###sois===-9393-###eois===-93936-###lif===-11-###soif===-###eoif===-442-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_call(void*,const char*,const char*,struct afb_wsj1_msg*)");}

}

/* called when wsj1 receives an event */
/** Instrumented function on_wsj1_event(void*,const char*,struct afb_wsj1_msg*) */
static void on_wsj1_event(void *closure, const char *event, struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_event(void*,const char*,struct afb_wsj1_msg*)");AKA_fCall++;
		if (AKA_mark("lis===338###sois===9582###eois===9585###lif===2###soif===93###eoif===96###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_event(void*,const char*,struct afb_wsj1_msg*)") && (AKA_mark("lis===338###sois===9582###eois===9585###lif===2###soif===93###eoif===96###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_event(void*,const char*,struct afb_wsj1_msg*)")&&raw)) {
		AKA_mark("lis===339###sois===9589###eois===9632###lif===3###soif===100###eoif===143###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_event(void*,const char*,struct afb_wsj1_msg*)");printf("%s\n", afb_wsj1_msg_object_s(msg));
	}
	else {AKA_mark("lis===-338-###sois===-9582-###eois===-95823-###lif===-2-###soif===-###eoif===-96-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_event(void*,const char*,struct afb_wsj1_msg*)");}

		if (AKA_mark("lis===340###sois===9638###eois===9643###lif===4###soif===149###eoif===154###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_event(void*,const char*,struct afb_wsj1_msg*)") && (AKA_mark("lis===340###sois===9638###eois===9643###lif===4###soif===149###eoif===154###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_event(void*,const char*,struct afb_wsj1_msg*)")&&human)) {
		AKA_mark("lis===341###sois===9647###eois===9810###lif===5###soif===158###eoif===321###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_event(void*,const char*,struct afb_wsj1_msg*)");printf("ON-EVENT %s:\n%s\n", event,
				json_object_to_json_string_ext(afb_wsj1_msg_object_j(msg),
							JSON_C_TO_STRING_PRETTY|JSON_C_TO_STRING_NOSLASHESCAPE));
	}
	else {AKA_mark("lis===-340-###sois===-9638-###eois===-96385-###lif===-4-###soif===-###eoif===-154-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_event(void*,const char*,struct afb_wsj1_msg*)");}

		AKA_mark("lis===344###sois===9812###eois===9827###lif===8###soif===323###eoif===338###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_event(void*,const char*,struct afb_wsj1_msg*)");fflush(stdout);

}

/* called when wsj1 receives a reply */
/** Instrumented function on_wsj1_reply(void*,struct afb_wsj1_msg*) */
static void on_wsj1_reply(void *closure, struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_reply(void*,struct afb_wsj1_msg*)");AKA_fCall++;
		AKA_mark("lis===350###sois===9941###eois===9986###lif===2###soif===70###eoif===115###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_reply(void*,struct afb_wsj1_msg*)");int iserror = !afb_wsj1_msg_is_reply_ok(msg);

		AKA_mark("lis===351###sois===9988###eois===10035###lif===3###soif===117###eoif===164###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_reply(void*,struct afb_wsj1_msg*)");exitcode = iserror ? Exit_Error : Exit_Success;

		if (AKA_mark("lis===352###sois===10041###eois===10044###lif===4###soif===170###eoif===173###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_reply(void*,struct afb_wsj1_msg*)") && (AKA_mark("lis===352###sois===10041###eois===10044###lif===4###soif===170###eoif===173###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_reply(void*,struct afb_wsj1_msg*)")&&raw)) {
		AKA_mark("lis===353###sois===10048###eois===10091###lif===5###soif===177###eoif===220###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_reply(void*,struct afb_wsj1_msg*)");printf("%s\n", afb_wsj1_msg_object_s(msg));
	}
	else {AKA_mark("lis===-352-###sois===-10041-###eois===-100413-###lif===-4-###soif===-###eoif===-173-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_reply(void*,struct afb_wsj1_msg*)");}

		if (AKA_mark("lis===354###sois===10097###eois===10102###lif===6###soif===226###eoif===231###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_reply(void*,struct afb_wsj1_msg*)") && (AKA_mark("lis===354###sois===10097###eois===10102###lif===6###soif===226###eoif===231###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_reply(void*,struct afb_wsj1_msg*)")&&human)) {
		AKA_mark("lis===355###sois===10106###eois===10311###lif===7###soif===235###eoif===440###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_reply(void*,struct afb_wsj1_msg*)");printf("ON-REPLY %s: %s\n%s\n", (char*)closure,
				iserror ? "ERROR" : "OK",
				json_object_to_json_string_ext(afb_wsj1_msg_object_j(msg),
							JSON_C_TO_STRING_PRETTY|JSON_C_TO_STRING_NOSLASHESCAPE));
	}
	else {AKA_mark("lis===-354-###sois===-10097-###eois===-100975-###lif===-6-###soif===-###eoif===-231-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_reply(void*,struct afb_wsj1_msg*)");}

		AKA_mark("lis===359###sois===10313###eois===10328###lif===11###soif===442###eoif===457###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_reply(void*,struct afb_wsj1_msg*)");fflush(stdout);

		AKA_mark("lis===360###sois===10330###eois===10344###lif===12###soif===459###eoif===473###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_reply(void*,struct afb_wsj1_msg*)");free(closure);

		AKA_mark("lis===361###sois===10346###eois===10362###lif===13###soif===475###eoif===491###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_wsj1_reply(void*,struct afb_wsj1_msg*)");dec_callcount();

}

/* makes a call */
/** Instrumented function wsj1_call(const char*,const char*,const char*) */
static void wsj1_call(const char *api, const char *verb, const char *object)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===367###sois===10465###eois===10484###lif===2###soif===80###eoif===99###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)");static int num = 0;

		AKA_mark("lis===368###sois===10486###eois===10496###lif===3###soif===101###eoif===111###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)");char *key;

		AKA_mark("lis===369###sois===10498###eois===10505###lif===4###soif===113###eoif===120###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)");int rc;


	/* allocates an id for the request */
		AKA_mark("lis===372###sois===10547###eois===10597###lif===7###soif===162###eoif===212###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)");rc = asprintf(&key, "%d:%s/%s", ++num, api, verb);


	/* echo the command if asked */
		if (AKA_mark("lis===375###sois===10637###eois===10641###lif===10###soif===252###eoif===256###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)") && (AKA_mark("lis===375###sois===10637###eois===10641###lif===10###soif===252###eoif===256###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)")&&echo)) {
		AKA_mark("lis===376###sois===10645###eois===10703###lif===11###soif===260###eoif===318###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)");printf("SEND-CALL %s/%s %s\n", api, verb, object?:"null");
	}
	else {AKA_mark("lis===-375-###sois===-10637-###eois===-106374-###lif===-10-###soif===-###eoif===-256-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)");}


	/* send the request */
		AKA_mark("lis===379###sois===10730###eois===10742###lif===14###soif===345###eoif===357###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)");callcount++;

		AKA_mark("lis===380###sois===10744###eois===10810###lif===15###soif===359###eoif===425###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)");rc = afb_wsj1_call_s(wsj1, api, verb, object, on_wsj1_reply, key);

		if (AKA_mark("lis===381###sois===10816###eois===10822###lif===16###soif===431###eoif===437###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)") && (AKA_mark("lis===381###sois===10816###eois===10822###lif===16###soif===431###eoif===437###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)")&&rc < 0)) {
				AKA_mark("lis===382###sois===10828###eois===10897###lif===17###soif===443###eoif===512###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)");fprintf(stderr, "calling %s/%s(%s) failed: %m\n", api, verb, object);

				AKA_mark("lis===383###sois===10900###eois===10916###lif===18###soif===515###eoif===531###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)");dec_callcount();

	}
	else {AKA_mark("lis===-381-###sois===-10816-###eois===-108166-###lif===-16-###soif===-###eoif===-437-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_call(const char*,const char*,const char*)");}

}

/* sends an event */
/** Instrumented function wsj1_event(const char*,const char*) */
static void wsj1_event(const char *event, const char *object)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/wsj1_event(const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===390###sois===11009###eois===11016###lif===2###soif===65###eoif===72###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_event(const char*,const char*)");int rc;


	/* echo the command if asked */
		if (AKA_mark("lis===393###sois===11056###eois===11060###lif===5###soif===112###eoif===116###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_event(const char*,const char*)") && (AKA_mark("lis===393###sois===11056###eois===11060###lif===5###soif===112###eoif===116###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_event(const char*,const char*)")&&echo)) {
		AKA_mark("lis===394###sois===11064###eois===11117###lif===6###soif===120###eoif===173###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_event(const char*,const char*)");printf("SEND-EVENT: %s %s\n", event, object?:"null");
	}
	else {AKA_mark("lis===-393-###sois===-11056-###eois===-110564-###lif===-5-###soif===-###eoif===-116-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_event(const char*,const char*)");}


		AKA_mark("lis===396###sois===11120###eois===11168###lif===8###soif===176###eoif===224###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_event(const char*,const char*)");rc = afb_wsj1_send_event_s(wsj1, event, object);

		if (AKA_mark("lis===397###sois===11174###eois===11180###lif===9###soif===230###eoif===236###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_event(const char*,const char*)") && (AKA_mark("lis===397###sois===11174###eois===11180###lif===9###soif===230###eoif===236###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_event(const char*,const char*)")&&rc < 0)) {
		AKA_mark("lis===398###sois===11184###eois===11247###lif===10###soif===240###eoif===303###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_event(const char*,const char*)");fprintf(stderr, "sending !%s(%s) failed: %m\n", event, object);
	}
	else {AKA_mark("lis===-397-###sois===-11174-###eois===-111746-###lif===-9-###soif===-###eoif===-236-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_event(const char*,const char*)");}

}

/* emits either a call (when api!='!') or an event */
/** Instrumented function wsj1_emit(const char*,const char*,const char*) */
static void wsj1_emit(const char *api, const char *verb, const char *object)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)");AKA_fCall++;
		if (AKA_mark("lis===404###sois===11389###eois===11421###lif===2###soif===84###eoif===116###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)") && ((AKA_mark("lis===404###sois===11389###eois===11403###lif===2###soif===84###eoif===98###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)")&&object == NULL)	||(AKA_mark("lis===404###sois===11407###eois===11421###lif===2###soif===102###eoif===116###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)")&&object[0] == 0))) {
		AKA_mark("lis===405###sois===11425###eois===11441###lif===3###soif===120###eoif===136###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)");object = "null";
	}
	else {AKA_mark("lis===-404-###sois===-11389-###eois===-1138932-###lif===-2-###soif===-###eoif===-116-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)");}


		if (AKA_mark("lis===407###sois===11448###eois===11476###lif===5###soif===143###eoif===171###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)") && ((AKA_mark("lis===407###sois===11448###eois===11461###lif===5###soif===143###eoif===156###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)")&&api[0] == '!')	&&(AKA_mark("lis===407###sois===11465###eois===11476###lif===5###soif===160###eoif===171###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)")&&api[1] == 0))) {
		AKA_mark("lis===408###sois===11480###eois===11505###lif===6###soif===175###eoif===200###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)");wsj1_event(verb, object);
	}
	else {
		AKA_mark("lis===410###sois===11514###eois===11543###lif===8###soif===209###eoif===238###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)");wsj1_call(api, verb, object);
	}

		if (AKA_mark("lis===411###sois===11549###eois===11557###lif===9###soif===244###eoif===252###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)") && (AKA_mark("lis===411###sois===11549###eois===11557###lif===9###soif===244###eoif===252###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)")&&breakcon)) {
		AKA_mark("lis===412###sois===11561###eois===11569###lif===10###soif===256###eoif===264###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)");exit(0);
	}
	else {AKA_mark("lis===-411-###sois===-11549-###eois===-115498-###lif===-9-###soif===-###eoif===-252-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/wsj1_emit(const char*,const char*,const char*)");}

}

/* process stdin */
/** Instrumented function process_stdin() */
static int process_stdin()
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");AKA_fCall++;
		AKA_mark("lis===418###sois===11623###eois===11647###lif===2###soif===30###eoif===54###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");static size_t count = 0;

		AKA_mark("lis===419###sois===11649###eois===11673###lif===3###soif===56###eoif===80###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");static char line[16384];

		AKA_mark("lis===420###sois===11675###eois===11701###lif===4###soif===82###eoif===108###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");static char sep[] = " \t";

		AKA_mark("lis===421###sois===11703###eois===11733###lif===5###soif===110###eoif===140###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");static char sepnl[] = " \t\n";


		AKA_mark("lis===423###sois===11736###eois===11751###lif===7###soif===143###eoif===158###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");int result = 0;

		AKA_mark("lis===424###sois===11753###eois===11768###lif===8###soif===160###eoif===175###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");ssize_t rc = 0;

		AKA_mark("lis===425###sois===11770###eois===11781###lif===9###soif===177###eoif===188###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");size_t pos;


	/* read the buffer */
		while (AKA_mark("lis===428###sois===11814###eois===11833###lif===12###soif===221###eoif===240###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && (AKA_mark("lis===428###sois===11814###eois===11833###lif===12###soif===221###eoif===240###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&sizeof line > count)) {
				AKA_mark("lis===429###sois===11839###eois===11887###lif===13###soif===246###eoif===294###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");rc = read(0, line + count, sizeof line - count);

				if (AKA_mark("lis===430###sois===11894###eois===11919###lif===14###soif===301###eoif===326###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && ((AKA_mark("lis===430###sois===11894###eois===11901###lif===14###soif===301###eoif===308###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&rc >= 0)	||(AKA_mark("lis===430###sois===11905###eois===11919###lif===14###soif===312###eoif===326###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&errno != EINTR))) {
			AKA_mark("lis===431###sois===11924###eois===11930###lif===15###soif===331###eoif===337###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");break;
		}
		else {AKA_mark("lis===-430-###sois===-11894-###eois===-1189425-###lif===-14-###soif===-###eoif===-326-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");}

	}

		if (AKA_mark("lis===433###sois===11939###eois===11945###lif===17###soif===346###eoif===352###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && (AKA_mark("lis===433###sois===11939###eois===11945###lif===17###soif===346###eoif===352###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&rc < 0)) {
				if (AKA_mark("lis===434###sois===11955###eois===11970###lif===18###soif===362###eoif===377###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && (AKA_mark("lis===434###sois===11955###eois===11970###lif===18###soif===362###eoif===377###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&errno == EAGAIN)) {
			AKA_mark("lis===435###sois===11975###eois===11984###lif===19###soif===382###eoif===391###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");return 0;
		}
		else {AKA_mark("lis===-434-###sois===-11955-###eois===-1195515-###lif===-18-###soif===-###eoif===-377-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");}

				AKA_mark("lis===436###sois===11987###eois===12023###lif===20###soif===394###eoif===430###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");fprintf(stderr, "read error: %m\n");

				AKA_mark("lis===437###sois===12026###eois===12048###lif===21###soif===433###eoif===455###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");exit(Exit_Input_Fail);

	}
	else {AKA_mark("lis===-433-###sois===-11939-###eois===-119396-###lif===-17-###soif===-###eoif===-352-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");}

		if (AKA_mark("lis===439###sois===12057###eois===12064###lif===23###soif===464###eoif===471###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && (AKA_mark("lis===439###sois===12057###eois===12064###lif===23###soif===464###eoif===471###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&rc == 0)) {
				AKA_mark("lis===440###sois===12070###eois===12089###lif===24###soif===477###eoif===496###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");usein = count != 0;

				if (AKA_mark("lis===441###sois===12096###eois===12114###lif===25###soif===503###eoif===521###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && ((AKA_mark("lis===441###sois===12096###eois===12102###lif===25###soif===503###eoif===509###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&!usein)	&&(AKA_mark("lis===441###sois===12106###eois===12114###lif===25###soif===513###eoif===521###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&!keeprun))) {
						if (AKA_mark("lis===442###sois===12125###eois===12135###lif===26###soif===532###eoif===542###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && (AKA_mark("lis===442###sois===12125###eois===12135###lif===26###soif===532###eoif===542###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&!callcount)) {
				AKA_mark("lis===443###sois===12141###eois===12156###lif===27###soif===548###eoif===563###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");exit(exitcode);
			}
			else {AKA_mark("lis===-442-###sois===-12125-###eois===-1212510-###lif===-26-###soif===-###eoif===-542-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");}

						AKA_mark("lis===444###sois===12160###eois===12172###lif===28###soif===567###eoif===579###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");exonrep = 1;

		}
		else {AKA_mark("lis===-441-###sois===-12096-###eois===-1209618-###lif===-25-###soif===-###eoif===-521-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");}

	}
	else {AKA_mark("lis===-439-###sois===-12057-###eois===-120577-###lif===-23-###soif===-###eoif===-471-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");}

		AKA_mark("lis===447###sois===12181###eois===12201###lif===31###soif===588###eoif===608###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");count += (size_t)rc;

		if (AKA_mark("lis===448###sois===12207###eois===12238###lif===32###soif===614###eoif===645###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && ((AKA_mark("lis===448###sois===12207###eois===12214###lif===32###soif===614###eoif===621###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&synchro)	&&(AKA_mark("lis===448###sois===12218###eois===12238###lif===32###soif===625###eoif===645###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&callcount >= synchro))) {
		AKA_mark("lis===449###sois===12242###eois===12251###lif===33###soif===649###eoif===658###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");return 0;
	}
	else {AKA_mark("lis===-448-###sois===-12207-###eois===-1220731-###lif===-32-###soif===-###eoif===-645-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");}


	/* normalise the buffer content */
	/* TODO: handle backspace \x7f ? */
	/* process the lines */
		AKA_mark("lis===454###sois===12352###eois===12360###lif===38###soif===759###eoif===767###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");pos = 0;

		for (;;) {
				AKA_mark("lis===456###sois===12374###eois===12409###lif===40###soif===781###eoif===816###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");size_t i, api[2], verb[2], rest[2];

				AKA_mark("lis===457###sois===12412###eois===12420###lif===41###soif===819###eoif===827###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");i = pos;

				while (AKA_mark("lis===458###sois===12429###eois===12462###lif===42###soif===836###eoif===869###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && ((AKA_mark("lis===458###sois===12429###eois===12438###lif===42###soif===836###eoif===845###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&i < count)	&&(AKA_mark("lis===458###sois===12442###eois===12462###lif===42###soif===849###eoif===869###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&strchr(sep, line[i])))) {
			AKA_mark("lis===458###sois===12464###eois===12468###lif===42###soif===871###eoif===875###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");i++;
		}

				AKA_mark("lis===459###sois===12471###eois===12482###lif===43###soif===878###eoif===889###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");api[0] = i;
 		while (AKA_mark("lis===459###sois===12489###eois===12525###lif===43###soif===896###eoif===932###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && ((AKA_mark("lis===459###sois===12489###eois===12498###lif===43###soif===896###eoif===905###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&i < count)	&&(AKA_mark("lis===459###sois===12502###eois===12525###lif===43###soif===909###eoif===932###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&!strchr(sepnl, line[i])))) {
			AKA_mark("lis===459###sois===12527###eois===12531###lif===43###soif===934###eoif===938###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");i++;
		}
 		AKA_mark("lis===459###sois===12532###eois===12543###lif===43###soif===939###eoif===950###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");api[1] = i;

				while (AKA_mark("lis===460###sois===12552###eois===12585###lif===44###soif===959###eoif===992###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && ((AKA_mark("lis===460###sois===12552###eois===12561###lif===44###soif===959###eoif===968###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&i < count)	&&(AKA_mark("lis===460###sois===12565###eois===12585###lif===44###soif===972###eoif===992###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&strchr(sep, line[i])))) {
			AKA_mark("lis===460###sois===12587###eois===12591###lif===44###soif===994###eoif===998###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");i++;
		}

				if (AKA_mark("lis===461###sois===12598###eois===12604###lif===45###soif===1005###eoif===1011###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && (AKA_mark("lis===461###sois===12598###eois===12604###lif===45###soif===1005###eoif===1011###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&direct)) {
						AKA_mark("lis===462###sois===12611###eois===12628###lif===46###soif===1018###eoif===1035###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");verb[0] = api[0];

						AKA_mark("lis===463###sois===12632###eois===12649###lif===47###soif===1039###eoif===1056###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");verb[1] = api[1];

		}
		else {
						AKA_mark("lis===465###sois===12664###eois===12676###lif===49###soif===1071###eoif===1083###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");verb[0] = i;
 			while (AKA_mark("lis===465###sois===12683###eois===12719###lif===49###soif===1090###eoif===1126###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && ((AKA_mark("lis===465###sois===12683###eois===12692###lif===49###soif===1090###eoif===1099###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&i < count)	&&(AKA_mark("lis===465###sois===12696###eois===12719###lif===49###soif===1103###eoif===1126###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&!strchr(sepnl, line[i])))) {
				AKA_mark("lis===465###sois===12721###eois===12725###lif===49###soif===1128###eoif===1132###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");i++;
			}
 			AKA_mark("lis===465###sois===12726###eois===12738###lif===49###soif===1133###eoif===1145###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");verb[1] = i;

						while (AKA_mark("lis===466###sois===12748###eois===12781###lif===50###soif===1155###eoif===1188###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && ((AKA_mark("lis===466###sois===12748###eois===12757###lif===50###soif===1155###eoif===1164###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&i < count)	&&(AKA_mark("lis===466###sois===12761###eois===12781###lif===50###soif===1168###eoif===1188###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&strchr(sep, line[i])))) {
				AKA_mark("lis===466###sois===12783###eois===12787###lif===50###soif===1190###eoif===1194###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");i++;
			}

		}

				AKA_mark("lis===468###sois===12794###eois===12806###lif===52###soif===1201###eoif===1213###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");rest[0] = i;
 		while (AKA_mark("lis===468###sois===12813###eois===12841###lif===52###soif===1220###eoif===1248###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && ((AKA_mark("lis===468###sois===12813###eois===12822###lif===52###soif===1220###eoif===1229###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&i < count)	&&(AKA_mark("lis===468###sois===12826###eois===12841###lif===52###soif===1233###eoif===1248###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&line[i] != '\n'))) {
			AKA_mark("lis===468###sois===12843###eois===12847###lif===52###soif===1250###eoif===1254###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");i++;
		}
 		AKA_mark("lis===468###sois===12848###eois===12860###lif===52###soif===1255###eoif===1267###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");rest[1] = i;

				if (AKA_mark("lis===469###sois===12867###eois===12877###lif===53###soif===1274###eoif===1284###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && (AKA_mark("lis===469###sois===12867###eois===12877###lif===53###soif===1274###eoif===1284###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&i == count)) {
			AKA_mark("lis===469###sois===12879###eois===12885###lif===53###soif===1286###eoif===1292###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");break;
		}
		else {AKA_mark("lis===-469-###sois===-12867-###eois===-1286710-###lif===-53-###soif===-###eoif===-1284-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");}

				AKA_mark("lis===470###sois===12888###eois===12902###lif===54###soif===1295###eoif===1309###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");line[i++] = 0;

				AKA_mark("lis===471###sois===12905###eois===12913###lif===55###soif===1312###eoif===1320###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");pos = i;

				if (AKA_mark("lis===472###sois===12920###eois===12936###lif===56###soif===1327###eoif===1343###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && (AKA_mark("lis===472###sois===12920###eois===12936###lif===56###soif===1327###eoif===1343###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&api[0] == api[1])) {AKA_mark("lis===+472+###sois===+12920+###eois===+1292016+###lif===+56+###soif===+###eoif===+1343+###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");}
		else {
			if (AKA_mark("lis===474###sois===12973###eois===12992###lif===58###soif===1380###eoif===1399###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && (AKA_mark("lis===474###sois===12973###eois===12992###lif===58###soif===1380###eoif===1399###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&line[api[0]] == '#')) {AKA_mark("lis===+474+###sois===+12973+###eois===+1297319+###lif===+58+###soif===+###eoif===+1399+###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");}
			else {
				if (AKA_mark("lis===476###sois===13026###eois===13044###lif===60###soif===1433###eoif===1451###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && (AKA_mark("lis===476###sois===13026###eois===13044###lif===60###soif===1433###eoif===1451###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&verb[0] == verb[1])) {
								AKA_mark("lis===477###sois===13051###eois===13109###lif===61###soif===1458###eoif===1516###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");fprintf(stderr, "verb missing, bad line: %s\n", line+pos);

		}
				else {
								AKA_mark("lis===479###sois===13124###eois===13157###lif===63###soif===1531###eoif===1564###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");line[api[1]] = line[verb[1]] = 0;

								if (AKA_mark("lis===480###sois===13165###eois===13171###lif===64###soif===1572###eoif===1578###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && (AKA_mark("lis===480###sois===13165###eois===13171###lif===64###soif===1572###eoif===1578###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&direct)) {
						AKA_mark("lis===481###sois===13177###eois===13218###lif===65###soif===1584###eoif===1625###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");pws_call(line + verb[0], line + rest[0]);
					}
					else {
						AKA_mark("lis===483###sois===13231###eois===13288###lif===67###soif===1638###eoif===1695###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");wsj1_emit(line + api[0], line + verb[0], line + rest[0]);
					}

								AKA_mark("lis===484###sois===13292###eois===13303###lif===68###soif===1699###eoif===1710###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");result = 1;

								AKA_mark("lis===485###sois===13307###eois===13313###lif===69###soif===1714###eoif===1720###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");break;

		}
			}
		}

	}

		AKA_mark("lis===488###sois===13322###eois===13335###lif===72###soif===1729###eoif===1742###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");count -= pos;

		if (AKA_mark("lis===489###sois===13341###eois===13361###lif===73###soif===1748###eoif===1768###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && (AKA_mark("lis===489###sois===13341###eois===13361###lif===73###soif===1748###eoif===1768###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&count == sizeof line)) {
				AKA_mark("lis===490###sois===13367###eois===13397###lif===74###soif===1774###eoif===1804###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");fprintf(stderr, "overflow\n");

				AKA_mark("lis===491###sois===13400###eois===13422###lif===75###soif===1807###eoif===1829###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");exit(Exit_Input_Fail);

	}
	else {AKA_mark("lis===-489-###sois===-13341-###eois===-1334120-###lif===-73-###soif===-###eoif===-1768-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");}

		if (AKA_mark("lis===493###sois===13431###eois===13436###lif===77###soif===1838###eoif===1843###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()") && (AKA_mark("lis===493###sois===13431###eois===13436###lif===77###soif===1838###eoif===1843###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()")&&count)) {
		AKA_mark("lis===494###sois===13440###eois===13473###lif===78###soif===1847###eoif===1880###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");memmove(line, line + pos, count);
	}
	else {AKA_mark("lis===-493-###sois===-13431-###eois===-134315-###lif===-77-###soif===-###eoif===-1843-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");}


		AKA_mark("lis===496###sois===13476###eois===13490###lif===80###soif===1883###eoif===1897###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/process_stdin()");return result;

}

/* called when something happens on stdin */
/** Instrumented function on_stdin(sd_event_source*,int,uint32_t,void*) */
static int on_stdin(sd_event_source *src, int fd, uint32_t revents, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/on_stdin(sd_event_source*,int,uint32_t,void*)");AKA_fCall++;
		AKA_mark("lis===502###sois===13625###eois===13641###lif===2###soif===86###eoif===102###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_stdin(sd_event_source*,int,uint32_t,void*)");process_stdin();

		if (AKA_mark("lis===503###sois===13647###eois===13653###lif===3###soif===108###eoif===114###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_stdin(sd_event_source*,int,uint32_t,void*)") && (AKA_mark("lis===503###sois===13647###eois===13653###lif===3###soif===108###eoif===114###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_stdin(sd_event_source*,int,uint32_t,void*)")&&!usein)) {
				AKA_mark("lis===504###sois===13659###eois===13686###lif===4###soif===120###eoif===147###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_stdin(sd_event_source*,int,uint32_t,void*)");sd_event_source_unref(src);

				AKA_mark("lis===505###sois===13689###eois===13702###lif===5###soif===150###eoif===163###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_stdin(sd_event_source*,int,uint32_t,void*)");evsrc = NULL;

	}
	else {AKA_mark("lis===-503-###sois===-13647-###eois===-136476-###lif===-3-###soif===-###eoif===-114-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_stdin(sd_event_source*,int,uint32_t,void*)");}

		AKA_mark("lis===507###sois===13707###eois===13716###lif===7###soif===168###eoif===177###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_stdin(sd_event_source*,int,uint32_t,void*)");return 1;

}

/** Instrumented function on_pws_reply(void*,void*,struct json_object*,const char*,const char*) */
static void on_pws_reply(void *closure, void *request, struct json_object *result, const char *error, const char *info)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===512###sois===13843###eois===13865###lif===2###soif===123###eoif===145###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");int iserror = !!error;

		AKA_mark("lis===513###sois===13867###eois===13914###lif===3###soif===147###eoif===194###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");exitcode = iserror ? Exit_Error : Exit_Success;

		AKA_mark("lis===514###sois===13916###eois===13943###lif===4###soif===196###eoif===223###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");error = error ?: "success";

		if (AKA_mark("lis===515###sois===13949###eois===13952###lif===5###soif===229###eoif===232###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)") && (AKA_mark("lis===515###sois===13949###eois===13952###lif===5###soif===229###eoif===232###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)")&&raw)) {
		/* TODO: transitionnal: fake the structured response */
				AKA_mark("lis===517###sois===14016###eois===14096###lif===7###soif===296###eoif===376###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");struct json_object *x = json_object_new_object(), *y = json_object_new_object();

				AKA_mark("lis===518###sois===14099###eois===14171###lif===8###soif===379###eoif===451###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");json_object_object_add(x, "jtype", json_object_new_string("afb-reply"));

				AKA_mark("lis===519###sois===14174###eois===14214###lif===9###soif===454###eoif===494###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");json_object_object_add(x, "request", y);

				AKA_mark("lis===520###sois===14217###eois===14284###lif===10###soif===497###eoif===564###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");json_object_object_add(y, "status", json_object_new_string(error));

				if (AKA_mark("lis===521###sois===14291###eois===14295###lif===11###soif===571###eoif===575###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)") && (AKA_mark("lis===521###sois===14291###eois===14295###lif===11###soif===571###eoif===575###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)")&&info)) {
			AKA_mark("lis===522###sois===14300###eois===14364###lif===12###soif===580###eoif===644###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");json_object_object_add(y, "info", json_object_new_string(info));
		}
		else {AKA_mark("lis===-521-###sois===-14291-###eois===-142914-###lif===-11-###soif===-###eoif===-575-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");}

				if (AKA_mark("lis===523###sois===14371###eois===14377###lif===13###soif===651###eoif===657###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)") && (AKA_mark("lis===523###sois===14371###eois===14377###lif===13###soif===651###eoif===657###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)")&&result)) {
			AKA_mark("lis===524###sois===14382###eois===14445###lif===14###soif===662###eoif===725###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");json_object_object_add(x, "response", json_object_get(result));
		}
		else {AKA_mark("lis===-523-###sois===-14371-###eois===-143716-###lif===-13-###soif===-###eoif===-657-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");}


				AKA_mark("lis===526###sois===14449###eois===14531###lif===16###soif===729###eoif===811###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");printf("%s\n", json_object_to_json_string_ext(x, JSON_C_TO_STRING_NOSLASHESCAPE));

				AKA_mark("lis===527###sois===14534###eois===14553###lif===17###soif===814###eoif===833###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");json_object_put(x);

	}
	else {AKA_mark("lis===-515-###sois===-13949-###eois===-139493-###lif===-5-###soif===-###eoif===-232-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");}

		if (AKA_mark("lis===529###sois===14562###eois===14567###lif===19###soif===842###eoif===847###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)") && (AKA_mark("lis===529###sois===14562###eois===14567###lif===19###soif===842###eoif===847###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)")&&human)) {
		AKA_mark("lis===530###sois===14571###eois===14737###lif===20###soif===851###eoif===1017###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");printf("ON-REPLY %s: %s %s\n%s\n", (char*)request, error, info ?: "", json_object_to_json_string_ext(result, JSON_C_TO_STRING_PRETTY|JSON_C_TO_STRING_NOSLASHESCAPE));
	}
	else {AKA_mark("lis===-529-###sois===-14562-###eois===-145625-###lif===-19-###soif===-###eoif===-847-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");}

		AKA_mark("lis===531###sois===14739###eois===14754###lif===21###soif===1019###eoif===1034###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");fflush(stdout);

		AKA_mark("lis===532###sois===14756###eois===14770###lif===22###soif===1036###eoif===1050###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");free(request);

		AKA_mark("lis===533###sois===14772###eois===14788###lif===23###soif===1052###eoif===1068###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_reply(void*,void*,struct json_object*,const char*,const char*)");dec_callcount();

}

/** Instrumented function on_pws_event_create(void*,uint16_t,const char*) */
static void on_pws_event_create(void *closure, uint16_t event_id, const char *event_name)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_create(void*,uint16_t,const char*)");AKA_fCall++;
		AKA_mark("lis===538###sois===14885###eois===14944###lif===2###soif===93###eoif===152###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_create(void*,uint16_t,const char*)");printf("ON-EVENT-CREATE: [%d:%s]\n", event_id, event_name);

		AKA_mark("lis===539###sois===14946###eois===14961###lif===3###soif===154###eoif===169###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_create(void*,uint16_t,const char*)");fflush(stdout);

}

/** Instrumented function on_pws_event_remove(void*,uint16_t) */
static void on_pws_event_remove(void *closure, uint16_t event_id)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_remove(void*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===544###sois===15034###eois===15078###lif===2###soif===69###eoif===113###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_remove(void*,uint16_t)");printf("ON-EVENT-REMOVE: [%d]\n", event_id);

		AKA_mark("lis===545###sois===15080###eois===15095###lif===3###soif===115###eoif===130###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_remove(void*,uint16_t)");fflush(stdout);

}

/** Instrumented function on_pws_event_subscribe(void*,void*,uint16_t) */
static void on_pws_event_subscribe(void *closure, void *request, uint16_t event_id)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_subscribe(void*,void*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===550###sois===15186###eois===15252###lif===2###soif===87###eoif===153###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_subscribe(void*,void*,uint16_t)");printf("ON-EVENT-SUBSCRIBE %s: [%d]\n", (char*)request, event_id);

		AKA_mark("lis===551###sois===15254###eois===15269###lif===3###soif===155###eoif===170###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_subscribe(void*,void*,uint16_t)");fflush(stdout);

}

/** Instrumented function on_pws_event_unsubscribe(void*,void*,uint16_t) */
static void on_pws_event_unsubscribe(void *closure, void *request, uint16_t event_id)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_unsubscribe(void*,void*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===556###sois===15362###eois===15430###lif===2###soif===89###eoif===157###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_unsubscribe(void*,void*,uint16_t)");printf("ON-EVENT-UNSUBSCRIBE %s: [%d]\n", (char*)request, event_id);

		AKA_mark("lis===557###sois===15432###eois===15447###lif===3###soif===159###eoif===174###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_unsubscribe(void*,void*,uint16_t)");fflush(stdout);

}

/** Instrumented function on_pws_event_push(void*,uint16_t,struct json_object*) */
static void on_pws_event_push(void *closure, uint16_t event_id, struct json_object *data)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_push(void*,uint16_t,struct json_object*)");AKA_fCall++;
		if (AKA_mark("lis===562###sois===15548###eois===15551###lif===2###soif===97###eoif===100###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_push(void*,uint16_t,struct json_object*)") && (AKA_mark("lis===562###sois===15548###eois===15551###lif===2###soif===97###eoif===100###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_push(void*,uint16_t,struct json_object*)")&&raw)) {
		AKA_mark("lis===563###sois===15555###eois===15671###lif===3###soif===104###eoif===220###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_push(void*,uint16_t,struct json_object*)");printf("ON-EVENT-PUSH: [%d]\n%s\n", event_id, json_object_to_json_string_ext(data, JSON_C_TO_STRING_NOSLASHESCAPE));
	}
	else {AKA_mark("lis===-562-###sois===-15548-###eois===-155483-###lif===-2-###soif===-###eoif===-100-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_push(void*,uint16_t,struct json_object*)");}

		if (AKA_mark("lis===564###sois===15677###eois===15682###lif===4###soif===226###eoif===231###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_push(void*,uint16_t,struct json_object*)") && (AKA_mark("lis===564###sois===15677###eois===15682###lif===4###soif===226###eoif===231###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_push(void*,uint16_t,struct json_object*)")&&human)) {
		AKA_mark("lis===565###sois===15686###eois===15826###lif===5###soif===235###eoif===375###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_push(void*,uint16_t,struct json_object*)");printf("ON-EVENT-PUSH: [%d]\n%s\n", event_id, json_object_to_json_string_ext(data, JSON_C_TO_STRING_PRETTY|JSON_C_TO_STRING_NOSLASHESCAPE));
	}
	else {AKA_mark("lis===-564-###sois===-15677-###eois===-156775-###lif===-4-###soif===-###eoif===-231-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_push(void*,uint16_t,struct json_object*)");}

		AKA_mark("lis===566###sois===15828###eois===15843###lif===6###soif===377###eoif===392###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_push(void*,uint16_t,struct json_object*)");fflush(stdout);

}

/** Instrumented function on_pws_event_broadcast(void*,const char*,struct json_object*,afb_proto_ws_uuid_t,uint8_t) */
static void on_pws_event_broadcast(void *closure, const char *event_name, struct json_object *data, const afb_proto_ws_uuid_t uuid, uint8_t hop)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_broadcast(void*,const char*,struct json_object*,afb_proto_ws_uuid_t,uint8_t)");AKA_fCall++;
		if (AKA_mark("lis===571###sois===15999###eois===16002###lif===2###soif===152###eoif===155###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_broadcast(void*,const char*,struct json_object*,afb_proto_ws_uuid_t,uint8_t)") && (AKA_mark("lis===571###sois===15999###eois===16002###lif===2###soif===152###eoif===155###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_broadcast(void*,const char*,struct json_object*,afb_proto_ws_uuid_t,uint8_t)")&&raw)) {
		AKA_mark("lis===572###sois===16006###eois===16129###lif===3###soif===159###eoif===282###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_broadcast(void*,const char*,struct json_object*,afb_proto_ws_uuid_t,uint8_t)");printf("ON-EVENT-BROADCAST: [%s]\n%s\n", event_name, json_object_to_json_string_ext(data, JSON_C_TO_STRING_NOSLASHESCAPE));
	}
	else {AKA_mark("lis===-571-###sois===-15999-###eois===-159993-###lif===-2-###soif===-###eoif===-155-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_broadcast(void*,const char*,struct json_object*,afb_proto_ws_uuid_t,uint8_t)");}

		if (AKA_mark("lis===573###sois===16135###eois===16140###lif===4###soif===288###eoif===293###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_broadcast(void*,const char*,struct json_object*,afb_proto_ws_uuid_t,uint8_t)") && (AKA_mark("lis===573###sois===16135###eois===16140###lif===4###soif===288###eoif===293###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_broadcast(void*,const char*,struct json_object*,afb_proto_ws_uuid_t,uint8_t)")&&human)) {
		AKA_mark("lis===574###sois===16144###eois===16291###lif===5###soif===297###eoif===444###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_broadcast(void*,const char*,struct json_object*,afb_proto_ws_uuid_t,uint8_t)");printf("ON-EVENT-BROADCAST: [%s]\n%s\n", event_name, json_object_to_json_string_ext(data, JSON_C_TO_STRING_PRETTY|JSON_C_TO_STRING_NOSLASHESCAPE));
	}
	else {AKA_mark("lis===-573-###sois===-16135-###eois===-161355-###lif===-4-###soif===-###eoif===-293-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_broadcast(void*,const char*,struct json_object*,afb_proto_ws_uuid_t,uint8_t)");}

		AKA_mark("lis===575###sois===16293###eois===16308###lif===6###soif===446###eoif===461###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_event_broadcast(void*,const char*,struct json_object*,afb_proto_ws_uuid_t,uint8_t)");fflush(stdout);

}

/* makes a call */
/** Instrumented function pws_call(const char*,const char*) */
static void pws_call(const char *verb, const char *object)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===581###sois===16393###eois===16412###lif===2###soif===62###eoif===81###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");static int num = 0;

		AKA_mark("lis===582###sois===16414###eois===16424###lif===3###soif===83###eoif===93###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");char *key;

		AKA_mark("lis===583###sois===16426###eois===16433###lif===4###soif===95###eoif===102###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");int rc;

		AKA_mark("lis===584###sois===16435###eois===16457###lif===5###soif===104###eoif===126###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");struct json_object *o;

		AKA_mark("lis===585###sois===16459###eois===16488###lif===6###soif===128###eoif===157###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");enum json_tokener_error jerr;


	/* allocates an id for the request */
		AKA_mark("lis===588###sois===16530###eois===16572###lif===9###soif===199###eoif===241###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");rc = asprintf(&key, "%d:%s", ++num, verb);


	/* echo the command if asked */
		if (AKA_mark("lis===591###sois===16612###eois===16616###lif===12###soif===281###eoif===285###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)") && (AKA_mark("lis===591###sois===16612###eois===16616###lif===12###soif===281###eoif===285###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)")&&echo)) {
		AKA_mark("lis===592###sois===16620###eois===16671###lif===13###soif===289###eoif===340###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");printf("SEND-CALL: %s %s\n", verb, object?:"null");
	}
	else {AKA_mark("lis===-591-###sois===-16612-###eois===-166124-###lif===-12-###soif===-###eoif===-285-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");}


	/* send the request */
		AKA_mark("lis===595###sois===16698###eois===16710###lif===16###soif===367###eoif===379###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");callcount++;

		if (AKA_mark("lis===596###sois===16716###eois===16748###lif===17###soif===385###eoif===417###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)") && ((AKA_mark("lis===596###sois===16716###eois===16730###lif===17###soif===385###eoif===399###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)")&&object == NULL)	||(AKA_mark("lis===596###sois===16734###eois===16748###lif===17###soif===403###eoif===417###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)")&&object[0] == 0))) {
		AKA_mark("lis===597###sois===16752###eois===16761###lif===18###soif===421###eoif===430###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");o = NULL;
	}
	else {
				AKA_mark("lis===599###sois===16772###eois===16818###lif===20###soif===441###eoif===487###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");o = json_tokener_parse_verbose(object, &jerr);

				if (AKA_mark("lis===600###sois===16825###eois===16853###lif===21###soif===494###eoif===522###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)") && (AKA_mark("lis===600###sois===16825###eois===16853###lif===21###soif===494###eoif===522###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)")&&jerr != json_tokener_success)) {
			AKA_mark("lis===601###sois===16858###eois===16893###lif===22###soif===527###eoif===562###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");o = json_object_new_string(object);
		}
		else {AKA_mark("lis===-600-###sois===-16825-###eois===-1682528-###lif===-21-###soif===-###eoif===-522-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");}

	}

		AKA_mark("lis===603###sois===16898###eois===16972###lif===24###soif===567###eoif===641###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");rc = afb_proto_ws_client_call(pws, verb, o, numuuid, numtoken, key, NULL);

		AKA_mark("lis===604###sois===16974###eois===16993###lif===25###soif===643###eoif===662###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");json_object_put(o);

		if (AKA_mark("lis===605###sois===16999###eois===17005###lif===26###soif===668###eoif===674###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)") && (AKA_mark("lis===605###sois===16999###eois===17005###lif===26###soif===668###eoif===674###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)")&&rc < 0)) {
				AKA_mark("lis===606###sois===17011###eois===17076###lif===27###soif===680###eoif===745###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");fprintf(stderr, "calling %s(%s) failed: %m\n", verb, object?:"");

				AKA_mark("lis===607###sois===17079###eois===17095###lif===28###soif===748###eoif===764###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");dec_callcount();

	}
	else {AKA_mark("lis===-605-###sois===-16999-###eois===-169996-###lif===-26-###soif===-###eoif===-674-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");}

		if (AKA_mark("lis===609###sois===17104###eois===17112###lif===30###soif===773###eoif===781###ifc===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)") && (AKA_mark("lis===609###sois===17104###eois===17112###lif===30###soif===773###eoif===781###isc===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)")&&breakcon)) {
		AKA_mark("lis===610###sois===17116###eois===17124###lif===31###soif===785###eoif===793###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");exit(0);
	}
	else {AKA_mark("lis===-609-###sois===-17104-###eois===-171048-###lif===-30-###soif===-###eoif===-781-###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/pws_call(const char*,const char*)");}

}

/* called when pws hangsup */
/** Instrumented function on_pws_hangup(void*) */
static void on_pws_hangup(void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-client-demo.c/on_pws_hangup(void*)");AKA_fCall++;
		AKA_mark("lis===616###sois===17202###eois===17224###lif===2###soif===44###eoif===66###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_hangup(void*)");printf("ON-HANGUP\n");

		AKA_mark("lis===617###sois===17226###eois===17241###lif===3###soif===68###eoif===83###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_hangup(void*)");fflush(stdout);

		AKA_mark("lis===618###sois===17243###eois===17261###lif===4###soif===85###eoif===103###ins===true###function===./app-framework-binder/src/main-afb-client-demo.c/on_pws_hangup(void*)");exit(Exit_HangUp);

}

#endif

