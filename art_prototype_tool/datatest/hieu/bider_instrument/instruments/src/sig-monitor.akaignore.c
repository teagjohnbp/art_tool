/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_SIG_MONITOR_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_SIG_MONITOR_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

/*******************************************************************************
*  sig-monitor is under the control of several compilation flags
*******************************************************************************/

/* controls whether to dump stack or not */
#if !defined(WITH_SIG_MONITOR_DUMPSTACK)
#  define WITH_SIG_MONITOR_DUMPSTACK 1
#endif

/* control whether to monitor signals */
#if !defined(WITH_SIG_MONITOR_SIGNALS)
#  define WITH_SIG_MONITOR_SIGNALS 1
#endif

/* controls whether to monitor calls */
#if !defined(WITH_SIG_MONITOR_FOR_CALL)
#  define WITH_SIG_MONITOR_FOR_CALL 1
#endif

/* control whether to monitor timers */
#if !defined(WITH_SIG_MONITOR_TIMERS)
#  define WITH_SIG_MONITOR_TIMERS 1
#endif

#if !WITH_SIG_MONITOR_SIGNALS
#  undef WITH_SIG_MONITOR_FOR_CALL
#  define WITH_SIG_MONITOR_FOR_CALL 0
#endif

#if !WITH_SIG_MONITOR_FOR_CALL
#  undef WITH_SIG_MONITOR_TIMERS
#  define WITH_SIG_MONITOR_TIMERS 0
#endif

/******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__SIG_MONITOR_H_
#define AKA_INCLUDE__SIG_MONITOR_H_
#include "sig-monitor.akaignore.h"
#endif


/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif


/******************************************************************************/
#if !WITH_SIG_MONITOR_DUMPSTACK

static inline void dumpstack(int crop, int signum) {}

#else

#include <execinfo.h>

/*
 * Dumps the current stack
 */
/** Instrumented function dumpstack(int,int) */
static void dumpstack(int crop, int signum)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");AKA_fCall++;
		AKA_mark("lis===79###sois===2106###eois===2125###lif===2###soif===47###eoif===66###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");int idx, count, rc;

		AKA_mark("lis===80###sois===2127###eois===2148###lif===3###soif===68###eoif===89###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");void *addresses[100];

		AKA_mark("lis===81###sois===2150###eois===2167###lif===4###soif===91###eoif===108###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");char **locations;

		AKA_mark("lis===82###sois===2169###eois===2187###lif===5###soif===110###eoif===128###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");char buffer[8000];

		AKA_mark("lis===83###sois===2189###eois===2208###lif===6###soif===130###eoif===149###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");size_t pos, length;


		AKA_mark("lis===85###sois===2211###eois===2278###lif===8###soif===152###eoif===219###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");count = backtrace(addresses, sizeof addresses / sizeof *addresses);

		if (AKA_mark("lis===86###sois===2284###eois===2297###lif===9###soif===225###eoif===238###ifc===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)") && (AKA_mark("lis===86###sois===2284###eois===2297###lif===9###soif===225###eoif===238###isc===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)")&&count <= crop)) {
		AKA_mark("lis===87###sois===2301###eois===2310###lif===10###soif===242###eoif===251###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");crop = 0;
	}
	else {AKA_mark("lis===-86-###sois===-2284-###eois===-228413-###lif===-9-###soif===-###eoif===-238-###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");}

		AKA_mark("lis===88###sois===2312###eois===2326###lif===11###soif===253###eoif===267###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");count -= crop;

		AKA_mark("lis===89###sois===2328###eois===2383###lif===12###soif===269###eoif===324###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");locations = backtrace_symbols(&addresses[crop], count);

		if (AKA_mark("lis===90###sois===2389###eois===2406###lif===13###soif===330###eoif===347###ifc===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)") && (AKA_mark("lis===90###sois===2389###eois===2406###lif===13###soif===330###eoif===347###isc===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)")&&locations == NULL)) {
		AKA_mark("lis===91###sois===2410###eois===2474###lif===14###soif===351###eoif===415###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");ERROR("can't get the backtrace (returned %d addresses)", count);
	}
	else {
				AKA_mark("lis===93###sois===2485###eois===2512###lif===16###soif===426###eoif===453###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");length = sizeof buffer - 1;

				AKA_mark("lis===94###sois===2515###eois===2523###lif===17###soif===456###eoif===464###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");pos = 0;

				AKA_mark("lis===95###sois===2526###eois===2534###lif===18###soif===467###eoif===475###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");idx = 0;

				while (AKA_mark("lis===96###sois===2544###eois===2571###lif===19###soif===485###eoif===512###ifc===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)") && ((AKA_mark("lis===96###sois===2544###eois===2556###lif===19###soif===485###eoif===497###isc===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)")&&pos < length)	&&(AKA_mark("lis===96###sois===2560###eois===2571###lif===19###soif===501###eoif===512###isc===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)")&&idx < count))) {
						AKA_mark("lis===97###sois===2578###eois===2669###lif===20###soif===519###eoif===610###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");rc = snprintf(&buffer[pos], length - pos, " [%d/%d] %s\n", idx + 1, count, locations[idx]);

						AKA_mark("lis===98###sois===2673###eois===2697###lif===21###soif===614###eoif===638###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");pos += rc >= 0 ? rc : 0;

						AKA_mark("lis===99###sois===2701###eois===2707###lif===22###soif===642###eoif===648###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");idx++;

		}

				AKA_mark("lis===101###sois===2714###eois===2733###lif===24###soif===655###eoif===674###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");buffer[length] = 0;

				if (AKA_mark("lis===102###sois===2740###eois===2746###lif===25###soif===681###eoif===687###ifc===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)") && (AKA_mark("lis===102###sois===2740###eois===2746###lif===25###soif===681###eoif===687###isc===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)")&&signum)) {
			AKA_mark("lis===103###sois===2751###eois===2830###lif===26###soif===692###eoif===771###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");ERROR("BACKTRACE due to signal %s/%d:\n%s", strsignal(signum), signum, buffer);
		}
		else {
			AKA_mark("lis===105###sois===2841###eois===2873###lif===28###soif===782###eoif===814###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");ERROR("BACKTRACE:\n%s", buffer);
		}

				AKA_mark("lis===106###sois===2876###eois===2892###lif===29###soif===817###eoif===833###ins===true###function===./app-framework-binder/src/sig-monitor.c/dumpstack(int,int)");free(locations);

	}

}

#endif
/******************************************************************************/
#if !WITH_SIG_MONITOR_TIMERS

static inline int timeout_create() { return 0; }
static inline int timeout_arm(int timeout) { return 0; }
static inline void timeout_disarm() {}
static inline void timeout_delete() {}

#define SIG_FOR_TIMER   0

#else

#include <time.h>
#include <sys/syscall.h>
#include <signal.h>

#define SIG_FOR_TIMER   SIGVTALRM

/* local per thread timers */
static _Thread_local int thread_timer_set;
static _Thread_local timer_t thread_timerid;

/*
 * Creates a timer for the current thread
 *
 * Returns 0 in case of success
 */
/** Instrumented function timeout_create() */
static inline int timeout_create()
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/timeout_create()");AKA_fCall++;
		AKA_mark("lis===140###sois===3576###eois===3583###lif===2###soif===38###eoif===45###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_create()");int rc;

		AKA_mark("lis===141###sois===3585###eois===3606###lif===3###soif===47###eoif===68###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_create()");struct sigevent sevp;


		if (AKA_mark("lis===143###sois===3613###eois===3629###lif===5###soif===75###eoif===91###ifc===true###function===./app-framework-binder/src/sig-monitor.c/timeout_create()") && (AKA_mark("lis===143###sois===3613###eois===3629###lif===5###soif===75###eoif===91###isc===true###function===./app-framework-binder/src/sig-monitor.c/timeout_create()")&&thread_timer_set)) {
		AKA_mark("lis===144###sois===3633###eois===3640###lif===6###soif===95###eoif===102###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_create()");rc = 0;
	}
	else {
				AKA_mark("lis===146###sois===3651###eois===3687###lif===8###soif===113###eoif===149###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_create()");sevp.sigev_notify = SIGEV_THREAD_ID;

				AKA_mark("lis===147###sois===3690###eois===3723###lif===9###soif===152###eoif===185###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_create()");sevp.sigev_signo = SIG_FOR_TIMER;

				AKA_mark("lis===148###sois===3726###eois===3760###lif===10###soif===188###eoif===222###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_create()");sevp.sigev_value.sival_ptr = NULL;

#if defined(sigev_notify_thread_id)
		sevp.sigev_notify_thread_id = (pid_t)syscall(SYS_gettid);
#else
				AKA_mark("lis===152###sois===3865###eois===3914###lif===14###soif===327###eoif===376###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_create()");sevp._sigev_un._tid = (pid_t)syscall(SYS_gettid);

#endif
				AKA_mark("lis===154###sois===3924###eois===3991###lif===16###soif===386###eoif===453###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_create()");rc = timer_create(CLOCK_THREAD_CPUTIME_ID, &sevp, &thread_timerid);

				AKA_mark("lis===155###sois===3994###eois===4017###lif===17###soif===456###eoif===479###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_create()");thread_timer_set = !rc;

	}

		AKA_mark("lis===157###sois===4022###eois===4032###lif===19###soif===484###eoif===494###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_create()");return rc;

}

/*
 * Arms the alarm in timeout seconds for the current thread
 */
/** Instrumented function timeout_arm(int) */
static inline int timeout_arm(int timeout)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/timeout_arm(int)");AKA_fCall++;
		AKA_mark("lis===165###sois===4149###eois===4156###lif===2###soif===46###eoif===53###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_arm(int)");int rc;

		AKA_mark("lis===166###sois===4158###eois===4180###lif===3###soif===55###eoif===77###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_arm(int)");struct itimerspec its;


		AKA_mark("lis===168###sois===4183###eois===4205###lif===5###soif===80###eoif===102###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_arm(int)");rc = timeout_create();

		if (AKA_mark("lis===169###sois===4211###eois===4218###lif===6###soif===108###eoif===115###ifc===true###function===./app-framework-binder/src/sig-monitor.c/timeout_arm(int)") && (AKA_mark("lis===169###sois===4211###eois===4218###lif===6###soif===108###eoif===115###isc===true###function===./app-framework-binder/src/sig-monitor.c/timeout_arm(int)")&&rc == 0)) {
				AKA_mark("lis===170###sois===4224###eois===4251###lif===7###soif===121###eoif===148###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_arm(int)");its.it_interval.tv_sec = 0;

				AKA_mark("lis===171###sois===4254###eois===4282###lif===8###soif===151###eoif===179###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_arm(int)");its.it_interval.tv_nsec = 0;

				AKA_mark("lis===172###sois===4285###eois===4315###lif===9###soif===182###eoif===212###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_arm(int)");its.it_value.tv_sec = timeout;

				AKA_mark("lis===173###sois===4318###eois===4343###lif===10###soif===215###eoif===240###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_arm(int)");its.it_value.tv_nsec = 0;

				AKA_mark("lis===174###sois===4346###eois===4396###lif===11###soif===243###eoif===293###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_arm(int)");rc = timer_settime(thread_timerid, 0, &its, NULL);

	}
	else {AKA_mark("lis===-169-###sois===-4211-###eois===-42117-###lif===-6-###soif===-###eoif===-115-###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_arm(int)");}


		AKA_mark("lis===177###sois===4402###eois===4412###lif===14###soif===299###eoif===309###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_arm(int)");return rc;

}

/*
 * Disarms the current alarm
 */
/** Instrumented function timeout_disarm() */
static inline void timeout_disarm()
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/timeout_disarm()");AKA_fCall++;
		if (AKA_mark("lis===185###sois===4495###eois===4511###lif===2###soif===43###eoif===59###ifc===true###function===./app-framework-binder/src/sig-monitor.c/timeout_disarm()") && (AKA_mark("lis===185###sois===4495###eois===4511###lif===2###soif===43###eoif===59###isc===true###function===./app-framework-binder/src/sig-monitor.c/timeout_disarm()")&&thread_timer_set)) {
		AKA_mark("lis===186###sois===4515###eois===4530###lif===3###soif===63###eoif===78###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_disarm()");timeout_arm(0);
	}
	else {AKA_mark("lis===-185-###sois===-4495-###eois===-449516-###lif===-2-###soif===-###eoif===-59-###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_disarm()");}

}

/*
 * Destroy any alarm resource for the current thread
 */
/** Instrumented function timeout_delete() */
static inline void timeout_delete()
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/timeout_delete()");AKA_fCall++;
		if (AKA_mark("lis===194###sois===4637###eois===4653###lif===2###soif===43###eoif===59###ifc===true###function===./app-framework-binder/src/sig-monitor.c/timeout_delete()") && (AKA_mark("lis===194###sois===4637###eois===4653###lif===2###soif===43###eoif===59###isc===true###function===./app-framework-binder/src/sig-monitor.c/timeout_delete()")&&thread_timer_set)) {
				AKA_mark("lis===195###sois===4659###eois===4688###lif===3###soif===65###eoif===94###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_delete()");timer_delete(thread_timerid);

				AKA_mark("lis===196###sois===4691###eois===4712###lif===4###soif===97###eoif===118###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_delete()");thread_timer_set = 0;

	}
	else {AKA_mark("lis===-194-###sois===-4637-###eois===-463716-###lif===-2-###soif===-###eoif===-59-###ins===true###function===./app-framework-binder/src/sig-monitor.c/timeout_delete()");}

}
#endif
/******************************************************************************/
#if !WITH_SIG_MONITOR_FOR_CALL

static inline void monitor_raise(int signum) {}

#else

#include <setjmp.h>

/* local handler */
static _Thread_local sigjmp_buf *error_handler;

/** Instrumented function monitor(int,void(*function)(int sig, void*),void*) */
static void monitor(int timeout, void (*function)(int sig, void*), void *arg)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");AKA_fCall++;
		AKA_mark("lis===214###sois===5065###eois===5094###lif===2###soif===81###eoif===110###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");volatile int signum, signum2;

		AKA_mark("lis===215###sois===5096###eois===5122###lif===3###soif===112###eoif===138###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");sigjmp_buf jmpbuf, *older;


		AKA_mark("lis===217###sois===5125###eois===5147###lif===5###soif===141###eoif===163###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");older = error_handler;

		AKA_mark("lis===218###sois===5149###eois===5173###lif===6###soif===165###eoif===189###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");signum = setjmp(jmpbuf);

		if (AKA_mark("lis===219###sois===5179###eois===5190###lif===7###soif===195###eoif===206###ifc===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)") && (AKA_mark("lis===219###sois===5179###eois===5190###lif===7###soif===195###eoif===206###isc===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)")&&signum == 0)) {
				AKA_mark("lis===220###sois===5196###eois===5220###lif===8###soif===212###eoif===236###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");error_handler = &jmpbuf;

				if (AKA_mark("lis===221###sois===5227###eois===5234###lif===9###soif===243###eoif===250###ifc===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)") && (AKA_mark("lis===221###sois===5227###eois===5234###lif===9###soif===243###eoif===250###isc===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)")&&timeout)) {
						AKA_mark("lis===222###sois===5241###eois===5258###lif===10###soif===257###eoif===274###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");timeout_create();

						AKA_mark("lis===223###sois===5262###eois===5283###lif===11###soif===278###eoif===299###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");timeout_arm(timeout);

		}
		else {AKA_mark("lis===-221-###sois===-5227-###eois===-52277-###lif===-9-###soif===-###eoif===-250-###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");}

				AKA_mark("lis===225###sois===5290###eois===5307###lif===13###soif===306###eoif===323###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");function(0, arg);

	}
	else {
				AKA_mark("lis===227###sois===5320###eois===5345###lif===15###soif===336###eoif===361###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");signum2 = setjmp(jmpbuf);

				if (AKA_mark("lis===228###sois===5352###eois===5364###lif===16###soif===368###eoif===380###ifc===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)") && (AKA_mark("lis===228###sois===5352###eois===5364###lif===16###soif===368###eoif===380###isc===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)")&&signum2 == 0)) {
			AKA_mark("lis===229###sois===5369###eois===5391###lif===17###soif===385###eoif===407###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");function(signum, arg);
		}
		else {AKA_mark("lis===-228-###sois===-5352-###eois===-535212-###lif===-16-###soif===-###eoif===-380-###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");}

	}

		if (AKA_mark("lis===231###sois===5400###eois===5407###lif===19###soif===416###eoif===423###ifc===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)") && (AKA_mark("lis===231###sois===5400###eois===5407###lif===19###soif===416###eoif===423###isc===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)")&&timeout)) {
		AKA_mark("lis===232###sois===5411###eois===5428###lif===20###soif===427###eoif===444###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");timeout_disarm();
	}
	else {AKA_mark("lis===-231-###sois===-5400-###eois===-54007-###lif===-19-###soif===-###eoif===-423-###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");}

		AKA_mark("lis===233###sois===5430###eois===5452###lif===21###soif===446###eoif===468###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor(int,void(*function)(int sig, void*),void*)");error_handler = older;

}

/** Instrumented function monitor_raise(int) */
static inline void monitor_raise(int signum)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/monitor_raise(int)");AKA_fCall++;
		if (AKA_mark("lis===238###sois===5508###eois===5529###lif===2###soif===52###eoif===73###ifc===true###function===./app-framework-binder/src/sig-monitor.c/monitor_raise(int)") && (AKA_mark("lis===238###sois===5508###eois===5529###lif===2###soif===52###eoif===73###isc===true###function===./app-framework-binder/src/sig-monitor.c/monitor_raise(int)")&&error_handler != NULL)) {
		AKA_mark("lis===239###sois===5533###eois===5565###lif===3###soif===77###eoif===109###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor_raise(int)");longjmp(*error_handler, signum);
	}
	else {AKA_mark("lis===-238-###sois===-5508-###eois===-550821-###lif===-2-###soif===-###eoif===-73-###ins===true###function===./app-framework-binder/src/sig-monitor.c/monitor_raise(int)");}

}
#endif
/******************************************************************************/
#if !WITH_SIG_MONITOR_SIGNALS

static inline int enable_signal_handling() { return 0; }

#else

#include <signal.h>

/* internal signal lists */
static int sigerr[] = { SIGSEGV, SIGFPE, SIGILL, SIGBUS, SIG_FOR_TIMER, 0 };
static int sigterm[] = { SIGINT, SIGABRT, SIGTERM, 0 };

static int exiting = 0;
static int enabled = 0;

/* install the handlers */
/** Instrumented function set_signals_handler(void(*handler)int,int*) */
static int set_signals_handler(void (*handler)(int), int *signals)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)");AKA_fCall++;
		AKA_mark("lis===261###sois===6081###eois===6096###lif===2###soif===70###eoif===85###ins===true###function===./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)");int result = 1;

		AKA_mark("lis===262###sois===6098###eois===6118###lif===3###soif===87###eoif===107###ins===true###function===./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)");struct sigaction sa;


		AKA_mark("lis===264###sois===6121###eois===6145###lif===5###soif===110###eoif===134###ins===true###function===./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)");sa.sa_handler = handler;

		AKA_mark("lis===265###sois===6147###eois===6172###lif===6###soif===136###eoif===161###ins===true###function===./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)");sigemptyset(&sa.sa_mask);

		AKA_mark("lis===266###sois===6174###eois===6199###lif===7###soif===163###eoif===188###ins===true###function===./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)");sa.sa_flags = SA_NODEFER;

		while (AKA_mark("lis===267###sois===6207###eois===6219###lif===8###soif===196###eoif===208###ifc===true###function===./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)") && (AKA_mark("lis===267###sois===6207###eois===6219###lif===8###soif===196###eoif===208###isc===true###function===./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)")&&*signals > 0)) {
				if (AKA_mark("lis===268###sois===6229###eois===6263###lif===9###soif===218###eoif===252###ifc===true###function===./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)") && (AKA_mark("lis===268###sois===6229###eois===6263###lif===9###soif===218###eoif===252###isc===true###function===./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)")&&sigaction(*signals, &sa, NULL) < 0)) {
						AKA_mark("lis===269###sois===6270###eois===6351###lif===10###soif===259###eoif===340###ins===true###function===./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)");ERROR("failed to install signal handler for signal %s: %m", strsignal(*signals));

						AKA_mark("lis===270###sois===6355###eois===6366###lif===11###soif===344###eoif===355###ins===true###function===./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)");result = 0;

		}
		else {AKA_mark("lis===-268-###sois===-6229-###eois===-622934-###lif===-9-###soif===-###eoif===-252-###ins===true###function===./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)");}

				AKA_mark("lis===272###sois===6373###eois===6383###lif===13###soif===362###eoif===372###ins===true###function===./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)");signals++;

	}

		AKA_mark("lis===274###sois===6388###eois===6402###lif===15###soif===377###eoif===391###ins===true###function===./app-framework-binder/src/sig-monitor.c/set_signals_handler(void(*handler)int,int*)");return result;

}

/*
 * rescue exit
 */
/** Instrumented function on_rescue_exit(int) */
static void on_rescue_exit(int signum)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/on_rescue_exit(int)");AKA_fCall++;
		AKA_mark("lis===282###sois===6470###eois===6536###lif===2###soif===42###eoif===108###ins===true###function===./app-framework-binder/src/sig-monitor.c/on_rescue_exit(int)");ERROR("Rescue exit for signal %d: %s", signum, strsignal(signum));

		AKA_mark("lis===283###sois===6538###eois===6553###lif===3###soif===110###eoif===125###ins===true###function===./app-framework-binder/src/sig-monitor.c/on_rescue_exit(int)");_exit(exiting);

}

/*
 * Do a direct safe exit
 */
/** Instrumented function direct_safe_exit(int) */
static void direct_safe_exit(int code)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/direct_safe_exit(int)");AKA_fCall++;
		AKA_mark("lis===291###sois===6631###eois===6675###lif===2###soif===42###eoif===86###ins===true###function===./app-framework-binder/src/sig-monitor.c/direct_safe_exit(int)");set_signals_handler(on_rescue_exit, sigerr);

		AKA_mark("lis===292###sois===6677###eois===6722###lif===3###soif===88###eoif===133###ins===true###function===./app-framework-binder/src/sig-monitor.c/direct_safe_exit(int)");set_signals_handler(on_rescue_exit, sigterm);

		AKA_mark("lis===293###sois===6724###eois===6739###lif===4###soif===135###eoif===150###ins===true###function===./app-framework-binder/src/sig-monitor.c/direct_safe_exit(int)");exiting = code;

		AKA_mark("lis===294###sois===6741###eois===6752###lif===5###soif===152###eoif===163###ins===true###function===./app-framework-binder/src/sig-monitor.c/direct_safe_exit(int)");exit(code);

}

/*
 * Do a safe exit
 */
#if WITH_SIG_MONITOR_NO_DEFERRED_EXIT
#  define safe_exit(x) direct_safe_exit(x)
#else
/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__JOBS_H_
#define AKA_INCLUDE__JOBS_H_
#include "jobs.akaignore.h"
#endif

/** Instrumented function exit_job(int,void*) */
static void exit_job(int signum, void* arg)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/exit_job(int,void*)");AKA_fCall++;
		AKA_mark("lis===306###sois===6933###eois===6962###lif===2###soif===47###eoif===76###ins===true###function===./app-framework-binder/src/sig-monitor.c/exit_job(int,void*)");exiting = (int)(intptr_t)arg;

		if (AKA_mark("lis===307###sois===6968###eois===6974###lif===3###soif===82###eoif===88###ifc===true###function===./app-framework-binder/src/sig-monitor.c/exit_job(int,void*)") && (AKA_mark("lis===307###sois===6968###eois===6974###lif===3###soif===82###eoif===88###isc===true###function===./app-framework-binder/src/sig-monitor.c/exit_job(int,void*)")&&signum)) {
		AKA_mark("lis===308###sois===6978###eois===7001###lif===4###soif===92###eoif===115###ins===true###function===./app-framework-binder/src/sig-monitor.c/exit_job(int,void*)");on_rescue_exit(signum);
	}
	else {AKA_mark("lis===-307-###sois===-6968-###eois===-69686-###lif===-3-###soif===-###eoif===-88-###ins===true###function===./app-framework-binder/src/sig-monitor.c/exit_job(int,void*)");}

		AKA_mark("lis===309###sois===7003###eois===7017###lif===5###soif===117###eoif===131###ins===true###function===./app-framework-binder/src/sig-monitor.c/exit_job(int,void*)");exit(exiting);

}

/** Instrumented function safe_exit(int) */
static void safe_exit(int code)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/safe_exit(int)");AKA_fCall++;
		if (AKA_mark("lis===314###sois===7060###eois===7117###lif===2###soif===39###eoif===96###ifc===true###function===./app-framework-binder/src/sig-monitor.c/safe_exit(int)") && (AKA_mark("lis===314###sois===7060###eois===7117###lif===2###soif===39###eoif===96###isc===true###function===./app-framework-binder/src/sig-monitor.c/safe_exit(int)")&&jobs_queue(safe_exit, 0, exit_job, (void*)(intptr_t)code))) {
		AKA_mark("lis===315###sois===7121###eois===7144###lif===3###soif===100###eoif===123###ins===true###function===./app-framework-binder/src/sig-monitor.c/safe_exit(int)");direct_safe_exit(code);
	}
	else {AKA_mark("lis===-314-###sois===-7060-###eois===-706057-###lif===-2-###soif===-###eoif===-96-###ins===true###function===./app-framework-binder/src/sig-monitor.c/safe_exit(int)");}

}
#endif

#if !WITH_SIG_MONITOR_DUMPSTACK

static inline void safe_dumpstack(int crop, int signum) {}
#define in_safe_dumpstack (0)

#else

static _Thread_local int in_safe_dumpstack;

/** Instrumented function safe_dumpstack_cb(int,void*) */
static void safe_dumpstack_cb(int signum, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/safe_dumpstack_cb(int,void*)");AKA_fCall++;
		AKA_mark("lis===330###sois===7390###eois===7410###lif===2###soif===60###eoif===80###ins===true###function===./app-framework-binder/src/sig-monitor.c/safe_dumpstack_cb(int,void*)");int *args = closure;

		if (AKA_mark("lis===331###sois===7416###eois===7422###lif===3###soif===86###eoif===92###ifc===true###function===./app-framework-binder/src/sig-monitor.c/safe_dumpstack_cb(int,void*)") && (AKA_mark("lis===331###sois===7416###eois===7422###lif===3###soif===86###eoif===92###isc===true###function===./app-framework-binder/src/sig-monitor.c/safe_dumpstack_cb(int,void*)")&&signum)) {
		AKA_mark("lis===332###sois===7426###eois===7496###lif===4###soif===96###eoif===166###ins===true###function===./app-framework-binder/src/sig-monitor.c/safe_dumpstack_cb(int,void*)");ERROR("Can't provide backtrace: raised signal %s", strsignal(signum));
	}
	else {
		AKA_mark("lis===334###sois===7505###eois===7533###lif===6###soif===175###eoif===203###ins===true###function===./app-framework-binder/src/sig-monitor.c/safe_dumpstack_cb(int,void*)");dumpstack(args[0], args[1]);
	}

}

/** Instrumented function safe_dumpstack(int,int) */
static void safe_dumpstack(int crop, int signum)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/safe_dumpstack(int,int)");AKA_fCall++;
		AKA_mark("lis===339###sois===7589###eois===7624###lif===2###soif===52###eoif===87###ins===true###function===./app-framework-binder/src/sig-monitor.c/safe_dumpstack(int,int)");int args[2] = { crop + 3, signum };


		AKA_mark("lis===341###sois===7627###eois===7649###lif===4###soif===90###eoif===112###ins===true###function===./app-framework-binder/src/sig-monitor.c/safe_dumpstack(int,int)");in_safe_dumpstack = 1;

		AKA_mark("lis===342###sois===7651###eois===7691###lif===5###soif===114###eoif===154###ins===true###function===./app-framework-binder/src/sig-monitor.c/safe_dumpstack(int,int)");sig_monitor(0, safe_dumpstack_cb, args);

		AKA_mark("lis===343###sois===7693###eois===7715###lif===6###soif===156###eoif===178###ins===true###function===./app-framework-binder/src/sig-monitor.c/safe_dumpstack(int,int)");in_safe_dumpstack = 0;

}
#endif

/* Handles signals that terminate the process */
/** Instrumented function on_signal_terminate(int) */
static void on_signal_terminate (int signum)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/on_signal_terminate(int)");AKA_fCall++;
		if (AKA_mark("lis===350###sois===7827###eois===7845###lif===2###soif===52###eoif===70###ifc===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_terminate(int)") && (AKA_mark("lis===350###sois===7827###eois===7845###lif===2###soif===52###eoif===70###isc===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_terminate(int)")&&!in_safe_dumpstack)) {
				AKA_mark("lis===351###sois===7851###eois===7922###lif===3###soif===76###eoif===147###ins===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_terminate(int)");ERROR("Terminating signal %d received: %s", signum, strsignal(signum));

				if (AKA_mark("lis===352###sois===7929###eois===7946###lif===4###soif===154###eoif===171###ifc===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_terminate(int)") && (AKA_mark("lis===352###sois===7929###eois===7946###lif===4###soif===154###eoif===171###isc===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_terminate(int)")&&signum == SIGABRT)) {
			AKA_mark("lis===353###sois===7951###eois===7977###lif===5###soif===176###eoif===202###ins===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_terminate(int)");safe_dumpstack(3, signum);
		}
		else {AKA_mark("lis===-352-###sois===-7929-###eois===-792917-###lif===-4-###soif===-###eoif===-171-###ins===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_terminate(int)");}

	}
	else {AKA_mark("lis===-350-###sois===-7827-###eois===-782718-###lif===-2-###soif===-###eoif===-70-###ins===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_terminate(int)");}

		AKA_mark("lis===355###sois===7982###eois===7995###lif===7###soif===207###eoif===220###ins===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_terminate(int)");safe_exit(1);

}

/* Handles monitored signals that can be continued */
/** Instrumented function on_signal_error(int) */
static void on_signal_error(int signum)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/on_signal_error(int)");AKA_fCall++;
		if (AKA_mark("lis===361###sois===8100###eois===8118###lif===2###soif===47###eoif===65###ifc===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_error(int)") && (AKA_mark("lis===361###sois===8100###eois===8118###lif===2###soif===47###eoif===65###isc===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_error(int)")&&!in_safe_dumpstack)) {
				AKA_mark("lis===362###sois===8124###eois===8190###lif===3###soif===71###eoif===137###ins===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_error(int)");ERROR("ALERT! signal %d received: %s", signum, strsignal(signum));


				AKA_mark("lis===364###sois===8194###eois===8220###lif===5###soif===141###eoif===167###ins===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_error(int)");safe_dumpstack(3, signum);

	}
	else {AKA_mark("lis===-361-###sois===-8100-###eois===-810018-###lif===-2-###soif===-###eoif===-65-###ins===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_error(int)");}

		AKA_mark("lis===366###sois===8225###eois===8247###lif===7###soif===172###eoif===194###ins===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_error(int)");monitor_raise(signum);


		if (AKA_mark("lis===368###sois===8254###eois===8277###lif===9###soif===201###eoif===224###ifc===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_error(int)") && (AKA_mark("lis===368###sois===8254###eois===8277###lif===9###soif===201###eoif===224###isc===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_error(int)")&&signum != SIG_FOR_TIMER)) {
				AKA_mark("lis===369###sois===8283###eois===8354###lif===10###soif===230###eoif===301###ins===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_error(int)");ERROR("Unmonitored signal %d received: %s", signum, strsignal(signum));

				AKA_mark("lis===370###sois===8357###eois===8370###lif===11###soif===304###eoif===317###ins===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_error(int)");safe_exit(2);

	}
	else {AKA_mark("lis===-368-###sois===-8254-###eois===-825423-###lif===-9-###soif===-###eoif===-224-###ins===true###function===./app-framework-binder/src/sig-monitor.c/on_signal_error(int)");}

}

/*
static void disable_signal_handling()
{
	set_signals_handler(SIG_DFL, sigerr);
	set_signals_handler(SIG_DFL, sigterm);
	enabled = 0;
}
*/

/** Instrumented function enable_signal_handling() */
static int enable_signal_handling()
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/enable_signal_handling()");AKA_fCall++;
		if (AKA_mark("lis===385###sois===8562###eois===8667###lif===2###soif===43###eoif===148###ifc===true###function===./app-framework-binder/src/sig-monitor.c/enable_signal_handling()") && ((AKA_mark("lis===385###sois===8562###eois===8607###lif===2###soif===43###eoif===88###isc===true###function===./app-framework-binder/src/sig-monitor.c/enable_signal_handling()")&&!set_signals_handler(on_signal_error, sigerr))	||(AKA_mark("lis===386###sois===8617###eois===8667###lif===3###soif===98###eoif===148###isc===true###function===./app-framework-binder/src/sig-monitor.c/enable_signal_handling()")&&!set_signals_handler(on_signal_terminate, sigterm)))) {
				AKA_mark("lis===387###sois===8673###eois===8683###lif===4###soif===154###eoif===164###ins===true###function===./app-framework-binder/src/sig-monitor.c/enable_signal_handling()");return -1;

	}
	else {AKA_mark("lis===-385-###sois===-8562-###eois===-8562105-###lif===-2-###soif===-###eoif===-148-###ins===true###function===./app-framework-binder/src/sig-monitor.c/enable_signal_handling()");}

		AKA_mark("lis===389###sois===8688###eois===8700###lif===6###soif===169###eoif===181###ins===true###function===./app-framework-binder/src/sig-monitor.c/enable_signal_handling()");enabled = 1;

		AKA_mark("lis===390###sois===8702###eois===8711###lif===7###soif===183###eoif===192###ins===true###function===./app-framework-binder/src/sig-monitor.c/enable_signal_handling()");return 0;

}
#endif
/******************************************************************************/

/** Instrumented function sig_monitor_init(int) */
int sig_monitor_init(int enable)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/sig_monitor_init(int)");AKA_fCall++;
		AKA_mark("lis===397###sois===8839###eois===8884###lif===2###soif===36###eoif===81###ins===true###function===./app-framework-binder/src/sig-monitor.c/sig_monitor_init(int)");return enable ? enable_signal_handling() : 0;

}

/** Instrumented function sig_monitor_init_timeouts() */
int sig_monitor_init_timeouts()
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/sig_monitor_init_timeouts()");AKA_fCall++;
		AKA_mark("lis===402###sois===8923###eois===8947###lif===2###soif===35###eoif===59###ins===true###function===./app-framework-binder/src/sig-monitor.c/sig_monitor_init_timeouts()");return timeout_create();

}

/** Instrumented function sig_monitor_clean_timeouts() */
void sig_monitor_clean_timeouts()
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/sig_monitor_clean_timeouts()");AKA_fCall++;
		AKA_mark("lis===407###sois===8988###eois===9005###lif===2###soif===37###eoif===54###ins===true###function===./app-framework-binder/src/sig-monitor.c/sig_monitor_clean_timeouts()");timeout_delete();

}

/** Instrumented function sig_monitor(int,void(*function)(int sig, void*),void*) */
void sig_monitor(int timeout, void (*function)(int sig, void*), void *arg)
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/sig_monitor(int,void(*function)(int sig, void*),void*)");AKA_fCall++;
#if WITH_SIG_MONITOR_SIGNALS && WITH_SIG_MONITOR_FOR_CALL
		if (AKA_mark("lis===413###sois===9149###eois===9156###lif===3###soif===140###eoif===147###ifc===true###function===./app-framework-binder/src/sig-monitor.c/sig_monitor(int,void(*function)(int sig, void*),void*)") && (AKA_mark("lis===413###sois===9149###eois===9156###lif===3###soif===140###eoif===147###isc===true###function===./app-framework-binder/src/sig-monitor.c/sig_monitor(int,void(*function)(int sig, void*),void*)")&&enabled)) {
		AKA_mark("lis===414###sois===9160###eois===9192###lif===4###soif===151###eoif===183###ins===true###function===./app-framework-binder/src/sig-monitor.c/sig_monitor(int,void(*function)(int sig, void*),void*)");monitor(timeout, function, arg);
	}
	else {
		AKA_mark("lis===417###sois===9208###eois===9225###lif===7###soif===199###eoif===216###ins===true###function===./app-framework-binder/src/sig-monitor.c/sig_monitor(int,void(*function)(int sig, void*),void*)");function(0, arg);
	}

}

/** Instrumented function sig_monitor_dumpstack() */
void sig_monitor_dumpstack()
{AKA_mark("Calling: ./app-framework-binder/src/sig-monitor.c/sig_monitor_dumpstack()");AKA_fCall++;
		AKA_mark("lis===422###sois===9261###eois===9284###lif===2###soif===32###eoif===55###ins===true###function===./app-framework-binder/src/sig-monitor.c/sig_monitor_dumpstack()");return dumpstack(1, 0);

}

#endif

