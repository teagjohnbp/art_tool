/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_AUTH_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_AUTH_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author "Fulup Ar Foll"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>

#include <json-c/json.h>
#include <afb/afb-auth.h>
#include <afb/afb-session-x2.h>
#if WITH_LEGACY_BINDING_V1
#include <afb/afb-session-x1.h>
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_AUTH_H_
#define AKA_INCLUDE__AFB_AUTH_H_
#include "afb-auth.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CONTEXT_H_
#define AKA_INCLUDE__AFB_CONTEXT_H_
#include "afb-context.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_XREQ_H_
#define AKA_INCLUDE__AFB_XREQ_H_
#include "afb-xreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif


/** Instrumented function afb_auth_check(struct afb_context*,const struct afb_auth*) */
int afb_auth_check(struct afb_context *context, const struct afb_auth *auth)
{AKA_mark("Calling: ./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");AKA_fCall++;
		AKA_mark("lis===37###sois===1040###eois===1050###lif===2###soif===88###eoif===98###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");switch(auth->type){
			default: if(auth->type != afb_auth_No && auth->type != afb_auth_Token && auth->type != afb_auth_LOA && auth->type != afb_auth_Permission && auth->type != afb_auth_Or && auth->type != afb_auth_And && auth->type != afb_auth_Not && auth->type != afb_auth_Yes)AKA_mark("lis===38###sois===1055###eois===1063###lif===3###soif===103###eoif===111###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");

			case afb_auth_No: if(auth->type == afb_auth_No)AKA_mark("lis===39###sois===1065###eois===1082###lif===4###soif===113###eoif===130###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");

				AKA_mark("lis===40###sois===1085###eois===1094###lif===5###soif===133###eoif===142###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");return 0;


			case afb_auth_Token: if(auth->type == afb_auth_Token)AKA_mark("lis===42###sois===1097###eois===1117###lif===7###soif===145###eoif===165###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");

				AKA_mark("lis===43###sois===1120###eois===1154###lif===8###soif===168###eoif===202###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");return afb_context_check(context);


			case afb_auth_LOA: if(auth->type == afb_auth_LOA)AKA_mark("lis===45###sois===1157###eois===1175###lif===10###soif===205###eoif===223###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");

				AKA_mark("lis===46###sois===1178###eois===1227###lif===11###soif===226###eoif===275###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");return afb_context_check_loa(context, auth->loa);


			case afb_auth_Permission: if(auth->type == afb_auth_Permission)AKA_mark("lis===48###sois===1230###eois===1255###lif===13###soif===278###eoif===303###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");

				AKA_mark("lis===49###sois===1258###eois===1313###lif===14###soif===306###eoif===361###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");return afb_context_has_permission(context, auth->text);


			case afb_auth_Or: if(auth->type == afb_auth_Or)AKA_mark("lis===51###sois===1316###eois===1333###lif===16###soif===364###eoif===381###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");

				AKA_mark("lis===52###sois===1336###eois===1419###lif===17###soif===384###eoif===467###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");return afb_auth_check(context, auth->first) || afb_auth_check(context, auth->next);


			case afb_auth_And: if(auth->type == afb_auth_And)AKA_mark("lis===54###sois===1422###eois===1440###lif===19###soif===470###eoif===488###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");

				AKA_mark("lis===55###sois===1443###eois===1526###lif===20###soif===491###eoif===574###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");return afb_auth_check(context, auth->first) && afb_auth_check(context, auth->next);


			case afb_auth_Not: if(auth->type == afb_auth_Not)AKA_mark("lis===57###sois===1529###eois===1547###lif===22###soif===577###eoif===595###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");

				AKA_mark("lis===58###sois===1550###eois===1595###lif===23###soif===598###eoif===643###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");return !afb_auth_check(context, auth->first);


			case afb_auth_Yes: if(auth->type == afb_auth_Yes)AKA_mark("lis===60###sois===1598###eois===1616###lif===25###soif===646###eoif===664###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");

				AKA_mark("lis===61###sois===1619###eois===1628###lif===26###soif===667###eoif===676###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check(struct afb_context*,const struct afb_auth*)");return 1;

	}

}


#if WITH_LEGACY_BINDING_V1
int afb_auth_check_and_set_session_x1(struct afb_xreq *xreq, int sessionflags)
{
	int loa;

	if ((sessionflags & (AFB_SESSION_CLOSE_X1|AFB_SESSION_CHECK_X1|AFB_SESSION_LOA_EQ_X1)) != 0) {
		if (!afb_context_check(&xreq->context)) {
			afb_context_close(&xreq->context);
			return afb_xreq_reply_invalid_token(xreq);
		}
	}

	if ((sessionflags & AFB_SESSION_LOA_GE_X1) != 0) {
		loa = (sessionflags >> AFB_SESSION_LOA_SHIFT_X1) & AFB_SESSION_LOA_MASK_X1;
		if (!afb_context_check_loa(&xreq->context, loa))
			return afb_xreq_reply_insufficient_scope(xreq, "invalid LOA");
	}

	if ((sessionflags & AFB_SESSION_LOA_LE_X1) != 0) {
		loa = (sessionflags >> AFB_SESSION_LOA_SHIFT_X1) & AFB_SESSION_LOA_MASK_X1;
		if (afb_context_check_loa(&xreq->context, loa + 1))
			return afb_xreq_reply_insufficient_scope(xreq, "invalid LOA");
	}

	if ((sessionflags & AFB_SESSION_CLOSE_X1) != 0) {
		afb_context_change_loa(&xreq->context, 0);
		afb_context_close(&xreq->context);
	}

	return 1;
}
#endif

/** Instrumented function afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t) */
int afb_auth_check_and_set_session_x2(struct afb_xreq *xreq, const struct afb_auth *auth, uint32_t sessionflags)
{AKA_mark("Calling: ./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)");AKA_fCall++;
		AKA_mark("lis===101###sois===2766###eois===2774###lif===2###soif===116###eoif===124###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)");int loa;


		if (AKA_mark("lis===103###sois===2781###eois===2798###lif===4###soif===131###eoif===148###ifc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)") && (AKA_mark("lis===103###sois===2781###eois===2798###lif===4###soif===131###eoif===148###isc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)")&&sessionflags != 0)) {
				if (AKA_mark("lis===104###sois===2808###eois===2842###lif===5###soif===158###eoif===192###ifc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)") && (AKA_mark("lis===104###sois===2808###eois===2842###lif===5###soif===158###eoif===192###isc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)")&&!afb_context_check(&xreq->context))) {
						AKA_mark("lis===105###sois===2849###eois===2883###lif===6###soif===199###eoif===233###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)");afb_context_close(&xreq->context);

						AKA_mark("lis===106###sois===2887###eois===2929###lif===7###soif===237###eoif===279###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)");return afb_xreq_reply_invalid_token(xreq);

		}
		else {AKA_mark("lis===-104-###sois===-2808-###eois===-280834-###lif===-5-###soif===-###eoif===-192-###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)");}

	}
	else {AKA_mark("lis===-103-###sois===-2781-###eois===-278117-###lif===-4-###soif===-###eoif===-148-###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)");}


		AKA_mark("lis===110###sois===2939###eois===2991###lif===11###soif===289###eoif===341###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)");loa = (int)(sessionflags & AFB_SESSION_LOA_MASK_X2);

		if (AKA_mark("lis===111###sois===2997###eois===3047###lif===12###soif===347###eoif===397###ifc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)") && ((AKA_mark("lis===111###sois===2997###eois===3000###lif===12###soif===347###eoif===350###isc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)")&&loa)	&&(AKA_mark("lis===111###sois===3004###eois===3047###lif===12###soif===354###eoif===397###isc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)")&&!afb_context_check_loa(&xreq->context, loa)))) {
		AKA_mark("lis===112###sois===3051###eois===3113###lif===13###soif===401###eoif===463###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)");return afb_xreq_reply_insufficient_scope(xreq, "invalid LOA");
	}
	else {AKA_mark("lis===-111-###sois===-2997-###eois===-299750-###lif===-12-###soif===-###eoif===-397-###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)");}


		if (AKA_mark("lis===114###sois===3120###eois===3165###lif===15###soif===470###eoif===515###ifc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)") && ((AKA_mark("lis===114###sois===3120###eois===3124###lif===15###soif===470###eoif===474###isc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)")&&auth)	&&(AKA_mark("lis===114###sois===3128###eois===3165###lif===15###soif===478###eoif===515###isc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)")&&!afb_auth_check(&xreq->context, auth)))) {
		AKA_mark("lis===115###sois===3169###eois===3233###lif===16###soif===519###eoif===583###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)");return afb_xreq_reply_insufficient_scope(xreq, NULL /* TODO */);
	}
	else {AKA_mark("lis===-114-###sois===-3120-###eois===-312045-###lif===-15-###soif===-###eoif===-515-###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)");}


		if (AKA_mark("lis===117###sois===3240###eois===3282###lif===18###soif===590###eoif===632###ifc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)") && (AKA_mark("lis===117###sois===3240###eois===3282###lif===18###soif===590###eoif===632###isc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)")&&(sessionflags & AFB_SESSION_CLOSE_X2) != 0)) {
		AKA_mark("lis===118###sois===3286###eois===3320###lif===19###soif===636###eoif===670###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)");afb_context_close(&xreq->context);
	}
	else {AKA_mark("lis===-117-###sois===-3240-###eois===-324042-###lif===-18-###soif===-###eoif===-632-###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)");}


		AKA_mark("lis===120###sois===3323###eois===3332###lif===21###soif===673###eoif===682###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_check_and_set_session_x2(struct afb_xreq*,const struct afb_auth*,uint32_t)");return 1;

}

/*********************************************************************************/

/** Instrumented function addperm(struct json_object*,struct json_object*) */
static struct json_object *addperm(struct json_object *o, struct json_object *x)
{AKA_mark("Calling: ./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===127###sois===3505###eois===3527###lif===2###soif===84###eoif===106###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)");struct json_object *a;


		if (AKA_mark("lis===129###sois===3534###eois===3536###lif===4###soif===113###eoif===115###ifc===true###function===./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)") && (AKA_mark("lis===129###sois===3534###eois===3536###lif===4###soif===113###eoif===115###isc===true###function===./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)")&&!o)) {
		AKA_mark("lis===130###sois===3540###eois===3549###lif===5###soif===119###eoif===128###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)");return x;
	}
	else {AKA_mark("lis===-129-###sois===-3534-###eois===-35342-###lif===-4-###soif===-###eoif===-115-###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)");}


		if (AKA_mark("lis===132###sois===3556###eois===3598###lif===7###soif===135###eoif===177###ifc===true###function===./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)") && (AKA_mark("lis===132###sois===3556###eois===3598###lif===7###soif===135###eoif===177###isc===true###function===./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)")&&!json_object_object_get_ex(o, "allOf", &a))) {
				AKA_mark("lis===133###sois===3604###eois===3632###lif===8###soif===183###eoif===211###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)");a = json_object_new_array();

				AKA_mark("lis===134###sois===3635###eois===3663###lif===9###soif===214###eoif===242###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)");json_object_array_add(a, o);

				AKA_mark("lis===135###sois===3666###eois===3695###lif===10###soif===245###eoif===274###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)");o = json_object_new_object();

				AKA_mark("lis===136###sois===3698###eois===3736###lif===11###soif===277###eoif===315###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)");json_object_object_add(o, "allOf", a);

	}
	else {AKA_mark("lis===-132-###sois===-3556-###eois===-355642-###lif===-7-###soif===-###eoif===-177-###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)");}

		AKA_mark("lis===138###sois===3741###eois===3769###lif===13###soif===320###eoif===348###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)");json_object_array_add(a, x);

		AKA_mark("lis===139###sois===3771###eois===3780###lif===14###soif===350###eoif===359###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm(struct json_object*,struct json_object*)");return o;

}

/** Instrumented function addperm_key_val(struct json_object*,const char*,struct json_object*) */
static struct json_object *addperm_key_val(struct json_object *o, const char *key, struct json_object *val)
{AKA_mark("Calling: ./app-framework-binder/src/afb-auth.c/addperm_key_val(struct json_object*,const char*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===144###sois===3895###eois===3944###lif===2###soif===111###eoif===160###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm_key_val(struct json_object*,const char*,struct json_object*)");struct json_object *x = json_object_new_object();

		AKA_mark("lis===145###sois===3946###eois===3982###lif===3###soif===162###eoif===198###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm_key_val(struct json_object*,const char*,struct json_object*)");json_object_object_add(x, key, val);

		AKA_mark("lis===146###sois===3984###eois===4005###lif===4###soif===200###eoif===221###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm_key_val(struct json_object*,const char*,struct json_object*)");return addperm(o, x);

}

/** Instrumented function addperm_key_valstr(struct json_object*,const char*,const char*) */
static struct json_object *addperm_key_valstr(struct json_object *o, const char *key, const char *val)
{AKA_mark("Calling: ./app-framework-binder/src/afb-auth.c/addperm_key_valstr(struct json_object*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===151###sois===4115###eois===4175###lif===2###soif===106###eoif===166###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm_key_valstr(struct json_object*,const char*,const char*)");return addperm_key_val(o, key, json_object_new_string(val));

}

/** Instrumented function addperm_key_valint(struct json_object*,const char*,int) */
static struct json_object *addperm_key_valint(struct json_object *o, const char *key, int val)
{AKA_mark("Calling: ./app-framework-binder/src/afb-auth.c/addperm_key_valint(struct json_object*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===156###sois===4277###eois===4334###lif===2###soif===98###eoif===155###ins===true###function===./app-framework-binder/src/afb-auth.c/addperm_key_valint(struct json_object*,const char*,int)");return addperm_key_val(o, key, json_object_new_int(val));

}

static struct json_object *addauth_or_array(struct json_object *o, const struct afb_auth *auth);

/** Instrumented function addauth(struct json_object*,const struct afb_auth*) */
static struct json_object *addauth(struct json_object *o, const struct afb_auth *auth)
{AKA_mark("Calling: ./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");AKA_fCall++;
		AKA_mark("lis===163###sois===4533###eois===4543###lif===2###soif===97###eoif===107###ins===true###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");switch(auth->type){
			case afb_auth_No: if(auth->type == afb_auth_No)AKA_mark("lis===164###sois===4548###eois===4565###lif===3###soif===112###eoif===129###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");
 		AKA_mark("lis===164###sois===4566###eois===4612###lif===3###soif===130###eoif===176###ins===true###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");return addperm(o, json_object_new_boolean(0));

			case afb_auth_Token: if(auth->type == afb_auth_Token)AKA_mark("lis===165###sois===4614###eois===4634###lif===4###soif===178###eoif===198###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");
 		AKA_mark("lis===165###sois===4635###eois===4684###lif===4###soif===199###eoif===248###ins===true###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");return addperm_key_valstr(o, "session", "check");

			case afb_auth_LOA: if(auth->type == afb_auth_LOA)AKA_mark("lis===166###sois===4686###eois===4704###lif===5###soif===250###eoif===268###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");
 		AKA_mark("lis===166###sois===4705###eois===4752###lif===5###soif===269###eoif===316###ins===true###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");return addperm_key_valint(o, "LOA", auth->loa);

			case afb_auth_Permission: if(auth->type == afb_auth_Permission)AKA_mark("lis===167###sois===4754###eois===4779###lif===6###soif===318###eoif===343###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");
 		AKA_mark("lis===167###sois===4780###eois===4835###lif===6###soif===344###eoif===399###ins===true###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");return addperm_key_valstr(o, "permission", auth->text);

			case afb_auth_Or: if(auth->type == afb_auth_Or)AKA_mark("lis===168###sois===4837###eois===4854###lif===7###soif===401###eoif===418###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");
 		AKA_mark("lis===168###sois===4855###eois===4939###lif===7###soif===419###eoif===503###ins===true###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");return addperm_key_val(o, "anyOf", addauth_or_array(json_object_new_array(), auth));

			case afb_auth_And: if(auth->type == afb_auth_And)AKA_mark("lis===169###sois===4941###eois===4959###lif===8###soif===505###eoif===523###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");
 		AKA_mark("lis===169###sois===4960###eois===5012###lif===8###soif===524###eoif===576###ins===true###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");return addauth(addauth(o, auth->first), auth->next);

			case afb_auth_Not: if(auth->type == afb_auth_Not)AKA_mark("lis===170###sois===5014###eois===5032###lif===9###soif===578###eoif===596###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");
 		AKA_mark("lis===170###sois===5033###eois===5094###lif===9###soif===597###eoif===658###ins===true###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");return addperm_key_val(o, "not", addauth(NULL, auth->first));

			case afb_auth_Yes: if(auth->type == afb_auth_Yes)AKA_mark("lis===171###sois===5096###eois===5114###lif===10###soif===660###eoif===678###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");
 		AKA_mark("lis===171###sois===5115###eois===5161###lif===10###soif===679###eoif===725###ins===true###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");return addperm(o, json_object_new_boolean(1));

	}

		AKA_mark("lis===173###sois===5166###eois===5175###lif===12###soif===730###eoif===739###ins===true###function===./app-framework-binder/src/afb-auth.c/addauth(struct json_object*,const struct afb_auth*)");return o;

}

/** Instrumented function addauth_or_array(struct json_object*,const struct afb_auth*) */
static struct json_object *addauth_or_array(struct json_object *o, const struct afb_auth *auth)
{AKA_mark("Calling: ./app-framework-binder/src/afb-auth.c/addauth_or_array(struct json_object*,const struct afb_auth*)");AKA_fCall++;
		if (AKA_mark("lis===178###sois===5282###eois===5307###lif===2###soif===103###eoif===128###ifc===true###function===./app-framework-binder/src/afb-auth.c/addauth_or_array(struct json_object*,const struct afb_auth*)") && (AKA_mark("lis===178###sois===5282###eois===5307###lif===2###soif===103###eoif===128###isc===true###function===./app-framework-binder/src/afb-auth.c/addauth_or_array(struct json_object*,const struct afb_auth*)")&&auth->type != afb_auth_Or)) {
		AKA_mark("lis===179###sois===5311###eois===5357###lif===3###soif===132###eoif===178###ins===true###function===./app-framework-binder/src/afb-auth.c/addauth_or_array(struct json_object*,const struct afb_auth*)");json_object_array_add(o, addauth(NULL, auth));
	}
	else {
				AKA_mark("lis===181###sois===5368###eois===5401###lif===5###soif===189###eoif===222###ins===true###function===./app-framework-binder/src/afb-auth.c/addauth_or_array(struct json_object*,const struct afb_auth*)");addauth_or_array(o, auth->first);

				AKA_mark("lis===182###sois===5404###eois===5436###lif===6###soif===225###eoif===257###ins===true###function===./app-framework-binder/src/afb-auth.c/addauth_or_array(struct json_object*,const struct afb_auth*)");addauth_or_array(o, auth->next);

	}


		AKA_mark("lis===185###sois===5442###eois===5451###lif===9###soif===263###eoif===272###ins===true###function===./app-framework-binder/src/afb-auth.c/addauth_or_array(struct json_object*,const struct afb_auth*)");return o;

}

/** Instrumented function afb_auth_json_x2(const struct afb_auth*,uint32_t) */
struct json_object *afb_auth_json_x2(const struct afb_auth *auth, uint32_t session)
{AKA_mark("Calling: ./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)");AKA_fCall++;
		AKA_mark("lis===190###sois===5542###eois===5576###lif===2###soif===87###eoif===121###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)");struct json_object *result = NULL;


		if (AKA_mark("lis===192###sois===5583###eois===5613###lif===4###soif===128###eoif===158###ifc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)") && (AKA_mark("lis===192###sois===5583###eois===5613###lif===4###soif===128###eoif===158###isc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)")&&session & AFB_SESSION_CLOSE_X2)) {
		AKA_mark("lis===193###sois===5617###eois===5673###lif===5###soif===162###eoif===218###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)");result = addperm_key_valstr(result, "session", "close");
	}
	else {AKA_mark("lis===-192-###sois===-5583-###eois===-558330-###lif===-4-###soif===-###eoif===-158-###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)");}


		if (AKA_mark("lis===195###sois===5680###eois===5710###lif===7###soif===225###eoif===255###ifc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)") && (AKA_mark("lis===195###sois===5680###eois===5710###lif===7###soif===225###eoif===255###isc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)")&&session & AFB_SESSION_CHECK_X2)) {
		AKA_mark("lis===196###sois===5714###eois===5770###lif===8###soif===259###eoif===315###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)");result = addperm_key_valstr(result, "session", "check");
	}
	else {AKA_mark("lis===-195-###sois===-5680-###eois===-568030-###lif===-7-###soif===-###eoif===-255-###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)");}


		if (AKA_mark("lis===198###sois===5777###eois===5809###lif===10###soif===322###eoif===354###ifc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)") && (AKA_mark("lis===198###sois===5777###eois===5809###lif===10###soif===322###eoif===354###isc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)")&&session & AFB_SESSION_REFRESH_X2)) {
		AKA_mark("lis===199###sois===5813###eois===5869###lif===11###soif===358###eoif===414###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)");result = addperm_key_valstr(result, "token", "refresh");
	}
	else {AKA_mark("lis===-198-###sois===-5777-###eois===-577732-###lif===-10-###soif===-###eoif===-354-###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)");}


		if (AKA_mark("lis===201###sois===5876###eois===5909###lif===13###soif===421###eoif===454###ifc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)") && (AKA_mark("lis===201###sois===5876###eois===5909###lif===13###soif===421###eoif===454###isc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)")&&session & AFB_SESSION_LOA_MASK_X2)) {
		AKA_mark("lis===202###sois===5913###eois===5991###lif===14###soif===458###eoif===536###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)");result = addperm_key_valint(result, "LOA", session & AFB_SESSION_LOA_MASK_X2);
	}
	else {AKA_mark("lis===-201-###sois===-5876-###eois===-587633-###lif===-13-###soif===-###eoif===-454-###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)");}


		if (AKA_mark("lis===204###sois===5998###eois===6002###lif===16###soif===543###eoif===547###ifc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)") && (AKA_mark("lis===204###sois===5998###eois===6002###lif===16###soif===543###eoif===547###isc===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)")&&auth)) {
		AKA_mark("lis===205###sois===6006###eois===6037###lif===17###soif===551###eoif===582###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)");result = addauth(result, auth);
	}
	else {AKA_mark("lis===-204-###sois===-5998-###eois===-59984-###lif===-16-###soif===-###eoif===-547-###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)");}


		AKA_mark("lis===207###sois===6040###eois===6054###lif===19###soif===585###eoif===599###ins===true###function===./app-framework-binder/src/afb-auth.c/afb_auth_json_x2(const struct afb_auth*,uint32_t)");return result;

}

 
#if WITH_LEGACY_BINDING_V1
struct json_object *afb_auth_json_x1(int session)
{
	struct json_object *result = NULL;

	if (session & AFB_SESSION_CLOSE_X1)
		result = addperm_key_valstr(result, "session", "close");
	if (session & AFB_SESSION_CHECK_X1)
		result = addperm_key_valstr(result, "session", "check");
	if (session & AFB_SESSION_RENEW_X1)
		result = addperm_key_valstr(result, "token", "refresh");
	if (session & AFB_SESSION_LOA_MASK_X1)
		result = addperm_key_valint(result, "LOA", (session >> AFB_SESSION_LOA_SHIFT_X1) & AFB_SESSION_LOA_MASK_X1);

	return result;
}
#endif

#endif

