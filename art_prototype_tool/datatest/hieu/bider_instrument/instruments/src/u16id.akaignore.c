/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_U16ID_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_U16ID_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>
#include <malloc.h>
#include <errno.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__U16ID_H_
#define AKA_INCLUDE__U16ID_H_
#include "u16id.akaignore.h"
#endif


/* compute P, the count of bits of pointers */
#if UINTPTR_MAX == (18446744073709551615UL)
#  define P 64
#elif UINTPTR_MAX == (4294967295U)
#  define P 32
#elif UINTPTR_MAX == (65535U)
#  define P 16
#else
#  error "Unsupported pointer size"
#endif

/* granule of allocation */
#define N 4

/*
 * The u16id maps are made of a single block of memory structured
 * as an array of uint16_t followed by an array of void*. To ensure
 * that void* pointers are correctly aligned, the array of uint16_t
 * at head is a multiple of N items, with N being a multiple of 2
 * if void* is 32 bits or 4 if void* is 64 bits.
 * 
 * The first item of the array of uint16_t is used to record the
 * upper index of valid uint16_t ids.
 * 
 * +-----+-----+-----+-----+ - - - - - - - - +-----+-----+-----+-----+ - - - - - - - - 
 * |upper| id1 | id2 | id3 |                 |         ptr1          |
 * +-----+-----+-----+-----+ - - - - - - - - +-----+-----+-----+-----+ - - - - - - - - 
 */

/** Instrumented function get_capacity(uint16_t) */
static inline uint16_t get_capacity(uint16_t upper)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/get_capacity(uint16_t)");AKA_fCall++;
	/* capacity is the smallest kN-1 such that kN-1 >= upper) */
#if N == 2 || N == 4 || N == 8 || N == 16
		AKA_mark("lis===57###sois===1857###eois===1880###lif===4###soif===159###eoif===182###ins===true###function===./app-framework-binder/src/u16id.c/get_capacity(uint16_t)");return upper | (N - 1);

#else
#	error "not supported"
#endif
}

typedef struct {
	uint16_t upper;
	uint16_t capacity;
	uint16_t *ids;
	void **ptrs;
} flat_t;

/** Instrumented function flatofup(flat_t*,void*,uint16_t) */
static void flatofup(flat_t *flat, void *base, uint16_t up)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/flatofup(flat_t*,void*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===72###sois===2079###eois===2098###lif===2###soif===63###eoif===82###ins===true###function===./app-framework-binder/src/u16id.c/flatofup(flat_t*,void*,uint16_t)");uint16_t cap, *ids;

	
		AKA_mark("lis===74###sois===2102###eois===2119###lif===4###soif===86###eoif===103###ins===true###function===./app-framework-binder/src/u16id.c/flatofup(flat_t*,void*,uint16_t)");flat->upper = up;

		AKA_mark("lis===75###sois===2121###eois===2161###lif===5###soif===105###eoif===145###ins===true###function===./app-framework-binder/src/u16id.c/flatofup(flat_t*,void*,uint16_t)");flat->capacity = cap = get_capacity(up);

		AKA_mark("lis===76###sois===2163###eois===2186###lif===6###soif===147###eoif===170###ins===true###function===./app-framework-binder/src/u16id.c/flatofup(flat_t*,void*,uint16_t)");flat->ids = ids = base;

		AKA_mark("lis===77###sois===2188###eois===2231###lif===7###soif===172###eoif===215###ins===true###function===./app-framework-binder/src/u16id.c/flatofup(flat_t*,void*,uint16_t)");flat->ptrs = ((void**)(&ids[cap + 1])) - 1;

}

/** Instrumented function flatof(flat_t*,void*) */
static void flatof(flat_t *flat, void *base)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/flatof(flat_t*,void*)");AKA_fCall++;
		if (AKA_mark("lis===82###sois===2287###eois===2291###lif===2###soif===52###eoif===56###ifc===true###function===./app-framework-binder/src/u16id.c/flatof(flat_t*,void*)") && (AKA_mark("lis===82###sois===2287###eois===2291###lif===2###soif===52###eoif===56###isc===true###function===./app-framework-binder/src/u16id.c/flatof(flat_t*,void*)")&&base)) {
		AKA_mark("lis===83###sois===2295###eois===2334###lif===3###soif===60###eoif===99###ins===true###function===./app-framework-binder/src/u16id.c/flatof(flat_t*,void*)");flatofup(flat, base, *(uint16_t*)base);
	}
	else {
				AKA_mark("lis===85###sois===2345###eois===2378###lif===5###soif===110###eoif===143###ins===true###function===./app-framework-binder/src/u16id.c/flatof(flat_t*,void*)");flat->upper = flat->capacity = 0;

				AKA_mark("lis===86###sois===2381###eois===2398###lif===6###soif===146###eoif===163###ins===true###function===./app-framework-binder/src/u16id.c/flatof(flat_t*,void*)");flat->ids = NULL;

				AKA_mark("lis===87###sois===2401###eois===2419###lif===7###soif===166###eoif===184###ins===true###function===./app-framework-binder/src/u16id.c/flatof(flat_t*,void*)");flat->ptrs = NULL;

	}

}

/** Instrumented function size(uint16_t) */
static inline size_t size(uint16_t capacity)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/size(uint16_t)");AKA_fCall++;
		AKA_mark("lis===93###sois===2474###eois===2544###lif===2###soif===48###eoif===118###ins===true###function===./app-framework-binder/src/u16id.c/size(uint16_t)");return sizeof(uint16_t) * (capacity + 1)
		+ sizeof(void*) * capacity;

}

/** Instrumented function search(flat_t*,uint16_t) */
static inline uint16_t search(flat_t *flat, uint16_t id)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/search(flat_t*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===99###sois===2608###eois===2634###lif===2###soif===60###eoif===86###ins===true###function===./app-framework-binder/src/u16id.c/search(flat_t*,uint16_t)");uint16_t *ids = flat->ids;

		AKA_mark("lis===100###sois===2636###eois===2661###lif===3###soif===88###eoif===113###ins===true###function===./app-framework-binder/src/u16id.c/search(flat_t*,uint16_t)");uint16_t r = flat->upper;

		while (AKA_mark("lis===101###sois===2669###eois===2686###lif===4###soif===121###eoif===138###ifc===true###function===./app-framework-binder/src/u16id.c/search(flat_t*,uint16_t)") && ((AKA_mark("lis===101###sois===2669###eois===2670###lif===4###soif===121###eoif===122###isc===true###function===./app-framework-binder/src/u16id.c/search(flat_t*,uint16_t)")&&r)	&&(AKA_mark("lis===101###sois===2674###eois===2686###lif===4###soif===126###eoif===138###isc===true###function===./app-framework-binder/src/u16id.c/search(flat_t*,uint16_t)")&&ids[r] != id))) {
		AKA_mark("lis===102###sois===2690###eois===2694###lif===5###soif===142###eoif===146###ins===true###function===./app-framework-binder/src/u16id.c/search(flat_t*,uint16_t)");r--;
	}

		AKA_mark("lis===103###sois===2696###eois===2705###lif===6###soif===148###eoif===157###ins===true###function===./app-framework-binder/src/u16id.c/search(flat_t*,uint16_t)");return r;

}

/** Instrumented function add(flat_t*,uint16_t,void*) */
static void *add(flat_t *flat, uint16_t id, void *ptr)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");AKA_fCall++;
		AKA_mark("lis===108###sois===2767###eois===2788###lif===2###soif===58###eoif===79###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");void *grown, *result;

		AKA_mark("lis===109###sois===2790###eois===2803###lif===3###soif===81###eoif===94###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");flat_t oflat;

		AKA_mark("lis===110###sois===2805###eois===2829###lif===4###soif===96###eoif===120###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");uint16_t nupper, oupper;


		AKA_mark("lis===112###sois===2832###eois===2853###lif===6###soif===123###eoif===144###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");oupper = flat->upper;

		AKA_mark("lis===113###sois===2855###eois===2887###lif===7###soif===146###eoif===178###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");nupper = (uint16_t)(oupper + 1);

		AKA_mark("lis===114###sois===2889###eois===2908###lif===8###soif===180###eoif===199###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");result = flat->ids;

		if (AKA_mark("lis===115###sois===2914###eois===2937###lif===9###soif===205###eoif===228###ifc===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)") && (AKA_mark("lis===115###sois===2914###eois===2937###lif===9###soif===205###eoif===228###isc===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)")&&nupper > flat->capacity)) {
				AKA_mark("lis===116###sois===2943###eois===2995###lif===10###soif===234###eoif===286###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");grown = realloc(result, size(get_capacity(nupper)));

				if (AKA_mark("lis===117###sois===3002###eois===3015###lif===11###soif===293###eoif===306###ifc===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)") && (AKA_mark("lis===117###sois===3002###eois===3015###lif===11###soif===293###eoif===306###isc===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)")&&grown == NULL)) {
			AKA_mark("lis===118###sois===3020###eois===3032###lif===12###soif===311###eoif===323###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");return NULL;
		}
		else {AKA_mark("lis===-117-###sois===-3002-###eois===-300213-###lif===-11-###soif===-###eoif===-306-###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");}

				AKA_mark("lis===119###sois===3035###eois===3050###lif===13###soif===326###eoif===341###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");result = grown;

				AKA_mark("lis===120###sois===3053###eois===3083###lif===14###soif===344###eoif===374###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");flatofup(flat, grown, nupper);

				if (AKA_mark("lis===121###sois===3090###eois===3096###lif===15###soif===381###eoif===387###ifc===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)") && (AKA_mark("lis===121###sois===3090###eois===3096###lif===15###soif===381###eoif===387###isc===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)")&&oupper)) {
						AKA_mark("lis===122###sois===3103###eois===3135###lif===16###soif===394###eoif===426###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");flatofup(&oflat, grown, oupper);

						while (AKA_mark("lis===123###sois===3146###eois===3152###lif===17###soif===437###eoif===443###ifc===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)") && (AKA_mark("lis===123###sois===3146###eois===3152###lif===17###soif===437###eoif===443###isc===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)")&&oupper)) {
								AKA_mark("lis===124###sois===3160###eois===3200###lif===18###soif===451###eoif===491###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");flat->ptrs[oupper] = oflat.ptrs[oupper];

								AKA_mark("lis===125###sois===3205###eois===3214###lif===19###soif===496###eoif===505###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");oupper--;

			}

		}
		else {AKA_mark("lis===-121-###sois===-3090-###eois===-30906-###lif===-15-###soif===-###eoif===-387-###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");}

	}
	else {AKA_mark("lis===-115-###sois===-2914-###eois===-291423-###lif===-9-###soif===-###eoif===-228-###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");}

	/* flat->upper = nupper; NOT DONE BECAUSE NOT NEEDED */
		AKA_mark("lis===130###sois===3285###eois===3307###lif===24###soif===576###eoif===598###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");flat->ids[0] = nupper;

		AKA_mark("lis===131###sois===3309###eois===3332###lif===25###soif===600###eoif===623###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");flat->ids[nupper] = id;

		AKA_mark("lis===132###sois===3334###eois===3359###lif===26###soif===625###eoif===650###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");flat->ptrs[nupper] = ptr;

		AKA_mark("lis===133###sois===3361###eois===3375###lif===27###soif===652###eoif===666###ins===true###function===./app-framework-binder/src/u16id.c/add(flat_t*,uint16_t,void*)");return result;

}

/** Instrumented function drop(flat_t*,uint16_t) */
static void *drop(flat_t *flat, uint16_t index)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===138###sois===3430###eois===3451###lif===2###soif===51###eoif===72###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");void **ptrs, *result;

		AKA_mark("lis===139###sois===3453###eois===3479###lif===3###soif===74###eoif===100###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");uint16_t upper, idx, capa;


		AKA_mark("lis===141###sois===3482###eois===3502###lif===5###soif===103###eoif===123###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");upper = flat->upper;

		if (AKA_mark("lis===142###sois===3508###eois===3522###lif===6###soif===129###eoif===143###ifc===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)") && (AKA_mark("lis===142###sois===3508###eois===3522###lif===6###soif===129###eoif===143###isc===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)")&&index != upper)) {
				AKA_mark("lis===143###sois===3528###eois===3564###lif===7###soif===149###eoif===185###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");flat->ids[index] = flat->ids[upper];

				AKA_mark("lis===144###sois===3567###eois===3605###lif===8###soif===188###eoif===226###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");flat->ptrs[index] = flat->ptrs[upper];

	}
	else {AKA_mark("lis===-142-###sois===-3508-###eois===-350814-###lif===-6-###soif===-###eoif===-143-###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");}

		AKA_mark("lis===146###sois===3610###eois===3633###lif===10###soif===231###eoif===254###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");flat->ids[0] = --upper;

		AKA_mark("lis===147###sois===3635###eois===3662###lif===11###soif===256###eoif===283###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");capa = get_capacity(upper);

		AKA_mark("lis===148###sois===3664###eois===3683###lif===12###soif===285###eoif===304###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");result = flat->ids;

		if (AKA_mark("lis===149###sois===3689###eois===3711###lif===13###soif===310###eoif===332###ifc===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)") && (AKA_mark("lis===149###sois===3689###eois===3711###lif===13###soif===310###eoif===332###isc===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)")&&capa != flat->capacity)) {
				AKA_mark("lis===150###sois===3717###eois===3735###lif===14###soif===338###eoif===356###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");ptrs = flat->ptrs;

				AKA_mark("lis===151###sois===3738###eois===3768###lif===15###soif===359###eoif===389###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");flatofup(flat, result, upper);

				AKA_mark("lis===152###sois===3771###eois===3779###lif===16###soif===392###eoif===400###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");idx = 1;

				while (AKA_mark("lis===153###sois===3788###eois===3800###lif===17###soif===409###eoif===421###ifc===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)") && (AKA_mark("lis===153###sois===3788###eois===3800###lif===17###soif===409###eoif===421###isc===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)")&&idx <= upper)) {
						AKA_mark("lis===154###sois===3807###eois===3835###lif===18###soif===428###eoif===456###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");flat->ptrs[idx] = ptrs[idx];

						AKA_mark("lis===155###sois===3839###eois===3845###lif===19###soif===460###eoif===466###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");idx++;

		}

#if U16ID_ALWAYS_SHRINK
		result = realloc(flat->ids, size(capa));
		if (result == NULL)
			result = flat->ids;
#endif
	}
	else {AKA_mark("lis===-149-###sois===-3689-###eois===-368922-###lif===-13-###soif===-###eoif===-332-###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");}

		AKA_mark("lis===163###sois===3973###eois===3987###lif===27###soif===594###eoif===608###ins===true###function===./app-framework-binder/src/u16id.c/drop(flat_t*,uint16_t)");return result;

}

/** Instrumented function dropall(void**) */
static void dropall(void **pbase)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/dropall(void**)");AKA_fCall++;
		AKA_mark("lis===168###sois===4028###eois===4039###lif===2###soif===37###eoif===48###ins===true###function===./app-framework-binder/src/u16id.c/dropall(void**)");void *base;


		AKA_mark("lis===170###sois===4042###eois===4056###lif===4###soif===51###eoif===65###ins===true###function===./app-framework-binder/src/u16id.c/dropall(void**)");base = *pbase;

		if (AKA_mark("lis===171###sois===4062###eois===4066###lif===5###soif===71###eoif===75###ifc===true###function===./app-framework-binder/src/u16id.c/dropall(void**)") && (AKA_mark("lis===171###sois===4062###eois===4066###lif===5###soif===71###eoif===75###isc===true###function===./app-framework-binder/src/u16id.c/dropall(void**)")&&base)) {
		AKA_mark("lis===172###sois===4070###eois===4091###lif===6###soif===79###eoif===100###ins===true###function===./app-framework-binder/src/u16id.c/dropall(void**)");*(uint16_t*)base = 0;
	}
	else {AKA_mark("lis===-171-###sois===-4062-###eois===-40624-###lif===-5-###soif===-###eoif===-75-###ins===true###function===./app-framework-binder/src/u16id.c/dropall(void**)");}

}

/** Instrumented function destroy(void**) */
static void destroy(void **pbase)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/destroy(void**)");AKA_fCall++;
		AKA_mark("lis===177###sois===4132###eois===4143###lif===2###soif===37###eoif===48###ins===true###function===./app-framework-binder/src/u16id.c/destroy(void**)");void *base;


		AKA_mark("lis===179###sois===4146###eois===4160###lif===4###soif===51###eoif===65###ins===true###function===./app-framework-binder/src/u16id.c/destroy(void**)");base = *pbase;

		AKA_mark("lis===180###sois===4162###eois===4176###lif===5###soif===67###eoif===81###ins===true###function===./app-framework-binder/src/u16id.c/destroy(void**)");*pbase = NULL;

		AKA_mark("lis===181###sois===4178###eois===4189###lif===6###soif===83###eoif===94###ins===true###function===./app-framework-binder/src/u16id.c/destroy(void**)");free(base);

}

/** Instrumented function create(void**) */
static int create(void **pbase)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/create(void**)");AKA_fCall++;
		AKA_mark("lis===186###sois===4228###eois===4239###lif===2###soif===35###eoif===46###ins===true###function===./app-framework-binder/src/u16id.c/create(void**)");void *base;


		AKA_mark("lis===188###sois===4242###eois===4288###lif===4###soif===49###eoif===95###ins===true###function===./app-framework-binder/src/u16id.c/create(void**)");*pbase = base = malloc(size(get_capacity(0)));

		if (AKA_mark("lis===189###sois===4294###eois===4306###lif===5###soif===101###eoif===113###ifc===true###function===./app-framework-binder/src/u16id.c/create(void**)") && (AKA_mark("lis===189###sois===4294###eois===4306###lif===5###soif===101###eoif===113###isc===true###function===./app-framework-binder/src/u16id.c/create(void**)")&&base == NULL)) {
		AKA_mark("lis===190###sois===4310###eois===4320###lif===6###soif===117###eoif===127###ins===true###function===./app-framework-binder/src/u16id.c/create(void**)");return -1;
	}
	else {AKA_mark("lis===-189-###sois===-4294-###eois===-429412-###lif===-5-###soif===-###eoif===-113-###ins===true###function===./app-framework-binder/src/u16id.c/create(void**)");}

		AKA_mark("lis===191###sois===4322###eois===4343###lif===7###soif===129###eoif===150###ins===true###function===./app-framework-binder/src/u16id.c/create(void**)");*(uint16_t*)base = 0;

		AKA_mark("lis===192###sois===4345###eois===4354###lif===8###soif===152###eoif===161###ins===true###function===./app-framework-binder/src/u16id.c/create(void**)");return 0;

}

/**********************************************************************/
/**        u16id2ptr                                                 **/
/**********************************************************************/

/** Instrumented function u16id2ptr_create(struct u16id2ptr**) */
int u16id2ptr_create(struct u16id2ptr **pi2p)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2ptr_create(struct u16id2ptr**)");AKA_fCall++;
		AKA_mark("lis===201###sois===4627###eois===4655###lif===2###soif===49###eoif===77###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_create(struct u16id2ptr**)");return create((void**)pi2p);

}

/** Instrumented function u16id2ptr_destroy(struct u16id2ptr**) */
void u16id2ptr_destroy(struct u16id2ptr **pi2p)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2ptr_destroy(struct u16id2ptr**)");AKA_fCall++;
		AKA_mark("lis===206###sois===4710###eois===4732###lif===2###soif===51###eoif===73###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_destroy(struct u16id2ptr**)");destroy((void**)pi2p);

}

/** Instrumented function u16id2ptr_dropall(struct u16id2ptr**) */
void u16id2ptr_dropall(struct u16id2ptr **pi2p)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2ptr_dropall(struct u16id2ptr**)");AKA_fCall++;
		AKA_mark("lis===211###sois===4787###eois===4809###lif===2###soif===51###eoif===73###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_dropall(struct u16id2ptr**)");dropall((void**)pi2p);

}

/** Instrumented function u16id2ptr_has(struct u16id2ptr*,uint16_t) */
int u16id2ptr_has(struct u16id2ptr *i2p, uint16_t id)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2ptr_has(struct u16id2ptr*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===216###sois===4870###eois===4882###lif===2###soif===57###eoif===69###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_has(struct u16id2ptr*,uint16_t)");flat_t flat;


		AKA_mark("lis===218###sois===4885###eois===4904###lif===4###soif===72###eoif===91###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_has(struct u16id2ptr*,uint16_t)");flatof(&flat, i2p);

		AKA_mark("lis===219###sois===4906###eois===4936###lif===5###soif===93###eoif===123###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_has(struct u16id2ptr*,uint16_t)");return search(&flat, id) != 0;

}

/** Instrumented function u16id2ptr_add(struct u16id2ptr**,uint16_t,void*) */
int u16id2ptr_add(struct u16id2ptr **pi2p, uint16_t id, void *ptr)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");AKA_fCall++;
		AKA_mark("lis===224###sois===5010###eois===5032###lif===2###soif===70###eoif===92###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");struct u16id2ptr *i2p;

		AKA_mark("lis===225###sois===5034###eois===5049###lif===3###soif===94###eoif===109###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");uint16_t index;

		AKA_mark("lis===226###sois===5051###eois===5063###lif===4###soif===111###eoif===123###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");flat_t flat;


		AKA_mark("lis===228###sois===5066###eois===5078###lif===6###soif===126###eoif===138###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");i2p = *pi2p;

		AKA_mark("lis===229###sois===5080###eois===5099###lif===7###soif===140###eoif===159###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");flatof(&flat, i2p);

		AKA_mark("lis===230###sois===5101###eois===5127###lif===8###soif===161###eoif===187###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");index = search(&flat, id);

		if (AKA_mark("lis===231###sois===5133###eois===5138###lif===9###soif===193###eoif===198###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)") && (AKA_mark("lis===231###sois===5133###eois===5138###lif===9###soif===193###eoif===198###isc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)")&&index)) {
				AKA_mark("lis===232###sois===5144###eois===5159###lif===10###soif===204###eoif===219###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");errno = EEXIST;

				AKA_mark("lis===233###sois===5162###eois===5172###lif===11###soif===222###eoif===232###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");return -1;

	}
	else {AKA_mark("lis===-231-###sois===-5133-###eois===-51335-###lif===-9-###soif===-###eoif===-198-###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");}

		AKA_mark("lis===235###sois===5177###eois===5203###lif===13###soif===237###eoif===263###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");i2p = add(&flat, id, ptr);

		if (AKA_mark("lis===236###sois===5209###eois===5213###lif===14###soif===269###eoif===273###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)") && (AKA_mark("lis===236###sois===5209###eois===5213###lif===14###soif===269###eoif===273###isc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)")&&!i2p)) {
		AKA_mark("lis===237###sois===5217###eois===5227###lif===15###soif===277###eoif===287###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");return -1;
	}
	else {AKA_mark("lis===-236-###sois===-5209-###eois===-52094-###lif===-14-###soif===-###eoif===-273-###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");}

		AKA_mark("lis===238###sois===5229###eois===5241###lif===16###soif===289###eoif===301###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");*pi2p = i2p;

		AKA_mark("lis===239###sois===5243###eois===5252###lif===17###soif===303###eoif===312###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_add(struct u16id2ptr**,uint16_t,void*)");return 0;

}

/** Instrumented function u16id2ptr_set(struct u16id2ptr**,uint16_t,void*) */
int u16id2ptr_set(struct u16id2ptr **pi2p, uint16_t id, void *ptr)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)");AKA_fCall++;
		AKA_mark("lis===244###sois===5326###eois===5348###lif===2###soif===70###eoif===92###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)");struct u16id2ptr *i2p;

		AKA_mark("lis===245###sois===5350###eois===5365###lif===3###soif===94###eoif===109###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)");uint16_t index;

		AKA_mark("lis===246###sois===5367###eois===5379###lif===4###soif===111###eoif===123###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)");flat_t flat;


		AKA_mark("lis===248###sois===5382###eois===5394###lif===6###soif===126###eoif===138###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)");i2p = *pi2p;

		AKA_mark("lis===249###sois===5396###eois===5415###lif===7###soif===140###eoif===159###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)");flatof(&flat, i2p);

		AKA_mark("lis===250###sois===5417###eois===5443###lif===8###soif===161###eoif===187###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)");index = search(&flat, id);

		if (AKA_mark("lis===251###sois===5449###eois===5454###lif===9###soif===193###eoif===198###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)") && (AKA_mark("lis===251###sois===5449###eois===5454###lif===9###soif===193###eoif===198###isc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)")&&index)) {
		AKA_mark("lis===252###sois===5458###eois===5481###lif===10###soif===202###eoif===225###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)");flat.ptrs[index] = ptr;
	}
	else {
				AKA_mark("lis===254###sois===5492###eois===5518###lif===12###soif===236###eoif===262###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)");i2p = add(&flat, id, ptr);

				if (AKA_mark("lis===255###sois===5525###eois===5529###lif===13###soif===269###eoif===273###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)") && (AKA_mark("lis===255###sois===5525###eois===5529###lif===13###soif===269###eoif===273###isc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)")&&!i2p)) {
			AKA_mark("lis===256###sois===5534###eois===5544###lif===14###soif===278###eoif===288###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)");return -1;
		}
		else {AKA_mark("lis===-255-###sois===-5525-###eois===-55254-###lif===-13-###soif===-###eoif===-273-###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)");}

				AKA_mark("lis===257###sois===5547###eois===5559###lif===15###soif===291###eoif===303###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)");*pi2p = i2p;

	}

		AKA_mark("lis===259###sois===5564###eois===5573###lif===17###soif===308###eoif===317###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_set(struct u16id2ptr**,uint16_t,void*)");return 0;

}

/** Instrumented function u16id2ptr_put(struct u16id2ptr*,uint16_t,void*) */
int u16id2ptr_put(struct u16id2ptr *i2p, uint16_t id, void *ptr)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2ptr_put(struct u16id2ptr*,uint16_t,void*)");AKA_fCall++;
		AKA_mark("lis===264###sois===5645###eois===5660###lif===2###soif===68###eoif===83###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_put(struct u16id2ptr*,uint16_t,void*)");uint16_t index;

		AKA_mark("lis===265###sois===5662###eois===5674###lif===3###soif===85###eoif===97###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_put(struct u16id2ptr*,uint16_t,void*)");flat_t flat;


		AKA_mark("lis===267###sois===5677###eois===5696###lif===5###soif===100###eoif===119###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_put(struct u16id2ptr*,uint16_t,void*)");flatof(&flat, i2p);

		AKA_mark("lis===268###sois===5698###eois===5724###lif===6###soif===121###eoif===147###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_put(struct u16id2ptr*,uint16_t,void*)");index = search(&flat, id);

		if (AKA_mark("lis===269###sois===5730###eois===5735###lif===7###soif===153###eoif===158###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_put(struct u16id2ptr*,uint16_t,void*)") && (AKA_mark("lis===269###sois===5730###eois===5735###lif===7###soif===153###eoif===158###isc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_put(struct u16id2ptr*,uint16_t,void*)")&&index)) {
				AKA_mark("lis===270###sois===5741###eois===5764###lif===8###soif===164###eoif===187###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_put(struct u16id2ptr*,uint16_t,void*)");flat.ptrs[index] = ptr;

				AKA_mark("lis===271###sois===5767###eois===5776###lif===9###soif===190###eoif===199###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_put(struct u16id2ptr*,uint16_t,void*)");return 0;

	}
	else {AKA_mark("lis===-269-###sois===-5730-###eois===-57305-###lif===-7-###soif===-###eoif===-158-###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_put(struct u16id2ptr*,uint16_t,void*)");}

		AKA_mark("lis===273###sois===5781###eois===5796###lif===11###soif===204###eoif===219###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_put(struct u16id2ptr*,uint16_t,void*)");errno = ENOENT;

		AKA_mark("lis===274###sois===5798###eois===5808###lif===12###soif===221###eoif===231###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_put(struct u16id2ptr*,uint16_t,void*)");return -1;

}

/** Instrumented function u16id2ptr_get(struct u16id2ptr*,uint16_t,void**) */
int u16id2ptr_get(struct u16id2ptr *i2p, uint16_t id, void **pptr)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2ptr_get(struct u16id2ptr*,uint16_t,void**)");AKA_fCall++;
		AKA_mark("lis===279###sois===5882###eois===5897###lif===2###soif===70###eoif===85###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_get(struct u16id2ptr*,uint16_t,void**)");uint16_t index;

		AKA_mark("lis===280###sois===5899###eois===5911###lif===3###soif===87###eoif===99###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_get(struct u16id2ptr*,uint16_t,void**)");flat_t flat;


		AKA_mark("lis===282###sois===5914###eois===5933###lif===5###soif===102###eoif===121###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_get(struct u16id2ptr*,uint16_t,void**)");flatof(&flat, i2p);

		AKA_mark("lis===283###sois===5935###eois===5961###lif===6###soif===123###eoif===149###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_get(struct u16id2ptr*,uint16_t,void**)");index = search(&flat, id);

		if (AKA_mark("lis===284###sois===5967###eois===5972###lif===7###soif===155###eoif===160###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_get(struct u16id2ptr*,uint16_t,void**)") && (AKA_mark("lis===284###sois===5967###eois===5972###lif===7###soif===155###eoif===160###isc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_get(struct u16id2ptr*,uint16_t,void**)")&&index)) {
				AKA_mark("lis===285###sois===5978###eois===6003###lif===8###soif===166###eoif===191###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_get(struct u16id2ptr*,uint16_t,void**)");*pptr = flat.ptrs[index];

				AKA_mark("lis===286###sois===6006###eois===6015###lif===9###soif===194###eoif===203###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_get(struct u16id2ptr*,uint16_t,void**)");return 0;

	}
	else {AKA_mark("lis===-284-###sois===-5967-###eois===-59675-###lif===-7-###soif===-###eoif===-160-###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_get(struct u16id2ptr*,uint16_t,void**)");}

		AKA_mark("lis===288###sois===6020###eois===6035###lif===11###soif===208###eoif===223###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_get(struct u16id2ptr*,uint16_t,void**)");errno = ENOENT;

		AKA_mark("lis===289###sois===6037###eois===6047###lif===12###soif===225###eoif===235###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_get(struct u16id2ptr*,uint16_t,void**)");return -1;

}

/** Instrumented function u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**) */
int u16id2ptr_drop(struct u16id2ptr **pi2p, uint16_t id, void **pptr)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");AKA_fCall++;
		AKA_mark("lis===294###sois===6124###eois===6146###lif===2###soif===73###eoif===95###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");struct u16id2ptr *i2p;

		AKA_mark("lis===295###sois===6148###eois===6163###lif===3###soif===97###eoif===112###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");uint16_t index;

		AKA_mark("lis===296###sois===6165###eois===6177###lif===4###soif===114###eoif===126###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");flat_t flat;


		AKA_mark("lis===298###sois===6180###eois===6192###lif===6###soif===129###eoif===141###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");i2p = *pi2p;

		AKA_mark("lis===299###sois===6194###eois===6213###lif===7###soif===143###eoif===162###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");flatof(&flat, i2p);

		AKA_mark("lis===300###sois===6215###eois===6241###lif===8###soif===164###eoif===190###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");index = search(&flat, id);

		if (AKA_mark("lis===301###sois===6247###eois===6253###lif===9###soif===196###eoif===202###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)") && (AKA_mark("lis===301###sois===6247###eois===6253###lif===9###soif===196###eoif===202###isc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)")&&!index)) {
				AKA_mark("lis===302###sois===6259###eois===6274###lif===10###soif===208###eoif===223###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");errno = ENOENT;

				AKA_mark("lis===303###sois===6277###eois===6287###lif===11###soif===226###eoif===236###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");return -1;

	}
	else {AKA_mark("lis===-301-###sois===-6247-###eois===-62476-###lif===-9-###soif===-###eoif===-202-###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");}

		if (AKA_mark("lis===305###sois===6296###eois===6300###lif===13###soif===245###eoif===249###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)") && (AKA_mark("lis===305###sois===6296###eois===6300###lif===13###soif===245###eoif===249###isc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)")&&pptr)) {
		AKA_mark("lis===306###sois===6304###eois===6329###lif===14###soif===253###eoif===278###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");*pptr = flat.ptrs[index];
	}
	else {AKA_mark("lis===-305-###sois===-6296-###eois===-62964-###lif===-13-###soif===-###eoif===-249-###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");}

		AKA_mark("lis===307###sois===6331###eois===6356###lif===15###soif===280###eoif===305###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");i2p = drop(&flat, index);

		if (AKA_mark("lis===308###sois===6362###eois===6366###lif===16###soif===311###eoif===315###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)") && (AKA_mark("lis===308###sois===6362###eois===6366###lif===16###soif===311###eoif===315###isc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)")&&!i2p)) {
		AKA_mark("lis===309###sois===6370###eois===6380###lif===17###soif===319###eoif===329###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");return -1;
	}
	else {AKA_mark("lis===-308-###sois===-6362-###eois===-63624-###lif===-16-###soif===-###eoif===-315-###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");}

		AKA_mark("lis===310###sois===6382###eois===6394###lif===18###soif===331###eoif===343###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");*pi2p = i2p;

		AKA_mark("lis===311###sois===6396###eois===6405###lif===19###soif===345###eoif===354###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_drop(struct u16id2ptr**,uint16_t,void**)");return 0;

}

/** Instrumented function u16id2ptr_count(struct u16id2ptr*) */
int u16id2ptr_count(struct u16id2ptr *i2p)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2ptr_count(struct u16id2ptr*)");AKA_fCall++;
		AKA_mark("lis===316###sois===6455###eois===6495###lif===2###soif===46###eoif===86###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_count(struct u16id2ptr*)");return i2p ? ((int)*(uint16_t*)i2p) : 0;

}

/** Instrumented function u16id2ptr_at(struct u16id2ptr*,int,uint16_t*,void**) */
int u16id2ptr_at(struct u16id2ptr *i2p, int index, uint16_t *pid, void **pptr)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2ptr_at(struct u16id2ptr*,int,uint16_t*,void**)");AKA_fCall++;
		AKA_mark("lis===321###sois===6581###eois===6593###lif===2###soif===82###eoif===94###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_at(struct u16id2ptr*,int,uint16_t*,void**)");flat_t flat;


		AKA_mark("lis===323###sois===6596###eois===6615###lif===4###soif===97###eoif===116###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_at(struct u16id2ptr*,int,uint16_t*,void**)");flatof(&flat, i2p);

		if (AKA_mark("lis===324###sois===6621###eois===6658###lif===5###soif===122###eoif===159###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_at(struct u16id2ptr*,int,uint16_t*,void**)") && ((AKA_mark("lis===324###sois===6621###eois===6631###lif===5###soif===122###eoif===132###isc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_at(struct u16id2ptr*,int,uint16_t*,void**)")&&index >= 0)	&&(AKA_mark("lis===324###sois===6635###eois===6658###lif===5###soif===136###eoif===159###isc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_at(struct u16id2ptr*,int,uint16_t*,void**)")&&index < (int)flat.upper))) {
				AKA_mark("lis===325###sois===6664###eois===6691###lif===6###soif===165###eoif===192###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_at(struct u16id2ptr*,int,uint16_t*,void**)");*pid = flat.ids[index + 1];

				AKA_mark("lis===326###sois===6694###eois===6723###lif===7###soif===195###eoif===224###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_at(struct u16id2ptr*,int,uint16_t*,void**)");*pptr = flat.ptrs[index + 1];

				AKA_mark("lis===327###sois===6726###eois===6735###lif===8###soif===227###eoif===236###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_at(struct u16id2ptr*,int,uint16_t*,void**)");return 0;

	}
	else {AKA_mark("lis===-324-###sois===-6621-###eois===-662137-###lif===-5-###soif===-###eoif===-159-###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_at(struct u16id2ptr*,int,uint16_t*,void**)");}

		AKA_mark("lis===329###sois===6740###eois===6755###lif===10###soif===241###eoif===256###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_at(struct u16id2ptr*,int,uint16_t*,void**)");errno = EINVAL;

		AKA_mark("lis===330###sois===6757###eois===6767###lif===11###soif===258###eoif===268###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_at(struct u16id2ptr*,int,uint16_t*,void**)");return -1;

}

/** Instrumented function u16id2ptr_forall(struct u16id2ptr*,void(*callback)(void*closure, uint16_t id, void*ptr),void*) */
void u16id2ptr_forall(struct u16id2ptr *i2p, void (*callback)(void*closure, uint16_t id, void *ptr), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2ptr_forall(struct u16id2ptr*,void(*callback)(void*closure, uint16_t id, void*ptr),void*)");AKA_fCall++;
		AKA_mark("lis===335###sois===6890###eois===6902###lif===2###soif===119###eoif===131###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_forall(struct u16id2ptr*,void(*callback)(void*closure, uint16_t id, void*ptr),void*)");flat_t flat;


		AKA_mark("lis===337###sois===6905###eois===6924###lif===4###soif===134###eoif===153###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_forall(struct u16id2ptr*,void(*callback)(void*closure, uint16_t id, void*ptr),void*)");flatof(&flat, i2p);

		while (AKA_mark("lis===338###sois===6933###eois===6943###lif===5###soif===162###eoif===172###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_forall(struct u16id2ptr*,void(*callback)(void*closure, uint16_t id, void*ptr),void*)") && (AKA_mark("lis===338###sois===6933###eois===6943###lif===5###soif===162###eoif===172###isc===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_forall(struct u16id2ptr*,void(*callback)(void*closure, uint16_t id, void*ptr),void*)")&&flat.upper)) {
				AKA_mark("lis===339###sois===6949###eois===7012###lif===6###soif===178###eoif===241###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_forall(struct u16id2ptr*,void(*callback)(void*closure, uint16_t id, void*ptr),void*)");callback(closure, flat.ids[flat.upper], flat.ptrs[flat.upper]);

				AKA_mark("lis===340###sois===7015###eois===7028###lif===7###soif===244###eoif===257###ins===true###function===./app-framework-binder/src/u16id.c/u16id2ptr_forall(struct u16id2ptr*,void(*callback)(void*closure, uint16_t id, void*ptr),void*)");flat.upper--;

	}

}

/**********************************************************************/
/**        u16id2bool                                                **/
/**********************************************************************/

/** Instrumented function u16id2bool_create(struct u16id2bool**) */
int u16id2bool_create(struct u16id2bool **pi2b)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2bool_create(struct u16id2bool**)");AKA_fCall++;
		AKA_mark("lis===350###sois===7306###eois===7334###lif===2###soif===51###eoif===79###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_create(struct u16id2bool**)");return create((void**)pi2b);

}

/** Instrumented function u16id2bool_destroy(struct u16id2bool**) */
void u16id2bool_destroy(struct u16id2bool **pi2b)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2bool_destroy(struct u16id2bool**)");AKA_fCall++;
		AKA_mark("lis===355###sois===7391###eois===7413###lif===2###soif===53###eoif===75###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_destroy(struct u16id2bool**)");destroy((void**)pi2b);

}

void u16id2bool_clearall(struct u16id2bool **pi2b)
{
	dropall((void**)pi2b);
}

/** Instrumented function u16id2bool_get(struct u16id2bool*,uint16_t) */
int u16id2bool_get(struct u16id2bool *i2b, uint16_t id)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2bool_get(struct u16id2bool*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===365###sois===7556###eois===7578###lif===2###soif===59###eoif===81###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_get(struct u16id2bool*,uint16_t)");uintptr_t mask, field;

		AKA_mark("lis===366###sois===7580###eois===7600###lif===3###soif===83###eoif===103###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_get(struct u16id2bool*,uint16_t)");uint16_t index, idm;

		AKA_mark("lis===367###sois===7602###eois===7614###lif===4###soif===105###eoif===117###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_get(struct u16id2bool*,uint16_t)");flat_t flat;


		AKA_mark("lis===369###sois===7617###eois===7636###lif===6###soif===120###eoif===139###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_get(struct u16id2bool*,uint16_t)");flatof(&flat, i2b);

		AKA_mark("lis===370###sois===7638###eois===7670###lif===7###soif===141###eoif===173###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_get(struct u16id2bool*,uint16_t)");idm = (uint16_t)(id & ~(P - 1));

		AKA_mark("lis===371###sois===7672###eois===7699###lif===8###soif===175###eoif===202###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_get(struct u16id2bool*,uint16_t)");index = search(&flat, idm);

		if (AKA_mark("lis===372###sois===7705###eois===7711###lif===9###soif===208###eoif===214###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_get(struct u16id2bool*,uint16_t)") && (AKA_mark("lis===372###sois===7705###eois===7711###lif===9###soif===208###eoif===214###isc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_get(struct u16id2bool*,uint16_t)")&&!index)) {
		AKA_mark("lis===373###sois===7715###eois===7724###lif===10###soif===218###eoif===227###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_get(struct u16id2bool*,uint16_t)");return 0;
	}
	else {AKA_mark("lis===-372-###sois===-7705-###eois===-77056-###lif===-9-###soif===-###eoif===-214-###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_get(struct u16id2bool*,uint16_t)");}


		AKA_mark("lis===375###sois===7727###eois===7763###lif===12###soif===230###eoif===266###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_get(struct u16id2bool*,uint16_t)");field = (uintptr_t)flat.ptrs[index];

		AKA_mark("lis===376###sois===7765###eois===7816###lif===13###soif===268###eoif===319###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_get(struct u16id2bool*,uint16_t)");mask = (uintptr_t)((uintptr_t)1 << (id & (P - 1)));

		AKA_mark("lis===377###sois===7818###eois===7845###lif===14###soif===321###eoif===348###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_get(struct u16id2bool*,uint16_t)");return (field & mask) != 0;

}

/** Instrumented function u16id2bool_set(struct u16id2bool**,uint16_t,int) */
int u16id2bool_set(struct u16id2bool **pi2b, uint16_t id, int value)
{AKA_mark("Calling: ./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");AKA_fCall++;
		AKA_mark("lis===382###sois===7921###eois===7944###lif===2###soif===72###eoif===95###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");struct u16id2bool *i2b;

		AKA_mark("lis===383###sois===7946###eois===7976###lif===3###soif===97###eoif===127###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");uintptr_t mask, field, ofield;

		AKA_mark("lis===384###sois===7978###eois===7998###lif===4###soif===129###eoif===149###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");uint16_t index, idm;

		AKA_mark("lis===385###sois===8000###eois===8012###lif===5###soif===151###eoif===163###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");flat_t flat;


		AKA_mark("lis===387###sois===8015###eois===8027###lif===7###soif===166###eoif===178###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");i2b = *pi2b;

		AKA_mark("lis===388###sois===8029###eois===8048###lif===8###soif===180###eoif===199###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");flatof(&flat, i2b);

		AKA_mark("lis===389###sois===8050###eois===8082###lif===9###soif===201###eoif===233###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");idm = (uint16_t)(id & ~(P - 1));

		AKA_mark("lis===390###sois===8084###eois===8111###lif===10###soif===235###eoif===262###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");index = search(&flat, idm);

		AKA_mark("lis===391###sois===8113###eois===8162###lif===11###soif===264###eoif===313###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");ofield = index ? (uintptr_t)flat.ptrs[index] : 0;

		AKA_mark("lis===392###sois===8164###eois===8215###lif===12###soif===315###eoif===366###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");mask = (uintptr_t)((uintptr_t)1 << (id & (P - 1)));

		if (AKA_mark("lis===393###sois===8221###eois===8226###lif===13###soif===372###eoif===377###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)") && (AKA_mark("lis===393###sois===8221###eois===8226###lif===13###soif===372###eoif===377###isc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)")&&value)) {
		AKA_mark("lis===394###sois===8230###eois===8252###lif===14###soif===381###eoif===403###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");field = ofield | mask;
	}
	else {
		AKA_mark("lis===396###sois===8261###eois===8284###lif===16###soif===412###eoif===435###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");field = ofield & ~mask;
	}

		if (AKA_mark("lis===397###sois===8290###eois===8305###lif===17###soif===441###eoif===456###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)") && (AKA_mark("lis===397###sois===8290###eois===8305###lif===17###soif===441###eoif===456###isc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)")&&field != ofield)) {
				if (AKA_mark("lis===398###sois===8315###eois===8320###lif===18###soif===466###eoif===471###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)") && (AKA_mark("lis===398###sois===8315###eois===8320###lif===18###soif===466###eoif===471###isc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)")&&field)) {
						if (AKA_mark("lis===399###sois===8331###eois===8336###lif===19###soif===482###eoif===487###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)") && (AKA_mark("lis===399###sois===8331###eois===8336###lif===19###soif===482###eoif===487###isc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)")&&index)) {
				AKA_mark("lis===400###sois===8342###eois===8374###lif===20###soif===493###eoif===525###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");flat.ptrs[index] = (void*)field;
			}
			else {
								AKA_mark("lis===402###sois===8389###eois===8425###lif===22###soif===540###eoif===576###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");i2b = add(&flat, idm, (void*)field);

								if (AKA_mark("lis===403###sois===8434###eois===8438###lif===23###soif===585###eoif===589###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)") && (AKA_mark("lis===403###sois===8434###eois===8438###lif===23###soif===585###eoif===589###isc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)")&&!i2b)) {
					AKA_mark("lis===404###sois===8445###eois===8455###lif===24###soif===596###eoif===606###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");return -1;
				}
				else {AKA_mark("lis===-403-###sois===-8434-###eois===-84344-###lif===-23-###soif===-###eoif===-589-###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");}

								AKA_mark("lis===405###sois===8460###eois===8472###lif===25###soif===611###eoif===623###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");*pi2b = i2b;

			}

		}
		else {
						if (AKA_mark("lis===408###sois===8496###eois===8501###lif===28###soif===647###eoif===652###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)") && (AKA_mark("lis===408###sois===8496###eois===8501###lif===28###soif===647###eoif===652###isc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)")&&index)) {
								AKA_mark("lis===409###sois===8509###eois===8534###lif===29###soif===660###eoif===685###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");i2b = drop(&flat, index);

								if (AKA_mark("lis===410###sois===8543###eois===8547###lif===30###soif===694###eoif===698###ifc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)") && (AKA_mark("lis===410###sois===8543###eois===8547###lif===30###soif===694###eoif===698###isc===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)")&&!i2b)) {
					AKA_mark("lis===411###sois===8554###eois===8564###lif===31###soif===705###eoif===715###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");return -1;
				}
				else {AKA_mark("lis===-410-###sois===-8543-###eois===-85434-###lif===-30-###soif===-###eoif===-698-###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");}

								AKA_mark("lis===412###sois===8569###eois===8581###lif===32###soif===720###eoif===732###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");*pi2b = i2b;

			}
			else {AKA_mark("lis===-408-###sois===-8496-###eois===-84965-###lif===-28-###soif===-###eoif===-652-###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");}

		}

	}
	else {AKA_mark("lis===-397-###sois===-8290-###eois===-829015-###lif===-17-###soif===-###eoif===-456-###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");}

		AKA_mark("lis===416###sois===8595###eois===8623###lif===36###soif===746###eoif===774###ins===true###function===./app-framework-binder/src/u16id.c/u16id2bool_set(struct u16id2bool**,uint16_t,int)");return (ofield & mask) != 0;

}

#endif

