/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_API_H
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_API_H
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

struct afb_xreq;
struct afb_apiset;
struct json_object;

struct afb_api_itf
{
	void (*call)(void *closure, struct afb_xreq *xreq);
	int (*service_start)(void *closure);
#if WITH_AFB_HOOK
	void (*update_hooks)(void *closure);
#endif
	int (*get_logmask)(void *closure);
	void (*set_logmask)(void *closure, int level);
	void (*describe)(void *closure, void (*describecb)(void *, struct json_object *), void *clocb);
	void (*unref)(void *closure);
};

struct afb_api_item
{
	void *closure;
	struct afb_api_itf *itf;
	const void *group;
};

extern int afb_api_is_valid_name(const char *name);

/** Instrumented function afb_api_is_public(const char*) */
static inline int afb_api_is_public(const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api.h/afb_api_is_public(const char*)");AKA_fCall++;
		AKA_mark("lis===48###sois===1304###eois===1324###lif===2###soif===57###eoif===77###ins===true###function===./app-framework-binder/src/afb-api.h/afb_api_is_public(const char*)");return *name != '.';

}



#endif

