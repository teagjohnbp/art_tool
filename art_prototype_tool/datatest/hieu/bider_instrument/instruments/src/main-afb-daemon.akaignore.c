/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_MAIN_AFB_DAEMON_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_MAIN_AFB_DAEMON_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author "Fulup Ar Foll"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>

#if !defined(NO_CALL_PERSONALITY)
#include <sys/personality.h>
#endif

#include <json-c/json.h>

#include <systemd/sd-daemon.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_ARGS_H_
#define AKA_INCLUDE__AFB_ARGS_H_
#include "afb-args.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HSWITCH_H_
#define AKA_INCLUDE__AFB_HSWITCH_H_
#include "afb-hswitch.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_APISET_H_
#define AKA_INCLUDE__AFB_APISET_H_
#include "afb-apiset.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_AUTOSET_H_
#define AKA_INCLUDE__AFB_AUTOSET_H_
#include "afb-autoset.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_SO_H_
#define AKA_INCLUDE__AFB_API_SO_H_
#include "afb-api-so.akaignore.h"
#endif

#if WITH_DBUS_TRANSPARENCY
#   include "afb-api-dbus.h"
#endif
/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_WS_H_
#define AKA_INCLUDE__AFB_API_WS_H_
#include "afb-api-ws.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HSRV_H_
#define AKA_INCLUDE__AFB_HSRV_H_
#include "afb-hsrv.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HREQ_H_
#define AKA_INCLUDE__AFB_HREQ_H_
#include "afb-hreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_XREQ_H_
#define AKA_INCLUDE__AFB_XREQ_H_
#include "afb-xreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SESSION_H_
#define AKA_INCLUDE__AFB_SESSION_H_
#include "afb-session.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_COMMON_H_
#define AKA_INCLUDE__AFB_COMMON_H_
#include "afb-common.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_EXPORT_H_
#define AKA_INCLUDE__AFB_EXPORT_H_
#include "afb-export.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_MONITOR_H_
#define AKA_INCLUDE__AFB_MONITOR_H_
#include "afb-monitor.akaignore.h"
#endif

#if WITH_AFB_HOOK
#include "afb-hook.h"
#include "afb-hook-flags.h"
#endif
/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_DEBUG_H_
#define AKA_INCLUDE__AFB_DEBUG_H_
#include "afb-debug.akaignore.h"
#endif

#if WITH_SUPERVISION
#   include "afb-supervision.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__PROCESS_NAME_H_
#define AKA_INCLUDE__PROCESS_NAME_H_
#include "process-name.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__WRAP_JSON_H_
#define AKA_INCLUDE__WRAP_JSON_H_
#include "wrap-json.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__JOBS_H_
#define AKA_INCLUDE__JOBS_H_
#include "jobs.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__SIG_MONITOR_H_
#define AKA_INCLUDE__SIG_MONITOR_H_
#include "sig-monitor.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__WATCHDOG_H_
#define AKA_INCLUDE__WATCHDOG_H_
#include "watchdog.akaignore.h"
#endif


#if !defined(DEFAULT_BINDER_INTERFACE)
#  define DEFAULT_BINDER_INTERFACE NULL
#endif

/*
   if SELF_PGROUP == 0 the launched command is the group leader
   if SELF_PGROUP != 0 afb-daemon is the group leader
*/
#define SELF_PGROUP 0

struct afb_apiset *main_apiset;
struct json_object *main_config;

static pid_t childpid;

/**
 * Tiny helper around putenv: add the variable name=value
 *
 * @param name name of the variable to set
 * @param value value to set to the variable
 *
 * @return 0 in case of success or -1 in case of error (with errno set to ENOMEM)
 */
/** Instrumented function addenv(const char*,const char*) */
static int addenv(const char *name, const char *value)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/addenv(const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===98###sois===2331###eois===2351###lif===2###soif===58###eoif===78###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv(const char*,const char*)");char *head, *middle;


		AKA_mark("lis===100###sois===2354###eois===2402###lif===4###soif===81###eoif===129###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv(const char*,const char*)");head = malloc(2 + strlen(name) + strlen(value));

		if (AKA_mark("lis===101###sois===2408###eois===2420###lif===5###soif===135###eoif===147###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv(const char*,const char*)") && (AKA_mark("lis===101###sois===2408###eois===2420###lif===5###soif===135###eoif===147###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv(const char*,const char*)")&&head == NULL)) {
				AKA_mark("lis===102###sois===2426###eois===2441###lif===6###soif===153###eoif===168###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv(const char*,const char*)");errno = ENOMEM;

				AKA_mark("lis===103###sois===2444###eois===2454###lif===7###soif===171###eoif===181###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv(const char*,const char*)");return -1;

	}
	else {AKA_mark("lis===-101-###sois===-2408-###eois===-240812-###lif===-5-###soif===-###eoif===-147-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv(const char*,const char*)");}

		AKA_mark("lis===105###sois===2459###eois===2487###lif===9###soif===186###eoif===214###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv(const char*,const char*)");middle = stpcpy(head, name);

		AKA_mark("lis===106###sois===2489###eois===2505###lif===10###soif===216###eoif===232###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv(const char*,const char*)");middle[0] = '=';

		AKA_mark("lis===107###sois===2507###eois===2533###lif===11###soif===234###eoif===260###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv(const char*,const char*)");strcpy(&middle[1], value);

		AKA_mark("lis===108###sois===2535###eois===2555###lif===12###soif===262###eoif===282###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv(const char*,const char*)");return putenv(head);

}

/**
 * Tiny helper around addenv that export the real path
 *
 * @param name name of the variable to set
 * @param path the path value to export to the variable
 *
 * @return 0 in case of success or -1 in case of error (with errno set to ENOMEM)
 */
/** Instrumented function addenv_realpath(const char*,const char*) */
static int addenv_realpath(const char *name, const char *path)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/addenv_realpath(const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===121###sois===2875###eois===2897###lif===2###soif===66###eoif===88###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv_realpath(const char*,const char*)");char buffer[PATH_MAX];

		AKA_mark("lis===122###sois===2899###eois===2932###lif===3###soif===90###eoif===123###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv_realpath(const char*,const char*)");char *p = realpath(path, buffer);

		AKA_mark("lis===123###sois===2934###eois===2966###lif===4###soif===125###eoif===157###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv_realpath(const char*,const char*)");return p ? addenv(name, p) : -1;

}

/**
 * Tiny helper around addenv that export an integer
 *
 * @param name name of the variable to set
 * @param value the integer value to export
 *
 * @return 0 in case of success or -1 in case of error (with errno set to ENOMEM)
 */
/** Instrumented function addenv_int(const char*,int) */
static int addenv_int(const char *name, int value)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/addenv_int(const char*,int)");AKA_fCall++;
		AKA_mark("lis===136###sois===3259###eois===3275###lif===2###soif===54###eoif===70###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv_int(const char*,int)");char buffer[64];

		AKA_mark("lis===137###sois===3277###eois===3322###lif===3###soif===72###eoif===117###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv_int(const char*,int)");snprintf(buffer, sizeof buffer, "%d", value);

		AKA_mark("lis===138###sois===3324###eois===3352###lif===4###soif===119###eoif===147###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/addenv_int(const char*,int)");return addenv(name, buffer);

}

/*----------------------------------------------------------
 |   helpers for handling list of arguments
 +--------------------------------------------------------- */

/** Instrumented function run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*) */
static const char *run_for_config_array_opt(const char *name,
					    int (*run) (void *closure, const char *value),
					    void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)");AKA_fCall++;
		AKA_mark("lis===149###sois===3670###eois===3683###lif===4###soif===145###eoif===158###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)");int i, n, rc;

		AKA_mark("lis===150###sois===3685###eois===3719###lif===5###soif===160###eoif===194###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)");struct json_object *array, *value;


		if (AKA_mark("lis===152###sois===3726###eois===3778###lif===7###soif===201###eoif===253###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)") && (AKA_mark("lis===152###sois===3726###eois===3778###lif===7###soif===201###eoif===253###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)")&&json_object_object_get_ex(main_config, name, &array))) {
				if (AKA_mark("lis===153###sois===3788###eois===3832###lif===8###soif===263###eoif===307###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)") && (AKA_mark("lis===153###sois===3788###eois===3832###lif===8###soif===263###eoif===307###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)")&&!json_object_is_type(array, json_type_array))) {
			AKA_mark("lis===154###sois===3837###eois===3874###lif===9###soif===312###eoif===349###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)");return json_object_get_string(array);
		}
		else {AKA_mark("lis===-153-###sois===-3788-###eois===-378844-###lif===-8-###soif===-###eoif===-307-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)");}

				AKA_mark("lis===155###sois===3877###eois===3918###lif===10###soif===352###eoif===393###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)");n = (int)json_object_array_length(array);

				AKA_mark("lis===156###sois===3926###eois===3933###lif===11###soif===401###eoif===408###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)");for (i = 0 ;AKA_mark("lis===156###sois===3934###eois===3939###lif===11###soif===409###eoif===414###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)") && AKA_mark("lis===156###sois===3934###eois===3939###lif===11###soif===409###eoif===414###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)")&&i < n;({AKA_mark("lis===156###sois===3942###eois===3945###lif===11###soif===417###eoif===420###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)");i++;})) {
						AKA_mark("lis===157###sois===3952###eois===3996###lif===12###soif===427###eoif===471###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)");value = json_object_array_get_idx(array, i);

						AKA_mark("lis===158###sois===4000###eois===4049###lif===13###soif===475###eoif===524###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)");rc = run(closure, json_object_get_string(value));

						if (AKA_mark("lis===159###sois===4057###eois===4060###lif===14###soif===532###eoif===535###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)") && (AKA_mark("lis===159###sois===4057###eois===4060###lif===14###soif===532###eoif===535###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)")&&!rc)) {
				AKA_mark("lis===160###sois===4066###eois===4103###lif===15###soif===541###eoif===578###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)");return json_object_get_string(value);
			}
			else {AKA_mark("lis===-159-###sois===-4057-###eois===-40573-###lif===-14-###soif===-###eoif===-535-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)");}

		}

	}
	else {AKA_mark("lis===-152-###sois===-3726-###eois===-372652-###lif===-7-###soif===-###eoif===-253-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)");}

		AKA_mark("lis===163###sois===4112###eois===4124###lif===18###soif===587###eoif===599###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_for_config_array_opt(const char*,int(*run) (void*closure, const char*value),void*)");return NULL;

}

/** Instrumented function run_start(void*,const char*) */
static int run_start(void *closure, const char *value)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/run_start(void*,const char*)");AKA_fCall++;
		AKA_mark("lis===168###sois===4186###eois===4292###lif===2###soif===58###eoif===164###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_start(void*,const char*)");int (*starter) (const char *value, struct afb_apiset *declare_set, struct afb_apiset *call_set) = closure;

		AKA_mark("lis===169###sois===4294###eois===4347###lif===3###soif===166###eoif===219###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_start(void*,const char*)");return starter(value, main_apiset, main_apiset) >= 0;

}

/** Instrumented function apiset_start_list(const char*,int(*starter) (const char*value, struct afb_apiset*declare_set, struct afb_apiset*call_set),const char*) */
static void apiset_start_list(const char *name,
			int (*starter) (const char *value, struct afb_apiset *declare_set, struct afb_apiset *call_set),
			const char *message)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/apiset_start_list(const char*,int(*starter) (const char*value, struct afb_apiset*declare_set, struct afb_apiset*call_set),const char*)");AKA_fCall++;
		AKA_mark("lis===176###sois===4526###eois===4596###lif===4###soif===175###eoif===245###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/apiset_start_list(const char*,int(*starter) (const char*value, struct afb_apiset*declare_set, struct afb_apiset*call_set),const char*)");const char *item = run_for_config_array_opt(name, run_start, starter);

		if (AKA_mark("lis===177###sois===4602###eois===4606###lif===5###soif===251###eoif===255###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/apiset_start_list(const char*,int(*starter) (const char*value, struct afb_apiset*declare_set, struct afb_apiset*call_set),const char*)") && (AKA_mark("lis===177###sois===4602###eois===4606###lif===5###soif===251###eoif===255###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/apiset_start_list(const char*,int(*starter) (const char*value, struct afb_apiset*declare_set, struct afb_apiset*call_set),const char*)")&&item)) {
				AKA_mark("lis===178###sois===4612###eois===4654###lif===6###soif===261###eoif===303###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/apiset_start_list(const char*,int(*starter) (const char*value, struct afb_apiset*declare_set, struct afb_apiset*call_set),const char*)");ERROR("can't start %s %s", message, item);

				AKA_mark("lis===179###sois===4657###eois===4665###lif===7###soif===306###eoif===314###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/apiset_start_list(const char*,int(*starter) (const char*value, struct afb_apiset*declare_set, struct afb_apiset*call_set),const char*)");exit(1);

	}
	else {AKA_mark("lis===-177-###sois===-4602-###eois===-46024-###lif===-5-###soif===-###eoif===-255-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/apiset_start_list(const char*,int(*starter) (const char*value, struct afb_apiset*declare_set, struct afb_apiset*call_set),const char*)");}

}

/*----------------------------------------------------------
 | exit_handler
 |   Handles on exit specific actions
 +--------------------------------------------------------- */
/** Instrumented function exit_handler() */
static void exit_handler()
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/exit_handler()");AKA_fCall++;
		AKA_mark("lis===189###sois===4880###eois===4902###lif===2###soif===30###eoif===52###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/exit_handler()");struct sigaction siga;


		AKA_mark("lis===191###sois===4905###eois===4935###lif===4###soif===55###eoif===85###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/exit_handler()");memset(&siga, 0, sizeof siga);

		AKA_mark("lis===192###sois===4937###eois===4963###lif===5###soif===87###eoif===113###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/exit_handler()");siga.sa_handler = SIG_IGN;

		AKA_mark("lis===193###sois===4965###eois===4997###lif===6###soif===115###eoif===147###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/exit_handler()");sigaction(SIGTERM, &siga, NULL);


		if (AKA_mark("lis===195###sois===5004###eois===5015###lif===8###soif===154###eoif===165###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/exit_handler()") && (AKA_mark("lis===195###sois===5004###eois===5015###lif===8###soif===154###eoif===165###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/exit_handler()")&&SELF_PGROUP)) {
		AKA_mark("lis===196###sois===5019###eois===5038###lif===9###soif===169###eoif===188###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/exit_handler()");killpg(0, SIGTERM);
	}
	else {
		if (AKA_mark("lis===197###sois===5049###eois===5061###lif===10###soif===199###eoif===211###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/exit_handler()") && (AKA_mark("lis===197###sois===5049###eois===5061###lif===10###soif===199###eoif===211###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/exit_handler()")&&childpid > 0)) {
			AKA_mark("lis===198###sois===5065###eois===5091###lif===11###soif===215###eoif===241###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/exit_handler()");killpg(childpid, SIGTERM);
		}
		else {AKA_mark("lis===-197-###sois===-5049-###eois===-504912-###lif===-10-###soif===-###eoif===-211-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/exit_handler()");}
	}

}

/** Instrumented function on_sigterm(int,siginfo_t*,void*) */
static void on_sigterm(int signum, siginfo_t *info, void *uctx)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/on_sigterm(int,siginfo_t*,void*)");AKA_fCall++;
		AKA_mark("lis===203###sois===5162###eois===5189###lif===2###soif===67###eoif===94###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/on_sigterm(int,siginfo_t*,void*)");NOTICE("Received SIGTERM");

		AKA_mark("lis===204###sois===5191###eois===5199###lif===3###soif===96###eoif===104###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/on_sigterm(int,siginfo_t*,void*)");exit(0);

}

/** Instrumented function on_sighup(int,siginfo_t*,void*) */
static void on_sighup(int signum, siginfo_t *info, void *uctx)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/on_sighup(int,siginfo_t*,void*)");AKA_fCall++;
		AKA_mark("lis===209###sois===5269###eois===5295###lif===2###soif===66###eoif===92###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/on_sighup(int,siginfo_t*,void*)");NOTICE("Received SIGHUP");

	/* TODO */
}

/** Instrumented function setup_daemon() */
static void setup_daemon()
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/setup_daemon()");AKA_fCall++;
		AKA_mark("lis===215###sois===5341###eois===5363###lif===2###soif===30###eoif===52###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/setup_daemon()");struct sigaction siga;


	/* install signal handlers */
		AKA_mark("lis===218###sois===5397###eois===5427###lif===5###soif===86###eoif===116###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/setup_daemon()");memset(&siga, 0, sizeof siga);

		AKA_mark("lis===219###sois===5429###eois===5456###lif===6###soif===118###eoif===145###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/setup_daemon()");siga.sa_flags = SA_SIGINFO;


		AKA_mark("lis===221###sois===5459###eois===5490###lif===8###soif===148###eoif===179###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/setup_daemon()");siga.sa_sigaction = on_sigterm;

		AKA_mark("lis===222###sois===5492###eois===5524###lif===9###soif===181###eoif===213###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/setup_daemon()");sigaction(SIGTERM, &siga, NULL);


		AKA_mark("lis===224###sois===5527###eois===5557###lif===11###soif===216###eoif===246###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/setup_daemon()");siga.sa_sigaction = on_sighup;

		AKA_mark("lis===225###sois===5559###eois===5590###lif===12###soif===248###eoif===279###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/setup_daemon()");sigaction(SIGHUP, &siga, NULL);


	/* handle groups */
		AKA_mark("lis===228###sois===5614###eois===5635###lif===15###soif===303###eoif===324###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/setup_daemon()");atexit(exit_handler);


	/* ignore any SIGPIPE */
		AKA_mark("lis===231###sois===5664###eois===5689###lif===18###soif===353###eoif===378###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/setup_daemon()");signal(SIGPIPE, SIG_IGN);

}

/*----------------------------------------------------------
 | daemonize
 |   set the process in background
 +--------------------------------------------------------- */
/** Instrumented function daemonize() */
static void daemonize()
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/daemonize()");AKA_fCall++;
		AKA_mark("lis===240###sois===5892###eois===5920###lif===2###soif===27###eoif===55###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");int fd = 0, daemon, nostdin;

		AKA_mark("lis===241###sois===5922###eois===5941###lif===3###soif===57###eoif===76###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");const char *output;

		AKA_mark("lis===242###sois===5943###eois===5953###lif===4###soif===78###eoif===88###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");pid_t pid;


		AKA_mark("lis===244###sois===5956###eois===5967###lif===6###soif===91###eoif===102###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");daemon = 0;

		AKA_mark("lis===245###sois===5969###eois===5983###lif===7###soif===104###eoif===118###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");output = NULL;

		AKA_mark("lis===246###sois===5985###eois===6066###lif===8###soif===120###eoif===201###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");wrap_json_unpack(main_config, "{s?b s?s}", "daemon", &daemon, "output", &output);

		AKA_mark("lis===247###sois===6068###eois===6080###lif===9###soif===203###eoif===215###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");nostdin = 0;


		if (AKA_mark("lis===249###sois===6087###eois===6093###lif===11###soif===222###eoif===228###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()") && (AKA_mark("lis===249###sois===6087###eois===6093###lif===11###soif===222###eoif===228###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()")&&output)) {
				AKA_mark("lis===250###sois===6099###eois===6154###lif===12###soif===234###eoif===289###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");fd = open(output, O_WRONLY | O_APPEND | O_CREAT, 0640);

				if (AKA_mark("lis===251###sois===6161###eois===6167###lif===13###soif===296###eoif===302###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()") && (AKA_mark("lis===251###sois===6161###eois===6167###lif===13###soif===296###eoif===302###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()")&&fd < 0)) {
						AKA_mark("lis===252###sois===6174###eois===6212###lif===14###soif===309###eoif===347###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");ERROR("Can't open output %s", output);

						AKA_mark("lis===253###sois===6216###eois===6224###lif===15###soif===351###eoif===359###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");exit(1);

		}
		else {AKA_mark("lis===-251-###sois===-6161-###eois===-61616-###lif===-13-###soif===-###eoif===-302-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");}

	}
	else {AKA_mark("lis===-249-###sois===-6087-###eois===-60876-###lif===-11-###soif===-###eoif===-228-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");}


		if (AKA_mark("lis===257###sois===6238###eois===6244###lif===19###soif===373###eoif===379###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()") && (AKA_mark("lis===257###sois===6238###eois===6244###lif===19###soif===373###eoif===379###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()")&&daemon)) {
				AKA_mark("lis===258###sois===6250###eois===6283###lif===20###soif===385###eoif===418###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");INFO("entering background mode");


				AKA_mark("lis===260###sois===6287###eois===6300###lif===22###soif===422###eoif===435###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");pid = fork();

				if (AKA_mark("lis===261###sois===6307###eois===6316###lif===23###soif===442###eoif===451###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()") && (AKA_mark("lis===261###sois===6307###eois===6316###lif===23###soif===442###eoif===451###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()")&&pid == -1)) {
						AKA_mark("lis===262###sois===6323###eois===6362###lif===24###soif===458###eoif===497###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");ERROR("Failed to fork daemon process");

						AKA_mark("lis===263###sois===6366###eois===6374###lif===25###soif===501###eoif===509###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");exit(1);

		}
		else {AKA_mark("lis===-261-###sois===-6307-###eois===-63079-###lif===-23-###soif===-###eoif===-451-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");}

				if (AKA_mark("lis===265###sois===6385###eois===6393###lif===27###soif===520###eoif===528###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()") && (AKA_mark("lis===265###sois===6385###eois===6393###lif===27###soif===520###eoif===528###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()")&&pid != 0)) {
			AKA_mark("lis===266###sois===6398###eois===6407###lif===28###soif===533###eoif===542###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");_exit(0);
		}
		else {AKA_mark("lis===-265-###sois===-6385-###eois===-63858-###lif===-27-###soif===-###eoif===-528-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");}


				AKA_mark("lis===268###sois===6411###eois===6423###lif===30###soif===546###eoif===558###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");nostdin = 1;

	}
	else {AKA_mark("lis===-257-###sois===-6238-###eois===-62386-###lif===-19-###soif===-###eoif===-379-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");}


	/* closes the input */
		if (AKA_mark("lis===272###sois===6457###eois===6463###lif===34###soif===592###eoif===598###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()") && (AKA_mark("lis===272###sois===6457###eois===6463###lif===34###soif===592###eoif===598###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()")&&output)) {
				AKA_mark("lis===273###sois===6469###eois===6512###lif===35###soif===604###eoif===647###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");NOTICE("Redirecting output to %s", output);

				AKA_mark("lis===274###sois===6515###eois===6524###lif===36###soif===650###eoif===659###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");close(2);

				AKA_mark("lis===275###sois===6527###eois===6535###lif===37###soif===662###eoif===670###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");dup(fd);

				AKA_mark("lis===276###sois===6538###eois===6547###lif===38###soif===673###eoif===682###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");close(1);

				AKA_mark("lis===277###sois===6550###eois===6558###lif===39###soif===685###eoif===693###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");dup(fd);

				AKA_mark("lis===278###sois===6561###eois===6571###lif===40###soif===696###eoif===706###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");close(fd);

	}
	else {AKA_mark("lis===-272-###sois===-6457-###eois===-64576-###lif===-34-###soif===-###eoif===-598-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");}


		if (AKA_mark("lis===281###sois===6581###eois===6588###lif===43###soif===716###eoif===723###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()") && (AKA_mark("lis===281###sois===6581###eois===6588###lif===43###soif===716###eoif===723###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()")&&nostdin)) {
		AKA_mark("lis===282###sois===6592###eois===6601###lif===44###soif===727###eoif===736###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");close(0);
	}
	else {AKA_mark("lis===-281-###sois===-6581-###eois===-65817-###lif===-43-###soif===-###eoif===-723-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/daemonize()");}
 /* except if 'daemon', ctrl+C still works after that */
}

/*---------------------------------------------------------
 | http server
 |   Handles the HTTP server
 +--------------------------------------------------------- */
/** Instrumented function init_alias(void*,const char*) */
static int init_alias(void *closure, const char *spec)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/init_alias(void*,const char*)");AKA_fCall++;
		AKA_mark("lis===291###sois===6886###eois===6918###lif===2###soif===58###eoif===90###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_alias(void*,const char*)");struct afb_hsrv *hsrv = closure;

		AKA_mark("lis===292###sois===6920###eois===6951###lif===3###soif===92###eoif===123###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_alias(void*,const char*)");char *path = strchr(spec, ':');


		if (AKA_mark("lis===294###sois===6958###eois===6970###lif===5###soif===130###eoif===142###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_alias(void*,const char*)") && (AKA_mark("lis===294###sois===6958###eois===6970###lif===5###soif===130###eoif===142###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_alias(void*,const char*)")&&path == NULL)) {
				AKA_mark("lis===295###sois===6976###eois===7030###lif===6###soif===148###eoif===202###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_alias(void*,const char*)");ERROR("Missing ':' in alias %s. Alias ignored", spec);

				AKA_mark("lis===296###sois===7033###eois===7042###lif===7###soif===205###eoif===214###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_alias(void*,const char*)");return 1;

	}
	else {AKA_mark("lis===-294-###sois===-6958-###eois===-695812-###lif===-5-###soif===-###eoif===-142-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_alias(void*,const char*)");}

		AKA_mark("lis===298###sois===7047###eois===7059###lif===9###soif===219###eoif===231###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_alias(void*,const char*)");*path++ = 0;

		AKA_mark("lis===299###sois===7061###eois===7109###lif===10###soif===233###eoif===281###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_alias(void*,const char*)");INFO("Alias for url=%s to path=%s", spec, path);

		AKA_mark("lis===300###sois===7111###eois===7196###lif===11###soif===283###eoif===368###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_alias(void*,const char*)");return afb_hsrv_add_alias(hsrv, spec, afb_common_rootdir_get_fd(), path,
				  0, 1);

}

/** Instrumented function init_http_server(struct afb_hsrv*) */
static int init_http_server(struct afb_hsrv *hsrv)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");AKA_fCall++;
		AKA_mark("lis===306###sois===7254###eois===7261###lif===2###soif===54###eoif===61###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");int rc;

		AKA_mark("lis===307###sois===7263###eois===7305###lif===3###soif===63###eoif===105###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");const char *rootapi, *roothttp, *rootbase;


		AKA_mark("lis===309###sois===7308###eois===7324###lif===5###soif===108###eoif===124###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");roothttp = NULL;

		AKA_mark("lis===310###sois===7326###eois===7455###lif===6###soif===126###eoif===255###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");rc = wrap_json_unpack(main_config, "{ss ss s?s}",
				"rootapi", &rootapi,
				"rootbase", &rootbase,
				"roothttp", &roothttp);

		if (AKA_mark("lis===314###sois===7461###eois===7467###lif===10###soif===261###eoif===267###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)") && (AKA_mark("lis===314###sois===7461###eois===7467###lif===10###soif===261###eoif===267###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)")&&rc < 0)) {
				AKA_mark("lis===315###sois===7473###eois===7511###lif===11###soif===273###eoif===311###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");ERROR("Can't get HTTP server config");

				AKA_mark("lis===316###sois===7514###eois===7522###lif===12###soif===314###eoif===322###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");exit(1);

	}
	else {AKA_mark("lis===-314-###sois===-7461-###eois===-74616-###lif===-10-###soif===-###eoif===-267-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");}


		if (AKA_mark("lis===319###sois===7532###eois===7618###lif===15###soif===332###eoif===418###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)") && (AKA_mark("lis===319###sois===7532###eois===7618###lif===15###soif===332###eoif===418###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)")&&!afb_hsrv_add_handler(hsrv, rootapi,
			afb_hswitch_websocket_switch, main_apiset, 20))) {
		AKA_mark("lis===321###sois===7622###eois===7631###lif===17###soif===422###eoif===431###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");return 0;
	}
	else {AKA_mark("lis===-319-###sois===-7532-###eois===-753286-###lif===-15-###soif===-###eoif===-418-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");}


		if (AKA_mark("lis===323###sois===7638###eois===7712###lif===19###soif===438###eoif===512###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)") && (AKA_mark("lis===323###sois===7638###eois===7712###lif===19###soif===438###eoif===512###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)")&&!afb_hsrv_add_handler(hsrv, rootapi,
			afb_hswitch_apis, main_apiset, 10))) {
		AKA_mark("lis===325###sois===7716###eois===7725###lif===21###soif===516###eoif===525###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");return 0;
	}
	else {AKA_mark("lis===-323-###sois===-7638-###eois===-763874-###lif===-19-###soif===-###eoif===-512-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");}


		if (AKA_mark("lis===327###sois===7732###eois===7783###lif===23###soif===532###eoif===583###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)") && (AKA_mark("lis===327###sois===7732###eois===7783###lif===23###soif===532###eoif===583###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)")&&run_for_config_array_opt("alias", init_alias, hsrv))) {
		AKA_mark("lis===328###sois===7787###eois===7796###lif===24###soif===587###eoif===596###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");return 0;
	}
	else {AKA_mark("lis===-327-###sois===-7732-###eois===-773251-###lif===-23-###soif===-###eoif===-583-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");}


		if (AKA_mark("lis===330###sois===7803###eois===7819###lif===26###soif===603###eoif===619###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)") && (AKA_mark("lis===330###sois===7803###eois===7819###lif===26###soif===603###eoif===619###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)")&&roothttp != NULL)) {
				if (AKA_mark("lis===331###sois===7829###eois===7908###lif===27###soif===629###eoif===708###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)") && (AKA_mark("lis===331###sois===7829###eois===7908###lif===27###soif===629###eoif===708###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)")&&!afb_hsrv_add_alias(hsrv, "",
			afb_common_rootdir_get_fd(), roothttp, -10, 1))) {
			AKA_mark("lis===333###sois===7913###eois===7922###lif===29###soif===713###eoif===722###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");return 0;
		}
		else {AKA_mark("lis===-331-###sois===-7829-###eois===-782979-###lif===-27-###soif===-###eoif===-708-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");}

	}
	else {AKA_mark("lis===-330-###sois===-7803-###eois===-780316-###lif===-26-###soif===-###eoif===-619-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");}


		if (AKA_mark("lis===336###sois===7932###eois===8018###lif===32###soif===732###eoif===818###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)") && (AKA_mark("lis===336###sois===7932###eois===8018###lif===32###soif===732###eoif===818###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)")&&!afb_hsrv_add_handler(hsrv, rootbase,
			afb_hswitch_one_page_api_redirect, NULL, -20))) {
		AKA_mark("lis===338###sois===8022###eois===8031###lif===34###soif===822###eoif===831###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");return 0;
	}
	else {AKA_mark("lis===-336-###sois===-7932-###eois===-793286-###lif===-32-###soif===-###eoif===-818-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");}


		AKA_mark("lis===340###sois===8034###eois===8043###lif===36###soif===834###eoif===843###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/init_http_server(struct afb_hsrv*)");return 1;

}

/** Instrumented function add_interface(void*,const char*) */
static int add_interface(void *closure, const char *value)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/add_interface(void*,const char*)");AKA_fCall++;
		AKA_mark("lis===345###sois===8109###eois===8141###lif===2###soif===62###eoif===94###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/add_interface(void*,const char*)");struct afb_hsrv *hsrv = closure;

		AKA_mark("lis===346###sois===8143###eois===8150###lif===3###soif===96###eoif===103###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/add_interface(void*,const char*)");int rc;


		AKA_mark("lis===348###sois===8153###eois===8194###lif===5###soif===106###eoif===147###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/add_interface(void*,const char*)");rc = afb_hsrv_add_interface(hsrv, value);

		AKA_mark("lis===349###sois===8196###eois===8210###lif===6###soif===149###eoif===163###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/add_interface(void*,const char*)");return rc > 0;

}

/** Instrumented function start_http_server() */
static struct afb_hsrv *start_http_server()
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/start_http_server()");AKA_fCall++;
		AKA_mark("lis===354###sois===8261###eois===8268###lif===2###soif===47###eoif===54###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");int rc;

		AKA_mark("lis===355###sois===8270###eois===8309###lif===3###soif===56###eoif===95###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");const char *uploaddir, *rootdir, *errs;

		AKA_mark("lis===356###sois===8311###eois===8333###lif===4###soif===97###eoif===119###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");struct afb_hsrv *hsrv;

		AKA_mark("lis===357###sois===8335###eois===8364###lif===5###soif===121###eoif===150###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");int cache_timeout, http_port;

		AKA_mark("lis===358###sois===8366###eois===8391###lif===6###soif===152###eoif===177###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");struct json_object *junk;


		AKA_mark("lis===360###sois===8394###eois===8409###lif===8###soif===180###eoif===195###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");http_port = -1;

		AKA_mark("lis===361###sois===8411###eois===8575###lif===9###soif===197###eoif===361###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");rc = wrap_json_unpack(main_config, "{ss ss si s?i}",
				"uploaddir", &uploaddir,
				"rootdir", &rootdir,
				"cache-eol", &cache_timeout,
				"port", &http_port);

		if (AKA_mark("lis===366###sois===8581###eois===8587###lif===14###soif===367###eoif===373###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()") && (AKA_mark("lis===366###sois===8581###eois===8587###lif===14###soif===367###eoif===373###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()")&&rc < 0)) {
				AKA_mark("lis===367###sois===8593###eois===8637###lif===15###soif===379###eoif===423###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");ERROR("Can't get HTTP server start config");

				AKA_mark("lis===368###sois===8640###eois===8648###lif===16###soif===426###eoif===434###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");exit(1);

	}
	else {AKA_mark("lis===-366-###sois===-8581-###eois===-85816-###lif===-14-###soif===-###eoif===-373-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");}


		if (AKA_mark("lis===371###sois===8658###eois===8696###lif===19###soif===444###eoif===482###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()") && (AKA_mark("lis===371###sois===8658###eois===8696###lif===19###soif===444###eoif===482###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()")&&afb_hreq_init_download_path(uploaddir))) {
				AKA_mark("lis===372###sois===8702###eois===8750###lif===20###soif===488###eoif===536###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");static const char fallback_uploaddir[] = "/tmp";

				AKA_mark("lis===373###sois===8753###eois===8813###lif===21###soif===539###eoif===599###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");WARNING("unable to set the upload directory %s", uploaddir);

				if (AKA_mark("lis===374###sois===8820###eois===8867###lif===22###soif===606###eoif===653###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()") && (AKA_mark("lis===374###sois===8820###eois===8867###lif===22###soif===606###eoif===653###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()")&&afb_hreq_init_download_path(fallback_uploaddir))) {
						AKA_mark("lis===375###sois===8874###eois===8945###lif===23###soif===660###eoif===731###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");ERROR("unable to fallback to upload directory %s", fallback_uploaddir);

						AKA_mark("lis===376###sois===8949###eois===8961###lif===24###soif===735###eoif===747###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");return NULL;

		}
		else {AKA_mark("lis===-374-###sois===-8820-###eois===-882047-###lif===-22-###soif===-###eoif===-653-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");}

				AKA_mark("lis===378###sois===8968###eois===8999###lif===26###soif===754###eoif===785###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");uploaddir = fallback_uploaddir;

	}
	else {AKA_mark("lis===-371-###sois===-8658-###eois===-865838-###lif===-19-###soif===-###eoif===-482-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");}


		AKA_mark("lis===381###sois===9005###eois===9030###lif===29###soif===791###eoif===816###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");hsrv = afb_hsrv_create();

		if (AKA_mark("lis===382###sois===9036###eois===9048###lif===30###soif===822###eoif===834###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()") && (AKA_mark("lis===382###sois===9036###eois===9048###lif===30###soif===822###eoif===834###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()")&&hsrv == NULL)) {
				AKA_mark("lis===383###sois===9054###eois===9089###lif===31###soif===840###eoif===875###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");ERROR("memory allocation failure");

				AKA_mark("lis===384###sois===9092###eois===9104###lif===32###soif===878###eoif===890###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");return NULL;

	}
	else {AKA_mark("lis===-382-###sois===-9036-###eois===-903612-###lif===-30-###soif===-###eoif===-834-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");}


		if (AKA_mark("lis===387###sois===9114###eois===9194###lif===35###soif===900###eoif===980###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()") && ((AKA_mark("lis===387###sois===9114###eois===9162###lif===35###soif===900###eoif===948###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()")&&!afb_hsrv_set_cache_timeout(hsrv, cache_timeout))	||(AKA_mark("lis===388###sois===9171###eois===9194###lif===36###soif===957###eoif===980###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()")&&!init_http_server(hsrv)))) {
				AKA_mark("lis===389###sois===9200###eois===9240###lif===37###soif===986###eoif===1026###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");ERROR("initialisation of httpd failed");

				AKA_mark("lis===390###sois===9243###eois===9262###lif===38###soif===1029###eoif===1048###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");afb_hsrv_put(hsrv);

				AKA_mark("lis===391###sois===9265###eois===9277###lif===39###soif===1051###eoif===1063###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");return NULL;

	}
	else {AKA_mark("lis===-387-###sois===-9114-###eois===-911480-###lif===-35-###soif===-###eoif===-980-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");}


		AKA_mark("lis===394###sois===9283###eois===9313###lif===42###soif===1069###eoif===1099###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");rc = afb_hsrv_start(hsrv, 15);

		if (AKA_mark("lis===395###sois===9319###eois===9322###lif===43###soif===1105###eoif===1108###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()") && (AKA_mark("lis===395###sois===9319###eois===9322###lif===43###soif===1105###eoif===1108###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()")&&!rc)) {
				AKA_mark("lis===396###sois===9328###eois===9362###lif===44###soif===1114###eoif===1148###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");ERROR("starting of httpd failed");

				AKA_mark("lis===397###sois===9365###eois===9384###lif===45###soif===1151###eoif===1170###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");afb_hsrv_put(hsrv);

				AKA_mark("lis===398###sois===9387###eois===9399###lif===46###soif===1173###eoif===1185###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");return NULL;

	}
	else {AKA_mark("lis===-395-###sois===-9319-###eois===-93193-###lif===-43-###soif===-###eoif===-1108-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");}


		AKA_mark("lis===401###sois===9405###eois===9467###lif===49###soif===1191###eoif===1253###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");NOTICE("Serving rootdir=%s uploaddir=%s", rootdir, uploaddir);


	/* check if port is set */
		if (AKA_mark("lis===404###sois===9502###eois===9515###lif===52###soif===1288###eoif===1301###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()") && (AKA_mark("lis===404###sois===9502###eois===9515###lif===52###soif===1288###eoif===1301###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()")&&http_port < 0)) {
		/* not set, check existing interfaces */
				if (AKA_mark("lis===406###sois===9568###eois===9627###lif===54###soif===1354###eoif===1413###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()") && (AKA_mark("lis===406###sois===9568###eois===9627###lif===54###soif===1354###eoif===1413###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()")&&!json_object_object_get_ex(main_config, "interface", &junk))) {
						AKA_mark("lis===407###sois===9634###eois===9669###lif===55###soif===1420###eoif===1455###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");ERROR("No port and no interface ");

		}
		else {AKA_mark("lis===-406-###sois===-9568-###eois===-956859-###lif===-54-###soif===-###eoif===-1413-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");}

	}
	else {
				AKA_mark("lis===410###sois===9686###eois===9772###lif===58###soif===1472###eoif===1558###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");rc = afb_hsrv_add_interface_tcp(hsrv, DEFAULT_BINDER_INTERFACE, (uint16_t) http_port);

				if (AKA_mark("lis===411###sois===9779###eois===9782###lif===59###soif===1565###eoif===1568###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()") && (AKA_mark("lis===411###sois===9779###eois===9782###lif===59###soif===1565###eoif===1568###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()")&&!rc)) {
						AKA_mark("lis===412###sois===9789###eois===9823###lif===60###soif===1575###eoif===1609###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");ERROR("setting interface failed");

						AKA_mark("lis===413###sois===9827###eois===9846###lif===61###soif===1613###eoif===1632###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");afb_hsrv_put(hsrv);

						AKA_mark("lis===414###sois===9850###eois===9862###lif===62###soif===1636###eoif===1648###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");return NULL;

		}
		else {AKA_mark("lis===-411-###sois===-9779-###eois===-97793-###lif===-59-###soif===-###eoif===-1568-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");}

				AKA_mark("lis===416###sois===9869###eois===9923###lif===64###soif===1655###eoif===1709###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");NOTICE("Browser URL= http://localhost:%d", http_port);

	}


		AKA_mark("lis===419###sois===9929###eois===9995###lif===67###soif===1715###eoif===1781###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");errs = run_for_config_array_opt("interface", add_interface, hsrv);

		if (AKA_mark("lis===420###sois===10001###eois===10005###lif===68###soif===1787###eoif===1791###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()") && (AKA_mark("lis===420###sois===10001###eois===10005###lif===68###soif===1787###eoif===1791###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()")&&errs)) {
				AKA_mark("lis===421###sois===10011###eois===10054###lif===69###soif===1797###eoif===1840###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");ERROR("setting interface %s failed", errs);

				AKA_mark("lis===422###sois===10057###eois===10076###lif===70###soif===1843###eoif===1862###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");afb_hsrv_put(hsrv);

				AKA_mark("lis===423###sois===10079###eois===10091###lif===71###soif===1865###eoif===1877###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");return NULL;

	}
	else {AKA_mark("lis===-420-###sois===-10001-###eois===-100014-###lif===-68-###soif===-###eoif===-1791-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");}


		AKA_mark("lis===426###sois===10097###eois===10109###lif===74###soif===1883###eoif===1895###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start_http_server()");return hsrv;

}

/*---------------------------------------------------------
 | execute_command
 +--------------------------------------------------------- */

/** Instrumented function exit_at_end() */
static void exit_at_end()
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/exit_at_end()");AKA_fCall++;
		AKA_mark("lis===435###sois===10285###eois===10293###lif===2###soif===29###eoif===37###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/exit_at_end()");exit(0);

}

/** Instrumented function wait_child(int,void*) */
static void wait_child(int signum, void* arg)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/wait_child(int,void*)");AKA_fCall++;
		AKA_mark("lis===440###sois===10346###eois===10379###lif===2###soif===49###eoif===82###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/wait_child(int,void*)");pid_t pid = (pid_t)(intptr_t)arg;

		AKA_mark("lis===441###sois===10381###eois===10406###lif===3###soif===84###eoif===109###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/wait_child(int,void*)");pid_t pidchld = childpid;


		if (AKA_mark("lis===443###sois===10413###eois===10427###lif===5###soif===116###eoif===130###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/wait_child(int,void*)") && (AKA_mark("lis===443###sois===10413###eois===10427###lif===5###soif===116###eoif===130###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/wait_child(int,void*)")&&pidchld == pid)) {
				AKA_mark("lis===444###sois===10433###eois===10446###lif===6###soif===136###eoif===149###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/wait_child(int,void*)");childpid = 0;

				if (AKA_mark("lis===445###sois===10453###eois===10465###lif===7###soif===156###eoif===168###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/wait_child(int,void*)") && (AKA_mark("lis===445###sois===10453###eois===10465###lif===7###soif===156###eoif===168###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/wait_child(int,void*)")&&!SELF_PGROUP)) {
			AKA_mark("lis===446###sois===10470###eois===10495###lif===8###soif===173###eoif===198###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/wait_child(int,void*)");killpg(pidchld, SIGKILL);
		}
		else {AKA_mark("lis===-445-###sois===-10453-###eois===-1045312-###lif===-7-###soif===-###eoif===-168-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/wait_child(int,void*)");}

				AKA_mark("lis===447###sois===10498###eois===10524###lif===9###soif===201###eoif===227###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/wait_child(int,void*)");waitpid(pidchld, NULL, 0);

				AKA_mark("lis===448###sois===10527###eois===10550###lif===10###soif===230###eoif===253###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/wait_child(int,void*)");jobs_exit(exit_at_end);

	}
	else {
				AKA_mark("lis===450###sois===10563###eois===10585###lif===12###soif===266###eoif===288###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/wait_child(int,void*)");waitpid(pid, NULL, 0);

	}

}

/** Instrumented function on_sigchld(int,siginfo_t*,void*) */
static void on_sigchld(int signum, siginfo_t *info, void *uctx)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/on_sigchld(int,siginfo_t*,void*)");AKA_fCall++;
		if (AKA_mark("lis===456###sois===10663###eois===10687###lif===2###soif===71###eoif===95###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/on_sigchld(int,siginfo_t*,void*)") && (AKA_mark("lis===456###sois===10663###eois===10687###lif===2###soif===71###eoif===95###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/on_sigchld(int,siginfo_t*,void*)")&&info->si_pid == childpid)) {
				AKA_mark("lis===457###sois===10701###eois===10714###lif===3###soif===109###eoif===122###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/on_sigchld(int,siginfo_t*,void*)");switch(info->si_code){
					case CLD_EXITED: if(info->si_code == CLD_EXITED)AKA_mark("lis===458###sois===10720###eois===10736###lif===4###soif===128###eoif===144###function===./app-framework-binder/src/main-afb-daemon.c/on_sigchld(int,siginfo_t*,void*)");

					case CLD_KILLED: if(info->si_code == CLD_KILLED)AKA_mark("lis===459###sois===10739###eois===10755###lif===5###soif===147###eoif===163###function===./app-framework-binder/src/main-afb-daemon.c/on_sigchld(int,siginfo_t*,void*)");

					case CLD_DUMPED: if(info->si_code == CLD_DUMPED)AKA_mark("lis===460###sois===10758###eois===10774###lif===6###soif===166###eoif===182###function===./app-framework-binder/src/main-afb-daemon.c/on_sigchld(int,siginfo_t*,void*)");

						AKA_mark("lis===461###sois===10778###eois===10843###lif===7###soif===186###eoif===251###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/on_sigchld(int,siginfo_t*,void*)");jobs_queue_lazy(0, 0, wait_child, (void*)(intptr_t)info->si_pid);

					default: if(info->si_code != CLD_EXITED && info->si_code != CLD_KILLED && info->si_code != CLD_DUMPED)AKA_mark("lis===462###sois===10846###eois===10854###lif===8###soif===254###eoif===262###function===./app-framework-binder/src/main-afb-daemon.c/on_sigchld(int,siginfo_t*,void*)");

						AKA_mark("lis===463###sois===10858###eois===10864###lif===9###soif===266###eoif===272###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/on_sigchld(int,siginfo_t*,void*)");break;

		}

	}
	else {AKA_mark("lis===-456-###sois===-10663-###eois===-1066324-###lif===-2-###soif===-###eoif===-95-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/on_sigchld(int,siginfo_t*,void*)");}

}

/*
# @@ @
# @p port
# @t token
*/

#define SUBST_CHAR  '@'
#define SUBST_STR   "@"

/** Instrumented function instanciate_string(char*,const char*,const char*) */
static char *instanciate_string(char *arg, const char *port, const char *token)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===479###sois===11042###eois===11066###lif===2###soif===83###eoif===107###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");char *resu, *it, *wr, c;

		AKA_mark("lis===480###sois===11068###eois===11081###lif===3###soif===109###eoif===122###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");int chg, dif;


	/* get the changes */
		AKA_mark("lis===483###sois===11107###eois===11115###lif===6###soif===148###eoif===156###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");chg = 0;

		AKA_mark("lis===484###sois===11117###eois===11125###lif===7###soif===158###eoif===166###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");dif = 0;

		AKA_mark("lis===485###sois===11127###eois===11159###lif===8###soif===168###eoif===200###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");it = strchrnul(arg, SUBST_CHAR);

		while (AKA_mark("lis===486###sois===11168###eois===11171###lif===9###soif===209###eoif===212###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)") && (AKA_mark("lis===486###sois===11168###eois===11171###lif===9###soif===209###eoif===212###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)")&&*it)) {
				AKA_mark("lis===487###sois===11177###eois===11187###lif===10###soif===218###eoif===228###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");c = *++it;

				if (AKA_mark("lis===488###sois===11194###eois===11210###lif===11###soif===235###eoif===251###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)") && ((AKA_mark("lis===488###sois===11194###eois===11202###lif===11###soif===235###eoif===243###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)")&&c == 'p')	&&(AKA_mark("lis===488###sois===11206###eois===11210###lif===11###soif===247###eoif===251###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)")&&port))) {
						AKA_mark("lis===489###sois===11217###eois===11223###lif===12###soif===258###eoif===264###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");chg++;

						AKA_mark("lis===490###sois===11227###eois===11256###lif===13###soif===268###eoif===297###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");dif += (int)strlen(port) - 2;

		}
		else {
			if (AKA_mark("lis===491###sois===11270###eois===11287###lif===14###soif===311###eoif===328###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)") && ((AKA_mark("lis===491###sois===11270###eois===11278###lif===14###soif===311###eoif===319###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)")&&c == 't')	&&(AKA_mark("lis===491###sois===11282###eois===11287###lif===14###soif===323###eoif===328###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)")&&token))) {
							AKA_mark("lis===492###sois===11294###eois===11300###lif===15###soif===335###eoif===341###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");chg++;

							AKA_mark("lis===493###sois===11304###eois===11334###lif===16###soif===345###eoif===375###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");dif += (int)strlen(token) - 2;

		}
			else {
				if (AKA_mark("lis===494###sois===11348###eois===11363###lif===17###soif===389###eoif===404###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)") && (AKA_mark("lis===494###sois===11348###eois===11363###lif===17###soif===389###eoif===404###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)")&&c == SUBST_CHAR)) {
								AKA_mark("lis===495###sois===11370###eois===11375###lif===18###soif===411###eoif===416###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");it++;

								AKA_mark("lis===496###sois===11379###eois===11385###lif===19###soif===420###eoif===426###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");chg++;

								AKA_mark("lis===497###sois===11389###eois===11395###lif===20###soif===430###eoif===436###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");dif--;

		}
				else {AKA_mark("lis===-494-###sois===-11348-###eois===-1134815-###lif===-17-###soif===-###eoif===-404-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");}
			}
		}

				AKA_mark("lis===499###sois===11402###eois===11433###lif===22###soif===443###eoif===474###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");it = strchrnul(it, SUBST_CHAR);

	}


	/* return arg when no change */
		if (AKA_mark("lis===503###sois===11476###eois===11480###lif===26###soif===517###eoif===521###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)") && (AKA_mark("lis===503###sois===11476###eois===11480###lif===26###soif===517###eoif===521###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)")&&!chg)) {
		AKA_mark("lis===504###sois===11484###eois===11495###lif===27###soif===525###eoif===536###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");return arg;
	}
	else {AKA_mark("lis===-503-###sois===-11476-###eois===-114764-###lif===-26-###soif===-###eoif===-521-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");}


	/* allocates the result */
		AKA_mark("lis===507###sois===11526###eois===11562###lif===30###soif===567###eoif===603###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");resu = malloc((it - arg) + dif + 1);

		if (AKA_mark("lis===508###sois===11568###eois===11573###lif===31###soif===609###eoif===614###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)") && (AKA_mark("lis===508###sois===11568###eois===11573###lif===31###soif===609###eoif===614###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)")&&!resu)) {
				AKA_mark("lis===509###sois===11579###eois===11602###lif===32###soif===620###eoif===643###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");ERROR("out of memory");

				AKA_mark("lis===510###sois===11605###eois===11617###lif===33###soif===646###eoif===658###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");return NULL;

	}
	else {AKA_mark("lis===-508-###sois===-11568-###eois===-115685-###lif===-31-###soif===-###eoif===-614-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");}


	/* instanciate the arguments */
		AKA_mark("lis===514###sois===11656###eois===11666###lif===37###soif===697###eoif===707###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");wr = resu;

		for (;;) {
				AKA_mark("lis===516###sois===11681###eois===11713###lif===39###soif===722###eoif===754###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");it = strchrnul(arg, SUBST_CHAR);

				AKA_mark("lis===517###sois===11716###eois===11748###lif===40###soif===757###eoif===789###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");wr = mempcpy(wr, arg, it - arg);

				if (AKA_mark("lis===518###sois===11755###eois===11759###lif===41###soif===796###eoif===800###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)") && (AKA_mark("lis===518###sois===11755###eois===11759###lif===41###soif===796###eoif===800###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)")&&!*it)) {
			AKA_mark("lis===519###sois===11764###eois===11770###lif===42###soif===805###eoif===811###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");break;
		}
		else {AKA_mark("lis===-518-###sois===-11755-###eois===-117554-###lif===-41-###soif===-###eoif===-800-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");}

				AKA_mark("lis===520###sois===11773###eois===11783###lif===43###soif===814###eoif===824###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");c = *++it;

				if (AKA_mark("lis===521###sois===11790###eois===11806###lif===44###soif===831###eoif===847###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)") && ((AKA_mark("lis===521###sois===11790###eois===11798###lif===44###soif===831###eoif===839###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)")&&c == 'p')	&&(AKA_mark("lis===521###sois===11802###eois===11806###lif===44###soif===843###eoif===847###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)")&&port))) {
			AKA_mark("lis===522###sois===11811###eois===11833###lif===45###soif===852###eoif===874###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");wr = stpcpy(wr, port);
		}
		else {
			if (AKA_mark("lis===523###sois===11845###eois===11862###lif===46###soif===886###eoif===903###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)") && ((AKA_mark("lis===523###sois===11845###eois===11853###lif===46###soif===886###eoif===894###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)")&&c == 't')	&&(AKA_mark("lis===523###sois===11857###eois===11862###lif===46###soif===898###eoif===903###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)")&&token))) {
				AKA_mark("lis===524###sois===11867###eois===11890###lif===47###soif===908###eoif===931###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");wr = stpcpy(wr, token);
			}
			else {
							if (AKA_mark("lis===526###sois===11907###eois===11922###lif===49###soif===948###eoif===963###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)") && (AKA_mark("lis===526###sois===11907###eois===11922###lif===49###soif===948###eoif===963###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)")&&c != SUBST_CHAR)) {
					AKA_mark("lis===527###sois===11928###eois===11947###lif===50###soif===969###eoif===988###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");*wr++ = SUBST_CHAR;
				}
				else {AKA_mark("lis===-526-###sois===-11907-###eois===-1190715-###lif===-49-###soif===-###eoif===-963-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");}

							AKA_mark("lis===528###sois===11951###eois===11963###lif===51###soif===992###eoif===1004###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");*wr++ = *it;

		}
		}

				AKA_mark("lis===530###sois===11970###eois===11981###lif===53###soif===1011###eoif===1022###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");arg = ++it;

	}


		AKA_mark("lis===533###sois===11987###eois===11995###lif===56###soif===1028###eoif===1036###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");*wr = 0;

		AKA_mark("lis===534###sois===11997###eois===12009###lif===57###soif===1038###eoif===1050###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_string(char*,const char*,const char*)");return resu;

}

/** Instrumented function instanciate_environ(const char*,const char*) */
static int instanciate_environ(const char *port, const char *token)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===539###sois===12084###eois===12106###lif===2###soif===71###eoif===93###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)");extern char **environ;

		AKA_mark("lis===540###sois===12108###eois===12119###lif===3###soif===95###eoif===106###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)");char *repl;

		AKA_mark("lis===541###sois===12121###eois===12127###lif===4###soif===108###eoif===114###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)");int i;


	/* instantiate the environment */
		AKA_mark("lis===544###sois===12170###eois===12177###lif===7###soif===157###eoif===164###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)");for (i = 0 ;AKA_mark("lis===544###sois===12178###eois===12188###lif===7###soif===165###eoif===175###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)") && AKA_mark("lis===544###sois===12178###eois===12188###lif===7###soif===165###eoif===175###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)")&&environ[i];({AKA_mark("lis===544###sois===12191###eois===12194###lif===7###soif===178###eoif===181###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)");i++;})) {
				AKA_mark("lis===545###sois===12200###eois===12251###lif===8###soif===187###eoif===238###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)");repl = instanciate_string(environ[i], port, token);

				if (AKA_mark("lis===546###sois===12258###eois===12263###lif===9###soif===245###eoif===250###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)") && (AKA_mark("lis===546###sois===12258###eois===12263###lif===9###soif===245###eoif===250###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)")&&!repl)) {
			AKA_mark("lis===547###sois===12268###eois===12278###lif===10###soif===255###eoif===265###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)");return -1;
		}
		else {AKA_mark("lis===-546-###sois===-12258-###eois===-122585-###lif===-9-###soif===-###eoif===-250-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)");}

				AKA_mark("lis===548###sois===12281###eois===12299###lif===11###soif===268###eoif===286###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)");environ[i] = repl;

	}

		AKA_mark("lis===550###sois===12304###eois===12313###lif===13###soif===291###eoif===300###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_environ(const char*,const char*)");return 0;

}

/** Instrumented function instanciate_command_args(struct json_object*,const char*,const char*) */
static char **instanciate_command_args(struct json_object *exec, const char *port, const char *token)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===555###sois===12422###eois===12436###lif===2###soif===105###eoif===119###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");char **result;

		AKA_mark("lis===556###sois===12438###eois===12456###lif===3###soif===121###eoif===139###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");char *repl, *item;

		AKA_mark("lis===557###sois===12458###eois===12467###lif===4###soif===141###eoif===150###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");int i, n;


	/* allocates the result */
		AKA_mark("lis===560###sois===12498###eois===12538###lif===7###soif===181###eoif===221###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");n = (int)json_object_array_length(exec);

		AKA_mark("lis===561###sois===12540###eois===12583###lif===8###soif===223###eoif===266###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");result = malloc((n + 1) * sizeof * result);

		if (AKA_mark("lis===562###sois===12589###eois===12596###lif===9###soif===272###eoif===279###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)") && (AKA_mark("lis===562###sois===12589###eois===12596###lif===9###soif===272###eoif===279###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)")&&!result)) {
				AKA_mark("lis===563###sois===12602###eois===12625###lif===10###soif===285###eoif===308###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");ERROR("out of memory");

				AKA_mark("lis===564###sois===12628###eois===12640###lif===11###soif===311###eoif===323###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");return NULL;

	}
	else {AKA_mark("lis===-562-###sois===-12589-###eois===-125897-###lif===-9-###soif===-###eoif===-279-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");}


	/* instanciate the arguments */
		AKA_mark("lis===568###sois===12684###eois===12691###lif===15###soif===367###eoif===374###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");for (i = 0 ;AKA_mark("lis===568###sois===12692###eois===12697###lif===15###soif===375###eoif===380###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)") && AKA_mark("lis===568###sois===12692###eois===12697###lif===15###soif===375###eoif===380###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)")&&i < n;({AKA_mark("lis===568###sois===12700###eois===12703###lif===15###soif===383###eoif===386###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");i++;})) {
				AKA_mark("lis===569###sois===12709###eois===12782###lif===16###soif===392###eoif===465###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");item = (char*)json_object_get_string(json_object_array_get_idx(exec, i));

				AKA_mark("lis===570###sois===12785###eois===12830###lif===17###soif===468###eoif===513###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");repl = instanciate_string(item, port, token);

				if (AKA_mark("lis===571###sois===12837###eois===12842###lif===18###soif===520###eoif===525###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)") && (AKA_mark("lis===571###sois===12837###eois===12842###lif===18###soif===520###eoif===525###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)")&&!repl)) {
						AKA_mark("lis===572###sois===12849###eois===12862###lif===19###soif===532###eoif===545###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");free(result);

						AKA_mark("lis===573###sois===12866###eois===12878###lif===20###soif===549###eoif===561###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");return NULL;

		}
		else {AKA_mark("lis===-571-###sois===-12837-###eois===-128375-###lif===-18-###soif===-###eoif===-525-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");}

				AKA_mark("lis===575###sois===12885###eois===12902###lif===22###soif===568###eoif===585###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");result[i] = repl;

	}

		AKA_mark("lis===577###sois===12907###eois===12924###lif===24###soif===590###eoif===607###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");result[i] = NULL;

		AKA_mark("lis===578###sois===12926###eois===12940###lif===25###soif===609###eoif===623###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/instanciate_command_args(struct json_object*,const char*,const char*)");return result;

}

/** Instrumented function execute_command() */
static int execute_command()
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/execute_command()");AKA_fCall++;
		AKA_mark("lis===583###sois===12976###eois===13016###lif===2###soif===32###eoif===72###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");struct json_object *exec, *oport, *otok;

		AKA_mark("lis===584###sois===13018###eois===13040###lif===3###soif===74###eoif===96###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");struct sigaction siga;

		AKA_mark("lis===585###sois===13042###eois===13067###lif===4###soif===98###eoif===123###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");const char *token, *port;

		AKA_mark("lis===586###sois===13069###eois===13081###lif===5###soif===125###eoif===137###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");char **args;


	/* check whether a command is to execute or not */
		if (AKA_mark("lis===589###sois===13140###eois===13194###lif===8###soif===196###eoif===250###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()") && (AKA_mark("lis===589###sois===13140###eois===13194###lif===8###soif===196###eoif===250###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()")&&!json_object_object_get_ex(main_config, "exec", &exec))) {
		AKA_mark("lis===590###sois===13198###eois===13207###lif===9###soif===254###eoif===263###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");return 0;
	}
	else {AKA_mark("lis===-589-###sois===-13140-###eois===-1314054-###lif===-8-###soif===-###eoif===-250-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");}


		if (AKA_mark("lis===592###sois===13214###eois===13225###lif===11###soif===270###eoif===281###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()") && (AKA_mark("lis===592###sois===13214###eois===13225###lif===11###soif===270###eoif===281###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()")&&SELF_PGROUP)) {
		AKA_mark("lis===593###sois===13229###eois===13243###lif===12###soif===285###eoif===299###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");setpgid(0, 0);
	}
	else {AKA_mark("lis===-592-###sois===-13214-###eois===-1321411-###lif===-11-###soif===-###eoif===-281-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");}


	/* install signal handler */
		AKA_mark("lis===596###sois===13276###eois===13306###lif===15###soif===332###eoif===362###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");memset(&siga, 0, sizeof siga);

		AKA_mark("lis===597###sois===13308###eois===13339###lif===16###soif===364###eoif===395###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");siga.sa_sigaction = on_sigchld;

		AKA_mark("lis===598###sois===13341###eois===13368###lif===17###soif===397###eoif===424###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");siga.sa_flags = SA_SIGINFO;

		AKA_mark("lis===599###sois===13370###eois===13402###lif===18###soif===426###eoif===458###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");sigaction(SIGCHLD, &siga, NULL);


	/* fork now */
		AKA_mark("lis===602###sois===13421###eois===13439###lif===21###soif===477###eoif===495###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");childpid = fork();

		if (AKA_mark("lis===603###sois===13445###eois===13453###lif===22###soif===501###eoif===509###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()") && (AKA_mark("lis===603###sois===13445###eois===13453###lif===22###soif===501###eoif===509###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()")&&childpid)) {
		AKA_mark("lis===604###sois===13457###eois===13466###lif===23###soif===513###eoif===522###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");return 0;
	}
	else {AKA_mark("lis===-603-###sois===-13445-###eois===-134458-###lif===-22-###soif===-###eoif===-509-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");}


	/* compute the string for port */
		if (AKA_mark("lis===607###sois===13508###eois===13562###lif===26###soif===564###eoif===618###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()") && (AKA_mark("lis===607###sois===13508###eois===13562###lif===26###soif===564###eoif===618###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()")&&json_object_object_get_ex(main_config, "port", &oport))) {
		AKA_mark("lis===608###sois===13566###eois===13603###lif===27###soif===622###eoif===659###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");port = json_object_get_string(oport);
	}
	else {
		AKA_mark("lis===610###sois===13612###eois===13621###lif===29###soif===668###eoif===677###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");port = 0;
	}

	/* instantiate arguments and environment */
		if (AKA_mark("lis===612###sois===13672###eois===13726###lif===31###soif===728###eoif===782###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()") && (AKA_mark("lis===612###sois===13672###eois===13726###lif===31###soif===728###eoif===782###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()")&&json_object_object_get_ex(main_config, "token", &otok))) {
		AKA_mark("lis===613###sois===13730###eois===13767###lif===32###soif===786###eoif===823###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");token = json_object_get_string(otok);
	}
	else {
		AKA_mark("lis===615###sois===13776###eois===13786###lif===34###soif===832###eoif===842###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");token = 0;
	}

		AKA_mark("lis===616###sois===13788###eois===13839###lif===35###soif===844###eoif===895###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");args = instanciate_command_args(exec, port, token);

		if (AKA_mark("lis===617###sois===13845###eois===13890###lif===36###soif===901###eoif===946###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()") && ((AKA_mark("lis===617###sois===13845###eois===13849###lif===36###soif===901###eoif===905###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()")&&args)	&&(AKA_mark("lis===617###sois===13853###eois===13890###lif===36###soif===909###eoif===946###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()")&&instanciate_environ(port, token) >= 0))) {
		/* run */
				if (AKA_mark("lis===619###sois===13912###eois===13924###lif===38###soif===968###eoif===980###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()") && (AKA_mark("lis===619###sois===13912###eois===13924###lif===38###soif===968###eoif===980###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()")&&!SELF_PGROUP)) {
			AKA_mark("lis===620###sois===13929###eois===13943###lif===39###soif===985###eoif===999###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");setpgid(0, 0);
		}
		else {AKA_mark("lis===-619-###sois===-13912-###eois===-1391212-###lif===-38-###soif===-###eoif===-980-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");}

				AKA_mark("lis===621###sois===13946###eois===13967###lif===40###soif===1002###eoif===1023###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");execv(args[0], args);

				AKA_mark("lis===622###sois===13970###eois===14008###lif===41###soif===1026###eoif===1064###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");ERROR("can't launch %s: %m", args[0]);

	}
	else {AKA_mark("lis===-617-###sois===-13845-###eois===-1384545-###lif===-36-###soif===-###eoif===-946-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");}

		AKA_mark("lis===624###sois===14013###eois===14021###lif===43###soif===1069###eoif===1077###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");exit(1);

		AKA_mark("lis===625###sois===14023###eois===14033###lif===44###soif===1079###eoif===1089###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/execute_command()");return -1;

}

/*---------------------------------------------------------
 | startup calls
 +--------------------------------------------------------- */

struct startup_req
{
	struct afb_xreq xreq;
	char *api;
	char *verb;
	struct json_object *calls;
	int index;
	int count;
	const char *callspec;
	struct afb_session *session;
};

/** Instrumented function startup_call_reply(struct afb_xreq*,struct json_object*,const char*,const char*) */
static void startup_call_reply(struct afb_xreq *xreq, struct json_object *object, const char *error, const char *info)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/startup_call_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===646###sois===14478###eois===14496###lif===2###soif===122###eoif===140###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");struct startup_req *sreq = CONTAINER_OF_XREQ(struct startup_req, xreq);

		AKA_mark("lis===648###sois===14552###eois===14570###lif===4###soif===196###eoif===214###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");info = info ?: "";

		if (AKA_mark("lis===649###sois===14576###eois===14582###lif===5###soif===220###eoif===226###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_reply(struct afb_xreq*,struct json_object*,const char*,const char*)") && (AKA_mark("lis===649###sois===14576###eois===14582###lif===5###soif===220###eoif===226###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_reply(struct afb_xreq*,struct json_object*,const char*,const char*)")&&!error)) {
				AKA_mark("lis===650###sois===14588###eois===14685###lif===6###soif===232###eoif===329###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");NOTICE("startup call %s returned %s (%s)", sreq->callspec, json_object_get_string(object), info);

				AKA_mark("lis===651###sois===14688###eois===14712###lif===7###soif===332###eoif===356###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");json_object_put(object);

	}
	else {
				AKA_mark("lis===653###sois===14725###eois===14794###lif===9###soif===369###eoif===438###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");ERROR("startup call %s ERROR! %s (%s)", sreq->callspec, error, info);

				AKA_mark("lis===654###sois===14797###eois===14805###lif===10###soif===441###eoif===449###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");exit(1);

	}

}

static void startup_call_current(struct startup_req *sreq);

/** Instrumented function startup_call_unref(struct afb_xreq*) */
static void startup_call_unref(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/startup_call_unref(struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===662###sois===14930###eois===14948###lif===2###soif===57###eoif===75###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");struct startup_req *sreq = CONTAINER_OF_XREQ(struct startup_req, xreq);

		AKA_mark("lis===664###sois===15004###eois===15020###lif===4###soif===131###eoif===147###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_unref(struct afb_xreq*)");free(sreq->api);

		AKA_mark("lis===665###sois===15022###eois===15039###lif===5###soif===149###eoif===166###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_unref(struct afb_xreq*)");free(sreq->verb);

		AKA_mark("lis===666###sois===15041###eois===15074###lif===6###soif===168###eoif===201###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_unref(struct afb_xreq*)");json_object_put(sreq->xreq.json);

		if (AKA_mark("lis===667###sois===15080###eois===15107###lif===7###soif===207###eoif===234###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_unref(struct afb_xreq*)") && (AKA_mark("lis===667###sois===15080###eois===15107###lif===7###soif===207###eoif===234###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_unref(struct afb_xreq*)")&&++sreq->index < sreq->count)) {
		AKA_mark("lis===668###sois===15111###eois===15138###lif===8###soif===238###eoif===265###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_unref(struct afb_xreq*)");startup_call_current(sreq);
	}
	else {
				AKA_mark("lis===670###sois===15149###eois===15182###lif===10###soif===276###eoif===309###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_unref(struct afb_xreq*)");afb_session_close(sreq->session);

				AKA_mark("lis===671###sois===15185###eois===15218###lif===11###soif===312###eoif===345###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_unref(struct afb_xreq*)");afb_session_unref(sreq->session);

				AKA_mark("lis===672###sois===15221###eois===15232###lif===12###soif===348###eoif===359###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_unref(struct afb_xreq*)");free(sreq);

	}

}

static struct afb_xreq_query_itf startup_xreq_itf =
{
	.reply = startup_call_reply,
	.unref = startup_call_unref
};

/** Instrumented function startup_call_current(struct startup_req*) */
static void startup_call_current(struct startup_req *sreq)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");AKA_fCall++;
		AKA_mark("lis===684###sois===15418###eois===15448###lif===2###soif===62###eoif===92###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");const char *api, *verb, *json;

		AKA_mark("lis===685###sois===15450###eois===15479###lif===3###soif===94###eoif===123###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");enum json_tokener_error jerr;


		AKA_mark("lis===687###sois===15482###eois===15598###lif===5###soif===126###eoif===242###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");sreq->callspec = json_object_get_string(json_object_array_get_idx(sreq->calls, sreq->index)),
	api = sreq->callspec;

		AKA_mark("lis===689###sois===15600###eois===15624###lif===7###soif===244###eoif===268###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");verb = strchr(api, '/');

		if (AKA_mark("lis===690###sois===15630###eois===15634###lif===8###soif===274###eoif===278###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)") && (AKA_mark("lis===690###sois===15630###eois===15634###lif===8###soif===274###eoif===278###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)")&&verb)) {
				AKA_mark("lis===691###sois===15640###eois===15665###lif===9###soif===284###eoif===309###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");json = strchr(verb, ':');

				if (AKA_mark("lis===692###sois===15672###eois===15676###lif===10###soif===316###eoif===320###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)") && (AKA_mark("lis===692###sois===15672###eois===15676###lif===10###soif===316###eoif===320###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)")&&json)) {
						AKA_mark("lis===693###sois===15683###eois===15729###lif===11###soif===327###eoif===373###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");afb_xreq_init(&sreq->xreq, &startup_xreq_itf);

						AKA_mark("lis===694###sois===15733###eois===15808###lif===12###soif===377###eoif===452###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");afb_context_init_validated(&sreq->xreq.context, sreq->session, NULL, NULL);

						AKA_mark("lis===695###sois===15812###eois===15849###lif===13###soif===456###eoif===493###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");sreq->api = strndup(api, verb - api);

						AKA_mark("lis===696###sois===15853###eois===15901###lif===14###soif===497###eoif===545###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");sreq->verb = strndup(verb + 1, json - verb - 1);

						AKA_mark("lis===697###sois===15905###eois===15947###lif===15###soif===549###eoif===591###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");sreq->xreq.request.called_api = sreq->api;

						AKA_mark("lis===698###sois===15951###eois===15995###lif===16###soif===595###eoif===639###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");sreq->xreq.request.called_verb = sreq->verb;

						AKA_mark("lis===699###sois===15999###eois===16061###lif===17###soif===643###eoif===705###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");sreq->xreq.json = json_tokener_parse_verbose(json + 1, &jerr);

						if (AKA_mark("lis===700###sois===16069###eois===16124###lif===18###soif===713###eoif===768###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)") && (((AKA_mark("lis===700###sois===16069###eois===16078###lif===18###soif===713###eoif===722###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)")&&sreq->api)	&&(AKA_mark("lis===700###sois===16082###eois===16092###lif===18###soif===726###eoif===736###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)")&&sreq->verb))	&&(AKA_mark("lis===700###sois===16096###eois===16124###lif===18###soif===740###eoif===768###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)")&&jerr == json_tokener_success))) {
								AKA_mark("lis===701###sois===16132###eois===16175###lif===19###soif===776###eoif===819###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");afb_xreq_process(&sreq->xreq, main_apiset);

								AKA_mark("lis===702###sois===16180###eois===16187###lif===20###soif===824###eoif===831###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");return;

			}
			else {AKA_mark("lis===-700-###sois===-16069-###eois===-1606955-###lif===-18-###soif===-###eoif===-768-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");}

		}
		else {AKA_mark("lis===-692-###sois===-15672-###eois===-156724-###lif===-10-###soif===-###eoif===-320-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");}

	}
	else {AKA_mark("lis===-690-###sois===-15630-###eois===-156304-###lif===-8-###soif===-###eoif===-278-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");}

		AKA_mark("lis===706###sois===16201###eois===16252###lif===24###soif===845###eoif===896###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");ERROR("Bad call specification %s", sreq->callspec);

		AKA_mark("lis===707###sois===16254###eois===16262###lif===25###soif===898###eoif===906###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/startup_call_current(struct startup_req*)");exit(1);

}

/** Instrumented function run_startup_calls() */
static void run_startup_calls()
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()");AKA_fCall++;
		AKA_mark("lis===712###sois===16301###eois===16327###lif===2###soif===35###eoif===61###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()");struct json_object *calls;

		AKA_mark("lis===713###sois===16329###eois===16354###lif===3###soif===63###eoif===88###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()");struct startup_req *sreq;

		AKA_mark("lis===714###sois===16356###eois===16366###lif===4###soif===90###eoif===100###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()");int count;


		if (AKA_mark("lis===716###sois===16373###eois===16528###lif===6###soif===107###eoif===262###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()") && (((AKA_mark("lis===716###sois===16373###eois===16427###lif===6###soif===107###eoif===161###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()")&&json_object_object_get_ex(main_config, "call", &calls))	&&(AKA_mark("lis===717###sois===16433###eois===16476###lif===7###soif===167###eoif===210###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()")&&json_object_is_type(calls, json_type_array)))	&&(count=((int)json_object_array_length(calls)) && AKA_mark("lis===718###sois===16483###eois===16527###lif===8###soif===217###eoif===261###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()")))) {
				AKA_mark("lis===719###sois===16534###eois===16565###lif===9###soif===268###eoif===299###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()");sreq = calloc(1, sizeof *sreq);

				AKA_mark("lis===720###sois===16568###eois===16609###lif===10###soif===302###eoif===343###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()");sreq->session = afb_session_create(3600);

				AKA_mark("lis===721###sois===16612###eois===16632###lif===11###soif===346###eoif===366###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()");sreq->calls = calls;

				AKA_mark("lis===722###sois===16635###eois===16651###lif===12###soif===369###eoif===385###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()");sreq->index = 0;

				AKA_mark("lis===723###sois===16654###eois===16674###lif===13###soif===388###eoif===408###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()");sreq->count = count;

				AKA_mark("lis===724###sois===16677###eois===16704###lif===14###soif===411###eoif===438###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()");startup_call_current(sreq);

	}
	else {AKA_mark("lis===-716-###sois===-16373-###eois===-16373155-###lif===-6-###soif===-###eoif===-262-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/run_startup_calls()");}

}

/*---------------------------------------------------------
 | job for starting the daemon
 +--------------------------------------------------------- */

/** Instrumented function start(int,void*) */
static void start(int signum, void *arg)
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");AKA_fCall++;
#if WITH_AFB_HOOK
	const char *tracereq = NULL, *traceapi = NULL, *traceevt = NULL,
#if !defined(REMOVE_LEGACY_TRACE)
		*tracesvc = NULL, *traceditf = NULL,
#endif
		*traceses = NULL, *traceglob = NULL;
#endif
		AKA_mark("lis===741###sois===17120###eois===17168###lif===9###soif===254###eoif===302###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");const char *workdir, *rootdir, *token, *rootapi;

		AKA_mark("lis===742###sois===17170###eois===17199###lif===10###soif===304###eoif===333###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");struct json_object *settings;

		AKA_mark("lis===743###sois===17201###eois===17223###lif===11###soif===335###eoif===357###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");struct afb_hsrv *hsrv;

		AKA_mark("lis===744###sois===17225###eois===17277###lif===12###soif===359###eoif===411###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");int max_session_count, session_timeout, api_timeout;

		AKA_mark("lis===745###sois===17279###eois===17303###lif===13###soif===413###eoif===437###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");int no_httpd, http_port;

		AKA_mark("lis===746###sois===17305###eois===17312###lif===14###soif===439###eoif===446###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");int rc;



		AKA_mark("lis===749###sois===17316###eois===17341###lif===17###soif===450###eoif===475###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");afb_debug("start-entry");


		if (AKA_mark("lis===751###sois===17348###eois===17354###lif===19###soif===482###eoif===488###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===751###sois===17348###eois===17354###lif===19###soif===482###eoif===488###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&signum)) {
				AKA_mark("lis===752###sois===17360###eois===17422###lif===20###soif===494###eoif===556###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");ERROR("start aborted: received signal %s", strsignal(signum));

				AKA_mark("lis===753###sois===17425###eois===17433###lif===21###soif===559###eoif===567###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");exit(1);

	}
	else {AKA_mark("lis===-751-###sois===-17348-###eois===-173486-###lif===-19-###soif===-###eoif===-488-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}


		AKA_mark("lis===756###sois===17439###eois===17455###lif===24###soif===573###eoif===589###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");settings = NULL;

		AKA_mark("lis===757###sois===17457###eois===17470###lif===25###soif===591###eoif===604###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");no_httpd = 0;

		AKA_mark("lis===758###sois===17472###eois===17487###lif===26###soif===606###eoif===621###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");http_port = -1;

		AKA_mark("lis===759###sois===17489###eois===17512###lif===27###soif===623###eoif===646###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");rootapi = token = NULL;

		AKA_mark("lis===760###sois===17514###eois===17891###lif===28###soif===648###eoif===1025###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");rc = wrap_json_unpack(main_config, "{"
			"ss ss s?s"
			"si si si"
			"s?b s?i s?s"
			"s?o"
			"}",

			"rootdir", &rootdir,
			"workdir", &workdir,
			"token", &token,

			"apitimeout", &api_timeout,
			"cntxtimeout", &session_timeout,
			"session-max", &max_session_count,

			"no-httpd", &no_httpd,
			"port", &http_port,
			"rootapi", &rootapi,

			"set", &settings
			);

		if (AKA_mark("lis===781###sois===17897###eois===17903###lif===49###soif===1031###eoif===1037###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===781###sois===17897###eois===17903###lif===49###soif===1031###eoif===1037###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&rc < 0)) {
				AKA_mark("lis===782###sois===17909###eois===17945###lif===50###soif===1043###eoif===1079###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");ERROR("Unable to get start config");

				AKA_mark("lis===783###sois===17948###eois===17956###lif===51###soif===1082###eoif===1090###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");exit(1);

	}
	else {AKA_mark("lis===-781-###sois===-17897-###eois===-178976-###lif===-49-###soif===-###eoif===-1037-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}


#if WITH_AFB_HOOK
	rc = wrap_json_unpack(main_config, "{"
#if !defined(REMOVE_LEGACY_TRACE)
			"s?s s?s"
#endif
			"s?s s?s s?s s?s s?s"
			"}",

#if !defined(REMOVE_LEGACY_TRACE)
			"tracesvc", &tracesvc,
			"traceditf", &traceditf,
#endif
			"tracereq", &tracereq,
			"traceapi", &traceapi,
			"traceevt", &traceevt,
			"traceses",  &traceses,
			"traceglob", &traceglob
			);
	if (rc < 0) {
		ERROR("Unable to get hook config");
		exit(1);
	}
#endif

	/* initialize session handling */
		if (AKA_mark("lis===811###sois===18455###eois===18507###lif===79###soif===1589###eoif===1641###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===811###sois===18455###eois===18507###lif===79###soif===1589###eoif===1641###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&afb_session_init(max_session_count, session_timeout))) {
				AKA_mark("lis===812###sois===18513###eois===18563###lif===80###soif===1647###eoif===1697###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");ERROR("initialisation of session manager failed");

				AKA_mark("lis===813###sois===18566###eois===18577###lif===81###soif===1700###eoif===1711###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");goto error;

	}
	else {AKA_mark("lis===-811-###sois===-18455-###eois===-1845552-###lif===-79-###soif===-###eoif===-1641-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}


	/* set the directories */
		AKA_mark("lis===817###sois===18610###eois===18654###lif===85###soif===1744###eoif===1788###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");mkdir(workdir, S_IRWXU | S_IRGRP | S_IXGRP);

		if (AKA_mark("lis===818###sois===18660###eois===18678###lif===86###soif===1794###eoif===1812###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===818###sois===18660###eois===18678###lif===86###soif===1794###eoif===1812###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&chdir(workdir) < 0)) {
				AKA_mark("lis===819###sois===18684###eois===18729###lif===87###soif===1818###eoif===1863###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");ERROR("Can't enter working dir %s", workdir);

				AKA_mark("lis===820###sois===18732###eois===18743###lif===88###soif===1866###eoif===1877###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");goto error;

	}
	else {AKA_mark("lis===-818-###sois===-18660-###eois===-1866018-###lif===-86-###soif===-###eoif===-1812-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}

		if (AKA_mark("lis===822###sois===18752###eois===18787###lif===90###soif===1886###eoif===1921###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===822###sois===18752###eois===18787###lif===90###soif===1886###eoif===1921###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&afb_common_rootdir_set(rootdir) < 0)) {
				AKA_mark("lis===823###sois===18793###eois===18850###lif===91###soif===1927###eoif===1984###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");ERROR("failed to set common root directory %s", rootdir);

				AKA_mark("lis===824###sois===18853###eois===18864###lif===92###soif===1987###eoif===1998###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");goto error;

	}
	else {AKA_mark("lis===-822-###sois===-18752-###eois===-1875235-###lif===-90-###soif===-###eoif===-1921-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}

		if (AKA_mark("lis===826###sois===18873###eois===19020###lif===94###soif===2007###eoif===2154###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && ((AKA_mark("lis===826###sois===18873###eois===18939###lif===94###soif===2007###eoif===2073###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&addenv_realpath("AFB_WORKDIR", "."     /* resolved by realpath */))	||(AKA_mark("lis===827###sois===18945###eois===19020###lif===95###soif===2079###eoif===2154###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&addenv_realpath("AFB_ROOTDIR", rootdir /* relative to current directory */)))) {
				AKA_mark("lis===828###sois===19026###eois===19061###lif===96###soif===2160###eoif===2195###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");ERROR("can't set DIR environment");

				AKA_mark("lis===829###sois===19064###eois===19075###lif===97###soif===2198###eoif===2209###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");goto error;

	}
	else {AKA_mark("lis===-826-###sois===-18873-###eois===-18873147-###lif===-94-###soif===-###eoif===-2154-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}


	/* setup HTTP */
		if (AKA_mark("lis===833###sois===19103###eois===19112###lif===101###soif===2237###eoif===2246###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===833###sois===19103###eois===19112###lif===101###soif===2237###eoif===2246###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&!no_httpd)) {
				if (AKA_mark("lis===834###sois===19122###eois===19136###lif===102###soif===2256###eoif===2270###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===834###sois===19122###eois===19136###lif===102###soif===2256###eoif===2270###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&http_port == 0)) {
						AKA_mark("lis===835###sois===19143###eois===19183###lif===103###soif===2277###eoif===2317###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");ERROR("random port is not implemented");

						AKA_mark("lis===836###sois===19187###eois===19198###lif===104###soif===2321###eoif===2332###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");goto error;

		}
		else {AKA_mark("lis===-834-###sois===-19122-###eois===-1912214-###lif===-102-###soif===-###eoif===-2270-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}

				if (AKA_mark("lis===838###sois===19209###eois===19305###lif===106###soif===2343###eoif===2439###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (((AKA_mark("lis===838###sois===19210###eois===19223###lif===106###soif===2344###eoif===2357###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&http_port > 0)	&&(AKA_mark("lis===838###sois===19227###eois===19260###lif===106###soif===2361###eoif===2394###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&addenv_int("AFB_PORT", http_port)))	||((AKA_mark("lis===839###sois===19269###eois===19274###lif===107###soif===2403###eoif===2408###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&token)	&&(AKA_mark("lis===839###sois===19278###eois===19304###lif===107###soif===2412###eoif===2438###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&addenv("AFB_TOKEN", token))))) {
						AKA_mark("lis===840###sois===19312###eois===19348###lif===108###soif===2446###eoif===2482###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");ERROR("can't set HTTP environment");

						AKA_mark("lis===841###sois===19352###eois===19363###lif===109###soif===2486###eoif===2497###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");goto error;

		}
		else {AKA_mark("lis===-838-###sois===-19209-###eois===-1920996-###lif===-106-###soif===-###eoif===-2439-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}

	}
	else {AKA_mark("lis===-833-###sois===-19103-###eois===-191039-###lif===-101-###soif===-###eoif===-2246-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}


	/* configure the daemon */
		AKA_mark("lis===846###sois===19401###eois===19433###lif===114###soif===2535###eoif===2567###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");afb_export_set_config(settings);

		AKA_mark("lis===847###sois===19435###eois===19488###lif===115###soif===2569###eoif===2622###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");main_apiset = afb_apiset_create("main", api_timeout);

		if (AKA_mark("lis===848###sois===19494###eois===19506###lif===116###soif===2628###eoif===2640###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===848###sois===19494###eois===19506###lif===116###soif===2628###eoif===2640###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&!main_apiset)) {
				AKA_mark("lis===849###sois===19512###eois===19547###lif===117###soif===2646###eoif===2681###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");ERROR("can't create main api set");

				AKA_mark("lis===850###sois===19550###eois===19561###lif===118###soif===2684###eoif===2695###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");goto error;

	}
	else {AKA_mark("lis===-848-###sois===-19494-###eois===-1949412-###lif===-116-###soif===-###eoif===-2640-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}

		if (AKA_mark("lis===852###sois===19570###eois===19616###lif===120###soif===2704###eoif===2750###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===852###sois===19570###eois===19616###lif===120###soif===2704###eoif===2750###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&afb_monitor_init(main_apiset, main_apiset) < 0)) {
				AKA_mark("lis===853###sois===19622###eois===19655###lif===121###soif===2756###eoif===2789###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");ERROR("failed to setup monitor");

				AKA_mark("lis===854###sois===19658###eois===19669###lif===122###soif===2792###eoif===2803###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");goto error;

	}
	else {AKA_mark("lis===-852-###sois===-19570-###eois===-1957046-###lif===-120-###soif===-###eoif===-2750-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}

#if WITH_SUPERVISION
	if (afb_supervision_init(main_apiset, main_config) < 0) {
		ERROR("failed to setup supervision");
		goto error;
	}
#endif

#if WITH_AFB_HOOK
	/* install hooks */
	if (tracereq)
		afb_hook_create_xreq(NULL, NULL, NULL, afb_hook_flags_xreq_from_text(tracereq), NULL, NULL);
#if !defined(REMOVE_LEGACY_TRACE)
	if (traceapi || tracesvc || traceditf)
		afb_hook_create_api(NULL, afb_hook_flags_api_from_text(traceapi)
			| afb_hook_flags_legacy_ditf_from_text(traceditf)
			| afb_hook_flags_legacy_svc_from_text(tracesvc), NULL, NULL);
#else
	if (traceapi)
		afb_hook_create_api(NULL, afb_hook_flags_api_from_text(traceapi), NULL, NULL);
#endif
	if (traceevt)
		afb_hook_create_evt(NULL, afb_hook_flags_evt_from_text(traceevt), NULL, NULL);
	if (traceses)
		afb_hook_create_session(NULL, afb_hook_flags_session_from_text(traceses), NULL, NULL);
	if (traceglob)
		afb_hook_create_global(afb_hook_flags_global_from_text(traceglob), NULL, NULL);
#endif

	/* load bindings and apis */
		AKA_mark("lis===885###sois===20672###eois===20696###lif===153###soif===3806###eoif===3830###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");afb_debug("start-load");

#if WITH_DYNAMIC_BINDING
	apiset_start_list("binding", afb_api_so_add_binding, "the binding");
	apiset_start_list("ldpaths", afb_api_so_add_pathset_fails, "the binding path set");
	apiset_start_list("weak-ldpaths", afb_api_so_add_pathset_nofails, "the weak binding path set");
#endif
		AKA_mark("lis===891###sois===20982###eois===21063###lif===159###soif===4116###eoif===4197###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");apiset_start_list("auto-api", afb_autoset_add_any, "the automatic api path set");

#if WITH_DBUS_TRANSPARENCY
	apiset_start_list("dbus-client", afb_api_dbus_add_client, "the afb-dbus client");
#endif
		AKA_mark("lis===895###sois===21182###eois===21269###lif===163###soif===4316###eoif===4403###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");apiset_start_list("ws-client", afb_api_ws_add_client_weak, "the afb-websocket client");


		AKA_mark("lis===897###sois===21272###eois===21298###lif===165###soif===4406###eoif===4432###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");DEBUG("Init config done");


	/* start the services */
		AKA_mark("lis===900###sois===21327###eois===21352###lif===168###soif===4461###eoif===4486###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");afb_debug("start-start");

#if !defined(NO_CALL_PERSONALITY)
		AKA_mark("lis===902###sois===21388###eois===21420###lif===170###soif===4522###eoif===4554###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");personality((unsigned long)-1L);

#endif
		if (AKA_mark("lis===904###sois===21433###eois===21479###lif===172###soif===4567###eoif===4613###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===904###sois===21433###eois===21479###lif===172###soif===4567###eoif===4613###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&afb_apiset_start_all_services(main_apiset) < 0)) {
		AKA_mark("lis===905###sois===21483###eois===21494###lif===173###soif===4617###eoif===4628###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");goto error;
	}
	else {AKA_mark("lis===-904-###sois===-21433-###eois===-2143346-###lif===-172-###soif===-###eoif===-4613-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}


	/* export started apis */
		AKA_mark("lis===908###sois===21524###eois===21607###lif===176###soif===4658###eoif===4741###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");apiset_start_list("ws-server", afb_api_ws_add_server, "the afb-websocket service");

#if WITH_DBUS_TRANSPARENCY
	apiset_start_list("dbus-server", afb_api_dbus_add_server, "the afb-dbus service");
#endif

	/* start the HTTP server */
		AKA_mark("lis===914###sois===21757###eois===21781###lif===182###soif===4891###eoif===4915###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");afb_debug("start-http");

		if (AKA_mark("lis===915###sois===21787###eois===21796###lif===183###soif===4921###eoif===4930###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===915###sois===21787###eois===21796###lif===183###soif===4921###eoif===4930###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&!no_httpd)) {
				if (AKA_mark("lis===916###sois===21806###eois===21864###lif===184###soif===4940###eoif===4998###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===916###sois===21806###eois===21864###lif===184###soif===4940###eoif===4998###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&!afb_hreq_init_cookie(http_port, rootapi, session_timeout))) {
						AKA_mark("lis===917###sois===21871###eois===21918###lif===185###soif===5005###eoif===5052###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");ERROR("initialisation of HTTP cookies failed");

						AKA_mark("lis===918###sois===21922###eois===21933###lif===186###soif===5056###eoif===5067###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");goto error;

		}
		else {AKA_mark("lis===-916-###sois===-21806-###eois===-2180658-###lif===-184-###soif===-###eoif===-4998-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}


				AKA_mark("lis===921###sois===21941###eois===21968###lif===189###soif===5075###eoif===5102###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");hsrv = start_http_server();

				if (AKA_mark("lis===922###sois===21975###eois===21987###lif===190###soif===5109###eoif===5121###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===922###sois===21975###eois===21987###lif===190###soif===5109###eoif===5121###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&hsrv == NULL)) {
			AKA_mark("lis===923###sois===21992###eois===22003###lif===191###soif===5126###eoif===5137###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");goto error;
		}
		else {AKA_mark("lis===-922-###sois===-21975-###eois===-2197512-###lif===-190-###soif===-###eoif===-5121-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}

	}
	else {AKA_mark("lis===-915-###sois===-21787-###eois===-217879-###lif===-183-###soif===-###eoif===-4930-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}


	/* run the startup calls */
		AKA_mark("lis===927###sois===22038###eois===22062###lif===195###soif===5172###eoif===5196###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");afb_debug("start-call");

		AKA_mark("lis===928###sois===22064###eois===22084###lif===196###soif===5198###eoif===5218###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");run_startup_calls();


	/* run the command */
		AKA_mark("lis===931###sois===22110###eois===22134###lif===199###soif===5244###eoif===5268###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");afb_debug("start-exec");

		if (AKA_mark("lis===932###sois===22140###eois===22177###lif===200###soif===5274###eoif===5311###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===932###sois===22140###eois===22177###lif===200###soif===5274###eoif===5311###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&execute_command(http_port, token) < 0)) {
		AKA_mark("lis===933###sois===22181###eois===22192###lif===201###soif===5315###eoif===5326###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");goto error;
	}
	else {AKA_mark("lis===-932-###sois===-22140-###eois===-2214037-###lif===-200-###soif===-###eoif===-5311-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}


	/* ready */
		AKA_mark("lis===936###sois===22208###eois===22232###lif===204###soif===5342###eoif===5366###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");sd_notify(1, "READY=1");


	/* activate the watchdog */
#if HAS_WATCHDOG
		if (AKA_mark("lis===940###sois===22285###eois===22308###lif===208###soif===5419###eoif===5442###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)") && (AKA_mark("lis===940###sois===22285###eois===22308###lif===208###soif===5419###eoif===5442###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)")&&watchdog_activate() < 0)) {
		AKA_mark("lis===941###sois===22312###eois===22346###lif===209###soif===5446###eoif===5480###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");ERROR("can't start the watchdog");
	}
	else {AKA_mark("lis===-940-###sois===-22285-###eois===-2228523-###lif===-208-###soif===-###eoif===-5442-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");}

#endif

		AKA_mark("lis===944###sois===22356###eois===22363###lif===212###soif===5490###eoif===5497###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");return;

	error:
	AKA_mark("lis===946###sois===22372###eois===22380###lif===214###soif===5506###eoif===5514###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/start(int,void*)");exit(1);

}

/*---------------------------------------------------------
 | main
 |   Parse option and launch action
 +--------------------------------------------------------- */

/** Instrumented function main(int,char*[]) */
int AKA_MAIN(int argc, char *argv[])
{AKA_mark("Calling: ./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");AKA_fCall++;
		AKA_mark("lis===956###sois===22588###eois===22612###lif===2###soif===36###eoif===60###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");struct json_object *obj;

		AKA_mark("lis===957###sois===22614###eois===22638###lif===3###soif===62###eoif===86###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");afb_debug("main-entry");


	// ------------- Build session handler & init config -------
		AKA_mark("lis===960###sois===22703###eois===22744###lif===6###soif===151###eoif===192###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");main_config = afb_args_parse(argc, argv);

		if (AKA_mark("lis===961###sois===22750###eois===22870###lif===7###soif===198###eoif===318###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])") && (AKA_mark("lis===961###sois===22750###eois===22870###lif===7###soif===198###eoif===318###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])")&&sig_monitor_init(
		!json_object_object_get_ex(main_config, "trap-faults", &obj)
			|| json_object_get_boolean(obj)) < 0)) {
				AKA_mark("lis===964###sois===22876###eois===22922###lif===10###soif===324###eoif===370###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");ERROR("failed to initialise signal handlers");

				AKA_mark("lis===965###sois===22925###eois===22934###lif===11###soif===373###eoif===382###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");return 1;

	}
	else {AKA_mark("lis===-961-###sois===-22750-###eois===-22750120-###lif===-7-###soif===-###eoif===-318-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");}



		if (AKA_mark("lis===969###sois===22945###eois===22997###lif===15###soif===393###eoif===445###ifc===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])") && (AKA_mark("lis===969###sois===22945###eois===22997###lif===15###soif===393###eoif===445###isc===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])")&&json_object_object_get_ex(main_config, "name", &obj))) {
				AKA_mark("lis===970###sois===23003###eois===23052###lif===16###soif===451###eoif===500###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");verbose_set_name(json_object_get_string(obj), 0);

				AKA_mark("lis===971###sois===23055###eois===23106###lif===17###soif===503###eoif===554###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");process_name_set_name(json_object_get_string(obj));

				AKA_mark("lis===972###sois===23109###eois===23173###lif===18###soif===557###eoif===621###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");process_name_replace_cmdline(argv, json_object_get_string(obj));

	}
	else {AKA_mark("lis===-969-###sois===-22945-###eois===-2294552-###lif===-15-###soif===-###eoif===-445-###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");}

		AKA_mark("lis===974###sois===23178###eois===23201###lif===20###soif===626###eoif===649###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");afb_debug("main-args");


	// --------- run -----------
		AKA_mark("lis===977###sois===23234###eois===23246###lif===23###soif===682###eoif===694###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");daemonize();

		AKA_mark("lis===978###sois===23248###eois===23286###lif===24###soif===696###eoif===734###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");INFO("running with pid %d", getpid());


	/* set the daemon environment */
		AKA_mark("lis===981###sois===23323###eois===23338###lif===27###soif===771###eoif===786###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");setup_daemon();


		AKA_mark("lis===983###sois===23341###eois===23365###lif===29###soif===789###eoif===813###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");afb_debug("main-start");


	/* enter job processing */
		AKA_mark("lis===986###sois===23396###eois===23431###lif===32###soif===844###eoif===879###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");jobs_start(3, 0, 100, start, NULL);

		AKA_mark("lis===987###sois===23433###eois===23489###lif===33###soif===881###eoif===937###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");WARNING("hoops returned from jobs_enter! [report bug]");

		AKA_mark("lis===988###sois===23491###eois===23500###lif===34###soif===939###eoif===948###ins===true###function===./app-framework-binder/src/main-afb-daemon.c/main(int,char*[])");return 1;

}


#endif

