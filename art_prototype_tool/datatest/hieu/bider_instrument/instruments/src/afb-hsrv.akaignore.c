/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_HSRV_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_HSRV_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <poll.h>
#include <fcntl.h>
#include <errno.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/stat.h>

#include <json-c/json.h>
#include <microhttpd.h>
#if MHD_VERSION < 0x00095206
# define MHD_ALLOW_SUSPEND_RESUME MHD_USE_SUSPEND_RESUME
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_METHOD_H_
#define AKA_INCLUDE__AFB_METHOD_H_
#include "afb-method.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CONTEXT_H_
#define AKA_INCLUDE__AFB_CONTEXT_H_
#include "afb-context.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_XREQ_H_
#define AKA_INCLUDE__AFB_XREQ_H_
#include "afb-xreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HREQ_H_
#define AKA_INCLUDE__AFB_HREQ_H_
#include "afb-hreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HSRV_H_
#define AKA_INCLUDE__AFB_HSRV_H_
#include "afb-hsrv.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_FDEV_H_
#define AKA_INCLUDE__AFB_FDEV_H_
#include "afb-fdev.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SOCKET_H_
#define AKA_INCLUDE__AFB_SOCKET_H_
#include "afb-socket.akaignore.h"
#endif


/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_H_
#define AKA_INCLUDE__FDEV_H_
#include "fdev.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__LOCALE_ROOT_H_
#define AKA_INCLUDE__LOCALE_ROOT_H_
#include "locale-root.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__SYSTEMD_H_
#define AKA_INCLUDE__SYSTEMD_H_
#include "systemd.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__JOBS_H_
#define AKA_INCLUDE__JOBS_H_
#include "jobs.akaignore.h"
#endif


#define JSON_CONTENT  "application/json"
#define FORM_CONTENT  MHD_HTTP_POST_ENCODING_MULTIPART_FORMDATA

struct hsrv_itf {
	struct hsrv_itf *next;
	struct afb_hsrv *hsrv;
	struct fdev *fdev;
	char uri[];
};

struct hsrv_handler {
	struct hsrv_handler *next;
	const char *prefix;
	size_t length;
	int (*handler) (struct afb_hreq *, void *);
	void *data;
	int priority;
};

struct hsrv_alias {
	struct locale_root *root;
	int relax;
};

struct afb_hsrv {
	unsigned refcount;
	struct hsrv_handler *handlers;
	struct hsrv_itf *interfaces;
	struct MHD_Daemon *httpd;
	struct fdev *fdev;
	char *cache_to;
};

/** Instrumented function reply_error(struct MHD_Connection*,unsigned int) */
static void reply_error(struct MHD_Connection *connection, unsigned int status)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/reply_error(struct MHD_Connection*,unsigned int)");AKA_fCall++;
		AKA_mark("lis===87###sois===1982###eois===2079###lif===2###soif===83###eoif===180###ins===true###function===./app-framework-binder/src/afb-hsrv.c/reply_error(struct MHD_Connection*,unsigned int)");struct MHD_Response *response = MHD_create_response_from_buffer(0, NULL, MHD_RESPMEM_PERSISTENT);

		AKA_mark("lis===88###sois===2081###eois===2130###lif===3###soif===182###eoif===231###ins===true###function===./app-framework-binder/src/afb-hsrv.c/reply_error(struct MHD_Connection*,unsigned int)");MHD_queue_response(connection, status, response);

		AKA_mark("lis===89###sois===2132###eois===2163###lif===4###soif===233###eoif===264###ins===true###function===./app-framework-binder/src/afb-hsrv.c/reply_error(struct MHD_Connection*,unsigned int)");MHD_destroy_response(response);

}

/** Instrumented function postproc(void*,enum MHD_ValueKind,const char*,const char*,const char*,const char*,const char*,uint64_t,size_t) */
static int postproc(void *cls,
                    enum MHD_ValueKind kind,
                    const char *key,
                    const char *filename,
                    const char *content_type,
                    const char *transfer_encoding,
                    const char *data,
		    uint64_t off,
		    size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/postproc(void*,enum MHD_ValueKind,const char*,const char*,const char*,const char*,const char*,uint64_t,size_t)");AKA_fCall++;
		AKA_mark("lis===102###sois===2499###eois===2527###lif===10###soif===332###eoif===360###ins===true###function===./app-framework-binder/src/afb-hsrv.c/postproc(void*,enum MHD_ValueKind,const char*,const char*,const char*,const char*,const char*,uint64_t,size_t)");struct afb_hreq *hreq = cls;

		if (AKA_mark("lis===103###sois===2533###eois===2549###lif===11###soif===366###eoif===382###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/postproc(void*,enum MHD_ValueKind,const char*,const char*,const char*,const char*,const char*,uint64_t,size_t)") && (AKA_mark("lis===103###sois===2533###eois===2549###lif===11###soif===366###eoif===382###isc===true###function===./app-framework-binder/src/afb-hsrv.c/postproc(void*,enum MHD_ValueKind,const char*,const char*,const char*,const char*,const char*,uint64_t,size_t)")&&filename != NULL)) {
		AKA_mark("lis===104###sois===2553###eois===2616###lif===12###soif===386###eoif===449###ins===true###function===./app-framework-binder/src/afb-hsrv.c/postproc(void*,enum MHD_ValueKind,const char*,const char*,const char*,const char*,const char*,uint64_t,size_t)");return afb_hreq_post_add_file(hreq, key, filename, data, size);
	}
	else {
		AKA_mark("lis===106###sois===2625###eois===2673###lif===14###soif===458###eoif===506###ins===true###function===./app-framework-binder/src/afb-hsrv.c/postproc(void*,enum MHD_ValueKind,const char*,const char*,const char*,const char*,const char*,uint64_t,size_t)");return afb_hreq_post_add(hreq, key, data, size);
	}

}

/** Instrumented function access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**) */
static int access_handler(
		void *cls,
		struct MHD_Connection *connection,
		const char *url,
		const char *methodstr,
		const char *version,
		const char *upload_data,
		size_t *upload_data_size,
		void **recordreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");AKA_fCall++;
		AKA_mark("lis===119###sois===2899###eois===2906###lif===10###soif===222###eoif===229###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");int rc;

		AKA_mark("lis===120###sois===2908###eois===2930###lif===11###soif===231###eoif===253###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");struct afb_hreq *hreq;

		AKA_mark("lis===121###sois===2932###eois===2955###lif===12###soif===255###eoif===278###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");enum afb_method method;

		AKA_mark("lis===122###sois===2957###eois===2979###lif===13###soif===280###eoif===302###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");struct afb_hsrv *hsrv;

		AKA_mark("lis===123###sois===2981###eois===3007###lif===14###soif===304###eoif===330###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");struct hsrv_handler *iter;

		AKA_mark("lis===124###sois===3009###eois===3026###lif===15###soif===332###eoif===349###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");const char *type;

		AKA_mark("lis===125###sois===3028###eois===3057###lif===16###soif===351###eoif===380###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");enum json_tokener_error jerr;


		AKA_mark("lis===127###sois===3060###eois===3071###lif===18###soif===383###eoif===394###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hsrv = cls;

		AKA_mark("lis===128###sois===3073###eois===3091###lif===19###soif===396###eoif===414###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq = *recordreq;

		if (AKA_mark("lis===129###sois===3097###eois===3109###lif===20###soif===420###eoif===432###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===129###sois===3097###eois===3109###lif===20###soif===420###eoif===432###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&hreq == NULL)) {
		/* get the method */
				AKA_mark("lis===131###sois===3138###eois===3169###lif===22###soif===461###eoif===492###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");method = get_method(methodstr);

				AKA_mark("lis===132###sois===3172###eois===3215###lif===23###soif===495###eoif===538###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");method &= afb_method_get | afb_method_post;

				if (AKA_mark("lis===133###sois===3222###eois===3247###lif===24###soif===545###eoif===570###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===133###sois===3222###eois===3247###lif===24###soif===545###eoif===570###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&method == afb_method_none)) {
						AKA_mark("lis===134###sois===3254###eois===3306###lif===25###soif===577###eoif===629###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");WARNING("Unsupported HTTP operation %s", methodstr);

						AKA_mark("lis===135###sois===3310###eois===3356###lif===26###soif===633###eoif===679###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");reply_error(connection, MHD_HTTP_BAD_REQUEST);

						AKA_mark("lis===136###sois===3360###eois===3375###lif===27###soif===683###eoif===698###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");return MHD_YES;

		}
		else {AKA_mark("lis===-133-###sois===-3222-###eois===-322225-###lif===-24-###soif===-###eoif===-570-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}


		/* create the request */
				AKA_mark("lis===140###sois===3410###eois===3435###lif===31###soif===733###eoif===758###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq = afb_hreq_create();

				if (AKA_mark("lis===141###sois===3442###eois===3454###lif===32###soif===765###eoif===777###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===141###sois===3442###eois===3454###lif===32###soif===765###eoif===777###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&hreq == NULL)) {
						AKA_mark("lis===142###sois===3461###eois===3492###lif===33###soif===784###eoif===815###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");ERROR("Can't allocate 'hreq'");

						AKA_mark("lis===143###sois===3496###eois===3552###lif===34###soif===819###eoif===875###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");reply_error(connection, MHD_HTTP_INTERNAL_SERVER_ERROR);

						AKA_mark("lis===144###sois===3556###eois===3571###lif===35###soif===879###eoif===894###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");return MHD_YES;

		}
		else {AKA_mark("lis===-141-###sois===-3442-###eois===-344212-###lif===-32-###soif===-###eoif===-777-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}


		/* init the request */
				AKA_mark("lis===148###sois===3604###eois===3622###lif===39###soif===927###eoif===945###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->hsrv = hsrv;

				AKA_mark("lis===149###sois===3625###eois===3661###lif===40###soif===948###eoif===984###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->cacheTimeout = hsrv->cache_to;

				AKA_mark("lis===150###sois===3664###eois===3694###lif===41###soif===987###eoif===1017###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->connection = connection;

				AKA_mark("lis===151###sois===3697###eois===3719###lif===42###soif===1020###eoif===1042###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->method = method;

				AKA_mark("lis===152###sois===3722###eois===3746###lif===43###soif===1045###eoif===1069###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->version = version;

				AKA_mark("lis===153###sois===3749###eois===3852###lif===44###soif===1072###eoif===1175###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->lang = MHD_lookup_connection_value(connection, MHD_HEADER_KIND, MHD_HTTP_HEADER_ACCEPT_LANGUAGE);

				AKA_mark("lis===154###sois===3855###eois===3884###lif===45###soif===1178###eoif===1207###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->tail = hreq->url = url;

				AKA_mark("lis===155###sois===3887###eois===3930###lif===46###soif===1210###eoif===1253###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->lentail = hreq->lenurl = strlen(url);

				AKA_mark("lis===156###sois===3933###eois===3951###lif===47###soif===1256###eoif===1274###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");*recordreq = hreq;


		/* init the post processing */
				if (AKA_mark("lis===159###sois===3992###eois===4017###lif===50###soif===1315###eoif===1340###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===159###sois===3992###eois===4017###lif===50###soif===1315###eoif===1340###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&method == afb_method_post)) {
						AKA_mark("lis===160###sois===4024###eois===4087###lif===51###soif===1347###eoif===1410###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");type = afb_hreq_get_header(hreq, MHD_HTTP_HEADER_CONTENT_TYPE);

						if (AKA_mark("lis===161###sois===4095###eois===4107###lif===52###soif===1418###eoif===1430###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===161###sois===4095###eois===4107###lif===52###soif===1418###eoif===1430###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&type == NULL)) {
				/* an empty post, let's process it as a get */
								AKA_mark("lis===163###sois===4166###eois===4196###lif===54###soif===1489###eoif===1519###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->method = afb_method_get;

			}
			else {
				if (AKA_mark("lis===164###sois===4211###eois===4249###lif===55###soif===1534###eoif===1572###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===164###sois===4211###eois===4249###lif===55###soif===1534###eoif===1572###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&strcasestr(type, FORM_CONTENT) != NULL)) {
									AKA_mark("lis===165###sois===4257###eois===4336###lif===56###soif===1580###eoif===1659###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->postform = MHD_create_post_processor (connection, 65500, postproc, hreq);

									if (AKA_mark("lis===166###sois===4345###eois===4367###lif===57###soif===1668###eoif===1690###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===166###sois===4345###eois===4367###lif===57###soif===1668###eoif===1690###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&hreq->postform == NULL)) {
											AKA_mark("lis===167###sois===4376###eois===4413###lif===58###soif===1699###eoif===1736###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");ERROR("Can't create POST processor");

											AKA_mark("lis===168###sois===4419###eois===4478###lif===59###soif===1742###eoif===1801###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");afb_hreq_reply_error(hreq, MHD_HTTP_INTERNAL_SERVER_ERROR);

				}
					else {AKA_mark("lis===-166-###sois===-4345-###eois===-434522-###lif===-57-###soif===-###eoif===-1690-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}

									AKA_mark("lis===170###sois===4489###eois===4504###lif===61###soif===1812###eoif===1827###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");return MHD_YES;

			}
				else {
					if (AKA_mark("lis===171###sois===4519###eois===4557###lif===62###soif===1842###eoif===1880###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===171###sois===4519###eois===4557###lif===62###soif===1842###eoif===1880###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&strcasestr(type, JSON_CONTENT) != NULL)) {
										AKA_mark("lis===172###sois===4565###eois===4600###lif===63###soif===1888###eoif===1923###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->tokener = json_tokener_new();

										if (AKA_mark("lis===173###sois===4609###eois===4630###lif===64###soif===1932###eoif===1953###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===173###sois===4609###eois===4630###lif===64###soif===1932###eoif===1953###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&hreq->tokener == NULL)) {
												AKA_mark("lis===174###sois===4639###eois===4678###lif===65###soif===1962###eoif===2001###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");ERROR("Can't create tokener for POST");

												AKA_mark("lis===175###sois===4684###eois===4743###lif===66###soif===2007###eoif===2066###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");afb_hreq_reply_error(hreq, MHD_HTTP_INTERNAL_SERVER_ERROR);

				}
						else {AKA_mark("lis===-173-###sois===-4609-###eois===-460921-###lif===-64-###soif===-###eoif===-1953-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}

										AKA_mark("lis===177###sois===4754###eois===4769###lif===68###soif===2077###eoif===2092###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");return MHD_YES;

                        }
					else {
										AKA_mark("lis===179###sois===4807###eois===4850###lif===70###soif===2130###eoif===2173###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");WARNING("Unsupported media type %s", type);

										AKA_mark("lis===180###sois===4855###eois===4915###lif===71###soif===2178###eoif===2238###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");afb_hreq_reply_error(hreq, MHD_HTTP_UNSUPPORTED_MEDIA_TYPE);

										AKA_mark("lis===181###sois===4920###eois===4935###lif===72###soif===2243###eoif===2258###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");return MHD_YES;

			}
				}
			}

		}
		else {AKA_mark("lis===-159-###sois===-3992-###eois===-399225-###lif===-50-###soif===-###eoif===-1340-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}

	}
	else {AKA_mark("lis===-129-###sois===-3097-###eois===-309712-###lif===-20-###soif===-###eoif===-432-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}


	/* process further data */
		if (AKA_mark("lis===187###sois===4982###eois===4999###lif===78###soif===2305###eoif===2322###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===187###sois===4982###eois===4999###lif===78###soif===2305###eoif===2322###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&*upload_data_size)) {
				if (AKA_mark("lis===188###sois===5009###eois===5031###lif===79###soif===2332###eoif===2354###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===188###sois===5009###eois===5031###lif===79###soif===2332###eoif===2354###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&hreq->postform != NULL)) {
						if (AKA_mark("lis===189###sois===5042###eois===5108###lif===80###soif===2365###eoif===2431###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===189###sois===5042###eois===5108###lif===80###soif===2365###eoif===2431###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&!MHD_post_process (hreq->postform, upload_data, *upload_data_size))) {
								AKA_mark("lis===190###sois===5116###eois===5149###lif===81###soif===2439###eoif===2472###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");ERROR("error in POST processor");

								AKA_mark("lis===191###sois===5154###eois===5213###lif===82###soif===2477###eoif===2536###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");afb_hreq_reply_error(hreq, MHD_HTTP_INTERNAL_SERVER_ERROR);

								AKA_mark("lis===192###sois===5218###eois===5233###lif===83###soif===2541###eoif===2556###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");return MHD_YES;

			}
			else {AKA_mark("lis===-189-###sois===-5042-###eois===-504266-###lif===-80-###soif===-###eoif===-2431-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}

		}
		else {
			if (AKA_mark("lis===194###sois===5252###eois===5265###lif===85###soif===2575###eoif===2588###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===194###sois===5252###eois===5265###lif===85###soif===2575###eoif===2588###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&hreq->tokener)) {
							AKA_mark("lis===195###sois===5272###eois===5359###lif===86###soif===2595###eoif===2682###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->json = json_tokener_parse_ex(hreq->tokener, upload_data, (int)*upload_data_size);

							AKA_mark("lis===196###sois===5363###eois===5408###lif===87###soif===2686###eoif===2731###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");jerr = json_tokener_get_error(hreq->tokener);

							if (AKA_mark("lis===197###sois===5416###eois===5445###lif===88###soif===2739###eoif===2768###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===197###sois===5416###eois===5445###lif===88###soif===2739###eoif===2768###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&jerr == json_tokener_continue)) {
									AKA_mark("lis===198###sois===5453###eois===5510###lif===89###soif===2776###eoif===2833###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->json = json_tokener_parse_ex(hreq->tokener, "", 1);

									AKA_mark("lis===199###sois===5515###eois===5560###lif===90###soif===2838###eoif===2883###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");jerr = json_tokener_get_error(hreq->tokener);

			}
				else {AKA_mark("lis===-197-###sois===-5416-###eois===-541629-###lif===-88-###soif===-###eoif===-2768-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}

							if (AKA_mark("lis===201###sois===5573###eois===5601###lif===92###soif===2896###eoif===2924###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===201###sois===5573###eois===5601###lif===92###soif===2896###eoif===2924###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&jerr != json_tokener_success)) {
									AKA_mark("lis===202###sois===5609###eois===5672###lif===93###soif===2932###eoif===2995###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");ERROR("error in POST json: %s", json_tokener_error_desc(jerr));

									AKA_mark("lis===203###sois===5677###eois===5726###lif===94###soif===3000###eoif===3049###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");afb_hreq_reply_error(hreq, MHD_HTTP_BAD_REQUEST);

									AKA_mark("lis===204###sois===5731###eois===5746###lif===95###soif===3054###eoif===3069###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");return MHD_YES;

			}
				else {AKA_mark("lis===-201-###sois===-5573-###eois===-557328-###lif===-92-###soif===-###eoif===-2924-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}

		}
			else {AKA_mark("lis===-194-###sois===-5252-###eois===-525213-###lif===-85-###soif===-###eoif===-2588-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}
		}

				AKA_mark("lis===207###sois===5758###eois===5780###lif===98###soif===3081###eoif===3103###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");*upload_data_size = 0;

				AKA_mark("lis===208###sois===5783###eois===5798###lif===99###soif===3106###eoif===3121###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");return MHD_YES;

	}
	else {AKA_mark("lis===-187-###sois===-4982-###eois===-498217-###lif===-78-###soif===-###eoif===-2322-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}


	/* flush the data */
		if (AKA_mark("lis===212###sois===5830###eois===5852###lif===103###soif===3153###eoif===3175###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===212###sois===5830###eois===5852###lif===103###soif===3153###eoif===3175###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&hreq->postform != NULL)) {
				AKA_mark("lis===213###sois===5858###eois===5906###lif===104###soif===3181###eoif===3229###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");rc = MHD_destroy_post_processor(hreq->postform);

				AKA_mark("lis===214###sois===5909###eois===5931###lif===105###soif===3232###eoif===3254###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->postform = NULL;

				if (AKA_mark("lis===215###sois===5938###eois===5950###lif===106###soif===3261###eoif===3273###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===215###sois===5938###eois===5950###lif===106###soif===3261###eoif===3273###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&rc == MHD_NO)) {
						AKA_mark("lis===216###sois===5957###eois===6000###lif===107###soif===3280###eoif===3323###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");ERROR("error detected in POST processing");

						AKA_mark("lis===217###sois===6004###eois===6053###lif===108###soif===3327###eoif===3376###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");afb_hreq_reply_error(hreq, MHD_HTTP_BAD_REQUEST);

						AKA_mark("lis===218###sois===6057###eois===6072###lif===109###soif===3380###eoif===3395###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");return MHD_YES;

		}
		else {AKA_mark("lis===-215-###sois===-5938-###eois===-593812-###lif===-106-###soif===-###eoif===-3273-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}

	}
	else {AKA_mark("lis===-212-###sois===-5830-###eois===-583022-###lif===-103-###soif===-###eoif===-3175-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}

		if (AKA_mark("lis===221###sois===6085###eois===6106###lif===112###soif===3408###eoif===3429###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===221###sois===6085###eois===6106###lif===112###soif===3408###eoif===3429###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&hreq->tokener != NULL)) {
				AKA_mark("lis===222###sois===6112###eois===6145###lif===113###soif===3435###eoif===3468###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");json_tokener_free(hreq->tokener);

				AKA_mark("lis===223###sois===6148###eois===6169###lif===114###soif===3471###eoif===3492###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->tokener = NULL;

	}
	else {AKA_mark("lis===-221-###sois===-6085-###eois===-608521-###lif===-112-###soif===-###eoif===-3429-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}


		if (AKA_mark("lis===226###sois===6179###eois===6197###lif===117###soif===3502###eoif===3520###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===226###sois===6179###eois===6197###lif===117###soif===3502###eoif===3520###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&hreq->scanned != 0)) {
				if (AKA_mark("lis===227###sois===6207###eois===6249###lif===118###soif===3530###eoif===3572###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && ((AKA_mark("lis===227###sois===6207###eois===6225###lif===118###soif===3530###eoif===3548###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&hreq->replied == 0)	&&(AKA_mark("lis===227###sois===6229###eois===6249###lif===118###soif===3552###eoif===3572###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&hreq->suspended == 0))) {
						AKA_mark("lis===228###sois===6256###eois===6292###lif===119###soif===3579###eoif===3615###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");MHD_suspend_connection (connection);

						AKA_mark("lis===229###sois===6296###eois===6316###lif===120###soif===3619###eoif===3639###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->suspended = 1;

		}
		else {AKA_mark("lis===-227-###sois===-6207-###eois===-620742-###lif===-118-###soif===-###eoif===-3572-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}

				AKA_mark("lis===231###sois===6323###eois===6338###lif===122###soif===3646###eoif===3661###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");return MHD_YES;

	}
	else {AKA_mark("lis===-226-###sois===-6179-###eois===-617918-###lif===-117-###soif===-###eoif===-3520-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}


	/* search an handler for the request */
		AKA_mark("lis===235###sois===6385###eois===6403###lif===126###soif===3708###eoif===3726###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->scanned = 1;

		AKA_mark("lis===236###sois===6405###eois===6427###lif===127###soif===3728###eoif===3750###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");iter = hsrv->handlers;

		while (AKA_mark("lis===237###sois===6436###eois===6440###lif===128###soif===3759###eoif===3763###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===237###sois===6436###eois===6440###lif===128###soif===3759###eoif===3763###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&iter)) {
				if (AKA_mark("lis===238###sois===6450###eois===6501###lif===129###soif===3773###eoif===3824###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===238###sois===6450###eois===6501###lif===129###soif===3773###eoif===3824###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&afb_hreq_unprefix(hreq, iter->prefix, iter->length))) {
						if (AKA_mark("lis===239###sois===6512###eois===6543###lif===130###soif===3835###eoif===3866###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && (AKA_mark("lis===239###sois===6512###eois===6543###lif===130###soif===3835###eoif===3866###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&iter->handler(hreq, iter->data))) {
								if (AKA_mark("lis===240###sois===6555###eois===6597###lif===131###soif===3878###eoif===3920###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)") && ((AKA_mark("lis===240###sois===6555###eois===6573###lif===131###soif===3878###eoif===3896###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&hreq->replied == 0)	&&(AKA_mark("lis===240###sois===6577###eois===6597###lif===131###soif===3900###eoif===3920###isc===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)")&&hreq->suspended == 0))) {
										AKA_mark("lis===241###sois===6606###eois===6642###lif===132###soif===3929###eoif===3965###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");MHD_suspend_connection (connection);

										AKA_mark("lis===242###sois===6648###eois===6668###lif===133###soif===3971###eoif===3991###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->suspended = 1;

				}
				else {AKA_mark("lis===-240-###sois===-6555-###eois===-655542-###lif===-131-###soif===-###eoif===-3920-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}

								AKA_mark("lis===244###sois===6679###eois===6694###lif===135###soif===4002###eoif===4017###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");return MHD_YES;

			}
			else {AKA_mark("lis===-239-###sois===-6512-###eois===-651231-###lif===-130-###soif===-###eoif===-3866-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}

						AKA_mark("lis===246###sois===6703###eois===6726###lif===137###soif===4026###eoif===4049###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->tail = hreq->url;

						AKA_mark("lis===247###sois===6730###eois===6759###lif===138###soif===4053###eoif===4082###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");hreq->lentail = hreq->lenurl;

		}
		else {AKA_mark("lis===-238-###sois===-6450-###eois===-645051-###lif===-129-###soif===-###eoif===-3824-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");}

				AKA_mark("lis===249###sois===6766###eois===6784###lif===140###soif===4089###eoif===4107###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");iter = iter->next;

	}


	/* no handler */
		AKA_mark("lis===253###sois===6808###eois===6853###lif===144###soif===4131###eoif===4176###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");NOTICE("Unhandled request to %s", hreq->url);

		AKA_mark("lis===254###sois===6855###eois===6902###lif===145###soif===4178###eoif===4225###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");afb_hreq_reply_error(hreq, MHD_HTTP_NOT_FOUND);

		AKA_mark("lis===255###sois===6904###eois===6919###lif===146###soif===4227###eoif===4242###ins===true###function===./app-framework-binder/src/afb-hsrv.c/access_handler(void*,struct MHD_Connection*,const char*,const char*,const char*,const char*,size_t*,void**)");return MHD_YES;

}

/* Because of POST call multiple time requestApi we need to free POST handle here */
/** Instrumented function end_handler(void*,struct MHD_Connection*,void**,enum MHD_RequestTerminationCode) */
static void end_handler(void *cls, struct MHD_Connection *connection, void **recordreq,
			enum MHD_RequestTerminationCode toe)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/end_handler(void*,struct MHD_Connection*,void**,enum MHD_RequestTerminationCode)");AKA_fCall++;
		AKA_mark("lis===262###sois===7139###eois===7161###lif===3###soif===131###eoif===153###ins===true###function===./app-framework-binder/src/afb-hsrv.c/end_handler(void*,struct MHD_Connection*,void**,enum MHD_RequestTerminationCode)");struct afb_hreq *hreq;


		AKA_mark("lis===264###sois===7164###eois===7182###lif===5###soif===156###eoif===174###ins===true###function===./app-framework-binder/src/afb-hsrv.c/end_handler(void*,struct MHD_Connection*,void**,enum MHD_RequestTerminationCode)");hreq = *recordreq;

		if (AKA_mark("lis===265###sois===7188###eois===7192###lif===6###soif===180###eoif===184###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/end_handler(void*,struct MHD_Connection*,void**,enum MHD_RequestTerminationCode)") && (AKA_mark("lis===265###sois===7188###eois===7192###lif===6###soif===180###eoif===184###isc===true###function===./app-framework-binder/src/afb-hsrv.c/end_handler(void*,struct MHD_Connection*,void**,enum MHD_RequestTerminationCode)")&&hreq)) {
				AKA_mark("lis===266###sois===7198###eois===7219###lif===7###soif===190###eoif===211###ins===true###function===./app-framework-binder/src/afb-hsrv.c/end_handler(void*,struct MHD_Connection*,void**,enum MHD_RequestTerminationCode)");afb_hreq_unref(hreq);

	}
	else {AKA_mark("lis===-265-###sois===-7188-###eois===-71884-###lif===-6-###soif===-###eoif===-184-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/end_handler(void*,struct MHD_Connection*,void**,enum MHD_RequestTerminationCode)");}

}

/** Instrumented function do_run(int,void*) */
static void do_run(int signum, void *arg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/do_run(int,void*)");AKA_fCall++;
		AKA_mark("lis===272###sois===7271###eois===7297###lif===2###soif===45###eoif===71###ins===true###function===./app-framework-binder/src/afb-hsrv.c/do_run(int,void*)");MHD_UNSIGNED_LONG_LONG to;

		AKA_mark("lis===273###sois===7299###eois===7327###lif===3###soif===73###eoif===101###ins===true###function===./app-framework-binder/src/afb-hsrv.c/do_run(int,void*)");struct afb_hsrv *hsrv = arg;


		if (AKA_mark("lis===275###sois===7334###eois===7341###lif===5###soif===108###eoif===115###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/do_run(int,void*)") && (AKA_mark("lis===275###sois===7334###eois===7341###lif===5###soif===108###eoif===115###isc===true###function===./app-framework-binder/src/afb-hsrv.c/do_run(int,void*)")&&!signum)) {
				do { 			AKA_mark("lis===276###sois===7352###eois===7373###lif===6###soif===126###eoif===147###ins===true###function===./app-framework-binder/src/afb-hsrv.c/do_run(int,void*)");MHD_run(hsrv->httpd);
 }
		while (AKA_mark("lis===276###sois===7382###eois===7433###lif===6###soif===156###eoif===207###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/do_run(int,void*)") && ((AKA_mark("lis===276###sois===7382###eois===7426###lif===6###soif===156###eoif===200###isc===true###function===./app-framework-binder/src/afb-hsrv.c/do_run(int,void*)")&&MHD_get_timeout(hsrv->httpd, &to) == MHD_YES)	&&(AKA_mark("lis===276###sois===7430###eois===7433###lif===6###soif===204###eoif===207###isc===true###function===./app-framework-binder/src/afb-hsrv.c/do_run(int,void*)")&&!to)));

	}
	else {AKA_mark("lis===-275-###sois===-7334-###eois===-73347-###lif===-5-###soif===-###eoif===-115-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/do_run(int,void*)");}

		AKA_mark("lis===278###sois===7440###eois===7477###lif===8###soif===214###eoif===251###ins===true###function===./app-framework-binder/src/afb-hsrv.c/do_run(int,void*)");fdev_set_events(hsrv->fdev, EPOLLIN);

}

/** Instrumented function afb_hsrv_run(struct afb_hsrv*) */
void afb_hsrv_run(struct afb_hsrv *hsrv)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/afb_hsrv_run(struct afb_hsrv*)");AKA_fCall++;
		AKA_mark("lis===283###sois===7525###eois===7556###lif===2###soif===44###eoif===75###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_run(struct afb_hsrv*)");fdev_set_events(hsrv->fdev, 0);

		if (AKA_mark("lis===284###sois===7562###eois===7599###lif===3###soif===81###eoif===118###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_run(struct afb_hsrv*)") && (AKA_mark("lis===284###sois===7562###eois===7599###lif===3###soif===81###eoif===118###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_run(struct afb_hsrv*)")&&jobs_queue(hsrv, 0, do_run, hsrv) < 0)) {
		AKA_mark("lis===285###sois===7603###eois===7619###lif===4###soif===122###eoif===138###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_run(struct afb_hsrv*)");do_run(0, hsrv);
	}
	else {AKA_mark("lis===-284-###sois===-7562-###eois===-756237-###lif===-3-###soif===-###eoif===-118-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_run(struct afb_hsrv*)");}

}

/** Instrumented function listen_callback(void*,uint32_t,struct fdev*) */
static void listen_callback(void *hsrv, uint32_t revents, struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/listen_callback(void*,uint32_t,struct fdev*)");AKA_fCall++;
		AKA_mark("lis===290###sois===7703###eois===7722###lif===2###soif===80###eoif===99###ins===true###function===./app-framework-binder/src/afb-hsrv.c/listen_callback(void*,uint32_t,struct fdev*)");afb_hsrv_run(hsrv);

}

/** Instrumented function new_client_handler(void*,const struct sockaddr*,socklen_t) */
static int new_client_handler(void *cls, const struct sockaddr *addr, socklen_t addrlen)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/new_client_handler(void*,const struct sockaddr*,socklen_t)");AKA_fCall++;
		AKA_mark("lis===295###sois===7818###eois===7833###lif===2###soif===92###eoif===107###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_client_handler(void*,const struct sockaddr*,socklen_t)");return MHD_YES;

}

/** Instrumented function new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int) */
static struct hsrv_handler *new_handler(
		struct hsrv_handler *head,
		const char *prefix,
		int (*handler) (struct afb_hreq *, void *),
		void *data,
		int priority)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");AKA_fCall++;
		AKA_mark("lis===305###sois===8008###eois===8052###lif===7###soif===171###eoif===215###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");struct hsrv_handler *link, *iter, *previous;

		AKA_mark("lis===306###sois===8054###eois===8068###lif===8###soif===217###eoif===231###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");size_t length;


	/* get the length of the prefix without its leading / */
		AKA_mark("lis===309###sois===8129###eois===8153###lif===11###soif===292###eoif===316###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");length = strlen(prefix);

		while (AKA_mark("lis===310###sois===8162###eois===8197###lif===12###soif===325###eoif===360###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)") && ((AKA_mark("lis===310###sois===8162###eois===8168###lif===12###soif===325###eoif===331###isc===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)")&&length)	&&(AKA_mark("lis===310###sois===8172###eois===8197###lif===12###soif===335###eoif===360###isc===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)")&&prefix[length - 1] == '/'))) {
		AKA_mark("lis===311###sois===8201###eois===8210###lif===13###soif===364###eoif===373###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");length--;
	}


	/* allocates the new link */
		AKA_mark("lis===314###sois===8243###eois===8271###lif===16###soif===406###eoif===434###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");link = malloc(sizeof *link);

		if (AKA_mark("lis===315###sois===8277###eois===8289###lif===17###soif===440###eoif===452###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)") && (AKA_mark("lis===315###sois===8277###eois===8289###lif===17###soif===440###eoif===452###isc===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)")&&link == NULL)) {
		AKA_mark("lis===316###sois===8293###eois===8305###lif===18###soif===456###eoif===468###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");return NULL;
	}
	else {AKA_mark("lis===-315-###sois===-8277-###eois===-827712-###lif===-17-###soif===-###eoif===-452-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");}


	/* initialize it */
		AKA_mark("lis===319###sois===8329###eois===8351###lif===21###soif===492###eoif===514###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");link->prefix = prefix;

		AKA_mark("lis===320###sois===8353###eois===8375###lif===22###soif===516###eoif===538###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");link->length = length;

		AKA_mark("lis===321###sois===8377###eois===8401###lif===23###soif===540###eoif===564###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");link->handler = handler;

		AKA_mark("lis===322###sois===8403###eois===8421###lif===24###soif===566###eoif===584###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");link->data = data;

		AKA_mark("lis===323###sois===8423###eois===8449###lif===25###soif===586###eoif===612###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");link->priority = priority;


	/* adds it */
		AKA_mark("lis===326###sois===8467###eois===8483###lif===28###soif===630###eoif===646###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");previous = NULL;

		AKA_mark("lis===327###sois===8485###eois===8497###lif===29###soif===648###eoif===660###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");iter = head;

		while (AKA_mark("lis===328###sois===8506###eois===8599###lif===30###soif===669###eoif===762###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)") && ((AKA_mark("lis===328###sois===8506###eois===8510###lif===30###soif===669###eoif===673###isc===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)")&&iter)	&&((AKA_mark("lis===328###sois===8515###eois===8540###lif===30###soif===678###eoif===703###isc===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)")&&priority < iter->priority)	||((AKA_mark("lis===328###sois===8545###eois===8571###lif===30###soif===708###eoif===734###isc===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)")&&priority == iter->priority)	&&(AKA_mark("lis===328###sois===8575###eois===8597###lif===30###soif===738###eoif===760###isc===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)")&&length <= iter->length))))) {
				AKA_mark("lis===329###sois===8605###eois===8621###lif===31###soif===768###eoif===784###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");previous = iter;

				AKA_mark("lis===330###sois===8624###eois===8642###lif===32###soif===787###eoif===805###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");iter = iter->next;

	}

		AKA_mark("lis===332###sois===8647###eois===8665###lif===34###soif===810###eoif===828###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");link->next = iter;

		if (AKA_mark("lis===333###sois===8671###eois===8687###lif===35###soif===834###eoif===850###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)") && (AKA_mark("lis===333###sois===8671###eois===8687###lif===35###soif===834###eoif===850###isc===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)")&&previous == NULL)) {
		AKA_mark("lis===334###sois===8691###eois===8703###lif===36###soif===854###eoif===866###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");return link;
	}
	else {AKA_mark("lis===-333-###sois===-8671-###eois===-867116-###lif===-35-###soif===-###eoif===-850-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");}

		AKA_mark("lis===335###sois===8705###eois===8727###lif===37###soif===868###eoif===890###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");previous->next = link;

		AKA_mark("lis===336###sois===8729###eois===8741###lif===38###soif===892###eoif===904###ins===true###function===./app-framework-binder/src/afb-hsrv.c/new_handler(struct hsrv_handler*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");return head;

}

/** Instrumented function handle_alias(struct afb_hreq*,void*) */
static int handle_alias(struct afb_hreq *hreq, void *data)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");AKA_fCall++;
		AKA_mark("lis===341###sois===8807###eois===8814###lif===2###soif===62###eoif===69###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");int rc;

		AKA_mark("lis===342###sois===8816###eois===8845###lif===3###soif===71###eoif===100###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");struct hsrv_alias *da = data;

		AKA_mark("lis===343###sois===8847###eois===8876###lif===4###soif===102###eoif===131###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");struct locale_search *search;


		if (AKA_mark("lis===345###sois===8883###eois===8913###lif===6###soif===138###eoif===168###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)") && (AKA_mark("lis===345###sois===8883###eois===8913###lif===6###soif===138###eoif===168###isc===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)")&&hreq->method != afb_method_get)) {
				if (AKA_mark("lis===346###sois===8923###eois===8932###lif===7###soif===178###eoif===187###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)") && (AKA_mark("lis===346###sois===8923###eois===8932###lif===7###soif===178###eoif===187###isc===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)")&&da->relax)) {
			AKA_mark("lis===347###sois===8937###eois===8946###lif===8###soif===192###eoif===201###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");return 0;
		}
		else {AKA_mark("lis===-346-###sois===-8923-###eois===-89239-###lif===-7-###soif===-###eoif===-187-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");}

				AKA_mark("lis===348###sois===8949###eois===9005###lif===9###soif===204###eoif===260###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");afb_hreq_reply_error(hreq, MHD_HTTP_METHOD_NOT_ALLOWED);

				AKA_mark("lis===349###sois===9008###eois===9017###lif===10###soif===263###eoif===272###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");return 1;

	}
	else {AKA_mark("lis===-345-###sois===-8883-###eois===-888330-###lif===-6-###soif===-###eoif===-168-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");}


		AKA_mark("lis===352###sois===9023###eois===9076###lif===13###soif===278###eoif===331###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");search = locale_root_search(da->root, hreq->lang, 0);

		AKA_mark("lis===353###sois===9078###eois===9149###lif===14###soif===333###eoif===404###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");rc = afb_hreq_reply_locale_file_if_exist(hreq, search, &hreq->tail[1]);

		AKA_mark("lis===354###sois===9151###eois===9179###lif===15###soif===406###eoif===434###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");locale_search_unref(search);

		if (AKA_mark("lis===355###sois===9185###eois===9192###lif===16###soif===440###eoif===447###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)") && (AKA_mark("lis===355###sois===9185###eois===9192###lif===16###soif===440###eoif===447###isc===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)")&&rc == 0)) {
				if (AKA_mark("lis===356###sois===9202###eois===9211###lif===17###soif===457###eoif===466###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)") && (AKA_mark("lis===356###sois===9202###eois===9211###lif===17###soif===457###eoif===466###isc===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)")&&da->relax)) {
			AKA_mark("lis===357###sois===9216###eois===9225###lif===18###soif===471###eoif===480###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");return 0;
		}
		else {AKA_mark("lis===-356-###sois===-9202-###eois===-92029-###lif===-17-###soif===-###eoif===-466-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");}

				AKA_mark("lis===358###sois===9228###eois===9275###lif===19###soif===483###eoif===530###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");afb_hreq_reply_error(hreq, MHD_HTTP_NOT_FOUND);

	}
	else {AKA_mark("lis===-355-###sois===-9185-###eois===-91857-###lif===-16-###soif===-###eoif===-447-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");}

		AKA_mark("lis===360###sois===9280###eois===9289###lif===21###soif===535###eoif===544###ins===true###function===./app-framework-binder/src/afb-hsrv.c/handle_alias(struct afb_hreq*,void*)");return 1;

}

/** Instrumented function afb_hsrv_add_handler(struct afb_hsrv*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int) */
int afb_hsrv_add_handler(
		struct afb_hsrv *hsrv,
		const char *prefix,
		int (*handler) (struct afb_hreq *, void *),
		void *data,
		int priority)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_handler(struct afb_hsrv*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");AKA_fCall++;
		AKA_mark("lis===370###sois===9445###eois===9471###lif===7###soif===152###eoif===178###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_handler(struct afb_hsrv*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");struct hsrv_handler *head;


		AKA_mark("lis===372###sois===9474###eois===9542###lif===9###soif===181###eoif===249###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_handler(struct afb_hsrv*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");head = new_handler(hsrv->handlers, prefix, handler, data, priority);

		if (AKA_mark("lis===373###sois===9548###eois===9560###lif===10###soif===255###eoif===267###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_handler(struct afb_hsrv*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)") && (AKA_mark("lis===373###sois===9548###eois===9560###lif===10###soif===255###eoif===267###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_handler(struct afb_hsrv*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)")&&head == NULL)) {
		AKA_mark("lis===374###sois===9564###eois===9573###lif===11###soif===271###eoif===280###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_handler(struct afb_hsrv*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");return 0;
	}
	else {AKA_mark("lis===-373-###sois===-9548-###eois===-954812-###lif===-10-###soif===-###eoif===-267-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_handler(struct afb_hsrv*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");}

		AKA_mark("lis===375###sois===9575###eois===9597###lif===12###soif===282###eoif===304###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_handler(struct afb_hsrv*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");hsrv->handlers = head;

		AKA_mark("lis===376###sois===9599###eois===9608###lif===13###soif===306###eoif===315###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_handler(struct afb_hsrv*,const char*,int(*handler) (struct afb_hreq*, void*),void*,int)");return 1;

}

/** Instrumented function afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int) */
int afb_hsrv_add_alias_root(struct afb_hsrv *hsrv, const char *prefix, struct locale_root *root, int priority, int relax)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)");AKA_fCall++;
		AKA_mark("lis===381###sois===9737###eois===9759###lif===2###soif===125###eoif===147###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)");struct hsrv_alias *da;


		AKA_mark("lis===383###sois===9762###eois===9786###lif===4###soif===150###eoif===174###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)");da = malloc(sizeof *da);

		if (AKA_mark("lis===384###sois===9792###eois===9802###lif===5###soif===180###eoif===190###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)") && (AKA_mark("lis===384###sois===9792###eois===9802###lif===5###soif===180###eoif===190###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)")&&da != NULL)) {
				AKA_mark("lis===385###sois===9808###eois===9824###lif===6###soif===196###eoif===212###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)");da->root = root;

				AKA_mark("lis===386###sois===9827###eois===9845###lif===7###soif===215###eoif===233###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)");da->relax = relax;

				if (AKA_mark("lis===387###sois===9852###eois===9914###lif===8###soif===240###eoif===302###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)") && (AKA_mark("lis===387###sois===9852###eois===9914###lif===8###soif===240###eoif===302###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)")&&afb_hsrv_add_handler(hsrv, prefix, handle_alias, da, priority))) {
						AKA_mark("lis===388###sois===9921###eois===9946###lif===9###soif===309###eoif===334###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)");locale_root_addref(root);

						AKA_mark("lis===389###sois===9950###eois===9959###lif===10###soif===338###eoif===347###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)");return 1;

		}
		else {AKA_mark("lis===-387-###sois===-9852-###eois===-985262-###lif===-8-###soif===-###eoif===-302-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)");}

				AKA_mark("lis===391###sois===9966###eois===9975###lif===12###soif===354###eoif===363###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)");free(da);

	}
	else {AKA_mark("lis===-384-###sois===-9792-###eois===-979210-###lif===-5-###soif===-###eoif===-190-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)");}

		AKA_mark("lis===393###sois===9980###eois===9989###lif===14###soif===368###eoif===377###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias_root(struct afb_hsrv*,const char*,struct locale_root*,int,int)");return 0;

}

/** Instrumented function afb_hsrv_add_alias(struct afb_hsrv*,const char*,int,const char*,int,int) */
int afb_hsrv_add_alias(struct afb_hsrv *hsrv, const char *prefix, int dirfd, const char *alias, int priority, int relax)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias(struct afb_hsrv*,const char*,int,const char*,int,int)");AKA_fCall++;
		AKA_mark("lis===398###sois===10117###eois===10142###lif===2###soif===124###eoif===149###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias(struct afb_hsrv*,const char*,int,const char*,int,int)");struct locale_root *root;

		AKA_mark("lis===399###sois===10144###eois===10151###lif===3###soif===151###eoif===158###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias(struct afb_hsrv*,const char*,int,const char*,int,int)");int rc;


		AKA_mark("lis===401###sois===10154###eois===10197###lif===5###soif===161###eoif===204###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias(struct afb_hsrv*,const char*,int,const char*,int,int)");root = locale_root_create_at(dirfd, alias);

		if (AKA_mark("lis===402###sois===10203###eois===10215###lif===6###soif===210###eoif===222###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias(struct afb_hsrv*,const char*,int,const char*,int,int)") && (AKA_mark("lis===402###sois===10203###eois===10215###lif===6###soif===210###eoif===222###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias(struct afb_hsrv*,const char*,int,const char*,int,int)")&&root == NULL)) {
				AKA_mark("lis===403###sois===10221###eois===10271###lif===7###soif===228###eoif===278###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias(struct afb_hsrv*,const char*,int,const char*,int,int)");ERROR("can't connect to directory %s: %m", alias);

				AKA_mark("lis===404###sois===10274###eois===10281###lif===8###soif===281###eoif===288###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias(struct afb_hsrv*,const char*,int,const char*,int,int)");rc = 0;

	}
	else {
				AKA_mark("lis===406###sois===10294###eois===10360###lif===10###soif===301###eoif===367###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias(struct afb_hsrv*,const char*,int,const char*,int,int)");rc = afb_hsrv_add_alias_root(hsrv, prefix, root, priority, relax);

				AKA_mark("lis===407###sois===10363###eois===10387###lif===11###soif===370###eoif===394###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias(struct afb_hsrv*,const char*,int,const char*,int,int)");locale_root_unref(root);

	}

		AKA_mark("lis===409###sois===10392###eois===10402###lif===13###soif===399###eoif===409###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_alias(struct afb_hsrv*,const char*,int,const char*,int,int)");return rc;

}

/** Instrumented function afb_hsrv_set_cache_timeout(struct afb_hsrv*,int) */
int afb_hsrv_set_cache_timeout(struct afb_hsrv *hsrv, int duration)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/afb_hsrv_set_cache_timeout(struct afb_hsrv*,int)");AKA_fCall++;
		AKA_mark("lis===414###sois===10477###eois===10484###lif===2###soif===71###eoif===78###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_set_cache_timeout(struct afb_hsrv*,int)");int rc;

		AKA_mark("lis===415###sois===10486###eois===10496###lif===3###soif===80###eoif===90###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_set_cache_timeout(struct afb_hsrv*,int)");char *dur;


		AKA_mark("lis===417###sois===10499###eois===10535###lif===5###soif===93###eoif===129###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_set_cache_timeout(struct afb_hsrv*,int)");rc = asprintf(&dur, "%d", duration);

		if (AKA_mark("lis===418###sois===10541###eois===10547###lif===6###soif===135###eoif===141###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_set_cache_timeout(struct afb_hsrv*,int)") && (AKA_mark("lis===418###sois===10541###eois===10547###lif===6###soif===135###eoif===141###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_set_cache_timeout(struct afb_hsrv*,int)")&&rc < 0)) {
		AKA_mark("lis===419###sois===10551###eois===10560###lif===7###soif===145###eoif===154###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_set_cache_timeout(struct afb_hsrv*,int)");return 0;
	}
	else {AKA_mark("lis===-418-###sois===-10541-###eois===-105416-###lif===-6-###soif===-###eoif===-141-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_set_cache_timeout(struct afb_hsrv*,int)");}


		AKA_mark("lis===421###sois===10563###eois===10584###lif===9###soif===157###eoif===178###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_set_cache_timeout(struct afb_hsrv*,int)");free(hsrv->cache_to);

		AKA_mark("lis===422###sois===10586###eois===10607###lif===10###soif===180###eoif===201###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_set_cache_timeout(struct afb_hsrv*,int)");hsrv->cache_to = dur;

		AKA_mark("lis===423###sois===10609###eois===10618###lif===11###soif===203###eoif===212###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_set_cache_timeout(struct afb_hsrv*,int)");return 1;

}

/** Instrumented function afb_hsrv_start(struct afb_hsrv*,unsigned int) */
int afb_hsrv_start(struct afb_hsrv *hsrv, unsigned int connection_timeout)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");AKA_fCall++;
		AKA_mark("lis===428###sois===10700###eois===10718###lif===2###soif===78###eoif===96###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");struct fdev *fdev;

		AKA_mark("lis===429###sois===10720###eois===10745###lif===3###soif===98###eoif===123###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");struct MHD_Daemon *httpd;

		AKA_mark("lis===430###sois===10747###eois===10780###lif===4###soif===125###eoif===158###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");const union MHD_DaemonInfo *info;


		AKA_mark("lis===432###sois===10783###eois===11245###lif===6###soif===161###eoif===623###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");httpd = MHD_start_daemon(
			MHD_USE_EPOLL
			| MHD_ALLOW_UPGRADE
			| MHD_USE_TCP_FASTOPEN
			| MHD_USE_NO_LISTEN_SOCKET
			| MHD_USE_DEBUG
			| MHD_ALLOW_SUSPEND_RESUME,
			0,				/* port */
			new_client_handler, NULL,	/* Tcp Accept call back + extra attribute */
			access_handler, hsrv,	/* Http Request Call back + extra attribute */
			MHD_OPTION_NOTIFY_COMPLETED, end_handler, hsrv,
			MHD_OPTION_CONNECTION_TIMEOUT, connection_timeout,
			MHD_OPTION_END);
	/* options-end */

		if (AKA_mark("lis===446###sois===11270###eois===11283###lif===20###soif===648###eoif===661###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)") && (AKA_mark("lis===446###sois===11270###eois===11283###lif===20###soif===648###eoif===661###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)")&&httpd == NULL)) {
				AKA_mark("lis===447###sois===11289###eois===11322###lif===21###soif===667###eoif===700###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");ERROR("httpStart invalid httpd");

				AKA_mark("lis===448###sois===11325###eois===11334###lif===22###soif===703###eoif===712###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");return 0;

	}
	else {AKA_mark("lis===-446-###sois===-11270-###eois===-1127013-###lif===-20-###soif===-###eoif===-661-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");}


		AKA_mark("lis===451###sois===11340###eois===11400###lif===25###soif===718###eoif===778###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");info = MHD_get_daemon_info(httpd, MHD_DAEMON_INFO_EPOLL_FD);

		if (AKA_mark("lis===452###sois===11406###eois===11418###lif===26###soif===784###eoif===796###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)") && (AKA_mark("lis===452###sois===11406###eois===11418###lif===26###soif===784###eoif===796###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)")&&info == NULL)) {
				AKA_mark("lis===453###sois===11424###eois===11447###lif===27###soif===802###eoif===825###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");MHD_stop_daemon(httpd);

				AKA_mark("lis===454###sois===11450###eois===11479###lif===28###soif===828###eoif===857###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");ERROR("httpStart no pollfd");

				AKA_mark("lis===455###sois===11482###eois===11491###lif===29###soif===860###eoif===869###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");return 0;

	}
	else {AKA_mark("lis===-452-###sois===-11406-###eois===-1140612-###lif===-26-###soif===-###eoif===-796-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");}


		AKA_mark("lis===458###sois===11497###eois===11536###lif===32###soif===875###eoif===914###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");fdev = afb_fdev_create(info->epoll_fd);

		if (AKA_mark("lis===459###sois===11542###eois===11554###lif===33###soif===920###eoif===932###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)") && (AKA_mark("lis===459###sois===11542###eois===11554###lif===33###soif===920###eoif===932###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)")&&fdev == NULL)) {
				AKA_mark("lis===460###sois===11560###eois===11583###lif===34###soif===938###eoif===961###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");MHD_stop_daemon(httpd);

				AKA_mark("lis===461###sois===11586###eois===11633###lif===35###soif===964###eoif===1011###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");ERROR("connection to events for httpd failed");

				AKA_mark("lis===462###sois===11636###eois===11645###lif===36###soif===1014###eoif===1023###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");return 0;

	}
	else {AKA_mark("lis===-459-###sois===-11542-###eois===-1154212-###lif===-33-###soif===-###eoif===-932-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");}

		AKA_mark("lis===464###sois===11650###eois===11678###lif===38###soif===1028###eoif===1056###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");fdev_set_autoclose(fdev, 0);

		AKA_mark("lis===465###sois===11680###eois===11711###lif===39###soif===1058###eoif===1089###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");fdev_set_events(fdev, EPOLLIN);

		AKA_mark("lis===466###sois===11713###eois===11760###lif===40###soif===1091###eoif===1138###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");fdev_set_callback(fdev, listen_callback, hsrv);


		AKA_mark("lis===468###sois===11763###eois===11783###lif===42###soif===1141###eoif===1161###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");hsrv->httpd = httpd;

		AKA_mark("lis===469###sois===11785###eois===11803###lif===43###soif===1163###eoif===1181###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");hsrv->fdev = fdev;

		AKA_mark("lis===470###sois===11805###eois===11814###lif===44###soif===1183###eoif===1192###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_start(struct afb_hsrv*,unsigned int)");return 1;

}

/** Instrumented function afb_hsrv_stop(struct afb_hsrv*) */
void afb_hsrv_stop(struct afb_hsrv *hsrv)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/afb_hsrv_stop(struct afb_hsrv*)");AKA_fCall++;
		if (AKA_mark("lis===475###sois===11867###eois===11885###lif===2###soif===49###eoif===67###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_stop(struct afb_hsrv*)") && (AKA_mark("lis===475###sois===11867###eois===11885###lif===2###soif===49###eoif===67###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_stop(struct afb_hsrv*)")&&hsrv->fdev != NULL)) {
				AKA_mark("lis===476###sois===11891###eois===11914###lif===3###soif===73###eoif===96###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_stop(struct afb_hsrv*)");fdev_unref(hsrv->fdev);

				AKA_mark("lis===477###sois===11917###eois===11935###lif===4###soif===99###eoif===117###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_stop(struct afb_hsrv*)");hsrv->fdev = NULL;

	}
	else {AKA_mark("lis===-475-###sois===-11867-###eois===-1186718-###lif===-2-###soif===-###eoif===-67-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_stop(struct afb_hsrv*)");}

		if (AKA_mark("lis===479###sois===11944###eois===11963###lif===6###soif===126###eoif===145###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_stop(struct afb_hsrv*)") && (AKA_mark("lis===479###sois===11944###eois===11963###lif===6###soif===126###eoif===145###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_stop(struct afb_hsrv*)")&&hsrv->httpd != NULL)) {
		AKA_mark("lis===480###sois===11967###eois===11996###lif===7###soif===149###eoif===178###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_stop(struct afb_hsrv*)");MHD_stop_daemon(hsrv->httpd);
	}
	else {AKA_mark("lis===-479-###sois===-11944-###eois===-1194419-###lif===-6-###soif===-###eoif===-145-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_stop(struct afb_hsrv*)");}

		AKA_mark("lis===481###sois===11998###eois===12017###lif===8###soif===180###eoif===199###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_stop(struct afb_hsrv*)");hsrv->httpd = NULL;

}

/** Instrumented function afb_hsrv_create() */
struct afb_hsrv *afb_hsrv_create()
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/afb_hsrv_create()");AKA_fCall++;
		AKA_mark("lis===486###sois===12059###eois===12120###lif===2###soif===38###eoif===99###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_create()");struct afb_hsrv *result = calloc(1, sizeof(struct afb_hsrv));

		if (AKA_mark("lis===487###sois===12126###eois===12140###lif===3###soif===105###eoif===119###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_create()") && (AKA_mark("lis===487###sois===12126###eois===12140###lif===3###soif===105###eoif===119###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_create()")&&result != NULL)) {
		AKA_mark("lis===488###sois===12144###eois===12165###lif===4###soif===123###eoif===144###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_create()");result->refcount = 1;
	}
	else {AKA_mark("lis===-487-###sois===-12126-###eois===-1212614-###lif===-3-###soif===-###eoif===-119-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_create()");}

		AKA_mark("lis===489###sois===12167###eois===12181###lif===5###soif===146###eoif===160###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_create()");return result;

}

/** Instrumented function afb_hsrv_put(struct afb_hsrv*) */
void afb_hsrv_put(struct afb_hsrv *hsrv)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/afb_hsrv_put(struct afb_hsrv*)");AKA_fCall++;
		AKA_mark("lis===494###sois===12229###eois===12257###lif===2###soif===44###eoif===72###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_put(struct afb_hsrv*)");assert(hsrv->refcount != 0);

		if (AKA_mark("lis===495###sois===12263###eois===12280###lif===3###soif===78###eoif===95###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_put(struct afb_hsrv*)") && (AKA_mark("lis===495###sois===12263###eois===12280###lif===3###soif===78###eoif===95###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_put(struct afb_hsrv*)")&&!--hsrv->refcount)) {
				AKA_mark("lis===496###sois===12286###eois===12306###lif===4###soif===101###eoif===121###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_put(struct afb_hsrv*)");afb_hsrv_stop(hsrv);

				AKA_mark("lis===497###sois===12309###eois===12320###lif===5###soif===124###eoif===135###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_put(struct afb_hsrv*)");free(hsrv);

	}
	else {AKA_mark("lis===-495-###sois===-12263-###eois===-1226317-###lif===-3-###soif===-###eoif===-95-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_put(struct afb_hsrv*)");}

}

static int hsrv_itf_connect(struct hsrv_itf *itf);

/** Instrumented function hsrv_itf_callback(void*,uint32_t,struct fdev*) */
static void hsrv_itf_callback(void *closure, uint32_t revents, struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");AKA_fCall++;
		AKA_mark("lis===505###sois===12464###eois===12495###lif===2###soif===85###eoif===116###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");struct hsrv_itf *itf = closure;

		AKA_mark("lis===506###sois===12497###eois===12509###lif===3###soif===118###eoif===130###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");int fd, sts;

		AKA_mark("lis===507###sois===12511###eois===12532###lif===4###soif===132###eoif===153###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");struct sockaddr addr;

		AKA_mark("lis===508###sois===12534###eois===12552###lif===5###soif===155###eoif===173###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");socklen_t lenaddr;


		if (AKA_mark("lis===510###sois===12559###eois===12584###lif===7###soif===180###eoif===205###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)") && (AKA_mark("lis===510###sois===12559###eois===12584###lif===7###soif===180###eoif===205###isc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)")&&(revents & EPOLLHUP) != 0)) {
				AKA_mark("lis===511###sois===12590###eois===12641###lif===8###soif===211###eoif===262###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");ERROR("disconnection for server %s: %m", itf->uri);

				AKA_mark("lis===512###sois===12644###eois===12666###lif===9###soif===265###eoif===287###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");hsrv_itf_connect(itf);

				AKA_mark("lis===513###sois===12669###eois===12686###lif===10###soif===290###eoif===307###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");fdev_unref(fdev);

	}
	else {
		if (AKA_mark("lis===514###sois===12699###eois===12723###lif===11###soif===320###eoif===344###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)") && (AKA_mark("lis===514###sois===12699###eois===12723###lif===11###soif===320###eoif===344###isc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)")&&(revents & EPOLLIN) != 0)) {
					AKA_mark("lis===515###sois===12729###eois===12762###lif===12###soif===350###eoif===383###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");lenaddr = (socklen_t)sizeof addr;

					AKA_mark("lis===516###sois===12765###eois===12809###lif===13###soif===386###eoif===430###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");fd = accept(fdev_fd(fdev), &addr, &lenaddr);

					if (AKA_mark("lis===517###sois===12816###eois===12822###lif===14###soif===437###eoif===443###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)") && (AKA_mark("lis===517###sois===12816###eois===12822###lif===14###soif===437###eoif===443###isc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)")&&fd < 0)) {
				AKA_mark("lis===518###sois===12827###eois===12880###lif===15###soif===448###eoif===501###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");ERROR("can't accept connection to %s: %m", itf->uri);
			}
			else {
							AKA_mark("lis===520###sois===12893###eois===12956###lif===17###soif===514###eoif===577###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");sts = MHD_add_connection(itf->hsrv->httpd, fd, &addr, lenaddr);

							if (AKA_mark("lis===521###sois===12964###eois===12978###lif===18###soif===585###eoif===599###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)") && (AKA_mark("lis===521###sois===12964###eois===12978###lif===18###soif===585###eoif===599###isc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)")&&sts != MHD_YES)) {
									AKA_mark("lis===522###sois===12986###eois===13045###lif===19###soif===607###eoif===666###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");ERROR("can't add incoming connection to %s: %m", itf->uri);

									AKA_mark("lis===523###sois===13050###eois===13060###lif===20###soif===671###eoif===681###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");close(fd);

			}
				else {AKA_mark("lis===-521-###sois===-12964-###eois===-1296414-###lif===-18-###soif===-###eoif===-599-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");}

		}

	}
		else {AKA_mark("lis===-514-###sois===-12699-###eois===-1269924-###lif===-11-###soif===-###eoif===-344-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_callback(void*,uint32_t,struct fdev*)");}
	}

}

/** Instrumented function hsrv_itf_connect(struct hsrv_itf*) */
static int hsrv_itf_connect(struct hsrv_itf *itf)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");AKA_fCall++;
		AKA_mark("lis===531###sois===13129###eois===13150###lif===2###soif===53###eoif===74###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");struct sockaddr addr;

		AKA_mark("lis===532###sois===13152###eois===13170###lif===3###soif===76###eoif===94###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");socklen_t lenaddr;

		AKA_mark("lis===533###sois===13172###eois===13212###lif===4###soif===96###eoif===136###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];

		AKA_mark("lis===534###sois===13214###eois===13223###lif===5###soif===138###eoif===147###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");int rgni;


		AKA_mark("lis===536###sois===13226###eois===13286###lif===7###soif===150###eoif===210###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");itf->fdev = afb_socket_open_fdev_scheme(itf->uri, 1, "tcp");

		if (AKA_mark("lis===537###sois===13292###eois===13302###lif===8###soif===216###eoif===226###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)") && (AKA_mark("lis===537###sois===13292###eois===13302###lif===8###soif===216###eoif===226###isc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)")&&!itf->fdev)) {
				AKA_mark("lis===538###sois===13308###eois===13350###lif===9###soif===232###eoif===274###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");ERROR("can't create socket %s", itf->uri);

				AKA_mark("lis===539###sois===13353###eois===13362###lif===10###soif===277###eoif===286###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");return 0;

	}
	else {AKA_mark("lis===-537-###sois===-13292-###eois===-1329210-###lif===-8-###soif===-###eoif===-226-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");}

		AKA_mark("lis===541###sois===13367###eois===13403###lif===12###soif===291###eoif===327###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");fdev_set_events(itf->fdev, EPOLLIN);

		AKA_mark("lis===542###sois===13405###eois===13458###lif===13###soif===329###eoif===382###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");fdev_set_callback(itf->fdev, hsrv_itf_callback, itf);

		AKA_mark("lis===543###sois===13460###eois===13490###lif===14###soif===384###eoif===414###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");memset(&addr, 0, sizeof addr);

		AKA_mark("lis===544###sois===13492###eois===13525###lif===15###soif===416###eoif===449###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");lenaddr = (socklen_t)sizeof addr;

		AKA_mark("lis===545###sois===13527###eois===13576###lif===16###soif===451###eoif===500###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");getsockname(fdev_fd(itf->fdev), &addr, &lenaddr);

		if (AKA_mark("lis===546###sois===13582###eois===13657###lif===17###soif===506###eoif===581###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)") && ((AKA_mark("lis===546###sois===13582###eois===13607###lif===17###soif===506###eoif===531###isc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)")&&addr.sa_family == AF_INET)	&&(AKA_mark("lis===546###sois===13611###eois===13657###lif===17###soif===535###eoif===581###isc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)")&&!((struct sockaddr_in*)&addr)->sin_addr.s_addr))) {
				AKA_mark("lis===547###sois===13663###eois===13694###lif===18###soif===587###eoif===618###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");strncpy(hbuf, "*", NI_MAXHOST);

				AKA_mark("lis===548###sois===13697###eois===13769###lif===19###soif===621###eoif===693###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");sprintf(sbuf, "%d", (int)ntohs(((struct sockaddr_in*)&addr)->sin_port));

	}
	else {
				AKA_mark("lis===550###sois===13782###eois===13871###lif===21###soif===706###eoif===795###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");rgni = getnameinfo(&addr, lenaddr, hbuf, sizeof hbuf, sbuf, sizeof sbuf, NI_NUMERICSERV);

				if (AKA_mark("lis===551###sois===13878###eois===13887###lif===22###soif===802###eoif===811###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)") && (AKA_mark("lis===551###sois===13878###eois===13887###lif===22###soif===802###eoif===811###isc===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)")&&rgni != 0)) {
						AKA_mark("lis===552###sois===13894###eois===13957###lif===23###soif===818###eoif===881###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");ERROR("getnameinfo returned %d: %s", rgni, gai_strerror(rgni));

						AKA_mark("lis===553###sois===13961###eois===13985###lif===24###soif===885###eoif===909###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");hbuf[0] = sbuf[0] = '?';

						AKA_mark("lis===554###sois===13989###eois===14011###lif===25###soif===913###eoif===935###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");hbuf[1] = sbuf[1] = 0;

		}
		else {AKA_mark("lis===-551-###sois===-13878-###eois===-138789-###lif===-22-###soif===-###eoif===-811-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");}

	}

		AKA_mark("lis===557###sois===14020###eois===14068###lif===28###soif===944###eoif===992###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");NOTICE("Listening interface %s:%s", hbuf, sbuf);

		AKA_mark("lis===558###sois===14070###eois===14079###lif===29###soif===994###eoif===1003###ins===true###function===./app-framework-binder/src/afb-hsrv.c/hsrv_itf_connect(struct hsrv_itf*)");return 1;

}

/** Instrumented function afb_hsrv_add_interface(struct afb_hsrv*,const char*) */
int afb_hsrv_add_interface(struct afb_hsrv *hsrv, const char *uri)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface(struct afb_hsrv*,const char*)");AKA_fCall++;
		AKA_mark("lis===563###sois===14153###eois===14174###lif===2###soif===70###eoif===91###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface(struct afb_hsrv*,const char*)");struct hsrv_itf *itf;


		AKA_mark("lis===565###sois===14177###eois===14221###lif===4###soif===94###eoif===138###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface(struct afb_hsrv*,const char*)");itf = malloc(sizeof *itf + 1 + strlen(uri));

		if (AKA_mark("lis===566###sois===14227###eois===14238###lif===5###soif===144###eoif===155###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface(struct afb_hsrv*,const char*)") && (AKA_mark("lis===566###sois===14227###eois===14238###lif===5###soif===144###eoif===155###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface(struct afb_hsrv*,const char*)")&&itf == NULL)) {
		AKA_mark("lis===567###sois===14242###eois===14252###lif===6###soif===159###eoif===169###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface(struct afb_hsrv*,const char*)");return -1;
	}
	else {AKA_mark("lis===-566-###sois===-14227-###eois===-1422711-###lif===-5-###soif===-###eoif===-155-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface(struct afb_hsrv*,const char*)");}


		AKA_mark("lis===569###sois===14255###eois===14272###lif===8###soif===172###eoif===189###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface(struct afb_hsrv*,const char*)");itf->hsrv = hsrv;

		AKA_mark("lis===570###sois===14274###eois===14291###lif===9###soif===191###eoif===208###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface(struct afb_hsrv*,const char*)");itf->fdev = NULL;

		AKA_mark("lis===571###sois===14293###eois===14315###lif===10###soif===210###eoif===232###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface(struct afb_hsrv*,const char*)");strcpy(itf->uri, uri);

		AKA_mark("lis===572###sois===14317###eois===14346###lif===11###soif===234###eoif===263###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface(struct afb_hsrv*,const char*)");itf->next = hsrv->interfaces;

		AKA_mark("lis===573###sois===14348###eois===14371###lif===12###soif===265###eoif===288###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface(struct afb_hsrv*,const char*)");hsrv->interfaces = itf;

		AKA_mark("lis===574###sois===14373###eois===14402###lif===13###soif===290###eoif===319###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface(struct afb_hsrv*,const char*)");return hsrv_itf_connect(itf);

}

/** Instrumented function afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t) */
int afb_hsrv_add_interface_tcp(struct afb_hsrv *hsrv, const char *itf, uint16_t port)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===579###sois===14495###eois===14502###lif===2###soif===89###eoif===96###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)");int rc;

		AKA_mark("lis===580###sois===14504###eois===14522###lif===3###soif===98###eoif===116###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)");char buffer[1024];


		if (AKA_mark("lis===582###sois===14529###eois===14540###lif===5###soif===123###eoif===134###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)") && (AKA_mark("lis===582###sois===14529###eois===14540###lif===5###soif===123###eoif===134###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)")&&itf == NULL)) {
				AKA_mark("lis===583###sois===14546###eois===14556###lif===6###soif===140###eoif===150###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)");itf = "*";
 /* awaiting getifaddrs impl */
	}
	else {AKA_mark("lis===-582-###sois===-14529-###eois===-1452911-###lif===-5-###soif===-###eoif===-134-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)");}

		AKA_mark("lis===585###sois===14592###eois===14658###lif===8###soif===186###eoif===252###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)");rc = snprintf(buffer, sizeof buffer, "tcp:%s:%d", itf, (int)port);

		if (AKA_mark("lis===586###sois===14664###eois===14698###lif===9###soif===258###eoif===292###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)") && ((AKA_mark("lis===586###sois===14664###eois===14670###lif===9###soif===258###eoif===264###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)")&&rc < 0)	||(AKA_mark("lis===586###sois===14674###eois===14698###lif===9###soif===268###eoif===292###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)")&&rc >= (int)sizeof buffer))) {
				if (AKA_mark("lis===587###sois===14708###eois===14714###lif===10###soif===302###eoif===308###ifc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)") && (AKA_mark("lis===587###sois===14708###eois===14714###lif===10###soif===302###eoif===308###isc===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)")&&rc > 0)) {
			AKA_mark("lis===588###sois===14719###eois===14734###lif===11###soif===313###eoif===328###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)");errno = EINVAL;
		}
		else {AKA_mark("lis===-587-###sois===-14708-###eois===-147086-###lif===-10-###soif===-###eoif===-308-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)");}

				AKA_mark("lis===589###sois===14737###eois===14746###lif===12###soif===331###eoif===340###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)");return 0;

	}
	else {AKA_mark("lis===-586-###sois===-14664-###eois===-1466434-###lif===-9-###soif===-###eoif===-292-###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)");}

		AKA_mark("lis===591###sois===14751###eois===14795###lif===14###soif===345###eoif===389###ins===true###function===./app-framework-binder/src/afb-hsrv.c/afb_hsrv_add_interface_tcp(struct afb_hsrv*,const char*,uint16_t)");return afb_hsrv_add_interface(hsrv, buffer);

}

#endif

