/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_FDEV_EPOLL_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_FDEV_EPOLL_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <unistd.h>
#include <sys/epoll.h>

#define FDEV_PROVIDER
/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_H_
#define AKA_INCLUDE__FDEV_H_
#include "fdev.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_EPOLL_H_
#define AKA_INCLUDE__FDEV_EPOLL_H_
#include "fdev-epoll.akaignore.h"
#endif


/*
 * For sake of simplicity there is no struct fdev_epoll.
 * Instead, the file descriptor of the internal epoll is used
 * and wrapped in a pseudo pointer to a pseudo struct.
 */
/** Instrumented function epollfd(fdev_epoll) */
#define epollfd(fdev_epoll)  ((int)(intptr_t)fdev_epoll)

/*
 * disable callback for fdev
 *
 * refs to fdev must not be counted here
 */
/** Instrumented function disable(void*,const struct fdev*) */
static void disable(void *closure, const struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/fdev-epoll.c/disable(void*,const struct fdev*)");AKA_fCall++;
		AKA_mark("lis===40###sois===1153###eois===1193###lif===2###soif===63###eoif===103###ins===true###function===./app-framework-binder/src/fdev-epoll.c/disable(void*,const struct fdev*)");struct fdev_epoll *fdev_epoll = closure;

		AKA_mark("lis===41###sois===1195###eois===1259###lif===3###soif===105###eoif===169###ins===true###function===./app-framework-binder/src/fdev-epoll.c/disable(void*,const struct fdev*)");epoll_ctl(epollfd(fdev_epoll), EPOLL_CTL_DEL, fdev_fd(fdev), 0);

}

/*
 * enable callback for fdev
 *
 * refs to fdev must not be counted here
 */
/** Instrumented function enable_or_update(void*,const struct fdev*,int,int) */
static void enable_or_update(void *closure, const struct fdev *fdev, int op, int err)
{AKA_mark("Calling: ./app-framework-binder/src/fdev-epoll.c/enable_or_update(void*,const struct fdev*,int,int)");AKA_fCall++;
		AKA_mark("lis===51###sois===1431###eois===1471###lif===2###soif===89###eoif===129###ins===true###function===./app-framework-binder/src/fdev-epoll.c/enable_or_update(void*,const struct fdev*,int,int)");struct fdev_epoll *fdev_epoll = closure;

		AKA_mark("lis===52###sois===1473###eois===1498###lif===3###soif===131###eoif===156###ins===true###function===./app-framework-binder/src/fdev-epoll.c/enable_or_update(void*,const struct fdev*,int,int)");struct epoll_event event;

		AKA_mark("lis===53###sois===1500###eois===1511###lif===4###soif===158###eoif===169###ins===true###function===./app-framework-binder/src/fdev-epoll.c/enable_or_update(void*,const struct fdev*,int,int)");int rc, fd;


		AKA_mark("lis===55###sois===1514###eois===1533###lif===6###soif===172###eoif===191###ins===true###function===./app-framework-binder/src/fdev-epoll.c/enable_or_update(void*,const struct fdev*,int,int)");fd = fdev_fd(fdev);

		AKA_mark("lis===56###sois===1535###eois===1568###lif===7###soif===193###eoif===226###ins===true###function===./app-framework-binder/src/fdev-epoll.c/enable_or_update(void*,const struct fdev*,int,int)");event.events = fdev_events(fdev);

		AKA_mark("lis===57###sois===1570###eois===1599###lif===8###soif===228###eoif===257###ins===true###function===./app-framework-binder/src/fdev-epoll.c/enable_or_update(void*,const struct fdev*,int,int)");event.data.ptr = (void*)fdev;

		AKA_mark("lis===58###sois===1601###eois===1653###lif===9###soif===259###eoif===311###ins===true###function===./app-framework-binder/src/fdev-epoll.c/enable_or_update(void*,const struct fdev*,int,int)");rc = epoll_ctl(epollfd(fdev_epoll), op, fd, &event);

		if (AKA_mark("lis===59###sois===1659###eois===1681###lif===10###soif===317###eoif===339###ifc===true###function===./app-framework-binder/src/fdev-epoll.c/enable_or_update(void*,const struct fdev*,int,int)") && ((AKA_mark("lis===59###sois===1659###eois===1665###lif===10###soif===317###eoif===323###isc===true###function===./app-framework-binder/src/fdev-epoll.c/enable_or_update(void*,const struct fdev*,int,int)")&&rc < 0)	&&(AKA_mark("lis===59###sois===1669###eois===1681###lif===10###soif===327###eoif===339###isc===true###function===./app-framework-binder/src/fdev-epoll.c/enable_or_update(void*,const struct fdev*,int,int)")&&errno == err))) {
		AKA_mark("lis===60###sois===1685###eois===1766###lif===11###soif===343###eoif===424###ins===true###function===./app-framework-binder/src/fdev-epoll.c/enable_or_update(void*,const struct fdev*,int,int)");epoll_ctl(epollfd(fdev_epoll), (EPOLL_CTL_MOD + EPOLL_CTL_ADD) - op, fd, &event);
	}
	else {AKA_mark("lis===-59-###sois===-1659-###eois===-165922-###lif===-10-###soif===-###eoif===-339-###ins===true###function===./app-framework-binder/src/fdev-epoll.c/enable_or_update(void*,const struct fdev*,int,int)");}

}

/*
 * enable callback for fdev
 *
 * refs to fdev must not be counted here
 */
/** Instrumented function enable(void*,const struct fdev*) */
static void enable(void *closure, const struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/fdev-epoll.c/enable(void*,const struct fdev*)");AKA_fCall++;
		AKA_mark("lis===70###sois===1911###eois===1966###lif===2###soif===62###eoif===117###ins===true###function===./app-framework-binder/src/fdev-epoll.c/enable(void*,const struct fdev*)");enable_or_update(closure, fdev, EPOLL_CTL_ADD, EEXIST);

}

/*
 * update callback for fdev
 *
 * refs to fdev must not be counted here
 */
/** Instrumented function update(void*,const struct fdev*) */
static void update(void *closure, const struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/fdev-epoll.c/update(void*,const struct fdev*)");AKA_fCall++;
		AKA_mark("lis===80###sois===2111###eois===2166###lif===2###soif===62###eoif===117###ins===true###function===./app-framework-binder/src/fdev-epoll.c/update(void*,const struct fdev*)");enable_or_update(closure, fdev, EPOLL_CTL_MOD, ENOENT);

}

/*
 * unref is not handled here
 */
static struct fdev_itf itf =
{
	.unref = 0,
	.disable = disable,
	.enable = enable,
	.update = update
};

/*
 * create an fdev_epoll
 */
/** Instrumented function fdev_epoll_create() */
struct fdev_epoll *fdev_epoll_create()
{AKA_mark("Calling: ./app-framework-binder/src/fdev-epoll.c/fdev_epoll_create()");AKA_fCall++;
		AKA_mark("lis===99###sois===2385###eois===2423###lif===2###soif===42###eoif===80###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_create()");int fd = epoll_create1(EPOLL_CLOEXEC);

		if (AKA_mark("lis===100###sois===2429###eois===2432###lif===3###soif===86###eoif===89###ifc===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_create()") && (AKA_mark("lis===100###sois===2429###eois===2432###lif===3###soif===86###eoif===89###isc===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_create()")&&!fd)) {
				AKA_mark("lis===101###sois===2438###eois===2451###lif===4###soif===95###eoif===108###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_create()");fd = dup(fd);

				AKA_mark("lis===102###sois===2454###eois===2463###lif===5###soif===111###eoif===120###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_create()");close(0);

	}
	else {AKA_mark("lis===-100-###sois===-2429-###eois===-24293-###lif===-3-###soif===-###eoif===-89-###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_create()");}

		AKA_mark("lis===104###sois===2468###eois===2521###lif===7###soif===125###eoif===178###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_create()");return fd < 0 ? 0 : (struct fdev_epoll*)(intptr_t)fd;

}

/*
 * destroy the fdev_epoll
 */
/** Instrumented function fdev_epoll_destroy(struct fdev_epoll*) */
void fdev_epoll_destroy(struct fdev_epoll *fdev_epoll)
{AKA_mark("Calling: ./app-framework-binder/src/fdev-epoll.c/fdev_epoll_destroy(struct fdev_epoll*)");AKA_fCall++;
		AKA_mark("lis===112###sois===2616###eois===2643###lif===2###soif===58###eoif===85###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_destroy(struct fdev_epoll*)");close(epollfd(fdev_epoll));

}

/*
 * get pollable fd for the fdev_epoll
 */
/** Instrumented function fdev_epoll_fd(struct fdev_epoll*) */
int fdev_epoll_fd(struct fdev_epoll *fdev_epoll)
{AKA_mark("Calling: ./app-framework-binder/src/fdev-epoll.c/fdev_epoll_fd(struct fdev_epoll*)");AKA_fCall++;
		AKA_mark("lis===120###sois===2744###eois===2771###lif===2###soif===52###eoif===79###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_fd(struct fdev_epoll*)");return epollfd(fdev_epoll);

}

/*
 * create an fdev linked to the 'fdev_epoll' for 'fd'
 */
/** Instrumented function fdev_epoll_add(struct fdev_epoll*,int) */
struct fdev *fdev_epoll_add(struct fdev_epoll *fdev_epoll, int fd)
{AKA_mark("Calling: ./app-framework-binder/src/fdev-epoll.c/fdev_epoll_add(struct fdev_epoll*,int)");AKA_fCall++;
		AKA_mark("lis===128###sois===2906###eois===2924###lif===2###soif===70###eoif===88###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_add(struct fdev_epoll*,int)");struct fdev *fdev;


		AKA_mark("lis===130###sois===2927###eois===2950###lif===4###soif===91###eoif===114###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_add(struct fdev_epoll*,int)");fdev = fdev_create(fd);

		if (AKA_mark("lis===131###sois===2956###eois===2960###lif===5###soif===120###eoif===124###ifc===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_add(struct fdev_epoll*,int)") && (AKA_mark("lis===131###sois===2956###eois===2960###lif===5###soif===120###eoif===124###isc===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_add(struct fdev_epoll*,int)")&&fdev)) {
		AKA_mark("lis===132###sois===2964###eois===3001###lif===6###soif===128###eoif===165###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_add(struct fdev_epoll*,int)");fdev_set_itf(fdev, &itf, fdev_epoll);
	}
	else {AKA_mark("lis===-131-###sois===-2956-###eois===-29564-###lif===-5-###soif===-###eoif===-124-###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_add(struct fdev_epoll*,int)");}

		AKA_mark("lis===133###sois===3003###eois===3015###lif===7###soif===167###eoif===179###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_add(struct fdev_epoll*,int)");return fdev;

}

/*
 * get pollable fd for the fdev_epoll
 */
/** Instrumented function fdev_epoll_wait_and_dispatch(struct fdev_epoll*,int) */
int fdev_epoll_wait_and_dispatch(struct fdev_epoll *fdev_epoll, int timeout_ms)
{AKA_mark("Calling: ./app-framework-binder/src/fdev-epoll.c/fdev_epoll_wait_and_dispatch(struct fdev_epoll*,int)");AKA_fCall++;
		AKA_mark("lis===141###sois===3147###eois===3165###lif===2###soif===83###eoif===101###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_wait_and_dispatch(struct fdev_epoll*,int)");struct fdev *fdev;

		AKA_mark("lis===142###sois===3167###eois===3193###lif===3###soif===103###eoif===129###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_wait_and_dispatch(struct fdev_epoll*,int)");struct epoll_event events;

		AKA_mark("lis===143###sois===3195###eois===3202###lif===4###soif===131###eoif===138###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_wait_and_dispatch(struct fdev_epoll*,int)");int rc;


		AKA_mark("lis===145###sois===3205###eois===3288###lif===6###soif===141###eoif===224###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_wait_and_dispatch(struct fdev_epoll*,int)");rc = epoll_wait(epollfd(fdev_epoll), &events, 1, timeout_ms < 0 ? -1 : timeout_ms);

		if (AKA_mark("lis===146###sois===3294###eois===3301###lif===7###soif===230###eoif===237###ifc===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_wait_and_dispatch(struct fdev_epoll*,int)") && (AKA_mark("lis===146###sois===3294###eois===3301###lif===7###soif===230###eoif===237###isc===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_wait_and_dispatch(struct fdev_epoll*,int)")&&rc == 1)) {
				AKA_mark("lis===147###sois===3307###eois===3330###lif===8###soif===243###eoif===266###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_wait_and_dispatch(struct fdev_epoll*,int)");fdev = events.data.ptr;

				AKA_mark("lis===148###sois===3333###eois===3368###lif===9###soif===269###eoif===304###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_wait_and_dispatch(struct fdev_epoll*,int)");fdev_dispatch(fdev, events.events);

	}
	else {AKA_mark("lis===-146-###sois===-3294-###eois===-32947-###lif===-7-###soif===-###eoif===-237-###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_wait_and_dispatch(struct fdev_epoll*,int)");}

		AKA_mark("lis===150###sois===3373###eois===3383###lif===11###soif===309###eoif===319###ins===true###function===./app-framework-binder/src/fdev-epoll.c/fdev_epoll_wait_and_dispatch(struct fdev_epoll*,int)");return rc;

}


#endif

