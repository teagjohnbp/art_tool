/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_ARGS_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_ARGS_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <limits.h>
#include <unistd.h>
#include <ctype.h>

#include <json-c/json.h>
#if !defined(JSON_C_TO_STRING_NOSLASHESCAPE)
#define JSON_C_TO_STRING_NOSLASHESCAPE 0
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_ARGS_H_
#define AKA_INCLUDE__AFB_ARGS_H_
#include "afb-args.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HOOK_FLAGS_H_
#define AKA_INCLUDE__AFB_HOOK_FLAGS_H_
#include "afb-hook-flags.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__WRAP_JSON_H_
#define AKA_INCLUDE__WRAP_JSON_H_
#include "wrap-json.akaignore.h"
#endif


#define _d2s_(x)  #x
/** Instrumented function d2s(x) */
#define d2s(x)    _d2s_(x)

#if !defined(AFB_VERSION)
#error "you should define AFB_VERSION"
#endif

/**
 * The default timeout of sessions in seconds
 */
#define DEFAULT_SESSION_TIMEOUT		32000000

/**
 * The default timeout of api calls in seconds
 */
#define DEFAULT_API_TIMEOUT		20

/**
 * The default timeout of cache in seconds
 */
#define DEFAULT_CACHE_TIMEOUT		100000

/**
 * The default maximum count of sessions
 */
#define DEFAULT_MAX_SESSION_COUNT       200

/**
 * The default HTTP port to serve
 */
#define DEFAULT_HTTP_PORT		1234

// Define command line option
#define SET_BACKGROUND       1
#define SET_FOREGROUND       2
#define SET_ROOT_DIR         3
#define SET_ROOT_BASE        4
#define SET_ROOT_API         5
#define ADD_ALIAS            6

#define SET_CACHE_TIMEOUT    7

#if WITH_DYNAMIC_BINDING
#define ADD_LDPATH          10
#define ADD_WEAK_LDPATH     11
#define SET_NO_LDPATH       12
#endif
#define SET_API_TIMEOUT     13
#define SET_SESSION_TIMEOUT 14

#define SET_SESSIONMAX      15

#define ADD_WS_CLIENT       16
#define ADD_WS_SERVICE      17

#define SET_ROOT_HTTP       18

#define SET_NO_HTTPD        19

#define SET_TRACEEVT        20
#define SET_TRACESES        21
#define SET_TRACEREQ        22
#define SET_TRACEAPI        23
#define SET_TRACEGLOB       24
#if !defined(REMOVE_LEGACY_TRACE)
#define SET_TRACEDITF       25
#define SET_TRACESVC        26
#endif
#define SET_TRAP_FAULTS     27
#define ADD_CALL            28
#if WITH_DBUS_TRANSPARENCY
#   define ADD_DBUS_CLIENT  30
#   define ADD_DBUS_SERVICE 31
#endif

#define ADD_AUTO_API       'A'
#if WITH_DYNAMIC_BINDING
#define ADD_BINDING        'b'
#endif
#define SET_CONFIG         'C'
#define SET_COLOR          'c'
#define SET_DAEMON         'D'
#define SET_EXEC           'e'
#define GET_HELP           'h'
#define ADD_INTERFACE      'i'
#define SET_LOG            'l'
#if defined(WITH_MONITORING_OPTION)
#define SET_MONITORING     'M'
#endif
#define SET_NAME           'n'
#define SET_OUTPUT         'o'
#define SET_PORT           'p'
#define SET_QUIET          'q'
#define SET_RANDOM_TOKEN   'r'
#define ADD_SET            's'
#define SET_TOKEN          't'
#define SET_UPLOAD_DIR     'u'
#define GET_VERSION        'V'
#define SET_VERBOSE        'v'
#define SET_WORK_DIR       'w'
#define DUMP_CONFIG        'Z'

/* structure for defining of options */
struct option_desc {
	int id;		/* id of the option         */
	int has_arg;	/* is a value required      */
	char *name;	/* long name of the option  */
	char *help;	/* help text                */
};

/* definition of options */
/** Guard statement to avoid multiple declaration */
#ifndef AKA_GLOBAL_HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_ARGS_C_OPTDEFS
#define AKA_GLOBAL_HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_ARGS_C_OPTDEFS
static struct option_desc optdefs[] = {
/* *INDENT-OFF* */
	{SET_VERBOSE,         0, "verbose",     "Verbose Mode, repeat to increase verbosity"},
	{SET_COLOR,           0, "color",       "Colorize the ouput"},
	{SET_QUIET,           0, "quiet",       "Quiet Mode, repeat to decrease verbosity"},
	{SET_LOG,             1, "log",         "Tune log level"},

	{SET_FOREGROUND,      0, "foreground",  "Get all in foreground mode"},
	{SET_BACKGROUND,      0, "background",  "Get all in background mode"},
	{SET_DAEMON,          0, "daemon",      "Get all in background mode"},

	{SET_NAME,            1, "name",        "Set the visible name"},

	{SET_PORT,            1, "port",        "HTTP listening TCP port of all interfaces [default " d2s(DEFAULT_HTTP_PORT) "]"},
	{ADD_INTERFACE,       1, "interface",   "Add HTTP listening interface (ex: tcp:localhost:8080)"},
	{SET_ROOT_HTTP,       1, "roothttp",    "HTTP Root Directory [default no root http (files not served but apis still available)]"},
	{SET_ROOT_BASE,       1, "rootbase",    "Angular Base Root URL [default /opa]"},
	{SET_ROOT_API,        1, "rootapi",     "HTML Root API URL [default /api]"},
	{ADD_ALIAS,           1, "alias",       "Multiple url map outside of rootdir [eg: --alias=/icons:/usr/share/icons]"},

	{SET_API_TIMEOUT,     1, "apitimeout",  "Binding API timeout in seconds [default " d2s(DEFAULT_API_TIMEOUT) "]"},
	{SET_SESSION_TIMEOUT, 1, "cntxtimeout", "Client Session Context Timeout [default " d2s(DEFAULT_SESSION_TIMEOUT) "]"},
	{SET_CACHE_TIMEOUT,   1, "cache-eol",   "Client cache end of live [default " d2s(DEFAULT_CACHE_TIMEOUT) "]"},

	{SET_WORK_DIR,        1, "workdir",     "Set the working directory [default: $PWD or current working directory]"},
	{SET_UPLOAD_DIR,      1, "uploaddir",   "Directory for uploading files [default: workdir] relative to workdir"},
	{SET_ROOT_DIR,        1, "rootdir",     "Root Directory of the application [default: workdir] relative to workdir"},

#if WITH_DYNAMIC_BINDING

	{ADD_LDPATH,          1, "ldpaths",     "Load bindings from dir1:dir2:..."
#if defined(INTRINSIC_BINDING_DIR)
	                                        "[default = " INTRINSIC_BINDING_DIR "]"
#endif
	                                        },
	{ADD_BINDING,         1, "binding",     "Load the binding of path"},
	{ADD_WEAK_LDPATH,     1, "weak-ldpaths","Same as --ldpaths but ignore errors"},
	{SET_NO_LDPATH,       0, "no-ldpaths",  "Discard default ldpaths loading"},
#endif
	{SET_TOKEN,           1, "token",       "Initial Secret [default=random, use --token="" to allow any token]"},
	{SET_RANDOM_TOKEN,    0, "random-token","Enforce a random token"},

	{GET_VERSION,         0, "version",     "Display version and copyright"},
	{GET_HELP,            0, "help",        "Display this help"},

#if WITH_DBUS_TRANSPARENCY
	{ADD_DBUS_CLIENT,     1, "dbus-client", "Bind to an afb service through dbus"},
	{ADD_DBUS_SERVICE,    1, "dbus-server", "Provide an afb service through dbus"},
#endif
	{ADD_WS_CLIENT,       1, "ws-client",   "Bind to an afb service through websocket"},
	{ADD_WS_SERVICE,      1, "ws-server",   "Provide an afb service through websockets"},

	{ADD_AUTO_API,        1, "auto-api",    "Automatic load of api of the given directory"},

	{SET_SESSIONMAX,      1, "session-max", "Max count of session simultaneously [default " d2s(DEFAULT_MAX_SESSION_COUNT) "]"},

#if WITH_AFB_HOOK
	{SET_TRACEREQ,        1, "tracereq",    "Log the requests: none, common, extra, all"},
	{SET_TRACEEVT,        1, "traceevt",    "Log the events: none, common, extra, all"},
	{SET_TRACESES,        1, "traceses",    "Log the sessions: none, all"},
	{SET_TRACEAPI,        1, "traceapi",    "Log the apis: none, common, api, event, all"},
	{SET_TRACEGLOB,       1, "traceglob",   "Log the globals: none, all"},
#if !defined(REMOVE_LEGACY_TRACE)
	{SET_TRACEDITF,       1, "traceditf",   "Log the daemons: no, common, all"},
	{SET_TRACESVC,        1, "tracesvc",    "Log the services: no, all"},
#endif
#endif

	{ADD_CALL,            1, "call",        "Call at start, format of val: API/VERB:json-args"},

	{SET_NO_HTTPD,        0, "no-httpd",    "Forbid HTTP service"},
	{SET_EXEC,            0, "exec",        "Execute the remaining arguments"},

#if defined(WITH_MONITORING_OPTION)
	{SET_MONITORING,      0, "monitoring",  "Enable HTTP monitoring at <ROOT>/monitoring/"},
#endif

	{SET_CONFIG,          1, "config",      "Load options from the given config file"},
	{DUMP_CONFIG,         0, "dump-config", "Dump the config to stdout and exit"},

	{ADD_SET,             1, "set",         "Set parameters ([API]/[KEY]:JSON or {\"API\":{\"KEY\":JSON}}" },
	{SET_OUTPUT,          1, "output",      "Redirect stdout and stderr to output file (when --daemon)"},

	{SET_TRAP_FAULTS,     1, "trap-faults", "Trap faults: on, off, yes, no, true, false, 1, 0 (default: true)"},

	{0, 0, NULL, NULL}
/* *INDENT-ON* */
};
#endif

#if defined(WITH_MONITORING_OPTION)
static const char MONITORING_ALIAS[] = "/monitoring:"INTRINSIC_BINDING_DIR"/monitoring";
#endif

static const struct {
	int optid;
	int valdef;
} default_optint_values[] = {
	{ SET_API_TIMEOUT,	DEFAULT_API_TIMEOUT },
	{ SET_CACHE_TIMEOUT,	DEFAULT_CACHE_TIMEOUT },
	{ SET_SESSION_TIMEOUT,	DEFAULT_SESSION_TIMEOUT },
	{ SET_SESSIONMAX,	DEFAULT_MAX_SESSION_COUNT }
};

static const struct {
	int optid;
	const char *valdef;
} default_optstr_values[] = {
	{ SET_WORK_DIR,		"." },
	{ SET_ROOT_DIR,		"." },
	{ SET_UPLOAD_DIR,	"." },
	{ SET_ROOT_BASE,	"/opa" },
	{ SET_ROOT_API,		"/api" }
};

/**********************************
* preparing items
***********************************/

static char *shortopts = NULL;
static int *id2idx = NULL;

/** Instrumented function oomchk(void*) */
static void *oomchk(void *ptr)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/oomchk(void*)");AKA_fCall++;
		if (AKA_mark("lis===272###sois===9342###eois===9346###lif===2###soif===38###eoif===42###ifc===true###function===./app-framework-binder/src/afb-args.c/oomchk(void*)") && (AKA_mark("lis===272###sois===9342###eois===9346###lif===2###soif===38###eoif===42###isc===true###function===./app-framework-binder/src/afb-args.c/oomchk(void*)")&&!ptr)) {
				AKA_mark("lis===273###sois===9352###eois===9375###lif===3###soif===48###eoif===71###ins===true###function===./app-framework-binder/src/afb-args.c/oomchk(void*)");ERROR("Out of memory");

				AKA_mark("lis===274###sois===9378###eois===9386###lif===4###soif===74###eoif===82###ins===true###function===./app-framework-binder/src/afb-args.c/oomchk(void*)");exit(1);

	}
	else {AKA_mark("lis===-272-###sois===-9342-###eois===-93424-###lif===-2-###soif===-###eoif===-42-###ins===true###function===./app-framework-binder/src/afb-args.c/oomchk(void*)");}

		AKA_mark("lis===276###sois===9391###eois===9402###lif===6###soif===87###eoif===98###ins===true###function===./app-framework-binder/src/afb-args.c/oomchk(void*)");return ptr;

}

/** Instrumented function is_short_option(int) */
static char is_short_option(int val)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/is_short_option(int)");AKA_fCall++;
		AKA_mark("lis===281###sois===9446###eois===9540###lif===2###soif===40###eoif===134###ins===true###function===./app-framework-binder/src/afb-args.c/is_short_option(int)");return (val >= 'a' && val <= 'z') || (val >= 'A' && val <= 'Z') || (val >= '0' && val <= '9');

}

/** Instrumented function init_options() */
static void init_options()
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/init_options()");AKA_fCall++;
		AKA_mark("lis===286###sois===9574###eois===9592###lif===2###soif===30###eoif===48###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");int i, ns, mi, id;


		if (AKA_mark("lis===288###sois===9599###eois===9609###lif===4###soif===55###eoif===65###ifc===true###function===./app-framework-binder/src/afb-args.c/init_options()") && (AKA_mark("lis===288###sois===9599###eois===9609###lif===4###soif===55###eoif===65###isc===true###function===./app-framework-binder/src/afb-args.c/init_options()")&&!shortopts)) {
				AKA_mark("lis===289###sois===9615###eois===9622###lif===5###soif===71###eoif===78###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");ns = 2;

				AKA_mark("lis===290###sois===9625###eois===9633###lif===6###soif===81###eoif===89###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");mi = -1;

				AKA_mark("lis===291###sois===9641###eois===9648###lif===7###soif===97###eoif===104###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");for (i = 0 ;AKA_mark("lis===291###sois===9649###eois===9664###lif===7###soif===105###eoif===120###ifc===true###function===./app-framework-binder/src/afb-args.c/init_options()") && AKA_mark("lis===291###sois===9649###eois===9664###lif===7###soif===105###eoif===120###isc===true###function===./app-framework-binder/src/afb-args.c/init_options()")&&optdefs[i].name;({AKA_mark("lis===291###sois===9667###eois===9670###lif===7###soif===123###eoif===126###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");i++;})) {
						AKA_mark("lis===292###sois===9677###eois===9696###lif===8###soif===133###eoif===152###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");id = optdefs[i].id;

						if (AKA_mark("lis===293###sois===9704###eois===9711###lif===9###soif===160###eoif===167###ifc===true###function===./app-framework-binder/src/afb-args.c/init_options()") && (AKA_mark("lis===293###sois===9704###eois===9711###lif===9###soif===160###eoif===167###isc===true###function===./app-framework-binder/src/afb-args.c/init_options()")&&id > mi)) {
				AKA_mark("lis===294###sois===9717###eois===9725###lif===10###soif===173###eoif===181###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");mi = id;
			}
			else {AKA_mark("lis===-293-###sois===-9704-###eois===-97047-###lif===-9-###soif===-###eoif===-167-###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");}

						if (AKA_mark("lis===295###sois===9733###eois===9752###lif===11###soif===189###eoif===208###ifc===true###function===./app-framework-binder/src/afb-args.c/init_options()") && (AKA_mark("lis===295###sois===9733###eois===9752###lif===11###soif===189###eoif===208###isc===true###function===./app-framework-binder/src/afb-args.c/init_options()")&&is_short_option(id))) {
				AKA_mark("lis===296###sois===9758###eois===9789###lif===12###soif===214###eoif===245###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");ns += 1 + !!optdefs[i].has_arg;
			}
			else {AKA_mark("lis===-295-###sois===-9733-###eois===-973319-###lif===-11-###soif===-###eoif===-208-###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");}

		}

				AKA_mark("lis===298###sois===9796###eois===9831###lif===14###soif===252###eoif===287###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");shortopts = oomchk(malloc(2 + ns));

				AKA_mark("lis===299###sois===9834###eois===9882###lif===15###soif===290###eoif===338###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");id2idx = oomchk(calloc(1 + mi, sizeof *id2idx));

				AKA_mark("lis===300###sois===9885###eois===9909###lif===16###soif===341###eoif===365###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");shortopts[ns = 0] = ':';

				AKA_mark("lis===301###sois===9917###eois===9924###lif===17###soif===373###eoif===380###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");for (i = 0 ;AKA_mark("lis===301###sois===9925###eois===9940###lif===17###soif===381###eoif===396###ifc===true###function===./app-framework-binder/src/afb-args.c/init_options()") && AKA_mark("lis===301###sois===9925###eois===9940###lif===17###soif===381###eoif===396###isc===true###function===./app-framework-binder/src/afb-args.c/init_options()")&&optdefs[i].name;({AKA_mark("lis===301###sois===9943###eois===9946###lif===17###soif===399###eoif===402###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");i++;})) {
						AKA_mark("lis===302###sois===9953###eois===9972###lif===18###soif===409###eoif===428###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");id = optdefs[i].id;

						AKA_mark("lis===303###sois===9976###eois===9991###lif===19###soif===432###eoif===447###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");id2idx[id] = i;

						if (AKA_mark("lis===304###sois===9999###eois===10018###lif===20###soif===455###eoif===474###ifc===true###function===./app-framework-binder/src/afb-args.c/init_options()") && (AKA_mark("lis===304###sois===9999###eois===10018###lif===20###soif===455###eoif===474###isc===true###function===./app-framework-binder/src/afb-args.c/init_options()")&&is_short_option(id))) {
								AKA_mark("lis===305###sois===10026###eois===10053###lif===21###soif===482###eoif===509###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");shortopts[++ns] = (char)id;

								if (AKA_mark("lis===306###sois===10062###eois===10080###lif===22###soif===518###eoif===536###ifc===true###function===./app-framework-binder/src/afb-args.c/init_options()") && (AKA_mark("lis===306###sois===10062###eois===10080###lif===22###soif===518###eoif===536###isc===true###function===./app-framework-binder/src/afb-args.c/init_options()")&&optdefs[i].has_arg)) {
					AKA_mark("lis===307###sois===10087###eois===10109###lif===23###soif===543###eoif===565###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");shortopts[++ns] = ':';
				}
				else {AKA_mark("lis===-306-###sois===-10062-###eois===-1006218-###lif===-22-###soif===-###eoif===-536-###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");}

			}
			else {AKA_mark("lis===-304-###sois===-9999-###eois===-999919-###lif===-20-###soif===-###eoif===-474-###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");}

		}

				AKA_mark("lis===310###sois===10121###eois===10141###lif===26###soif===577###eoif===597###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");shortopts[++ns] = 0;

	}
	else {AKA_mark("lis===-288-###sois===-9599-###eois===-959910-###lif===-4-###soif===-###eoif===-65-###ins===true###function===./app-framework-binder/src/afb-args.c/init_options()");}

}

/** Instrumented function name_of_optid(int) */
static const char *name_of_optid(int optid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/name_of_optid(int)");AKA_fCall++;
		AKA_mark("lis===316###sois===10195###eois===10230###lif===2###soif===47###eoif===82###ins===true###function===./app-framework-binder/src/afb-args.c/name_of_optid(int)");return optdefs[id2idx[optid]].name;

}

/** Instrumented function get_enum_val(const char*,int,int(*func)(const char*)) */
static int get_enum_val(const char *name, int optid, int (*func)(const char*))
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/get_enum_val(const char*,int,int(*func)(const char*))");AKA_fCall++;
		AKA_mark("lis===321###sois===10316###eois===10322###lif===2###soif===82###eoif===88###ins===true###function===./app-framework-binder/src/afb-args.c/get_enum_val(const char*,int,int(*func)(const char*))");int i;


		AKA_mark("lis===323###sois===10325###eois===10340###lif===4###soif===91###eoif===106###ins===true###function===./app-framework-binder/src/afb-args.c/get_enum_val(const char*,int,int(*func)(const char*))");i = func(name);

		if (AKA_mark("lis===324###sois===10346###eois===10351###lif===5###soif===112###eoif===117###ifc===true###function===./app-framework-binder/src/afb-args.c/get_enum_val(const char*,int,int(*func)(const char*))") && (AKA_mark("lis===324###sois===10346###eois===10351###lif===5###soif===112###eoif===117###isc===true###function===./app-framework-binder/src/afb-args.c/get_enum_val(const char*,int,int(*func)(const char*))")&&i < 0)) {
				AKA_mark("lis===325###sois===10357###eois===10432###lif===6###soif===123###eoif===198###ins===true###function===./app-framework-binder/src/afb-args.c/get_enum_val(const char*,int,int(*func)(const char*))");ERROR("option [--%s] bad value (found %s)",
			name_of_optid(optid), name);

				AKA_mark("lis===327###sois===10435###eois===10443###lif===8###soif===201###eoif===209###ins===true###function===./app-framework-binder/src/afb-args.c/get_enum_val(const char*,int,int(*func)(const char*))");exit(1);

	}
	else {AKA_mark("lis===-324-###sois===-10346-###eois===-103465-###lif===-5-###soif===-###eoif===-117-###ins===true###function===./app-framework-binder/src/afb-args.c/get_enum_val(const char*,int,int(*func)(const char*))");}

		AKA_mark("lis===329###sois===10448###eois===10457###lif===10###soif===214###eoif===223###ins===true###function===./app-framework-binder/src/afb-args.c/get_enum_val(const char*,int,int(*func)(const char*))");return i;

}


/*----------------------------------------------------------
 | printversion
 |   print version and copyright
 +--------------------------------------------------------- */
/** Instrumented function printVersion(FILE*) */
static void printVersion(FILE * file)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/printVersion(FILE*)");AKA_fCall++;
	/* Cant instrument this following code */
fprintf(file,
		"\n"
		"  AGL Framework Binder [AFB %s] "

#if WITH_DBUS_TRANSPARENCY
		"+"
#else
		"-"
#endif
		"DBUS "

#if defined(WITH_MONITORING_OPTION)
		"+"
#else
		"-"
#endif
		"MONITOR "
#if WITH_SUPERVISION
		"+"
#else
		"-"
#endif
		"SUPERVISION "

#if WITH_AFB_HOOK
		"+"
#else
		"-"
#endif
		"HOOK "

#if WITH_AFB_TRACE
		"+"
#else
		"-"
#endif
		"TRACE "

		"["
#if WITH_DYNAMIC_BINDING
		"+"
#else
		"-"
#endif
		"BINDINGS "
#if WITH_LEGACY_BINDING_V1
		"+V1 "
#endif
#if WITH_LEGACY_BINDING_VDYN
		"+VDYN "
#endif
#if WITH_LEGACY_BINDING_V2
		"+V2 "
#endif
		"+V3]\n"
		"\n",
		AFB_VERSION
	);
		AKA_mark("lis===397###sois===11287###eois===11425###lif===60###soif===652###eoif===790###ins===true###function===./app-framework-binder/src/afb-args.c/printVersion(FILE*)");fprintf(file,
		"  Copyright (C) 2015-2020 \"IoT.bzh\"\n"
		"  AFB comes with ABSOLUTELY NO WARRANTY.\n"
		"  Licence Apache 2\n"
		"\n");

}

/*----------------------------------------------------------
 | printHelp
 |   print information from long option array
 +--------------------------------------------------------- */

/** Instrumented function printHelp(FILE*,const char*) */
static void printHelp(FILE * file, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");AKA_fCall++;
		AKA_mark("lis===411###sois===11669###eois===11677###lif===2###soif===56###eoif===64###ins===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");int ind;

		AKA_mark("lis===412###sois===11679###eois===11704###lif===3###soif===66###eoif===91###ins===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");char command[50], sht[4];


		AKA_mark("lis===414###sois===11707###eois===11799###lif===5###soif===94###eoif===186###ins===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");fprintf(file, "%s:\nallowed options\n", strrchr(name, '/') ? strrchr(name, '/') + 1 : name);

		AKA_mark("lis===415###sois===11801###eois===11812###lif===6###soif===188###eoif===199###ins===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");sht[3] = 0;

		AKA_mark("lis===416###sois===11819###eois===11827###lif===7###soif===206###eoif===214###ins===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");for (ind = 0;AKA_mark("lis===416###sois===11828###eois===11853###lif===7###soif===215###eoif===240###ifc===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)") && AKA_mark("lis===416###sois===11828###eois===11853###lif===7###soif===215###eoif===240###isc===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)")&&optdefs[ind].name != NULL;({AKA_mark("lis===416###sois===11855###eois===11860###lif===7###soif===242###eoif===247###ins===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");ind++;})) {
				if (AKA_mark("lis===417###sois===11870###eois===11902###lif===8###soif===257###eoif===289###ifc===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)") && (AKA_mark("lis===417###sois===11870###eois===11902###lif===8###soif===257###eoif===289###isc===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)")&&is_short_option(optdefs[ind].id))) {
						AKA_mark("lis===418###sois===11909###eois===11922###lif===9###soif===296###eoif===309###ins===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");sht[0] = '-';

						AKA_mark("lis===419###sois===11926###eois===11957###lif===10###soif===313###eoif===344###ins===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");sht[1] = (char)optdefs[ind].id;

						AKA_mark("lis===420###sois===11961###eois===11974###lif===11###soif===348###eoif===361###ins===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");sht[2] = ',';

		}
		else {
						AKA_mark("lis===422###sois===11989###eois===12020###lif===13###soif===376###eoif===407###ins===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");sht[0] = sht[1] = sht[2] = ' ';

		}

				AKA_mark("lis===424###sois===12027###eois===12062###lif===15###soif===414###eoif===449###ins===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");strcpy(command, optdefs[ind].name);

				if (AKA_mark("lis===425###sois===12069###eois===12089###lif===16###soif===456###eoif===476###ifc===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)") && (AKA_mark("lis===425###sois===12069###eois===12089###lif===16###soif===456###eoif===476###isc===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)")&&optdefs[ind].has_arg)) {
			AKA_mark("lis===426###sois===12094###eois===12119###lif===17###soif===481###eoif===506###ins===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");strcat(command, "=xxxx");
		}
		else {AKA_mark("lis===-425-###sois===-12069-###eois===-1206920-###lif===-16-###soif===-###eoif===-476-###ins===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");}

				AKA_mark("lis===427###sois===12122###eois===12189###lif===18###soif===509###eoif===576###ins===true###function===./app-framework-binder/src/afb-args.c/printHelp(FILE*,const char*)");fprintf(file, " %s --%-17s %s\n", sht, command, optdefs[ind].help);

	}

	/* Cant instrument this following code */
fprintf(file,
		"Example:\n  %s  --verbose --port="
		d2s(DEFAULT_HTTP_PORT)
		" --token='azerty'"
#if WITH_DYNAMIC_BINDING
		" --ldpaths=build/bindings:/usr/lib64/agl/bindings"
#endif
		"\n",
		name);
}

/** Instrumented function dump(struct json_object*,FILE*,const char*,const char*) */
static void dump(struct json_object *config, FILE *file, const char *prefix, const char *title)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===442###sois===12498###eois===12522###lif===2###soif===99###eoif===123###ins===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)");const char *head, *tail;


		if (AKA_mark("lis===444###sois===12529###eois===12534###lif===4###soif===130###eoif===135###ifc===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)") && (AKA_mark("lis===444###sois===12529###eois===12534###lif===4###soif===130###eoif===135###isc===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)")&&title)) {
		AKA_mark("lis===445###sois===12538###eois===12601###lif===5###soif===139###eoif===202###ins===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)");fprintf(file, "%s----BEGIN OF %s-----\n", prefix ?: "", title);
	}
	else {AKA_mark("lis===-444-###sois===-12529-###eois===-125295-###lif===-4-###soif===-###eoif===-135-###ins===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)");}


		AKA_mark("lis===447###sois===12604###eois===12733###lif===7###soif===205###eoif===334###ins===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)");head = json_object_to_json_string_ext(config, JSON_C_TO_STRING_PRETTY
		|JSON_C_TO_STRING_SPACED|JSON_C_TO_STRING_NOSLASHESCAPE);


		if (AKA_mark("lis===450###sois===12740###eois===12747###lif===10###soif===341###eoif===348###ifc===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)") && (AKA_mark("lis===450###sois===12740###eois===12747###lif===10###soif===341###eoif===348###isc===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)")&&!prefix)) {
		AKA_mark("lis===451###sois===12751###eois===12779###lif===11###soif===352###eoif===380###ins===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)");fprintf(file, "%s\n", head);
	}
	else {
				while (AKA_mark("lis===453###sois===12796###eois===12801###lif===13###soif===397###eoif===402###ifc===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)") && (AKA_mark("lis===453###sois===12796###eois===12801###lif===13###soif===397###eoif===402###isc===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)")&&*head)) {
						AKA_mark("lis===454###sois===12813###eois===12826###lif===14###soif===414###eoif===427###ins===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)");for (tail = head ;AKA_mark("lis===454###sois===12827###eois===12849###lif===14###soif===428###eoif===450###ifc===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)") && (AKA_mark("lis===454###sois===12827###eois===12832###lif===14###soif===428###eoif===433###isc===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)")&&*tail)	&&(AKA_mark("lis===454###sois===12836###eois===12849###lif===14###soif===437###eoif===450###isc===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)")&&*tail != '\n');({AKA_mark("lis===454###sois===12852###eois===12858###lif===14###soif===453###eoif===459###ins===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)");tail++;})) {
				;
			}

						AKA_mark("lis===455###sois===12864###eois===12925###lif===15###soif===465###eoif===526###ins===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)");fprintf(file, "%s %.*s\n", prefix, (int)(tail - head), head);

						AKA_mark("lis===456###sois===12929###eois===12951###lif===16###soif===530###eoif===552###ins===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)");head = tail + !!*tail;

		}

	}


		if (AKA_mark("lis===460###sois===12965###eois===12970###lif===20###soif===566###eoif===571###ifc===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)") && (AKA_mark("lis===460###sois===12965###eois===12970###lif===20###soif===566###eoif===571###isc===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)")&&title)) {
		AKA_mark("lis===461###sois===12974###eois===13035###lif===21###soif===575###eoif===636###ins===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)");fprintf(file, "%s----END OF %s-----\n", prefix ?: "", title);
	}
	else {AKA_mark("lis===-460-###sois===-12965-###eois===-129655-###lif===-20-###soif===-###eoif===-571-###ins===true###function===./app-framework-binder/src/afb-args.c/dump(struct json_object*,FILE*,const char*,const char*)");}

}

/**********************************
* json helpers
***********************************/

/** Instrumented function joomchk(struct json_object*) */
static struct json_object *joomchk(struct json_object *value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/joomchk(struct json_object*)");AKA_fCall++;
		AKA_mark("lis===470###sois===13193###eois===13214###lif===2###soif===65###eoif===86###ins===true###function===./app-framework-binder/src/afb-args.c/joomchk(struct json_object*)");return oomchk(value);

}

/** Instrumented function to_jstr(const char*) */
static struct json_object *to_jstr(const char *value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/to_jstr(const char*)");AKA_fCall++;
		AKA_mark("lis===475###sois===13275###eois===13321###lif===2###soif===57###eoif===103###ins===true###function===./app-framework-binder/src/afb-args.c/to_jstr(const char*)");return joomchk(json_object_new_string(value));

}

/** Instrumented function to_jint(int) */
static struct json_object *to_jint(int value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/to_jint(int)");AKA_fCall++;
		AKA_mark("lis===480###sois===13374###eois===13417###lif===2###soif===49###eoif===92###ins===true###function===./app-framework-binder/src/afb-args.c/to_jint(int)");return joomchk(json_object_new_int(value));

}

/** Instrumented function to_jbool(int) */
static struct json_object *to_jbool(int value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/to_jbool(int)");AKA_fCall++;
		AKA_mark("lis===485###sois===13471###eois===13518###lif===2###soif===50###eoif===97###ins===true###function===./app-framework-binder/src/afb-args.c/to_jbool(int)");return joomchk(json_object_new_boolean(value));

}

/**********************************
* arguments helpers
***********************************/

/** Instrumented function string_to_bool(const char*) */
static int string_to_bool(const char *value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/string_to_bool(const char*)");AKA_fCall++;
		AKA_mark("lis===494###sois===13664###eois===13716###lif===2###soif===48###eoif===100###ins===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)");static const char true_names[] = "1\0yes\0true\0on";

		AKA_mark("lis===495###sois===13718###eois===13772###lif===3###soif===102###eoif===156###ins===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)");static const char false_names[] = "0\0no\0false\0off";

		AKA_mark("lis===496###sois===13774###eois===13785###lif===4###soif===158###eoif===169###ins===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)");size_t pos;


		AKA_mark("lis===498###sois===13788###eois===13796###lif===6###soif===172###eoif===180###ins===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)");pos = 0;

		while (AKA_mark("lis===499###sois===13805###eois===13828###lif===7###soif===189###eoif===212###ifc===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)") && (AKA_mark("lis===499###sois===13805###eois===13828###lif===7###soif===189###eoif===212###isc===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)")&&pos < sizeof true_names)) {
		if (AKA_mark("lis===500###sois===13836###eois===13871###lif===8###soif===220###eoif===255###ifc===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)") && (AKA_mark("lis===500###sois===13836###eois===13871###lif===8###soif===220###eoif===255###isc===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)")&&strcasecmp(value, &true_names[pos]))) {
			AKA_mark("lis===501###sois===13876###eois===13912###lif===9###soif===260###eoif===296###ins===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)");pos += 1 + strlen(&true_names[pos]);
		}
		else {
			AKA_mark("lis===503###sois===13923###eois===13932###lif===11###soif===307###eoif===316###ins===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)");return 1;
		}
	}


		AKA_mark("lis===505###sois===13935###eois===13943###lif===13###soif===319###eoif===327###ins===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)");pos = 0;

		while (AKA_mark("lis===506###sois===13952###eois===13976###lif===14###soif===336###eoif===360###ifc===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)") && (AKA_mark("lis===506###sois===13952###eois===13976###lif===14###soif===336###eoif===360###isc===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)")&&pos < sizeof false_names)) {
		if (AKA_mark("lis===507###sois===13984###eois===14020###lif===15###soif===368###eoif===404###ifc===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)") && (AKA_mark("lis===507###sois===13984###eois===14020###lif===15###soif===368###eoif===404###isc===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)")&&strcasecmp(value, &false_names[pos]))) {
			AKA_mark("lis===508###sois===14025###eois===14062###lif===16###soif===409###eoif===446###ins===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)");pos += 1 + strlen(&false_names[pos]);
		}
		else {
			AKA_mark("lis===510###sois===14073###eois===14082###lif===18###soif===457###eoif===466###ins===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)");return 0;
		}
	}


		AKA_mark("lis===512###sois===14085###eois===14095###lif===20###soif===469###eoif===479###ins===true###function===./app-framework-binder/src/afb-args.c/string_to_bool(const char*)");return -1;

}

/** Instrumented function noarg(int) */
static void noarg(int optid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/noarg(int)");AKA_fCall++;
		if (AKA_mark("lis===517###sois===14135###eois===14141###lif===2###soif===36###eoif===42###ifc===true###function===./app-framework-binder/src/afb-args.c/noarg(int)") && (AKA_mark("lis===517###sois===14135###eois===14141###lif===2###soif===36###eoif===42###isc===true###function===./app-framework-binder/src/afb-args.c/noarg(int)")&&optarg)) {
				AKA_mark("lis===518###sois===14147###eois===14225###lif===3###soif===48###eoif===126###ins===true###function===./app-framework-binder/src/afb-args.c/noarg(int)");ERROR("option [--%s] need no value (found %s)", name_of_optid(optid), optarg);

				AKA_mark("lis===519###sois===14228###eois===14236###lif===4###soif===129###eoif===137###ins===true###function===./app-framework-binder/src/afb-args.c/noarg(int)");exit(1);

	}
	else {AKA_mark("lis===-517-###sois===-14135-###eois===-141356-###lif===-2-###soif===-###eoif===-42-###ins===true###function===./app-framework-binder/src/afb-args.c/noarg(int)");}

}

/** Instrumented function get_arg(int) */
static const char *get_arg(int optid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/get_arg(int)");AKA_fCall++;
		if (AKA_mark("lis===525###sois===14288###eois===14299###lif===2###soif===45###eoif===56###ifc===true###function===./app-framework-binder/src/afb-args.c/get_arg(int)") && (AKA_mark("lis===525###sois===14288###eois===14299###lif===2###soif===45###eoif===56###isc===true###function===./app-framework-binder/src/afb-args.c/get_arg(int)")&&optarg == 0)) {
				AKA_mark("lis===526###sois===14305###eois===14404###lif===3###soif===62###eoif===161###ins===true###function===./app-framework-binder/src/afb-args.c/get_arg(int)");ERROR("option [--%s] needs a value i.e. --%s=xxx",
				name_of_optid(optid), name_of_optid(optid));

				AKA_mark("lis===528###sois===14407###eois===14415###lif===5###soif===164###eoif===172###ins===true###function===./app-framework-binder/src/afb-args.c/get_arg(int)");exit(1);

	}
	else {AKA_mark("lis===-525-###sois===-14288-###eois===-1428811-###lif===-2-###soif===-###eoif===-56-###ins===true###function===./app-framework-binder/src/afb-args.c/get_arg(int)");}

		AKA_mark("lis===530###sois===14420###eois===14434###lif===7###soif===177###eoif===191###ins===true###function===./app-framework-binder/src/afb-args.c/get_arg(int)");return optarg;

}

/** Instrumented function get_arg_bool(int) */
static int get_arg_bool(int optid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/get_arg_bool(int)");AKA_fCall++;
		AKA_mark("lis===535###sois===14476###eois===14519###lif===2###soif===38###eoif===81###ins===true###function===./app-framework-binder/src/afb-args.c/get_arg_bool(int)");int value = string_to_bool(get_arg(optid));

		if (AKA_mark("lis===536###sois===14525###eois===14534###lif===3###soif===87###eoif===96###ifc===true###function===./app-framework-binder/src/afb-args.c/get_arg_bool(int)") && (AKA_mark("lis===536###sois===14525###eois===14534###lif===3###soif===87###eoif===96###isc===true###function===./app-framework-binder/src/afb-args.c/get_arg_bool(int)")&&value < 0)) {
				AKA_mark("lis===537###sois===14540###eois===14644###lif===4###soif===102###eoif===206###ins===true###function===./app-framework-binder/src/afb-args.c/get_arg_bool(int)");ERROR("option [--%s] needs a boolean value: yes/no, true/false, on/off, 1/0",
				name_of_optid(optid));

				AKA_mark("lis===539###sois===14647###eois===14655###lif===6###soif===209###eoif===217###ins===true###function===./app-framework-binder/src/afb-args.c/get_arg_bool(int)");exit(1);

	}
	else {AKA_mark("lis===-536-###sois===-14525-###eois===-145259-###lif===-3-###soif===-###eoif===-96-###ins===true###function===./app-framework-binder/src/afb-args.c/get_arg_bool(int)");}

		AKA_mark("lis===541###sois===14660###eois===14673###lif===8###soif===222###eoif===235###ins===true###function===./app-framework-binder/src/afb-args.c/get_arg_bool(int)");return value;

}

/** Instrumented function config_del(struct json_object*,int) */
static void config_del(struct json_object *config, int optid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_del(struct json_object*,int)");AKA_fCall++;
		AKA_mark("lis===546###sois===14742###eois===14802###lif===2###soif===65###eoif===125###ins===true###function===./app-framework-binder/src/afb-args.c/config_del(struct json_object*,int)");return json_object_object_del(config, name_of_optid(optid));

}

/** Instrumented function config_has(struct json_object*,int) */
static int config_has(struct json_object *config, int optid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_has(struct json_object*,int)");AKA_fCall++;
		AKA_mark("lis===551###sois===14870###eois===14939###lif===2###soif===64###eoif===133###ins===true###function===./app-framework-binder/src/afb-args.c/config_has(struct json_object*,int)");return json_object_object_get_ex(config, name_of_optid(optid), NULL);

}

/** Instrumented function config_has_bool(struct json_object*,int) */
static int config_has_bool(struct json_object *config, int optid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_has_bool(struct json_object*,int)");AKA_fCall++;
		AKA_mark("lis===556###sois===15012###eois===15034###lif===2###soif===69###eoif===91###ins===true###function===./app-framework-binder/src/afb-args.c/config_has_bool(struct json_object*,int)");struct json_object *x;

		AKA_mark("lis===557###sois===15036###eois===15135###lif===3###soif===93###eoif===192###ins===true###function===./app-framework-binder/src/afb-args.c/config_has_bool(struct json_object*,int)");return json_object_object_get_ex(config, name_of_optid(optid), &x)
		&& json_object_get_boolean(x);

}

/** Instrumented function config_has_str(struct json_object*,int,const char*) */
__attribute__((unused))
static int config_has_str(struct json_object *config, int optid, const char *val)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)");AKA_fCall++;
		AKA_mark("lis===564###sois===15248###eois===15257###lif===3###soif===109###eoif===118###ins===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)");int i, n;

		AKA_mark("lis===565###sois===15259###eois===15281###lif===4###soif===120###eoif===142###ins===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)");struct json_object *a;


		if (AKA_mark("lis===567###sois===15288###eois===15348###lif===6###soif===149###eoif===209###ifc===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)") && (AKA_mark("lis===567###sois===15288###eois===15348###lif===6###soif===149###eoif===209###isc===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)")&&!json_object_object_get_ex(config, name_of_optid(optid), &a))) {
		AKA_mark("lis===568###sois===15352###eois===15361###lif===7###soif===213###eoif===222###ins===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)");return 0;
	}
	else {AKA_mark("lis===-567-###sois===-15288-###eois===-1528860-###lif===-6-###soif===-###eoif===-209-###ins===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)");}


		if (AKA_mark("lis===570###sois===15368###eois===15408###lif===9###soif===229###eoif===269###ifc===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)") && (AKA_mark("lis===570###sois===15368###eois===15408###lif===9###soif===229###eoif===269###isc===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)")&&!json_object_is_type(a, json_type_array))) {
		AKA_mark("lis===571###sois===15412###eois===15459###lif===10###soif===273###eoif===320###ins===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)");return !strcmp(val, json_object_get_string(a));
	}
	else {AKA_mark("lis===-570-###sois===-15368-###eois===-1536840-###lif===-9-###soif===-###eoif===-269-###ins===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)");}


		AKA_mark("lis===573###sois===15462###eois===15499###lif===12###soif===323###eoif===360###ins===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)");n = (int)json_object_array_length(a);

		AKA_mark("lis===574###sois===15506###eois===15513###lif===13###soif===367###eoif===374###ins===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)");for (i = 0 ;AKA_mark("lis===574###sois===15514###eois===15519###lif===13###soif===375###eoif===380###ifc===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)") && AKA_mark("lis===574###sois===15514###eois===15519###lif===13###soif===375###eoif===380###isc===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)")&&i < n;({AKA_mark("lis===574###sois===15522###eois===15525###lif===13###soif===383###eoif===386###ins===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)");i++;})) {
				if (AKA_mark("lis===575###sois===15535###eois===15604###lif===14###soif===396###eoif===465###ifc===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)") && (AKA_mark("lis===575###sois===15535###eois===15604###lif===14###soif===396###eoif===465###isc===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)")&&!strcmp(val, json_object_get_string(json_object_array_get_idx(a, i))))) {
			AKA_mark("lis===576###sois===15609###eois===15618###lif===15###soif===470###eoif===479###ins===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)");return 1;
		}
		else {AKA_mark("lis===-575-###sois===-15535-###eois===-1553569-###lif===-14-###soif===-###eoif===-465-###ins===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)");}

	}

		AKA_mark("lis===578###sois===15623###eois===15632###lif===17###soif===484###eoif===493###ins===true###function===./app-framework-binder/src/afb-args.c/config_has_str(struct json_object*,int,const char*)");return 0;

}

/** Instrumented function config_set(struct json_object*,int,struct json_object*) */
static void config_set(struct json_object *config, int optid, struct json_object *val)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_set(struct json_object*,int,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===583###sois===15726###eois===15784###lif===2###soif===90###eoif===148###ins===true###function===./app-framework-binder/src/afb-args.c/config_set(struct json_object*,int,struct json_object*)");json_object_object_add(config, name_of_optid(optid), val);

}

/** Instrumented function config_set_str(struct json_object*,int,const char*) */
static void config_set_str(struct json_object *config, int optid, const char *val)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_set_str(struct json_object*,int,const char*)");AKA_fCall++;
		AKA_mark("lis===588###sois===15874###eois===15914###lif===2###soif===86###eoif===126###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_str(struct json_object*,int,const char*)");config_set(config, optid, to_jstr(val));

}

/** Instrumented function config_set_optstr(struct json_object*,int) */
static void config_set_optstr(struct json_object *config, int optid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_set_optstr(struct json_object*,int)");AKA_fCall++;
		AKA_mark("lis===593###sois===15990###eois===16036###lif===2###soif===72###eoif===118###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optstr(struct json_object*,int)");config_set_str(config, optid, get_arg(optid));

}

/** Instrumented function config_set_int(struct json_object*,int,int) */
static void config_set_int(struct json_object *config, int optid, int value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_set_int(struct json_object*,int,int)");AKA_fCall++;
		AKA_mark("lis===598###sois===16120###eois===16162###lif===2###soif===80###eoif===122###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_int(struct json_object*,int,int)");config_set(config, optid, to_jint(value));

}

/** Instrumented function config_set_bool(struct json_object*,int,int) */
static void config_set_bool(struct json_object *config, int optid, int value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_set_bool(struct json_object*,int,int)");AKA_fCall++;
		AKA_mark("lis===603###sois===16247###eois===16290###lif===2###soif===81###eoif===124###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_bool(struct json_object*,int,int)");config_set(config, optid, to_jbool(value));

}

/** Instrumented function config_set_optint_base(struct json_object*,int,int,int,int) */
static void config_set_optint_base(struct json_object *config, int optid, int mini, int maxi, int base)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)");AKA_fCall++;
		AKA_mark("lis===608###sois===16401###eois===16423###lif===2###soif===107###eoif===129###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)");const char *beg, *end;

		AKA_mark("lis===609###sois===16425###eois===16438###lif===3###soif===131###eoif===144###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)");long int val;


		AKA_mark("lis===611###sois===16441###eois===16462###lif===5###soif===147###eoif===168###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)");beg = get_arg(optid);

		AKA_mark("lis===612###sois===16464###eois===16502###lif===6###soif===170###eoif===208###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)");val = strtol(beg, (char**)&end, base);

		if (AKA_mark("lis===613###sois===16508###eois===16526###lif===7###soif===214###eoif===232###ifc===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)") && ((AKA_mark("lis===613###sois===16508###eois===16512###lif===7###soif===214###eoif===218###isc===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)")&&*end)	||(AKA_mark("lis===613###sois===16516###eois===16526###lif===7###soif===222###eoif===232###isc===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)")&&end == beg))) {
				AKA_mark("lis===614###sois===16532###eois===16621###lif===8###soif===238###eoif===327###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)");ERROR("option [--%s] requires a valid integer (found %s)",
			name_of_optid(optid), beg);

				AKA_mark("lis===616###sois===16624###eois===16632###lif===10###soif===330###eoif===338###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)");exit(1);

	}
	else {AKA_mark("lis===-613-###sois===-16508-###eois===-1650818-###lif===-7-###soif===-###eoif===-232-###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)");}

		if (AKA_mark("lis===618###sois===16641###eois===16685###lif===12###soif===347###eoif===391###ifc===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)") && ((AKA_mark("lis===618###sois===16641###eois===16661###lif===12###soif===347###eoif===367###isc===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)")&&val < (long int)mini)	||(AKA_mark("lis===618###sois===16665###eois===16685###lif===12###soif===371###eoif===391###isc===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)")&&val > (long int)maxi))) {
				AKA_mark("lis===619###sois===16691###eois===16799###lif===13###soif===397###eoif===505###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)");ERROR("option [--%s] value %ld out of bounds (not in [%d , %d])",
			name_of_optid(optid), val, mini, maxi);

				AKA_mark("lis===621###sois===16802###eois===16810###lif===15###soif===508###eoif===516###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)");exit(1);

	}
	else {AKA_mark("lis===-618-###sois===-16641-###eois===-1664144-###lif===-12-###soif===-###eoif===-391-###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)");}

		AKA_mark("lis===623###sois===16815###eois===16855###lif===17###soif===521###eoif===561###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optint_base(struct json_object*,int,int,int,int)");config_set_int(config, optid, (int)val);

}

/** Instrumented function config_set_optint(struct json_object*,int,int,int) */
static void config_set_optint(struct json_object *config, int optid, int mini, int maxi)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_set_optint(struct json_object*,int,int,int)");AKA_fCall++;
		AKA_mark("lis===628###sois===16951###eois===17012###lif===2###soif===92###eoif===153###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optint(struct json_object*,int,int,int)");return config_set_optint_base(config, optid, mini, maxi, 10);

}

/** Instrumented function config_set_optenum(struct json_object*,int,int(*func)(const char*)) */
__attribute__((unused))
static void config_set_optenum(struct json_object *config, int optid, int (*func)(const char*))
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_set_optenum(struct json_object*,int,int(*func)(const char*))");AKA_fCall++;
		AKA_mark("lis===634###sois===17139###eois===17173###lif===3###soif===123###eoif===157###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optenum(struct json_object*,int,int(*func)(const char*))");const char *name = get_arg(optid);

		AKA_mark("lis===635###sois===17175###eois===17207###lif===4###soif===159###eoif===191###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optenum(struct json_object*,int,int(*func)(const char*))");get_enum_val(name, optid, func);

		AKA_mark("lis===636###sois===17209###eois===17245###lif===5###soif===193###eoif===229###ins===true###function===./app-framework-binder/src/afb-args.c/config_set_optenum(struct json_object*,int,int(*func)(const char*))");config_set_str(config, optid, name);

}

/** Instrumented function config_add(struct json_object*,int,struct json_object*) */
static void config_add(struct json_object *config, int optid, struct json_object *val)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_add(struct json_object*,int,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===641###sois===17339###eois===17361###lif===2###soif===90###eoif===112###ins===true###function===./app-framework-binder/src/afb-args.c/config_add(struct json_object*,int,struct json_object*)");struct json_object *a;

		if (AKA_mark("lis===642###sois===17367###eois===17427###lif===3###soif===118###eoif===178###ifc===true###function===./app-framework-binder/src/afb-args.c/config_add(struct json_object*,int,struct json_object*)") && (AKA_mark("lis===642###sois===17367###eois===17427###lif===3###soif===118###eoif===178###isc===true###function===./app-framework-binder/src/afb-args.c/config_add(struct json_object*,int,struct json_object*)")&&!json_object_object_get_ex(config, name_of_optid(optid), &a))) {
				AKA_mark("lis===643###sois===17433###eois===17470###lif===4###soif===184###eoif===221###ins===true###function===./app-framework-binder/src/afb-args.c/config_add(struct json_object*,int,struct json_object*)");a = joomchk(json_object_new_array());

				AKA_mark("lis===644###sois===17473###eois===17529###lif===5###soif===224###eoif===280###ins===true###function===./app-framework-binder/src/afb-args.c/config_add(struct json_object*,int,struct json_object*)");json_object_object_add(config, name_of_optid(optid), a);

	}
	else {AKA_mark("lis===-642-###sois===-17367-###eois===-1736760-###lif===-3-###soif===-###eoif===-178-###ins===true###function===./app-framework-binder/src/afb-args.c/config_add(struct json_object*,int,struct json_object*)");}

		AKA_mark("lis===646###sois===17534###eois===17564###lif===7###soif===285###eoif===315###ins===true###function===./app-framework-binder/src/afb-args.c/config_add(struct json_object*,int,struct json_object*)");json_object_array_add(a, val);

}

/** Instrumented function config_add_str(struct json_object*,int,const char*) */
static void config_add_str(struct json_object *config, int optid, const char *val)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_add_str(struct json_object*,int,const char*)");AKA_fCall++;
		AKA_mark("lis===651###sois===17654###eois===17694###lif===2###soif===86###eoif===126###ins===true###function===./app-framework-binder/src/afb-args.c/config_add_str(struct json_object*,int,const char*)");config_add(config, optid, to_jstr(val));

}

/** Instrumented function config_add_optstr(struct json_object*,int) */
static void config_add_optstr(struct json_object *config, int optid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_add_optstr(struct json_object*,int)");AKA_fCall++;
		AKA_mark("lis===656###sois===17770###eois===17816###lif===2###soif===72###eoif===118###ins===true###function===./app-framework-binder/src/afb-args.c/config_add_optstr(struct json_object*,int)");config_add_str(config, optid, get_arg(optid));

}

/** Instrumented function config_mix2_cb(void*,struct json_object*,const char*) */
static void config_mix2_cb(void *closure, struct json_object *obj, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)");AKA_fCall++;
		AKA_mark("lis===661###sois===17908###eois===17950###lif===2###soif===88###eoif===130###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)");struct json_object *dest, *base = closure;


		if (AKA_mark("lis===663###sois===17957###eois===17962###lif===4###soif===137###eoif===142###ifc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)") && (AKA_mark("lis===663###sois===17957###eois===17962###lif===4###soif===137###eoif===142###isc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)")&&!name)) {
		AKA_mark("lis===664###sois===17966###eois===17976###lif===5###soif===146###eoif===156###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)");name = "";
	}
	else {AKA_mark("lis===-663-###sois===-17957-###eois===-179575-###lif===-4-###soif===-###eoif===-142-###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)");}


		if (AKA_mark("lis===666###sois===17983###eois===18028###lif===7###soif===163###eoif===208###ifc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)") && (AKA_mark("lis===666###sois===17983###eois===18028###lif===7###soif===163###eoif===208###isc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)")&&!json_object_object_get_ex(base, name, &dest))) {
				AKA_mark("lis===667###sois===18034###eois===18075###lif===8###soif===214###eoif===255###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)");dest = joomchk(json_object_new_object());

				AKA_mark("lis===668###sois===18078###eois===18119###lif===9###soif===258###eoif===299###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)");json_object_object_add(base, name, dest);

	}
	else {AKA_mark("lis===-666-###sois===-17983-###eois===-1798345-###lif===-7-###soif===-###eoif===-208-###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)");}

		if (AKA_mark("lis===670###sois===18128###eois===18170###lif===11###soif===308###eoif===350###ifc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)") && (AKA_mark("lis===670###sois===18128###eois===18170###lif===11###soif===308###eoif===350###isc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)")&&json_object_is_type(obj, json_type_object))) {
		AKA_mark("lis===671###sois===18174###eois===18206###lif===12###soif===354###eoif===386###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)");wrap_json_object_add(dest, obj);
	}
	else {
		AKA_mark("lis===673###sois===18215###eois===18270###lif===14###soif===395###eoif===450###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_cb(void*,struct json_object*,const char*)");json_object_object_add(dest, "", json_object_get(obj));
	}

}

/** Instrumented function config_mix2(struct json_object*,int,struct json_object*) */
static void config_mix2(struct json_object *config, int optid, struct json_object *val)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_mix2(struct json_object*,int,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===678###sois===18365###eois===18389###lif===2###soif===91###eoif===115###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2(struct json_object*,int,struct json_object*)");struct json_object *obj;


		if (AKA_mark("lis===680###sois===18396###eois===18458###lif===4###soif===122###eoif===184###ifc===true###function===./app-framework-binder/src/afb-args.c/config_mix2(struct json_object*,int,struct json_object*)") && (AKA_mark("lis===680###sois===18396###eois===18458###lif===4###soif===122###eoif===184###isc===true###function===./app-framework-binder/src/afb-args.c/config_mix2(struct json_object*,int,struct json_object*)")&&!json_object_object_get_ex(config, name_of_optid(optid), &obj))) {
				AKA_mark("lis===681###sois===18464###eois===18504###lif===5###soif===190###eoif===230###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2(struct json_object*,int,struct json_object*)");obj = joomchk(json_object_new_object());

				AKA_mark("lis===682###sois===18507###eois===18565###lif===6###soif===233###eoif===291###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2(struct json_object*,int,struct json_object*)");json_object_object_add(config, name_of_optid(optid), obj);

	}
	else {AKA_mark("lis===-680-###sois===-18396-###eois===-1839662-###lif===-4-###soif===-###eoif===-184-###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2(struct json_object*,int,struct json_object*)");}

		AKA_mark("lis===684###sois===18570###eois===18614###lif===8###soif===296###eoif===340###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2(struct json_object*,int,struct json_object*)");wrap_json_for_all(val, config_mix2_cb, obj);

}

/** Instrumented function config_mix2_str(struct json_object*,int,const char*) */
static void config_mix2_str(struct json_object *config, int optid, const char *val)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");AKA_fCall++;
		AKA_mark("lis===689###sois===18705###eois===18721###lif===2###soif===87###eoif===103###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");size_t st1, st2;

		AKA_mark("lis===690###sois===18723###eois===18745###lif===3###soif===105###eoif===127###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");const char *api, *key;

		AKA_mark("lis===691###sois===18747###eois===18777###lif===4###soif===129###eoif===159###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");struct json_object *obj, *sub;

		AKA_mark("lis===692###sois===18779###eois===18808###lif===5###soif===161###eoif===190###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");enum json_tokener_error jerr;


		AKA_mark("lis===694###sois===18811###eois===18840###lif===7###soif===193###eoif===222###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");st1 = strcspn(val, "/:{[\"");

		AKA_mark("lis===695###sois===18842###eois===18876###lif===8###soif===224###eoif===258###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");st2 = strcspn(&val[st1], ":{[\"");

		if (AKA_mark("lis===696###sois===18882###eois===18922###lif===9###soif===264###eoif===304###ifc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)") && ((AKA_mark("lis===696###sois===18882###eois===18897###lif===9###soif===264###eoif===279###isc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)")&&val[st1] != '/')	||(AKA_mark("lis===696###sois===18901###eois===18922###lif===9###soif===283###eoif===304###isc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)")&&val[st1 + st2] != ':'))) {
				AKA_mark("lis===697###sois===18928###eois===18973###lif===10###soif===310###eoif===355###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");obj = json_tokener_parse_verbose(val, &jerr);

				if (AKA_mark("lis===698###sois===18980###eois===19008###lif===11###soif===362###eoif===390###ifc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)") && (AKA_mark("lis===698###sois===18980###eois===19008###lif===11###soif===362###eoif===390###isc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)")&&jerr != json_tokener_success)) {
			AKA_mark("lis===699###sois===19013###eois===19047###lif===12###soif===395###eoif===429###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");obj = json_object_new_string(val);
		}
		else {AKA_mark("lis===-698-###sois===-18980-###eois===-1898028-###lif===-11-###soif===-###eoif===-390-###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");}

	}
	else {
				AKA_mark("lis===701###sois===19060###eois===19102###lif===14###soif===442###eoif===484###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");api = st1 == 0 ? "*" : strndupa(val, st1);

				AKA_mark("lis===702###sois===19105###eois===19120###lif===15###soif===487###eoif===502###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");val += st1 + 1;

				AKA_mark("lis===703###sois===19123###eois===19199###lif===16###soif===505###eoif===581###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");key = st2 <= 1 || (st2 == 2 && *val == '*') ? NULL : strndupa(val, st2 - 1);

				AKA_mark("lis===704###sois===19202###eois===19213###lif===17###soif===584###eoif===595###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");val += st2;

				AKA_mark("lis===705###sois===19216###eois===19261###lif===18###soif===598###eoif===643###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");sub = json_tokener_parse_verbose(val, &jerr);

				if (AKA_mark("lis===706###sois===19268###eois===19296###lif===19###soif===650###eoif===678###ifc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)") && (AKA_mark("lis===706###sois===19268###eois===19296###lif===19###soif===650###eoif===678###isc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)")&&jerr != json_tokener_success)) {
			AKA_mark("lis===707###sois===19301###eois===19335###lif===20###soif===683###eoif===717###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");sub = json_object_new_string(val);
		}
		else {AKA_mark("lis===-706-###sois===-19268-###eois===-1926828-###lif===-19-###soif===-###eoif===-678-###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");}


				if (AKA_mark("lis===709###sois===19343###eois===19346###lif===22###soif===725###eoif===728###ifc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)") && (AKA_mark("lis===709###sois===19343###eois===19346###lif===22###soif===725###eoif===728###isc===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)")&&key)) {
						AKA_mark("lis===710###sois===19353###eois===19384###lif===23###soif===735###eoif===766###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");		AKA_mark("lis===714###sois===19447###eois===19478###lif===27###soif===829###eoif===860###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");obj = json_object_new_object();


						AKA_mark("lis===711###sois===19388###eois===19426###lif===24###soif===770###eoif===808###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");json_object_object_add(obj, key, sub);

						AKA_mark("lis===712###sois===19430###eois===19440###lif===25###soif===812###eoif===822###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");sub = obj;

		}
		else {AKA_mark("lis===-709-###sois===-19343-###eois===-193433-###lif===-22-###soif===-###eoif===-728-###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");}

		obj = json_object_new_object();
				AKA_mark("lis===715###sois===19481###eois===19519###lif===28###soif===863###eoif===901###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");json_object_object_add(obj, api, sub);

	}

		AKA_mark("lis===717###sois===19524###eois===19556###lif===30###soif===906###eoif===938###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");config_mix2(config, optid, obj);

		AKA_mark("lis===718###sois===19558###eois===19579###lif===31###soif===940###eoif===961###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_str(struct json_object*,int,const char*)");json_object_put(obj);

}

/** Instrumented function config_mix2_optstr(struct json_object*,int) */
static void config_mix2_optstr(struct json_object *config, int optid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/config_mix2_optstr(struct json_object*,int)");AKA_fCall++;
		AKA_mark("lis===723###sois===19656###eois===19703###lif===2###soif===73###eoif===120###ins===true###function===./app-framework-binder/src/afb-args.c/config_mix2_optstr(struct json_object*,int)");config_mix2_str(config, optid, get_arg(optid));

}

/*---------------------------------------------------------
 |   set the log levels
 +--------------------------------------------------------- */

/** Instrumented function set_log(const char*) */
static void set_log(const char *args)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/set_log(const char*)");AKA_fCall++;
		AKA_mark("lis===732###sois===19896###eois===19934###lif===2###soif===41###eoif===79###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");char o = 0, s, *p, *i = strdupa(args);

		AKA_mark("lis===733###sois===19936###eois===19944###lif===3###soif===81###eoif===89###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");int lvl;


		for (;;) AKA_mark("lis===735###sois===19963###eois===19965###lif===5###soif===108###eoif===110###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");switch(*i){
			case 0: if(*i == 0)AKA_mark("lis===736###sois===19970###eois===19977###lif===6###soif===115###eoif===122###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");

				AKA_mark("lis===737###sois===19980###eois===19987###lif===7###soif===125###eoif===132###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");return;

			case '+': if(*i == '+')AKA_mark("lis===738###sois===19989###eois===19998###lif===8###soif===134###eoif===143###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");

			case '-': if(*i == '-')AKA_mark("lis===739###sois===20000###eois===20009###lif===9###soif===145###eoif===154###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");

				AKA_mark("lis===740###sois===20012###eois===20019###lif===10###soif===157###eoif===164###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");o = *i;

		/*@fallthrough@*/
			case ' ': if(*i == ' ')AKA_mark("lis===742###sois===20041###eois===20050###lif===12###soif===186###eoif===195###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");

			case ',': if(*i == ',')AKA_mark("lis===743###sois===20052###eois===20061###lif===13###soif===197###eoif===206###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");

				AKA_mark("lis===744###sois===20064###eois===20068###lif===14###soif===209###eoif===213###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");i++;

				AKA_mark("lis===745###sois===20071###eois===20077###lif===15###soif===216###eoif===222###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");break;

			default: if(*i != 0 && *i != '+' && *i != '-' && *i != ' ' && *i != ',')AKA_mark("lis===746###sois===20079###eois===20087###lif===16###soif===224###eoif===232###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");

				AKA_mark("lis===747###sois===20090###eois===20096###lif===17###soif===235###eoif===241###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");p = i;

				while (AKA_mark("lis===748###sois===20106###eois===20117###lif===18###soif===251###eoif===262###ifc===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)") && (AKA_mark("lis===748###sois===20106###eois===20117###lif===18###soif===251###eoif===262###isc===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)")&&isalpha(*p))) {
			AKA_mark("lis===748###sois===20119###eois===20123###lif===18###soif===264###eoif===268###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");p++;
		}

				AKA_mark("lis===749###sois===20126###eois===20133###lif===19###soif===271###eoif===278###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");s = *p;

				AKA_mark("lis===750###sois===20136###eois===20143###lif===20###soif===281###eoif===288###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");*p = 0;

				AKA_mark("lis===751###sois===20146###eois===20177###lif===21###soif===291###eoif===322###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");lvl = verbose_level_of_name(i);

				if (AKA_mark("lis===752###sois===20184###eois===20191###lif===22###soif===329###eoif===336###ifc===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)") && (AKA_mark("lis===752###sois===20184###eois===20191###lif===22###soif===329###eoif===336###isc===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)")&&lvl < 0)) {
						AKA_mark("lis===753###sois===20198###eois===20213###lif===23###soif===343###eoif===358###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");i = strdupa(i);

						AKA_mark("lis===754###sois===20217###eois===20224###lif===24###soif===362###eoif===369###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");		AKA_mark("lis===758###sois===20289###eois===20296###lif===28###soif===434###eoif===441###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");*p = s;


						AKA_mark("lis===755###sois===20228###eois===20270###lif===25###soif===373###eoif===415###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");ERROR("Bad log name '%s' in %s", i, args);

						AKA_mark("lis===756###sois===20274###eois===20282###lif===26###soif===419###eoif===427###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");exit(1);

		}
		else {AKA_mark("lis===-752-###sois===-20184-###eois===-201847-###lif===-22-###soif===-###eoif===-336-###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");}

		*p = s;
				AKA_mark("lis===759###sois===20299###eois===20305###lif===29###soif===444###eoif===450###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");i = p;

				if (AKA_mark("lis===760###sois===20312###eois===20320###lif===30###soif===457###eoif===465###ifc===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)") && (AKA_mark("lis===760###sois===20312###eois===20320###lif===30###soif===457###eoif===465###isc===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)")&&o == '-')) {
			AKA_mark("lis===761###sois===20325###eois===20342###lif===31###soif===470###eoif===487###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");verbose_sub(lvl);
		}
		else {
						if (AKA_mark("lis===763###sois===20359###eois===20361###lif===33###soif===504###eoif===506###ifc===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)") && (AKA_mark("lis===763###sois===20359###eois===20361###lif===33###soif===504###eoif===506###isc===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)")&&!o)) {
								AKA_mark("lis===764###sois===20369###eois===20385###lif===34###soif===514###eoif===530###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");verbose_clear();

								AKA_mark("lis===765###sois===20390###eois===20398###lif===35###soif===535###eoif===543###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");o = '+';

			}
			else {AKA_mark("lis===-763-###sois===-20359-###eois===-203592-###lif===-33-###soif===-###eoif===-506-###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");}

						AKA_mark("lis===767###sois===20407###eois===20424###lif===37###soif===552###eoif===569###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");verbose_add(lvl);

		}

				AKA_mark("lis===769###sois===20431###eois===20437###lif===39###soif===576###eoif===582###ins===true###function===./app-framework-binder/src/afb-args.c/set_log(const char*)");break;

	}

}

/*---------------------------------------------------------
 |   Parse option and launch action
 +--------------------------------------------------------- */

/** Instrumented function parse_arguments_inner(int,char**,struct json_object*,struct option*) */
static void parse_arguments_inner(int argc, char **argv, struct json_object *config, struct option *options)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");AKA_fCall++;
		AKA_mark("lis===779###sois===20716###eois===20741###lif===2###soif===112###eoif===137###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");struct json_object *conf;

		AKA_mark("lis===780###sois===20743###eois===20771###lif===3###soif===139###eoif===167###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");int optid, cind, dodump = 0;


		for (;;) {
				AKA_mark("lis===783###sois===20787###eois===20801###lif===6###soif===183###eoif===197###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");cind = optind;

				AKA_mark("lis===784###sois===20804###eois===20862###lif===7###soif===200###eoif===258###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");optid = getopt_long(argc, argv, shortopts, options, NULL);

				if (AKA_mark("lis===785###sois===20869###eois===20878###lif===8###soif===265###eoif===274###ifc===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)") && (AKA_mark("lis===785###sois===20869###eois===20878###lif===8###soif===265###eoif===274###isc===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)")&&optid < 0)) {
			/* end of options */
						AKA_mark("lis===787###sois===20909###eois===20915###lif===10###soif===305###eoif===311###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;

		}
		else {AKA_mark("lis===-785-###sois===-20869-###eois===-208699-###lif===-8-###soif===-###eoif===-274-###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");}

				AKA_mark("lis===789###sois===20930###eois===20935###lif===12###soif===326###eoif===331###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");switch(optid){
					case SET_VERBOSE: if(optid == SET_VERBOSE)AKA_mark("lis===790###sois===20941###eois===20958###lif===13###soif===337###eoif===354###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===791###sois===20962###eois===20976###lif===14###soif===358###eoif===372###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");verbose_inc();

						AKA_mark("lis===792###sois===20980###eois===20986###lif===15###soif===376###eoif===382###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;


					case SET_COLOR: if(optid == SET_COLOR)AKA_mark("lis===794###sois===20990###eois===21005###lif===17###soif===386###eoif===401###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===795###sois===21009###eois===21028###lif===18###soif===405###eoif===424###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");verbose_colorize();

						AKA_mark("lis===796###sois===21032###eois===21038###lif===19###soif===428###eoif===434###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;


					case SET_QUIET: if(optid == SET_QUIET)AKA_mark("lis===798###sois===21042###eois===21057###lif===21###soif===438###eoif===453###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===799###sois===21061###eois===21075###lif===22###soif===457###eoif===471###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");verbose_dec();

						AKA_mark("lis===800###sois===21079###eois===21085###lif===23###soif===475###eoif===481###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;


					case SET_LOG: if(optid == SET_LOG)AKA_mark("lis===802###sois===21089###eois===21102###lif===25###soif===485###eoif===498###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===803###sois===21106###eois===21130###lif===26###soif===502###eoif===526###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");set_log(get_arg(optid));

						AKA_mark("lis===804###sois===21134###eois===21140###lif===27###soif===530###eoif===536###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;


					case SET_PORT: if(optid == SET_PORT)AKA_mark("lis===806###sois===21144###eois===21158###lif===29###soif===540###eoif===554###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===807###sois===21162###eois===21208###lif===30###soif===558###eoif===604###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");config_set_optint(config, optid, 1024, 32767);

						AKA_mark("lis===808###sois===21212###eois===21218###lif===31###soif===608###eoif===614###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;


					case SET_API_TIMEOUT: if(optid == SET_API_TIMEOUT)AKA_mark("lis===810###sois===21222###eois===21243###lif===33###soif===618###eoif===639###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case SET_SESSION_TIMEOUT: if(optid == SET_SESSION_TIMEOUT)AKA_mark("lis===811###sois===21246###eois===21271###lif===34###soif===642###eoif===667###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case SET_CACHE_TIMEOUT: if(optid == SET_CACHE_TIMEOUT)AKA_mark("lis===812###sois===21274###eois===21297###lif===35###soif===670###eoif===693###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===813###sois===21301###eois===21346###lif===36###soif===697###eoif===742###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");config_set_optint(config, optid, 0, INT_MAX);

						AKA_mark("lis===814###sois===21350###eois===21356###lif===37###soif===746###eoif===752###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;


					case SET_SESSIONMAX: if(optid == SET_SESSIONMAX)AKA_mark("lis===816###sois===21360###eois===21380###lif===39###soif===756###eoif===776###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===817###sois===21384###eois===21429###lif===40###soif===780###eoif===825###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");config_set_optint(config, optid, 1, INT_MAX);

						AKA_mark("lis===818###sois===21433###eois===21439###lif===41###soif===829###eoif===835###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;


					case SET_ROOT_DIR: if(optid == SET_ROOT_DIR)AKA_mark("lis===820###sois===21443###eois===21461###lif===43###soif===839###eoif===857###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case SET_ROOT_HTTP: if(optid == SET_ROOT_HTTP)AKA_mark("lis===821###sois===21464###eois===21483###lif===44###soif===860###eoif===879###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case SET_ROOT_BASE: if(optid == SET_ROOT_BASE)AKA_mark("lis===822###sois===21486###eois===21505###lif===45###soif===882###eoif===901###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case SET_ROOT_API: if(optid == SET_ROOT_API)AKA_mark("lis===823###sois===21508###eois===21526###lif===46###soif===904###eoif===922###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case SET_TOKEN: if(optid == SET_TOKEN)AKA_mark("lis===824###sois===21529###eois===21544###lif===47###soif===925###eoif===940###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case SET_UPLOAD_DIR: if(optid == SET_UPLOAD_DIR)AKA_mark("lis===825###sois===21547###eois===21567###lif===48###soif===943###eoif===963###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case SET_WORK_DIR: if(optid == SET_WORK_DIR)AKA_mark("lis===826###sois===21570###eois===21588###lif===49###soif===966###eoif===984###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case SET_NAME: if(optid == SET_NAME)AKA_mark("lis===827###sois===21591###eois===21605###lif===50###soif===987###eoif===1001###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===828###sois===21609###eois===21642###lif===51###soif===1005###eoif===1038###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");config_set_optstr(config, optid);

						AKA_mark("lis===829###sois===21646###eois===21652###lif===52###soif===1042###eoif===1048###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;


#if WITH_DBUS_TRANSPARENCY
		case ADD_DBUS_CLIENT:
		case ADD_DBUS_SERVICE:
#endif
					case ADD_ALIAS: if(optid == ADD_ALIAS)AKA_mark("lis===835###sois===21739###eois===21754###lif===58###soif===1135###eoif===1150###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

#if WITH_DYNAMIC_BINDING
		case ADD_LDPATH:
		case ADD_WEAK_LDPATH:
		case ADD_BINDING:
#endif
					case ADD_CALL: if(optid == ADD_CALL)AKA_mark("lis===841###sois===21852###eois===21866###lif===64###soif===1248###eoif===1262###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case ADD_WS_CLIENT: if(optid == ADD_WS_CLIENT)AKA_mark("lis===842###sois===21869###eois===21888###lif===65###soif===1265###eoif===1284###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case ADD_WS_SERVICE: if(optid == ADD_WS_SERVICE)AKA_mark("lis===843###sois===21891###eois===21911###lif===66###soif===1287###eoif===1307###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case ADD_AUTO_API: if(optid == ADD_AUTO_API)AKA_mark("lis===844###sois===21914###eois===21932###lif===67###soif===1310###eoif===1328###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case ADD_INTERFACE: if(optid == ADD_INTERFACE)AKA_mark("lis===845###sois===21935###eois===21954###lif===68###soif===1331###eoif===1350###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===846###sois===21958###eois===21991###lif===69###soif===1354###eoif===1387###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");config_add_optstr(config, optid);

						AKA_mark("lis===847###sois===21995###eois===22001###lif===70###soif===1391###eoif===1397###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;


					case ADD_SET: if(optid == ADD_SET)AKA_mark("lis===849###sois===22005###eois===22018###lif===72###soif===1401###eoif===1414###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===850###sois===22022###eois===22056###lif===73###soif===1418###eoif===1452###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");config_mix2_optstr(config, optid);

						AKA_mark("lis===851###sois===22060###eois===22066###lif===74###soif===1456###eoif===1462###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;


#if defined(WITH_MONITORING_OPTION)
		case SET_MONITORING:
#endif
					case SET_RANDOM_TOKEN: if(optid == SET_RANDOM_TOKEN)AKA_mark("lis===856###sois===22136###eois===22158###lif===79###soif===1532###eoif===1554###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case SET_NO_HTTPD: if(optid == SET_NO_HTTPD)AKA_mark("lis===857###sois===22161###eois===22179###lif===80###soif===1557###eoif===1575###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

#if WITH_DYNAMIC_BINDING
		case SET_NO_LDPATH:
#endif
						AKA_mark("lis===861###sois===22237###eois===22250###lif===84###soif===1633###eoif===1646###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");noarg(optid);

						AKA_mark("lis===862###sois===22254###eois===22288###lif===85###soif===1650###eoif===1684###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");config_set_bool(config, optid, 1);

						AKA_mark("lis===863###sois===22292###eois===22298###lif===86###soif===1688###eoif===1694###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;



					case SET_FOREGROUND: if(optid == SET_FOREGROUND)AKA_mark("lis===866###sois===22303###eois===22323###lif===89###soif===1699###eoif===1719###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case SET_BACKGROUND: if(optid == SET_BACKGROUND)AKA_mark("lis===867###sois===22326###eois===22346###lif===90###soif===1722###eoif===1742###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

					case SET_DAEMON: if(optid == SET_DAEMON)AKA_mark("lis===868###sois===22349###eois===22365###lif===91###soif===1745###eoif===1761###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===869###sois===22369###eois===22382###lif===92###soif===1765###eoif===1778###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");noarg(optid);

						AKA_mark("lis===870###sois===22386###eois===22447###lif===93###soif===1782###eoif===1843###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");config_set_bool(config, SET_DAEMON, optid != SET_FOREGROUND);

						AKA_mark("lis===871###sois===22451###eois===22457###lif===94###soif===1847###eoif===1853###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;


					case SET_TRAP_FAULTS: if(optid == SET_TRAP_FAULTS)AKA_mark("lis===873###sois===22461###eois===22482###lif===96###soif===1857###eoif===1878###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===874###sois===22486###eois===22538###lif===97###soif===1882###eoif===1934###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");config_set_bool(config, optid, get_arg_bool(optid));

						AKA_mark("lis===875###sois===22542###eois===22548###lif===98###soif===1938###eoif===1944###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;



#if WITH_AFB_HOOK
		case SET_TRACEREQ:
			config_set_optenum(config, optid, afb_hook_flags_xreq_from_text);
			break;

		case SET_TRACEEVT:
			config_set_optenum(config, optid, afb_hook_flags_evt_from_text);
			break;

		case SET_TRACESES:
			config_set_optenum(config, optid, afb_hook_flags_session_from_text);
			break;

		case SET_TRACEAPI:
			config_set_optenum(config, optid, afb_hook_flags_api_from_text);
			break;

		case SET_TRACEGLOB:
			config_set_optenum(config, optid, afb_hook_flags_global_from_text);
			break;

#if !defined(REMOVE_LEGACY_TRACE)
		case SET_TRACEDITF:
			config_set_optenum(config, optid, afb_hook_flags_legacy_ditf_from_text);
			break;

		case SET_TRACESVC:
			config_set_optenum(config, optid, afb_hook_flags_legacy_svc_from_text);
			break;
#endif
#endif

					case SET_EXEC: if(optid == SET_EXEC)AKA_mark("lis===910###sois===23344###eois===23358###lif===133###soif===2740###eoif===2754###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						if (AKA_mark("lis===911###sois===23366###eois===23380###lif===134###soif===2762###eoif===2776###ifc===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)") && (AKA_mark("lis===911###sois===23366###eois===23380###lif===134###soif===2762###eoif===2776###isc===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)")&&optind == argc)) {
								AKA_mark("lis===912###sois===23388###eois===23434###lif===135###soif===2784###eoif===2830###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");ERROR("The option --exec requires arguments");

								AKA_mark("lis===913###sois===23439###eois===23447###lif===136###soif===2835###eoif===2843###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");exit(1);

			}
			else {AKA_mark("lis===-911-###sois===-23366-###eois===-2336614-###lif===-134-###soif===-###eoif===-2776-###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");}

						while (AKA_mark("lis===915###sois===23463###eois===23477###lif===138###soif===2859###eoif===2873###ifc===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)") && (AKA_mark("lis===915###sois===23463###eois===23477###lif===138###soif===2859###eoif===2873###isc===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)")&&optind != argc)) {
				AKA_mark("lis===916###sois===23483###eois===23529###lif===139###soif===2879###eoif===2925###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");config_add_str(config, optid, argv[optind++]);
			}

						AKA_mark("lis===917###sois===23533###eois===23539###lif===140###soif===2929###eoif===2935###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;


					case SET_CONFIG: if(optid == SET_CONFIG)AKA_mark("lis===919###sois===23543###eois===23559###lif===142###soif===2939###eoif===2955###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===920###sois===23563###eois===23608###lif===143###soif===2959###eoif===3004###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");conf = json_object_from_file(get_arg(optid));

						if (AKA_mark("lis===921###sois===23616###eois===23621###lif===144###soif===3012###eoif===3017###ifc===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)") && (AKA_mark("lis===921###sois===23616###eois===23621###lif===144###soif===3012###eoif===3017###isc===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)")&&!conf)) {
								AKA_mark("lis===922###sois===23629###eois===23680###lif===145###soif===3025###eoif===3076###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");ERROR("Can't read config file %s", get_arg(optid));

								AKA_mark("lis===923###sois===23685###eois===23693###lif===146###soif===3081###eoif===3089###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");exit(1);

			}
			else {AKA_mark("lis===-921-###sois===-23616-###eois===-236165-###lif===-144-###soif===-###eoif===-3017-###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");}

						AKA_mark("lis===925###sois===23702###eois===23737###lif===148###soif===3098###eoif===3133###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");wrap_json_object_add(config, conf);

						AKA_mark("lis===926###sois===23741###eois===23763###lif===149###soif===3137###eoif===3159###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");json_object_put(conf);

						AKA_mark("lis===927###sois===23767###eois===23773###lif===150###soif===3163###eoif===3169###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;


					case DUMP_CONFIG: if(optid == DUMP_CONFIG)AKA_mark("lis===929###sois===23777###eois===23794###lif===152###soif===3173###eoif===3190###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===930###sois===23798###eois===23811###lif===153###soif===3194###eoif===3207###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");noarg(optid);

						AKA_mark("lis===931###sois===23815###eois===23826###lif===154###soif===3211###eoif===3222###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");dodump = 1;

						AKA_mark("lis===932###sois===23830###eois===23836###lif===155###soif===3226###eoif===3232###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");break;


					case GET_VERSION: if(optid == GET_VERSION)AKA_mark("lis===934###sois===23840###eois===23857###lif===157###soif===3236###eoif===3253###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===935###sois===23861###eois===23874###lif===158###soif===3257###eoif===3270###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");noarg(optid);

						AKA_mark("lis===936###sois===23878###eois===23899###lif===159###soif===3274###eoif===3295###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");printVersion(stdout);

						AKA_mark("lis===937###sois===23903###eois===23911###lif===160###soif===3299###eoif===3307###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");exit(0);


					case GET_HELP: if(optid == GET_HELP)AKA_mark("lis===939###sois===23915###eois===23929###lif===162###soif===3311###eoif===3325###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===940###sois===23933###eois===23960###lif===163###soif===3329###eoif===3356###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");printHelp(stdout, argv[0]);

						AKA_mark("lis===941###sois===23964###eois===23972###lif===164###soif===3360###eoif===3368###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");exit(0);


					default: if(optid != SET_VERBOSE && optid != SET_COLOR && optid != SET_QUIET && optid != SET_LOG && optid != SET_PORT && optid != SET_API_TIMEOUT && optid != SET_SESSION_TIMEOUT && optid != SET_CACHE_TIMEOUT && optid != SET_SESSIONMAX && optid != SET_ROOT_DIR && optid != SET_ROOT_HTTP && optid != SET_ROOT_BASE && optid != SET_ROOT_API && optid != SET_TOKEN && optid != SET_UPLOAD_DIR && optid != SET_WORK_DIR && optid != SET_NAME && optid != ADD_ALIAS && optid != ADD_CALL && optid != ADD_WS_CLIENT && optid != ADD_WS_SERVICE && optid != ADD_AUTO_API && optid != ADD_INTERFACE && optid != ADD_SET && optid != SET_RANDOM_TOKEN && optid != SET_NO_HTTPD && optid != SET_FOREGROUND && optid != SET_BACKGROUND && optid != SET_DAEMON && optid != SET_TRAP_FAULTS && optid != SET_EXEC && optid != SET_CONFIG && optid != DUMP_CONFIG && optid != GET_VERSION && optid != GET_HELP)AKA_mark("lis===943###sois===23976###eois===23984###lif===166###soif===3372###eoif===3380###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");

						AKA_mark("lis===944###sois===23988###eois===24039###lif===167###soif===3384###eoif===3435###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");ERROR("Bad option detected, check %s", argv[cind]);

						AKA_mark("lis===945###sois===24043###eois===24051###lif===168###soif===3439###eoif===3447###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");exit(1);

		}

	}

	/* TODO: check for extra value */

		if (AKA_mark("lis===950###sois===24100###eois===24106###lif===173###soif===3496###eoif===3502###ifc===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)") && (AKA_mark("lis===950###sois===24100###eois===24106###lif===173###soif===3496###eoif===3502###isc===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)")&&dodump)) {
				AKA_mark("lis===951###sois===24112###eois===24145###lif===174###soif===3508###eoif===3541###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");dump(config, stdout, NULL, NULL);

				AKA_mark("lis===952###sois===24148###eois===24156###lif===175###soif===3544###eoif===3552###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");exit(0);

	}
	else {AKA_mark("lis===-950-###sois===-24100-###eois===-241006-###lif===-173-###soif===-###eoif===-3502-###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments_inner(int,char**,struct json_object*,struct option*)");}

}

/** Instrumented function parse_arguments(int,char**,struct json_object*) */
static void parse_arguments(int argc, char **argv, struct json_object *config)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===958###sois===24245###eois===24253###lif===2###soif===82###eoif===90###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)");int ind;

		AKA_mark("lis===959###sois===24255###eois===24278###lif===3###soif===92###eoif===115###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)");struct option *options;


	/* create GNU getopt options from optdefs */
		AKA_mark("lis===962###sois===24327###eois===24400###lif===6###soif===164###eoif===237###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)");options = malloc((sizeof optdefs / sizeof * optdefs) * sizeof * options);

		AKA_mark("lis===963###sois===24407###eois===24415###lif===7###soif===244###eoif===252###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)");for (ind = 0;AKA_mark("lis===963###sois===24416###eois===24433###lif===7###soif===253###eoif===270###ifc===true###function===./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)") && AKA_mark("lis===963###sois===24416###eois===24433###lif===7###soif===253###eoif===270###isc===true###function===./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)")&&optdefs[ind].name;({AKA_mark("lis===963###sois===24435###eois===24440###lif===7###soif===272###eoif===277###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)");ind++;})) {
				AKA_mark("lis===964###sois===24446###eois===24484###lif===8###soif===283###eoif===321###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)");options[ind].name = optdefs[ind].name;

				AKA_mark("lis===965###sois===24487###eois===24531###lif===9###soif===324###eoif===368###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)");options[ind].has_arg = optdefs[ind].has_arg;

				AKA_mark("lis===966###sois===24534###eois===24559###lif===10###soif===371###eoif===396###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)");options[ind].flag = NULL;

				AKA_mark("lis===967###sois===24562###eois===24597###lif===11###soif===399###eoif===434###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)");options[ind].val = optdefs[ind].id;

	}

		AKA_mark("lis===969###sois===24602###eois===24648###lif===13###soif===439###eoif===485###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)");memset(&options[ind], 0, sizeof options[ind]);


	/* parse the arguments */
		AKA_mark("lis===972###sois===24678###eois===24729###lif===16###soif===515###eoif===566###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)");parse_arguments_inner(argc, argv, config, options);


	/* release the memory of options */
		AKA_mark("lis===975###sois===24769###eois===24783###lif===19###soif===606###eoif===620###ins===true###function===./app-framework-binder/src/afb-args.c/parse_arguments(int,char**,struct json_object*)");free(options);

}

/** Instrumented function fulfill_config(struct json_object*) */
static void fulfill_config(struct json_object *config)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)");AKA_fCall++;
		AKA_mark("lis===980###sois===24845###eois===24851###lif===2###soif===58###eoif===64###ins===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)");int i;


		AKA_mark("lis===982###sois===24859###eois===24866###lif===4###soif===72###eoif===79###ins===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)");for (i = 0 ;AKA_mark("lis===982###sois===24867###eois===24932###lif===4###soif===80###eoif===145###ifc===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)") && AKA_mark("lis===982###sois===24867###eois===24932###lif===4###soif===80###eoif===145###isc===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)")&&i < sizeof default_optint_values / sizeof * default_optint_values;({AKA_mark("lis===982###sois===24935###eois===24938###lif===4###soif===148###eoif===151###ins===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)");i++;})) {
		if (AKA_mark("lis===983###sois===24946###eois===24997###lif===5###soif===159###eoif===210###ifc===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)") && (AKA_mark("lis===983###sois===24946###eois===24997###lif===5###soif===159###eoif===210###isc===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)")&&!config_has(config, default_optint_values[i].optid))) {
			AKA_mark("lis===984###sois===25002###eois===25090###lif===6###soif===215###eoif===303###ins===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)");config_set_int(config, default_optint_values[i].optid, default_optint_values[i].valdef);
		}
		else {AKA_mark("lis===-983-###sois===-24946-###eois===-2494651-###lif===-5-###soif===-###eoif===-210-###ins===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)");}
	}


		AKA_mark("lis===986###sois===25098###eois===25105###lif===8###soif===311###eoif===318###ins===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)");for (i = 0 ;AKA_mark("lis===986###sois===25106###eois===25171###lif===8###soif===319###eoif===384###ifc===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)") && AKA_mark("lis===986###sois===25106###eois===25171###lif===8###soif===319###eoif===384###isc===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)")&&i < sizeof default_optstr_values / sizeof * default_optstr_values;({AKA_mark("lis===986###sois===25174###eois===25177###lif===8###soif===387###eoif===390###ins===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)");i++;})) {
		if (AKA_mark("lis===987###sois===25185###eois===25236###lif===9###soif===398###eoif===449###ifc===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)") && (AKA_mark("lis===987###sois===25185###eois===25236###lif===9###soif===398###eoif===449###isc===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)")&&!config_has(config, default_optstr_values[i].optid))) {
			AKA_mark("lis===988###sois===25241###eois===25329###lif===10###soif===454###eoif===542###ins===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)");config_set_str(config, default_optstr_values[i].optid, default_optstr_values[i].valdef);
		}
		else {AKA_mark("lis===-987-###sois===-25185-###eois===-2518551-###lif===-9-###soif===-###eoif===-449-###ins===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)");}
	}


		if (AKA_mark("lis===990###sois===25336###eois===25445###lif===12###soif===549###eoif===658###ifc===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)") && (((AKA_mark("lis===990###sois===25336###eois===25365###lif===12###soif===549###eoif===578###isc===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)")&&!config_has(config, SET_PORT))	&&(AKA_mark("lis===990###sois===25369###eois===25403###lif===12###soif===582###eoif===616###isc===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)")&&!config_has(config, ADD_INTERFACE)))	&&(AKA_mark("lis===990###sois===25407###eois===25445###lif===12###soif===620###eoif===658###isc===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)")&&!config_has_bool(config, SET_NO_HTTPD)))) {
		AKA_mark("lis===991###sois===25449###eois===25501###lif===13###soif===662###eoif===714###ins===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)");config_set_int(config, SET_PORT, DEFAULT_HTTP_PORT);
	}
	else {AKA_mark("lis===-990-###sois===-25336-###eois===-25336109-###lif===-12-###soif===-###eoif===-658-###ins===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)");}


	// default AUTH_TOKEN
		if (AKA_mark("lis===994###sois===25531###eois===25572###lif===16###soif===744###eoif===785###ifc===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)") && (AKA_mark("lis===994###sois===25531###eois===25572###lif===16###soif===744###eoif===785###isc===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)")&&config_has_bool(config, SET_RANDOM_TOKEN))) {
		AKA_mark("lis===995###sois===25576###eois===25606###lif===17###soif===789###eoif===819###ins===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)");config_del(config, SET_TOKEN);
	}
	else {AKA_mark("lis===-994-###sois===-25531-###eois===-2553141-###lif===-16-###soif===-###eoif===-785-###ins===true###function===./app-framework-binder/src/afb-args.c/fulfill_config(struct json_object*)");}


#if WITH_DYNAMIC_BINDING && defined(INTRINSIC_BINDING_DIR)
	if (!config_has(config, ADD_LDPATH) && !config_has(config, ADD_WEAK_LDPATH) && !config_has_bool(config, SET_NO_LDPATH))
		config_add_str(config, ADD_LDPATH, INTRINSIC_BINDING_DIR);
#endif

#if defined(WITH_MONITORING_OPTION)
	if (config_has_bool(config, SET_MONITORING) && !config_has_str(config, ADD_ALIAS, MONITORING_ALIAS))
		config_add_str(config, ADD_ALIAS, MONITORING_ALIAS);
#endif

#if !defined(REMOVE_LEGACY_TRACE) && 0
	config->traceapi |= config->traceditf | config->tracesvc;
#endif
}

/** Instrumented function on_environment(struct json_object*,int,const char*,void(*func)(struct json_object*, int, const char*)) */
static void on_environment(struct json_object *config, int optid, const char *name, void (*func)(struct json_object*, int, const char*))
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/on_environment(struct json_object*,int,const char*,void(*func)(struct json_object*, int, const char*))");AKA_fCall++;
		AKA_mark("lis===1014###sois===26306###eois===26340###lif===2###soif===140###eoif===174###ins===true###function===./app-framework-binder/src/afb-args.c/on_environment(struct json_object*,int,const char*,void(*func)(struct json_object*, int, const char*))");char *value = secure_getenv(name);


		if (AKA_mark("lis===1016###sois===26347###eois===26362###lif===4###soif===181###eoif===196###ifc===true###function===./app-framework-binder/src/afb-args.c/on_environment(struct json_object*,int,const char*,void(*func)(struct json_object*, int, const char*))") && ((AKA_mark("lis===1016###sois===26347###eois===26352###lif===4###soif===181###eoif===186###isc===true###function===./app-framework-binder/src/afb-args.c/on_environment(struct json_object*,int,const char*,void(*func)(struct json_object*, int, const char*))")&&value)	&&(AKA_mark("lis===1016###sois===26356###eois===26362###lif===4###soif===190###eoif===196###isc===true###function===./app-framework-binder/src/afb-args.c/on_environment(struct json_object*,int,const char*,void(*func)(struct json_object*, int, const char*))")&&*value))) {
		AKA_mark("lis===1017###sois===26366###eois===26393###lif===5###soif===200###eoif===227###ins===true###function===./app-framework-binder/src/afb-args.c/on_environment(struct json_object*,int,const char*,void(*func)(struct json_object*, int, const char*))");func(config, optid, value);
	}
	else {AKA_mark("lis===-1016-###sois===-26347-###eois===-2634715-###lif===-4-###soif===-###eoif===-196-###ins===true###function===./app-framework-binder/src/afb-args.c/on_environment(struct json_object*,int,const char*,void(*func)(struct json_object*, int, const char*))");}

}

/** Instrumented function on_environment_enum(struct json_object*,int,const char*,int(*func)(const char*)) */
__attribute__((unused))
static void on_environment_enum(struct json_object *config, int optid, const char *name, int (*func)(const char*))
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/on_environment_enum(struct json_object*,int,const char*,int(*func)(const char*))");AKA_fCall++;
		AKA_mark("lis===1023###sois===26539###eois===26573###lif===3###soif===142###eoif===176###ins===true###function===./app-framework-binder/src/afb-args.c/on_environment_enum(struct json_object*,int,const char*,int(*func)(const char*))");char *value = secure_getenv(name);


		if (AKA_mark("lis===1025###sois===26580###eois===26585###lif===5###soif===183###eoif===188###ifc===true###function===./app-framework-binder/src/afb-args.c/on_environment_enum(struct json_object*,int,const char*,int(*func)(const char*))") && (AKA_mark("lis===1025###sois===26580###eois===26585###lif===5###soif===183###eoif===188###isc===true###function===./app-framework-binder/src/afb-args.c/on_environment_enum(struct json_object*,int,const char*,int(*func)(const char*))")&&value)) {
				if (AKA_mark("lis===1026###sois===26595###eois===26612###lif===6###soif===198###eoif===215###ifc===true###function===./app-framework-binder/src/afb-args.c/on_environment_enum(struct json_object*,int,const char*,int(*func)(const char*))") && (AKA_mark("lis===1026###sois===26595###eois===26612###lif===6###soif===198###eoif===215###isc===true###function===./app-framework-binder/src/afb-args.c/on_environment_enum(struct json_object*,int,const char*,int(*func)(const char*))")&&func(value) == -1)) {
			AKA_mark("lis===1027###sois===26617###eois===26695###lif===7###soif===220###eoif===298###ins===true###function===./app-framework-binder/src/afb-args.c/on_environment_enum(struct json_object*,int,const char*,int(*func)(const char*))");WARNING("Unknown value %s for environment variable %s, ignored", value, name);
		}
		else {
			AKA_mark("lis===1029###sois===26706###eois===26743###lif===9###soif===309###eoif===346###ins===true###function===./app-framework-binder/src/afb-args.c/on_environment_enum(struct json_object*,int,const char*,int(*func)(const char*))");config_set_str(config, optid, value);
		}

	}
	else {AKA_mark("lis===-1025-###sois===-26580-###eois===-265805-###lif===-5-###soif===-###eoif===-188-###ins===true###function===./app-framework-binder/src/afb-args.c/on_environment_enum(struct json_object*,int,const char*,int(*func)(const char*))");}

}

/** Instrumented function on_environment_bool(struct json_object*,int,const char*) */
static void on_environment_bool(struct json_object *config, int optid, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/on_environment_bool(struct json_object*,int,const char*)");AKA_fCall++;
		AKA_mark("lis===1035###sois===26842###eois===26876###lif===2###soif===92###eoif===126###ins===true###function===./app-framework-binder/src/afb-args.c/on_environment_bool(struct json_object*,int,const char*)");char *value = secure_getenv(name);

		AKA_mark("lis===1036###sois===26878###eois===26889###lif===3###soif===128###eoif===139###ins===true###function===./app-framework-binder/src/afb-args.c/on_environment_bool(struct json_object*,int,const char*)");int asbool;


		if (AKA_mark("lis===1038###sois===26896###eois===26901###lif===5###soif===146###eoif===151###ifc===true###function===./app-framework-binder/src/afb-args.c/on_environment_bool(struct json_object*,int,const char*)") && (AKA_mark("lis===1038###sois===26896###eois===26901###lif===5###soif===146###eoif===151###isc===true###function===./app-framework-binder/src/afb-args.c/on_environment_bool(struct json_object*,int,const char*)")&&value)) {
				AKA_mark("lis===1039###sois===26907###eois===26938###lif===6###soif===157###eoif===188###ins===true###function===./app-framework-binder/src/afb-args.c/on_environment_bool(struct json_object*,int,const char*)");asbool = string_to_bool(value);

				if (AKA_mark("lis===1040###sois===26945###eois===26955###lif===7###soif===195###eoif===205###ifc===true###function===./app-framework-binder/src/afb-args.c/on_environment_bool(struct json_object*,int,const char*)") && (AKA_mark("lis===1040###sois===26945###eois===26955###lif===7###soif===195###eoif===205###isc===true###function===./app-framework-binder/src/afb-args.c/on_environment_bool(struct json_object*,int,const char*)")&&asbool < 0)) {
			AKA_mark("lis===1041###sois===26960###eois===27038###lif===8###soif===210###eoif===288###ins===true###function===./app-framework-binder/src/afb-args.c/on_environment_bool(struct json_object*,int,const char*)");WARNING("Unknown value %s for environment variable %s, ignored", value, name);
		}
		else {
			AKA_mark("lis===1043###sois===27049###eois===27088###lif===10###soif===299###eoif===338###ins===true###function===./app-framework-binder/src/afb-args.c/on_environment_bool(struct json_object*,int,const char*)");config_set_bool(config, optid, asbool);
		}

	}
	else {AKA_mark("lis===-1038-###sois===-26896-###eois===-268965-###lif===-5-###soif===-###eoif===-151-###ins===true###function===./app-framework-binder/src/afb-args.c/on_environment_bool(struct json_object*,int,const char*)");}

}

/** Instrumented function parse_environment(struct json_object*) */
static void parse_environment(struct json_object *config)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/parse_environment(struct json_object*)");AKA_fCall++;
#if WITH_AFB_HOOK
	on_environment_enum(config, SET_TRACEREQ, "AFB_TRACEREQ", afb_hook_flags_xreq_from_text);
	on_environment_enum(config, SET_TRACEEVT, "AFB_TRACEEVT", afb_hook_flags_evt_from_text);
	on_environment_enum(config, SET_TRACESES, "AFB_TRACESES", afb_hook_flags_session_from_text);
	on_environment_enum(config, SET_TRACEAPI, "AFB_TRACEAPI", afb_hook_flags_api_from_text);
	on_environment_enum(config, SET_TRACEGLOB, "AFB_TRACEGLOB", afb_hook_flags_global_from_text);
#if !defined(REMOVE_LEGACY_TRACE)
	on_environment_enum(config, SET_TRACEDITF, "AFB_TRACEDITF", afb_hook_flags_legacy_ditf_from_text);
	on_environment_enum(config, SET_TRACESVC, "AFB_TRACESVC", afb_hook_flags_legacy_svc_from_text);
#endif
#endif
#if WITH_DYNAMIC_BINDING
	on_environment(config, ADD_LDPATH, "AFB_LDPATHS", config_add_str);
#endif
		AKA_mark("lis===1063###sois===27979###eois===28039###lif===16###soif===884###eoif===944###ins===true###function===./app-framework-binder/src/afb-args.c/parse_environment(struct json_object*)");on_environment(config, ADD_SET, "AFB_SET", config_mix2_str);

		AKA_mark("lis===1064###sois===28041###eois===28105###lif===17###soif===946###eoif===1010###ins===true###function===./app-framework-binder/src/afb-args.c/parse_environment(struct json_object*)");on_environment_bool(config, SET_TRAP_FAULTS, "AFB_TRAP_FAULTS");

}

/** Instrumented function afb_args_parse(int,char**) */
struct json_object *afb_args_parse(int argc, char **argv)
{AKA_mark("Calling: ./app-framework-binder/src/afb-args.c/afb_args_parse(int,char**)");AKA_fCall++;
		AKA_mark("lis===1069###sois===28170###eois===28197###lif===2###soif===61###eoif===88###ins===true###function===./app-framework-binder/src/afb-args.c/afb_args_parse(int,char**)");struct json_object *result;


		AKA_mark("lis===1071###sois===28200###eois===28215###lif===4###soif===91###eoif===106###ins===true###function===./app-framework-binder/src/afb-args.c/afb_args_parse(int,char**)");init_options();


		AKA_mark("lis===1073###sois===28218###eois===28252###lif===6###soif===109###eoif===143###ins===true###function===./app-framework-binder/src/afb-args.c/afb_args_parse(int,char**)");result = json_object_new_object();


		AKA_mark("lis===1075###sois===28255###eois===28281###lif===8###soif===146###eoif===172###ins===true###function===./app-framework-binder/src/afb-args.c/afb_args_parse(int,char**)");parse_environment(result);

		AKA_mark("lis===1076###sois===28283###eois===28319###lif===9###soif===174###eoif===210###ins===true###function===./app-framework-binder/src/afb-args.c/afb_args_parse(int,char**)");parse_arguments(argc, argv, result);

		AKA_mark("lis===1077###sois===28321###eois===28344###lif===10###soif===212###eoif===235###ins===true###function===./app-framework-binder/src/afb-args.c/afb_args_parse(int,char**)");fulfill_config(result);

		AKA_mark("lis===1078###sois===28350###eois===28379###lif===11###soif===241###eoif===270###function===./app-framework-binder/src/afb-args.c/afb_args_parse(int,char**)");if (verbose_wants(Log_Level_Info)){
		AKA_mark("lis===1079###sois===28383###eois===28420###lif===12###soif===274###eoif===311###ins===true###function===./app-framework-binder/src/afb-args.c/afb_args_parse(int,char**)");dump(result, stderr, "--", "CONFIG");
	}
	else {AKA_mark("");}

		AKA_mark("lis===1080###sois===28422###eois===28436###lif===13###soif===313###eoif===327###ins===true###function===./app-framework-binder/src/afb-args.c/afb_args_parse(int,char**)");return result;

}



#endif

