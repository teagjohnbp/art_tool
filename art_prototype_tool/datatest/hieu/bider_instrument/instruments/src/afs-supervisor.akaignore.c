/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFS_SUPERVISOR_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFS_SUPERVISOR_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <json-c/json.h>

#define AFB_BINDING_VERSION 3
#define AFB_BINDING_NO_ROOT
#include <afb/afb-binding.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CRED_H_
#define AKA_INCLUDE__AFB_CRED_H_
#include "afb-cred.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_STUB_WS_H_
#define AKA_INCLUDE__AFB_STUB_WS_H_
#include "afb-stub-ws.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_H_
#define AKA_INCLUDE__AFB_API_H_
#include "afb-api.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_XREQ_H_
#define AKA_INCLUDE__AFB_XREQ_H_
#include "afb-xreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_V3_H_
#define AKA_INCLUDE__AFB_API_V3_H_
#include "afb-api-v3.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_APISET_H_
#define AKA_INCLUDE__AFB_APISET_H_
#include "afb-apiset.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_FDEV_H_
#define AKA_INCLUDE__AFB_FDEV_H_
#include "afb-fdev.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SOCKET_H_
#define AKA_INCLUDE__AFB_SOCKET_H_
#include "afb-socket.akaignore.h"
#endif


/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_H_
#define AKA_INCLUDE__FDEV_H_
#include "fdev.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__WRAP_JSON_H_
#define AKA_INCLUDE__WRAP_JSON_H_
#include "wrap-json.akaignore.h"
#endif


/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFS_SUPERVISION_H_
#define AKA_INCLUDE__AFS_SUPERVISION_H_
#include "afs-supervision.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFS_SUPERVISOR_H_
#define AKA_INCLUDE__AFS_SUPERVISOR_H_
#include "afs-supervisor.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFS_DISCOVER_H_
#define AKA_INCLUDE__AFS_DISCOVER_H_
#include "afs-discover.akaignore.h"
#endif


/* supervised items */
struct supervised
{
	/* link to the next supervised */
	struct supervised *next;

	/* credentials of the supervised */
	struct afb_cred *cred;

	/* connection with the supervised */
	struct afb_stub_ws *stub;
};

/* api and apiset name */
static const char supervision_apiname[] = AFS_SUPERVISION_APINAME;
static const char supervisor_apiname[] = AFS_SUPERVISOR_APINAME;

/* the empty apiset */
static struct afb_apiset *empty_apiset;

/* supervision socket path */
static const char supervision_socket_path[] = AFS_SUPERVISION_SOCKET;
static struct fdev *supervision_fdev;

/* global mutex */
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

/* list of supervised daemons */
static struct supervised *superviseds;

/* events */
static afb_event_t event_add_pid;
static afb_event_t event_del_pid;

/*************************************************************************************/


/*************************************************************************************/

/**
 * send on 'fd' an initiator with 'command'
 * return 0 on success or -1 on failure
 */
/** Instrumented function send_initiator(int,const char*) */
static int send_initiator(int fd, const char *command)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");AKA_fCall++;
		AKA_mark("lis===98###sois===2457###eois===2464###lif===2###soif===58###eoif===65###ins===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");int rc;

		AKA_mark("lis===99###sois===2466###eois===2478###lif===3###soif===67###eoif===79###ins===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");ssize_t swr;

		AKA_mark("lis===100###sois===2480###eois===2517###lif===4###soif===81###eoif===118###ins===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");struct afs_supervision_initiator asi;


	/* set  */
		AKA_mark("lis===103###sois===2532###eois===2560###lif===7###soif===133###eoif===161###ins===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");memset(&asi, 0, sizeof asi);

		AKA_mark("lis===104###sois===2562###eois===2613###lif===8###soif===163###eoif===214###ins===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");strcpy(asi.interface, AFS_SUPERVISION_INTERFACE_1);

		if (AKA_mark("lis===105###sois===2619###eois===2626###lif===9###soif===220###eoif===227###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)") && (AKA_mark("lis===105###sois===2619###eois===2626###lif===9###soif===220###eoif===227###isc===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)")&&command)) {
		AKA_mark("lis===106###sois===2630###eois===2680###lif===10###soif===231###eoif===281###ins===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");strncpy(asi.extra, command, sizeof asi.extra - 1);
	}
	else {AKA_mark("lis===-105-###sois===-2619-###eois===-26197-###lif===-9-###soif===-###eoif===-227-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");}


	/* send the initiator */
		AKA_mark("lis===109###sois===2709###eois===2743###lif===13###soif===310###eoif===344###ins===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");swr = write(fd, &asi, sizeof asi);

		if (AKA_mark("lis===110###sois===2749###eois===2756###lif===14###soif===350###eoif===357###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)") && (AKA_mark("lis===110###sois===2749###eois===2756###lif===14###soif===350###eoif===357###isc===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)")&&swr < 0)) {
				AKA_mark("lis===111###sois===2762###eois===2796###lif===15###soif===363###eoif===397###ins===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");ERROR("Can't send initiator: %m");

				AKA_mark("lis===112###sois===2799###eois===2807###lif===16###soif===400###eoif===408###ins===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");rc = -1;

	}
	else {
		if (AKA_mark("lis===113###sois===2820###eois===2836###lif===17###soif===421###eoif===437###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)") && (AKA_mark("lis===113###sois===2820###eois===2836###lif===17###soif===421###eoif===437###isc===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)")&&swr < sizeof asi)) {
					AKA_mark("lis===114###sois===2842###eois===2884###lif===18###soif===443###eoif===485###ins===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");ERROR("Sending incomplete initiator: %m");

					AKA_mark("lis===115###sois===2887###eois===2895###lif===19###soif===488###eoif===496###ins===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");rc = -1;

	}
		else {
			AKA_mark("lis===117###sois===2906###eois===2913###lif===21###soif===507###eoif===514###ins===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");rc = 0;
		}
	}

		AKA_mark("lis===118###sois===2915###eois===2925###lif===22###soif===516###eoif===526###ins===true###function===./app-framework-binder/src/afs-supervisor.c/send_initiator(int,const char*)");return rc;

}

/*
 * checks whether the incoming supervised represented by its creds
 * is to be accepted or not.
 * return 1 if yes or 0 otherwise.
 */
/** Instrumented function should_accept(struct afb_cred*) */
static int should_accept(struct afb_cred *cred)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/should_accept(struct afb_cred*)");AKA_fCall++;
		AKA_mark("lis===128###sois===3118###eois===3155###lif===2###soif===51###eoif===88###ins===true###function===./app-framework-binder/src/afs-supervisor.c/should_accept(struct afb_cred*)");return cred && cred->pid != getpid();
 /* not me! */
}

/** Instrumented function on_supervised_hangup(struct afb_stub_ws*) */
static void on_supervised_hangup(struct afb_stub_ws *stub)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)");AKA_fCall++;
		AKA_mark("lis===133###sois===3235###eois===3262###lif===2###soif===62###eoif===89###ins===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)");struct supervised *s, **ps;


	/* Search the supervised of the ws-stub */
		AKA_mark("lis===136###sois===3309###eois===3336###lif===5###soif===136###eoif===163###ins===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)");pthread_mutex_lock(&mutex);

		AKA_mark("lis===137###sois===3338###eois===3356###lif===6###soif===165###eoif===183###ins===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)");ps = &superviseds;

		while (AKA_mark("lis===138###sois===3365###eois===3393###lif===7###soif===192###eoif===220###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)") && ((s=(*ps) && AKA_mark("lis===138###sois===3366###eois===3373###lif===7###soif===193###eoif===200###isc===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)"))	&&(AKA_mark("lis===138###sois===3378###eois===3393###lif===7###soif===205###eoif===220###isc===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)")&&s->stub != stub))) {
		AKA_mark("lis===139###sois===3397###eois===3411###lif===8###soif===224###eoif===238###ins===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)");ps = &s->next;
	}


	/* unlink the supervised if found */
		if (AKA_mark("lis===142###sois===3456###eois===3457###lif===11###soif===283###eoif===284###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)") && (AKA_mark("lis===142###sois===3456###eois===3457###lif===11###soif===283###eoif===284###isc===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)")&&s)) {
		AKA_mark("lis===143###sois===3461###eois===3475###lif===12###soif===288###eoif===302###ins===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)");*ps = s->next;
	}
	else {AKA_mark("lis===-142-###sois===-3456-###eois===-34561-###lif===-11-###soif===-###eoif===-284-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)");}

		AKA_mark("lis===144###sois===3477###eois===3506###lif===13###soif===304###eoif===333###ins===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)");pthread_mutex_unlock(&mutex);


	/* forgive the ws-stub */
		AKA_mark("lis===147###sois===3536###eois===3560###lif===16###soif===363###eoif===387###ins===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)");afb_stub_ws_unref(stub);


	/* forgive the supervised */
		if (AKA_mark("lis===150###sois===3597###eois===3598###lif===19###soif===424###eoif===425###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)") && (AKA_mark("lis===150###sois===3597###eois===3598###lif===19###soif===424###eoif===425###isc===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)")&&s)) {
				AKA_mark("lis===151###sois===3604###eois===3674###lif===20###soif===431###eoif===501###ins===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)");afb_event_push(event_del_pid, json_object_new_int((int)s->cred->pid));

				AKA_mark("lis===152###sois===3677###eois===3701###lif===21###soif===504###eoif===528###ins===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)");afb_cred_unref(s->cred);

				AKA_mark("lis===153###sois===3704###eois===3712###lif===22###soif===531###eoif===539###ins===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)");free(s);

	}
	else {AKA_mark("lis===-150-###sois===-3597-###eois===-35971-###lif===-19-###soif===-###eoif===-425-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/on_supervised_hangup(struct afb_stub_ws*)");}

}

/*
 * create a supervised for socket 'fd' and 'cred'
 * return 0 in case of success or -1 in case of error
 */
/** Instrumented function make_supervised(int,struct afb_cred*) */
static int make_supervised(int fd, struct afb_cred *cred)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");AKA_fCall++;
		AKA_mark("lis===163###sois===3891###eois===3912###lif===2###soif===61###eoif===82###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");struct supervised *s;

		AKA_mark("lis===164###sois===3914###eois===3932###lif===3###soif===84###eoif===102###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");struct fdev *fdev;


		AKA_mark("lis===166###sois===3935###eois===3957###lif===5###soif===105###eoif===127###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");s = malloc(sizeof *s);

		if (AKA_mark("lis===167###sois===3963###eois===3965###lif===6###soif===133###eoif===135###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)") && (AKA_mark("lis===167###sois===3963###eois===3965###lif===6###soif===133###eoif===135###isc===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)")&&!s)) {
		AKA_mark("lis===168###sois===3969###eois===3979###lif===7###soif===139###eoif===149###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");return -1;
	}
	else {AKA_mark("lis===-167-###sois===-3963-###eois===-39632-###lif===-6-###soif===-###eoif===-135-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");}


		AKA_mark("lis===170###sois===3982###eois===4009###lif===9###soif===152###eoif===179###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");fdev = afb_fdev_create(fd);

		if (AKA_mark("lis===171###sois===4015###eois===4020###lif===10###soif===185###eoif===190###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)") && (AKA_mark("lis===171###sois===4015###eois===4020###lif===10###soif===185###eoif===190###isc===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)")&&!fdev)) {
				AKA_mark("lis===172###sois===4026###eois===4034###lif===11###soif===196###eoif===204###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");free(s);

				AKA_mark("lis===173###sois===4037###eois===4047###lif===12###soif===207###eoif===217###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");return -1;

	}
	else {AKA_mark("lis===-171-###sois===-4015-###eois===-40155-###lif===-10-###soif===-###eoif===-190-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");}


		AKA_mark("lis===176###sois===4053###eois===4068###lif===15###soif===223###eoif===238###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");s->cred = cred;

		AKA_mark("lis===177###sois===4070###eois===4147###lif===16###soif===240###eoif===317###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");s->stub = afb_stub_ws_create_client(fdev, supervision_apiname, empty_apiset);

		if (AKA_mark("lis===178###sois===4153###eois===4161###lif===17###soif===323###eoif===331###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)") && (AKA_mark("lis===178###sois===4153###eois===4161###lif===17###soif===323###eoif===331###isc===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)")&&!s->stub)) {
				AKA_mark("lis===179###sois===4167###eois===4175###lif===18###soif===337###eoif===345###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");free(s);

				AKA_mark("lis===180###sois===4178###eois===4188###lif===19###soif===348###eoif===358###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");return -1;

	}
	else {AKA_mark("lis===-178-###sois===-4153-###eois===-41538-###lif===-17-###soif===-###eoif===-331-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");}

		AKA_mark("lis===182###sois===4193###eois===4220###lif===21###soif===363###eoif===390###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");pthread_mutex_lock(&mutex);

		AKA_mark("lis===183###sois===4222###eois===4244###lif===22###soif===392###eoif===414###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");s->next = superviseds;

		AKA_mark("lis===184###sois===4246###eois===4262###lif===23###soif===416###eoif===432###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");superviseds = s;

		AKA_mark("lis===185###sois===4264###eois===4293###lif===24###soif===434###eoif===463###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");pthread_mutex_unlock(&mutex);

		AKA_mark("lis===186###sois===4295###eois===4352###lif===25###soif===465###eoif===522###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");afb_stub_ws_set_on_hangup(s->stub, on_supervised_hangup);

		AKA_mark("lis===187###sois===4354###eois===4363###lif===26###soif===524###eoif===533###ins===true###function===./app-framework-binder/src/afs-supervisor.c/make_supervised(int,struct afb_cred*)");return 0;

}

/**
 * Search the supervised of 'pid', return it or NULL.
 */
/** Instrumented function supervised_of_pid(pid_t) */
static struct supervised *supervised_of_pid(pid_t pid)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/supervised_of_pid(pid_t)");AKA_fCall++;
		AKA_mark("lis===195###sois===4487###eois===4508###lif===2###soif===58###eoif===79###ins===true###function===./app-framework-binder/src/afs-supervisor.c/supervised_of_pid(pid_t)");struct supervised *s;


		AKA_mark("lis===197###sois===4511###eois===4538###lif===4###soif===82###eoif===109###ins===true###function===./app-framework-binder/src/afs-supervisor.c/supervised_of_pid(pid_t)");pthread_mutex_lock(&mutex);

		AKA_mark("lis===198###sois===4540###eois===4556###lif===5###soif===111###eoif===127###ins===true###function===./app-framework-binder/src/afs-supervisor.c/supervised_of_pid(pid_t)");s = superviseds;

		while (AKA_mark("lis===199###sois===4565###eois===4589###lif===6###soif===136###eoif===160###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/supervised_of_pid(pid_t)") && ((AKA_mark("lis===199###sois===4565###eois===4566###lif===6###soif===136###eoif===137###isc===true###function===./app-framework-binder/src/afs-supervisor.c/supervised_of_pid(pid_t)")&&s)	&&(AKA_mark("lis===199###sois===4570###eois===4589###lif===6###soif===141###eoif===160###isc===true###function===./app-framework-binder/src/afs-supervisor.c/supervised_of_pid(pid_t)")&&pid != s->cred->pid))) {
		AKA_mark("lis===200###sois===4593###eois===4605###lif===7###soif===164###eoif===176###ins===true###function===./app-framework-binder/src/afs-supervisor.c/supervised_of_pid(pid_t)");s = s->next;
	}

		AKA_mark("lis===201###sois===4607###eois===4636###lif===8###soif===178###eoif===207###ins===true###function===./app-framework-binder/src/afs-supervisor.c/supervised_of_pid(pid_t)");pthread_mutex_unlock(&mutex);


		AKA_mark("lis===203###sois===4639###eois===4648###lif===10###soif===210###eoif===219###ins===true###function===./app-framework-binder/src/afs-supervisor.c/supervised_of_pid(pid_t)");return s;

}

/*
 * handles incoming connection on 'sock'
 */
/** Instrumented function accept_supervision_link(int) */
static void accept_supervision_link(int sock)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");AKA_fCall++;
		AKA_mark("lis===211###sois===4749###eois===4760###lif===2###soif===49###eoif===60###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");int rc, fd;

		AKA_mark("lis===212###sois===4762###eois===4783###lif===3###soif===62###eoif===83###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");struct sockaddr addr;

		AKA_mark("lis===213###sois===4785###eois===4803###lif===4###soif===85###eoif===103###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");socklen_t lenaddr;

		AKA_mark("lis===214###sois===4805###eois===4827###lif===5###soif===105###eoif===127###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");struct afb_cred *cred;


		AKA_mark("lis===216###sois===4830###eois===4863###lif===7###soif===130###eoif===163###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");lenaddr = (socklen_t)sizeof addr;

		AKA_mark("lis===217###sois===4865###eois===4900###lif===8###soif===165###eoif===200###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");fd = accept(sock, &addr, &lenaddr);

		if (AKA_mark("lis===218###sois===4906###eois===4913###lif===9###soif===206###eoif===213###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)") && (AKA_mark("lis===218###sois===4906###eois===4913###lif===9###soif===206###eoif===213###isc===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)")&&fd >= 0)) {
				AKA_mark("lis===219###sois===4919###eois===4957###lif===10###soif===219###eoif===257###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");cred = afb_cred_create_for_socket(fd);

				AKA_mark("lis===220###sois===4960###eois===4985###lif===11###soif===260###eoif===285###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");rc = should_accept(cred);

				if (AKA_mark("lis===221###sois===4992###eois===4994###lif===12###soif===292###eoif===294###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)") && (AKA_mark("lis===221###sois===4992###eois===4994###lif===12###soif===292###eoif===294###isc===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)")&&rc)) {
						AKA_mark("lis===222###sois===5001###eois===5031###lif===13###soif===301###eoif===331###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");rc = send_initiator(fd, NULL);

						if (AKA_mark("lis===223###sois===5039###eois===5042###lif===14###soif===339###eoif===342###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)") && (AKA_mark("lis===223###sois===5039###eois===5042###lif===14###soif===339###eoif===342###isc===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)")&&!rc)) {
								AKA_mark("lis===224###sois===5050###eois===5081###lif===15###soif===350###eoif===381###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");rc = make_supervised(fd, cred);

								if (AKA_mark("lis===225###sois===5090###eois===5093###lif===16###soif===390###eoif===393###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)") && (AKA_mark("lis===225###sois===5090###eois===5093###lif===16###soif===390###eoif===393###isc===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)")&&!rc)) {
										AKA_mark("lis===226###sois===5102###eois===5169###lif===17###soif===402###eoif===469###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");afb_event_push(event_add_pid, json_object_new_int((int)cred->pid));

										AKA_mark("lis===227###sois===5175###eois===5182###lif===18###soif===475###eoif===482###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");return;

				}
				else {AKA_mark("lis===-225-###sois===-5090-###eois===-50903-###lif===-16-###soif===-###eoif===-393-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");}

			}
			else {AKA_mark("lis===-223-###sois===-5039-###eois===-50393-###lif===-14-###soif===-###eoif===-342-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");}

		}
		else {AKA_mark("lis===-221-###sois===-4992-###eois===-49922-###lif===-12-###soif===-###eoif===-294-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");}

				AKA_mark("lis===231###sois===5200###eois===5221###lif===22###soif===500###eoif===521###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");afb_cred_unref(cred);

				AKA_mark("lis===232###sois===5224###eois===5234###lif===23###soif===524###eoif===534###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");close(fd);

	}
	else {AKA_mark("lis===-218-###sois===-4906-###eois===-49067-###lif===-9-###soif===-###eoif===-213-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/accept_supervision_link(int)");}

}

/*
 * handle even on server socket
 */
/** Instrumented function listening(void*,uint32_t,struct fdev*) */
static void listening(void *closure, uint32_t revents, struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/listening(void*,uint32_t,struct fdev*)");AKA_fCall++;
		if (AKA_mark("lis===241###sois===5361###eois===5386###lif===2###soif===81###eoif===106###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/listening(void*,uint32_t,struct fdev*)") && (AKA_mark("lis===241###sois===5361###eois===5386###lif===2###soif===81###eoif===106###isc===true###function===./app-framework-binder/src/afs-supervisor.c/listening(void*,uint32_t,struct fdev*)")&&(revents & EPOLLHUP) != 0)) {
				AKA_mark("lis===242###sois===5392###eois===5427###lif===3###soif===112###eoif===147###ins===true###function===./app-framework-binder/src/afs-supervisor.c/listening(void*,uint32_t,struct fdev*)");ERROR("supervision socket closed");

				AKA_mark("lis===243###sois===5430###eois===5438###lif===4###soif===150###eoif===158###ins===true###function===./app-framework-binder/src/afs-supervisor.c/listening(void*,uint32_t,struct fdev*)");exit(1);

	}
	else {AKA_mark("lis===-241-###sois===-5361-###eois===-536125-###lif===-2-###soif===-###eoif===-106-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/listening(void*,uint32_t,struct fdev*)");}

		if (AKA_mark("lis===245###sois===5447###eois===5471###lif===6###soif===167###eoif===191###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/listening(void*,uint32_t,struct fdev*)") && (AKA_mark("lis===245###sois===5447###eois===5471###lif===6###soif===167###eoif===191###isc===true###function===./app-framework-binder/src/afs-supervisor.c/listening(void*,uint32_t,struct fdev*)")&&(revents & EPOLLIN) != 0)) {
		AKA_mark("lis===246###sois===5475###eois===5523###lif===7###soif===195###eoif===243###ins===true###function===./app-framework-binder/src/afs-supervisor.c/listening(void*,uint32_t,struct fdev*)");accept_supervision_link((int)(intptr_t)closure);
	}
	else {AKA_mark("lis===-245-###sois===-5447-###eois===-544724-###lif===-6-###soif===-###eoif===-191-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/listening(void*,uint32_t,struct fdev*)");}

}

/*
 */
/** Instrumented function discovered_cb(void*,pid_t) */
static void discovered_cb(void *closure, pid_t pid)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/discovered_cb(void*,pid_t)");AKA_fCall++;
		AKA_mark("lis===253###sois===5589###eois===5610###lif===2###soif===55###eoif===76###ins===true###function===./app-framework-binder/src/afs-supervisor.c/discovered_cb(void*,pid_t)");struct supervised *s;


		AKA_mark("lis===255###sois===5613###eois===5640###lif===4###soif===79###eoif===106###ins===true###function===./app-framework-binder/src/afs-supervisor.c/discovered_cb(void*,pid_t)");s = supervised_of_pid(pid);

		if (AKA_mark("lis===256###sois===5646###eois===5648###lif===5###soif===112###eoif===114###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/discovered_cb(void*,pid_t)") && (AKA_mark("lis===256###sois===5646###eois===5648###lif===5###soif===112###eoif===114###isc===true###function===./app-framework-binder/src/afs-supervisor.c/discovered_cb(void*,pid_t)")&&!s)) {
				AKA_mark("lis===257###sois===5654###eois===5673###lif===6###soif===120###eoif===139###ins===true###function===./app-framework-binder/src/afs-supervisor.c/discovered_cb(void*,pid_t)");(*(int*)closure)++;

				AKA_mark("lis===258###sois===5676###eois===5694###lif===7###soif===142###eoif===160###ins===true###function===./app-framework-binder/src/afs-supervisor.c/discovered_cb(void*,pid_t)");kill(pid, SIGHUP);

	}
	else {AKA_mark("lis===-256-###sois===-5646-###eois===-56462-###lif===-5-###soif===-###eoif===-114-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/discovered_cb(void*,pid_t)");}

}

/** Instrumented function afs_supervisor_discover() */
int afs_supervisor_discover()
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/afs_supervisor_discover()");AKA_fCall++;
		AKA_mark("lis===264###sois===5734###eois===5744###lif===2###soif===33###eoif===43###ins===true###function===./app-framework-binder/src/afs-supervisor.c/afs_supervisor_discover()");int n = 0;

		AKA_mark("lis===265###sois===5746###eois===5792###lif===3###soif===45###eoif===91###ins===true###function===./app-framework-binder/src/afs-supervisor.c/afs_supervisor_discover()");afs_discover("afb-daemon", discovered_cb, &n);

		AKA_mark("lis===266###sois===5794###eois===5803###lif===4###soif===93###eoif===102###ins===true###function===./app-framework-binder/src/afs-supervisor.c/afs_supervisor_discover()");return n;

}

/*************************************************************************************/

/** Instrumented function f_subscribe(afb_req_t) */
static void f_subscribe(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===273###sois===5938###eois===5983###lif===2###soif===42###eoif===87###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)");struct json_object *args = afb_req_json(req);

		AKA_mark("lis===274###sois===5985###eois===6000###lif===3###soif===89###eoif===104###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)");int revoke, ok;


		AKA_mark("lis===276###sois===6003###eois===6093###lif===5###soif===107###eoif===197###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)");revoke = json_object_is_type(args, json_type_boolean)
		&& !json_object_get_boolean(args);


		AKA_mark("lis===279###sois===6096###eois===6103###lif===8###soif===200###eoif===207###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)");ok = 1;

		if (AKA_mark("lis===280###sois===6109###eois===6116###lif===9###soif===213###eoif===220###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)") && (AKA_mark("lis===280###sois===6109###eois===6116###lif===9###soif===213###eoif===220###isc===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)")&&!revoke)) {
				AKA_mark("lis===281###sois===6122###eois===6211###lif===10###soif===226###eoif===315###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)");ok = !afb_req_subscribe(req, event_add_pid)
			&& !afb_req_subscribe(req, event_del_pid);

	}
	else {AKA_mark("lis===-280-###sois===-6109-###eois===-61097-###lif===-9-###soif===-###eoif===-220-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)");}

		if (AKA_mark("lis===284###sois===6220###eois===6233###lif===13###soif===324###eoif===337###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)") && ((AKA_mark("lis===284###sois===6220###eois===6226###lif===13###soif===324###eoif===330###isc===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)")&&revoke)	||(AKA_mark("lis===284###sois===6230###eois===6233###lif===13###soif===334###eoif===337###isc===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)")&&!ok))) {
				AKA_mark("lis===285###sois===6239###eois===6279###lif===14###soif===343###eoif===383###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)");afb_req_unsubscribe(req, event_add_pid);

				AKA_mark("lis===286###sois===6282###eois===6322###lif===15###soif===386###eoif===426###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)");afb_req_unsubscribe(req, event_del_pid);

	}
	else {AKA_mark("lis===-284-###sois===-6220-###eois===-622013-###lif===-13-###soif===-###eoif===-337-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)");}

		AKA_mark("lis===288###sois===6327###eois===6379###lif===17###soif===431###eoif===483###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_subscribe(afb_req_t)");afb_req_reply(req, NULL, ok ? NULL : "error", NULL);

}

/** Instrumented function f_list(afb_req_t) */
static void f_list(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/f_list(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===293###sois===6420###eois===6433###lif===2###soif===37###eoif===50###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_list(afb_req_t)");char pid[50];

		AKA_mark("lis===294###sois===6435###eois===6467###lif===3###soif===52###eoif===84###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_list(afb_req_t)");struct json_object *resu, *item;

		AKA_mark("lis===295###sois===6469###eois===6490###lif===4###soif===86###eoif===107###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_list(afb_req_t)");struct supervised *s;


		AKA_mark("lis===297###sois===6493###eois===6525###lif===6###soif===110###eoif===142###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_list(afb_req_t)");resu = json_object_new_object();

		AKA_mark("lis===298###sois===6527###eois===6543###lif===7###soif===144###eoif===160###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_list(afb_req_t)");s = superviseds;

		while (AKA_mark("lis===299###sois===6552###eois===6553###lif===8###soif===169###eoif===170###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/f_list(afb_req_t)") && (AKA_mark("lis===299###sois===6552###eois===6553###lif===8###soif===169###eoif===170###isc===true###function===./app-framework-binder/src/afs-supervisor.c/f_list(afb_req_t)")&&s)) {
				AKA_mark("lis===300###sois===6559###eois===6597###lif===9###soif===176###eoif===214###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_list(afb_req_t)");sprintf(pid, "%d", (int)s->cred->pid);

				AKA_mark("lis===301###sois===6600###eois===6612###lif===10###soif===217###eoif===229###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_list(afb_req_t)");item = NULL;

				AKA_mark("lis===302###sois===6615###eois===6834###lif===11###soif===232###eoif===451###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_list(afb_req_t)");wrap_json_pack(&item, "{si si si ss ss ss}",
				"pid", (int)s->cred->pid,
				"uid", (int)s->cred->uid,
				"gid", (int)s->cred->gid,
				"id", s->cred->id,
				"label", s->cred->label,
				"user", s->cred->user
				);

				AKA_mark("lis===310###sois===6837###eois===6877###lif===19###soif===454###eoif===494###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_list(afb_req_t)");json_object_object_add(resu, pid, item);

				AKA_mark("lis===311###sois===6880###eois===6892###lif===20###soif===497###eoif===509###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_list(afb_req_t)");s = s->next;

	}

		AKA_mark("lis===313###sois===6897###eois===6930###lif===22###soif===514###eoif===547###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_list(afb_req_t)");afb_req_success(req, resu, NULL);

}

/** Instrumented function f_discover(afb_req_t) */
static void f_discover(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/f_discover(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===318###sois===6975###eois===7001###lif===2###soif===41###eoif===67###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_discover(afb_req_t)");afs_supervisor_discover();

		AKA_mark("lis===319###sois===7003###eois===7036###lif===3###soif===69###eoif===102###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_discover(afb_req_t)");afb_req_success(req, NULL, NULL);

}

/** Instrumented function propagate(afb_req_t,const char*) */
static void propagate(afb_req_t req, const char *verb)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");AKA_fCall++;
		AKA_mark("lis===324###sois===7098###eois===7120###lif===2###soif===58###eoif===80###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");struct afb_xreq *xreq;

		AKA_mark("lis===325###sois===7122###eois===7154###lif===3###soif===82###eoif===114###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");struct json_object *args, *item;

		AKA_mark("lis===326###sois===7156###eois===7177###lif===4###soif===116###eoif===137###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");struct supervised *s;

		AKA_mark("lis===327###sois===7179###eois===7203###lif===5###soif===139###eoif===163###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");struct afb_api_item api;

		AKA_mark("lis===328###sois===7205###eois===7211###lif===6###soif===165###eoif===171###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");int p;


		AKA_mark("lis===330###sois===7214###eois===7243###lif===8###soif===174###eoif===203###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");xreq = xreq_from_req_x2(req);

		AKA_mark("lis===331###sois===7245###eois===7272###lif===9###soif===205###eoif===232###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");args = afb_xreq_json(xreq);


	/* extract the pid */
		if (AKA_mark("lis===334###sois===7302###eois===7348###lif===12###soif===262###eoif===308###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)") && (AKA_mark("lis===334###sois===7302###eois===7348###lif===12###soif===262###eoif===308###isc===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)")&&!json_object_object_get_ex(args, "pid", &item))) {
				AKA_mark("lis===335###sois===7354###eois===7397###lif===13###soif===314###eoif===357###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");afb_xreq_reply(xreq, NULL, "no-pid", NULL);

				AKA_mark("lis===336###sois===7400###eois===7407###lif===14###soif===360###eoif===367###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");return;

	}
	else {AKA_mark("lis===-334-###sois===-7302-###eois===-730246-###lif===-12-###soif===-###eoif===-308-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");}

		AKA_mark("lis===338###sois===7412###eois===7422###lif===16###soif===372###eoif===382###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");errno = 0;

		AKA_mark("lis===339###sois===7424###eois===7454###lif===17###soif===384###eoif===414###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");p = json_object_get_int(item);

		if (AKA_mark("lis===340###sois===7460###eois===7471###lif===18###soif===420###eoif===431###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)") && ((AKA_mark("lis===340###sois===7460###eois===7462###lif===18###soif===420###eoif===422###isc===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)")&&!p)	&&(AKA_mark("lis===340###sois===7466###eois===7471###lif===18###soif===426###eoif===431###isc===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)")&&errno))) {
				AKA_mark("lis===341###sois===7477###eois===7521###lif===19###soif===437###eoif===481###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");afb_xreq_reply(xreq, NULL, "bad-pid", NULL);

				AKA_mark("lis===342###sois===7524###eois===7531###lif===20###soif===484###eoif===491###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");return;

	}
	else {AKA_mark("lis===-340-###sois===-7460-###eois===-746011-###lif===-18-###soif===-###eoif===-431-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");}


	/* get supervised of pid */
		AKA_mark("lis===346###sois===7566###eois===7598###lif===24###soif===526###eoif===558###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");s = supervised_of_pid((pid_t)p);

		if (AKA_mark("lis===347###sois===7604###eois===7606###lif===25###soif===564###eoif===566###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)") && (AKA_mark("lis===347###sois===7604###eois===7606###lif===25###soif===564###eoif===566###isc===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)")&&!s)) {
				AKA_mark("lis===348###sois===7612###eois===7658###lif===26###soif===572###eoif===618###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");afb_req_reply(req, NULL, "unknown-pid", NULL);

				AKA_mark("lis===349###sois===7661###eois===7668###lif===27###soif===621###eoif===628###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");return;

	}
	else {AKA_mark("lis===-347-###sois===-7604-###eois===-76042-###lif===-25-###soif===-###eoif===-566-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");}

		AKA_mark("lis===351###sois===7673###eois===7709###lif===29###soif===633###eoif===669###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");json_object_object_del(args, "pid");


	/* replace the verb to call if needed */
		if (AKA_mark("lis===354###sois===7758###eois===7762###lif===32###soif===718###eoif===722###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)") && (AKA_mark("lis===354###sois===7758###eois===7762###lif===32###soif===718###eoif===722###isc===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)")&&verb)) {
		AKA_mark("lis===355###sois===7766###eois===7799###lif===33###soif===726###eoif===759###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");xreq->request.called_verb = verb;
	}
	else {AKA_mark("lis===-354-###sois===-7758-###eois===-77584-###lif===-32-###soif===-###eoif===-722-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");}


	/* call it now */
		AKA_mark("lis===358###sois===7821###eois===7859###lif===36###soif===781###eoif===819###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");api = afb_stub_ws_client_api(s->stub);

		AKA_mark("lis===359###sois===7861###eois===7894###lif===37###soif===821###eoif===854###ins===true###function===./app-framework-binder/src/afs-supervisor.c/propagate(afb_req_t,const char*)");api.itf->call(api.closure, xreq);

}

/** Instrumented function f_do(afb_req_t) */
static void f_do(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/f_do(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===364###sois===7933###eois===7954###lif===2###soif===35###eoif===56###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_do(afb_req_t)");propagate(req, NULL);

}

/** Instrumented function f_config(afb_req_t) */
static void f_config(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/f_config(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===369###sois===7997###eois===8018###lif===2###soif===39###eoif===60###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_config(afb_req_t)");propagate(req, NULL);

}

/** Instrumented function f_trace(afb_req_t) */
static void f_trace(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/f_trace(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===374###sois===8060###eois===8081###lif===2###soif===38###eoif===59###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_trace(afb_req_t)");propagate(req, NULL);

}

/** Instrumented function f_sessions(afb_req_t) */
static void f_sessions(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/f_sessions(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===379###sois===8126###eois===8150###lif===2###soif===41###eoif===65###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_sessions(afb_req_t)");propagate(req, "slist");

}

/** Instrumented function f_session_close(afb_req_t) */
static void f_session_close(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/f_session_close(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===384###sois===8200###eois===8225###lif===2###soif===46###eoif===71###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_session_close(afb_req_t)");propagate(req, "sclose");

}

/** Instrumented function f_exit(afb_req_t) */
static void f_exit(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/f_exit(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===389###sois===8266###eois===8287###lif===2###soif===37###eoif===58###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_exit(afb_req_t)");propagate(req, NULL);

		AKA_mark("lis===390###sois===8289###eois===8322###lif===3###soif===60###eoif===93###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_exit(afb_req_t)");afb_req_success(req, NULL, NULL);

}

/** Instrumented function f_debug_wait(afb_req_t) */
static void f_debug_wait(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/f_debug_wait(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===395###sois===8369###eois===8392###lif===2###soif===43###eoif===66###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_debug_wait(afb_req_t)");propagate(req, "wait");

		AKA_mark("lis===396###sois===8394###eois===8427###lif===3###soif===68###eoif===101###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_debug_wait(afb_req_t)");afb_req_success(req, NULL, NULL);

}

/** Instrumented function f_debug_break(afb_req_t) */
static void f_debug_break(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/f_debug_break(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===401###sois===8475###eois===8499###lif===2###soif===44###eoif===68###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_debug_break(afb_req_t)");propagate(req, "break");

		AKA_mark("lis===402###sois===8501###eois===8534###lif===3###soif===70###eoif===103###ins===true###function===./app-framework-binder/src/afs-supervisor.c/f_debug_break(afb_req_t)");afb_req_success(req, NULL, NULL);

}

/*************************************************************************************/

/**
 * initialize the supervisor
 */
/** Instrumented function init_supervisor(afb_api_t) */
static int init_supervisor(afb_api_t api)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");AKA_fCall++;
		AKA_mark("lis===412###sois===8709###eois===8760###lif===2###soif===45###eoif===96###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");event_add_pid = afb_api_make_event(api, "add-pid");

		if (AKA_mark("lis===413###sois===8766###eois===8800###lif===3###soif===102###eoif===136###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)") && (AKA_mark("lis===413###sois===8766###eois===8800###lif===3###soif===102###eoif===136###isc===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)")&&!afb_event_is_valid(event_add_pid))) {
				AKA_mark("lis===414###sois===8806###eois===8840###lif===4###soif===142###eoif===176###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");ERROR("Can't create added event");

				AKA_mark("lis===415###sois===8843###eois===8853###lif===5###soif===179###eoif===189###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");return -1;

	}
	else {AKA_mark("lis===-413-###sois===-8766-###eois===-876634-###lif===-3-###soif===-###eoif===-136-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");}


		AKA_mark("lis===418###sois===8859###eois===8910###lif===8###soif===195###eoif===246###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");event_del_pid = afb_api_make_event(api, "del-pid");

		if (AKA_mark("lis===419###sois===8916###eois===8950###lif===9###soif===252###eoif===286###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)") && (AKA_mark("lis===419###sois===8916###eois===8950###lif===9###soif===252###eoif===286###isc===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)")&&!afb_event_is_valid(event_del_pid))) {
				AKA_mark("lis===420###sois===8956###eois===8992###lif===10###soif===292###eoif===328###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");ERROR("Can't create deleted event");

				AKA_mark("lis===421###sois===8995###eois===9005###lif===11###soif===331###eoif===341###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");return -1;

	}
	else {AKA_mark("lis===-419-###sois===-8916-###eois===-891634-###lif===-9-###soif===-###eoif===-286-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");}


	/* create an empty set for superviseds */
		AKA_mark("lis===425###sois===9054###eois===9111###lif===15###soif===390###eoif===447###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");empty_apiset = afb_apiset_create(supervision_apiname, 0);

		if (AKA_mark("lis===426###sois===9117###eois===9130###lif===16###soif===453###eoif===466###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)") && (AKA_mark("lis===426###sois===9117###eois===9130###lif===16###soif===453###eoif===466###isc===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)")&&!empty_apiset)) {
				AKA_mark("lis===427###sois===9136###eois===9177###lif===17###soif===472###eoif===513###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");ERROR("Can't create supervision apiset");

				AKA_mark("lis===428###sois===9180###eois===9190###lif===18###soif===516###eoif===526###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");return -1;

	}
	else {AKA_mark("lis===-426-###sois===-9117-###eois===-911713-###lif===-16-###soif===-###eoif===-466-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");}


	/* create the supervision socket */
		AKA_mark("lis===432###sois===9233###eois===9301###lif===22###soif===569###eoif===637###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");supervision_fdev = afb_socket_open_fdev(supervision_socket_path, 1);

		if (AKA_mark("lis===433###sois===9307###eois===9324###lif===23###soif===643###eoif===660###ifc===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)") && (AKA_mark("lis===433###sois===9307###eois===9324###lif===23###soif===643###eoif===660###isc===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)")&&!supervision_fdev)) {
		AKA_mark("lis===434###sois===9328###eois===9338###lif===24###soif===664###eoif===674###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");return -1;
	}
	else {AKA_mark("lis===-433-###sois===-9307-###eois===-930717-###lif===-23-###soif===-###eoif===-660-###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");}


		AKA_mark("lis===436###sois===9341###eois===9384###lif===26###soif===677###eoif===720###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");fdev_set_events(supervision_fdev, EPOLLIN);

		AKA_mark("lis===437###sois===9386###eois===9482###lif===27###soif===722###eoif===818###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");fdev_set_callback(supervision_fdev, listening,
			  (void*)(intptr_t)fdev_fd(supervision_fdev));


		AKA_mark("lis===440###sois===9485###eois===9494###lif===30###soif===821###eoif===830###ins===true###function===./app-framework-binder/src/afs-supervisor.c/init_supervisor(afb_api_t)");return 0;

}

/*************************************************************************************/

static const struct afb_auth _afb_auths_v2_supervisor[] = {
	/* 0 */
	{
		.type = afb_auth_Permission,
		.text = "urn:AGL:permission:#supervision:platform:access"
	}
};

static const struct afb_verb_v3 _afb_verbs_supervisor[] = {
    {
        .verb = "subscribe",
        .callback = f_subscribe,
        .auth = &_afb_auths_v2_supervisor[0],
        .info = NULL,
        .session = AFB_SESSION_CHECK_X2
    },
    {
        .verb = "list",
        .callback = f_list,
        .auth = &_afb_auths_v2_supervisor[0],
        .info = NULL,
        .session = AFB_SESSION_CHECK_X2
    },
    {
        .verb = "config",
        .callback = f_config,
        .auth = &_afb_auths_v2_supervisor[0],
        .info = NULL,
        .session = AFB_SESSION_CHECK_X2
    },
    {
        .verb = "do",
        .callback = f_do,
        .auth = &_afb_auths_v2_supervisor[0],
        .info = NULL,
        .session = AFB_SESSION_CHECK_X2
    },
    {
        .verb = "trace",
        .callback = f_trace,
        .auth = &_afb_auths_v2_supervisor[0],
        .info = NULL,
        .session = AFB_SESSION_CHECK_X2
    },
    {
        .verb = "sessions",
        .callback = f_sessions,
        .auth = &_afb_auths_v2_supervisor[0],
        .info = NULL,
        .session = AFB_SESSION_CHECK_X2
    },
    {
        .verb = "session-close",
        .callback = f_session_close,
        .auth = &_afb_auths_v2_supervisor[0],
        .info = NULL,
        .session = AFB_SESSION_CHECK_X2
    },
    {
        .verb = "exit",
        .callback = f_exit,
        .auth = &_afb_auths_v2_supervisor[0],
        .info = NULL,
        .session = AFB_SESSION_CHECK_X2
    },
    {
        .verb = "debug-wait",
        .callback = f_debug_wait,
        .auth = &_afb_auths_v2_supervisor[0],
        .info = NULL,
        .session = AFB_SESSION_CHECK_X2
    },
    {
        .verb = "debug-break",
        .callback = f_debug_break,
        .auth = &_afb_auths_v2_supervisor[0],
        .info = NULL,
        .session = AFB_SESSION_CHECK_X2
    },
    {
        .verb = "discover",
        .callback = f_discover,
        .auth = &_afb_auths_v2_supervisor[0],
        .info = NULL,
        .session = AFB_SESSION_CHECK_X2
    },
    { .verb = NULL }
};

static const struct afb_binding_v3 _afb_binding_supervisor = {
    .api = supervisor_apiname,
    .specification = NULL,
    .info = NULL,
    .verbs = _afb_verbs_supervisor,
    .preinit = NULL,
    .init = init_supervisor,
    .onevent = NULL,
    .noconcurrency = 0
};

/** Instrumented function afs_supervisor_add(struct afb_apiset*,struct afb_apiset*) */
int afs_supervisor_add(
		struct afb_apiset *declare_set,
		struct afb_apiset * call_set)
{AKA_mark("Calling: ./app-framework-binder/src/afs-supervisor.c/afs_supervisor_add(struct afb_apiset*,struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===549###sois===12183###eois===12265###lif===4###soif===93###eoif===175###ins===true###function===./app-framework-binder/src/afs-supervisor.c/afs_supervisor_add(struct afb_apiset*,struct afb_apiset*)");return -!afb_api_v3_from_binding(&_afb_binding_supervisor, declare_set, call_set);

}


#endif

