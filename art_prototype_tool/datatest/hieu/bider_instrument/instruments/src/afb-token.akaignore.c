/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_TOKEN_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_TOKEN_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author "Fulup Ar Foll"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_TOKEN_H_
#define AKA_INCLUDE__AFB_TOKEN_H_
#include "afb-token.akaignore.h"
#endif


/**
 * structure for recording a token
 */
struct afb_token
{
	/** link to the next token of the list */
	struct afb_token *next;

	/** reference of the token */
	uint16_t refcount;

	/** local numeric id of the token */
	uint16_t id;

	/** string value of the token */
	char text[];
};

struct tokenset
{
	struct afb_token *first;
	pthread_mutex_t mutex;
	uint16_t idgen;
};

static struct tokenset tokenset = {
	.first = 0,
	.mutex = PTHREAD_MUTEX_INITIALIZER,
	.idgen = 0
};

/** Instrumented function searchid(uint16_t) */
static struct afb_token *searchid(uint16_t id)
{AKA_mark("Calling: ./app-framework-binder/src/afb-token.c/searchid(uint16_t)");AKA_fCall++;
		AKA_mark("lis===62###sois===1345###eois===1382###lif===2###soif===50###eoif===87###ins===true###function===./app-framework-binder/src/afb-token.c/searchid(uint16_t)");struct afb_token *r = tokenset.first;

		while (AKA_mark("lis===63###sois===1391###eois===1407###lif===3###soif===96###eoif===112###ifc===true###function===./app-framework-binder/src/afb-token.c/searchid(uint16_t)") && ((AKA_mark("lis===63###sois===1391###eois===1392###lif===3###soif===96###eoif===97###isc===true###function===./app-framework-binder/src/afb-token.c/searchid(uint16_t)")&&r)	&&(AKA_mark("lis===63###sois===1396###eois===1407###lif===3###soif===101###eoif===112###isc===true###function===./app-framework-binder/src/afb-token.c/searchid(uint16_t)")&&r->id != id))) {
		AKA_mark("lis===64###sois===1411###eois===1423###lif===4###soif===116###eoif===128###ins===true###function===./app-framework-binder/src/afb-token.c/searchid(uint16_t)");r = r->next;
	}

		AKA_mark("lis===65###sois===1425###eois===1434###lif===5###soif===130###eoif===139###ins===true###function===./app-framework-binder/src/afb-token.c/searchid(uint16_t)");return r;

}

/**
 * Get a token for the given value
 *
 * @param token address to return the pointer to the gotten token
 * @param tokenstring string value of the token to get
 * @return 0 in case of success or a -errno like negative code
 */
/** Instrumented function afb_token_get(struct afb_token**,const char*) */
int afb_token_get(struct afb_token **token, const char *tokenstring)
{AKA_mark("Calling: ./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");AKA_fCall++;
		AKA_mark("lis===77###sois===1740###eois===1747###lif===2###soif===72###eoif===79###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");int rc;

		AKA_mark("lis===78###sois===1749###eois===1771###lif===3###soif===81###eoif===103###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");struct afb_token *tok;

		AKA_mark("lis===79###sois===1773###eois===1787###lif===4###soif===105###eoif===119###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");size_t length;


	/* get length of the token string */
		AKA_mark("lis===82###sois===1828###eois===1860###lif===7###soif===160###eoif===192###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");length =  + strlen(tokenstring);


	/* concurrency */
		AKA_mark("lis===85###sois===1882###eois===1918###lif===10###soif===214###eoif===250###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");pthread_mutex_lock(&tokenset.mutex);


	/* search the token */
		AKA_mark("lis===88###sois===1945###eois===1966###lif===13###soif===277###eoif===298###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");tok = tokenset.first;

		while (AKA_mark("lis===89###sois===1975###eois===2045###lif===14###soif===307###eoif===377###ifc===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)") && ((AKA_mark("lis===89###sois===1975###eois===1978###lif===14###soif===307###eoif===310###isc===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)")&&tok)	&&((AKA_mark("lis===89###sois===1983###eois===2021###lif===14###soif===315###eoif===353###isc===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)")&&memcmp(tokenstring, tok->text, length))	||(AKA_mark("lis===89###sois===2025###eois===2044###lif===14###soif===357###eoif===376###isc===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)")&&tokenstring[length])))) {
		AKA_mark("lis===90###sois===2049###eois===2065###lif===15###soif===381###eoif===397###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");tok = tok->next;
	}


	/* search done */
		if (AKA_mark("lis===93###sois===2091###eois===2094###lif===18###soif===423###eoif===426###ifc===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)") && (AKA_mark("lis===93###sois===2091###eois===2094###lif===18###soif===423###eoif===426###isc===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)")&&tok)) {
		/* found */
				AKA_mark("lis===95###sois===2114###eois===2142###lif===20###soif===446###eoif===474###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");tok = afb_token_addref(tok);

				AKA_mark("lis===96###sois===2145###eois===2152###lif===21###soif===477###eoif===484###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");rc = 0;

	}
	else {
		/* not found, create */
				AKA_mark("lis===99###sois===2191###eois===2230###lif===24###soif===523###eoif===562###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");tok = malloc(length + 1 + sizeof *tok);

				if (AKA_mark("lis===100###sois===2237###eois===2241###lif===25###soif===569###eoif===573###ifc===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)") && (AKA_mark("lis===100###sois===2237###eois===2241###lif===25###soif===569###eoif===573###isc===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)")&&!tok)) {
			AKA_mark("lis===102###sois===2271###eois===2284###lif===27###soif===603###eoif===616###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");rc = -ENOMEM;
		}
		else {
						while (AKA_mark("lis===104###sois===2303###eois===2348###lif===29###soif===635###eoif===680###ifc===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)") && ((AKA_mark("lis===104###sois===2303###eois===2320###lif===29###soif===635###eoif===652###isc===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)")&&!++tokenset.idgen)	||(AKA_mark("lis===104###sois===2324###eois===2348###lif===29###soif===656###eoif===680###isc===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)")&&searchid(tokenset.idgen)))) {
				;
			}

						AKA_mark("lis===105###sois===2354###eois===2381###lif===30###soif===686###eoif===713###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");tok->next = tokenset.first;

						AKA_mark("lis===106###sois===2385###eois===2406###lif===31###soif===717###eoif===738###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");tokenset.first = tok;

						AKA_mark("lis===107###sois===2410###eois===2435###lif===32###soif===742###eoif===767###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");tok->id = tokenset.idgen;

						AKA_mark("lis===108###sois===2439###eois===2457###lif===33###soif===771###eoif===789###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");tok->refcount = 1;

						AKA_mark("lis===109###sois===2461###eois===2504###lif===34###soif===793###eoif===836###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");memcpy(tok->text, tokenstring, length + 1);

						AKA_mark("lis===110###sois===2508###eois===2515###lif===35###soif===840###eoif===847###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");rc = 0;

		}

	}

		AKA_mark("lis===113###sois===2524###eois===2562###lif===38###soif===856###eoif===894###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");pthread_mutex_unlock(&tokenset.mutex);

		AKA_mark("lis===114###sois===2564###eois===2577###lif===39###soif===896###eoif===909###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");*token = tok;

		AKA_mark("lis===115###sois===2579###eois===2589###lif===40###soif===911###eoif===921###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_get(struct afb_token**,const char*)");return rc;

}

/**
 * Add a reference count to the given token
 *
 * @param token the token to reference
 * @return the token with the reference added
 */
/** Instrumented function afb_token_addref(struct afb_token*) */
struct afb_token *afb_token_addref(struct afb_token *token)
{AKA_mark("Calling: ./app-framework-binder/src/afb-token.c/afb_token_addref(struct afb_token*)");AKA_fCall++;
		if (AKA_mark("lis===126###sois===2800###eois===2805###lif===2###soif===67###eoif===72###ifc===true###function===./app-framework-binder/src/afb-token.c/afb_token_addref(struct afb_token*)") && (AKA_mark("lis===126###sois===2800###eois===2805###lif===2###soif===67###eoif===72###isc===true###function===./app-framework-binder/src/afb-token.c/afb_token_addref(struct afb_token*)")&&token)) {
		AKA_mark("lis===127###sois===2809###eois===2867###lif===3###soif===76###eoif===134###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_addref(struct afb_token*)");__atomic_add_fetch(&token->refcount, 1, __ATOMIC_RELAXED);
	}
	else {AKA_mark("lis===-126-###sois===-2800-###eois===-28005-###lif===-2-###soif===-###eoif===-72-###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_addref(struct afb_token*)");}

		AKA_mark("lis===128###sois===2869###eois===2882###lif===4###soif===136###eoif===149###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_addref(struct afb_token*)");return token;

}

/**
 * Remove a reference to the given token and clean the memory if needed
 *
 * @param token the token that is unreferenced
 */
/** Instrumented function afb_token_unref(struct afb_token*) */
void afb_token_unref(struct afb_token *token)
{AKA_mark("Calling: ./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)");AKA_fCall++;
		AKA_mark("lis===138###sois===3065###eois===3087###lif===2###soif===49###eoif===71###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)");struct afb_token **pt;

		if (AKA_mark("lis===139###sois===3093###eois===3160###lif===3###soif===77###eoif===144###ifc===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)") && ((AKA_mark("lis===139###sois===3093###eois===3098###lif===3###soif===77###eoif===82###isc===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)")&&token)	&&(AKA_mark("lis===139###sois===3102###eois===3160###lif===3###soif===86###eoif===144###isc===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)")&&!__atomic_sub_fetch(&token->refcount, 1, __ATOMIC_RELAXED)))) {
				AKA_mark("lis===140###sois===3166###eois===3202###lif===4###soif===150###eoif===186###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)");pthread_mutex_lock(&tokenset.mutex);

				AKA_mark("lis===141###sois===3205###eois===3226###lif===5###soif===189###eoif===210###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)");pt = &tokenset.first;

				while (AKA_mark("lis===142###sois===3236###eois===3255###lif===6###soif===220###eoif===239###ifc===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)") && ((AKA_mark("lis===142###sois===3236###eois===3239###lif===6###soif===220###eoif===223###isc===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)")&&*pt)	&&(AKA_mark("lis===142###sois===3243###eois===3255###lif===6###soif===227###eoif===239###isc===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)")&&*pt != token))) {
			AKA_mark("lis===143###sois===3260###eois===3278###lif===7###soif===244###eoif===262###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)");pt = &(*pt)->next;
		}

				if (AKA_mark("lis===144###sois===3285###eois===3288###lif===8###soif===269###eoif===272###ifc===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)") && (AKA_mark("lis===144###sois===3285###eois===3288###lif===8###soif===269###eoif===272###isc===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)")&&*pt)) {
			AKA_mark("lis===145###sois===3293###eois===3311###lif===9###soif===277###eoif===295###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)");*pt = token->next;
		}
		else {AKA_mark("lis===-144-###sois===-3285-###eois===-32853-###lif===-8-###soif===-###eoif===-272-###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)");}

				AKA_mark("lis===146###sois===3314###eois===3352###lif===10###soif===298###eoif===336###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)");pthread_mutex_unlock(&tokenset.mutex);

				AKA_mark("lis===147###sois===3355###eois===3367###lif===11###soif===339###eoif===351###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)");free(token);

	}
	else {AKA_mark("lis===-139-###sois===-3093-###eois===-309367-###lif===-3-###soif===-###eoif===-144-###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_unref(struct afb_token*)");}

}

/**
 * Get the string value of the token
 *
 * @param token the token whose string value is queried
 * @return the string value of the token
 */
/** Instrumented function afb_token_string(const struct afb_token*) */
const char *afb_token_string(const struct afb_token *token)
{AKA_mark("Calling: ./app-framework-binder/src/afb-token.c/afb_token_string(const struct afb_token*)");AKA_fCall++;
		AKA_mark("lis===159###sois===3582###eois===3601###lif===2###soif===63###eoif===82###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_string(const struct afb_token*)");return token->text;

}

/**
 * Get the "local" numeric id of the token
 *
 * @param token the token whose id is queried
 * @return the numeric id of the token
 */
/** Instrumented function afb_token_id(const struct afb_token*) */
uint16_t afb_token_id(const struct afb_token *token)
{AKA_mark("Calling: ./app-framework-binder/src/afb-token.c/afb_token_id(const struct afb_token*)");AKA_fCall++;
		AKA_mark("lis===170###sois===3800###eois===3817###lif===2###soif===56###eoif===73###ins===true###function===./app-framework-binder/src/afb-token.c/afb_token_id(const struct afb_token*)");return token->id;

}

#endif

