/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_XREQ_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_XREQ_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>

#include <json-c/json.h>
#if !defined(JSON_C_TO_STRING_NOSLASHESCAPE)
#define JSON_C_TO_STRING_NOSLASHESCAPE 0
#endif

#include <afb/afb-binding-v1.h>
#include <afb/afb-binding-v2.h>
#include <afb/afb-binding-v3.h>
#include <afb/afb-req-x2.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_H_
#define AKA_INCLUDE__AFB_API_H_
#include "afb-api.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_APISET_H_
#define AKA_INCLUDE__AFB_APISET_H_
#include "afb-apiset.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_AUTH_H_
#define AKA_INCLUDE__AFB_AUTH_H_
#include "afb-auth.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CALLS_H_
#define AKA_INCLUDE__AFB_CALLS_H_
#include "afb-calls.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CONTEXT_H_
#define AKA_INCLUDE__AFB_CONTEXT_H_
#include "afb-context.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_EVT_H_
#define AKA_INCLUDE__AFB_EVT_H_
#include "afb-evt.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CRED_H_
#define AKA_INCLUDE__AFB_CRED_H_
#include "afb-cred.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HOOK_H_
#define AKA_INCLUDE__AFB_HOOK_H_
#include "afb-hook.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_MSG_JSON_H_
#define AKA_INCLUDE__AFB_MSG_JSON_H_
#include "afb-msg-json.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_XREQ_H_
#define AKA_INCLUDE__AFB_XREQ_H_
#include "afb-xreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_ERROR_TEXT_H_
#define AKA_INCLUDE__AFB_ERROR_TEXT_H_
#include "afb-error-text.akaignore.h"
#endif


/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__JOBS_H_
#define AKA_INCLUDE__JOBS_H_
#include "jobs.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif


/******************************************************************************/

/** Instrumented function xreq_finalize(struct afb_xreq*) */
static void xreq_finalize(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_finalize(struct afb_xreq*)");AKA_fCall++;
		if (AKA_mark("lis===55###sois===1442###eois===1456###lif===2###soif===56###eoif===70###ifc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_finalize(struct afb_xreq*)") && (AKA_mark("lis===55###sois===1442###eois===1456###lif===2###soif===56###eoif===70###isc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_finalize(struct afb_xreq*)")&&!xreq->replied)) {
		AKA_mark("lis===56###sois===1460###eois===1521###lif===3###soif===74###eoif===135###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_finalize(struct afb_xreq*)");afb_xreq_reply(xreq, NULL, afb_error_text_not_replied, NULL);
	}
	else {AKA_mark("lis===-55-###sois===-1442-###eois===-144214-###lif===-2-###soif===-###eoif===-70-###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_finalize(struct afb_xreq*)");}

#if WITH_AFB_HOOK
	if (xreq->hookflags)
		afb_hook_xreq_end(xreq);
#endif
		if (AKA_mark("lis===61###sois===1601###eois===1613###lif===8###soif===215###eoif===227###ifc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_finalize(struct afb_xreq*)") && (AKA_mark("lis===61###sois===1601###eois===1613###lif===8###soif===215###eoif===227###isc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_finalize(struct afb_xreq*)")&&xreq->caller)) {
		AKA_mark("lis===62###sois===1617###eois===1655###lif===9###soif===231###eoif===269###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_finalize(struct afb_xreq*)");afb_xreq_unhooked_unref(xreq->caller);
	}
	else {AKA_mark("lis===-61-###sois===-1601-###eois===-160112-###lif===-8-###soif===-###eoif===-227-###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_finalize(struct afb_xreq*)");}

		AKA_mark("lis===63###sois===1657###eois===1685###lif===10###soif===271###eoif===299###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_finalize(struct afb_xreq*)");xreq->queryitf->unref(xreq);

}

/** Instrumented function afb_xreq_unhooked_addref(struct afb_xreq*) */
inline void afb_xreq_unhooked_addref(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_addref(struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===68###sois===1752###eois===1809###lif===2###soif===63###eoif===120###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_addref(struct afb_xreq*)");__atomic_add_fetch(&xreq->refcount, 1, __ATOMIC_RELAXED);

}

/** Instrumented function afb_xreq_unhooked_unref(struct afb_xreq*) */
inline void afb_xreq_unhooked_unref(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_unref(struct afb_xreq*)");AKA_fCall++;
		if (AKA_mark("lis===73###sois===1879###eois===1936###lif===2###soif===66###eoif===123###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_unref(struct afb_xreq*)") && (AKA_mark("lis===73###sois===1879###eois===1936###lif===2###soif===66###eoif===123###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_unref(struct afb_xreq*)")&&!__atomic_sub_fetch(&xreq->refcount, 1, __ATOMIC_RELAXED))) {
		AKA_mark("lis===74###sois===1940###eois===1960###lif===3###soif===127###eoif===147###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_unref(struct afb_xreq*)");xreq_finalize(xreq);
	}
	else {AKA_mark("lis===-73-###sois===-1879-###eois===-187957-###lif===-2-###soif===-###eoif===-123-###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_unref(struct afb_xreq*)");}

}

/******************************************************************************/

/** Instrumented function afb_xreq_unhooked_json(struct afb_xreq*) */
struct json_object *afb_xreq_unhooked_json(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_json(struct afb_xreq*)");AKA_fCall++;
		if (AKA_mark("lis===81###sois===2119###eois===2154###lif===2###soif===73###eoif===108###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_json(struct afb_xreq*)") && ((AKA_mark("lis===81###sois===2119###eois===2130###lif===2###soif===73###eoif===84###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_json(struct afb_xreq*)")&&!xreq->json)	&&(AKA_mark("lis===81###sois===2134###eois===2154###lif===2###soif===88###eoif===108###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_json(struct afb_xreq*)")&&xreq->queryitf->json))) {
		AKA_mark("lis===82###sois===2158###eois===2198###lif===3###soif===112###eoif===152###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_json(struct afb_xreq*)");xreq->json = xreq->queryitf->json(xreq);
	}
	else {AKA_mark("lis===-81-###sois===-2119-###eois===-211935-###lif===-2-###soif===-###eoif===-108-###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_json(struct afb_xreq*)");}

		AKA_mark("lis===83###sois===2200###eois===2218###lif===4###soif===154###eoif===172###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_json(struct afb_xreq*)");return xreq->json;

}

/** Instrumented function xreq_json_cb(struct afb_req_x2*) */
static struct json_object *xreq_json_cb(struct afb_req_x2 *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_json_cb(struct afb_req_x2*)");AKA_fCall++;
		AKA_mark("lis===88###sois===2293###eois===2343###lif===2###soif===71###eoif===121###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_json_cb(struct afb_req_x2*)");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===89###sois===2345###eois===2381###lif===3###soif===123###eoif===159###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_json_cb(struct afb_req_x2*)");return afb_xreq_unhooked_json(xreq);

}

/** Instrumented function xreq_get_cb(struct afb_req_x2*,const char*) */
static struct afb_arg xreq_get_cb(struct afb_req_x2 *closure, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)");AKA_fCall++;
		AKA_mark("lis===94###sois===2468###eois===2518###lif===2###soif===83###eoif===133###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===95###sois===2520###eois===2539###lif===3###soif===135###eoif===154###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)");struct afb_arg arg;

		AKA_mark("lis===96###sois===2541###eois===2576###lif===4###soif===156###eoif===191###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)");struct json_object *object, *value;


		if (AKA_mark("lis===98###sois===2583###eois===2602###lif===6###soif===198###eoif===217###ifc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)") && (AKA_mark("lis===98###sois===2583###eois===2602###lif===6###soif===198###eoif===217###isc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)")&&xreq->queryitf->get)) {
		AKA_mark("lis===99###sois===2606###eois===2644###lif===7###soif===221###eoif===259###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)");arg = xreq->queryitf->get(xreq, name);
	}
	else {
				AKA_mark("lis===101###sois===2655###eois===2686###lif===9###soif===270###eoif===301###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)");object = xreq_json_cb(closure);

				if (AKA_mark("lis===102###sois===2693###eois===2740###lif===10###soif===308###eoif===355###ifc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)") && (AKA_mark("lis===102###sois===2693###eois===2740###lif===10###soif===308###eoif===355###isc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)")&&json_object_object_get_ex(object, name, &value))) {
						AKA_mark("lis===103###sois===2747###eois===2763###lif===11###soif===362###eoif===378###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)");arg.name = name;

						AKA_mark("lis===104###sois===2767###eois===2809###lif===12###soif===382###eoif===424###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)");arg.value = json_object_get_string(value);

		}
		else {
						AKA_mark("lis===106###sois===2824###eois===2840###lif===14###soif===439###eoif===455###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)");arg.name = NULL;

						AKA_mark("lis===107###sois===2844###eois===2861###lif===15###soif===459###eoif===476###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)");arg.value = NULL;

		}

				AKA_mark("lis===109###sois===2868###eois===2884###lif===17###soif===483###eoif===499###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)");arg.path = NULL;

	}

		AKA_mark("lis===111###sois===2889###eois===2900###lif===19###soif===504###eoif===515###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_cb(struct afb_req_x2*,const char*)");return arg;

}

/** Instrumented function xreq_reply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*) */
static void xreq_reply_cb(struct afb_req_x2 *closure, struct json_object *obj, const char *error, const char *info)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_reply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===116###sois===3023###eois===3073###lif===2###soif===119###eoif===169###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_reply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*)");struct afb_xreq *xreq = xreq_from_req_x2(closure);


		if (AKA_mark("lis===118###sois===3080###eois===3093###lif===4###soif===176###eoif===189###ifc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_reply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*)") && (AKA_mark("lis===118###sois===3080###eois===3093###lif===4###soif===176###eoif===189###isc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_reply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*)")&&xreq->replied)) {
				AKA_mark("lis===119###sois===3099###eois===3142###lif===5###soif===195###eoif===238###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_reply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*)");ERROR("reply called more than one time!!");

				AKA_mark("lis===120###sois===3145###eois===3166###lif===6###soif===241###eoif===262###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_reply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*)");json_object_put(obj);

	}
	else {
				AKA_mark("lis===122###sois===3179###eois===3197###lif===8###soif===275###eoif===293###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_reply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*)");xreq->replied = 1;

				AKA_mark("lis===123###sois===3200###eois===3246###lif===9###soif===296###eoif===342###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_reply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*)");xreq->queryitf->reply(xreq, obj, error, info);

	}

}

/** Instrumented function xreq_vreply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*,va_list) */
static void xreq_vreply_cb(struct afb_req_x2 *closure, struct json_object *obj, const char *error, const char *fmt, va_list args)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_vreply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*,va_list)");AKA_fCall++;
		AKA_mark("lis===129###sois===3386###eois===3397###lif===2###soif===133###eoif===144###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vreply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*,va_list)");char *info;

		if (AKA_mark("lis===130###sois===3403###eois===3449###lif===3###soif===150###eoif===196###ifc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vreply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*,va_list)") && ((AKA_mark("lis===130###sois===3403###eois===3414###lif===3###soif===150###eoif===161###isc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vreply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*,va_list)")&&fmt == NULL)	||(AKA_mark("lis===130###sois===3418###eois===3449###lif===3###soif===165###eoif===196###isc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vreply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*,va_list)")&&vasprintf(&info, fmt, args) < 0))) {
		AKA_mark("lis===131###sois===3453###eois===3465###lif===4###soif===200###eoif===212###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vreply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*,va_list)");info = NULL;
	}
	else {AKA_mark("lis===-130-###sois===-3403-###eois===-340346-###lif===-3-###soif===-###eoif===-196-###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vreply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*,va_list)");}

		AKA_mark("lis===132###sois===3467###eois===3508###lif===5###soif===214###eoif===255###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vreply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*,va_list)");xreq_reply_cb(closure, obj, error, info);

		AKA_mark("lis===133###sois===3510###eois===3521###lif===6###soif===257###eoif===268###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vreply_cb(struct afb_req_x2*,struct json_object*,const char*,const char*,va_list)");free(info);

}

/** Instrumented function xreq_legacy_success_cb(struct afb_req_x2*,struct json_object*,const char*) */
static void xreq_legacy_success_cb(struct afb_req_x2 *closure, struct json_object *obj, const char *info)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_legacy_success_cb(struct afb_req_x2*,struct json_object*,const char*)");AKA_fCall++;
		AKA_mark("lis===138###sois===3634###eois===3674###lif===2###soif===109###eoif===149###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_success_cb(struct afb_req_x2*,struct json_object*,const char*)");xreq_reply_cb(closure, obj, NULL, info);

}

/** Instrumented function xreq_legacy_fail_cb(struct afb_req_x2*,const char*,const char*) */
static void xreq_legacy_fail_cb(struct afb_req_x2 *closure, const char *status, const char *info)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_legacy_fail_cb(struct afb_req_x2*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===143###sois===3779###eois===3822###lif===2###soif===101###eoif===144###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_fail_cb(struct afb_req_x2*,const char*,const char*)");xreq_reply_cb(closure, NULL, status, info);

}

/** Instrumented function xreq_legacy_vsuccess_cb(struct afb_req_x2*,struct json_object*,const char*,va_list) */
static void xreq_legacy_vsuccess_cb(struct afb_req_x2 *closure, struct json_object *obj, const char *fmt, va_list args)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_legacy_vsuccess_cb(struct afb_req_x2*,struct json_object*,const char*,va_list)");AKA_fCall++;
		AKA_mark("lis===148###sois===3949###eois===3995###lif===2###soif===123###eoif===169###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_vsuccess_cb(struct afb_req_x2*,struct json_object*,const char*,va_list)");xreq_vreply_cb(closure, obj, NULL, fmt, args);

}

/** Instrumented function xreq_legacy_vfail_cb(struct afb_req_x2*,const char*,const char*,va_list) */
static void xreq_legacy_vfail_cb(struct afb_req_x2 *closure, const char *status, const char *fmt, va_list args)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_legacy_vfail_cb(struct afb_req_x2*,const char*,const char*,va_list)");AKA_fCall++;
		AKA_mark("lis===153###sois===4114###eois===4163###lif===2###soif===115###eoif===164###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_vfail_cb(struct afb_req_x2*,const char*,const char*,va_list)");xreq_vreply_cb(closure, NULL, status, fmt, args);

}

/** Instrumented function xreq_legacy_context_get_cb(struct afb_req_x2*) */
static void *xreq_legacy_context_get_cb(struct afb_req_x2 *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_legacy_context_get_cb(struct afb_req_x2*)");AKA_fCall++;
		AKA_mark("lis===158###sois===4238###eois===4288###lif===2###soif===71###eoif===121###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_context_get_cb(struct afb_req_x2*)");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===159###sois===4290###eois===4329###lif===3###soif===123###eoif===162###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_context_get_cb(struct afb_req_x2*)");return afb_context_get(&xreq->context);

}

/** Instrumented function xreq_legacy_context_set_cb(struct afb_req_x2*,void*,void(*free_value)(void*)) */
static void xreq_legacy_context_set_cb(struct afb_req_x2 *closure, void *value, void (*free_value)(void*))
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_legacy_context_set_cb(struct afb_req_x2*,void*,void(*free_value)(void*))");AKA_fCall++;
		AKA_mark("lis===164###sois===4443###eois===4493###lif===2###soif===110###eoif===160###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_context_set_cb(struct afb_req_x2*,void*,void(*free_value)(void*))");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===165###sois===4495###eois===4546###lif===3###soif===162###eoif===213###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_context_set_cb(struct afb_req_x2*,void*,void(*free_value)(void*))");afb_context_set(&xreq->context, value, free_value);

}

/** Instrumented function xreq_addref_cb(struct afb_req_x2*) */
static struct afb_req_x2 *xreq_addref_cb(struct afb_req_x2 *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_addref_cb(struct afb_req_x2*)");AKA_fCall++;
		AKA_mark("lis===170###sois===4622###eois===4672###lif===2###soif===72###eoif===122###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_addref_cb(struct afb_req_x2*)");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===171###sois===4674###eois===4705###lif===3###soif===124###eoif===155###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_addref_cb(struct afb_req_x2*)");afb_xreq_unhooked_addref(xreq);

		AKA_mark("lis===172###sois===4707###eois===4722###lif===4###soif===157###eoif===172###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_addref_cb(struct afb_req_x2*)");return closure;

}

/** Instrumented function xreq_unref_cb(struct afb_req_x2*) */
static void xreq_unref_cb(struct afb_req_x2 *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_unref_cb(struct afb_req_x2*)");AKA_fCall++;
		AKA_mark("lis===177###sois===4783###eois===4833###lif===2###soif===57###eoif===107###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_unref_cb(struct afb_req_x2*)");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===178###sois===4835###eois===4865###lif===3###soif===109###eoif===139###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_unref_cb(struct afb_req_x2*)");afb_xreq_unhooked_unref(xreq);

}

/** Instrumented function xreq_session_close_cb(struct afb_req_x2*) */
static void xreq_session_close_cb(struct afb_req_x2 *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_session_close_cb(struct afb_req_x2*)");AKA_fCall++;
		AKA_mark("lis===183###sois===4934###eois===4984###lif===2###soif===65###eoif===115###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_session_close_cb(struct afb_req_x2*)");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===184###sois===4986###eois===5020###lif===3###soif===117###eoif===151###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_session_close_cb(struct afb_req_x2*)");afb_context_close(&xreq->context);

}

/** Instrumented function xreq_session_set_LOA_cb(struct afb_req_x2*,unsigned) */
static int xreq_session_set_LOA_cb(struct afb_req_x2 *closure, unsigned level)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_session_set_LOA_cb(struct afb_req_x2*,unsigned)");AKA_fCall++;
		AKA_mark("lis===189###sois===5106###eois===5156###lif===2###soif===82###eoif===132###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_session_set_LOA_cb(struct afb_req_x2*,unsigned)");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===190###sois===5158###eois===5211###lif===3###soif===134###eoif===187###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_session_set_LOA_cb(struct afb_req_x2*,unsigned)");return afb_context_change_loa(&xreq->context, level);

}

/** Instrumented function xreq_subscribe_event_x2_cb(struct afb_req_x2*,struct afb_event_x2*) */
static int xreq_subscribe_event_x2_cb(struct afb_req_x2 *closure, struct afb_event_x2 *event)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_subscribe_event_x2_cb(struct afb_req_x2*,struct afb_event_x2*)");AKA_fCall++;
		AKA_mark("lis===195###sois===5312###eois===5362###lif===2###soif===97###eoif===147###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_subscribe_event_x2_cb(struct afb_req_x2*,struct afb_event_x2*)");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===196###sois===5364###eois===5403###lif===3###soif===149###eoif===188###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_subscribe_event_x2_cb(struct afb_req_x2*,struct afb_event_x2*)");return afb_xreq_subscribe(xreq, event);

}

/** Instrumented function xreq_legacy_subscribe_event_x1_cb(struct afb_req_x2*,struct afb_event_x1) */
static int xreq_legacy_subscribe_event_x1_cb(struct afb_req_x2 *closure, struct afb_event_x1 event)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_legacy_subscribe_event_x1_cb(struct afb_req_x2*,struct afb_event_x1)");AKA_fCall++;
		AKA_mark("lis===201###sois===5510###eois===5586###lif===2###soif===103###eoif===179###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_subscribe_event_x1_cb(struct afb_req_x2*,struct afb_event_x1)");return xreq_subscribe_event_x2_cb(closure, afb_event_x1_to_event_x2(event));

}

/** Instrumented function afb_xreq_subscribe(struct afb_xreq*,struct afb_event_x2*) */
int afb_xreq_subscribe(struct afb_xreq *xreq, struct afb_event_x2 *event)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_subscribe(struct afb_xreq*,struct afb_event_x2*)");AKA_fCall++;
		if (AKA_mark("lis===206###sois===5671###eois===5684###lif===2###soif===81###eoif===94###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_subscribe(struct afb_xreq*,struct afb_event_x2*)") && (AKA_mark("lis===206###sois===5671###eois===5684###lif===2###soif===81###eoif===94###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_subscribe(struct afb_xreq*,struct afb_event_x2*)")&&xreq->replied)) {
				AKA_mark("lis===207###sois===5690###eois===5740###lif===3###soif===100###eoif===150###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_subscribe(struct afb_xreq*,struct afb_event_x2*)");ERROR("request replied, subscription impossible");

				AKA_mark("lis===208###sois===5743###eois===5758###lif===4###soif===153###eoif===168###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_subscribe(struct afb_xreq*,struct afb_event_x2*)");errno = EINVAL;

	}
	else {
				if (AKA_mark("lis===210###sois===5775###eois===5800###lif===6###soif===185###eoif===210###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_subscribe(struct afb_xreq*,struct afb_event_x2*)") && (AKA_mark("lis===210###sois===5775###eois===5800###lif===6###soif===185###eoif===210###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_subscribe(struct afb_xreq*,struct afb_event_x2*)")&&xreq->queryitf->subscribe)) {
			AKA_mark("lis===211###sois===5805###eois===5851###lif===7###soif===215###eoif===261###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_subscribe(struct afb_xreq*,struct afb_event_x2*)");return xreq->queryitf->subscribe(xreq, event);
		}
		else {AKA_mark("lis===-210-###sois===-5775-###eois===-577525-###lif===-6-###soif===-###eoif===-210-###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_subscribe(struct afb_xreq*,struct afb_event_x2*)");}

				AKA_mark("lis===212###sois===5854###eois===5906###lif===8###soif===264###eoif===316###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_subscribe(struct afb_xreq*,struct afb_event_x2*)");ERROR("no event listener, subscription impossible");

				AKA_mark("lis===213###sois===5909###eois===5925###lif===9###soif===319###eoif===335###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_subscribe(struct afb_xreq*,struct afb_event_x2*)");errno = ENOTSUP;

	}

		AKA_mark("lis===215###sois===5930###eois===5940###lif===11###soif===340###eoif===350###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_subscribe(struct afb_xreq*,struct afb_event_x2*)");return -1;

}

/** Instrumented function xreq_unsubscribe_event_x2_cb(struct afb_req_x2*,struct afb_event_x2*) */
static int xreq_unsubscribe_event_x2_cb(struct afb_req_x2 *closure, struct afb_event_x2 *event)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_unsubscribe_event_x2_cb(struct afb_req_x2*,struct afb_event_x2*)");AKA_fCall++;
		AKA_mark("lis===220###sois===6043###eois===6093###lif===2###soif===99###eoif===149###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_unsubscribe_event_x2_cb(struct afb_req_x2*,struct afb_event_x2*)");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===221###sois===6095###eois===6136###lif===3###soif===151###eoif===192###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_unsubscribe_event_x2_cb(struct afb_req_x2*,struct afb_event_x2*)");return afb_xreq_unsubscribe(xreq, event);

}

/** Instrumented function xreq_legacy_unsubscribe_event_x1_cb(struct afb_req_x2*,struct afb_event_x1) */
static int xreq_legacy_unsubscribe_event_x1_cb(struct afb_req_x2 *closure, struct afb_event_x1 event)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_legacy_unsubscribe_event_x1_cb(struct afb_req_x2*,struct afb_event_x1)");AKA_fCall++;
		AKA_mark("lis===226###sois===6245###eois===6323###lif===2###soif===105###eoif===183###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_unsubscribe_event_x1_cb(struct afb_req_x2*,struct afb_event_x1)");return xreq_unsubscribe_event_x2_cb(closure, afb_event_x1_to_event_x2(event));

}

/** Instrumented function afb_xreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*) */
int afb_xreq_unsubscribe(struct afb_xreq *xreq, struct afb_event_x2 *event)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*)");AKA_fCall++;
		if (AKA_mark("lis===231###sois===6410###eois===6423###lif===2###soif===83###eoif===96###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*)") && (AKA_mark("lis===231###sois===6410###eois===6423###lif===2###soif===83###eoif===96###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*)")&&xreq->replied)) {
				AKA_mark("lis===232###sois===6429###eois===6481###lif===3###soif===102###eoif===154###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*)");ERROR("request replied, unsubscription impossible");

				AKA_mark("lis===233###sois===6484###eois===6499###lif===4###soif===157###eoif===172###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*)");errno = EINVAL;

	}
	else {
				if (AKA_mark("lis===235###sois===6516###eois===6543###lif===6###soif===189###eoif===216###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*)") && (AKA_mark("lis===235###sois===6516###eois===6543###lif===6###soif===189###eoif===216###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*)")&&xreq->queryitf->unsubscribe)) {
			AKA_mark("lis===236###sois===6548###eois===6596###lif===7###soif===221###eoif===269###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*)");return xreq->queryitf->unsubscribe(xreq, event);
		}
		else {AKA_mark("lis===-235-###sois===-6516-###eois===-651627-###lif===-6-###soif===-###eoif===-216-###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*)");}

				AKA_mark("lis===237###sois===6599###eois===6653###lif===8###soif===272###eoif===326###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*)");ERROR("no event listener, unsubscription impossible");

				AKA_mark("lis===238###sois===6656###eois===6672###lif===9###soif===329###eoif===345###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*)");errno = ENOTSUP;

	}

		AKA_mark("lis===240###sois===6677###eois===6687###lif===11###soif===350###eoif===360###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*)");return -1;

}

/** Instrumented function xreq_legacy_subcall_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*),void*) */
static void xreq_legacy_subcall_cb(struct afb_req_x2 *req, const char *api, const char *verb, struct json_object *args, void (*callback)(void*, int, struct json_object*), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_legacy_subcall_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*),void*)");AKA_fCall++;
		AKA_mark("lis===245###sois===6880###eois===6926###lif===2###soif===189###eoif===235###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_subcall_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*),void*)");struct afb_xreq *xreq = xreq_from_req_x2(req);

		AKA_mark("lis===246###sois===6928###eois===7005###lif===3###soif===237###eoif===314###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_subcall_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*),void*)");return afb_calls_legacy_subcall_v1(xreq, api, verb, args, callback, closure);

}

/** Instrumented function xreq_legacy_subcall_req_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x1),void*) */
static void xreq_legacy_subcall_req_cb(struct afb_req_x2 *req, const char *api, const char *verb, struct json_object *args, void (*callback)(void*, int, struct json_object*, struct afb_req_x1), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_legacy_subcall_req_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x1),void*)");AKA_fCall++;
		AKA_mark("lis===251###sois===7221###eois===7267###lif===2###soif===212###eoif===258###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_subcall_req_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x1),void*)");struct afb_xreq *xreq = xreq_from_req_x2(req);

		AKA_mark("lis===252###sois===7269###eois===7346###lif===3###soif===260###eoif===337###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_subcall_req_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x1),void*)");return afb_calls_legacy_subcall_v2(xreq, api, verb, args, callback, closure);

}

/** Instrumented function xreq_legacy_subcall_request_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x2*),void*) */
static void xreq_legacy_subcall_request_cb(struct afb_req_x2 *req, const char *api, const char *verb, struct json_object *args, void (*callback)(void*, int, struct json_object*, struct afb_req_x2*), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_legacy_subcall_request_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x2*),void*)");AKA_fCall++;
		AKA_mark("lis===257###sois===7567###eois===7613###lif===2###soif===217###eoif===263###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_subcall_request_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x2*),void*)");struct afb_xreq *xreq = xreq_from_req_x2(req);

		AKA_mark("lis===258###sois===7615###eois===7692###lif===3###soif===265###eoif===342###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_subcall_request_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x2*),void*)");return afb_calls_legacy_subcall_v3(xreq, api, verb, args, callback, closure);

}


/** Instrumented function xreq_legacy_subcallsync_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,struct json_object**) */
static int xreq_legacy_subcallsync_cb(struct afb_req_x2 *req, const char *api, const char *verb, struct json_object *args, struct json_object **result)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_legacy_subcallsync_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,struct json_object**)");AKA_fCall++;
		AKA_mark("lis===264###sois===7852###eois===7898###lif===2###soif===155###eoif===201###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_subcallsync_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,struct json_object**)");struct afb_xreq *xreq = xreq_from_req_x2(req);

		AKA_mark("lis===265###sois===7900###eois===7968###lif===3###soif===203###eoif===271###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_subcallsync_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,struct json_object**)");return afb_calls_legacy_subcall_sync(xreq, api, verb, args, result);

}

/** Instrumented function xreq_vverbose_cb(struct afb_req_x2*,int,const char*,int,const char*,const char*,va_list) */
static void xreq_vverbose_cb(struct afb_req_x2 *closure, int level, const char *file, int line, const char *func, const char *fmt, va_list args)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_vverbose_cb(struct afb_req_x2*,int,const char*,int,const char*,const char*,va_list)");AKA_fCall++;
		AKA_mark("lis===270###sois===8120###eois===8128###lif===2###soif===148###eoif===156###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vverbose_cb(struct afb_req_x2*,int,const char*,int,const char*,const char*,va_list)");char *p;

		AKA_mark("lis===271###sois===8130###eois===8180###lif===3###soif===158###eoif===208###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vverbose_cb(struct afb_req_x2*,int,const char*,int,const char*,const char*,va_list)");struct afb_xreq *xreq = xreq_from_req_x2(closure);


		if (AKA_mark("lis===273###sois===8187###eois===8223###lif===5###soif===215###eoif===251###ifc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vverbose_cb(struct afb_req_x2*,int,const char*,int,const char*,const char*,va_list)") && ((AKA_mark("lis===273###sois===8187###eois===8191###lif===5###soif===215###eoif===219###isc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vverbose_cb(struct afb_req_x2*,int,const char*,int,const char*,const char*,va_list)")&&!fmt)	||(AKA_mark("lis===273###sois===8195###eois===8223###lif===5###soif===223###eoif===251###isc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vverbose_cb(struct afb_req_x2*,int,const char*,int,const char*,const char*,va_list)")&&vasprintf(&p, fmt, args) < 0))) {
		AKA_mark("lis===274###sois===8227###eois===8272###lif===6###soif===255###eoif===300###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vverbose_cb(struct afb_req_x2*,int,const char*,int,const char*,const char*,va_list)");vverbose(level, file, line, func, fmt, args);
	}
	else {
				AKA_mark("lis===276###sois===8283###eois===8364###lif===8###soif===311###eoif===392###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vverbose_cb(struct afb_req_x2*,int,const char*,int,const char*,const char*,va_list)");verbose(level, file, line, func, "[REQ/API %s] %s", xreq->request.called_api, p);

				AKA_mark("lis===277###sois===8367###eois===8375###lif===9###soif===395###eoif===403###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_vverbose_cb(struct afb_req_x2*,int,const char*,int,const char*,const char*,va_list)");free(p);

	}

}

/** Instrumented function xreq_legacy_store_cb(struct afb_req_x2*) */
static struct afb_stored_req *xreq_legacy_store_cb(struct afb_req_x2 *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_legacy_store_cb(struct afb_req_x2*)");AKA_fCall++;
		AKA_mark("lis===283###sois===8464###eois===8488###lif===2###soif===82###eoif===106###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_store_cb(struct afb_req_x2*)");xreq_addref_cb(closure);

		AKA_mark("lis===284###sois===8490###eois===8529###lif===3###soif===108###eoif===147###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_legacy_store_cb(struct afb_req_x2*)");return (struct afb_stored_req*)closure;

}

/** Instrumented function xreq_has_permission_cb(struct afb_req_x2*,const char*) */
static int xreq_has_permission_cb(struct afb_req_x2 *closure, const char *permission)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_has_permission_cb(struct afb_req_x2*,const char*)");AKA_fCall++;
		AKA_mark("lis===289###sois===8622###eois===8672###lif===2###soif===89###eoif===139###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_has_permission_cb(struct afb_req_x2*,const char*)");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===290###sois===8674###eois===8736###lif===3###soif===141###eoif===203###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_has_permission_cb(struct afb_req_x2*,const char*)");return afb_context_has_permission(&xreq->context, permission);

}

/** Instrumented function xreq_get_application_id_cb(struct afb_req_x2*) */
static char *xreq_get_application_id_cb(struct afb_req_x2 *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_get_application_id_cb(struct afb_req_x2*)");AKA_fCall++;
		AKA_mark("lis===295###sois===8811###eois===8861###lif===2###soif===71###eoif===121###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_application_id_cb(struct afb_req_x2*)");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===296###sois===8863###eois===8913###lif===3###soif===123###eoif===173###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_application_id_cb(struct afb_req_x2*)");struct afb_cred *cred = xreq->context.credentials;

		AKA_mark("lis===297###sois===8915###eois===8965###lif===4###soif===175###eoif===225###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_application_id_cb(struct afb_req_x2*)");return cred && cred->id ? strdup(cred->id) : NULL;

}

/** Instrumented function xreq_context_make_cb(struct afb_req_x2*,int,void*(*create_value)(void*),void(*free_value)(void*),void*) */
static void *xreq_context_make_cb(struct afb_req_x2 *closure, int replace, void *(*create_value)(void*), void (*free_value)(void*), void *create_closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_context_make_cb(struct afb_req_x2*,int,void*(*create_value)(void*),void(*free_value)(void*),void*)");AKA_fCall++;
		AKA_mark("lis===302###sois===9126###eois===9176###lif===2###soif===157###eoif===207###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_context_make_cb(struct afb_req_x2*,int,void*(*create_value)(void*),void(*free_value)(void*),void*)");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===303###sois===9178###eois===9269###lif===3###soif===209###eoif===300###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_context_make_cb(struct afb_req_x2*,int,void*(*create_value)(void*),void(*free_value)(void*),void*)");return afb_context_make(&xreq->context, replace, create_value, free_value, create_closure);

}

/** Instrumented function xreq_get_uid_cb(struct afb_req_x2*) */
static int xreq_get_uid_cb(struct afb_req_x2 *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_get_uid_cb(struct afb_req_x2*)");AKA_fCall++;
		AKA_mark("lis===308###sois===9331###eois===9381###lif===2###soif===58###eoif===108###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_uid_cb(struct afb_req_x2*)");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===309###sois===9383###eois===9433###lif===3###soif===110###eoif===160###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_uid_cb(struct afb_req_x2*)");struct afb_cred *cred = xreq->context.credentials;

		AKA_mark("lis===310###sois===9435###eois===9481###lif===4###soif===162###eoif===208###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_uid_cb(struct afb_req_x2*)");return cred && cred->id ? (int)cred->uid : -1;

}

/** Instrumented function xreq_get_client_info_cb(struct afb_req_x2*) */
static struct json_object *xreq_get_client_info_cb(struct afb_req_x2 *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");AKA_fCall++;
		AKA_mark("lis===315###sois===9567###eois===9617###lif===2###soif===82###eoif===132###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");struct afb_xreq *xreq = xreq_from_req_x2(closure);

		AKA_mark("lis===316###sois===9619###eois===9669###lif===3###soif===134###eoif===184###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");struct afb_cred *cred = xreq->context.credentials;

		AKA_mark("lis===317###sois===9671###eois===9720###lif===4###soif===186###eoif===235###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");struct json_object *r = json_object_new_object();

		if (AKA_mark("lis===318###sois===9726###eois===9742###lif===5###soif===241###eoif===257###ifc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)") && ((AKA_mark("lis===318###sois===9726###eois===9730###lif===5###soif===241###eoif===245###isc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)")&&cred)	&&(AKA_mark("lis===318###sois===9734###eois===9742###lif===5###soif===249###eoif===257###isc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)")&&cred->id))) {
				AKA_mark("lis===319###sois===9748###eois===9813###lif===6###soif===263###eoif===328###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");json_object_object_add(r, "uid", json_object_new_int(cred->uid));

				AKA_mark("lis===320###sois===9816###eois===9881###lif===7###soif===331###eoif===396###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");json_object_object_add(r, "gid", json_object_new_int(cred->gid));

				AKA_mark("lis===321###sois===9884###eois===9949###lif===8###soif===399###eoif===464###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");json_object_object_add(r, "pid", json_object_new_int(cred->pid));

				AKA_mark("lis===322###sois===9952###eois===10022###lif===9###soif===467###eoif===537###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");json_object_object_add(r, "user", json_object_new_string(cred->user));

				AKA_mark("lis===323###sois===10025###eois===10097###lif===10###soif===540###eoif===612###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");json_object_object_add(r, "label", json_object_new_string(cred->label));

				AKA_mark("lis===324###sois===10100###eois===10166###lif===11###soif===615###eoif===681###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");json_object_object_add(r, "id", json_object_new_string(cred->id));

	}
	else {AKA_mark("lis===-318-###sois===-9726-###eois===-972616-###lif===-5-###soif===-###eoif===-257-###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");}

		if (AKA_mark("lis===326###sois===10175###eois===10196###lif===13###soif===690###eoif===711###ifc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)") && (AKA_mark("lis===326###sois===10175###eois===10196###lif===13###soif===690###eoif===711###isc===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)")&&xreq->context.session)) {
				AKA_mark("lis===327###sois===10202###eois===10298###lif===14###soif===717###eoif===813###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");json_object_object_add(r, "uuid", json_object_new_string(afb_context_uuid(&xreq->context)?:""));

				AKA_mark("lis===328###sois===10301###eois===10392###lif===15###soif===816###eoif===907###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");json_object_object_add(r, "LOA", json_object_new_int(afb_context_get_loa(&xreq->context)));

	}
	else {AKA_mark("lis===-326-###sois===-10175-###eois===-1017521-###lif===-13-###soif===-###eoif===-711-###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");}

		AKA_mark("lis===330###sois===10397###eois===10406###lif===17###soif===912###eoif===921###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_get_client_info_cb(struct afb_req_x2*)");return r;

}

/** Instrumented function xreq_subcall_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,int,void(*callback)(void*closure, struct json_object*object, const char*error, const char*info, struct afb_req_x2*req),void*) */
static void xreq_subcall_cb(
				struct afb_req_x2 *req,
				const char *api,
				const char *verb,
				struct json_object *args,
				int flags,
				void (*callback)(void *closure, struct json_object *object, const char *error, const char * info, struct afb_req_x2 *req),
				void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_subcall_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,int,void(*callback)(void*closure, struct json_object*object, const char*error, const char*info, struct afb_req_x2*req),void*)");AKA_fCall++;
		AKA_mark("lis===342###sois===10704###eois===10750###lif===9###soif===294###eoif===340###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_subcall_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,int,void(*callback)(void*closure, struct json_object*object, const char*error, const char*info, struct afb_req_x2*req),void*)");struct afb_xreq *xreq = xreq_from_req_x2(req);

		AKA_mark("lis===343###sois===10752###eois===10819###lif===10###soif===342###eoif===409###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_subcall_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,int,void(*callback)(void*closure, struct json_object*object, const char*error, const char*info, struct afb_req_x2*req),void*)");afb_calls_subcall(xreq, api, verb, args, flags, callback, closure);

}

/** Instrumented function xreq_subcallsync_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,int,struct json_object**,char**,char**) */
static int xreq_subcallsync_cb(
				struct afb_req_x2 *req,
				const char *api,
				const char *verb,
				struct json_object *args,
				int flags,
				struct json_object **object,
				char **error,
				char **info)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_subcallsync_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,int,struct json_object**,char**,char**)");AKA_fCall++;
		AKA_mark("lis===356###sois===11042###eois===11088###lif===10###soif===219###eoif===265###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_subcallsync_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,int,struct json_object**,char**,char**)");struct afb_xreq *xreq = xreq_from_req_x2(req);

		AKA_mark("lis===357###sois===11090###eois===11171###lif===11###soif===267###eoif===348###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_subcallsync_cb(struct afb_req_x2*,const char*,const char*,struct json_object*,int,struct json_object**,char**,char**)");return afb_calls_subcall_sync(xreq, api, verb, args, flags, object, error, info);

}

/******************************************************************************/

const struct afb_req_x2_itf xreq_itf = {
	.json = xreq_json_cb,
	.get = xreq_get_cb,
	.legacy_success = xreq_legacy_success_cb,
	.legacy_fail = xreq_legacy_fail_cb,
	.legacy_vsuccess = xreq_legacy_vsuccess_cb,
	.legacy_vfail = xreq_legacy_vfail_cb,
	.legacy_context_get = xreq_legacy_context_get_cb,
	.legacy_context_set = xreq_legacy_context_set_cb,
	.addref = xreq_addref_cb,
	.unref = xreq_unref_cb,
	.session_close = xreq_session_close_cb,
	.session_set_LOA = xreq_session_set_LOA_cb,
	.legacy_subscribe_event_x1 = xreq_legacy_subscribe_event_x1_cb,
	.legacy_unsubscribe_event_x1 = xreq_legacy_unsubscribe_event_x1_cb,
	.legacy_subcall = xreq_legacy_subcall_cb,
	.legacy_subcallsync = xreq_legacy_subcallsync_cb,
	.vverbose = xreq_vverbose_cb,
	.legacy_store_req = xreq_legacy_store_cb,
	.legacy_subcall_req = xreq_legacy_subcall_req_cb,
	.has_permission = xreq_has_permission_cb,
	.get_application_id = xreq_get_application_id_cb,
	.context_make = xreq_context_make_cb,
	.subscribe_event_x2 = xreq_subscribe_event_x2_cb,
	.unsubscribe_event_x2 = xreq_unsubscribe_event_x2_cb,
	.legacy_subcall_request = xreq_legacy_subcall_request_cb,
	.get_uid = xreq_get_uid_cb,
	.reply = xreq_reply_cb,
	.vreply = xreq_vreply_cb,
	.get_client_info = xreq_get_client_info_cb,
	.subcall = xreq_subcall_cb,
	.subcallsync = xreq_subcallsync_cb,
};
/******************************************************************************/
#if WITH_AFB_HOOK

static struct json_object *xreq_hooked_json_cb(struct afb_req_x2 *closure)
{
	struct json_object *r = xreq_json_cb(closure);
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	return afb_hook_xreq_json(xreq, r);
}

static struct afb_arg xreq_hooked_get_cb(struct afb_req_x2 *closure, const char *name)
{
	struct afb_arg r = xreq_get_cb(closure, name);
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	return afb_hook_xreq_get(xreq, name, r);
}

static void xreq_hooked_reply_cb(struct afb_req_x2 *closure, struct json_object *obj, const char *error, const char *info)
{
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	afb_hook_xreq_reply(xreq, obj, error, info);
	xreq_reply_cb(closure, obj, error, info);
}

static void xreq_hooked_vreply_cb(struct afb_req_x2 *closure, struct json_object *obj, const char *error, const char *fmt, va_list args)
{
	char *info;
	if (fmt == NULL || vasprintf(&info, fmt, args) < 0)
		info = NULL;
	xreq_hooked_reply_cb(closure, obj, error, info);
	free(info);
}

static void xreq_hooked_legacy_success_cb(struct afb_req_x2 *closure, struct json_object *obj, const char *info)
{
	xreq_hooked_reply_cb(closure, obj, NULL, info);
}

static void xreq_hooked_legacy_fail_cb(struct afb_req_x2 *closure, const char *status, const char *info)
{
	xreq_hooked_reply_cb(closure, NULL, status, info);
}

static void xreq_hooked_legacy_vsuccess_cb(struct afb_req_x2 *closure, struct json_object *obj, const char *fmt, va_list args)
{
	xreq_hooked_vreply_cb(closure, obj, NULL, fmt, args);
}

static void xreq_hooked_legacy_vfail_cb(struct afb_req_x2 *closure, const char *status, const char *fmt, va_list args)
{
	xreq_hooked_vreply_cb(closure, NULL, status, fmt, args);
}

static void *xreq_hooked_legacy_context_get_cb(struct afb_req_x2 *closure)
{
	void *r = xreq_legacy_context_get_cb(closure);
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	return afb_hook_xreq_legacy_context_get(xreq, r);
}

static void xreq_hooked_legacy_context_set_cb(struct afb_req_x2 *closure, void *value, void (*free_value)(void*))
{
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	afb_hook_xreq_legacy_context_set(xreq, value, free_value);
	xreq_legacy_context_set_cb(closure, value, free_value);
}

static struct afb_req_x2 *xreq_hooked_addref_cb(struct afb_req_x2 *closure)
{
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	afb_hook_xreq_addref(xreq);
	return xreq_addref_cb(closure);
}

static void xreq_hooked_unref_cb(struct afb_req_x2 *closure)
{
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	afb_hook_xreq_unref(xreq);
	xreq_unref_cb(closure);
}

static void xreq_hooked_session_close_cb(struct afb_req_x2 *closure)
{
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	afb_hook_xreq_session_close(xreq);
	xreq_session_close_cb(closure);
}

static int xreq_hooked_session_set_LOA_cb(struct afb_req_x2 *closure, unsigned level)
{
	int r = xreq_session_set_LOA_cb(closure, level);
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	return afb_hook_xreq_session_set_LOA(xreq, level, r);
}

static int xreq_hooked_subscribe_event_x2_cb(struct afb_req_x2 *closure, struct afb_event_x2 *event)
{
	int r = xreq_subscribe_event_x2_cb(closure, event);
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	return afb_hook_xreq_subscribe(xreq, event, r);
}

static int xreq_hooked_legacy_subscribe_event_x1_cb(struct afb_req_x2 *closure, struct afb_event_x1 event)
{
	return xreq_hooked_subscribe_event_x2_cb(closure, afb_event_x1_to_event_x2(event));
}

static int xreq_hooked_unsubscribe_event_x2_cb(struct afb_req_x2 *closure, struct afb_event_x2 *event)
{
	int r = xreq_unsubscribe_event_x2_cb(closure, event);
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	return afb_hook_xreq_unsubscribe(xreq, event, r);
}

static int xreq_hooked_legacy_unsubscribe_event_x1_cb(struct afb_req_x2 *closure, struct afb_event_x1 event)
{
	return xreq_hooked_unsubscribe_event_x2_cb(closure, afb_event_x1_to_event_x2(event));
}

static void xreq_hooked_legacy_subcall_cb(struct afb_req_x2 *req, const char *api, const char *verb, struct json_object *args, void (*callback)(void*, int, struct json_object*), void *closure)
{
	struct afb_xreq *xreq = xreq_from_req_x2(req);
	afb_calls_legacy_hooked_subcall_v1(xreq, api, verb, args, callback, closure);
}

static void xreq_hooked_legacy_subcall_req_cb(struct afb_req_x2 *req, const char *api, const char *verb, struct json_object *args, void (*callback)(void*, int, struct json_object*, struct afb_req_x1), void *closure)
{
	struct afb_xreq *xreq = xreq_from_req_x2(req);
	afb_calls_legacy_hooked_subcall_v2(xreq, api, verb, args, callback, closure);
}

static void xreq_hooked_legacy_subcall_request_cb(struct afb_req_x2 *req, const char *api, const char *verb, struct json_object *args, void (*callback)(void*, int, struct json_object*, struct afb_req_x2 *), void *closure)
{
	struct afb_xreq *xreq = xreq_from_req_x2(req);
	afb_calls_legacy_hooked_subcall_v3(xreq, api, verb, args, callback, closure);
}

static int xreq_hooked_legacy_subcallsync_cb(struct afb_req_x2 *req, const char *api, const char *verb, struct json_object *args, struct json_object **result)
{
	struct afb_xreq *xreq = xreq_from_req_x2(req);
	return afb_calls_legacy_hooked_subcall_sync(xreq, api, verb, args, result);
}

static void xreq_hooked_vverbose_cb(struct afb_req_x2 *closure, int level, const char *file, int line, const char *func, const char *fmt, va_list args)
{
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	va_list ap;
	va_copy(ap, args);
	xreq_vverbose_cb(closure, level, file, line, func, fmt, args);
	afb_hook_xreq_vverbose(xreq, level, file, line, func, fmt, ap);
	va_end(ap);
}

static struct afb_stored_req *xreq_hooked_legacy_store_cb(struct afb_req_x2 *closure)
{
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	struct afb_stored_req *r = xreq_legacy_store_cb(closure);
	afb_hook_xreq_legacy_store(xreq, r);
	return r;
}

static int xreq_hooked_has_permission_cb(struct afb_req_x2 *closure, const char *permission)
{
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	int r = xreq_has_permission_cb(closure, permission);
	return afb_hook_xreq_has_permission(xreq, permission, r);
}

static char *xreq_hooked_get_application_id_cb(struct afb_req_x2 *closure)
{
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	char *r = xreq_get_application_id_cb(closure);
	return afb_hook_xreq_get_application_id(xreq, r);
}

static void *xreq_hooked_context_make_cb(struct afb_req_x2 *closure, int replace, void *(*create_value)(void*), void (*free_value)(void*), void *create_closure)
{
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	void *result = xreq_context_make_cb(closure, replace, create_value, free_value, create_closure);
	return afb_hook_xreq_context_make(xreq, replace, create_value, free_value, create_closure, result);
}

static int xreq_hooked_get_uid_cb(struct afb_req_x2 *closure)
{
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	int r = xreq_get_uid_cb(closure);
	return afb_hook_xreq_get_uid(xreq, r);
}

static struct json_object *xreq_hooked_get_client_info_cb(struct afb_req_x2 *closure)
{
	struct afb_xreq *xreq = xreq_from_req_x2(closure);
	struct json_object *r = xreq_get_client_info_cb(closure);
	return afb_hook_xreq_get_client_info(xreq, r);
}

static void xreq_hooked_subcall_cb(
				struct afb_req_x2 *req,
				const char *api,
				const char *verb,
				struct json_object *args,
				int flags,
				void (*callback)(void *closure, struct json_object *object, const char *error, const char * info, struct afb_req_x2 *req),
				void *closure)
{
	struct afb_xreq *xreq = xreq_from_req_x2(req);
	afb_calls_hooked_subcall(xreq, api, verb, args, flags, callback, closure);
}

static int xreq_hooked_subcallsync_cb(
				struct afb_req_x2 *req,
				const char *api,
				const char *verb,
				struct json_object *args,
				int flags,
				struct json_object **object,
				char **error,
				char **info)
{
	struct afb_xreq *xreq = xreq_from_req_x2(req);
	return afb_calls_hooked_subcall_sync(xreq, api, verb, args, flags, object, error, info);
}

/******************************************************************************/

const struct afb_req_x2_itf xreq_hooked_itf = {
	.json = xreq_hooked_json_cb,
	.get = xreq_hooked_get_cb,
	.legacy_success = xreq_hooked_legacy_success_cb,
	.legacy_fail = xreq_hooked_legacy_fail_cb,
	.legacy_vsuccess = xreq_hooked_legacy_vsuccess_cb,
	.legacy_vfail = xreq_hooked_legacy_vfail_cb,
	.legacy_context_get = xreq_hooked_legacy_context_get_cb,
	.legacy_context_set = xreq_hooked_legacy_context_set_cb,
	.addref = xreq_hooked_addref_cb,
	.unref = xreq_hooked_unref_cb,
	.session_close = xreq_hooked_session_close_cb,
	.session_set_LOA = xreq_hooked_session_set_LOA_cb,
	.legacy_subscribe_event_x1 = xreq_hooked_legacy_subscribe_event_x1_cb,
	.legacy_unsubscribe_event_x1 = xreq_hooked_legacy_unsubscribe_event_x1_cb,
	.legacy_subcall = xreq_hooked_legacy_subcall_cb,
	.legacy_subcallsync = xreq_hooked_legacy_subcallsync_cb,
	.vverbose = xreq_hooked_vverbose_cb,
	.legacy_store_req = xreq_hooked_legacy_store_cb,
	.legacy_subcall_req = xreq_hooked_legacy_subcall_req_cb,
	.has_permission = xreq_hooked_has_permission_cb,
	.get_application_id = xreq_hooked_get_application_id_cb,
	.context_make = xreq_hooked_context_make_cb,
	.subscribe_event_x2 = xreq_hooked_subscribe_event_x2_cb,
	.unsubscribe_event_x2 = xreq_hooked_unsubscribe_event_x2_cb,
	.legacy_subcall_request = xreq_hooked_legacy_subcall_request_cb,
	.get_uid = xreq_hooked_get_uid_cb,
	.reply = xreq_hooked_reply_cb,
	.vreply = xreq_hooked_vreply_cb,
	.get_client_info = xreq_hooked_get_client_info_cb,
	.subcall = xreq_hooked_subcall_cb,
	.subcallsync = xreq_hooked_subcallsync_cb,
};
#endif

/******************************************************************************/

/** Instrumented function afb_xreq_unstore(struct afb_stored_req*) */
struct afb_req_x1 afb_xreq_unstore(struct afb_stored_req *sreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_unstore(struct afb_stored_req*)");AKA_fCall++;
		AKA_mark("lis===659###sois===22545###eois===22593###lif===2###soif===67###eoif===115###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unstore(struct afb_stored_req*)");struct afb_xreq *xreq = (struct afb_xreq *)sreq;

#if WITH_AFB_HOOK
	if (xreq->hookflags)
		afb_hook_xreq_legacy_unstore(xreq);
#endif
		AKA_mark("lis===664###sois===22680###eois===22708###lif===7###soif===202###eoif===230###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unstore(struct afb_stored_req*)");return xreq_to_req_x1(xreq);

}

/** Instrumented function afb_xreq_json(struct afb_xreq*) */
struct json_object *afb_xreq_json(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_json(struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===669###sois===22772###eois===22817###lif===2###soif===60###eoif===105###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_json(struct afb_xreq*)");return afb_req_x2_json(xreq_to_req_x2(xreq));

}

/** Instrumented function afb_xreq_reply(struct afb_xreq*,struct json_object*,const char*,const char*) */
void afb_xreq_reply(struct afb_xreq *xreq, struct json_object *obj, const char *error, const char *info)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===674###sois===22929###eois===22986###lif===2###soif===108###eoif===165###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");afb_req_x2_reply(xreq_to_req_x2(xreq), obj, error, info);

}

/** Instrumented function afb_xreq_reply_f(struct afb_xreq*,struct json_object*,const char*,const char*) */
void afb_xreq_reply_f(struct afb_xreq *xreq, struct json_object *obj, const char *error, const char *info, ...)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_f(struct afb_xreq*,struct json_object*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===679###sois===23105###eois===23118###lif===2###soif===115###eoif===128###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_f(struct afb_xreq*,struct json_object*,const char*,const char*)");va_list args;


		AKA_mark("lis===681###sois===23121###eois===23142###lif===4###soif===131###eoif===152###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_f(struct afb_xreq*,struct json_object*,const char*,const char*)");va_start(args, info);

		AKA_mark("lis===682###sois===23144###eois===23209###lif===5###soif===154###eoif===219###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_f(struct afb_xreq*,struct json_object*,const char*,const char*)");afb_req_x2_reply_v(xreq_to_req_x2(xreq), obj, error, info, args);

		AKA_mark("lis===683###sois===23211###eois===23224###lif===6###soif===221###eoif===234###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_f(struct afb_xreq*,struct json_object*,const char*,const char*)");va_end(args);

}

/** Instrumented function afb_xreq_raw(struct afb_xreq*,size_t*) */
const char *afb_xreq_raw(struct afb_xreq *xreq, size_t *size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_raw(struct afb_xreq*,size_t*)");AKA_fCall++;
		AKA_mark("lis===688###sois===23293###eois===23354###lif===2###soif===65###eoif===126###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_raw(struct afb_xreq*,size_t*)");struct json_object *obj = xreq_json_cb(xreq_to_req_x2(xreq));

		AKA_mark("lis===689###sois===23356###eois===23445###lif===3###soif===128###eoif===217###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_raw(struct afb_xreq*,size_t*)");const char *result = json_object_to_json_string_ext(obj, JSON_C_TO_STRING_NOSLASHESCAPE);

		if (AKA_mark("lis===690###sois===23451###eois===23463###lif===4###soif===223###eoif===235###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_raw(struct afb_xreq*,size_t*)") && (AKA_mark("lis===690###sois===23451###eois===23463###lif===4###soif===223###eoif===235###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_raw(struct afb_xreq*,size_t*)")&&size != NULL)) {
		AKA_mark("lis===691###sois===23467###eois===23490###lif===5###soif===239###eoif===262###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_raw(struct afb_xreq*,size_t*)");*size = strlen(result);
	}
	else {AKA_mark("lis===-690-###sois===-23451-###eois===-2345112-###lif===-4-###soif===-###eoif===-235-###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_raw(struct afb_xreq*,size_t*)");}

		AKA_mark("lis===692###sois===23492###eois===23506###lif===6###soif===264###eoif===278###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_raw(struct afb_xreq*,size_t*)");return result;

}

/** Instrumented function afb_xreq_unhooked_legacy_subcall(struct afb_xreq*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x2*),void*) */
void afb_xreq_unhooked_legacy_subcall(struct afb_xreq *xreq, const char *api, const char *verb, struct json_object *args, void (*callback)(void*, int, struct json_object*, struct afb_req_x2 *), void *cb_closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_legacy_subcall(struct afb_xreq*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x2*),void*)");AKA_fCall++;
		AKA_mark("lis===697###sois===23725###eois===23817###lif===2###soif===215###eoif===307###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_legacy_subcall(struct afb_xreq*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x2*),void*)");xreq_legacy_subcall_request_cb(xreq_to_req_x2(xreq), api, verb, args, callback, cb_closure);

}

/** Instrumented function afb_xreq_unhooked_subcall(struct afb_xreq*,const char*,const char*,struct json_object*,int,void(*callback)(void*, struct json_object*, const char*, const char*, struct afb_req_x2*),void*) */
void afb_xreq_unhooked_subcall(struct afb_xreq *xreq, const char *api, const char *verb, struct json_object *args, int flags, void (*callback)(void*, struct json_object*, const char*, const char*, struct afb_req_x2 *), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_subcall(struct afb_xreq*,const char*,const char*,struct json_object*,int,void(*callback)(void*, struct json_object*, const char*, const char*, struct afb_req_x2*),void*)");AKA_fCall++;
		AKA_mark("lis===702###sois===24058###eois===24139###lif===2###soif===237###eoif===318###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_subcall(struct afb_xreq*,const char*,const char*,struct json_object*,int,void(*callback)(void*, struct json_object*, const char*, const char*, struct afb_req_x2*),void*)");xreq_subcall_cb(xreq_to_req_x2(xreq), api, verb, args, flags, callback, closure);

}

/** Instrumented function afb_xreq_unhooked_legacy_subcall_sync(struct afb_xreq*,const char*,const char*,struct json_object*,struct json_object**) */
int afb_xreq_unhooked_legacy_subcall_sync(struct afb_xreq *xreq, const char *api, const char *verb, struct json_object *args, struct json_object **result)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_legacy_subcall_sync(struct afb_xreq*,const char*,const char*,struct json_object*,struct json_object**)");AKA_fCall++;
		AKA_mark("lis===707###sois===24301###eois===24382###lif===2###soif===158###eoif===239###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unhooked_legacy_subcall_sync(struct afb_xreq*,const char*,const char*,struct json_object*,struct json_object**)");return xreq_legacy_subcallsync_cb(xreq_to_req_x2(xreq), api, verb, args, result);

}

/** Instrumented function afb_xreq_addref(struct afb_xreq*) */
void afb_xreq_addref(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_addref(struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===712###sois===24433###eois===24473###lif===2###soif===47###eoif===87###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_addref(struct afb_xreq*)");afb_req_x2_addref(xreq_to_req_x2(xreq));

}

/** Instrumented function afb_xreq_unref(struct afb_xreq*) */
void afb_xreq_unref(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_unref(struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===717###sois===24523###eois===24562###lif===2###soif===46###eoif===85###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_unref(struct afb_xreq*)");afb_req_x2_unref(xreq_to_req_x2(xreq));

}

/** Instrumented function afb_xreq_legacy_subcall(struct afb_xreq*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x2*),void*) */
void afb_xreq_legacy_subcall(struct afb_xreq *xreq, const char *api, const char *verb, struct json_object *args, void (*callback)(void*, int, struct json_object*, struct afb_req_x2 *), void *cb_closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_legacy_subcall(struct afb_xreq*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x2*),void*)");AKA_fCall++;
		AKA_mark("lis===722###sois===24772###eois===24859###lif===2###soif===206###eoif===293###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_legacy_subcall(struct afb_xreq*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_req_x2*),void*)");afb_req_x2_subcall_legacy(xreq_to_req_x2(xreq), api, verb, args, callback, cb_closure);

}

/** Instrumented function afb_xreq_subcall(struct afb_xreq*,const char*,const char*,struct json_object*,int,void(*callback)(void*, struct json_object*, const char*, const char*, struct afb_req_x2*),void*) */
void afb_xreq_subcall(struct afb_xreq *xreq, const char *api, const char *verb, struct json_object *args, int flags, void (*callback)(void*, struct json_object*, const char*, const char*, struct afb_req_x2 *), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_subcall(struct afb_xreq*,const char*,const char*,struct json_object*,int,void(*callback)(void*, struct json_object*, const char*, const char*, struct afb_req_x2*),void*)");AKA_fCall++;
		AKA_mark("lis===727###sois===25091###eois===25175###lif===2###soif===228###eoif===312###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_subcall(struct afb_xreq*,const char*,const char*,struct json_object*,int,void(*callback)(void*, struct json_object*, const char*, const char*, struct afb_req_x2*),void*)");afb_req_x2_subcall(xreq_to_req_x2(xreq), api, verb, args, flags, callback, closure);

}

/** Instrumented function afb_xreq_legacy_subcall_sync(struct afb_xreq*,const char*,const char*,struct json_object*,struct json_object**) */
int afb_xreq_legacy_subcall_sync(struct afb_xreq *xreq, const char *api, const char *verb, struct json_object *args, struct json_object **result)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_legacy_subcall_sync(struct afb_xreq*,const char*,const char*,struct json_object*,struct json_object**)");AKA_fCall++;
		AKA_mark("lis===732###sois===25328###eois===25413###lif===2###soif===149###eoif===234###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_legacy_subcall_sync(struct afb_xreq*,const char*,const char*,struct json_object*,struct json_object**)");return afb_req_x2_subcall_sync_legacy(xreq_to_req_x2(xreq), api, verb, args, result);

}

/** Instrumented function afb_xreq_reply_unknown_api(struct afb_xreq*) */
int afb_xreq_reply_unknown_api(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_unknown_api(struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===737###sois===25474###eois===25618###lif===2###soif===57###eoif===201###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_unknown_api(struct afb_xreq*)");afb_xreq_reply_f(xreq, NULL, afb_error_text_unknown_api, "api %s not found (for verb %s)", xreq->request.called_api, xreq->request.called_verb);

		AKA_mark("lis===738###sois===25620###eois===25635###lif===3###soif===203###eoif===218###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_unknown_api(struct afb_xreq*)");errno = EINVAL;

		AKA_mark("lis===739###sois===25637###eois===25647###lif===4###soif===220###eoif===230###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_unknown_api(struct afb_xreq*)");return -1;

}

/** Instrumented function afb_xreq_reply_unknown_verb(struct afb_xreq*) */
int afb_xreq_reply_unknown_verb(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_unknown_verb(struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===744###sois===25709###eois===25853###lif===2###soif===58###eoif===202###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_unknown_verb(struct afb_xreq*)");afb_xreq_reply_f(xreq, NULL, afb_error_text_unknown_verb, "verb %s unknown within api %s", xreq->request.called_verb, xreq->request.called_api);

		AKA_mark("lis===745###sois===25855###eois===25870###lif===3###soif===204###eoif===219###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_unknown_verb(struct afb_xreq*)");errno = EINVAL;

		AKA_mark("lis===746###sois===25872###eois===25882###lif===4###soif===221###eoif===231###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_unknown_verb(struct afb_xreq*)");return -1;

}

/** Instrumented function afb_xreq_reply_invalid_token(struct afb_xreq*) */
int afb_xreq_reply_invalid_token(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_invalid_token(struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===751###sois===25945###eois===26019###lif===2###soif===59###eoif===133###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_invalid_token(struct afb_xreq*)");afb_xreq_reply(xreq, NULL, afb_error_text_invalid_token, "invalid token");
 /* TODO: or "no token" */
		AKA_mark("lis===752###sois===26047###eois===26062###lif===3###soif===161###eoif===176###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_invalid_token(struct afb_xreq*)");errno = EINVAL;

		AKA_mark("lis===753###sois===26064###eois===26074###lif===4###soif===178###eoif===188###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_invalid_token(struct afb_xreq*)");return -1;

}

/** Instrumented function afb_xreq_reply_insufficient_scope(struct afb_xreq*,const char*) */
int afb_xreq_reply_insufficient_scope(struct afb_xreq *xreq, const char *scope)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_insufficient_scope(struct afb_xreq*,const char*)");AKA_fCall++;
		AKA_mark("lis===758###sois===26161###eois===26254###lif===2###soif===83###eoif===176###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_insufficient_scope(struct afb_xreq*,const char*)");afb_xreq_reply(xreq, NULL, afb_error_text_insufficient_scope, scope ?: "insufficient scope");

		AKA_mark("lis===759###sois===26256###eois===26270###lif===3###soif===178###eoif===192###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_insufficient_scope(struct afb_xreq*,const char*)");errno = EPERM;

		AKA_mark("lis===760###sois===26272###eois===26282###lif===4###soif===194###eoif===204###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_reply_insufficient_scope(struct afb_xreq*,const char*)");return -1;

}

#if WITH_LEGACY_BINDING_V1
void afb_xreq_call_verb_v1(struct afb_xreq *xreq, const struct afb_verb_desc_v1 *verb)
{
	if (!verb)
		afb_xreq_reply_unknown_verb(xreq);
	else
		if (afb_auth_check_and_set_session_x1(xreq, verb->session) >= 0)
			verb->callback(xreq_to_req_x1(xreq));
}
#endif

#if WITH_LEGACY_BINDING_V2
void afb_xreq_call_verb_v2(struct afb_xreq *xreq, const struct afb_verb_v2 *verb)
{
	if (!verb)
		afb_xreq_reply_unknown_verb(xreq);
	else
		if (afb_auth_check_and_set_session_x2(xreq, verb->auth, verb->session) > 0)
			verb->callback(xreq_to_req_x1(xreq));
}
#endif

/** Instrumented function afb_xreq_call_verb_v3(struct afb_xreq*,const struct afb_verb_v3*) */
void afb_xreq_call_verb_v3(struct afb_xreq *xreq, const struct afb_verb_v3 *verb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_call_verb_v3(struct afb_xreq*,const struct afb_verb_v3*)");AKA_fCall++;
		if (AKA_mark("lis===787###sois===26959###eois===26964###lif===2###soif===89###eoif===94###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_call_verb_v3(struct afb_xreq*,const struct afb_verb_v3*)") && (AKA_mark("lis===787###sois===26959###eois===26964###lif===2###soif===89###eoif===94###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_call_verb_v3(struct afb_xreq*,const struct afb_verb_v3*)")&&!verb)) {
		AKA_mark("lis===788###sois===26968###eois===27002###lif===3###soif===98###eoif===132###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_call_verb_v3(struct afb_xreq*,const struct afb_verb_v3*)");afb_xreq_reply_unknown_verb(xreq);
	}
	else {
		if (AKA_mark("lis===790###sois===27015###eois===27085###lif===5###soif===145###eoif===215###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_call_verb_v3(struct afb_xreq*,const struct afb_verb_v3*)") && (AKA_mark("lis===790###sois===27015###eois===27085###lif===5###soif===145###eoif===215###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_call_verb_v3(struct afb_xreq*,const struct afb_verb_v3*)")&&afb_auth_check_and_set_session_x2(xreq, verb->auth, verb->session) > 0)) {
			AKA_mark("lis===791###sois===27090###eois===27127###lif===6###soif===220###eoif===257###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_call_verb_v3(struct afb_xreq*,const struct afb_verb_v3*)");verb->callback(xreq_to_req_x2(xreq));
		}
		else {AKA_mark("lis===-790-###sois===-27015-###eois===-2701570-###lif===-5-###soif===-###eoif===-215-###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_call_verb_v3(struct afb_xreq*,const struct afb_verb_v3*)");}
	}

}

/** Instrumented function afb_xreq_init(struct afb_xreq*,const struct afb_xreq_query_itf*) */
void afb_xreq_init(struct afb_xreq *xreq, const struct afb_xreq_query_itf *queryitf)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_init(struct afb_xreq*,const struct afb_xreq_query_itf*)");AKA_fCall++;
		AKA_mark("lis===796###sois===27219###eois===27249###lif===2###soif===88###eoif===118###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_init(struct afb_xreq*,const struct afb_xreq_query_itf*)");memset(xreq, 0, sizeof *xreq);

		AKA_mark("lis===797###sois===27251###eois===27281###lif===3###soif===120###eoif===150###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_init(struct afb_xreq*,const struct afb_xreq_query_itf*)");xreq->request.itf = &xreq_itf;
 /* no hook by default */
		AKA_mark("lis===798###sois===27308###eois===27327###lif===4###soif===177###eoif===196###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_init(struct afb_xreq*,const struct afb_xreq_query_itf*)");xreq->refcount = 1;

		AKA_mark("lis===799###sois===27329###eois===27355###lif===5###soif===198###eoif===224###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_init(struct afb_xreq*,const struct afb_xreq_query_itf*)");xreq->queryitf = queryitf;

}

#if WITH_AFB_HOOK
static void init_hooking(struct afb_xreq *xreq)
{
	afb_hook_init_xreq(xreq);
	if (xreq->hookflags) {
		xreq->request.itf = &xreq_hooked_itf; /* unhook the interface */
		afb_hook_xreq_begin(xreq);
	}
}
#endif

/**
 * job callback for asynchronous and secured processing of the request.
 */
/** Instrumented function process_async(int,void*) */
static void process_async(int signum, void *arg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/process_async(int,void*)");AKA_fCall++;
		AKA_mark("lis===818###sois===27719###eois===27747###lif===2###soif===52###eoif===80###ins===true###function===./app-framework-binder/src/afb-xreq.c/process_async(int,void*)");struct afb_xreq *xreq = arg;

		AKA_mark("lis===819###sois===27749###eois===27780###lif===3###soif===82###eoif===113###ins===true###function===./app-framework-binder/src/afb-xreq.c/process_async(int,void*)");const struct afb_api_item *api;


		if (AKA_mark("lis===821###sois===27787###eois===27798###lif===5###soif===120###eoif===131###ifc===true###function===./app-framework-binder/src/afb-xreq.c/process_async(int,void*)") && (AKA_mark("lis===821###sois===27787###eois===27798###lif===5###soif===120###eoif===131###isc===true###function===./app-framework-binder/src/afb-xreq.c/process_async(int,void*)")&&signum != 0)) {
		/* emit the error (assumes that hooking is initialised) */
				AKA_mark("lis===823###sois===27865###eois===27969###lif===7###soif===198###eoif===302###ins===true###function===./app-framework-binder/src/afb-xreq.c/process_async(int,void*)");afb_xreq_reply_f(xreq, NULL, afb_error_text_aborted, "signal %s(%d) caught", strsignal(signum), signum);

	}
	else {
#if WITH_AFB_HOOK
		/* init hooking */
		init_hooking(xreq);
#endif
		/* invoke api call method to process the request */
				AKA_mark("lis===830###sois===28104###eois===28160###lif===14###soif===437###eoif===493###ins===true###function===./app-framework-binder/src/afb-xreq.c/process_async(int,void*)");api = (const struct afb_api_item*)xreq->context.api_key;

				AKA_mark("lis===831###sois===28163###eois===28198###lif===15###soif===496###eoif===531###ins===true###function===./app-framework-binder/src/afb-xreq.c/process_async(int,void*)");api->itf->call(api->closure, xreq);

	}

	/* release the request */
		AKA_mark("lis===834###sois===28230###eois===28260###lif===18###soif===563###eoif===593###ins===true###function===./app-framework-binder/src/afb-xreq.c/process_async(int,void*)");afb_xreq_unhooked_unref(xreq);

}

/**
 * Early request failure of the request 'xreq' with, as usual, 'status' and 'info'
 * The early failure occurs only in function 'afb_xreq_process' where normally,
 * the hooking is not initialised. So this "early" failure takes care to initialise
 * the hooking in first.
 */
/** Instrumented function early_failure(struct afb_xreq*,const char*,const char*) */
static void early_failure(struct afb_xreq *xreq, const char *status, const char *info, ...)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/early_failure(struct afb_xreq*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===845###sois===28639###eois===28652###lif===2###soif===95###eoif===108###ins===true###function===./app-framework-binder/src/afb-xreq.c/early_failure(struct afb_xreq*,const char*,const char*)");va_list args;


#if WITH_AFB_HOOK
	/* init hooking */
	init_hooking(xreq);
#endif

	/* send error */
		AKA_mark("lis===853###sois===28740###eois===28761###lif===10###soif===196###eoif===217###ins===true###function===./app-framework-binder/src/afb-xreq.c/early_failure(struct afb_xreq*,const char*,const char*)");va_start(args, info);

		AKA_mark("lis===854###sois===28763###eois===28830###lif===11###soif===219###eoif===286###ins===true###function===./app-framework-binder/src/afb-xreq.c/early_failure(struct afb_xreq*,const char*,const char*)");afb_req_x2_reply_v(xreq_to_req_x2(xreq), NULL, status, info, args);

		AKA_mark("lis===855###sois===28832###eois===28845###lif===12###soif===288###eoif===301###ins===true###function===./app-framework-binder/src/afb-xreq.c/early_failure(struct afb_xreq*,const char*,const char*)");va_end(args);

}

/**
 * Enqueue a job for processing the request 'xreq' using the given 'apiset'.
 * Errors are reported as request failures.
 */
/** Instrumented function afb_xreq_process(struct afb_xreq*,struct afb_apiset*) */
void afb_xreq_process(struct afb_xreq *xreq, struct afb_apiset *apiset)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===864###sois===29053###eois===29084###lif===2###soif===75###eoif===106###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");const struct afb_api_item *api;

		AKA_mark("lis===865###sois===29086###eois===29110###lif===3###soif===108###eoif===132###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");struct afb_xreq *caller;


	/* lookup at the api */
		AKA_mark("lis===868###sois===29138###eois===29160###lif===6###soif===160###eoif===182###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");xreq->apiset = apiset;

		AKA_mark("lis===869###sois===29162###eois===29231###lif===7###soif===184###eoif===253###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");api = afb_apiset_lookup_started(apiset, xreq->request.called_api, 1);

		if (AKA_mark("lis===870###sois===29237###eois===29241###lif===8###soif===259###eoif===263###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)") && (AKA_mark("lis===870###sois===29237###eois===29241###lif===8###soif===259###eoif===263###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)")&&!api)) {
				if (AKA_mark("lis===871###sois===29251###eois===29266###lif===9###soif===273###eoif===288###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)") && (AKA_mark("lis===871###sois===29251###eois===29266###lif===9###soif===273###eoif===288###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)")&&errno == ENOENT)) {
			AKA_mark("lis===872###sois===29271###eois===29393###lif===10###soif===293###eoif===415###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");early_failure(xreq, "unknown-api", "api %s not found (for verb %s)", xreq->request.called_api, xreq->request.called_verb);
		}
		else {
			AKA_mark("lis===874###sois===29404###eois===29503###lif===12###soif===426###eoif===525###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");early_failure(xreq, "bad-api-state", "api %s not started correctly: %m", xreq->request.called_api);
		}

				AKA_mark("lis===875###sois===29506###eois===29515###lif===13###soif===528###eoif===537###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");goto end;

	}
	else {AKA_mark("lis===-870-###sois===-29237-###eois===-292374-###lif===-8-###soif===-###eoif===-263-###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");}

		AKA_mark("lis===877###sois===29520###eois===29548###lif===15###soif===542###eoif===570###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");xreq->context.api_key = api;


	/* check self locking */
		if (AKA_mark("lis===880###sois===29581###eois===29591###lif===18###soif===603###eoif===613###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)") && (AKA_mark("lis===880###sois===29581###eois===29591###lif===18###soif===603###eoif===613###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)")&&api->group)) {
				AKA_mark("lis===881###sois===29597###eois===29619###lif===19###soif===619###eoif===641###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");caller = xreq->caller;

				while (AKA_mark("lis===882###sois===29629###eois===29635###lif===20###soif===651###eoif===657###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)") && (AKA_mark("lis===882###sois===29629###eois===29635###lif===20###soif===651###eoif===657###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)")&&caller)) {
						if (AKA_mark("lis===883###sois===29646###eois===29720###lif===21###soif===668###eoif===742###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)") && (AKA_mark("lis===883###sois===29646###eois===29720###lif===21###soif===668###eoif===742###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)")&&((const struct afb_api_item*)caller->context.api_key)->group == api->group)) {
				/* noconcurrency lock detected */
								AKA_mark("lis===885###sois===29766###eois===29845###lif===23###soif===788###eoif===867###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");ERROR("self-lock detected in call stack for API %s", xreq->request.called_api);

								AKA_mark("lis===886###sois===29850###eois===29942###lif===24###soif===872###eoif===964###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");early_failure(xreq, "self-locked", "recursive self lock, API %s", xreq->request.called_api);

								AKA_mark("lis===887###sois===29947###eois===29956###lif===25###soif===969###eoif===978###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");goto end;

			}
			else {AKA_mark("lis===-883-###sois===-29646-###eois===-2964674-###lif===-21-###soif===-###eoif===-742-###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");}

						AKA_mark("lis===889###sois===29965###eois===29989###lif===27###soif===987###eoif===1011###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");caller = caller->caller;

		}

	}
	else {AKA_mark("lis===-880-###sois===-29581-###eois===-2958110-###lif===-18-###soif===-###eoif===-613-###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");}


	/* queue the request job */
		AKA_mark("lis===894###sois===30028###eois===30059###lif===32###soif===1050###eoif===1081###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");afb_xreq_unhooked_addref(xreq);

		if (AKA_mark("lis===895###sois===30065###eois===30144###lif===33###soif===1087###eoif===1166###ifc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)") && (AKA_mark("lis===895###sois===30065###eois===30144###lif===33###soif===1087###eoif===1166###isc===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)")&&jobs_queue(api->group, afb_apiset_timeout_get(apiset), process_async, xreq) < 0)) {
		/* TODO: allows or not to proccess it directly as when no threading? (see above) */
				AKA_mark("lis===897###sois===30236###eois===30280###lif===35###soif===1258###eoif===1302###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");ERROR("can't process job with threads: %m");

				AKA_mark("lis===898###sois===30283###eois===30357###lif===36###soif===1305###eoif===1379###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");early_failure(xreq, "cancelled", "not able to create a job for the task");

				AKA_mark("lis===899###sois===30360###eois===30390###lif===37###soif===1382###eoif===1412###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");afb_xreq_unhooked_unref(xreq);

	}
	else {AKA_mark("lis===-895-###sois===-30065-###eois===-3006579-###lif===-33-###soif===-###eoif===-1166-###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");}

	end:
	AKA_mark("lis===902###sois===30400###eois===30430###lif===40###soif===1422###eoif===1452###ins===true###function===./app-framework-binder/src/afb-xreq.c/afb_xreq_process(struct afb_xreq*,struct afb_apiset*)");afb_xreq_unhooked_unref(xreq);

}

/** Instrumented function xreq_on_behalf_cred_export(struct afb_xreq*) */
const char *xreq_on_behalf_cred_export(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-xreq.c/xreq_on_behalf_cred_export(struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===907###sois===30499###eois===30551###lif===2###soif===65###eoif===117###ins===true###function===./app-framework-binder/src/afb-xreq.c/xreq_on_behalf_cred_export(struct afb_xreq*)");return afb_context_on_behalf_export(&xreq->context);

}


#endif

