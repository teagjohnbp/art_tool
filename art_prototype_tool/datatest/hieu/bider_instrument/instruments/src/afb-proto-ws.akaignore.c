/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_PROTO_WS_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_PROTO_WS_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <endian.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <pthread.h>

#include <json-c/json.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_WS_H_
#define AKA_INCLUDE__AFB_WS_H_
#include "afb-ws.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_MSG_JSON_H_
#define AKA_INCLUDE__AFB_MSG_JSON_H_
#include "afb-msg-json.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_PROTO_WS_H_
#define AKA_INCLUDE__AFB_PROTO_WS_H_
#include "afb-proto-ws.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_H_
#define AKA_INCLUDE__FDEV_H_
#include "fdev.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif


struct afb_proto_ws;

/******** implementation of internal binder protocol per api **************/
/*

This protocol is asymmetric: there is a client and a server

The client can require the following actions:

  - call a verb

  - ask for description

The server must reply to the previous actions by

  - answering success or failure of the call

  - answering the required description

The server can also within the context of a call

  - subscribe or unsubscribe an event

For the purpose of handling events the server can:

  - create/destroy an event

  - push or broadcast data as an event

  - signal unexpected event

*/
/************** constants for protocol definition *************************/

#define CHAR_FOR_CALL             'K'	/* client -> server */
#define CHAR_FOR_REPLY            'k'	/* server -> client */
#define CHAR_FOR_EVT_BROADCAST    'B'	/* server -> client */
#define CHAR_FOR_EVT_ADD          'E'	/* server -> client */
#define CHAR_FOR_EVT_DEL          'e'	/* server -> client */
#define CHAR_FOR_EVT_PUSH         'P'	/* server -> client */
#define CHAR_FOR_EVT_SUBSCRIBE    'X'	/* server -> client */
#define CHAR_FOR_EVT_UNSUBSCRIBE  'x'	/* server -> client */
#define CHAR_FOR_EVT_UNEXPECTED   'U'	/* client -> server */
#define CHAR_FOR_DESCRIBE         'D'	/* client -> server */
#define CHAR_FOR_DESCRIPTION      'd'	/* server -> client */
#define CHAR_FOR_TOKEN_ADD        'T'	/* client -> server */
#define CHAR_FOR_TOKEN_DROP       't'	/* client -> server */
#define CHAR_FOR_SESSION_ADD      'S'	/* client -> server */
#define CHAR_FOR_SESSION_DROP     's'	/* client -> server */
#define CHAR_FOR_VERSION_OFFER    'V'	/* client -> server */
#define CHAR_FOR_VERSION_SET      'v'	/* server -> client */

/******************* manage versions *****************************/

#define WSAPI_IDENTIFIER        02723012011  /* wsapi: 23.19.1.16.9 */

#define WSAPI_VERSION_UNSET	0
#define WSAPI_VERSION_1		1

#define WSAPI_VERSION_MIN	WSAPI_VERSION_1
#define WSAPI_VERSION_MAX	WSAPI_VERSION_1

/******************* maximum count of ids ***********************/

#define ACTIVE_ID_MAX		4095

/******************* handling calls *****************************/

/*
 * structure for recording calls on client side
 */
struct client_call {
	struct client_call *next;	/* the next call */
	void *request;			/* the request closure */
	uint16_t callid;		/* the message identifier */
};

/*
 * structure for a ws request
 */
struct afb_proto_ws_call {
	struct afb_proto_ws *protows;	/* the client of the request */
	char *buffer;			/* the incoming buffer */
	uint16_t refcount;		/* reference count */
	uint16_t callid;		/* the incoming request callid */
};

/*
 * structure for recording describe requests
 */
struct client_describe
{
	struct client_describe *next;
	void (*callback)(void*, struct json_object*);
	void *closure;
	uint16_t descid;
};

/*
 * structure for jobs of describing
 */
struct afb_proto_ws_describe
{
	struct afb_proto_ws *protows;
	uint16_t descid;
};

/******************* proto description for client or servers ******************/

struct afb_proto_ws
{
	/* count of references */
	uint16_t refcount;

	/* id generator */
	uint16_t genid;

	/* count actives ids */
	uint16_t idcount;

	/* version */
	uint8_t version;

	/* resource control */
	pthread_mutex_t mutex;

	/* websocket */
	struct afb_ws *ws;

	/* the client closure */
	void *closure;

	/* the client side interface */
	const struct afb_proto_ws_client_itf *client_itf;

	/* the server side interface */
	const struct afb_proto_ws_server_itf *server_itf;

	/* emitted calls (client side) */
	struct client_call *calls;

	/* pending description (client side) */
	struct client_describe *describes;

	/* on hangup callback */
	void (*on_hangup)(void *closure);

	/* queuing facility for processing messages */
	int (*queuing)(struct afb_proto_ws *proto, void (*process)(int s, void *c), void *closure);
};

/******************* streaming objects **********************************/

#define WRITEBUF_COUNT_MAX	32
#define WRITEBUF_BUFSZ		(WRITEBUF_COUNT_MAX * sizeof(uint32_t))

struct writebuf
{
	int iovcount, bufcount;
	struct iovec iovec[WRITEBUF_COUNT_MAX];
	char buf[WRITEBUF_BUFSZ];
};

struct readbuf
{
	char *base, *head, *end;
};

struct binary
{
	struct afb_proto_ws *protows;
	struct readbuf rb;
};

/******************* serialization part **********************************/

/** Instrumented function readbuf_get(struct readbuf*,uint32_t) */
static char *readbuf_get(struct readbuf *rb, uint32_t length)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/readbuf_get(struct readbuf*,uint32_t)");AKA_fCall++;
		AKA_mark("lis===219###sois===5517###eois===5541###lif===2###soif===65###eoif===89###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_get(struct readbuf*,uint32_t)");char *before = rb->head;

		AKA_mark("lis===220###sois===5543###eois===5573###lif===3###soif===91###eoif===121###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_get(struct readbuf*,uint32_t)");char *after = before + length;

		if (AKA_mark("lis===221###sois===5579###eois===5594###lif===4###soif===127###eoif===142###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_get(struct readbuf*,uint32_t)") && (AKA_mark("lis===221###sois===5579###eois===5594###lif===4###soif===127###eoif===142###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_get(struct readbuf*,uint32_t)")&&after > rb->end)) {
		AKA_mark("lis===222###sois===5598###eois===5607###lif===5###soif===146###eoif===155###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_get(struct readbuf*,uint32_t)");return 0;
	}
	else {AKA_mark("lis===-221-###sois===-5579-###eois===-557915-###lif===-4-###soif===-###eoif===-142-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_get(struct readbuf*,uint32_t)");}

		AKA_mark("lis===223###sois===5609###eois===5626###lif===6###soif===157###eoif===174###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_get(struct readbuf*,uint32_t)");rb->head = after;

		AKA_mark("lis===224###sois===5628###eois===5642###lif===7###soif===176###eoif===190###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_get(struct readbuf*,uint32_t)");return before;

}

/** Instrumented function readbuf_getat(struct readbuf*,void*,uint32_t) */
static int readbuf_getat(struct readbuf *rb, void *to, uint32_t length)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/readbuf_getat(struct readbuf*,void*,uint32_t)");AKA_fCall++;
		AKA_mark("lis===229###sois===5721###eois===5758###lif===2###soif===75###eoif===112###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_getat(struct readbuf*,void*,uint32_t)");char *head = readbuf_get(rb, length);

		if (AKA_mark("lis===230###sois===5764###eois===5769###lif===3###soif===118###eoif===123###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_getat(struct readbuf*,void*,uint32_t)") && (AKA_mark("lis===230###sois===5764###eois===5769###lif===3###soif===118###eoif===123###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_getat(struct readbuf*,void*,uint32_t)")&&!head)) {
		AKA_mark("lis===231###sois===5773###eois===5782###lif===4###soif===127###eoif===136###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_getat(struct readbuf*,void*,uint32_t)");return 0;
	}
	else {AKA_mark("lis===-230-###sois===-5764-###eois===-57645-###lif===-3-###soif===-###eoif===-123-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_getat(struct readbuf*,void*,uint32_t)");}

		AKA_mark("lis===232###sois===5784###eois===5809###lif===5###soif===138###eoif===163###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_getat(struct readbuf*,void*,uint32_t)");memcpy(to, head, length);

		AKA_mark("lis===233###sois===5811###eois===5820###lif===6###soif===165###eoif===174###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_getat(struct readbuf*,void*,uint32_t)");return 1;

}

/** Instrumented function readbuf_char(struct readbuf*,char*) */
__attribute__((unused))
static int readbuf_char(struct readbuf *rb, char *value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/readbuf_char(struct readbuf*,char*)");AKA_fCall++;
		AKA_mark("lis===239###sois===5908###eois===5955###lif===3###soif===84###eoif===131###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_char(struct readbuf*,char*)");return readbuf_getat(rb, value, sizeof *value);

}

/** Instrumented function readbuf_uint32(struct readbuf*,uint32_t*) */
static int readbuf_uint32(struct readbuf *rb, uint32_t *value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/readbuf_uint32(struct readbuf*,uint32_t*)");AKA_fCall++;
		AKA_mark("lis===244###sois===6025###eois===6073###lif===2###soif===66###eoif===114###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_uint32(struct readbuf*,uint32_t*)");int r = readbuf_getat(rb, value, sizeof *value);

		if (AKA_mark("lis===245###sois===6079###eois===6080###lif===3###soif===120###eoif===121###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_uint32(struct readbuf*,uint32_t*)") && (AKA_mark("lis===245###sois===6079###eois===6080###lif===3###soif===120###eoif===121###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_uint32(struct readbuf*,uint32_t*)")&&r)) {
		AKA_mark("lis===246###sois===6084###eois===6109###lif===4###soif===125###eoif===150###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_uint32(struct readbuf*,uint32_t*)");*value = le32toh(*value);
	}
	else {AKA_mark("lis===-245-###sois===-6079-###eois===-60791-###lif===-3-###soif===-###eoif===-121-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_uint32(struct readbuf*,uint32_t*)");}

		AKA_mark("lis===247###sois===6111###eois===6120###lif===5###soif===152###eoif===161###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_uint32(struct readbuf*,uint32_t*)");return r;

}

/** Instrumented function readbuf_uint16(struct readbuf*,uint16_t*) */
static int readbuf_uint16(struct readbuf *rb, uint16_t *value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/readbuf_uint16(struct readbuf*,uint16_t*)");AKA_fCall++;
		AKA_mark("lis===252###sois===6190###eois===6238###lif===2###soif===66###eoif===114###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_uint16(struct readbuf*,uint16_t*)");int r = readbuf_getat(rb, value, sizeof *value);

		if (AKA_mark("lis===253###sois===6244###eois===6245###lif===3###soif===120###eoif===121###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_uint16(struct readbuf*,uint16_t*)") && (AKA_mark("lis===253###sois===6244###eois===6245###lif===3###soif===120###eoif===121###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_uint16(struct readbuf*,uint16_t*)")&&r)) {
		AKA_mark("lis===254###sois===6249###eois===6274###lif===4###soif===125###eoif===150###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_uint16(struct readbuf*,uint16_t*)");*value = le16toh(*value);
	}
	else {AKA_mark("lis===-253-###sois===-6244-###eois===-62441-###lif===-3-###soif===-###eoif===-121-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_uint16(struct readbuf*,uint16_t*)");}

		AKA_mark("lis===255###sois===6276###eois===6285###lif===5###soif===152###eoif===161###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_uint16(struct readbuf*,uint16_t*)");return r;

}

/** Instrumented function readbuf_uint8(struct readbuf*,uint8_t*) */
static int readbuf_uint8(struct readbuf *rb, uint8_t *value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/readbuf_uint8(struct readbuf*,uint8_t*)");AKA_fCall++;
		AKA_mark("lis===260###sois===6353###eois===6400###lif===2###soif===64###eoif===111###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_uint8(struct readbuf*,uint8_t*)");return readbuf_getat(rb, value, sizeof *value);

}

/** Instrumented function _readbuf_string_(struct readbuf*,const char**,size_t*,int) */
static int _readbuf_string_(struct readbuf *rb, const char **value, size_t *length, int nulok)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)");AKA_fCall++;
		AKA_mark("lis===265###sois===6502###eois===6515###lif===2###soif===98###eoif===111###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)");uint32_t len;

		if (AKA_mark("lis===266###sois===6521###eois===6546###lif===3###soif===117###eoif===142###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)") && (AKA_mark("lis===266###sois===6521###eois===6546###lif===3###soif===117###eoif===142###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)")&&!readbuf_uint32(rb, &len))) {
		AKA_mark("lis===267###sois===6550###eois===6559###lif===4###soif===146###eoif===155###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)");return 0;
	}
	else {AKA_mark("lis===-266-###sois===-6521-###eois===-652125-###lif===-3-###soif===-###eoif===-142-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)");}

		if (AKA_mark("lis===268###sois===6565###eois===6569###lif===5###soif===161###eoif===165###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)") && (AKA_mark("lis===268###sois===6565###eois===6569###lif===5###soif===161###eoif===165###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)")&&!len)) {
				if (AKA_mark("lis===269###sois===6579###eois===6585###lif===6###soif===175###eoif===181###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)") && (AKA_mark("lis===269###sois===6579###eois===6585###lif===6###soif===175###eoif===181###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)")&&!nulok)) {
			AKA_mark("lis===270###sois===6590###eois===6599###lif===7###soif===186###eoif===195###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)");return 0;
		}
		else {AKA_mark("lis===-269-###sois===-6579-###eois===-65796-###lif===-6-###soif===-###eoif===-181-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)");}

				AKA_mark("lis===271###sois===6602###eois===6616###lif===8###soif===198###eoif===212###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)");*value = NULL;

				if (AKA_mark("lis===272###sois===6623###eois===6629###lif===9###soif===219###eoif===225###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)") && (AKA_mark("lis===272###sois===6623###eois===6629###lif===9###soif===219###eoif===225###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)")&&length)) {
			AKA_mark("lis===273###sois===6634###eois===6646###lif===10###soif===230###eoif===242###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)");*length = 0;
		}
		else {AKA_mark("lis===-272-###sois===-6623-###eois===-66236-###lif===-9-###soif===-###eoif===-225-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)");}

				AKA_mark("lis===274###sois===6649###eois===6658###lif===11###soif===245###eoif===254###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)");return 1;

	}
	else {AKA_mark("lis===-268-###sois===-6565-###eois===-65654-###lif===-5-###soif===-###eoif===-165-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)");}

		if (AKA_mark("lis===276###sois===6667###eois===6673###lif===13###soif===263###eoif===269###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)") && (AKA_mark("lis===276###sois===6667###eois===6673###lif===13###soif===263###eoif===269###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)")&&length)) {
		AKA_mark("lis===277###sois===6677###eois===6705###lif===14###soif===273###eoif===301###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)");*length = (size_t)(len - 1);
	}
	else {AKA_mark("lis===-276-###sois===-6667-###eois===-66676-###lif===-13-###soif===-###eoif===-269-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)");}

		AKA_mark("lis===278###sois===6707###eois===6776###lif===15###soif===303###eoif===372###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/_readbuf_string_(struct readbuf*,const char**,size_t*,int)");return (*value = readbuf_get(rb, len)) != NULL &&  rb->head[-1] == 0;

}


/** Instrumented function readbuf_string(struct readbuf*,const char**,size_t*) */
static int readbuf_string(struct readbuf *rb, const char **value, size_t *length)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/readbuf_string(struct readbuf*,const char**,size_t*)");AKA_fCall++;
		AKA_mark("lis===284###sois===6866###eois===6912###lif===2###soif===85###eoif===131###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_string(struct readbuf*,const char**,size_t*)");return _readbuf_string_(rb, value, length, 0);

}

/** Instrumented function readbuf_nullstring(struct readbuf*,const char**,size_t*) */
static int readbuf_nullstring(struct readbuf *rb, const char **value, size_t *length)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/readbuf_nullstring(struct readbuf*,const char**,size_t*)");AKA_fCall++;
		AKA_mark("lis===289###sois===7005###eois===7051###lif===2###soif===89###eoif===135###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_nullstring(struct readbuf*,const char**,size_t*)");return _readbuf_string_(rb, value, length, 1);

}

/** Instrumented function readbuf_object(struct readbuf*,struct json_object**) */
static int readbuf_object(struct readbuf *rb, struct json_object **object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)");AKA_fCall++;
		AKA_mark("lis===294###sois===7133###eois===7152###lif===2###soif===78###eoif===97###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)");const char *string;

		AKA_mark("lis===295###sois===7154###eois===7176###lif===3###soif===99###eoif===121###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)");struct json_object *o;

		AKA_mark("lis===296###sois===7178###eois===7207###lif===4###soif===123###eoif===152###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)");enum json_tokener_error jerr;

		AKA_mark("lis===297###sois===7209###eois===7252###lif===5###soif===154###eoif===197###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)");int rc = readbuf_string(rb, &string, NULL);

		if (AKA_mark("lis===298###sois===7258###eois===7260###lif===6###soif===203###eoif===205###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)") && (AKA_mark("lis===298###sois===7258###eois===7260###lif===6###soif===203###eoif===205###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)")&&rc)) {
				AKA_mark("lis===299###sois===7266###eois===7312###lif===7###soif===211###eoif===257###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)");o = json_tokener_parse_verbose(string, &jerr);

				if (AKA_mark("lis===300###sois===7319###eois===7347###lif===8###soif===264###eoif===292###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)") && (AKA_mark("lis===300###sois===7319###eois===7347###lif===8###soif===264###eoif===292###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)")&&jerr != json_tokener_success)) {
			AKA_mark("lis===301###sois===7352###eois===7387###lif===9###soif===297###eoif===332###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)");o = json_object_new_string(string);
		}
		else {AKA_mark("lis===-300-###sois===-7319-###eois===-731928-###lif===-8-###soif===-###eoif===-292-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)");}

				AKA_mark("lis===302###sois===7390###eois===7402###lif===10###soif===335###eoif===347###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)");*object = o;

	}
	else {AKA_mark("lis===-298-###sois===-7258-###eois===-72582-###lif===-6-###soif===-###eoif===-205-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)");}

		AKA_mark("lis===304###sois===7407###eois===7417###lif===12###soif===352###eoif===362###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/readbuf_object(struct readbuf*,struct json_object**)");return rc;

}

/** Instrumented function writebuf_put(struct writebuf*,const void*,size_t) */
static int writebuf_put(struct writebuf *wb, const void *value, size_t length)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/writebuf_put(struct writebuf*,const void*,size_t)");AKA_fCall++;
		AKA_mark("lis===309###sois===7503###eois===7524###lif===2###soif===82###eoif===103###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_put(struct writebuf*,const void*,size_t)");int i = wb->iovcount;

		if (AKA_mark("lis===310###sois===7530###eois===7553###lif===3###soif===109###eoif===132###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_put(struct writebuf*,const void*,size_t)") && (AKA_mark("lis===310###sois===7530###eois===7553###lif===3###soif===109###eoif===132###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_put(struct writebuf*,const void*,size_t)")&&i == WRITEBUF_COUNT_MAX)) {
		AKA_mark("lis===311###sois===7557###eois===7566###lif===4###soif===136###eoif===145###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_put(struct writebuf*,const void*,size_t)");return 0;
	}
	else {AKA_mark("lis===-310-###sois===-7530-###eois===-753023-###lif===-3-###soif===-###eoif===-132-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_put(struct writebuf*,const void*,size_t)");}

		AKA_mark("lis===312###sois===7568###eois===7605###lif===5###soif===147###eoif===184###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_put(struct writebuf*,const void*,size_t)");wb->iovec[i].iov_base = (void*)value;

		AKA_mark("lis===313###sois===7607###eois===7637###lif===6###soif===186###eoif===216###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_put(struct writebuf*,const void*,size_t)");wb->iovec[i].iov_len = length;

		AKA_mark("lis===314###sois===7639###eois===7660###lif===7###soif===218###eoif===239###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_put(struct writebuf*,const void*,size_t)");wb->iovcount = i + 1;

		AKA_mark("lis===315###sois===7662###eois===7671###lif===8###soif===241###eoif===250###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_put(struct writebuf*,const void*,size_t)");return 1;

}

/** Instrumented function writebuf_putbuf(struct writebuf*,const void*,int) */
static int writebuf_putbuf(struct writebuf *wb, const void *value, int length)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");AKA_fCall++;
		AKA_mark("lis===320###sois===7757###eois===7765###lif===2###soif===82###eoif===90###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");char *p;

		AKA_mark("lis===321###sois===7767###eois===7814###lif===3###soif===92###eoif===139###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");int i = wb->iovcount, n = wb->bufcount, nafter;


	/* check enough length */
		AKA_mark("lis===324###sois===7844###eois===7864###lif===6###soif===169###eoif===189###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");nafter = n + length;

		if (AKA_mark("lis===325###sois===7870###eois===7893###lif===7###soif===195###eoif===218###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)") && (AKA_mark("lis===325###sois===7870###eois===7893###lif===7###soif===195###eoif===218###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)")&&nafter > WRITEBUF_BUFSZ)) {
		AKA_mark("lis===326###sois===7897###eois===7906###lif===8###soif===222###eoif===231###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");return 0;
	}
	else {AKA_mark("lis===-325-###sois===-7870-###eois===-787023-###lif===-7-###soif===-###eoif===-218-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");}


	/* get where to store */
		AKA_mark("lis===329###sois===7935###eois===7951###lif===11###soif===260###eoif===276###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");p = &wb->buf[n];

		if (AKA_mark("lis===330###sois===7957###eois===8030###lif===12###soif===282###eoif===355###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)") && ((AKA_mark("lis===330###sois===7957###eois===7958###lif===12###soif===282###eoif===283###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)")&&i)	&&(AKA_mark("lis===330###sois===7962###eois===8030###lif===12###soif===287###eoif===355###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)")&&p == (((char*)wb->iovec[i - 1].iov_base) + wb->iovec[i - 1].iov_len)))) {
		AKA_mark("lis===332###sois===8066###eois===8109###lif===14###soif===391###eoif===434###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");wb->iovec[i - 1].iov_len += (size_t)length;
	}
	else {
		if (AKA_mark("lis===333###sois===8120###eois===8143###lif===15###soif===445###eoif===468###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)") && (AKA_mark("lis===333###sois===8120###eois===8143###lif===15###soif===445###eoif===468###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)")&&i == WRITEBUF_COUNT_MAX)) {
			AKA_mark("lis===335###sois===8169###eois===8178###lif===17###soif===494###eoif===503###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");return 0;
		}
		else {
		/* new iovec */
					AKA_mark("lis===338###sois===8207###eois===8233###lif===20###soif===532###eoif===558###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");wb->iovec[i].iov_base = p;

					AKA_mark("lis===339###sois===8236###eois===8274###lif===21###soif===561###eoif===599###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");wb->iovec[i].iov_len = (size_t)length;

					AKA_mark("lis===340###sois===8277###eois===8298###lif===22###soif===602###eoif===623###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");wb->iovcount = i + 1;

	}
	}

	/* store now */
		AKA_mark("lis===343###sois===8320###eois===8353###lif===25###soif===645###eoif===678###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");memcpy(p, value, (size_t)length);

		AKA_mark("lis===344###sois===8355###eois===8377###lif===26###soif===680###eoif===702###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");wb->bufcount = nafter;

		AKA_mark("lis===345###sois===8379###eois===8388###lif===27###soif===704###eoif===713###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_putbuf(struct writebuf*,const void*,int)");return 1;

}

/** Instrumented function writebuf_char(struct writebuf*,char) */
__attribute__((unused))
static int writebuf_char(struct writebuf *wb, char value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/writebuf_char(struct writebuf*,char)");AKA_fCall++;
		AKA_mark("lis===351###sois===8477###eois===8515###lif===3###soif===85###eoif===123###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_char(struct writebuf*,char)");return writebuf_putbuf(wb, &value, 1);

}

/** Instrumented function writebuf_uint32(struct writebuf*,uint32_t) */
static int writebuf_uint32(struct writebuf *wb, uint32_t value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/writebuf_uint32(struct writebuf*,uint32_t)");AKA_fCall++;
		AKA_mark("lis===356###sois===8586###eois===8609###lif===2###soif===67###eoif===90###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_uint32(struct writebuf*,uint32_t)");value = htole32(value);

		AKA_mark("lis===357###sois===8611###eois===8665###lif===3###soif===92###eoif===146###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_uint32(struct writebuf*,uint32_t)");return writebuf_putbuf(wb, &value, (int)sizeof value);

}

/** Instrumented function writebuf_uint16(struct writebuf*,uint16_t) */
static int writebuf_uint16(struct writebuf *wb, uint16_t value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/writebuf_uint16(struct writebuf*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===362###sois===8736###eois===8759###lif===2###soif===67###eoif===90###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_uint16(struct writebuf*,uint16_t)");value = htole16(value);

		AKA_mark("lis===363###sois===8761###eois===8815###lif===3###soif===92###eoif===146###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_uint16(struct writebuf*,uint16_t)");return writebuf_putbuf(wb, &value, (int)sizeof value);

}

/** Instrumented function writebuf_uint8(struct writebuf*,uint8_t) */
static int writebuf_uint8(struct writebuf *wb, uint8_t value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/writebuf_uint8(struct writebuf*,uint8_t)");AKA_fCall++;
		AKA_mark("lis===368###sois===8884###eois===8938###lif===2###soif===65###eoif===119###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_uint8(struct writebuf*,uint8_t)");return writebuf_putbuf(wb, &value, (int)sizeof value);

}

/** Instrumented function writebuf_string_length(struct writebuf*,const char*,size_t) */
static int writebuf_string_length(struct writebuf *wb, const char *value, size_t length)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/writebuf_string_length(struct writebuf*,const char*,size_t)");AKA_fCall++;
		AKA_mark("lis===373###sois===9034###eois===9068###lif===2###soif===92###eoif===126###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_string_length(struct writebuf*,const char*,size_t)");uint32_t len = (uint32_t)++length;

		AKA_mark("lis===374###sois===9070###eois===9169###lif===3###soif===128###eoif===227###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_string_length(struct writebuf*,const char*,size_t)");return (size_t)len == length && len && writebuf_uint32(wb, len) && writebuf_put(wb, value, length);

}

/** Instrumented function writebuf_string(struct writebuf*,const char*) */
static int writebuf_string(struct writebuf *wb, const char *value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/writebuf_string(struct writebuf*,const char*)");AKA_fCall++;
		AKA_mark("lis===379###sois===9243###eois===9299###lif===2###soif===70###eoif===126###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_string(struct writebuf*,const char*)");return writebuf_string_length(wb, value, strlen(value));

}

/** Instrumented function writebuf_nullstring(struct writebuf*,const char*) */
static int writebuf_nullstring(struct writebuf *wb, const char *value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/writebuf_nullstring(struct writebuf*,const char*)");AKA_fCall++;
		AKA_mark("lis===384###sois===9377###eois===9466###lif===2###soif===74###eoif===163###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_nullstring(struct writebuf*,const char*)");return value ? writebuf_string_length(wb, value, strlen(value)) : writebuf_uint32(wb, 0);

}

/** Instrumented function writebuf_object(struct writebuf*,struct json_object*) */
static int writebuf_object(struct writebuf *wb, struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/writebuf_object(struct writebuf*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===389###sois===9549###eois===9633###lif===2###soif===79###eoif===163###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_object(struct writebuf*,struct json_object*)");const char *string = json_object_to_json_string_ext(object, JSON_C_TO_STRING_PLAIN);

		AKA_mark("lis===390###sois===9635###eois===9688###lif===3###soif===165###eoif===218###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/writebuf_object(struct writebuf*,struct json_object*)");return string != NULL && writebuf_string(wb, string);

}

/******************* queuing of messages *****************/

/* queue the processing of the received message (except if size=0 cause it's not a valid message) */
/** Instrumented function queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*)) */
static void queue_message_processing(struct afb_proto_ws *protows, char *data, size_t size, void (*processing)(int,void*))
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))");AKA_fCall++;
		AKA_mark("lis===398###sois===9980###eois===10002###lif===2###soif===126###eoif===148###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))");struct binary *binary;


		if (AKA_mark("lis===400###sois===10009###eois===10013###lif===4###soif===155###eoif===159###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))") && (AKA_mark("lis===400###sois===10009###eois===10013###lif===4###soif===155###eoif===159###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))")&&size)) {
				AKA_mark("lis===401###sois===10019###eois===10051###lif===5###soif===165###eoif===197###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))");binary = malloc(sizeof *binary);

				if (AKA_mark("lis===402###sois===10058###eois===10065###lif===6###soif===204###eoif===211###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))") && (AKA_mark("lis===402###sois===10058###eois===10065###lif===6###soif===204###eoif===211###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))")&&!binary)) {
			/* TODO process the problem */
						AKA_mark("lis===404###sois===10106###eois===10121###lif===8###soif===252###eoif===267###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))");errno = ENOMEM;

		}
		else {
						AKA_mark("lis===406###sois===10136###eois===10162###lif===10###soif===282###eoif===308###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))");binary->protows = protows;

						AKA_mark("lis===407###sois===10166###eois===10189###lif===11###soif===312###eoif===335###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))");binary->rb.base = data;

						AKA_mark("lis===408###sois===10193###eois===10216###lif===12###soif===339###eoif===362###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))");binary->rb.head = data;

						AKA_mark("lis===409###sois===10220###eois===10249###lif===13###soif===366###eoif===395###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))");binary->rb.end = data + size;

						if (AKA_mark("lis===410###sois===10257###eois===10331###lif===14###soif===403###eoif===477###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))") && ((AKA_mark("lis===410###sois===10257###eois===10274###lif===14###soif===403###eoif===420###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))")&&!protows->queuing)	||(AKA_mark("lis===411###sois===10282###eois===10331###lif===15###soif===428###eoif===477###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))")&&protows->queuing(protows, processing, binary) < 0))) {
				AKA_mark("lis===412###sois===10337###eois===10359###lif===16###soif===483###eoif===505###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))");processing(0, binary);
			}
			else {AKA_mark("lis===-410-###sois===-10257-###eois===-1025774-###lif===-14-###soif===-###eoif===-477-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))");}

						AKA_mark("lis===413###sois===10363###eois===10370###lif===17###soif===509###eoif===516###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))");return;

		}

	}
	else {AKA_mark("lis===-400-###sois===-10009-###eois===-100094-###lif===-4-###soif===-###eoif===-159-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))");}

		AKA_mark("lis===416###sois===10379###eois===10390###lif===20###soif===525###eoif===536###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/queue_message_processing(struct afb_proto_ws*,char*,size_t,void(*processing)(int,void*))");free(data);

}

/******************* sending messages *****************/

/** Instrumented function proto_write(struct afb_proto_ws*,struct writebuf*) */
static int proto_write(struct afb_proto_ws *protows, struct writebuf *wb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)");AKA_fCall++;
		AKA_mark("lis===423###sois===10529###eois===10536###lif===2###soif===77###eoif===84###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)");int rc;

		AKA_mark("lis===424###sois===10538###eois===10556###lif===3###soif===86###eoif===104###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)");struct afb_ws *ws;


		AKA_mark("lis===426###sois===10559###eois===10595###lif===5###soif===107###eoif===143###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)");pthread_mutex_lock(&protows->mutex);

		AKA_mark("lis===427###sois===10597###eois===10614###lif===6###soif===145###eoif===162###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)");ws = protows->ws;

		if (AKA_mark("lis===428###sois===10620###eois===10630###lif===7###soif===168###eoif===178###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)") && (AKA_mark("lis===428###sois===10620###eois===10630###lif===7###soif===168###eoif===178###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)")&&ws == NULL)) {
				AKA_mark("lis===429###sois===10636###eois===10650###lif===8###soif===184###eoif===198###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)");errno = EPIPE;

				AKA_mark("lis===430###sois===10653###eois===10661###lif===9###soif===201###eoif===209###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)");rc = -1;

	}
	else {
				AKA_mark("lis===432###sois===10674###eois===10724###lif===11###soif===222###eoif===272###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)");rc = afb_ws_binary_v(ws, wb->iovec, wb->iovcount);

				if (AKA_mark("lis===433###sois===10731###eois===10737###lif===12###soif===279###eoif===285###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)") && (AKA_mark("lis===433###sois===10731###eois===10737###lif===12###soif===279###eoif===285###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)")&&rc > 0)) {
			AKA_mark("lis===434###sois===10742###eois===10749###lif===13###soif===290###eoif===297###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)");rc = 0;
		}
		else {AKA_mark("lis===-433-###sois===-10731-###eois===-107316-###lif===-12-###soif===-###eoif===-285-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)");}

	}

		AKA_mark("lis===436###sois===10754###eois===10792###lif===15###soif===302###eoif===340###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)");pthread_mutex_unlock(&protows->mutex);

		AKA_mark("lis===437###sois===10794###eois===10804###lif===16###soif===342###eoif===352###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/proto_write(struct afb_proto_ws*,struct writebuf*)");return rc;

}

/** Instrumented function send_version_offer_1(struct afb_proto_ws*,uint8_t) */
static int send_version_offer_1(struct afb_proto_ws *protows, uint8_t version)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/send_version_offer_1(struct afb_proto_ws*,uint8_t)");AKA_fCall++;
		AKA_mark("lis===442###sois===10890###eois===10902###lif===2###soif===82###eoif===94###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_offer_1(struct afb_proto_ws*,uint8_t)");int rc = -1;

		AKA_mark("lis===443###sois===10904###eois===10958###lif===3###soif===96###eoif===150###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_offer_1(struct afb_proto_ws*,uint8_t)");struct writebuf wb = { .iovcount = 0, .bufcount = 0 };


		if (AKA_mark("lis===445###sois===10965###eois===11137###lif===5###soif===157###eoif===329###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_offer_1(struct afb_proto_ws*,uint8_t)") && ((((AKA_mark("lis===445###sois===10965###eois===11007###lif===5###soif===157###eoif===199###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_offer_1(struct afb_proto_ws*,uint8_t)")&&writebuf_char(&wb, CHAR_FOR_VERSION_OFFER))	&&(AKA_mark("lis===446###sois===11013###eois===11051###lif===6###soif===205###eoif===243###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_offer_1(struct afb_proto_ws*,uint8_t)")&&writebuf_uint32(&wb, WSAPI_IDENTIFIER)))	&&(AKA_mark("lis===447###sois===11057###eois===11079###lif===7###soif===249###eoif===271###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_offer_1(struct afb_proto_ws*,uint8_t)")&&writebuf_uint8(&wb, 1)))	&&(AKA_mark("lis===448###sois===11109###eois===11137###lif===8###soif===301###eoif===329###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_offer_1(struct afb_proto_ws*,uint8_t)")&&writebuf_uint8(&wb, version)))) {
		AKA_mark("lis===449###sois===11141###eois===11172###lif===9###soif===333###eoif===364###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_offer_1(struct afb_proto_ws*,uint8_t)");rc = proto_write(protows, &wb);
	}
	else {AKA_mark("lis===-445-###sois===-10965-###eois===-10965172-###lif===-5-###soif===-###eoif===-329-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_offer_1(struct afb_proto_ws*,uint8_t)");}

		AKA_mark("lis===450###sois===11174###eois===11184###lif===10###soif===366###eoif===376###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_offer_1(struct afb_proto_ws*,uint8_t)");return rc;

}

/** Instrumented function send_version_set(struct afb_proto_ws*,uint8_t) */
static int send_version_set(struct afb_proto_ws *protows, uint8_t version)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/send_version_set(struct afb_proto_ws*,uint8_t)");AKA_fCall++;
		AKA_mark("lis===455###sois===11266###eois===11278###lif===2###soif===78###eoif===90###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_set(struct afb_proto_ws*,uint8_t)");int rc = -1;

		AKA_mark("lis===456###sois===11280###eois===11334###lif===3###soif===92###eoif===146###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_set(struct afb_proto_ws*,uint8_t)");struct writebuf wb = { .iovcount = 0, .bufcount = 0 };


		if (AKA_mark("lis===458###sois===11341###eois===11415###lif===5###soif===153###eoif===227###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_set(struct afb_proto_ws*,uint8_t)") && ((AKA_mark("lis===458###sois===11341###eois===11381###lif===5###soif===153###eoif===193###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_set(struct afb_proto_ws*,uint8_t)")&&writebuf_char(&wb, CHAR_FOR_VERSION_SET))	&&(AKA_mark("lis===459###sois===11387###eois===11415###lif===6###soif===199###eoif===227###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_set(struct afb_proto_ws*,uint8_t)")&&writebuf_uint8(&wb, version)))) {
		AKA_mark("lis===460###sois===11419###eois===11450###lif===7###soif===231###eoif===262###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_set(struct afb_proto_ws*,uint8_t)");rc = proto_write(protows, &wb);
	}
	else {AKA_mark("lis===-458-###sois===-11341-###eois===-1134174-###lif===-5-###soif===-###eoif===-227-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_set(struct afb_proto_ws*,uint8_t)");}

		AKA_mark("lis===461###sois===11452###eois===11462###lif===8###soif===264###eoif===274###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/send_version_set(struct afb_proto_ws*,uint8_t)");return rc;

}

/******************* ws request part for server *****************/

/** Instrumented function afb_proto_ws_call_addref(struct afb_proto_ws_call*) */
void afb_proto_ws_call_addref(struct afb_proto_ws_call *call)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_addref(struct afb_proto_ws_call*)");AKA_fCall++;
		AKA_mark("lis===468###sois===11599###eois===11656###lif===2###soif===65###eoif===122###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_addref(struct afb_proto_ws_call*)");__atomic_add_fetch(&call->refcount, 1, __ATOMIC_RELAXED);

}

/** Instrumented function afb_proto_ws_call_unref(struct afb_proto_ws_call*) */
void afb_proto_ws_call_unref(struct afb_proto_ws_call *call)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unref(struct afb_proto_ws_call*)");AKA_fCall++;
		if (AKA_mark("lis===473###sois===11728###eois===11784###lif===2###soif===68###eoif===124###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unref(struct afb_proto_ws_call*)") && (AKA_mark("lis===473###sois===11728###eois===11784###lif===2###soif===68###eoif===124###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unref(struct afb_proto_ws_call*)")&&__atomic_sub_fetch(&call->refcount, 1, __ATOMIC_RELAXED))) {
		AKA_mark("lis===474###sois===11788###eois===11795###lif===3###soif===128###eoif===135###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unref(struct afb_proto_ws_call*)");return;
	}
	else {AKA_mark("lis===-473-###sois===-11728-###eois===-1172856-###lif===-2-###soif===-###eoif===-124-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unref(struct afb_proto_ws_call*)");}


		AKA_mark("lis===476###sois===11798###eois===11832###lif===5###soif===138###eoif===172###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unref(struct afb_proto_ws_call*)");afb_proto_ws_unref(call->protows);

		AKA_mark("lis===477###sois===11834###eois===11853###lif===6###soif===174###eoif===193###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unref(struct afb_proto_ws_call*)");free(call->buffer);

		AKA_mark("lis===478###sois===11855###eois===11866###lif===7###soif===195###eoif===206###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unref(struct afb_proto_ws_call*)");free(call);

}

/** Instrumented function afb_proto_ws_call_reply(struct afb_proto_ws_call*,struct json_object*,const char*,const char*) */
int afb_proto_ws_call_reply(struct afb_proto_ws_call *call, struct json_object *obj, const char *error, const char *info)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_reply(struct afb_proto_ws_call*,struct json_object*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===483###sois===11995###eois===12007###lif===2###soif===125###eoif===137###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_reply(struct afb_proto_ws_call*,struct json_object*,const char*,const char*)");int rc = -1;

		AKA_mark("lis===484###sois===12009###eois===12063###lif===3###soif===139###eoif===193###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_reply(struct afb_proto_ws_call*,struct json_object*,const char*,const char*)");struct writebuf wb = { .iovcount = 0, .bufcount = 0 };

		AKA_mark("lis===485###sois===12065###eois===12110###lif===4###soif===195###eoif===240###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_reply(struct afb_proto_ws_call*,struct json_object*,const char*,const char*)");struct afb_proto_ws *protows = call->protows;


		if (AKA_mark("lis===487###sois===12117###eois===12295###lif===6###soif===247###eoif===425###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_reply(struct afb_proto_ws_call*,struct json_object*,const char*,const char*)") && (((((AKA_mark("lis===487###sois===12117###eois===12151###lif===6###soif===247###eoif===281###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_reply(struct afb_proto_ws_call*,struct json_object*,const char*,const char*)")&&writebuf_char(&wb, CHAR_FOR_REPLY))	&&(AKA_mark("lis===488###sois===12157###eois===12191###lif===7###soif===287###eoif===321###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_reply(struct afb_proto_ws_call*,struct json_object*,const char*,const char*)")&&writebuf_uint16(&wb, call->callid)))	&&(AKA_mark("lis===489###sois===12197###eois===12228###lif===8###soif===327###eoif===358###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_reply(struct afb_proto_ws_call*,struct json_object*,const char*,const char*)")&&writebuf_nullstring(&wb, error)))	&&(AKA_mark("lis===490###sois===12234###eois===12264###lif===9###soif===364###eoif===394###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_reply(struct afb_proto_ws_call*,struct json_object*,const char*,const char*)")&&writebuf_nullstring(&wb, info)))	&&(AKA_mark("lis===491###sois===12270###eois===12295###lif===10###soif===400###eoif===425###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_reply(struct afb_proto_ws_call*,struct json_object*,const char*,const char*)")&&writebuf_object(&wb, obj)))) {
		AKA_mark("lis===492###sois===12299###eois===12330###lif===11###soif===429###eoif===460###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_reply(struct afb_proto_ws_call*,struct json_object*,const char*,const char*)");rc = proto_write(protows, &wb);
	}
	else {AKA_mark("lis===-487-###sois===-12117-###eois===-12117178-###lif===-6-###soif===-###eoif===-425-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_reply(struct afb_proto_ws_call*,struct json_object*,const char*,const char*)");}

		AKA_mark("lis===493###sois===12332###eois===12342###lif===12###soif===462###eoif===472###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_reply(struct afb_proto_ws_call*,struct json_object*,const char*,const char*)");return rc;

}

/** Instrumented function afb_proto_ws_call_subscribe(struct afb_proto_ws_call*,uint16_t) */
int afb_proto_ws_call_subscribe(struct afb_proto_ws_call *call, uint16_t event_id)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_subscribe(struct afb_proto_ws_call*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===498###sois===12432###eois===12444###lif===2###soif===86###eoif===98###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_subscribe(struct afb_proto_ws_call*,uint16_t)");int rc = -1;

		AKA_mark("lis===499###sois===12446###eois===12500###lif===3###soif===100###eoif===154###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_subscribe(struct afb_proto_ws_call*,uint16_t)");struct writebuf wb = { .iovcount = 0, .bufcount = 0 };

		AKA_mark("lis===500###sois===12502###eois===12547###lif===4###soif===156###eoif===201###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_subscribe(struct afb_proto_ws_call*,uint16_t)");struct afb_proto_ws *protows = call->protows;


		if (AKA_mark("lis===502###sois===12554###eois===12672###lif===6###soif===208###eoif===326###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_subscribe(struct afb_proto_ws_call*,uint16_t)") && (((AKA_mark("lis===502###sois===12554###eois===12596###lif===6###soif===208###eoif===250###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_subscribe(struct afb_proto_ws_call*,uint16_t)")&&writebuf_char(&wb, CHAR_FOR_EVT_SUBSCRIBE))	&&(AKA_mark("lis===503###sois===12602###eois===12636###lif===7###soif===256###eoif===290###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_subscribe(struct afb_proto_ws_call*,uint16_t)")&&writebuf_uint16(&wb, call->callid)))	&&(AKA_mark("lis===504###sois===12642###eois===12672###lif===8###soif===296###eoif===326###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_subscribe(struct afb_proto_ws_call*,uint16_t)")&&writebuf_uint16(&wb, event_id)))) {
		AKA_mark("lis===505###sois===12676###eois===12707###lif===9###soif===330###eoif===361###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_subscribe(struct afb_proto_ws_call*,uint16_t)");rc = proto_write(protows, &wb);
	}
	else {AKA_mark("lis===-502-###sois===-12554-###eois===-12554118-###lif===-6-###soif===-###eoif===-326-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_subscribe(struct afb_proto_ws_call*,uint16_t)");}

		AKA_mark("lis===506###sois===12709###eois===12719###lif===10###soif===363###eoif===373###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_subscribe(struct afb_proto_ws_call*,uint16_t)");return rc;

}

/** Instrumented function afb_proto_ws_call_unsubscribe(struct afb_proto_ws_call*,uint16_t) */
int afb_proto_ws_call_unsubscribe(struct afb_proto_ws_call *call, uint16_t event_id)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unsubscribe(struct afb_proto_ws_call*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===511###sois===12811###eois===12823###lif===2###soif===88###eoif===100###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unsubscribe(struct afb_proto_ws_call*,uint16_t)");int rc = -1;

		AKA_mark("lis===512###sois===12825###eois===12879###lif===3###soif===102###eoif===156###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unsubscribe(struct afb_proto_ws_call*,uint16_t)");struct writebuf wb = { .iovcount = 0, .bufcount = 0 };

		AKA_mark("lis===513###sois===12881###eois===12926###lif===4###soif===158###eoif===203###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unsubscribe(struct afb_proto_ws_call*,uint16_t)");struct afb_proto_ws *protows = call->protows;


		if (AKA_mark("lis===515###sois===12933###eois===13053###lif===6###soif===210###eoif===330###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unsubscribe(struct afb_proto_ws_call*,uint16_t)") && (((AKA_mark("lis===515###sois===12933###eois===12977###lif===6###soif===210###eoif===254###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unsubscribe(struct afb_proto_ws_call*,uint16_t)")&&writebuf_char(&wb, CHAR_FOR_EVT_UNSUBSCRIBE))	&&(AKA_mark("lis===516###sois===12983###eois===13017###lif===7###soif===260###eoif===294###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unsubscribe(struct afb_proto_ws_call*,uint16_t)")&&writebuf_uint16(&wb, call->callid)))	&&(AKA_mark("lis===517###sois===13023###eois===13053###lif===8###soif===300###eoif===330###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unsubscribe(struct afb_proto_ws_call*,uint16_t)")&&writebuf_uint16(&wb, event_id)))) {
		AKA_mark("lis===518###sois===13057###eois===13088###lif===9###soif===334###eoif===365###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unsubscribe(struct afb_proto_ws_call*,uint16_t)");rc = proto_write(protows, &wb);
	}
	else {AKA_mark("lis===-515-###sois===-12933-###eois===-12933120-###lif===-6-###soif===-###eoif===-330-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unsubscribe(struct afb_proto_ws_call*,uint16_t)");}

		AKA_mark("lis===519###sois===13090###eois===13100###lif===10###soif===367###eoif===377###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_call_unsubscribe(struct afb_proto_ws_call*,uint16_t)");return rc;

}

/******************* client part **********************************/

/* search a memorized call */
/** Instrumented function client_call_search_locked(struct afb_proto_ws*,uint16_t) */
static struct client_call *client_call_search_locked(struct afb_proto_ws *protows, uint16_t callid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_call_search_locked(struct afb_proto_ws*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===527###sois===13307###eois===13332###lif===2###soif===103###eoif===128###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_search_locked(struct afb_proto_ws*,uint16_t)");struct client_call *call;


		AKA_mark("lis===529###sois===13335###eois===13357###lif===4###soif===131###eoif===153###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_search_locked(struct afb_proto_ws*,uint16_t)");call = protows->calls;

		while (AKA_mark("lis===530###sois===13366###eois===13404###lif===5###soif===162###eoif===200###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_search_locked(struct afb_proto_ws*,uint16_t)") && ((AKA_mark("lis===530###sois===13366###eois===13378###lif===5###soif===162###eoif===174###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_search_locked(struct afb_proto_ws*,uint16_t)")&&call != NULL)	&&(AKA_mark("lis===530###sois===13382###eois===13404###lif===5###soif===178###eoif===200###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_search_locked(struct afb_proto_ws*,uint16_t)")&&call->callid != callid))) {
		AKA_mark("lis===531###sois===13408###eois===13426###lif===6###soif===204###eoif===222###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_search_locked(struct afb_proto_ws*,uint16_t)");call = call->next;
	}


		AKA_mark("lis===533###sois===13429###eois===13441###lif===8###soif===225###eoif===237###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_search_locked(struct afb_proto_ws*,uint16_t)");return call;

}

/** Instrumented function client_call_search_unlocked(struct afb_proto_ws*,uint16_t) */
static struct client_call *client_call_search_unlocked(struct afb_proto_ws *protows, uint16_t callid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_call_search_unlocked(struct afb_proto_ws*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===538###sois===13550###eois===13577###lif===2###soif===105###eoif===132###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_search_unlocked(struct afb_proto_ws*,uint16_t)");struct client_call *result;


		AKA_mark("lis===540###sois===13580###eois===13616###lif===4###soif===135###eoif===171###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_search_unlocked(struct afb_proto_ws*,uint16_t)");pthread_mutex_lock(&protows->mutex);

		AKA_mark("lis===541###sois===13618###eois===13670###lif===5###soif===173###eoif===225###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_search_unlocked(struct afb_proto_ws*,uint16_t)");result = client_call_search_locked(protows, callid);

		AKA_mark("lis===542###sois===13672###eois===13710###lif===6###soif===227###eoif===265###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_search_unlocked(struct afb_proto_ws*,uint16_t)");pthread_mutex_unlock(&protows->mutex);

		AKA_mark("lis===543###sois===13712###eois===13726###lif===7###soif===267###eoif===281###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_search_unlocked(struct afb_proto_ws*,uint16_t)");return result;

}

/* free and release the memorizing call */
/** Instrumented function client_call_destroy(struct afb_proto_ws*,struct client_call*) */
static void client_call_destroy(struct afb_proto_ws *protows, struct client_call *call)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)");AKA_fCall++;
		AKA_mark("lis===549###sois===13864###eois===13889###lif===2###soif===91###eoif===116###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)");struct client_call **prv;


		AKA_mark("lis===551###sois===13892###eois===13928###lif===4###soif===119###eoif===155###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)");pthread_mutex_lock(&protows->mutex);

		AKA_mark("lis===552###sois===13930###eois===13952###lif===5###soif===157###eoif===179###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)");prv = &protows->calls;

		while (AKA_mark("lis===553###sois===13961###eois===13973###lif===6###soif===188###eoif===200###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)") && (AKA_mark("lis===553###sois===13961###eois===13973###lif===6###soif===188###eoif===200###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)")&&*prv != NULL)) {
				if (AKA_mark("lis===554###sois===13983###eois===13995###lif===7###soif===210###eoif===222###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)") && (AKA_mark("lis===554###sois===13983###eois===13995###lif===7###soif===210###eoif===222###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)")&&*prv == call)) {
						AKA_mark("lis===555###sois===14002###eois===14021###lif===8###soif===229###eoif===248###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)");protows->idcount--;

						AKA_mark("lis===556###sois===14025###eois===14043###lif===9###soif===252###eoif===270###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)");*prv = call->next;

						AKA_mark("lis===557###sois===14047###eois===14085###lif===10###soif===274###eoif===312###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)");	AKA_mark("lis===563###sois===14143###eois===14181###lif===16###soif===370###eoif===408###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)");pthread_mutex_unlock(&protows->mutex);


						AKA_mark("lis===558###sois===14089###eois===14100###lif===11###soif===316###eoif===327###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)");free(call);

						AKA_mark("lis===559###sois===14104###eois===14111###lif===12###soif===331###eoif===338###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)");return;

		}
		else {AKA_mark("lis===-554-###sois===-13983-###eois===-1398312-###lif===-7-###soif===-###eoif===-222-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)");}

				AKA_mark("lis===561###sois===14118###eois===14138###lif===14###soif===345###eoif===365###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_call_destroy(struct afb_proto_ws*,struct client_call*)");prv = &(*prv)->next;

	}

	pthread_mutex_unlock(&protows->mutex);
}

/* get event from the message */
/** Instrumented function client_msg_call_get(struct afb_proto_ws*,struct readbuf*,struct client_call**) */
static int client_msg_call_get(struct afb_proto_ws *protows, struct readbuf *rb, struct client_call **call)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_msg_call_get(struct afb_proto_ws*,struct readbuf*,struct client_call**)");AKA_fCall++;
		AKA_mark("lis===569###sois===14329###eois===14345###lif===2###soif===111###eoif===127###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_msg_call_get(struct afb_proto_ws*,struct readbuf*,struct client_call**)");uint16_t callid;


	/* get event data from the message */
		if (AKA_mark("lis===572###sois===14391###eois===14419###lif===5###soif===173###eoif===201###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_msg_call_get(struct afb_proto_ws*,struct readbuf*,struct client_call**)") && (AKA_mark("lis===572###sois===14391###eois===14419###lif===5###soif===173###eoif===201###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_msg_call_get(struct afb_proto_ws*,struct readbuf*,struct client_call**)")&&!readbuf_uint16(rb, &callid))) {
		AKA_mark("lis===573###sois===14423###eois===14432###lif===6###soif===205###eoif===214###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_msg_call_get(struct afb_proto_ws*,struct readbuf*,struct client_call**)");return 0;
	}
	else {AKA_mark("lis===-572-###sois===-14391-###eois===-1439128-###lif===-5-###soif===-###eoif===-201-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_msg_call_get(struct afb_proto_ws*,struct readbuf*,struct client_call**)");}


	/* get the call */
		AKA_mark("lis===576###sois===14455###eois===14508###lif===9###soif===237###eoif===290###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_msg_call_get(struct afb_proto_ws*,struct readbuf*,struct client_call**)");*call = client_call_search_unlocked(protows, callid);

		AKA_mark("lis===577###sois===14510###eois===14531###lif===10###soif===292###eoif===313###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_msg_call_get(struct afb_proto_ws*,struct readbuf*,struct client_call**)");return *call != NULL;

}

/* adds an event */
/** Instrumented function client_on_event_create(struct afb_proto_ws*,struct readbuf*) */
static void client_on_event_create(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_on_event_create(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===583###sois===14643###eois===14666###lif===2###soif===88###eoif===111###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_create(struct afb_proto_ws*,struct readbuf*)");const char *event_name;

		AKA_mark("lis===584###sois===14668###eois===14686###lif===3###soif===113###eoif===131###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_create(struct afb_proto_ws*,struct readbuf*)");uint16_t event_id;


		if (AKA_mark("lis===586###sois===14693###eois===14810###lif===5###soif===138###eoif===255###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_create(struct afb_proto_ws*,struct readbuf*)") && (((AKA_mark("lis===586###sois===14693###eois===14729###lif===5###soif===138###eoif===174###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_create(struct afb_proto_ws*,struct readbuf*)")&&protows->client_itf->on_event_create)	&&(AKA_mark("lis===587###sois===14737###eois===14766###lif===6###soif===182###eoif===211###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_create(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint16(rb, &event_id)))	&&(AKA_mark("lis===588###sois===14773###eois===14810###lif===7###soif===218###eoif===255###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_create(struct afb_proto_ws*,struct readbuf*)")&&readbuf_string(rb, &event_name, NULL)))) {
		AKA_mark("lis===589###sois===14814###eois===14891###lif===8###soif===259###eoif===336###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_create(struct afb_proto_ws*,struct readbuf*)");protows->client_itf->on_event_create(protows->closure, event_id, event_name);
	}
	else {
		AKA_mark("lis===591###sois===14900###eois===14936###lif===10###soif===345###eoif===381###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_create(struct afb_proto_ws*,struct readbuf*)");ERROR("Ignoring creation of event");
	}

}

/* removes an event */
/** Instrumented function client_on_event_remove(struct afb_proto_ws*,struct readbuf*) */
static void client_on_event_remove(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_on_event_remove(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===597###sois===15051###eois===15069###lif===2###soif===88###eoif===106###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_remove(struct afb_proto_ws*,struct readbuf*)");uint16_t event_id;


		if (AKA_mark("lis===599###sois===15076###eois===15145###lif===4###soif===113###eoif===182###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_remove(struct afb_proto_ws*,struct readbuf*)") && ((AKA_mark("lis===599###sois===15076###eois===15112###lif===4###soif===113###eoif===149###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_remove(struct afb_proto_ws*,struct readbuf*)")&&protows->client_itf->on_event_remove)	&&(AKA_mark("lis===599###sois===15116###eois===15145###lif===4###soif===153###eoif===182###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_remove(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint16(rb, &event_id)))) {
		AKA_mark("lis===600###sois===15149###eois===15214###lif===5###soif===186###eoif===251###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_remove(struct afb_proto_ws*,struct readbuf*)");protows->client_itf->on_event_remove(protows->closure, event_id);
	}
	else {
		AKA_mark("lis===602###sois===15223###eois===15259###lif===7###soif===260###eoif===296###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_remove(struct afb_proto_ws*,struct readbuf*)");ERROR("Ignoring deletion of event");
	}

}

/* subscribes an event */
/** Instrumented function client_on_event_subscribe(struct afb_proto_ws*,struct readbuf*) */
static void client_on_event_subscribe(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_on_event_subscribe(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===608###sois===15380###eois===15398###lif===2###soif===91###eoif===109###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_subscribe(struct afb_proto_ws*,struct readbuf*)");uint16_t event_id;

		AKA_mark("lis===609###sois===15400###eois===15425###lif===3###soif===111###eoif===136###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_subscribe(struct afb_proto_ws*,struct readbuf*)");struct client_call *call;


		if (AKA_mark("lis===611###sois===15432###eois===15547###lif===5###soif===143###eoif===258###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_subscribe(struct afb_proto_ws*,struct readbuf*)") && (((AKA_mark("lis===611###sois===15432###eois===15471###lif===5###soif===143###eoif===182###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_subscribe(struct afb_proto_ws*,struct readbuf*)")&&protows->client_itf->on_event_subscribe)	&&(AKA_mark("lis===611###sois===15475###eois===15514###lif===5###soif===186###eoif===225###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_subscribe(struct afb_proto_ws*,struct readbuf*)")&&client_msg_call_get(protows, rb, &call)))	&&(AKA_mark("lis===611###sois===15518###eois===15547###lif===5###soif===229###eoif===258###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_subscribe(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint16(rb, &event_id)))) {
		AKA_mark("lis===612###sois===15551###eois===15634###lif===6###soif===262###eoif===345###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_subscribe(struct afb_proto_ws*,struct readbuf*)");protows->client_itf->on_event_subscribe(protows->closure, call->request, event_id);
	}
	else {
		AKA_mark("lis===614###sois===15643###eois===15683###lif===8###soif===354###eoif===394###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_subscribe(struct afb_proto_ws*,struct readbuf*)");ERROR("Ignoring subscription to event");
	}

}

/* unsubscribes an event */
/** Instrumented function client_on_event_unsubscribe(struct afb_proto_ws*,struct readbuf*) */
static void client_on_event_unsubscribe(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_on_event_unsubscribe(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===620###sois===15808###eois===15826###lif===2###soif===93###eoif===111###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_unsubscribe(struct afb_proto_ws*,struct readbuf*)");uint16_t event_id;

		AKA_mark("lis===621###sois===15828###eois===15853###lif===3###soif===113###eoif===138###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_unsubscribe(struct afb_proto_ws*,struct readbuf*)");struct client_call *call;


		if (AKA_mark("lis===623###sois===15860###eois===15977###lif===5###soif===145###eoif===262###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_unsubscribe(struct afb_proto_ws*,struct readbuf*)") && (((AKA_mark("lis===623###sois===15860###eois===15901###lif===5###soif===145###eoif===186###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_unsubscribe(struct afb_proto_ws*,struct readbuf*)")&&protows->client_itf->on_event_unsubscribe)	&&(AKA_mark("lis===623###sois===15905###eois===15944###lif===5###soif===190###eoif===229###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_unsubscribe(struct afb_proto_ws*,struct readbuf*)")&&client_msg_call_get(protows, rb, &call)))	&&(AKA_mark("lis===623###sois===15948###eois===15977###lif===5###soif===233###eoif===262###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_unsubscribe(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint16(rb, &event_id)))) {
		AKA_mark("lis===624###sois===15981###eois===16066###lif===6###soif===266###eoif===351###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_unsubscribe(struct afb_proto_ws*,struct readbuf*)");protows->client_itf->on_event_unsubscribe(protows->closure, call->request, event_id);
	}
	else {
		AKA_mark("lis===626###sois===16075###eois===16117###lif===8###soif===360###eoif===402###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_unsubscribe(struct afb_proto_ws*,struct readbuf*)");ERROR("Ignoring unsubscription to event");
	}

}

/* receives broadcasted events */
/** Instrumented function client_on_event_broadcast(struct afb_proto_ws*,struct readbuf*) */
static void client_on_event_broadcast(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_on_event_broadcast(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===632###sois===16246###eois===16276###lif===2###soif===91###eoif===121###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_broadcast(struct afb_proto_ws*,struct readbuf*)");const char *event_name, *uuid;

		AKA_mark("lis===633###sois===16278###eois===16290###lif===3###soif===123###eoif===135###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_broadcast(struct afb_proto_ws*,struct readbuf*)");uint8_t hop;

		AKA_mark("lis===634###sois===16292###eois===16319###lif===4###soif===137###eoif===164###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_broadcast(struct afb_proto_ws*,struct readbuf*)");struct json_object *object;


		if (AKA_mark("lis===636###sois===16326###eois===16496###lif===6###soif===171###eoif===341###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_broadcast(struct afb_proto_ws*,struct readbuf*)") && (((((AKA_mark("lis===636###sois===16326###eois===16365###lif===6###soif===171###eoif===210###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_broadcast(struct afb_proto_ws*,struct readbuf*)")&&protows->client_itf->on_event_broadcast)	&&(AKA_mark("lis===636###sois===16369###eois===16406###lif===6###soif===214###eoif===251###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_broadcast(struct afb_proto_ws*,struct readbuf*)")&&readbuf_string(rb, &event_name, NULL)))	&&(AKA_mark("lis===636###sois===16410###eois===16437###lif===6###soif===255###eoif===282###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_broadcast(struct afb_proto_ws*,struct readbuf*)")&&readbuf_object(rb, &object)))	&&(uuid=(readbuf_get(rb, 16)) && AKA_mark("lis===636###sois===16442###eois===16468###lif===6###soif===287###eoif===313###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_broadcast(struct afb_proto_ws*,struct readbuf*)")))	&&(AKA_mark("lis===636###sois===16473###eois===16496###lif===6###soif===318###eoif===341###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_broadcast(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint8(rb, &hop)))) {
		AKA_mark("lis===637###sois===16500###eois===16605###lif===7###soif===345###eoif===450###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_broadcast(struct afb_proto_ws*,struct readbuf*)");protows->client_itf->on_event_broadcast(protows->closure, event_name, object, (unsigned char*)uuid, hop);
	}
	else {
		AKA_mark("lis===639###sois===16614###eois===16651###lif===9###soif===459###eoif===496###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_broadcast(struct afb_proto_ws*,struct readbuf*)");ERROR("Ignoring broadcast of event");
	}

}

/* pushs an event */
/** Instrumented function client_on_event_push(struct afb_proto_ws*,struct readbuf*) */
static void client_on_event_push(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_on_event_push(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===645###sois===16762###eois===16780###lif===2###soif===86###eoif===104###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_push(struct afb_proto_ws*,struct readbuf*)");uint16_t event_id;

		AKA_mark("lis===646###sois===16782###eois===16809###lif===3###soif===106###eoif===133###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_push(struct afb_proto_ws*,struct readbuf*)");struct json_object *object;


		if (AKA_mark("lis===648###sois===16816###eois===16914###lif===5###soif===140###eoif===238###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_push(struct afb_proto_ws*,struct readbuf*)") && (((AKA_mark("lis===648###sois===16816###eois===16850###lif===5###soif===140###eoif===174###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_push(struct afb_proto_ws*,struct readbuf*)")&&protows->client_itf->on_event_push)	&&(AKA_mark("lis===648###sois===16854###eois===16883###lif===5###soif===178###eoif===207###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_push(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint16(rb, &event_id)))	&&(AKA_mark("lis===648###sois===16887###eois===16914###lif===5###soif===211###eoif===238###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_push(struct afb_proto_ws*,struct readbuf*)")&&readbuf_object(rb, &object)))) {
		AKA_mark("lis===649###sois===16918###eois===16989###lif===6###soif===242###eoif===313###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_push(struct afb_proto_ws*,struct readbuf*)");protows->client_itf->on_event_push(protows->closure, event_id, object);
	}
	else {
		AKA_mark("lis===651###sois===16998###eois===17030###lif===8###soif===322###eoif===354###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_event_push(struct afb_proto_ws*,struct readbuf*)");ERROR("Ignoring push of event");
	}

}

/** Instrumented function client_on_reply(struct afb_proto_ws*,struct readbuf*) */
static void client_on_reply(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===656###sois===17115###eois===17140###lif===2###soif===81###eoif===106###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)");struct client_call *call;

		AKA_mark("lis===657###sois===17142###eois===17169###lif===3###soif===108###eoif===135###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)");struct json_object *object;

		AKA_mark("lis===658###sois===17171###eois===17196###lif===4###soif===137###eoif===162###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)");const char *error, *info;


		if (AKA_mark("lis===660###sois===17203###eois===17243###lif===6###soif===169###eoif===209###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)") && (AKA_mark("lis===660###sois===17203###eois===17243###lif===6###soif===169###eoif===209###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)")&&!client_msg_call_get(protows, rb, &call))) {
		AKA_mark("lis===661###sois===17247###eois===17254###lif===7###soif===213###eoif===220###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)");return;
	}
	else {AKA_mark("lis===-660-###sois===-17203-###eois===-1720340-###lif===-6-###soif===-###eoif===-209-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)");}


		if (AKA_mark("lis===663###sois===17261###eois===17367###lif===9###soif===227###eoif===333###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)") && (((AKA_mark("lis===663###sois===17261###eois===17297###lif===9###soif===227###eoif===263###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)")&&readbuf_nullstring(rb, &error, NULL))	&&(AKA_mark("lis===663###sois===17301###eois===17336###lif===9###soif===267###eoif===302###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)")&&readbuf_nullstring(rb, &info, NULL)))	&&(AKA_mark("lis===663###sois===17340###eois===17367###lif===9###soif===306###eoif===333###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)")&&readbuf_object(rb, &object)))) {
				AKA_mark("lis===664###sois===17373###eois===17457###lif===10###soif===339###eoif===423###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)");protows->client_itf->on_reply(protows->closure, call->request, object, error, info);

	}
	else {
				AKA_mark("lis===666###sois===17470###eois===17579###lif===12###soif===436###eoif===545###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)");protows->client_itf->on_reply(protows->closure, call->request, NULL, "proto-error", "can't process success");

	}

		AKA_mark("lis===668###sois===17584###eois===17619###lif===14###soif===550###eoif===585###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_reply(struct afb_proto_ws*,struct readbuf*)");client_call_destroy(protows, call);

}

/** Instrumented function client_on_description(struct afb_proto_ws*,struct readbuf*) */
static void client_on_description(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===673###sois===17710###eois===17726###lif===2###soif===87###eoif===103###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");uint16_t descid;

		AKA_mark("lis===674###sois===17728###eois===17764###lif===3###soif===105###eoif===141###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");struct client_describe *desc, **prv;

		AKA_mark("lis===675###sois===17766###eois===17793###lif===4###soif===143###eoif===170###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");struct json_object *object;


		if (AKA_mark("lis===677###sois===17800###eois===17827###lif===6###soif===177###eoif===204###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)") && (AKA_mark("lis===677###sois===17800###eois===17827###lif===6###soif===177###eoif===204###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint16(rb, &descid))) {
				AKA_mark("lis===678###sois===17833###eois===17869###lif===7###soif===210###eoif===246###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");pthread_mutex_lock(&protows->mutex);

				AKA_mark("lis===679###sois===17872###eois===17898###lif===8###soif===249###eoif===275###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");prv = &protows->describes;

				while (AKA_mark("lis===680###sois===17908###eois===17947###lif===9###soif===285###eoif===324###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)") && ((desc=(*prv) && AKA_mark("lis===680###sois===17909###eois===17920###lif===9###soif===286###eoif===297###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)"))	&&(AKA_mark("lis===680###sois===17925###eois===17947###lif===9###soif===302###eoif===324###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)")&&desc->descid != descid))) {
			AKA_mark("lis===681###sois===17952###eois===17970###lif===10###soif===329###eoif===347###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");prv = &desc->next;
		}

				if (AKA_mark("lis===682###sois===17977###eois===17982###lif===11###soif===354###eoif===359###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)") && (AKA_mark("lis===682###sois===17977###eois===17982###lif===11###soif===354###eoif===359###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)")&&!desc)) {
			AKA_mark("lis===683###sois===17987###eois===18025###lif===12###soif===364###eoif===402###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");pthread_mutex_unlock(&protows->mutex);
		}
		else {
						AKA_mark("lis===685###sois===18038###eois===18056###lif===14###soif===415###eoif===433###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");*prv = desc->next;

						AKA_mark("lis===686###sois===18060###eois===18079###lif===15###soif===437###eoif===456###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");protows->idcount--;

						AKA_mark("lis===687###sois===18083###eois===18121###lif===16###soif===460###eoif===498###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");pthread_mutex_unlock(&protows->mutex);

						if (AKA_mark("lis===688###sois===18129###eois===18157###lif===17###soif===506###eoif===534###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)") && (AKA_mark("lis===688###sois===18129###eois===18157###lif===17###soif===506###eoif===534###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)")&&!readbuf_object(rb, &object))) {
				AKA_mark("lis===689###sois===18163###eois===18177###lif===18###soif===540###eoif===554###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");object = NULL;
			}
			else {AKA_mark("lis===-688-###sois===-18129-###eois===-1812928-###lif===-17-###soif===-###eoif===-534-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");}

						AKA_mark("lis===690###sois===18181###eois===18219###lif===19###soif===558###eoif===596###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");desc->callback(desc->closure, object);

						AKA_mark("lis===691###sois===18223###eois===18234###lif===20###soif===600###eoif===611###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");free(desc);

		}

	}
	else {AKA_mark("lis===-677-###sois===-17800-###eois===-1780027-###lif===-6-###soif===-###eoif===-204-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_description(struct afb_proto_ws*,struct readbuf*)");}

}

/* received a version set */
/** Instrumented function client_on_version_set(struct afb_proto_ws*,struct readbuf*) */
static void client_on_version_set(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_on_version_set(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===699###sois===18361###eois===18377###lif===2###soif===87###eoif===103###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_version_set(struct afb_proto_ws*,struct readbuf*)");uint8_t version;


	/* reads the descid */
		if (AKA_mark("lis===702###sois===18408###eois===18503###lif===5###soif===134###eoif===229###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_version_set(struct afb_proto_ws*,struct readbuf*)") && (((AKA_mark("lis===702###sois===18408###eois===18435###lif===5###soif===134###eoif===161###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_version_set(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint8(rb, &version))	&&(AKA_mark("lis===703###sois===18441###eois===18469###lif===6###soif===167###eoif===195###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_version_set(struct afb_proto_ws*,struct readbuf*)")&&WSAPI_VERSION_MIN <= version))	&&(AKA_mark("lis===704###sois===18475###eois===18503###lif===7###soif===201###eoif===229###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_version_set(struct afb_proto_ws*,struct readbuf*)")&&version <= WSAPI_VERSION_MAX))) {
				AKA_mark("lis===705###sois===18509###eois===18536###lif===8###soif===235###eoif===262###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_version_set(struct afb_proto_ws*,struct readbuf*)");protows->version = version;

				AKA_mark("lis===706###sois===18539###eois===18546###lif===9###soif===265###eoif===272###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_version_set(struct afb_proto_ws*,struct readbuf*)");return;

	}
	else {AKA_mark("lis===-702-###sois===-18408-###eois===-1840895-###lif===-5-###soif===-###eoif===-229-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_version_set(struct afb_proto_ws*,struct readbuf*)");}

		AKA_mark("lis===708###sois===18551###eois===18580###lif===11###soif===277###eoif===306###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_version_set(struct afb_proto_ws*,struct readbuf*)");afb_proto_ws_hangup(protows);

}


/* callback when receiving binary data */
/** Instrumented function client_on_binary_job(int,void*) */
static void client_on_binary_job(int sig, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");AKA_fCall++;
		AKA_mark("lis===715###sois===18687###eois===18719###lif===2###soif===60###eoif===92###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");struct binary *binary = closure;


		if (AKA_mark("lis===717###sois===18726###eois===18730###lif===4###soif===99###eoif===103###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)") && (AKA_mark("lis===717###sois===18726###eois===18730###lif===4###soif===99###eoif===103###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)")&&!sig)) {
				AKA_mark("lis===718###sois===18744###eois===18762###lif===5###soif===117###eoif===135###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");switch(*binary->rb.head++){
					case CHAR_FOR_REPLY: if(*binary->rb.head++ == CHAR_FOR_REPLY)AKA_mark("lis===719###sois===18768###eois===18788###lif===6###soif===141###eoif===161###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");
 /* reply */
						AKA_mark("lis===720###sois===18804###eois===18850###lif===7###soif===177###eoif===223###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");client_on_reply(binary->protows, &binary->rb);

						AKA_mark("lis===721###sois===18854###eois===18860###lif===8###soif===227###eoif===233###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");break;

					case CHAR_FOR_EVT_BROADCAST: if(*binary->rb.head++ == CHAR_FOR_EVT_BROADCAST)AKA_mark("lis===722###sois===18863###eois===18891###lif===9###soif===236###eoif===264###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");
 /* broadcast */
						AKA_mark("lis===723###sois===18911###eois===18967###lif===10###soif===284###eoif===340###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");client_on_event_broadcast(binary->protows, &binary->rb);

						AKA_mark("lis===724###sois===18971###eois===18977###lif===11###soif===344###eoif===350###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");break;

					case CHAR_FOR_EVT_ADD: if(*binary->rb.head++ == CHAR_FOR_EVT_ADD)AKA_mark("lis===725###sois===18980###eois===19002###lif===12###soif===353###eoif===375###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");
 /* creates the event */
						AKA_mark("lis===726###sois===19030###eois===19083###lif===13###soif===403###eoif===456###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");client_on_event_create(binary->protows, &binary->rb);

						AKA_mark("lis===727###sois===19087###eois===19093###lif===14###soif===460###eoif===466###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");break;

					case CHAR_FOR_EVT_DEL: if(*binary->rb.head++ == CHAR_FOR_EVT_DEL)AKA_mark("lis===728###sois===19096###eois===19118###lif===15###soif===469###eoif===491###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");
 /* removes the event */
						AKA_mark("lis===729###sois===19146###eois===19199###lif===16###soif===519###eoif===572###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");client_on_event_remove(binary->protows, &binary->rb);

						AKA_mark("lis===730###sois===19203###eois===19209###lif===17###soif===576###eoif===582###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");break;

					case CHAR_FOR_EVT_PUSH: if(*binary->rb.head++ == CHAR_FOR_EVT_PUSH)AKA_mark("lis===731###sois===19212###eois===19235###lif===18###soif===585###eoif===608###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");
 /* pushs the event */
						AKA_mark("lis===732###sois===19261###eois===19312###lif===19###soif===634###eoif===685###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");client_on_event_push(binary->protows, &binary->rb);

						AKA_mark("lis===733###sois===19316###eois===19322###lif===20###soif===689###eoif===695###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");break;

					case CHAR_FOR_EVT_SUBSCRIBE: if(*binary->rb.head++ == CHAR_FOR_EVT_SUBSCRIBE)AKA_mark("lis===734###sois===19325###eois===19353###lif===21###soif===698###eoif===726###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");
 /* subscribe event for a request */
						AKA_mark("lis===735###sois===19393###eois===19449###lif===22###soif===766###eoif===822###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");client_on_event_subscribe(binary->protows, &binary->rb);

						AKA_mark("lis===736###sois===19453###eois===19459###lif===23###soif===826###eoif===832###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");break;

					case CHAR_FOR_EVT_UNSUBSCRIBE: if(*binary->rb.head++ == CHAR_FOR_EVT_UNSUBSCRIBE)AKA_mark("lis===737###sois===19462###eois===19492###lif===24###soif===835###eoif===865###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");
 /* unsubscribe event for a request */
						AKA_mark("lis===738###sois===19534###eois===19592###lif===25###soif===907###eoif===965###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");client_on_event_unsubscribe(binary->protows, &binary->rb);

						AKA_mark("lis===739###sois===19596###eois===19602###lif===26###soif===969###eoif===975###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");break;

					case CHAR_FOR_DESCRIPTION: if(*binary->rb.head++ == CHAR_FOR_DESCRIPTION)AKA_mark("lis===740###sois===19605###eois===19631###lif===27###soif===978###eoif===1004###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");
 /* description */
						AKA_mark("lis===741###sois===19653###eois===19705###lif===28###soif===1026###eoif===1078###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");client_on_description(binary->protows, &binary->rb);

						AKA_mark("lis===742###sois===19709###eois===19715###lif===29###soif===1082###eoif===1088###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");break;

					case CHAR_FOR_VERSION_SET: if(*binary->rb.head++ == CHAR_FOR_VERSION_SET)AKA_mark("lis===743###sois===19718###eois===19744###lif===30###soif===1091###eoif===1117###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");
 /* set the protocol version */
						AKA_mark("lis===744###sois===19779###eois===19831###lif===31###soif===1152###eoif===1204###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");client_on_version_set(binary->protows, &binary->rb);

						AKA_mark("lis===745###sois===19835###eois===19841###lif===32###soif===1208###eoif===1214###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");break;

					default: if(*binary->rb.head++ != CHAR_FOR_REPLY && *binary->rb.head++ != CHAR_FOR_EVT_BROADCAST && *binary->rb.head++ != CHAR_FOR_EVT_ADD && *binary->rb.head++ != CHAR_FOR_EVT_DEL && *binary->rb.head++ != CHAR_FOR_EVT_PUSH && *binary->rb.head++ != CHAR_FOR_EVT_SUBSCRIBE && *binary->rb.head++ != CHAR_FOR_EVT_UNSUBSCRIBE && *binary->rb.head++ != CHAR_FOR_DESCRIPTION && *binary->rb.head++ != CHAR_FOR_VERSION_SET)AKA_mark("lis===746###sois===19844###eois===19852###lif===33###soif===1217###eoif===1225###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");
 /* unexpected message */
			/* TODO: close the connection */
						AKA_mark("lis===748###sois===19917###eois===19923###lif===35###soif===1290###eoif===1296###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");break;

		}

	}
	else {AKA_mark("lis===-717-###sois===-18726-###eois===-187264-###lif===-4-###soif===-###eoif===-103-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");}

		AKA_mark("lis===751###sois===19932###eois===19954###lif===38###soif===1305###eoif===1327###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");free(binary->rb.base);

		AKA_mark("lis===752###sois===19956###eois===19969###lif===39###soif===1329###eoif===1342###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary_job(int,void*)");free(binary);

}

/* callback when receiving binary data */
/** Instrumented function client_on_binary(void*,char*,size_t) */
static void client_on_binary(void *closure, char *data, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_on_binary(void*,char*,size_t)");AKA_fCall++;
		AKA_mark("lis===758###sois===20087###eois===20126###lif===2###soif===72###eoif===111###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary(void*,char*,size_t)");struct afb_proto_ws *protows = closure;


		AKA_mark("lis===760###sois===20129###eois===20197###lif===4###soif===114###eoif===182###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_on_binary(void*,char*,size_t)");queue_message_processing(protows, data, size, client_on_binary_job);

}

/** Instrumented function client_send_cmd_id16_optstr(struct afb_proto_ws*,char,uint16_t,const char*) */
static int client_send_cmd_id16_optstr(struct afb_proto_ws *protows, char order, uint16_t id, const char *value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/client_send_cmd_id16_optstr(struct afb_proto_ws*,char,uint16_t,const char*)");AKA_fCall++;
		AKA_mark("lis===765###sois===20317###eois===20371###lif===2###soif===116###eoif===170###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_send_cmd_id16_optstr(struct afb_proto_ws*,char,uint16_t,const char*)");struct writebuf wb = { .iovcount = 0, .bufcount = 0 };

		AKA_mark("lis===766###sois===20373###eois===20385###lif===3###soif===172###eoif===184###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_send_cmd_id16_optstr(struct afb_proto_ws*,char,uint16_t,const char*)");int rc = -1;


		if (AKA_mark("lis===768###sois===20392###eois===20492###lif===5###soif===191###eoif===291###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_send_cmd_id16_optstr(struct afb_proto_ws*,char,uint16_t,const char*)") && (((AKA_mark("lis===768###sois===20392###eois===20417###lif===5###soif===191###eoif===216###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_send_cmd_id16_optstr(struct afb_proto_ws*,char,uint16_t,const char*)")&&writebuf_char(&wb, order))	&&(AKA_mark("lis===769###sois===20423###eois===20447###lif===6###soif===222###eoif===246###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_send_cmd_id16_optstr(struct afb_proto_ws*,char,uint16_t,const char*)")&&writebuf_uint16(&wb, id)))	&&((AKA_mark("lis===770###sois===20454###eois===20460###lif===7###soif===253###eoif===259###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_send_cmd_id16_optstr(struct afb_proto_ws*,char,uint16_t,const char*)")&&!value)	||(AKA_mark("lis===770###sois===20464###eois===20491###lif===7###soif===263###eoif===290###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/client_send_cmd_id16_optstr(struct afb_proto_ws*,char,uint16_t,const char*)")&&writebuf_string(&wb, value))))) {
		AKA_mark("lis===771###sois===20496###eois===20527###lif===8###soif===295###eoif===326###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_send_cmd_id16_optstr(struct afb_proto_ws*,char,uint16_t,const char*)");rc = proto_write(protows, &wb);
	}
	else {AKA_mark("lis===-768-###sois===-20392-###eois===-20392100-###lif===-5-###soif===-###eoif===-291-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_send_cmd_id16_optstr(struct afb_proto_ws*,char,uint16_t,const char*)");}

		AKA_mark("lis===772###sois===20529###eois===20539###lif===9###soif===328###eoif===338###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/client_send_cmd_id16_optstr(struct afb_proto_ws*,char,uint16_t,const char*)");return rc;

}

/** Instrumented function afb_proto_ws_client_session_create(struct afb_proto_ws*,uint16_t,const char*) */
int afb_proto_ws_client_session_create(struct afb_proto_ws *protows, uint16_t sessionid, const char *sessionstr)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_session_create(struct afb_proto_ws*,uint16_t,const char*)");AKA_fCall++;
		AKA_mark("lis===777###sois===20659###eois===20748###lif===2###soif===116###eoif===205###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_session_create(struct afb_proto_ws*,uint16_t,const char*)");return client_send_cmd_id16_optstr(protows, CHAR_FOR_SESSION_ADD, sessionid, sessionstr);

}

/** Instrumented function afb_proto_ws_client_session_remove(struct afb_proto_ws*,uint16_t) */
int afb_proto_ws_client_session_remove(struct afb_proto_ws *protows, uint16_t sessionid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_session_remove(struct afb_proto_ws*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===782###sois===20844###eois===20928###lif===2###soif===92###eoif===176###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_session_remove(struct afb_proto_ws*,uint16_t)");return client_send_cmd_id16_optstr(protows, CHAR_FOR_SESSION_DROP, sessionid, NULL);

}

/** Instrumented function afb_proto_ws_client_token_create(struct afb_proto_ws*,uint16_t,const char*) */
int afb_proto_ws_client_token_create(struct afb_proto_ws *protows, uint16_t tokenid, const char *tokenstr)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_token_create(struct afb_proto_ws*,uint16_t,const char*)");AKA_fCall++;
		AKA_mark("lis===787###sois===21042###eois===21125###lif===2###soif===110###eoif===193###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_token_create(struct afb_proto_ws*,uint16_t,const char*)");return client_send_cmd_id16_optstr(protows, CHAR_FOR_TOKEN_ADD, tokenid, tokenstr);


}

/** Instrumented function afb_proto_ws_client_token_remove(struct afb_proto_ws*,uint16_t) */
int afb_proto_ws_client_token_remove(struct afb_proto_ws *protows, uint16_t tokenid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_token_remove(struct afb_proto_ws*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===793###sois===21218###eois===21298###lif===2###soif===88###eoif===168###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_token_remove(struct afb_proto_ws*,uint16_t)");return client_send_cmd_id16_optstr(protows, CHAR_FOR_TOKEN_DROP, tokenid, NULL);

}

/** Instrumented function afb_proto_ws_client_event_unexpected(struct afb_proto_ws*,uint16_t) */
int afb_proto_ws_client_event_unexpected(struct afb_proto_ws *protows, uint16_t eventid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_event_unexpected(struct afb_proto_ws*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===798###sois===21394###eois===21478###lif===2###soif===92###eoif===176###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_event_unexpected(struct afb_proto_ws*,uint16_t)");return client_send_cmd_id16_optstr(protows, CHAR_FOR_EVT_UNEXPECTED, eventid, NULL);

}

/** Instrumented function afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*) */
int afb_proto_ws_client_call(
		struct afb_proto_ws *protows,
		const char *verb,
		struct json_object *args,
		uint16_t sessionid,
		uint16_t tokenid,
		void *request,
		const char *user_creds
)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");AKA_fCall++;
		AKA_mark("lis===811###sois===21681###eois===21693###lif===10###soif===199###eoif===211###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");int rc = -1;

		AKA_mark("lis===812###sois===21695###eois===21720###lif===11###soif===213###eoif===238###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");struct client_call *call;

		AKA_mark("lis===813###sois===21722###eois===21776###lif===12###soif===240###eoif===294###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");struct writebuf wb = { .iovcount = 0, .bufcount = 0 };

		AKA_mark("lis===814###sois===21778###eois===21790###lif===13###soif===296###eoif===308###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");uint16_t id;


	/* allocate call data */
		AKA_mark("lis===817###sois===21819###eois===21847###lif===16###soif===337###eoif===365###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");call = malloc(sizeof *call);

		if (AKA_mark("lis===818###sois===21853###eois===21865###lif===17###soif===371###eoif===383###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)") && (AKA_mark("lis===818###sois===21853###eois===21865###lif===17###soif===371###eoif===383###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)")&&call == NULL)) {
				AKA_mark("lis===819###sois===21871###eois===21886###lif===18###soif===389###eoif===404###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");errno = ENOMEM;

				AKA_mark("lis===820###sois===21889###eois===21899###lif===19###soif===407###eoif===417###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");return -1;

	}
	else {AKA_mark("lis===-818-###sois===-21853-###eois===-2185312-###lif===-17-###soif===-###eoif===-383-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");}

		AKA_mark("lis===822###sois===21904###eois===21928###lif===21###soif===422###eoif===446###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");call->request = request;


	/* init call data */
		AKA_mark("lis===825###sois===21953###eois===21989###lif===24###soif===471###eoif===507###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");pthread_mutex_lock(&protows->mutex);

		if (AKA_mark("lis===826###sois===21995###eois===22028###lif===25###soif===513###eoif===546###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)") && (AKA_mark("lis===826###sois===21995###eois===22028###lif===25###soif===513###eoif===546###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)")&&protows->idcount >= ACTIVE_ID_MAX)) {
				AKA_mark("lis===827###sois===22034###eois===22072###lif===26###soif===552###eoif===590###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");pthread_mutex_unlock(&protows->mutex);

				AKA_mark("lis===828###sois===22075###eois===22089###lif===27###soif===593###eoif===607###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");errno = EBUSY;

				AKA_mark("lis===829###sois===22092###eois===22103###lif===28###soif===610###eoif===621###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");goto clean;

	}
	else {AKA_mark("lis===-826-###sois===-21995-###eois===-2199533-###lif===-25-###soif===-###eoif===-546-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");}

		AKA_mark("lis===831###sois===22108###eois===22127###lif===30###soif===626###eoif===645###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");protows->idcount++;

		AKA_mark("lis===832###sois===22129###eois===22151###lif===31###soif===647###eoif===669###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");id = ++protows->genid;

		while (AKA_mark("lis===833###sois===22159###eois===22212###lif===32###soif===677###eoif===730###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)") && ((AKA_mark("lis===833###sois===22159###eois===22162###lif===32###soif===677###eoif===680###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)")&&!id)	||(AKA_mark("lis===833###sois===22166###eois===22212###lif===32###soif===684###eoif===730###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)")&&client_call_search_locked(protows, id) != NULL))) {
		AKA_mark("lis===834###sois===22216###eois===22221###lif===33###soif===734###eoif===739###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");id++;
	}

		AKA_mark("lis===835###sois===22223###eois===22258###lif===34###soif===741###eoif===776###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");call->callid = protows->genid = id;

		AKA_mark("lis===836###sois===22260###eois===22288###lif===35###soif===778###eoif===806###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");call->next = protows->calls;

		AKA_mark("lis===837###sois===22290###eois===22312###lif===36###soif===808###eoif===830###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");protows->calls = call;

		AKA_mark("lis===838###sois===22314###eois===22352###lif===37###soif===832###eoif===870###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");pthread_mutex_unlock(&protows->mutex);


	/* creates the call message */
		if (AKA_mark("lis===841###sois===22391###eois===22649###lif===40###soif===909###eoif===1167###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)") && (((((((AKA_mark("lis===841###sois===22391###eois===22425###lif===40###soif===909###eoif===943###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)")&&!writebuf_char(&wb, CHAR_FOR_CALL))	||(AKA_mark("lis===842###sois===22431###eois===22466###lif===41###soif===949###eoif===984###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)")&&!writebuf_uint16(&wb, call->callid)))	||(AKA_mark("lis===843###sois===22472###eois===22499###lif===42###soif===990###eoif===1017###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)")&&!writebuf_string(&wb, verb)))	||(AKA_mark("lis===844###sois===22505###eois===22537###lif===43###soif===1023###eoif===1055###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)")&&!writebuf_uint16(&wb, sessionid)))	||(AKA_mark("lis===845###sois===22543###eois===22573###lif===44###soif===1061###eoif===1091###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)")&&!writebuf_uint16(&wb, tokenid)))	||(AKA_mark("lis===846###sois===22579###eois===22606###lif===45###soif===1097###eoif===1124###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)")&&!writebuf_object(&wb, args)))	||(AKA_mark("lis===847###sois===22612###eois===22649###lif===46###soif===1130###eoif===1167###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)")&&!writebuf_nullstring(&wb, user_creds)))) {
				AKA_mark("lis===848###sois===22655###eois===22670###lif===47###soif===1173###eoif===1188###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");errno = EINVAL;

				AKA_mark("lis===849###sois===22673###eois===22684###lif===48###soif===1191###eoif===1202###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");goto clean;

	}
	else {AKA_mark("lis===-841-###sois===-22391-###eois===-22391258-###lif===-40-###soif===-###eoif===-1167-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");}


	/* send */
		AKA_mark("lis===853###sois===22702###eois===22733###lif===52###soif===1220###eoif===1251###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");rc = proto_write(protows, &wb);

		if (AKA_mark("lis===854###sois===22739###eois===22742###lif===53###soif===1257###eoif===1260###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)") && (AKA_mark("lis===854###sois===22739###eois===22742###lif===53###soif===1257###eoif===1260###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)")&&!rc)) {
		AKA_mark("lis===855###sois===22746###eois===22755###lif===54###soif===1264###eoif===1273###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");goto end;
	}
	else {AKA_mark("lis===-854-###sois===-22739-###eois===-227393-###lif===-53-###soif===-###eoif===-1260-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");}


	clean:
	AKA_mark("lis===858###sois===22765###eois===22800###lif===57###soif===1283###eoif===1318###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");client_call_destroy(protows, call);

	end:
	AKA_mark("lis===860###sois===22807###eois===22817###lif===59###soif===1325###eoif===1335###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_call(struct afb_proto_ws*,const char*,struct json_object*,uint16_t,uint16_t,void*,const char*)");return rc;

}

/* get the description */
/** Instrumented function afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*) */
int afb_proto_ws_client_describe(struct afb_proto_ws *protows, void (*callback)(void*, struct json_object*), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");AKA_fCall++;
		AKA_mark("lis===866###sois===22974###eois===23007###lif===2###soif===127###eoif===160###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");struct client_describe *desc, *d;

		AKA_mark("lis===867###sois===23009###eois===23063###lif===3###soif===162###eoif===216###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");struct writebuf wb = { .iovcount = 0, .bufcount = 0 };

		AKA_mark("lis===868###sois===23065###eois===23077###lif===4###soif===218###eoif===230###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");uint16_t id;


		AKA_mark("lis===870###sois===23080###eois===23108###lif===6###soif===233###eoif===261###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");desc = malloc(sizeof *desc);

		if (AKA_mark("lis===871###sois===23114###eois===23119###lif===7###soif===267###eoif===272###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)") && (AKA_mark("lis===871###sois===23114###eois===23119###lif===7###soif===267###eoif===272###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)")&&!desc)) {
				AKA_mark("lis===872###sois===23125###eois===23140###lif===8###soif===278###eoif===293###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");errno = ENOMEM;

				AKA_mark("lis===873###sois===23143###eois===23154###lif===9###soif===296###eoif===307###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");goto error;

	}
	else {AKA_mark("lis===-871-###sois===-23114-###eois===-231145-###lif===-7-###soif===-###eoif===-272-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");}


	/* fill in stack the description of the task */
		AKA_mark("lis===877###sois===23209###eois===23245###lif===13###soif===362###eoif===398###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");pthread_mutex_lock(&protows->mutex);

		if (AKA_mark("lis===878###sois===23251###eois===23284###lif===14###soif===404###eoif===437###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)") && (AKA_mark("lis===878###sois===23251###eois===23284###lif===14###soif===404###eoif===437###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)")&&protows->idcount >= ACTIVE_ID_MAX)) {
				AKA_mark("lis===879###sois===23290###eois===23304###lif===15###soif===443###eoif===457###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");errno = EBUSY;

				AKA_mark("lis===880###sois===23307###eois===23317###lif===16###soif===460###eoif===470###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");goto busy;

	}
	else {AKA_mark("lis===-878-###sois===-23251-###eois===-2325133-###lif===-14-###soif===-###eoif===-437-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");}

		AKA_mark("lis===882###sois===23322###eois===23341###lif===18###soif===475###eoif===494###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");protows->idcount++;

		AKA_mark("lis===883###sois===23343###eois===23365###lif===19###soif===496###eoif===518###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");id = ++protows->genid;

		AKA_mark("lis===884###sois===23367###eois===23390###lif===20###soif===520###eoif===543###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");d = protows->describes;

		while (AKA_mark("lis===885###sois===23399###eois===23400###lif===21###soif===552###eoif===553###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)") && (AKA_mark("lis===885###sois===23399###eois===23400###lif===21###soif===552###eoif===553###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)")&&d)) {
				if (AKA_mark("lis===886###sois===23410###eois===23431###lif===22###soif===563###eoif===584###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)") && ((AKA_mark("lis===886###sois===23410###eois===23412###lif===22###soif===563###eoif===565###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)")&&id)	&&(AKA_mark("lis===886###sois===23416###eois===23431###lif===22###soif===569###eoif===584###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)")&&d->descid != id))) {
			AKA_mark("lis===887###sois===23436###eois===23448###lif===23###soif===589###eoif===601###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");d = d->next;
		}
		else {
						AKA_mark("lis===889###sois===23461###eois===23466###lif===25###soif===614###eoif===619###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");id++;

						AKA_mark("lis===890###sois===23470###eois===23493###lif===26###soif===623###eoif===646###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");d = protows->describes;

		}

	}

		AKA_mark("lis===893###sois===23502###eois===23537###lif===29###soif===655###eoif===690###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");desc->descid = protows->genid = id;

		AKA_mark("lis===894###sois===23539###eois===23565###lif===30###soif===692###eoif===718###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");desc->callback = callback;

		AKA_mark("lis===895###sois===23567###eois===23591###lif===31###soif===720###eoif===744###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");desc->closure = closure;

		AKA_mark("lis===896###sois===23593###eois===23625###lif===32###soif===746###eoif===778###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");desc->next = protows->describes;

		AKA_mark("lis===897###sois===23627###eois===23653###lif===33###soif===780###eoif===806###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");protows->describes = desc;

		AKA_mark("lis===898###sois===23655###eois===23693###lif===34###soif===808###eoif===846###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");pthread_mutex_unlock(&protows->mutex);


	/* send */
		if (AKA_mark("lis===901###sois===23712###eois===23791###lif===37###soif===865###eoif===944###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)") && ((AKA_mark("lis===901###sois===23712###eois===23750###lif===37###soif===865###eoif===903###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)")&&!writebuf_char(&wb, CHAR_FOR_DESCRIBE))	||(AKA_mark("lis===902###sois===23756###eois===23791###lif===38###soif===909###eoif===944###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)")&&!writebuf_uint16(&wb, desc->descid)))) {
		 		AKA_mark("lis===903###sois===23798###eois===23813###lif===39###soif===951###eoif===966###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");errno = EINVAL;

		 		AKA_mark("lis===904###sois===23817###eois===23829###lif===40###soif===970###eoif===982###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");goto error2;

	}
	else {AKA_mark("lis===-901-###sois===-23712-###eois===-2371279-###lif===-37-###soif===-###eoif===-944-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");}


		if (AKA_mark("lis===907###sois===23839###eois===23869###lif===43###soif===992###eoif===1022###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)") && (AKA_mark("lis===907###sois===23839###eois===23869###lif===43###soif===992###eoif===1022###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)")&&proto_write(protows, &wb) == 0)) {
		AKA_mark("lis===908###sois===23873###eois===23882###lif===44###soif===1026###eoif===1035###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");return 0;
	}
	else {AKA_mark("lis===-907-###sois===-23839-###eois===-2383930-###lif===-43-###soif===-###eoif===-1022-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");}


	error2:
	AKA_mark("lis===911###sois===23893###eois===23929###lif===47###soif===1046###eoif===1082###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");pthread_mutex_lock(&protows->mutex);

		AKA_mark("lis===912###sois===23931###eois===23954###lif===48###soif===1084###eoif===1107###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");d = protows->describes;

		if (AKA_mark("lis===913###sois===23960###eois===23969###lif===49###soif===1113###eoif===1122###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)") && (AKA_mark("lis===913###sois===23960###eois===23969###lif===49###soif===1113###eoif===1122###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)")&&d == desc)) {
		AKA_mark("lis===914###sois===23973###eois===24005###lif===50###soif===1126###eoif===1158###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");protows->describes = desc->next;
	}
	else {
				while (AKA_mark("lis===916###sois===24022###eois===24042###lif===52###soif===1175###eoif===1195###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)") && ((AKA_mark("lis===916###sois===24022###eois===24023###lif===52###soif===1175###eoif===1176###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)")&&d)	&&(AKA_mark("lis===916###sois===24027###eois===24042###lif===52###soif===1180###eoif===1195###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)")&&d->next != desc))) {
			AKA_mark("lis===917###sois===24047###eois===24059###lif===53###soif===1200###eoif===1212###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");d = d->next;
		}

				if (AKA_mark("lis===918###sois===24066###eois===24067###lif===54###soif===1219###eoif===1220###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)") && (AKA_mark("lis===918###sois===24066###eois===24067###lif===54###soif===1219###eoif===1220###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)")&&d)) {
			AKA_mark("lis===919###sois===24072###eois===24093###lif===55###soif===1225###eoif===1246###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");d->next = desc->next;
		}
		else {AKA_mark("lis===-918-###sois===-24066-###eois===-240661-###lif===-54-###soif===-###eoif===-1220-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");}

	}

		AKA_mark("lis===921###sois===24098###eois===24117###lif===57###soif===1251###eoif===1270###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");protows->idcount--;

	busy:
	AKA_mark("lis===923###sois===24125###eois===24163###lif===59###soif===1278###eoif===1316###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");pthread_mutex_unlock(&protows->mutex);

		AKA_mark("lis===924###sois===24165###eois===24176###lif===60###soif===1318###eoif===1329###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");free(desc);

	error:
	/* TODO? callback(closure, NULL); */
	AKA_mark("lis===927###sois===24223###eois===24233###lif===63###soif===1376###eoif===1386###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_client_describe(struct afb_proto_ws*,void(*callback)(void*, struct json_object*),void*)");return -1;

}

/******************* client description part for server *****************************/

/* on call, propagate it to the ws service */
/** Instrumented function server_on_call(struct afb_proto_ws*,struct readbuf*) */
static void server_on_call(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===935###sois===24451###eois===24482###lif===2###soif===80###eoif===111###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");struct afb_proto_ws_call *call;

		AKA_mark("lis===936###sois===24484###eois===24514###lif===3###soif===113###eoif===143###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");const char *verb, *user_creds;

		AKA_mark("lis===937###sois===24516###eois===24552###lif===4###soif===145###eoif===181###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");uint16_t callid, sessionid, tokenid;

		AKA_mark("lis===938###sois===24554###eois===24569###lif===5###soif===183###eoif===198###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");size_t lenverb;

		AKA_mark("lis===939###sois===24571###eois===24598###lif===6###soif===200###eoif===227###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");struct json_object *object;


		AKA_mark("lis===941###sois===24601###eois===24630###lif===8###soif===230###eoif===259###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");afb_proto_ws_addref(protows);


	/* reads the call message data */
		if (AKA_mark("lis===944###sois===24672###eois===24896###lif===11###soif===301###eoif===525###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)") && ((((((AKA_mark("lis===944###sois===24672###eois===24700###lif===11###soif===301###eoif===329###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)")&&!readbuf_uint16(rb, &callid))	||(AKA_mark("lis===945###sois===24706###eois===24742###lif===12###soif===335###eoif===371###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)")&&!readbuf_string(rb, &verb, &lenverb)))	||(AKA_mark("lis===946###sois===24748###eois===24779###lif===13###soif===377###eoif===408###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)")&&!readbuf_uint16(rb, &sessionid)))	||(AKA_mark("lis===947###sois===24785###eois===24814###lif===14###soif===414###eoif===443###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)")&&!readbuf_uint16(rb, &tokenid)))	||(AKA_mark("lis===948###sois===24820###eois===24848###lif===15###soif===449###eoif===477###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)")&&!readbuf_object(rb, &object)))	||(AKA_mark("lis===949###sois===24854###eois===24896###lif===16###soif===483###eoif===525###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)")&&!readbuf_nullstring(rb, &user_creds, NULL)))) {
		AKA_mark("lis===950###sois===24900###eois===24914###lif===17###soif===529###eoif===543###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");goto overflow;
	}
	else {AKA_mark("lis===-944-###sois===-24672-###eois===-24672224-###lif===-11-###soif===-###eoif===-525-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");}


	/* create the request */
		AKA_mark("lis===953###sois===24943###eois===24971###lif===20###soif===572###eoif===600###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");call = malloc(sizeof *call);

		if (AKA_mark("lis===954###sois===24977###eois===24989###lif===21###soif===606###eoif===618###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)") && (AKA_mark("lis===954###sois===24977###eois===24989###lif===21###soif===606###eoif===618###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)")&&call == NULL)) {
		AKA_mark("lis===955###sois===24993###eois===25012###lif===22###soif===622###eoif===641###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");goto out_of_memory;
	}
	else {AKA_mark("lis===-954-###sois===-24977-###eois===-2497712-###lif===-21-###soif===-###eoif===-618-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");}


		AKA_mark("lis===957###sois===25015###eois===25039###lif===24###soif===644###eoif===668###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");call->protows = protows;

		AKA_mark("lis===958###sois===25041###eois===25063###lif===25###soif===670###eoif===692###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");call->callid = callid;

		AKA_mark("lis===959###sois===25065###eois===25084###lif===26###soif===694###eoif===713###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");call->refcount = 1;

		AKA_mark("lis===960###sois===25086###eois===25110###lif===27###soif===715###eoif===739###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");call->buffer = rb->base;

		AKA_mark("lis===961###sois===25112###eois===25128###lif===28###soif===741###eoif===757###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");rb->base = NULL;
 /* don't free the buffer */

		AKA_mark("lis===963###sois===25159###eois===25258###lif===30###soif===788###eoif===887###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");protows->server_itf->on_call(protows->closure, call, verb, object, sessionid, tokenid, user_creds);

		AKA_mark("lis===964###sois===25260###eois===25267###lif===31###soif===889###eoif===896###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");return;


	out_of_memory:
	AKA_mark("lis===967###sois===25285###eois===25309###lif===34###soif===914###eoif===938###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");json_object_put(object);


	overflow:
	AKA_mark("lis===970###sois===25322###eois===25350###lif===37###soif===951###eoif===979###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_call(struct afb_proto_ws*,struct readbuf*)");afb_proto_ws_unref(protows);

}

/** Instrumented function server_send_description(struct afb_proto_ws*,uint16_t,struct json_object*) */
static int server_send_description(struct afb_proto_ws *protows, uint16_t descid, struct json_object *descobj)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/server_send_description(struct afb_proto_ws*,uint16_t,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===975###sois===25468###eois===25480###lif===2###soif===114###eoif===126###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_send_description(struct afb_proto_ws*,uint16_t,struct json_object*)");int rc = -1;

		AKA_mark("lis===976###sois===25482###eois===25536###lif===3###soif===128###eoif===182###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_send_description(struct afb_proto_ws*,uint16_t,struct json_object*)");struct writebuf wb = { .iovcount = 0, .bufcount = 0 };


		if (AKA_mark("lis===978###sois===25543###eois===25652###lif===5###soif===189###eoif===298###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_send_description(struct afb_proto_ws*,uint16_t,struct json_object*)") && (((AKA_mark("lis===978###sois===25543###eois===25583###lif===5###soif===189###eoif===229###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_send_description(struct afb_proto_ws*,uint16_t,struct json_object*)")&&writebuf_char(&wb, CHAR_FOR_DESCRIPTION))	&&(AKA_mark("lis===979###sois===25589###eois===25617###lif===6###soif===235###eoif===263###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_send_description(struct afb_proto_ws*,uint16_t,struct json_object*)")&&writebuf_uint16(&wb, descid)))	&&(AKA_mark("lis===980###sois===25623###eois===25652###lif===7###soif===269###eoif===298###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_send_description(struct afb_proto_ws*,uint16_t,struct json_object*)")&&writebuf_object(&wb, descobj)))) {
		AKA_mark("lis===981###sois===25656###eois===25687###lif===8###soif===302###eoif===333###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_send_description(struct afb_proto_ws*,uint16_t,struct json_object*)");rc = proto_write(protows, &wb);
	}
	else {AKA_mark("lis===-978-###sois===-25543-###eois===-25543109-###lif===-5-###soif===-###eoif===-298-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_send_description(struct afb_proto_ws*,uint16_t,struct json_object*)");}

		AKA_mark("lis===982###sois===25689###eois===25699###lif===9###soif===335###eoif===345###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_send_description(struct afb_proto_ws*,uint16_t,struct json_object*)");return rc;

}

/** Instrumented function afb_proto_ws_describe_put(struct afb_proto_ws_describe*,struct json_object*) */
int afb_proto_ws_describe_put(struct afb_proto_ws_describe *describe, struct json_object *description)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_describe_put(struct afb_proto_ws_describe*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===987###sois===25809###eois===25892###lif===2###soif===106###eoif===189###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_describe_put(struct afb_proto_ws_describe*,struct json_object*)");int rc = server_send_description(describe->protows, describe->descid, description);

		AKA_mark("lis===988###sois===25894###eois===25932###lif===3###soif===191###eoif===229###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_describe_put(struct afb_proto_ws_describe*,struct json_object*)");afb_proto_ws_unref(describe->protows);

		AKA_mark("lis===989###sois===25934###eois===25949###lif===4###soif===231###eoif===246###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_describe_put(struct afb_proto_ws_describe*,struct json_object*)");free(describe);

		AKA_mark("lis===990###sois===25951###eois===25961###lif===5###soif===248###eoif===258###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_describe_put(struct afb_proto_ws_describe*,struct json_object*)");return rc;

}

/* on describe, propagate it to the ws service */
/** Instrumented function server_on_describe(struct afb_proto_ws*,struct readbuf*) */
static void server_on_describe(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===996###sois===26099###eois===26115###lif===2###soif===84###eoif===100###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)");uint16_t descid;

		AKA_mark("lis===997###sois===26117###eois===26152###lif===3###soif===102###eoif===137###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)");struct afb_proto_ws_describe *desc;


	/* reads the descid */
		if (AKA_mark("lis===1000###sois===26183###eois===26210###lif===6###soif===168###eoif===195###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)") && (AKA_mark("lis===1000###sois===26183###eois===26210###lif===6###soif===168###eoif===195###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint16(rb, &descid))) {
				if (AKA_mark("lis===1001###sois===26220###eois===26252###lif===7###soif===205###eoif===237###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)") && (AKA_mark("lis===1001###sois===26220###eois===26252###lif===7###soif===205###eoif===237###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)")&&protows->server_itf->on_describe)) {
			/* create asynchronous job */
						AKA_mark("lis===1003###sois===26292###eois===26320###lif===9###soif===277###eoif===305###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)");desc = malloc(sizeof *desc);

						if (AKA_mark("lis===1004###sois===26328###eois===26332###lif===10###soif===313###eoif===317###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)") && (AKA_mark("lis===1004###sois===26328###eois===26332###lif===10###soif===313###eoif===317###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)")&&desc)) {
								AKA_mark("lis===1005###sois===26340###eois===26362###lif===11###soif===325###eoif===347###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)");desc->descid = descid;

								AKA_mark("lis===1006###sois===26367###eois===26391###lif===12###soif===352###eoif===376###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)");desc->protows = protows;

								AKA_mark("lis===1007###sois===26396###eois===26425###lif===13###soif===381###eoif===410###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)");afb_proto_ws_addref(protows);

								AKA_mark("lis===1008###sois===26430###eois===26487###lif===14###soif===415###eoif===472###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)");protows->server_itf->on_describe(protows->closure, desc);

								AKA_mark("lis===1009###sois===26492###eois===26499###lif===15###soif===477###eoif===484###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)");return;

			}
			else {AKA_mark("lis===-1004-###sois===-26328-###eois===-263284-###lif===-10-###soif===-###eoif===-317-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)");}

		}
		else {AKA_mark("lis===-1001-###sois===-26220-###eois===-2622032-###lif===-7-###soif===-###eoif===-237-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)");}

				AKA_mark("lis===1012###sois===26511###eois===26558###lif===18###soif===496###eoif===543###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)");server_send_description(protows, descid, NULL);

	}
	else {AKA_mark("lis===-1000-###sois===-26183-###eois===-2618327-###lif===-6-###soif===-###eoif===-195-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_describe(struct afb_proto_ws*,struct readbuf*)");}

}

/** Instrumented function server_on_session_add(struct afb_proto_ws*,struct readbuf*) */
static void server_on_session_add(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/server_on_session_add(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===1018###sois===26652###eois===26671###lif===2###soif===87###eoif===106###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_session_add(struct afb_proto_ws*,struct readbuf*)");uint16_t sessionid;

		AKA_mark("lis===1019###sois===26673###eois===26696###lif===3###soif===108###eoif===131###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_session_add(struct afb_proto_ws*,struct readbuf*)");const char *sessionstr;


		if (AKA_mark("lis===1021###sois===26703###eois===26774###lif===5###soif===138###eoif===209###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_session_add(struct afb_proto_ws*,struct readbuf*)") && ((AKA_mark("lis===1021###sois===26703###eois===26733###lif===5###soif===138###eoif===168###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_session_add(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint16(rb, &sessionid))	&&(AKA_mark("lis===1021###sois===26737###eois===26774###lif===5###soif===172###eoif===209###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_session_add(struct afb_proto_ws*,struct readbuf*)")&&readbuf_string(rb, &sessionstr, NULL)))) {
		AKA_mark("lis===1022###sois===26778###eois===26858###lif===6###soif===213###eoif===293###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_session_add(struct afb_proto_ws*,struct readbuf*)");protows->server_itf->on_session_create(protows->closure, sessionid, sessionstr);
	}
	else {AKA_mark("lis===-1021-###sois===-26703-###eois===-2670371-###lif===-5-###soif===-###eoif===-209-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_session_add(struct afb_proto_ws*,struct readbuf*)");}

}

/** Instrumented function server_on_session_drop(struct afb_proto_ws*,struct readbuf*) */
static void server_on_session_drop(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/server_on_session_drop(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===1027###sois===26950###eois===26969###lif===2###soif===88###eoif===107###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_session_drop(struct afb_proto_ws*,struct readbuf*)");uint16_t sessionid;


		if (AKA_mark("lis===1029###sois===26976###eois===27006###lif===4###soif===114###eoif===144###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_session_drop(struct afb_proto_ws*,struct readbuf*)") && (AKA_mark("lis===1029###sois===26976###eois===27006###lif===4###soif===114###eoif===144###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_session_drop(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint16(rb, &sessionid))) {
		AKA_mark("lis===1030###sois===27010###eois===27078###lif===5###soif===148###eoif===216###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_session_drop(struct afb_proto_ws*,struct readbuf*)");protows->server_itf->on_session_remove(protows->closure, sessionid);
	}
	else {AKA_mark("lis===-1029-###sois===-26976-###eois===-2697630-###lif===-4-###soif===-###eoif===-144-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_session_drop(struct afb_proto_ws*,struct readbuf*)");}

}

/** Instrumented function server_on_token_add(struct afb_proto_ws*,struct readbuf*) */
static void server_on_token_add(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/server_on_token_add(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===1035###sois===27167###eois===27184###lif===2###soif===85###eoif===102###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_token_add(struct afb_proto_ws*,struct readbuf*)");uint16_t tokenid;

		AKA_mark("lis===1036###sois===27186###eois===27207###lif===3###soif===104###eoif===125###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_token_add(struct afb_proto_ws*,struct readbuf*)");const char *tokenstr;


		if (AKA_mark("lis===1038###sois===27214###eois===27281###lif===5###soif===132###eoif===199###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_token_add(struct afb_proto_ws*,struct readbuf*)") && ((AKA_mark("lis===1038###sois===27214###eois===27242###lif===5###soif===132###eoif===160###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_token_add(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint16(rb, &tokenid))	&&(AKA_mark("lis===1038###sois===27246###eois===27281###lif===5###soif===164###eoif===199###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_token_add(struct afb_proto_ws*,struct readbuf*)")&&readbuf_string(rb, &tokenstr, NULL)))) {
		AKA_mark("lis===1039###sois===27285###eois===27359###lif===6###soif===203###eoif===277###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_token_add(struct afb_proto_ws*,struct readbuf*)");protows->server_itf->on_token_create(protows->closure, tokenid, tokenstr);
	}
	else {AKA_mark("lis===-1038-###sois===-27214-###eois===-2721467-###lif===-5-###soif===-###eoif===-199-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_token_add(struct afb_proto_ws*,struct readbuf*)");}

}

/** Instrumented function server_on_token_drop(struct afb_proto_ws*,struct readbuf*) */
static void server_on_token_drop(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/server_on_token_drop(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===1044###sois===27449###eois===27466###lif===2###soif===86###eoif===103###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_token_drop(struct afb_proto_ws*,struct readbuf*)");uint16_t tokenid;


		if (AKA_mark("lis===1046###sois===27473###eois===27501###lif===4###soif===110###eoif===138###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_token_drop(struct afb_proto_ws*,struct readbuf*)") && (AKA_mark("lis===1046###sois===27473###eois===27501###lif===4###soif===110###eoif===138###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_token_drop(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint16(rb, &tokenid))) {
		AKA_mark("lis===1047###sois===27505###eois===27569###lif===5###soif===142###eoif===206###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_token_drop(struct afb_proto_ws*,struct readbuf*)");protows->server_itf->on_token_remove(protows->closure, tokenid);
	}
	else {AKA_mark("lis===-1046-###sois===-27473-###eois===-2747328-###lif===-4-###soif===-###eoif===-138-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_token_drop(struct afb_proto_ws*,struct readbuf*)");}

}

/** Instrumented function server_on_event_unexpected(struct afb_proto_ws*,struct readbuf*) */
static void server_on_event_unexpected(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/server_on_event_unexpected(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===1052###sois===27665###eois===27682###lif===2###soif===92###eoif===109###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_event_unexpected(struct afb_proto_ws*,struct readbuf*)");uint16_t eventid;


		if (AKA_mark("lis===1054###sois===27689###eois===27717###lif===4###soif===116###eoif===144###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_event_unexpected(struct afb_proto_ws*,struct readbuf*)") && (AKA_mark("lis===1054###sois===27689###eois===27717###lif===4###soif===116###eoif===144###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_event_unexpected(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint16(rb, &eventid))) {
		AKA_mark("lis===1055###sois===27721###eois===27789###lif===5###soif===148###eoif===216###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_event_unexpected(struct afb_proto_ws*,struct readbuf*)");protows->server_itf->on_event_unexpected(protows->closure, eventid);
	}
	else {AKA_mark("lis===-1054-###sois===-27689-###eois===-2768928-###lif===-4-###soif===-###eoif===-144-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_event_unexpected(struct afb_proto_ws*,struct readbuf*)");}

}

/* on version offer */
/** Instrumented function server_on_version_offer(struct afb_proto_ws*,struct readbuf*) */
static void server_on_version_offer(struct afb_proto_ws *protows, struct readbuf *rb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");AKA_fCall++;
		AKA_mark("lis===1061###sois===27905###eois===27919###lif===2###soif===89###eoif===103###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");uint8_t count;

		AKA_mark("lis===1062###sois===27921###eois===27939###lif===3###soif===105###eoif===123###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");uint8_t *versions;

		AKA_mark("lis===1063###sois===27941###eois===27957###lif===4###soif===125###eoif===141###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");uint8_t version;

		AKA_mark("lis===1064###sois===27959###eois===27969###lif===5###soif===143###eoif===153###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");uint8_t v;

		AKA_mark("lis===1065###sois===27971###eois===27983###lif===6###soif===155###eoif===167###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");uint32_t id;


	/* reads the descid */
		if (AKA_mark("lis===1068###sois===28014###eois===28172###lif===9###soif===198###eoif===356###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)") && (((((AKA_mark("lis===1068###sois===28014###eois===28037###lif===9###soif===198###eoif===221###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint32(rb, &id))	&&(AKA_mark("lis===1069###sois===28043###eois===28065###lif===10###soif===227###eoif===249###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)")&&id == WSAPI_IDENTIFIER))	&&(AKA_mark("lis===1070###sois===28071###eois===28096###lif===11###soif===255###eoif===280###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)")&&readbuf_uint8(rb, &count)))	&&(AKA_mark("lis===1071###sois===28102###eois===28111###lif===12###soif===286###eoif===295###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)")&&count > 0))	&&(versions=((uint8_t*)readbuf_get(rb, (uint32_t)count)) && AKA_mark("lis===1072###sois===28118###eois===28171###lif===13###soif===302###eoif===355###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)")))) {
				AKA_mark("lis===1073###sois===28178###eois===28208###lif===14###soif===362###eoif===392###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");version = WSAPI_VERSION_UNSET;

				while (AKA_mark("lis===1074###sois===28218###eois===28223###lif===15###soif===402###eoif===407###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)") && (AKA_mark("lis===1074###sois===28218###eois===28223###lif===15###soif===402###eoif===407###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)")&&count)) {
						AKA_mark("lis===1075###sois===28230###eois===28252###lif===16###soif===414###eoif===436###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");v = versions[--count];

						if (AKA_mark("lis===1076###sois===28260###eois===28367###lif===17###soif===444###eoif===551###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)") && (((AKA_mark("lis===1076###sois===28260###eois===28282###lif===17###soif===444###eoif===466###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)")&&v >= WSAPI_VERSION_MIN)	&&(AKA_mark("lis===1077###sois===28290###eois===28312###lif===18###soif===474###eoif===496###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)")&&v <= WSAPI_VERSION_MAX))	&&((AKA_mark("lis===1078###sois===28321###eois===28351###lif===19###soif===505###eoif===535###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)")&&version == WSAPI_VERSION_UNSET)	||(AKA_mark("lis===1078###sois===28355###eois===28366###lif===19###soif===539###eoif===550###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)")&&version < v)))) {
				AKA_mark("lis===1079###sois===28373###eois===28385###lif===20###soif===557###eoif===569###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");version = v;
			}
			else {AKA_mark("lis===-1076-###sois===-28260-###eois===-28260107-###lif===-17-###soif===-###eoif===-551-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");}

		}

				if (AKA_mark("lis===1081###sois===28396###eois===28426###lif===22###soif===580###eoif===610###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)") && (AKA_mark("lis===1081###sois===28396###eois===28426###lif===22###soif===580###eoif===610###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)")&&version != WSAPI_VERSION_UNSET)) {
						if (AKA_mark("lis===1082###sois===28437###eois===28476###lif===23###soif===621###eoif===660###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)") && (AKA_mark("lis===1082###sois===28437###eois===28476###lif===23###soif===621###eoif===660###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)")&&send_version_set(protows, version) >= 0)) {
								AKA_mark("lis===1083###sois===28484###eois===28511###lif===24###soif===668###eoif===695###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");protows->version = version;

								AKA_mark("lis===1084###sois===28516###eois===28523###lif===25###soif===700###eoif===707###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");return;

			}
			else {AKA_mark("lis===-1082-###sois===-28437-###eois===-2843739-###lif===-23-###soif===-###eoif===-660-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");}

		}
		else {AKA_mark("lis===-1081-###sois===-28396-###eois===-2839630-###lif===-22-###soif===-###eoif===-610-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");}

	}
	else {AKA_mark("lis===-1068-###sois===-28014-###eois===-28014158-###lif===-9-###soif===-###eoif===-356-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");}

		AKA_mark("lis===1088###sois===28537###eois===28566###lif===29###soif===721###eoif===750###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_version_offer(struct afb_proto_ws*,struct readbuf*)");afb_proto_ws_hangup(protows);

}

/* callback when receiving binary data */
/** Instrumented function server_on_binary_job(int,void*) */
static void server_on_binary_job(int sig, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");AKA_fCall++;
		AKA_mark("lis===1094###sois===28672###eois===28704###lif===2###soif===60###eoif===92###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");struct binary *binary = closure;


		if (AKA_mark("lis===1096###sois===28711###eois===28715###lif===4###soif===99###eoif===103###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)") && (AKA_mark("lis===1096###sois===28711###eois===28715###lif===4###soif===99###eoif===103###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)")&&!sig)) {
				AKA_mark("lis===1097###sois===28729###eois===28747###lif===5###soif===117###eoif===135###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");switch(*binary->rb.head++){
					case CHAR_FOR_CALL: if(*binary->rb.head++ == CHAR_FOR_CALL)AKA_mark("lis===1098###sois===28753###eois===28772###lif===6###soif===141###eoif===160###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");

						AKA_mark("lis===1099###sois===28776###eois===28821###lif===7###soif===164###eoif===209###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");server_on_call(binary->protows, &binary->rb);

						AKA_mark("lis===1100###sois===28825###eois===28831###lif===8###soif===213###eoif===219###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");break;

					case CHAR_FOR_DESCRIBE: if(*binary->rb.head++ == CHAR_FOR_DESCRIBE)AKA_mark("lis===1101###sois===28834###eois===28857###lif===9###soif===222###eoif===245###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");

						AKA_mark("lis===1102###sois===28861###eois===28910###lif===10###soif===249###eoif===298###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");server_on_describe(binary->protows, &binary->rb);

						AKA_mark("lis===1103###sois===28914###eois===28920###lif===11###soif===302###eoif===308###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");break;

					case CHAR_FOR_SESSION_ADD: if(*binary->rb.head++ == CHAR_FOR_SESSION_ADD)AKA_mark("lis===1104###sois===28923###eois===28949###lif===12###soif===311###eoif===337###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");

						AKA_mark("lis===1105###sois===28953###eois===29005###lif===13###soif===341###eoif===393###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");server_on_session_add(binary->protows, &binary->rb);

						AKA_mark("lis===1106###sois===29009###eois===29015###lif===14###soif===397###eoif===403###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");break;

					case CHAR_FOR_SESSION_DROP: if(*binary->rb.head++ == CHAR_FOR_SESSION_DROP)AKA_mark("lis===1107###sois===29018###eois===29045###lif===15###soif===406###eoif===433###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");

						AKA_mark("lis===1108###sois===29049###eois===29102###lif===16###soif===437###eoif===490###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");server_on_session_drop(binary->protows, &binary->rb);

						AKA_mark("lis===1109###sois===29106###eois===29112###lif===17###soif===494###eoif===500###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");break;

					case CHAR_FOR_TOKEN_ADD: if(*binary->rb.head++ == CHAR_FOR_TOKEN_ADD)AKA_mark("lis===1110###sois===29115###eois===29139###lif===18###soif===503###eoif===527###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");

						AKA_mark("lis===1111###sois===29143###eois===29193###lif===19###soif===531###eoif===581###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");server_on_token_add(binary->protows, &binary->rb);

						AKA_mark("lis===1112###sois===29197###eois===29203###lif===20###soif===585###eoif===591###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");break;

					case CHAR_FOR_TOKEN_DROP: if(*binary->rb.head++ == CHAR_FOR_TOKEN_DROP)AKA_mark("lis===1113###sois===29206###eois===29231###lif===21###soif===594###eoif===619###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");

						AKA_mark("lis===1114###sois===29235###eois===29286###lif===22###soif===623###eoif===674###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");server_on_token_drop(binary->protows, &binary->rb);

						AKA_mark("lis===1115###sois===29290###eois===29296###lif===23###soif===678###eoif===684###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");break;

					case CHAR_FOR_EVT_UNEXPECTED: if(*binary->rb.head++ == CHAR_FOR_EVT_UNEXPECTED)AKA_mark("lis===1116###sois===29299###eois===29328###lif===24###soif===687###eoif===716###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");

						AKA_mark("lis===1117###sois===29332###eois===29389###lif===25###soif===720###eoif===777###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");server_on_event_unexpected(binary->protows, &binary->rb);

						AKA_mark("lis===1118###sois===29393###eois===29399###lif===26###soif===781###eoif===787###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");break;

					case CHAR_FOR_VERSION_OFFER: if(*binary->rb.head++ == CHAR_FOR_VERSION_OFFER)AKA_mark("lis===1119###sois===29402###eois===29430###lif===27###soif===790###eoif===818###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");

						AKA_mark("lis===1120###sois===29434###eois===29488###lif===28###soif===822###eoif===876###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");server_on_version_offer(binary->protows, &binary->rb);

						AKA_mark("lis===1121###sois===29492###eois===29498###lif===29###soif===880###eoif===886###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");break;

					default: if(*binary->rb.head++ != CHAR_FOR_CALL && *binary->rb.head++ != CHAR_FOR_DESCRIBE && *binary->rb.head++ != CHAR_FOR_SESSION_ADD && *binary->rb.head++ != CHAR_FOR_SESSION_DROP && *binary->rb.head++ != CHAR_FOR_TOKEN_ADD && *binary->rb.head++ != CHAR_FOR_TOKEN_DROP && *binary->rb.head++ != CHAR_FOR_EVT_UNEXPECTED && *binary->rb.head++ != CHAR_FOR_VERSION_OFFER)AKA_mark("lis===1122###sois===29501###eois===29509###lif===30###soif===889###eoif===897###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");
 /* unexpected message */
			/* TODO: close the connection */
						AKA_mark("lis===1124###sois===29574###eois===29580###lif===32###soif===962###eoif===968###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");break;

		}

	}
	else {AKA_mark("lis===-1096-###sois===-28711-###eois===-287114-###lif===-4-###soif===-###eoif===-103-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");}

		AKA_mark("lis===1127###sois===29589###eois===29611###lif===35###soif===977###eoif===999###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");free(binary->rb.base);

		AKA_mark("lis===1128###sois===29613###eois===29626###lif===36###soif===1001###eoif===1014###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary_job(int,void*)");free(binary);

}

/** Instrumented function server_on_binary(void*,char*,size_t) */
static void server_on_binary(void *closure, char *data, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/server_on_binary(void*,char*,size_t)");AKA_fCall++;
		AKA_mark("lis===1133###sois===29702###eois===29741###lif===2###soif===72###eoif===111###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary(void*,char*,size_t)");struct afb_proto_ws *protows = closure;


		AKA_mark("lis===1135###sois===29744###eois===29812###lif===4###soif===114###eoif===182###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_on_binary(void*,char*,size_t)");queue_message_processing(protows, data, size, server_on_binary_job);

}

/******************* server part: manage events **********************************/

/** Instrumented function server_event_send(struct afb_proto_ws*,char,uint16_t,const char*,struct json_object*) */
static int server_event_send(struct afb_proto_ws *protows, char order, uint16_t event_id, const char *event_name, struct json_object *data)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/server_event_send(struct afb_proto_ws*,char,uint16_t,const char*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===1142###sois===30044###eois===30098###lif===2###soif===143###eoif===197###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_event_send(struct afb_proto_ws*,char,uint16_t,const char*,struct json_object*)");struct writebuf wb = { .iovcount = 0, .bufcount = 0 };

		AKA_mark("lis===1143###sois===30100###eois===30112###lif===3###soif===199###eoif===211###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_event_send(struct afb_proto_ws*,char,uint16_t,const char*,struct json_object*)");int rc = -1;


		if (AKA_mark("lis===1145###sois===30119###eois===30313###lif===5###soif===218###eoif===412###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_event_send(struct afb_proto_ws*,char,uint16_t,const char*,struct json_object*)") && ((((AKA_mark("lis===1145###sois===30119###eois===30144###lif===5###soif===218###eoif===243###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_event_send(struct afb_proto_ws*,char,uint16_t,const char*,struct json_object*)")&&writebuf_char(&wb, order))	&&(AKA_mark("lis===1146###sois===30150###eois===30180###lif===6###soif===249###eoif===279###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_event_send(struct afb_proto_ws*,char,uint16_t,const char*,struct json_object*)")&&writebuf_uint16(&wb, event_id)))	&&((AKA_mark("lis===1147###sois===30187###eois===30212###lif===7###soif===286###eoif===311###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_event_send(struct afb_proto_ws*,char,uint16_t,const char*,struct json_object*)")&&order != CHAR_FOR_EVT_ADD)	||(AKA_mark("lis===1147###sois===30216###eois===30248###lif===7###soif===315###eoif===347###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_event_send(struct afb_proto_ws*,char,uint16_t,const char*,struct json_object*)")&&writebuf_string(&wb, event_name))))	&&((AKA_mark("lis===1148###sois===30256###eois===30282###lif===8###soif===355###eoif===381###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_event_send(struct afb_proto_ws*,char,uint16_t,const char*,struct json_object*)")&&order != CHAR_FOR_EVT_PUSH)	||(AKA_mark("lis===1148###sois===30286###eois===30312###lif===8###soif===385###eoif===411###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/server_event_send(struct afb_proto_ws*,char,uint16_t,const char*,struct json_object*)")&&writebuf_object(&wb, data))))) {
		AKA_mark("lis===1149###sois===30317###eois===30348###lif===9###soif===416###eoif===447###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_event_send(struct afb_proto_ws*,char,uint16_t,const char*,struct json_object*)");rc = proto_write(protows, &wb);
	}
	else {AKA_mark("lis===-1145-###sois===-30119-###eois===-30119194-###lif===-5-###soif===-###eoif===-412-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_event_send(struct afb_proto_ws*,char,uint16_t,const char*,struct json_object*)");}

		AKA_mark("lis===1150###sois===30350###eois===30360###lif===10###soif===449###eoif===459###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/server_event_send(struct afb_proto_ws*,char,uint16_t,const char*,struct json_object*)");return rc;

}

/** Instrumented function afb_proto_ws_server_event_create(struct afb_proto_ws*,uint16_t,const char*) */
int afb_proto_ws_server_event_create(struct afb_proto_ws *protows, uint16_t event_id, const char *event_name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_create(struct afb_proto_ws*,uint16_t,const char*)");AKA_fCall++;
		AKA_mark("lis===1155###sois===30477###eois===30557###lif===2###soif===113###eoif===193###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_create(struct afb_proto_ws*,uint16_t,const char*)");return server_event_send(protows, CHAR_FOR_EVT_ADD, event_id, event_name, NULL);

}

/** Instrumented function afb_proto_ws_server_event_remove(struct afb_proto_ws*,uint16_t) */
int afb_proto_ws_server_event_remove(struct afb_proto_ws *protows, uint16_t event_id)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_remove(struct afb_proto_ws*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===1160###sois===30650###eois===30724###lif===2###soif===89###eoif===163###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_remove(struct afb_proto_ws*,uint16_t)");return server_event_send(protows, CHAR_FOR_EVT_DEL, event_id, NULL, NULL);

}

/** Instrumented function afb_proto_ws_server_event_push(struct afb_proto_ws*,uint16_t,struct json_object*) */
int afb_proto_ws_server_event_push(struct afb_proto_ws *protows, uint16_t event_id, struct json_object *data)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_push(struct afb_proto_ws*,uint16_t,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===1165###sois===30841###eois===30916###lif===2###soif===113###eoif===188###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_push(struct afb_proto_ws*,uint16_t,struct json_object*)");return server_event_send(protows, CHAR_FOR_EVT_PUSH, event_id, NULL, data);

}

/** Instrumented function afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t) */
int afb_proto_ws_server_event_broadcast(struct afb_proto_ws *protows, const char *event_name, struct json_object *data, const unsigned char uuid[16], uint8_t hop)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)");AKA_fCall++;
		AKA_mark("lis===1170###sois===31086###eois===31140###lif===2###soif===166###eoif===220###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)");struct writebuf wb = { .iovcount = 0, .bufcount = 0 };

		AKA_mark("lis===1171###sois===31142###eois===31154###lif===3###soif===222###eoif===234###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)");int rc = -1;


		if (AKA_mark("lis===1173###sois===31161###eois===31165###lif===5###soif===241###eoif===245###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)") && (AKA_mark("lis===1173###sois===31161###eois===31165###lif===5###soif===241###eoif===245###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)")&&!hop)) {
		AKA_mark("lis===1174###sois===31169###eois===31178###lif===6###soif===249###eoif===258###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)");return 0;
	}
	else {AKA_mark("lis===-1173-###sois===-31161-###eois===-311614-###lif===-5-###soif===-###eoif===-245-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)");}


		if (AKA_mark("lis===1176###sois===31185###eois===31375###lif===8###soif===265###eoif===455###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)") && (((((AKA_mark("lis===1176###sois===31185###eois===31227###lif===8###soif===265###eoif===307###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)")&&writebuf_char(&wb, CHAR_FOR_EVT_BROADCAST))	&&(AKA_mark("lis===1177###sois===31233###eois===31265###lif===9###soif===313###eoif===345###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)")&&writebuf_string(&wb, event_name)))	&&(AKA_mark("lis===1178###sois===31271###eois===31297###lif===10###soif===351###eoif===377###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)")&&writebuf_object(&wb, data)))	&&(AKA_mark("lis===1179###sois===31303###eois===31330###lif===11###soif===383###eoif===410###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)")&&writebuf_put(&wb, uuid, 16)))	&&(AKA_mark("lis===1180###sois===31336###eois===31375###lif===12###soif===416###eoif===455###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)")&&writebuf_uint8(&wb, (uint8_t)(hop - 1))))) {
		AKA_mark("lis===1181###sois===31379###eois===31410###lif===13###soif===459###eoif===490###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)");rc = proto_write(protows, &wb);
	}
	else {AKA_mark("lis===-1176-###sois===-31185-###eois===-31185190-###lif===-8-###soif===-###eoif===-455-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)");}

		AKA_mark("lis===1182###sois===31412###eois===31422###lif===14###soif===492###eoif===502###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_server_event_broadcast(struct afb_proto_ws*,const char*,struct json_object*,const unsigned char[16],uint8_t)");return rc;

}

/*****************************************************/

/* callback when receiving a hangup */
/** Instrumented function on_hangup(void*) */
static void on_hangup(void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");AKA_fCall++;
		AKA_mark("lis===1190###sois===31562###eois===31601###lif===2###soif===40###eoif===79###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");struct afb_proto_ws *protows = closure;

		AKA_mark("lis===1191###sois===31603###eois===31636###lif===3###soif===81###eoif===114###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");struct client_describe *cd, *ncd;

		AKA_mark("lis===1192###sois===31638###eois===31671###lif===4###soif===116###eoif===149###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");struct client_call *call, *ncall;

		AKA_mark("lis===1193###sois===31673###eois===31691###lif===5###soif===151###eoif===169###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");struct afb_ws *ws;


		AKA_mark("lis===1195###sois===31694###eois===31730###lif===7###soif===172###eoif===208###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");pthread_mutex_lock(&protows->mutex);

		AKA_mark("lis===1196###sois===31732###eois===31757###lif===8###soif===210###eoif===235###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");ncd = protows->describes;

		AKA_mark("lis===1197###sois===31759###eois===31785###lif===9###soif===237###eoif===263###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");protows->describes = NULL;

		AKA_mark("lis===1198###sois===31787###eois===31810###lif===10###soif===265###eoif===288###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");ncall = protows->calls;

		AKA_mark("lis===1199###sois===31812###eois===31834###lif===11###soif===290###eoif===312###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");protows->calls = NULL;

		AKA_mark("lis===1200###sois===31836###eois===31853###lif===12###soif===314###eoif===331###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");ws = protows->ws;

		AKA_mark("lis===1201###sois===31855###eois===31874###lif===13###soif===333###eoif===352###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");protows->ws = NULL;

		AKA_mark("lis===1202###sois===31876###eois===31897###lif===14###soif===354###eoif===375###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");protows->idcount = 0;

		AKA_mark("lis===1203###sois===31899###eois===31937###lif===15###soif===377###eoif===415###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");pthread_mutex_unlock(&protows->mutex);


		while (AKA_mark("lis===1205###sois===31947###eois===31952###lif===17###soif===425###eoif===430###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)") && (AKA_mark("lis===1205###sois===31947###eois===31952###lif===17###soif===425###eoif===430###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)")&&ncall)) {
				AKA_mark("lis===1206###sois===31958###eois===31970###lif===18###soif===436###eoif===448###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");call= ncall;

				AKA_mark("lis===1207###sois===31973###eois===31992###lif===19###soif===451###eoif===470###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");ncall = call->next;

				AKA_mark("lis===1208###sois===31995###eois===32098###lif===20###soif===473###eoif===576###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");protows->client_itf->on_reply(protows->closure, call->request, NULL, "disconnected", "server hung up");

				AKA_mark("lis===1209###sois===32101###eois===32112###lif===21###soif===579###eoif===590###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");free(call);

	}


		while (AKA_mark("lis===1212###sois===32125###eois===32128###lif===24###soif===603###eoif===606###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)") && (AKA_mark("lis===1212###sois===32125###eois===32128###lif===24###soif===603###eoif===606###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)")&&ncd)) {
				AKA_mark("lis===1213###sois===32134###eois===32142###lif===25###soif===612###eoif===620###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");cd= ncd;

				AKA_mark("lis===1214###sois===32145###eois===32160###lif===26###soif===623###eoif===638###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");ncd = cd->next;

				AKA_mark("lis===1215###sois===32163###eois===32195###lif===27###soif===641###eoif===673###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");cd->callback(cd->closure, NULL);

				AKA_mark("lis===1216###sois===32198###eois===32207###lif===28###soif===676###eoif===685###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");free(cd);

	}


		if (AKA_mark("lis===1219###sois===32217###eois===32219###lif===31###soif===695###eoif===697###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)") && (AKA_mark("lis===1219###sois===32217###eois===32219###lif===31###soif===695###eoif===697###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)")&&ws)) {
				AKA_mark("lis===1220###sois===32225###eois===32244###lif===32###soif===703###eoif===722###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");afb_ws_destroy(ws);

				if (AKA_mark("lis===1221###sois===32251###eois===32269###lif===33###soif===729###eoif===747###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)") && (AKA_mark("lis===1221###sois===32251###eois===32269###lif===33###soif===729###eoif===747###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)")&&protows->on_hangup)) {
			AKA_mark("lis===1222###sois===32274###eois===32311###lif===34###soif===752###eoif===789###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");protows->on_hangup(protows->closure);
		}
		else {AKA_mark("lis===-1221-###sois===-32251-###eois===-3225118-###lif===-33-###soif===-###eoif===-747-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");}

	}
	else {AKA_mark("lis===-1219-###sois===-32217-###eois===-322172-###lif===-31-###soif===-###eoif===-697-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/on_hangup(void*)");}

}

/*****************************************************/

static const struct afb_ws_itf proto_ws_client_ws_itf =
{
	.on_close = NULL,
	.on_text = NULL,
	.on_binary = client_on_binary,
	.on_error = NULL,
	.on_hangup = on_hangup
};

static const struct afb_ws_itf server_ws_itf =
{
	.on_close = NULL,
	.on_text = NULL,
	.on_binary = server_on_binary,
	.on_error = NULL,
	.on_hangup = on_hangup
};

/*****************************************************/

/** Instrumented function afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*) */
static struct afb_proto_ws *afb_proto_ws_create(struct fdev *fdev, const struct afb_proto_ws_server_itf *itfs, const struct afb_proto_ws_client_itf *itfc, void *closure, const struct afb_ws_itf *itf)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");AKA_fCall++;
		AKA_mark("lis===1250###sois===32974###eois===33003###lif===2###soif===203###eoif===232###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");struct afb_proto_ws *protows;


		AKA_mark("lis===1252###sois===33006###eois===33043###lif===4###soif===235###eoif===272###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");protows = calloc(1, sizeof *protows);

		if (AKA_mark("lis===1253###sois===33049###eois===33064###lif===5###soif===278###eoif===293###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)") && (AKA_mark("lis===1253###sois===33049###eois===33064###lif===5###soif===278###eoif===293###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)")&&protows == NULL)) {
		AKA_mark("lis===1254###sois===33068###eois===33083###lif===6###soif===297###eoif===312###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");errno = ENOMEM;
	}
	else {
				AKA_mark("lis===1256###sois===33094###eois===33136###lif===8###soif===323###eoif===365###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");fcntl(fdev_fd(fdev), F_SETFD, FD_CLOEXEC);

				AKA_mark("lis===1257###sois===33139###eois===33181###lif===9###soif===368###eoif===410###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");fcntl(fdev_fd(fdev), F_SETFL, O_NONBLOCK);

				AKA_mark("lis===1258###sois===33184###eois===33232###lif===10###soif===413###eoif===461###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");protows->ws = afb_ws_create(fdev, itf, protows);

				if (AKA_mark("lis===1259###sois===33239###eois===33258###lif===11###soif===468###eoif===487###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)") && (AKA_mark("lis===1259###sois===33239###eois===33258###lif===11###soif===468###eoif===487###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)")&&protows->ws != NULL)) {
						AKA_mark("lis===1260###sois===33265###eois===33287###lif===12###soif===494###eoif===516###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");protows->refcount = 1;

						AKA_mark("lis===1261###sois===33291###eois===33330###lif===13###soif===520###eoif===559###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");protows->version = WSAPI_VERSION_UNSET;

						AKA_mark("lis===1262###sois===33334###eois===33361###lif===14###soif===563###eoif===590###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");protows->closure = closure;

						AKA_mark("lis===1263###sois===33365###eois===33392###lif===15###soif===594###eoif===621###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");protows->server_itf = itfs;

						AKA_mark("lis===1264###sois===33396###eois===33423###lif===16###soif===625###eoif===652###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");protows->client_itf = itfc;

						AKA_mark("lis===1265###sois===33427###eois===33469###lif===17###soif===656###eoif===698###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");pthread_mutex_init(&protows->mutex, NULL);

						AKA_mark("lis===1266###sois===33473###eois===33488###lif===18###soif===702###eoif===717###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");return protows;

		}
		else {AKA_mark("lis===-1259-###sois===-33239-###eois===-3323919-###lif===-11-###soif===-###eoif===-487-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");}

				AKA_mark("lis===1268###sois===33495###eois===33509###lif===20###soif===724###eoif===738###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");free(protows);

	}

		AKA_mark("lis===1270###sois===33514###eois===33526###lif===22###soif===743###eoif===755###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create(struct fdev*,const struct afb_proto_ws_server_itf*,const struct afb_proto_ws_client_itf*,void*,const struct afb_ws_itf*)");return NULL;

}

/** Instrumented function afb_proto_ws_create_client(struct fdev*,const struct afb_proto_ws_client_itf*,void*) */
struct afb_proto_ws *afb_proto_ws_create_client(struct fdev *fdev, const struct afb_proto_ws_client_itf *itf, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create_client(struct fdev*,const struct afb_proto_ws_client_itf*,void*)");AKA_fCall++;
		AKA_mark("lis===1275###sois===33658###eois===33687###lif===2###soif===128###eoif===157###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create_client(struct fdev*,const struct afb_proto_ws_client_itf*,void*)");struct afb_proto_ws *protows;


		AKA_mark("lis===1277###sois===33690###eois===33771###lif===4###soif===160###eoif===241###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create_client(struct fdev*,const struct afb_proto_ws_client_itf*,void*)");protows = afb_proto_ws_create(fdev, NULL, itf, closure, &proto_ws_client_ws_itf);

		if (AKA_mark("lis===1278###sois===33777###eois===33784###lif===5###soif===247###eoif===254###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create_client(struct fdev*,const struct afb_proto_ws_client_itf*,void*)") && (AKA_mark("lis===1278###sois===33777###eois===33784###lif===5###soif===247###eoif===254###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create_client(struct fdev*,const struct afb_proto_ws_client_itf*,void*)")&&protows)) {
				if (AKA_mark("lis===1279###sois===33794###eois===33845###lif===6###soif===264###eoif===315###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create_client(struct fdev*,const struct afb_proto_ws_client_itf*,void*)") && (AKA_mark("lis===1279###sois===33794###eois===33845###lif===6###soif===264###eoif===315###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create_client(struct fdev*,const struct afb_proto_ws_client_itf*,void*)")&&send_version_offer_1(protows, WSAPI_VERSION_1) != 0)) {
						AKA_mark("lis===1280###sois===33852###eois===33880###lif===7###soif===322###eoif===350###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create_client(struct fdev*,const struct afb_proto_ws_client_itf*,void*)");afb_proto_ws_unref(protows);

						AKA_mark("lis===1281###sois===33884###eois===33899###lif===8###soif===354###eoif===369###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create_client(struct fdev*,const struct afb_proto_ws_client_itf*,void*)");protows = NULL;

		}
		else {AKA_mark("lis===-1279-###sois===-33794-###eois===-3379451-###lif===-6-###soif===-###eoif===-315-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create_client(struct fdev*,const struct afb_proto_ws_client_itf*,void*)");}

	}
	else {AKA_mark("lis===-1278-###sois===-33777-###eois===-337777-###lif===-5-###soif===-###eoif===-254-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create_client(struct fdev*,const struct afb_proto_ws_client_itf*,void*)");}

		AKA_mark("lis===1284###sois===33908###eois===33923###lif===11###soif===378###eoif===393###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create_client(struct fdev*,const struct afb_proto_ws_client_itf*,void*)");return protows;

}

/** Instrumented function afb_proto_ws_create_server(struct fdev*,const struct afb_proto_ws_server_itf*,void*) */
struct afb_proto_ws *afb_proto_ws_create_server(struct fdev *fdev, const struct afb_proto_ws_server_itf *itf, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create_server(struct fdev*,const struct afb_proto_ws_server_itf*,void*)");AKA_fCall++;
		AKA_mark("lis===1289###sois===34055###eois===34124###lif===2###soif===128###eoif===197###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_create_server(struct fdev*,const struct afb_proto_ws_server_itf*,void*)");return afb_proto_ws_create(fdev, itf, NULL, closure, &server_ws_itf);

}

/** Instrumented function afb_proto_ws_unref(struct afb_proto_ws*) */
void afb_proto_ws_unref(struct afb_proto_ws *protows)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_unref(struct afb_proto_ws*)");AKA_fCall++;
		if (AKA_mark("lis===1294###sois===34189###eois===34260###lif===2###soif===61###eoif===132###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_unref(struct afb_proto_ws*)") && ((AKA_mark("lis===1294###sois===34189###eois===34196###lif===2###soif===61###eoif===68###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_unref(struct afb_proto_ws*)")&&protows)	&&(AKA_mark("lis===1294###sois===34200###eois===34260###lif===2###soif===72###eoif===132###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_unref(struct afb_proto_ws*)")&&!__atomic_sub_fetch(&protows->refcount, 1, __ATOMIC_RELAXED)))) {
				AKA_mark("lis===1295###sois===34266###eois===34295###lif===3###soif===138###eoif===167###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_unref(struct afb_proto_ws*)");afb_proto_ws_hangup(protows);

				AKA_mark("lis===1296###sois===34298###eois===34337###lif===4###soif===170###eoif===209###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_unref(struct afb_proto_ws*)");pthread_mutex_destroy(&protows->mutex);

				AKA_mark("lis===1297###sois===34340###eois===34354###lif===5###soif===212###eoif===226###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_unref(struct afb_proto_ws*)");free(protows);

	}
	else {AKA_mark("lis===-1294-###sois===-34189-###eois===-3418971-###lif===-2-###soif===-###eoif===-132-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_unref(struct afb_proto_ws*)");}

}

/** Instrumented function afb_proto_ws_addref(struct afb_proto_ws*) */
void afb_proto_ws_addref(struct afb_proto_ws *protows)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_addref(struct afb_proto_ws*)");AKA_fCall++;
		AKA_mark("lis===1303###sois===34419###eois===34479###lif===2###soif===58###eoif===118###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_addref(struct afb_proto_ws*)");__atomic_add_fetch(&protows->refcount, 1, __ATOMIC_RELAXED);

}

/** Instrumented function afb_proto_ws_is_client(struct afb_proto_ws*) */
int afb_proto_ws_is_client(struct afb_proto_ws *protows)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_is_client(struct afb_proto_ws*)");AKA_fCall++;
		AKA_mark("lis===1308###sois===34543###eois===34572###lif===2###soif===60###eoif===89###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_is_client(struct afb_proto_ws*)");return !!protows->client_itf;

}

/** Instrumented function afb_proto_ws_is_server(struct afb_proto_ws*) */
int afb_proto_ws_is_server(struct afb_proto_ws *protows)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_is_server(struct afb_proto_ws*)");AKA_fCall++;
		AKA_mark("lis===1313###sois===34636###eois===34665###lif===2###soif===60###eoif===89###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_is_server(struct afb_proto_ws*)");return !!protows->server_itf;

}

/** Instrumented function afb_proto_ws_hangup(struct afb_proto_ws*) */
void afb_proto_ws_hangup(struct afb_proto_ws *protows)
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_hangup(struct afb_proto_ws*)");AKA_fCall++;
		if (AKA_mark("lis===1318###sois===34731###eois===34742###lif===2###soif===62###eoif===73###ifc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_hangup(struct afb_proto_ws*)") && (AKA_mark("lis===1318###sois===34731###eois===34742###lif===2###soif===62###eoif===73###isc===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_hangup(struct afb_proto_ws*)")&&protows->ws)) {
		AKA_mark("lis===1319###sois===34746###eois===34773###lif===3###soif===77###eoif===104###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_hangup(struct afb_proto_ws*)");afb_ws_hangup(protows->ws);
	}
	else {AKA_mark("lis===-1318-###sois===-34731-###eois===-3473111-###lif===-2-###soif===-###eoif===-73-###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_hangup(struct afb_proto_ws*)");}

}

/** Instrumented function afb_proto_ws_on_hangup(struct afb_proto_ws*,void(*on_hangup)(void*closure)) */
void afb_proto_ws_on_hangup(struct afb_proto_ws *protows, void (*on_hangup)(void *closure))
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_on_hangup(struct afb_proto_ws*,void(*on_hangup)(void*closure))");AKA_fCall++;
		AKA_mark("lis===1324###sois===34872###eois===34903###lif===2###soif===95###eoif===126###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_on_hangup(struct afb_proto_ws*,void(*on_hangup)(void*closure))");protows->on_hangup = on_hangup;

}

/** Instrumented function afb_proto_ws_set_queuing(struct afb_proto_ws*,int(*queuing)(struct afb_proto_ws*, void (*)(int,void*), void*)) */
void afb_proto_ws_set_queuing(struct afb_proto_ws *protows, int (*queuing)(struct afb_proto_ws*, void (*)(int,void*), void*))
{AKA_mark("Calling: ./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_set_queuing(struct afb_proto_ws*,int(*queuing)(struct afb_proto_ws*, void (*)(int,void*), void*))");AKA_fCall++;
		AKA_mark("lis===1329###sois===35036###eois===35063###lif===2###soif===129###eoif===156###ins===true###function===./app-framework-binder/src/afb-proto-ws.c/afb_proto_ws_set_queuing(struct afb_proto_ws*,int(*queuing)(struct afb_proto_ws*, void (*)(int,void*), void*))");protows->queuing = queuing;

}

#endif

