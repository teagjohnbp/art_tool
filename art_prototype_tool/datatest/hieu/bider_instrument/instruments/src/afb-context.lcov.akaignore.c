/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_CONTEXT_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_CONTEXT_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author "Fulup Ar Foll"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SESSION_H_
#define AKA_INCLUDE__AFB_SESSION_H_
#include "afb-session.lcov.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CONTEXT_H_
#define AKA_INCLUDE__AFB_CONTEXT_H_
#include "afb-context.lcov.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_TOKEN_H_
#define AKA_INCLUDE__AFB_TOKEN_H_
#include "afb-token.lcov.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CRED_H_
#define AKA_INCLUDE__AFB_CRED_H_
#include "afb-cred.lcov.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_PERM_H_
#define AKA_INCLUDE__AFB_PERM_H_
#include "afb-perm.lcov.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_PERMISSION_TEXT_H_
#define AKA_INCLUDE__AFB_PERMISSION_TEXT_H_
#include "afb-permission-text.lcov.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.lcov.akaignore.h"
#endif


static void init_context(struct afb_context *context, struct afb_session *session, struct afb_token *token, struct afb_cred *cred)
{AKA_fCall++;
	assert(session != NULL);

	/* reset the context for the session */
	context->session = session;
	context->flags = 0;
	context->super = NULL;
	context->api_key = NULL;
	context->token = afb_token_addref(token);
	context->credentials = afb_cred_addref(cred);
}

void afb_context_subinit(struct afb_context *context, struct afb_context *super)
{AKA_fCall++;
	context->session = afb_session_addref(super->session);
	context->flags = 0;
	context->super = super;
	context->api_key = NULL;
	context->token = afb_token_addref(super->token);
	context->credentials = afb_cred_addref(super->credentials);
}

void afb_context_init(struct afb_context *context, struct afb_session *session, struct afb_token *token, struct afb_cred *cred)
{AKA_fCall++;
	init_context(context, afb_session_addref(session), token, cred);
}

int afb_context_connect(struct afb_context *context, const char *uuid, struct afb_token *token, struct afb_cred *cred)
{AKA_fCall++;
	int created;
	struct afb_session *session;

	session = afb_session_get (uuid, AFB_SESSION_TIMEOUT_DEFAULT, &created);
	if (session == NULL)
		return -1;
	init_context(context, session, token, cred);
	if (created) {
		context->created = 1;
	}
	return 0;
}

int afb_context_connect_validated(struct afb_context *context, const char *uuid, struct afb_token *token, struct afb_cred *cred)
{AKA_fCall++;
	int rc = afb_context_connect(context, uuid, token, cred);
	if (!rc)
		context->validated = 1;
	return rc;
}

void afb_context_init_validated(struct afb_context *context, struct afb_session *session, struct afb_token *token, struct afb_cred *cred)
{AKA_fCall++;
	afb_context_init(context, session, token, cred);
	context->validated = 1;
}

void afb_context_disconnect(struct afb_context *context)
{AKA_fCall++;
	if (context->session && !context->super && context->closing && !context->closed) {
		afb_context_change_loa(context, 0);
		afb_context_set(context, NULL, NULL);
		context->closed = 1;
	}
	afb_session_unref(context->session);
	context->session = NULL;
	afb_cred_unref(context->credentials);
	context->credentials = NULL;
	afb_token_unref(context->token);
	context->token = NULL;
}

void afb_context_change_cred(struct afb_context *context, struct afb_cred *cred)
{AKA_fCall++;
	struct afb_cred *ocred = context->credentials;
	if (ocred != cred) {
		context->credentials = afb_cred_addref(cred);
		afb_cred_unref(ocred);
	}
}

void afb_context_change_token(struct afb_context *context, struct afb_token *token)
{AKA_fCall++;
	struct afb_token *otoken = context->token;
	if (otoken != token) {
		context->token = afb_token_addref(token);
		afb_token_unref(otoken);
	}
}

const char *afb_context_on_behalf_export(struct afb_context *context)
{AKA_fCall++;
	return context->credentials ? afb_cred_export(context->credentials) : NULL;
}

int afb_context_on_behalf_import(struct afb_context *context, const char *exported)
{AKA_fCall++;
	int rc;
	struct afb_cred *imported, *ocred;

	if (!exported || !*exported)
		rc = 0;
	else {
		if (afb_context_has_permission(context, afb_permission_on_behalf_credential)) {
			imported = afb_cred_import(exported);
			if (!imported) {
				ERROR("Can't import on behalf credentials: %m");
				rc = -1;
			} else {
				ocred = context->credentials;
				context->credentials = imported;
				afb_cred_unref(ocred);
				rc = 0;
			}
		} else {
			ERROR("On behalf credentials refused");
			rc = -1;
		}
	}
	return rc;
}

void afb_context_on_behalf_other_context(struct afb_context *context, struct afb_context *other)
{AKA_fCall++;
	afb_context_change_cred(context, other->credentials);
	afb_context_change_token(context, other->token);
}

int afb_context_has_permission(struct afb_context *context, const char *permission)
{AKA_fCall++;
	return afb_perm_check(context, permission);
}

void afb_context_has_permission_async(
	struct afb_context *context,
	const char *permission,
	void (*callback)(void *_closure, int _status),
	void *closure
)
{AKA_fCall++;
	return afb_perm_check_async(context, permission, callback, closure);
}

const char *afb_context_uuid(struct afb_context *context)
{AKA_fCall++;
	return context->session ? afb_session_uuid(context->session) : NULL;
}

void *afb_context_make(struct afb_context *context, int replace, void *(*make_value)(void *closure), void (*free_value)(void *item), void *closure)
{AKA_fCall++;
	assert(context->session != NULL);
	return afb_session_cookie(context->session, context->api_key, make_value, free_value, closure, replace);
}

void *afb_context_get(struct afb_context *context)
{AKA_fCall++;
	assert(context->session != NULL);
	return afb_session_get_cookie(context->session, context->api_key);
}

int afb_context_set(struct afb_context *context, void *value, void (*free_value)(void*))
{AKA_fCall++;
	assert(context->session != NULL);
	return afb_session_set_cookie(context->session, context->api_key, value, free_value);
}

void afb_context_close(struct afb_context *context)
{AKA_fCall++;
	context->closing = 1;
}

struct chkctx {
	struct afb_context *context;
	void (*callback)(void *_closure, int _status);
	void *closure;
};

static void check_context_cb(void *closure_chkctx, int status)
{AKA_fCall++;
	struct chkctx *cc = closure_chkctx;
	struct afb_context *context = cc->context;
	void (*callback)(void*,int) = cc->callback;
	void *closure = cc->closure;

	free(cc);
	if (status)
		context->validated = 1;
	else
		context->invalidated = 1;
	callback(closure, status);
}

static int check_context(
	struct afb_context *context,
	void (*callback)(void *_closure, int _status),
	void *closure
) {AKA_fCall++;
	int r;
	struct chkctx *cc;

	if (context->validated)
		r = 1;
	else if (context->invalidated)
		r = 0;
	else {
		if (context->super)
			r = check_context(context->super, callback, closure);
		else if (!callback)
			r = afb_context_has_permission(context, afb_permission_token_valid);
		else {
			cc = malloc(sizeof *cc);
			if (cc) {
				cc->context = context;
				cc->callback = callback;
				cc->closure = closure;
				afb_context_has_permission_async(context, afb_permission_token_valid, check_context_cb, cc);
				return -1;
			}
			ERROR("out-of-memory");
			r = 0;
		}
		if (r)
			context->validated = 1;
		else
			context->invalidated = 1;
	}
	return r;
}

int afb_context_check(struct afb_context *context)
{AKA_fCall++;
	return check_context(context, 0, 0);
}

void afb_context_check_async(
	struct afb_context *context,
	void (*callback)(void *_closure, int _status),
	void *closure
) {AKA_fCall++;
	int r = check_context(context, callback, closure);
	if (r >= 0)
		callback(closure, r);
}

static inline const void *loa_key(struct afb_context *context)
{AKA_fCall++;
	return (const void*)(1+(intptr_t)(context->api_key));
}

static inline void *loa2ptr(unsigned loa)
{AKA_fCall++;
	return (void*)(intptr_t)loa;
}

static inline unsigned ptr2loa(void *ptr)
{AKA_fCall++;
	return (unsigned)(intptr_t)ptr;
}

int afb_context_change_loa(struct afb_context *context, unsigned loa)
{AKA_fCall++;
	if (loa > 7) {
		errno = EINVAL;
		return -1;
	}
	if (!afb_context_check(context)) {
		errno = EPERM;
		return -1;
	}

	return afb_session_set_cookie(context->session, loa_key(context), loa2ptr(loa), NULL);
}

unsigned afb_context_get_loa(struct afb_context *context)
{AKA_fCall++;
	assert(context->session != NULL);
	return ptr2loa(afb_session_get_cookie(context->session, loa_key(context)));
}

int afb_context_check_loa(struct afb_context *context, unsigned loa)
{AKA_fCall++;
	return afb_context_get_loa(context) >= loa;
}

#endif

