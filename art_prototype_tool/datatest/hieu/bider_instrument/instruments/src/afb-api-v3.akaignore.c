/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_API_V3_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_API_V3_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <fnmatch.h>

#include <json-c/json.h>

#define AFB_BINDING_VERSION 0
#include <afb/afb-binding.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_H_
#define AKA_INCLUDE__AFB_API_H_
#include "afb-api.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_V3_H_
#define AKA_INCLUDE__AFB_API_V3_H_
#include "afb-api-v3.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_APISET_H_
#define AKA_INCLUDE__AFB_APISET_H_
#include "afb-apiset.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_AUTH_H_
#define AKA_INCLUDE__AFB_AUTH_H_
#include "afb-auth.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_EXPORT_H_
#define AKA_INCLUDE__AFB_EXPORT_H_
#include "afb-export.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_XREQ_H_
#define AKA_INCLUDE__AFB_XREQ_H_
#include "afb-xreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__SIG_MONITOR_H_
#define AKA_INCLUDE__SIG_MONITOR_H_
#include "sig-monitor.akaignore.h"
#endif


/*
 * Description of a binding
 */
struct afb_api_v3 {
	int refcount;
	int count;
	struct afb_verb_v3 **verbs;
#if WITH_LEGACY_BINDING_V2
	const struct afb_verb_v2 *verbsv2;
#endif
	const struct afb_verb_v3 *verbsv3;
	struct afb_export *export;
	const char *info;
};

static const char nulchar = 0;

/** Instrumented function verb_name_compare(const struct afb_verb_v3*,const char*) */
static int verb_name_compare(const struct afb_verb_v3 *verb, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/verb_name_compare(const struct afb_verb_v3*,const char*)");AKA_fCall++;
		AKA_mark("lis===59###sois===1417###eois===1549###lif===2###soif===82###eoif===214###ins===true###function===./app-framework-binder/src/afb-api-v3.c/verb_name_compare(const struct afb_verb_v3*,const char*)");return verb->glob
		? fnmatch(verb->verb, name, FNM_NOESCAPE|FNM_PATHNAME|FNM_CASEFOLD|FNM_PERIOD)
		: strcasecmp(verb->verb, name);

}

/** Instrumented function search_dynamic_verb(struct afb_api_v3*,const char*) */
static struct afb_verb_v3 *search_dynamic_verb(struct afb_api_v3 *api, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/search_dynamic_verb(struct afb_api_v3*,const char*)");AKA_fCall++;
		AKA_mark("lis===66###sois===1645###eois===1677###lif===2###soif===92###eoif===124###ins===true###function===./app-framework-binder/src/afb-api-v3.c/search_dynamic_verb(struct afb_api_v3*,const char*)");struct afb_verb_v3 **v, **e, *i;


		AKA_mark("lis===68###sois===1680###eois===1695###lif===4###soif===127###eoif===142###ins===true###function===./app-framework-binder/src/afb-api-v3.c/search_dynamic_verb(struct afb_api_v3*,const char*)");v = api->verbs;

		AKA_mark("lis===69###sois===1697###eois===1716###lif===5###soif===144###eoif===163###ins===true###function===./app-framework-binder/src/afb-api-v3.c/search_dynamic_verb(struct afb_api_v3*,const char*)");e = &v[api->count];

		while (AKA_mark("lis===70###sois===1725###eois===1731###lif===6###soif===172###eoif===178###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/search_dynamic_verb(struct afb_api_v3*,const char*)") && (AKA_mark("lis===70###sois===1725###eois===1731###lif===6###soif===172###eoif===178###isc===true###function===./app-framework-binder/src/afb-api-v3.c/search_dynamic_verb(struct afb_api_v3*,const char*)")&&v != e)) {
				AKA_mark("lis===71###sois===1737###eois===1744###lif===7###soif===184###eoif===191###ins===true###function===./app-framework-binder/src/afb-api-v3.c/search_dynamic_verb(struct afb_api_v3*,const char*)");i = *v;

				if (AKA_mark("lis===72###sois===1751###eois===1778###lif===8###soif===198###eoif===225###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/search_dynamic_verb(struct afb_api_v3*,const char*)") && (AKA_mark("lis===72###sois===1751###eois===1778###lif===8###soif===198###eoif===225###isc===true###function===./app-framework-binder/src/afb-api-v3.c/search_dynamic_verb(struct afb_api_v3*,const char*)")&&!verb_name_compare(i, name))) {
			AKA_mark("lis===73###sois===1783###eois===1792###lif===9###soif===230###eoif===239###ins===true###function===./app-framework-binder/src/afb-api-v3.c/search_dynamic_verb(struct afb_api_v3*,const char*)");return i;
		}
		else {AKA_mark("lis===-72-###sois===-1751-###eois===-175127-###lif===-8-###soif===-###eoif===-225-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/search_dynamic_verb(struct afb_api_v3*,const char*)");}

				AKA_mark("lis===74###sois===1795###eois===1799###lif===10###soif===242###eoif===246###ins===true###function===./app-framework-binder/src/afb-api-v3.c/search_dynamic_verb(struct afb_api_v3*,const char*)");v++;

	}

		AKA_mark("lis===76###sois===1804###eois===1813###lif===12###soif===251###eoif===260###ins===true###function===./app-framework-binder/src/afb-api-v3.c/search_dynamic_verb(struct afb_api_v3*,const char*)");return 0;

}

/** Instrumented function afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*) */
void afb_api_v3_process_call(struct afb_api_v3 *api, struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===81###sois===1896###eois===1930###lif===2###soif===79###eoif===113###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");const struct afb_verb_v3 *verbsv3;

#if WITH_LEGACY_BINDING_V2
	const struct afb_verb_v2 *verbsv2;
#endif
		AKA_mark("lis===85###sois===2002###eois===2019###lif===6###soif===185###eoif===202###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");const char *name;


		AKA_mark("lis===87###sois===2022###eois===2055###lif===8###soif===205###eoif===238###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");name = xreq->request.called_verb;


	/* look first in dynamic set */
		AKA_mark("lis===90###sois===2091###eois===2132###lif===11###soif===274###eoif===315###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");verbsv3 = search_dynamic_verb(api, name);

		if (AKA_mark("lis===91###sois===2138###eois===2146###lif===12###soif===321###eoif===329###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)") && (AKA_mark("lis===91###sois===2138###eois===2146###lif===12###soif===321###eoif===329###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)")&&!verbsv3)) {
		/* look then in static set */
				AKA_mark("lis===93###sois===2184###eois===2207###lif===14###soif===367###eoif===390###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");verbsv3 = api->verbsv3;

				while (AKA_mark("lis===94###sois===2217###eois===2224###lif===15###soif===400###eoif===407###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)") && (AKA_mark("lis===94###sois===2217###eois===2224###lif===15###soif===400###eoif===407###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)")&&verbsv3)) {
						if (AKA_mark("lis===95###sois===2235###eois===2249###lif===16###soif===418###eoif===432###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)") && (AKA_mark("lis===95###sois===2235###eois===2249###lif===16###soif===418###eoif===432###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)")&&!verbsv3->verb)) {
				AKA_mark("lis===96###sois===2255###eois===2267###lif===17###soif===438###eoif===450###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");verbsv3 = 0;
			}
			else {
				if (AKA_mark("lis===97###sois===2280###eois===2313###lif===18###soif===463###eoif===496###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)") && (AKA_mark("lis===97###sois===2280###eois===2313###lif===18###soif===463###eoif===496###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)")&&!verb_name_compare(verbsv3, name))) {
					AKA_mark("lis===98###sois===2319###eois===2325###lif===19###soif===502###eoif===508###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");break;
				}
				else {
					AKA_mark("lis===100###sois===2338###eois===2348###lif===21###soif===521###eoif===531###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");verbsv3++;
				}
			}

		}

	}
	else {AKA_mark("lis===-91-###sois===-2138-###eois===-21388-###lif===-12-###soif===-###eoif===-329-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");}

	/* is it a v3 verb ? */
		if (AKA_mark("lis===104###sois===2386###eois===2393###lif===25###soif===569###eoif===576###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)") && (AKA_mark("lis===104###sois===2386###eois===2393###lif===25###soif===569###eoif===576###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)")&&verbsv3)) {
		/* yes */
				AKA_mark("lis===106###sois===2411###eois===2452###lif===27###soif===594###eoif===635###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");xreq->request.vcbdata = verbsv3->vcbdata;

				AKA_mark("lis===107###sois===2455###eois===2492###lif===28###soif===638###eoif===675###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");afb_xreq_call_verb_v3(xreq, verbsv3);

				AKA_mark("lis===108###sois===2495###eois===2502###lif===29###soif===678###eoif===685###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");return;

	}
	else {AKA_mark("lis===-104-###sois===-2386-###eois===-23867-###lif===-25-###soif===-###eoif===-576-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");}


#if WITH_LEGACY_BINDING_V2
	/* look in legacy set */
	verbsv2 = api->verbsv2;
	if (verbsv2) {
		while (verbsv2->verb) {
			if (strcasecmp(verbsv2->verb, name))
				verbsv2++;
			else {
				afb_xreq_call_verb_v2(xreq, verbsv2);
				return;
			}
		}
	}
#endif
		AKA_mark("lis===125###sois===2766###eois===2800###lif===46###soif===949###eoif===983###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_process_call(struct afb_api_v3*,struct afb_xreq*)");afb_xreq_reply_unknown_verb(xreq);

}

/** Instrumented function describe_verb_v3(const struct afb_verb_v3*) */
static struct json_object *describe_verb_v3(const struct afb_verb_v3 *verb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)");AKA_fCall++;
		AKA_mark("lis===130###sois===2883###eois===2913###lif===2###soif===79###eoif===109###ins===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)");struct json_object *f, *a, *g;


		AKA_mark("lis===132###sois===2916###eois===2945###lif===4###soif===112###eoif===141###ins===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)");f = json_object_new_object();


		AKA_mark("lis===134###sois===2948###eois===2977###lif===6###soif===144###eoif===173###ins===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)");g = json_object_new_object();

		AKA_mark("lis===135###sois===2979###eois===3015###lif===7###soif===175###eoif===211###ins===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)");json_object_object_add(f, "get", g);


		AKA_mark("lis===137###sois===3018###eois===3066###lif===9###soif===214###eoif===262###ins===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)");a = afb_auth_json_x2(verb->auth, verb->session);

		if (AKA_mark("lis===138###sois===3072###eois===3073###lif===10###soif===268###eoif===269###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)") && (AKA_mark("lis===138###sois===3072###eois===3073###lif===10###soif===268###eoif===269###isc===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)")&&a)) {
		AKA_mark("lis===139###sois===3077###eois===3123###lif===11###soif===273###eoif===319###ins===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)");json_object_object_add(g, "x-permissions", a);
	}
	else {AKA_mark("lis===-138-###sois===-3072-###eois===-30721-###lif===-10-###soif===-###eoif===-269-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)");}


		AKA_mark("lis===141###sois===3126###eois===3155###lif===13###soif===322###eoif===351###ins===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)");a = json_object_new_object();

		AKA_mark("lis===142###sois===3157###eois===3199###lif===14###soif===353###eoif===395###ins===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)");json_object_object_add(g, "responses", a);

		AKA_mark("lis===143###sois===3201###eois===3230###lif===15###soif===397###eoif===426###ins===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)");g = json_object_new_object();

		AKA_mark("lis===144###sois===3232###eois===3268###lif===16###soif===428###eoif===464###ins===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)");json_object_object_add(a, "200", g);

		AKA_mark("lis===145###sois===3270###eois===3359###lif===17###soif===466###eoif===555###ins===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)");json_object_object_add(g, "description", json_object_new_string(verb->info?:verb->verb));


		AKA_mark("lis===147###sois===3362###eois===3371###lif===19###soif===558###eoif===567###ins===true###function===./app-framework-binder/src/afb-api-v3.c/describe_verb_v3(const struct afb_verb_v3*)");return f;

}

/** Instrumented function afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*) */
struct json_object *afb_api_v3_make_description_openAPIv3(struct afb_api_v3 *api, const char *apiname)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");AKA_fCall++;
		AKA_mark("lis===152###sois===3481###eois===3498###lif===2###soif===106###eoif===123###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");char buffer[256];

		AKA_mark("lis===153###sois===3500###eois===3533###lif===3###soif===125###eoif===158###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");struct afb_verb_v3 **iter, **end;

		AKA_mark("lis===154###sois===3535###eois===3566###lif===4###soif===160###eoif===191###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");const struct afb_verb_v3 *verb;

		AKA_mark("lis===155###sois===3568###eois===3598###lif===5###soif===193###eoif===223###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");struct json_object *r, *i, *p;


		AKA_mark("lis===157###sois===3601###eois===3630###lif===7###soif===226###eoif===255###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");r = json_object_new_object();

		AKA_mark("lis===158###sois===3632###eois===3702###lif===8###soif===257###eoif===327###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");json_object_object_add(r, "openapi", json_object_new_string("3.0.0"));


		AKA_mark("lis===160###sois===3705###eois===3734###lif===10###soif===330###eoif===359###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");i = json_object_new_object();

		AKA_mark("lis===161###sois===3736###eois===3773###lif===11###soif===361###eoif===398###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");json_object_object_add(r, "info", i);

		AKA_mark("lis===162###sois===3775###eois===3843###lif===12###soif===400###eoif===468###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");json_object_object_add(i, "title", json_object_new_string(apiname));

		AKA_mark("lis===163###sois===3845###eois===3915###lif===13###soif===470###eoif===540###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");json_object_object_add(i, "version", json_object_new_string("0.0.0"));

		AKA_mark("lis===164###sois===3917###eois===3993###lif===14###soif===542###eoif===618###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");json_object_object_add(i, "description", json_object_new_string(api->info));


		AKA_mark("lis===166###sois===3996###eois===4012###lif===16###soif===621###eoif===637###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");buffer[0] = '/';

		AKA_mark("lis===167###sois===4014###eois===4044###lif===17###soif===639###eoif===669###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");buffer[sizeof buffer - 1] = 0;


		AKA_mark("lis===169###sois===4047###eois===4076###lif===19###soif===672###eoif===701###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");p = json_object_new_object();

		AKA_mark("lis===170###sois===4078###eois===4116###lif===20###soif===703###eoif===741###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");json_object_object_add(r, "paths", p);

		AKA_mark("lis===171###sois===4118###eois===4136###lif===21###soif===743###eoif===761###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");iter = api->verbs;

		AKA_mark("lis===172###sois===4138###eois===4162###lif===22###soif===763###eoif===787###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");end = iter + api->count;

		while (AKA_mark("lis===173###sois===4171###eois===4182###lif===23###soif===796###eoif===807###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)") && (AKA_mark("lis===173###sois===4171###eois===4182###lif===23###soif===796###eoif===807###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)")&&iter != end)) {
				AKA_mark("lis===174###sois===4188###eois===4203###lif===24###soif===813###eoif===828###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");verb = *iter++;

				AKA_mark("lis===175###sois===4206###eois===4257###lif===25###soif===831###eoif===882###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");strncpy(buffer + 1, verb->verb, sizeof buffer - 2);

				AKA_mark("lis===176###sois===4260###eois===4318###lif===26###soif===885###eoif===943###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");json_object_object_add(p, buffer, describe_verb_v3(verb));

	}

		AKA_mark("lis===178###sois===4323###eois===4343###lif===28###soif===948###eoif===968###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");verb = api->verbsv3;

		if (AKA_mark("lis===179###sois===4349###eois===4353###lif===29###soif===974###eoif===978###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)") && (AKA_mark("lis===179###sois===4349###eois===4353###lif===29###soif===974###eoif===978###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)")&&verb)) {
		while (AKA_mark("lis===180###sois===4363###eois===4373###lif===30###soif===988###eoif===998###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)") && (AKA_mark("lis===180###sois===4363###eois===4373###lif===30###soif===988###eoif===998###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)")&&verb->verb)) {
						AKA_mark("lis===181###sois===4380###eois===4431###lif===31###soif===1005###eoif===1056###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");strncpy(buffer + 1, verb->verb, sizeof buffer - 2);

						AKA_mark("lis===182###sois===4435###eois===4493###lif===32###soif===1060###eoif===1118###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");json_object_object_add(p, buffer, describe_verb_v3(verb));

						AKA_mark("lis===183###sois===4497###eois===4504###lif===33###soif===1122###eoif===1129###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");verb++;

		}
	}
	else {AKA_mark("lis===-179-###sois===-4349-###eois===-43494-###lif===-29-###soif===-###eoif===-978-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");}

		AKA_mark("lis===185###sois===4510###eois===4519###lif===35###soif===1135###eoif===1144###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_make_description_openAPIv3(struct afb_api_v3*,const char*)");return r;

}

/** Instrumented function afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*) */
struct afb_api_v3 *afb_api_v3_create(struct afb_apiset *declare_set,
		struct afb_apiset *call_set,
		const char *apiname,
		const char *info,
		int noconcurrency,
		int (*preinit)(void*, struct afb_api_x3 *),
		void *closure,
		int copy_info,
		struct afb_export* creator,
		const char* path)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");AKA_fCall++;
		AKA_mark("lis===199###sois===4820###eois===4843###lif===11###soif===297###eoif===320###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");struct afb_api_v3 *api;


	/* allocates the description */
		AKA_mark("lis===202###sois===4879###eois===4953###lif===14###soif===356###eoif===430###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");api = calloc(1, sizeof *api + (copy_info && info ? 1 + strlen(info) : 0));

		if (AKA_mark("lis===203###sois===4959###eois===4963###lif===15###soif===436###eoif===440###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)") && (AKA_mark("lis===203###sois===4959###eois===4963###lif===15###soif===436###eoif===440###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)")&&!api)) {
				AKA_mark("lis===204###sois===4969###eois===4992###lif===16###soif===446###eoif===469###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");ERROR("out of memory");

				AKA_mark("lis===205###sois===4995###eois===5004###lif===17###soif===472###eoif===481###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");goto oom;

	}
	else {AKA_mark("lis===-203-###sois===-4959-###eois===-49594-###lif===-15-###soif===-###eoif===-440-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");}

		AKA_mark("lis===207###sois===5009###eois===5027###lif===19###soif===486###eoif===504###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");api->refcount = 1;

		if (AKA_mark("lis===208###sois===5033###eois===5038###lif===20###soif===510###eoif===515###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)") && (AKA_mark("lis===208###sois===5033###eois===5038###lif===20###soif===510###eoif===515###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)")&&!info)) {
		AKA_mark("lis===209###sois===5042###eois===5063###lif===21###soif===519###eoif===540###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");api->info = &nulchar;
	}
	else {
		if (AKA_mark("lis===210###sois===5074###eois===5083###lif===22###soif===551###eoif===560###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)") && (AKA_mark("lis===210###sois===5074###eois===5083###lif===22###soif===551###eoif===560###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)")&&copy_info)) {
			AKA_mark("lis===211###sois===5087###eois===5130###lif===23###soif===564###eoif===607###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");api->info = strcpy((char*)(api + 1), info);
		}
		else {
			AKA_mark("lis===213###sois===5139###eois===5156###lif===25###soif===616###eoif===633###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");api->info = info;
		}
	}


	/* Cant instrument this following code */
api->export = afb_export_create_v3(declare_set, call_set, apiname, api, creator, path);
		if (AKA_mark("lis===216###sois===5252###eois===5264###lif===28###soif===729###eoif===741###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)") && (AKA_mark("lis===216###sois===5252###eois===5264###lif===28###soif===729###eoif===741###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)")&&!api->export)) {
		AKA_mark("lis===217###sois===5268###eois===5278###lif===29###soif===745###eoif===755###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");goto oom2;
	}
	else {AKA_mark("lis===-216-###sois===-5252-###eois===-525212-###lif===-28-###soif===-###eoif===-741-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");}


		if (AKA_mark("lis===219###sois===5285###eois===5335###lif===31###soif===762###eoif===812###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)") && (AKA_mark("lis===219###sois===5285###eois===5335###lif===31###soif===762###eoif===812###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)")&&afb_export_declare(api->export, noconcurrency) < 0)) {
		AKA_mark("lis===220###sois===5339###eois===5349###lif===32###soif===816###eoif===826###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");goto oom3;
	}
	else {AKA_mark("lis===-219-###sois===-5285-###eois===-528550-###lif===-31-###soif===-###eoif===-812-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");}


		if (AKA_mark("lis===222###sois===5356###eois===5423###lif===34###soif===833###eoif===900###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)") && (AKA_mark("lis===222###sois===5356###eois===5423###lif===34###soif===833###eoif===900###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)")&&preinit && afb_export_preinit_x3(api->export, preinit, closure) < 0)) {
		AKA_mark("lis===223###sois===5427###eois===5437###lif===35###soif===904###eoif===914###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");goto oom4;
	}
	else {AKA_mark("lis===-222-###sois===-5356-###eois===-535667-###lif===-34-###soif===-###eoif===-900-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");}


		AKA_mark("lis===225###sois===5440###eois===5451###lif===37###soif===917###eoif===928###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");return api;


/* Cant instrument this following code */
oom4:
	afb_export_undeclare(api->export);
/* Cant instrument this following code */
oom3:
	afb_export_unref(api->export);
	oom2:
	AKA_mark("lis===232###sois===5540###eois===5550###lif===44###soif===1017###eoif===1027###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");free(api);

	oom:
	AKA_mark("lis===234###sois===5557###eois===5569###lif===46###soif===1034###eoif===1046###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*,int,struct afb_export*,const char*)");return NULL;

}

/** Instrumented function afb_api_v3_addref(struct afb_api_v3*) */
struct afb_api_v3 *afb_api_v3_addref(struct afb_api_v3 *api)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/afb_api_v3_addref(struct afb_api_v3*)");AKA_fCall++;
		if (AKA_mark("lis===239###sois===5641###eois===5644###lif===2###soif===68###eoif===71###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_addref(struct afb_api_v3*)") && (AKA_mark("lis===239###sois===5641###eois===5644###lif===2###soif===68###eoif===71###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_addref(struct afb_api_v3*)")&&api)) {
		AKA_mark("lis===240###sois===5648###eois===5704###lif===3###soif===75###eoif===131###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_addref(struct afb_api_v3*)");__atomic_add_fetch(&api->refcount, 1, __ATOMIC_RELAXED);
	}
	else {AKA_mark("lis===-239-###sois===-5641-###eois===-56413-###lif===-2-###soif===-###eoif===-71-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_addref(struct afb_api_v3*)");}

		AKA_mark("lis===241###sois===5706###eois===5717###lif===4###soif===133###eoif===144###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_addref(struct afb_api_v3*)");return api;

}

/** Instrumented function afb_api_v3_unref(struct afb_api_v3*) */
void afb_api_v3_unref(struct afb_api_v3 *api)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/afb_api_v3_unref(struct afb_api_v3*)");AKA_fCall++;
		if (AKA_mark("lis===246###sois===5774###eois===5837###lif===2###soif===53###eoif===116###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_unref(struct afb_api_v3*)") && ((AKA_mark("lis===246###sois===5774###eois===5777###lif===2###soif===53###eoif===56###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_unref(struct afb_api_v3*)")&&api)	&&(AKA_mark("lis===246###sois===5781###eois===5837###lif===2###soif===60###eoif===116###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_unref(struct afb_api_v3*)")&&!__atomic_sub_fetch(&api->refcount, 1, __ATOMIC_RELAXED)))) {
		/* Cant instrument this following code */
afb_export_destroy(api->export);
				while (AKA_mark("lis===248###sois===5885###eois===5895###lif===4###soif===164###eoif===174###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_unref(struct afb_api_v3*)") && (AKA_mark("lis===248###sois===5885###eois===5895###lif===4###soif===164###eoif===174###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_unref(struct afb_api_v3*)")&&api->count)) {
			AKA_mark("lis===249###sois===5900###eois===5931###lif===5###soif===179###eoif===210###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_unref(struct afb_api_v3*)");free(api->verbs[--api->count]);
		}

				AKA_mark("lis===250###sois===5934###eois===5951###lif===6###soif===213###eoif===230###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_unref(struct afb_api_v3*)");free(api->verbs);

				AKA_mark("lis===251###sois===5954###eois===5964###lif===7###soif===233###eoif===243###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_unref(struct afb_api_v3*)");free(api);

	}
	else {AKA_mark("lis===-246-###sois===-5774-###eois===-577463-###lif===-2-###soif===-###eoif===-116-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_unref(struct afb_api_v3*)");}

}

/** Instrumented function afb_api_v3_export(struct afb_api_v3*) */
struct afb_export *afb_api_v3_export(struct afb_api_v3 *api)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/afb_api_v3_export(struct afb_api_v3*)");AKA_fCall++;
	/* Cant instrument this following code */
return api->export;
}

#if WITH_LEGACY_BINDING_V2
void afb_api_v3_set_verbs_v2(
		struct afb_api_v3 *api,
		const struct afb_verb_v2 *verbs)
{
	api->verbsv2 = verbs;
}
#endif

/** Instrumented function afb_api_v3_set_verbs_v3(struct afb_api_v3*,const struct afb_verb_v3*) */
void afb_api_v3_set_verbs_v3(
		struct afb_api_v3 *api,
		const struct afb_verb_v3 *verbs)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_verbs_v3(struct afb_api_v3*,const struct afb_verb_v3*)");AKA_fCall++;
		AKA_mark("lis===273###sois===6305###eois===6326###lif===4###soif===94###eoif===115###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_verbs_v3(struct afb_api_v3*,const struct afb_verb_v3*)");api->verbsv3 = verbs;

}

/** Instrumented function afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int) */
int afb_api_v3_add_verb(
		struct afb_api_v3 *api,
		const char *verb,
		const char *info,
		void (*callback)(struct afb_req_x2 *req),
		void *vcbdata,
		const struct afb_auth *auth,
		uint16_t session,
		int glob)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");AKA_fCall++;
		AKA_mark("lis===286###sois===6548###eois===6576###lif===10###soif===218###eoif===246###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");struct afb_verb_v3 *v, **vv;

		AKA_mark("lis===287###sois===6578###eois===6588###lif===11###soif===248###eoif===258###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");char *txt;

		AKA_mark("lis===288###sois===6590###eois===6596###lif===12###soif===260###eoif===266###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");int i;


		AKA_mark("lis===290###sois===6604###eois===6611###lif===14###soif===274###eoif===281###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");for (i = 0 ;AKA_mark("lis===290###sois===6612###eois===6626###lif===14###soif===282###eoif===296###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)") && AKA_mark("lis===290###sois===6612###eois===6626###lif===14###soif===282###eoif===296###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)")&&i < api->count;({AKA_mark("lis===290###sois===6629###eois===6632###lif===14###soif===299###eoif===302###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");i++;})) {
				AKA_mark("lis===291###sois===6638###eois===6656###lif===15###soif===308###eoif===326###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");v = api->verbs[i];

				if (AKA_mark("lis===292###sois===6663###eois===6708###lif===16###soif===333###eoif===378###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)") && ((AKA_mark("lis===292###sois===6663###eois===6678###lif===16###soif===333###eoif===348###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)")&&glob == v->glob)	&&(AKA_mark("lis===292###sois===6682###eois===6708###lif===16###soif===352###eoif===378###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)")&&!strcasecmp(verb, v->verb)))) {
			/* refuse to redefine a dynamic verb */
						AKA_mark("lis===294###sois===6758###eois===6773###lif===18###soif===428###eoif===443###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");errno = EEXIST;

						AKA_mark("lis===295###sois===6777###eois===6787###lif===19###soif===447###eoif===457###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");return -1;

		}
		else {AKA_mark("lis===-292-###sois===-6663-###eois===-666345-###lif===-16-###soif===-###eoif===-378-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");}

	}


		AKA_mark("lis===299###sois===6797###eois===6853###lif===23###soif===467###eoif===523###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");vv = realloc(api->verbs, (1 + api->count) * sizeof *vv);

		if (AKA_mark("lis===300###sois===6859###eois===6862###lif===24###soif===529###eoif===532###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)") && (AKA_mark("lis===300###sois===6859###eois===6862###lif===24###soif===529###eoif===532###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)")&&!vv)) {
		AKA_mark("lis===301###sois===6866###eois===6875###lif===25###soif===536###eoif===545###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");goto oom;
	}
	else {AKA_mark("lis===-300-###sois===-6859-###eois===-68593-###lif===-24-###soif===-###eoif===-532-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");}

		AKA_mark("lis===302###sois===6877###eois===6893###lif===26###soif===547###eoif===563###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");api->verbs = vv;


		AKA_mark("lis===304###sois===6896###eois===6971###lif===28###soif===566###eoif===641###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");v = malloc(sizeof *v + (1 + strlen(verb)) + (info ? 1 + strlen(info) : 0));

		if (AKA_mark("lis===305###sois===6977###eois===6979###lif===29###soif===647###eoif===649###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)") && (AKA_mark("lis===305###sois===6977###eois===6979###lif===29###soif===647###eoif===649###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)")&&!v)) {
		AKA_mark("lis===306###sois===6983###eois===6992###lif===30###soif===653###eoif===662###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");goto oom;
	}
	else {AKA_mark("lis===-305-###sois===-6977-###eois===-69772-###lif===-29-###soif===-###eoif===-649-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");}


		AKA_mark("lis===308###sois===6995###eois===7018###lif===32###soif===665###eoif===688###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");v->callback = callback;

		AKA_mark("lis===309###sois===7020###eois===7041###lif===33###soif===690###eoif===711###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");v->vcbdata = vcbdata;

		AKA_mark("lis===310###sois===7043###eois===7058###lif===34###soif===713###eoif===728###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");v->auth = auth;

		AKA_mark("lis===311###sois===7060###eois===7081###lif===35###soif===730###eoif===751###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");v->session = session;

		AKA_mark("lis===312###sois===7083###eois===7100###lif===36###soif===753###eoif===770###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");v->glob = !!glob;


		AKA_mark("lis===314###sois===7103###eois===7124###lif===38###soif===773###eoif===794###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");txt = (char*)(v + 1);

		AKA_mark("lis===315###sois===7126###eois===7140###lif===39###soif===796###eoif===810###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");v->verb = txt;

		AKA_mark("lis===316###sois===7142###eois===7166###lif===40###soif===812###eoif===836###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");txt = stpcpy(txt, verb);

		if (AKA_mark("lis===317###sois===7172###eois===7177###lif===41###soif===842###eoif===847###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)") && (AKA_mark("lis===317###sois===7172###eois===7177###lif===41###soif===842###eoif===847###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)")&&!info)) {
		AKA_mark("lis===318###sois===7181###eois===7196###lif===42###soif===851###eoif===866###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");v->info = NULL;
	}
	else {
				AKA_mark("lis===320###sois===7207###eois===7223###lif===44###soif===877###eoif===893###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");v->info = ++txt;

				AKA_mark("lis===321###sois===7226###eois===7244###lif===45###soif===896###eoif===914###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");strcpy(txt, info);

	}


		AKA_mark("lis===324###sois===7250###eois===7279###lif===48###soif===920###eoif===949###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");api->verbs[api->count++] = v;

		AKA_mark("lis===325###sois===7281###eois===7290###lif===49###soif===951###eoif===960###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");return 0;

	oom:
	AKA_mark("lis===327###sois===7297###eois===7312###lif===51###soif===967###eoif===982###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");errno = ENOMEM;

		AKA_mark("lis===328###sois===7314###eois===7324###lif===52###soif===984###eoif===994###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_add_verb(struct afb_api_v3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint16_t,int)");return -1;

}

/** Instrumented function afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**) */
int afb_api_v3_del_verb(
		struct afb_api_v3 *api,
		const char *verb,
		void **vcbdata)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)");AKA_fCall++;
		AKA_mark("lis===336###sois===7420###eois===7442###lif===5###soif===92###eoif===114###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)");struct afb_verb_v3 *v;

		AKA_mark("lis===337###sois===7444###eois===7450###lif===6###soif===116###eoif===122###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)");int i;


		AKA_mark("lis===339###sois===7458###eois===7465###lif===8###soif===130###eoif===137###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)");for (i = 0 ;AKA_mark("lis===339###sois===7466###eois===7480###lif===8###soif===138###eoif===152###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)") && AKA_mark("lis===339###sois===7466###eois===7480###lif===8###soif===138###eoif===152###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)")&&i < api->count;({AKA_mark("lis===339###sois===7483###eois===7486###lif===8###soif===155###eoif===158###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)");i++;})) {
				AKA_mark("lis===340###sois===7492###eois===7510###lif===9###soif===164###eoif===182###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)");v = api->verbs[i];

				if (AKA_mark("lis===341###sois===7517###eois===7543###lif===10###soif===189###eoif===215###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)") && (AKA_mark("lis===341###sois===7517###eois===7543###lif===10###soif===189###eoif===215###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)")&&!strcasecmp(verb, v->verb))) {
						AKA_mark("lis===342###sois===7550###eois===7591###lif===11###soif===222###eoif===263###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)");api->verbs[i] = api->verbs[--api->count];

						if (AKA_mark("lis===343###sois===7599###eois===7606###lif===12###soif===271###eoif===278###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)") && (AKA_mark("lis===343###sois===7599###eois===7606###lif===12###soif===271###eoif===278###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)")&&vcbdata)) {
				AKA_mark("lis===344###sois===7612###eois===7634###lif===13###soif===284###eoif===306###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)");*vcbdata = v->vcbdata;
			}
			else {AKA_mark("lis===-343-###sois===-7599-###eois===-75997-###lif===-12-###soif===-###eoif===-278-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)");}

						AKA_mark("lis===345###sois===7638###eois===7646###lif===14###soif===310###eoif===318###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)");free(v);

						AKA_mark("lis===346###sois===7650###eois===7659###lif===15###soif===322###eoif===331###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)");return 0;

		}
		else {AKA_mark("lis===-341-###sois===-7517-###eois===-751726-###lif===-10-###soif===-###eoif===-215-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)");}

	}


		AKA_mark("lis===350###sois===7669###eois===7684###lif===19###soif===341###eoif===356###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)");errno = ENOENT;

		AKA_mark("lis===351###sois===7686###eois===7696###lif===20###soif===358###eoif===368###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_del_verb(struct afb_api_v3*,const char*,void**)");return -1;

}

/** Instrumented function afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*) */
int afb_api_v3_set_binding_fields(const struct afb_binding_v3 *desc, struct afb_api_x3 *api)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");AKA_fCall++;
		AKA_mark("lis===356###sois===7796###eois===7807###lif===2###soif===96###eoif===107###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");int rc = 0;

		if (AKA_mark("lis===357###sois===7813###eois===7824###lif===3###soif===113###eoif===124###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)") && (AKA_mark("lis===357###sois===7813###eois===7824###lif===3###soif===113###eoif===124###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)")&&desc->verbs)) {
		AKA_mark("lis===358###sois===7828###eois===7876###lif===4###soif===128###eoif===176###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");rc =  afb_api_x3_set_verbs_v3(api, desc->verbs);
	}
	else {AKA_mark("lis===-357-###sois===-7813-###eois===-781311-###lif===-3-###soif===-###eoif===-124-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");}

		if (AKA_mark("lis===359###sois===7882###eois===7902###lif===5###soif===182###eoif===202###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)") && ((AKA_mark("lis===359###sois===7882###eois===7885###lif===5###soif===182###eoif===185###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)")&&!rc)	&&(AKA_mark("lis===359###sois===7889###eois===7902###lif===5###soif===189###eoif===202###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)")&&desc->onevent))) {
		AKA_mark("lis===360###sois===7906###eois===7952###lif===6###soif===206###eoif===252###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");rc =  afb_api_x3_on_event(api, desc->onevent);
	}
	else {AKA_mark("lis===-359-###sois===-7882-###eois===-788220-###lif===-5-###soif===-###eoif===-202-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");}

		if (AKA_mark("lis===361###sois===7958###eois===7975###lif===7###soif===258###eoif===275###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)") && ((AKA_mark("lis===361###sois===7958###eois===7961###lif===7###soif===258###eoif===261###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)")&&!rc)	&&(AKA_mark("lis===361###sois===7965###eois===7975###lif===7###soif===265###eoif===275###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)")&&desc->init))) {
		AKA_mark("lis===362###sois===7979###eois===8021###lif===8###soif===279###eoif===321###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");rc =  afb_api_x3_on_init(api, desc->init);
	}
	else {AKA_mark("lis===-361-###sois===-7958-###eois===-795817-###lif===-7-###soif===-###eoif===-275-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");}

		if (AKA_mark("lis===363###sois===8027###eois===8053###lif===9###soif===327###eoif===353###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)") && ((AKA_mark("lis===363###sois===8027###eois===8030###lif===9###soif===327###eoif===330###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)")&&!rc)	&&(AKA_mark("lis===363###sois===8034###eois===8053###lif===9###soif===334###eoif===353###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)")&&desc->provide_class))) {
		AKA_mark("lis===364###sois===8057###eois===8114###lif===10###soif===357###eoif===414###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");rc =  afb_api_x3_provide_class(api, desc->provide_class);
	}
	else {AKA_mark("lis===-363-###sois===-8027-###eois===-802726-###lif===-9-###soif===-###eoif===-353-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");}

		if (AKA_mark("lis===365###sois===8120###eois===8146###lif===11###soif===420###eoif===446###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)") && ((AKA_mark("lis===365###sois===8120###eois===8123###lif===11###soif===420###eoif===423###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)")&&!rc)	&&(AKA_mark("lis===365###sois===8127###eois===8146###lif===11###soif===427###eoif===446###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)")&&desc->require_class))) {
		AKA_mark("lis===366###sois===8150###eois===8207###lif===12###soif===450###eoif===507###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");rc =  afb_api_x3_require_class(api, desc->require_class);
	}
	else {AKA_mark("lis===-365-###sois===-8120-###eois===-812026-###lif===-11-###soif===-###eoif===-446-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");}

		if (AKA_mark("lis===367###sois===8213###eois===8237###lif===13###soif===513###eoif===537###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)") && ((AKA_mark("lis===367###sois===8213###eois===8216###lif===13###soif===513###eoif===516###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)")&&!rc)	&&(AKA_mark("lis===367###sois===8220###eois===8237###lif===13###soif===520###eoif===537###isc===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)")&&desc->require_api))) {
		AKA_mark("lis===368###sois===8241###eois===8297###lif===14###soif===541###eoif===597###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");rc =  afb_api_x3_require_api(api, desc->require_api, 0);
	}
	else {AKA_mark("lis===-367-###sois===-8213-###eois===-821324-###lif===-13-###soif===-###eoif===-537-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");}

		AKA_mark("lis===369###sois===8299###eois===8309###lif===15###soif===599###eoif===609###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_set_binding_fields(const struct afb_binding_v3*,struct afb_api_x3*)");return rc;

}

struct safe_preinit_data
{
	int (*preinit)(struct afb_api_x3 *);
	struct afb_api_x3 *api;
	int result;
};

/** Instrumented function safe_preinit(int,void*) */
static void safe_preinit(int sig, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/safe_preinit(int,void*)");AKA_fCall++;
		AKA_mark("lis===381###sois===8472###eois===8512###lif===2###soif===52###eoif===92###ins===true###function===./app-framework-binder/src/afb-api-v3.c/safe_preinit(int,void*)");struct safe_preinit_data *spd = closure;

		if (AKA_mark("lis===382###sois===8518###eois===8522###lif===3###soif===98###eoif===102###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/safe_preinit(int,void*)") && (AKA_mark("lis===382###sois===8518###eois===8522###lif===3###soif===98###eoif===102###isc===true###function===./app-framework-binder/src/afb-api-v3.c/safe_preinit(int,void*)")&&!sig)) {
		AKA_mark("lis===383###sois===8526###eois===8563###lif===4###soif===106###eoif===143###ins===true###function===./app-framework-binder/src/afb-api-v3.c/safe_preinit(int,void*)");spd->result = spd->preinit(spd->api);
	}
	else {
				AKA_mark("lis===385###sois===8574###eois===8591###lif===6###soif===154###eoif===171###ins===true###function===./app-framework-binder/src/afb-api-v3.c/safe_preinit(int,void*)");spd->result = -1;

				AKA_mark("lis===386###sois===8594###eois===8609###lif===7###soif===174###eoif===189###ins===true###function===./app-framework-binder/src/afb-api-v3.c/safe_preinit(int,void*)");errno = EFAULT;

	}

}

/** Instrumented function afb_api_v3_safe_preinit(struct afb_api_x3*,int(*preinit)(struct afb_api_x3*)) */
int afb_api_v3_safe_preinit(struct afb_api_x3 *api, int (*preinit)(struct afb_api_x3 *))
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/afb_api_v3_safe_preinit(struct afb_api_x3*,int(*preinit)(struct afb_api_x3*))");AKA_fCall++;
		AKA_mark("lis===392###sois===8708###eois===8737###lif===2###soif===92###eoif===121###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_safe_preinit(struct afb_api_x3*,int(*preinit)(struct afb_api_x3*))");struct safe_preinit_data spd;


		AKA_mark("lis===394###sois===8740###eois===8762###lif===4###soif===124###eoif===146###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_safe_preinit(struct afb_api_x3*,int(*preinit)(struct afb_api_x3*))");spd.preinit = preinit;

		AKA_mark("lis===395###sois===8764###eois===8778###lif===5###soif===148###eoif===162###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_safe_preinit(struct afb_api_x3*,int(*preinit)(struct afb_api_x3*))");spd.api = api;

		AKA_mark("lis===396###sois===8780###eois===8816###lif===6###soif===164###eoif===200###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_safe_preinit(struct afb_api_x3*,int(*preinit)(struct afb_api_x3*))");sig_monitor(60, safe_preinit, &spd);

		AKA_mark("lis===397###sois===8818###eois===8836###lif===7###soif===202###eoif===220###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_safe_preinit(struct afb_api_x3*,int(*preinit)(struct afb_api_x3*))");return spd.result;

}

/** Instrumented function init_binding(void*,struct afb_api_x3*) */
static int init_binding(void *closure, struct afb_api_x3 *api)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/init_binding(void*,struct afb_api_x3*)");AKA_fCall++;
		AKA_mark("lis===402###sois===8906###eois===8950###lif===2###soif===66###eoif===110###ins===true###function===./app-framework-binder/src/afb-api-v3.c/init_binding(void*,struct afb_api_x3*)");const struct afb_binding_v3 *desc = closure;

		AKA_mark("lis===403###sois===8952###eois===9002###lif===3###soif===112###eoif===162###ins===true###function===./app-framework-binder/src/afb-api-v3.c/init_binding(void*,struct afb_api_x3*)");int rc = afb_api_v3_set_binding_fields(desc, api);

		if (AKA_mark("lis===404###sois===9008###eois===9028###lif===4###soif===168###eoif===188###ifc===true###function===./app-framework-binder/src/afb-api-v3.c/init_binding(void*,struct afb_api_x3*)") && ((AKA_mark("lis===404###sois===9008###eois===9011###lif===4###soif===168###eoif===171###isc===true###function===./app-framework-binder/src/afb-api-v3.c/init_binding(void*,struct afb_api_x3*)")&&!rc)	&&(AKA_mark("lis===404###sois===9015###eois===9028###lif===4###soif===175###eoif===188###isc===true###function===./app-framework-binder/src/afb-api-v3.c/init_binding(void*,struct afb_api_x3*)")&&desc->preinit))) {
		AKA_mark("lis===405###sois===9032###eois===9081###lif===5###soif===192###eoif===241###ins===true###function===./app-framework-binder/src/afb-api-v3.c/init_binding(void*,struct afb_api_x3*)");rc = afb_api_v3_safe_preinit(api, desc->preinit);
	}
	else {AKA_mark("lis===-404-###sois===-9008-###eois===-900820-###lif===-4-###soif===-###eoif===-188-###ins===true###function===./app-framework-binder/src/afb-api-v3.c/init_binding(void*,struct afb_api_x3*)");}

		AKA_mark("lis===406###sois===9083###eois===9093###lif===6###soif===243###eoif===253###ins===true###function===./app-framework-binder/src/afb-api-v3.c/init_binding(void*,struct afb_api_x3*)");return rc;

}

/** Instrumented function afb_api_v3_from_binding(const struct afb_binding_v3*,struct afb_apiset*,struct afb_apiset*) */
struct afb_api_v3 *afb_api_v3_from_binding(const struct afb_binding_v3 *desc, struct afb_apiset *declare_set, struct afb_apiset * call_set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-v3.c/afb_api_v3_from_binding(const struct afb_binding_v3*,struct afb_apiset*,struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===411###sois===9240###eois===9374###lif===2###soif===143###eoif===277###ins===true###function===./app-framework-binder/src/afb-api-v3.c/afb_api_v3_from_binding(const struct afb_binding_v3*,struct afb_apiset*,struct afb_apiset*)");return afb_api_v3_create(declare_set, call_set, desc->api, desc->info, desc->noconcurrency, init_binding, (void*)desc, 0, NULL, NULL);

}


#endif

