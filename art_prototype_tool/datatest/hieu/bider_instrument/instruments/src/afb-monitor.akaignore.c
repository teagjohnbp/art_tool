/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_MONITOR_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_MONITOR_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <string.h>

#include <json-c/json.h>

#define AFB_BINDING_VERSION 3
#define AFB_BINDING_NO_ROOT
#include <afb/afb-binding.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_H_
#define AKA_INCLUDE__AFB_API_H_
#include "afb-api.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_APISET_H_
#define AKA_INCLUDE__AFB_APISET_H_
#include "afb-apiset.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_V3_H_
#define AKA_INCLUDE__AFB_API_V3_H_
#include "afb-api-v3.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_EVT_H_
#define AKA_INCLUDE__AFB_EVT_H_
#include "afb-evt.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_XREQ_H_
#define AKA_INCLUDE__AFB_XREQ_H_
#include "afb-xreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_TRACE_H_
#define AKA_INCLUDE__AFB_TRACE_H_
#include "afb-trace.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SESSION_H_
#define AKA_INCLUDE__AFB_SESSION_H_
#include "afb-session.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_ERROR_TEXT_H_
#define AKA_INCLUDE__AFB_ERROR_TEXT_H_
#include "afb-error-text.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__WRAP_JSON_H_
#define AKA_INCLUDE__WRAP_JSON_H_
#include "wrap-json.akaignore.h"
#endif


/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__MONITOR_API_INC_
#define AKA_INCLUDE__MONITOR_API_INC_
#include "monitor-api.inc"
#endif



static const char _verbosity_[] = "verbosity";
static const char _apis_[] = "apis";

static const char _debug_[] = "debug";
static const char _info_[] = "info";
static const char _notice_[] = "notice";
static const char _warning_[] = "warning";
static const char _error_[] = "error";


static struct afb_apiset *target_set;

/** Instrumented function afb_monitor_init(struct afb_apiset*,struct afb_apiset*) */
int afb_monitor_init(struct afb_apiset *declare_set, struct afb_apiset *call_set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/afb_monitor_init(struct afb_apiset*,struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===56###sois===1471###eois===1493###lif===2###soif===85###eoif===107###ins===true###function===./app-framework-binder/src/afb-monitor.c/afb_monitor_init(struct afb_apiset*,struct afb_apiset*)");target_set = call_set;

		AKA_mark("lis===57###sois===1495###eois===1574###lif===3###soif===109###eoif===188###ins===true###function===./app-framework-binder/src/afb-monitor.c/afb_monitor_init(struct afb_apiset*,struct afb_apiset*)");return -!afb_api_v3_from_binding(&_afb_binding_monitor, declare_set, call_set);

}

/******************************************************************************
**** Monitoring verbosity
******************************************************************************/

/**
 * Translate verbosity indication to an integer value.
 * @param v the verbosity indication
 * @return the verbosity level (0, 1, 2 or 3) or -1 in case of error
 */
/** Instrumented function decode_verbosity(struct json_object*) */
static int decode_verbosity(struct json_object *v)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");AKA_fCall++;
		AKA_mark("lis===71###sois===1988###eois===2002###lif===2###soif===54###eoif===68###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");const char *s;

		AKA_mark("lis===72###sois===2004###eois===2019###lif===3###soif===70###eoif===85###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");int level = -1;


		if (AKA_mark("lis===74###sois===2026###eois===2059###lif===5###soif===92###eoif===125###ifc===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)") && (AKA_mark("lis===74###sois===2026###eois===2059###lif===5###soif===92###eoif===125###isc===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)")&&!wrap_json_unpack(v, "i", &level))) {
				AKA_mark("lis===75###sois===2065###eois===2219###lif===6###soif===131###eoif===285###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");level = level < _VERBOSITY_(Log_Level_Error) ? _VERBOSITY_(Log_Level_Error) : level > _VERBOSITY_(Log_Level_Debug) ? _VERBOSITY_(Log_Level_Debug) : level;

	}
	else {
		if (AKA_mark("lis===76###sois===2232###eois===2261###lif===7###soif===298###eoif===327###ifc===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)") && (AKA_mark("lis===76###sois===2232###eois===2261###lif===7###soif===298###eoif===327###isc===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)")&&!wrap_json_unpack(v, "s", &s))) {
					AKA_mark("lis===77###sois===2274###eois===2281###lif===8###soif===340###eoif===347###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");switch(*s&~' '){
						case 'D': if(*s&~' ' == 'D')AKA_mark("lis===78###sois===2287###eois===2296###lif===9###soif===353###eoif===362###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");

							if (AKA_mark("lis===79###sois===2304###eois===2327###lif===10###soif===370###eoif===393###ifc===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)") && (AKA_mark("lis===79###sois===2304###eois===2327###lif===10###soif===370###eoif===393###isc===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)")&&!strcasecmp(s, _debug_))) {
					AKA_mark("lis===80###sois===2333###eois===2370###lif===11###soif===399###eoif===436###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");level = _VERBOSITY_(Log_Level_Debug);
				}
				else {AKA_mark("lis===-79-###sois===-2304-###eois===-230423-###lif===-10-###soif===-###eoif===-393-###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");}

							AKA_mark("lis===81###sois===2374###eois===2380###lif===12###soif===440###eoif===446###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");break;

						case 'I': if(*s&~' ' == 'I')AKA_mark("lis===82###sois===2383###eois===2392###lif===13###soif===449###eoif===458###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");

							if (AKA_mark("lis===83###sois===2400###eois===2422###lif===14###soif===466###eoif===488###ifc===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)") && (AKA_mark("lis===83###sois===2400###eois===2422###lif===14###soif===466###eoif===488###isc===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)")&&!strcasecmp(s, _info_))) {
					AKA_mark("lis===84###sois===2428###eois===2464###lif===15###soif===494###eoif===530###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");level = _VERBOSITY_(Log_Level_Info);
				}
				else {AKA_mark("lis===-83-###sois===-2400-###eois===-240022-###lif===-14-###soif===-###eoif===-488-###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");}

							AKA_mark("lis===85###sois===2468###eois===2474###lif===16###soif===534###eoif===540###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");break;

						case 'N': if(*s&~' ' == 'N')AKA_mark("lis===86###sois===2477###eois===2486###lif===17###soif===543###eoif===552###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");

							if (AKA_mark("lis===87###sois===2494###eois===2518###lif===18###soif===560###eoif===584###ifc===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)") && (AKA_mark("lis===87###sois===2494###eois===2518###lif===18###soif===560###eoif===584###isc===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)")&&!strcasecmp(s, _notice_))) {
					AKA_mark("lis===88###sois===2524###eois===2562###lif===19###soif===590###eoif===628###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");level = _VERBOSITY_(Log_Level_Notice);
				}
				else {AKA_mark("lis===-87-###sois===-2494-###eois===-249424-###lif===-18-###soif===-###eoif===-584-###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");}

							AKA_mark("lis===89###sois===2566###eois===2572###lif===20###soif===632###eoif===638###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");break;

						case 'W': if(*s&~' ' == 'W')AKA_mark("lis===90###sois===2575###eois===2584###lif===21###soif===641###eoif===650###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");

							if (AKA_mark("lis===91###sois===2592###eois===2617###lif===22###soif===658###eoif===683###ifc===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)") && (AKA_mark("lis===91###sois===2592###eois===2617###lif===22###soif===658###eoif===683###isc===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)")&&!strcasecmp(s, _warning_))) {
					AKA_mark("lis===92###sois===2623###eois===2662###lif===23###soif===689###eoif===728###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");level = _VERBOSITY_(Log_Level_Warning);
				}
				else {AKA_mark("lis===-91-###sois===-2592-###eois===-259225-###lif===-22-###soif===-###eoif===-683-###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");}

							AKA_mark("lis===93###sois===2666###eois===2672###lif===24###soif===732###eoif===738###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");break;

						case 'E': if(*s&~' ' == 'E')AKA_mark("lis===94###sois===2675###eois===2684###lif===25###soif===741###eoif===750###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");

							if (AKA_mark("lis===95###sois===2692###eois===2715###lif===26###soif===758###eoif===781###ifc===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)") && (AKA_mark("lis===95###sois===2692###eois===2715###lif===26###soif===758###eoif===781###isc===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)")&&!strcasecmp(s, _error_))) {
					AKA_mark("lis===96###sois===2721###eois===2758###lif===27###soif===787###eoif===824###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");level = _VERBOSITY_(Log_Level_Error);
				}
				else {AKA_mark("lis===-95-###sois===-2692-###eois===-269223-###lif===-26-###soif===-###eoif===-781-###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");}

							AKA_mark("lis===97###sois===2762###eois===2768###lif===28###soif===828###eoif===834###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");break;

		}

	}
		else {AKA_mark("lis===-76-###sois===-2232-###eois===-223229-###lif===-7-###soif===-###eoif===-327-###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");}
	}

		AKA_mark("lis===100###sois===2777###eois===2790###lif===31###soif===843###eoif===856###ins===true###function===./app-framework-binder/src/afb-monitor.c/decode_verbosity(struct json_object*)");return level;

}

/**
 * callback for setting verbosity on all apis
 * @param set the apiset
 * @param the name of the api to set
 * @param closure the verbosity to set as an integer casted to a pointer
 */
/** Instrumented function set_verbosity_to_all_cb(void*,struct afb_apiset*,const char*,int) */
static void set_verbosity_to_all_cb(void *closure, struct afb_apiset *set, const char *name, int isalias)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/set_verbosity_to_all_cb(void*,struct afb_apiset*,const char*,int)");AKA_fCall++;
		if (AKA_mark("lis===111###sois===3096###eois===3104###lif===2###soif===113###eoif===121###ifc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity_to_all_cb(void*,struct afb_apiset*,const char*,int)") && (AKA_mark("lis===111###sois===3096###eois===3104###lif===2###soif===113###eoif===121###isc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity_to_all_cb(void*,struct afb_apiset*,const char*,int)")&&!isalias)) {
		AKA_mark("lis===112###sois===3108###eois===3166###lif===3###soif===125###eoif===183###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity_to_all_cb(void*,struct afb_apiset*,const char*,int)");afb_apiset_set_logmask(set, name, (int)(intptr_t)closure);
	}
	else {AKA_mark("lis===-111-###sois===-3096-###eois===-30968-###lif===-2-###soif===-###eoif===-121-###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity_to_all_cb(void*,struct afb_apiset*,const char*,int)");}

}

/**
 * set the verbosity 'level' of the api of 'name'
 * @param name the api name or "*" for any api or NULL or "" for global verbosity
 * @param level the verbosity level to set
 */
/** Instrumented function set_verbosity_to(const char*,int) */
static void set_verbosity_to(const char *name, int level)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/set_verbosity_to(const char*,int)");AKA_fCall++;
		AKA_mark("lis===122###sois===3414###eois===3450###lif===2###soif===61###eoif===97###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity_to(const char*,int)");int mask = verbosity_to_mask(level);

		if (AKA_mark("lis===123###sois===3456###eois===3473###lif===3###soif===103###eoif===120###ifc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity_to(const char*,int)") && ((AKA_mark("lis===123###sois===3456###eois===3461###lif===3###soif===103###eoif===108###isc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity_to(const char*,int)")&&!name)	||(AKA_mark("lis===123###sois===3465###eois===3473###lif===3###soif===112###eoif===120###isc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity_to(const char*,int)")&&!name[0]))) {
		AKA_mark("lis===124###sois===3477###eois===3498###lif===4###soif===124###eoif===145###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity_to(const char*,int)");verbosity_set(level);
	}
	else {
		if (AKA_mark("lis===125###sois===3509###eois===3535###lif===5###soif===156###eoif===182###ifc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity_to(const char*,int)") && ((AKA_mark("lis===125###sois===3509###eois===3523###lif===5###soif===156###eoif===170###isc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity_to(const char*,int)")&&name[0] == '*')	&&(AKA_mark("lis===125###sois===3527###eois===3535###lif===5###soif===174###eoif===182###isc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity_to(const char*,int)")&&!name[1]))) {
			AKA_mark("lis===126###sois===3539###eois===3618###lif===6###soif===186###eoif===265###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity_to(const char*,int)");afb_apiset_enum(target_set, 1, set_verbosity_to_all_cb, (void*)(intptr_t)mask);
		}
		else {
			AKA_mark("lis===128###sois===3627###eois===3674###lif===8###soif===274###eoif===321###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity_to(const char*,int)");afb_apiset_set_logmask(target_set, name, mask);
		}
	}

}

/**
 * Set verbosities accordling to specification in 'spec'
 * @param spec specification of the verbosity to set
 */
/** Instrumented function set_verbosity(struct json_object*) */
static void set_verbosity(struct json_object *spec)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)");AKA_fCall++;
		AKA_mark("lis===137###sois===3851###eois===3857###lif===2###soif===55###eoif===61###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)");int l;

		AKA_mark("lis===138###sois===3859###eois===3895###lif===3###soif===63###eoif===99###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)");struct json_object_iterator it, end;


		if (AKA_mark("lis===140###sois===3902###eois===3945###lif===5###soif===106###eoif===149###ifc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)") && (AKA_mark("lis===140###sois===3902###eois===3945###lif===5###soif===106###eoif===149###isc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)")&&json_object_is_type(spec, json_type_object))) {
				AKA_mark("lis===141###sois===3951###eois===3985###lif===6###soif===155###eoif===189###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)");it = json_object_iter_begin(spec);

				AKA_mark("lis===142###sois===3988###eois===4021###lif===7###soif===192###eoif===225###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)");end = json_object_iter_end(spec);

				while (AKA_mark("lis===143###sois===4031###eois===4065###lif===8###soif===235###eoif===269###ifc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)") && (AKA_mark("lis===143###sois===4031###eois===4065###lif===8###soif===235###eoif===269###isc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)")&&!json_object_iter_equal(&it, &end))) {
						AKA_mark("lis===144###sois===4072###eois===4127###lif===9###soif===276###eoif===331###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)");l = decode_verbosity(json_object_iter_peek_value(&it));

						if (AKA_mark("lis===145###sois===4135###eois===4141###lif===10###soif===339###eoif===345###ifc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)") && (AKA_mark("lis===145###sois===4135###eois===4141###lif===10###soif===339###eoif===345###isc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)")&&l >= 0)) {
				AKA_mark("lis===146###sois===4147###eois===4200###lif===11###soif===351###eoif===404###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)");set_verbosity_to(json_object_iter_peek_name(&it), l);
			}
			else {AKA_mark("lis===-145-###sois===-4135-###eois===-41356-###lif===-10-###soif===-###eoif===-345-###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)");}

						AKA_mark("lis===147###sois===4204###eois===4231###lif===12###soif===408###eoif===435###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)");json_object_iter_next(&it);

		}

	}
	else {
				AKA_mark("lis===150###sois===4248###eois===4275###lif===15###soif===452###eoif===479###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)");l = decode_verbosity(spec);

				if (AKA_mark("lis===151###sois===4282###eois===4288###lif===16###soif===486###eoif===492###ifc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)") && (AKA_mark("lis===151###sois===4282###eois===4288###lif===16###soif===486###eoif===492###isc===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)")&&l >= 0)) {
						AKA_mark("lis===152###sois===4295###eois===4319###lif===17###soif===499###eoif===523###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)");set_verbosity_to("", l);

						AKA_mark("lis===153###sois===4323###eois===4348###lif===18###soif===527###eoif===552###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)");set_verbosity_to("*", l);

		}
		else {AKA_mark("lis===-151-###sois===-4282-###eois===-42826-###lif===-16-###soif===-###eoif===-492-###ins===true###function===./app-framework-binder/src/afb-monitor.c/set_verbosity(struct json_object*)");}

	}

}

/**
 * Translate verbosity level to a protocol indication.
 * @param level the verbosity
 * @return the encoded verbosity
 */
/** Instrumented function encode_verbosity(int) */
static struct json_object *encode_verbosity(int level)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/encode_verbosity(int)");AKA_fCall++;
		AKA_mark("lis===165###sois===4550###eois===4570###lif===2###soif===65###eoif===85###ins===true###function===./app-framework-binder/src/afb-monitor.c/encode_verbosity(int)");switch(_DEVERBOSITY_(level)){
			case Log_Level_Error: if(_DEVERBOSITY_(level) == Log_Level_Error)AKA_mark("lis===166###sois===4575###eois===4596###lif===3###soif===90###eoif===111###function===./app-framework-binder/src/afb-monitor.c/encode_verbosity(int)");
			AKA_mark("lis===166###sois===4597###eois===4636###lif===3###soif===112###eoif===151###ins===true###function===./app-framework-binder/src/afb-monitor.c/encode_verbosity(int)");return json_object_new_string(_error_);

			case Log_Level_Warning: if(_DEVERBOSITY_(level) == Log_Level_Warning)AKA_mark("lis===167###sois===4638###eois===4661###lif===4###soif===153###eoif===176###function===./app-framework-binder/src/afb-monitor.c/encode_verbosity(int)");
			AKA_mark("lis===167###sois===4662###eois===4703###lif===4###soif===177###eoif===218###ins===true###function===./app-framework-binder/src/afb-monitor.c/encode_verbosity(int)");return json_object_new_string(_warning_);

			case Log_Level_Notice: if(_DEVERBOSITY_(level) == Log_Level_Notice)AKA_mark("lis===168###sois===4705###eois===4727###lif===5###soif===220###eoif===242###function===./app-framework-binder/src/afb-monitor.c/encode_verbosity(int)");
			AKA_mark("lis===168###sois===4728###eois===4768###lif===5###soif===243###eoif===283###ins===true###function===./app-framework-binder/src/afb-monitor.c/encode_verbosity(int)");return json_object_new_string(_notice_);

			case Log_Level_Info: if(_DEVERBOSITY_(level) == Log_Level_Info)AKA_mark("lis===169###sois===4770###eois===4790###lif===6###soif===285###eoif===305###function===./app-framework-binder/src/afb-monitor.c/encode_verbosity(int)");
			AKA_mark("lis===169###sois===4791###eois===4829###lif===6###soif===306###eoif===344###ins===true###function===./app-framework-binder/src/afb-monitor.c/encode_verbosity(int)");return json_object_new_string(_info_);

			case Log_Level_Debug: if(_DEVERBOSITY_(level) == Log_Level_Debug)AKA_mark("lis===170###sois===4831###eois===4852###lif===7###soif===346###eoif===367###function===./app-framework-binder/src/afb-monitor.c/encode_verbosity(int)");
			AKA_mark("lis===170###sois===4853###eois===4892###lif===7###soif===368###eoif===407###ins===true###function===./app-framework-binder/src/afb-monitor.c/encode_verbosity(int)");return json_object_new_string(_debug_);

			default: if(_DEVERBOSITY_(level) != Log_Level_Error && _DEVERBOSITY_(level) != Log_Level_Warning && _DEVERBOSITY_(level) != Log_Level_Notice && _DEVERBOSITY_(level) != Log_Level_Info && _DEVERBOSITY_(level) != Log_Level_Debug)AKA_mark("lis===171###sois===4894###eois===4902###lif===8###soif===409###eoif===417###function===./app-framework-binder/src/afb-monitor.c/encode_verbosity(int)");
 		AKA_mark("lis===171###sois===4903###eois===4937###lif===8###soif===418###eoif===452###ins===true###function===./app-framework-binder/src/afb-monitor.c/encode_verbosity(int)");return json_object_new_int(level);

	}

}

/**
 * callback for getting verbosity of all apis
 * @param set the apiset
 * @param the name of the api to set
 * @param closure the json object to build
 */
/** Instrumented function get_verbosity_of_all_cb(void*,struct afb_apiset*,const char*,int) */
static void get_verbosity_of_all_cb(void *closure, struct afb_apiset *set, const char *name, int isalias)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/get_verbosity_of_all_cb(void*,struct afb_apiset*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===183###sois===5212###eois===5247###lif===2###soif===109###eoif===144###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of_all_cb(void*,struct afb_apiset*,const char*,int)");struct json_object *resu = closure;

		AKA_mark("lis===184###sois===5249###eois===5291###lif===3###soif===146###eoif===188###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of_all_cb(void*,struct afb_apiset*,const char*,int)");int m = afb_apiset_get_logmask(set, name);

		if (AKA_mark("lis===185###sois===5297###eois===5303###lif===4###soif===194###eoif===200###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of_all_cb(void*,struct afb_apiset*,const char*,int)") && (AKA_mark("lis===185###sois===5297###eois===5303###lif===4###soif===194###eoif===200###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of_all_cb(void*,struct afb_apiset*,const char*,int)")&&m >= 0)) {
		AKA_mark("lis===186###sois===5307###eois===5384###lif===5###soif===204###eoif===281###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of_all_cb(void*,struct afb_apiset*,const char*,int)");json_object_object_add(resu, name, encode_verbosity(verbosity_from_mask(m)));
	}
	else {AKA_mark("lis===-185-###sois===-5297-###eois===-52976-###lif===-4-###soif===-###eoif===-200-###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of_all_cb(void*,struct afb_apiset*,const char*,int)");}

}

/**
 * get in resu the verbosity of the api of 'name'
 * @param resu the json object to build
 * @param name the api name or "*" for any api or NULL or "" for global verbosity
 */
/** Instrumented function get_verbosity_of(struct json_object*,const char*) */
static void get_verbosity_of(struct json_object *resu, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)");AKA_fCall++;
		AKA_mark("lis===196###sois===5644###eois===5650###lif===2###soif===76###eoif===82###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)");int m;

		if (AKA_mark("lis===197###sois===5656###eois===5673###lif===3###soif===88###eoif===105###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)") && ((AKA_mark("lis===197###sois===5656###eois===5661###lif===3###soif===88###eoif===93###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)")&&!name)	||(AKA_mark("lis===197###sois===5665###eois===5673###lif===3###soif===97###eoif===105###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)")&&!name[0]))) {
		AKA_mark("lis===198###sois===5677###eois===5745###lif===4###soif===109###eoif===177###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)");json_object_object_add(resu, "", encode_verbosity(verbosity_get()));
	}
	else {
		if (AKA_mark("lis===199###sois===5756###eois===5782###lif===5###soif===188###eoif===214###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)") && ((AKA_mark("lis===199###sois===5756###eois===5770###lif===5###soif===188###eoif===202###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)")&&name[0] == '*')	&&(AKA_mark("lis===199###sois===5774###eois===5782###lif===5###soif===206###eoif===214###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)")&&!name[1]))) {
			AKA_mark("lis===200###sois===5786###eois===5848###lif===6###soif===218###eoif===280###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)");afb_apiset_enum(target_set, 1, get_verbosity_of_all_cb, resu);
		}
		else {
					AKA_mark("lis===202###sois===5859###eois===5904###lif===8###soif===291###eoif===336###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)");m = afb_apiset_get_logmask(target_set, name);

					if (AKA_mark("lis===203###sois===5911###eois===5917###lif===9###soif===343###eoif===349###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)") && (AKA_mark("lis===203###sois===5911###eois===5917###lif===9###soif===343###eoif===349###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)")&&m >= 0)) {
				AKA_mark("lis===204###sois===5922###eois===5999###lif===10###soif===354###eoif===431###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)");json_object_object_add(resu, name, encode_verbosity(verbosity_from_mask(m)));
			}
			else {AKA_mark("lis===-203-###sois===-5911-###eois===-59116-###lif===-9-###soif===-###eoif===-349-###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity_of(struct json_object*,const char*)");}

	}
	}

}

/**
 * get verbosities accordling to specification in 'spec'
 * @param resu the json object to build
 * @param spec specification of the verbosity to set
 */
/** Instrumented function get_verbosity(struct json_object*) */
static struct json_object *get_verbosity(struct json_object *spec)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");AKA_fCall++;
		AKA_mark("lis===215###sois===6234###eois===6243###lif===2###soif===70###eoif===79###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");int i, n;

		AKA_mark("lis===216###sois===6245###eois===6270###lif===3###soif===81###eoif===106###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");struct json_object *resu;

		AKA_mark("lis===217###sois===6272###eois===6308###lif===4###soif===108###eoif===144###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");struct json_object_iterator it, end;


		AKA_mark("lis===219###sois===6311###eois===6343###lif===6###soif===147###eoif===179###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");resu = json_object_new_object();

		if (AKA_mark("lis===220###sois===6349###eois===6392###lif===7###soif===185###eoif===228###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)") && (AKA_mark("lis===220###sois===6349###eois===6392###lif===7###soif===185###eoif===228###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)")&&json_object_is_type(spec, json_type_object))) {
				AKA_mark("lis===221###sois===6398###eois===6432###lif===8###soif===234###eoif===268###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");it = json_object_iter_begin(spec);

				AKA_mark("lis===222###sois===6435###eois===6468###lif===9###soif===271###eoif===304###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");end = json_object_iter_end(spec);

				while (AKA_mark("lis===223###sois===6478###eois===6512###lif===10###soif===314###eoif===348###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)") && (AKA_mark("lis===223###sois===6478###eois===6512###lif===10###soif===314###eoif===348###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)")&&!json_object_iter_equal(&it, &end))) {
						AKA_mark("lis===224###sois===6519###eois===6575###lif===11###soif===355###eoif===411###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");get_verbosity_of(resu, json_object_iter_peek_name(&it));

						AKA_mark("lis===225###sois===6579###eois===6606###lif===12###soif===415###eoif===442###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");json_object_iter_next(&it);

		}

	}
	else {
		if (AKA_mark("lis===227###sois===6623###eois===6665###lif===14###soif===459###eoif===501###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)") && (AKA_mark("lis===227###sois===6623###eois===6665###lif===14###soif===459###eoif===501###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)")&&json_object_is_type(spec, json_type_array))) {
					AKA_mark("lis===228###sois===6671###eois===6711###lif===15###soif===507###eoif===547###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");n = (int)json_object_array_length(spec);

					AKA_mark("lis===229###sois===6719###eois===6726###lif===16###soif===555###eoif===562###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");for (i = 0 ;AKA_mark("lis===229###sois===6727###eois===6732###lif===16###soif===563###eoif===568###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)") && AKA_mark("lis===229###sois===6727###eois===6732###lif===16###soif===563###eoif===568###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)")&&i < n;({AKA_mark("lis===229###sois===6735###eois===6738###lif===16###soif===571###eoif===574###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");i++;})) {
				AKA_mark("lis===230###sois===6743###eois===6826###lif===17###soif===579###eoif===662###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");get_verbosity_of(resu, json_object_get_string(json_object_array_get_idx(spec, i)));
			}

	}
		else {
			if (AKA_mark("lis===231###sois===6839###eois===6882###lif===18###soif===675###eoif===718###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)") && (AKA_mark("lis===231###sois===6839###eois===6882###lif===18###soif===675###eoif===718###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)")&&json_object_is_type(spec, json_type_string))) {
						AKA_mark("lis===232###sois===6888###eois===6941###lif===19###soif===724###eoif===777###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");get_verbosity_of(resu, json_object_get_string(spec));

	}
			else {
				if (AKA_mark("lis===233###sois===6954###eois===6983###lif===20###soif===790###eoif===819###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)") && (AKA_mark("lis===233###sois===6954###eois===6983###lif===20###soif===790###eoif===819###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)")&&json_object_get_boolean(spec))) {
							AKA_mark("lis===234###sois===6989###eois===7016###lif===21###soif===825###eoif===852###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");get_verbosity_of(resu, "");

							AKA_mark("lis===235###sois===7019###eois===7047###lif===22###soif===855###eoif===883###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");get_verbosity_of(resu, "*");

	}
				else {AKA_mark("lis===-233-###sois===-6954-###eois===-695429-###lif===-20-###soif===-###eoif===-819-###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");}
			}
		}
	}

		AKA_mark("lis===237###sois===7052###eois===7064###lif===24###soif===888###eoif===900###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_verbosity(struct json_object*)");return resu;

}

/******************************************************************************
**** Manage namelist of api names
******************************************************************************/

struct namelist {
	struct namelist *next;
	json_object *data;
	char name[];
};

/** Instrumented function reverse_namelist(struct namelist*) */
static struct namelist *reverse_namelist(struct namelist *head)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/reverse_namelist(struct namelist*)");AKA_fCall++;
		AKA_mark("lis===252###sois===7410###eois===7443###lif===2###soif===67###eoif===100###ins===true###function===./app-framework-binder/src/afb-monitor.c/reverse_namelist(struct namelist*)");struct namelist *previous, *next;


		AKA_mark("lis===254###sois===7446###eois===7462###lif===4###soif===103###eoif===119###ins===true###function===./app-framework-binder/src/afb-monitor.c/reverse_namelist(struct namelist*)");previous = NULL;

		while (AKA_mark("lis===255###sois===7470###eois===7474###lif===5###soif===127###eoif===131###ifc===true###function===./app-framework-binder/src/afb-monitor.c/reverse_namelist(struct namelist*)") && (AKA_mark("lis===255###sois===7470###eois===7474###lif===5###soif===127###eoif===131###isc===true###function===./app-framework-binder/src/afb-monitor.c/reverse_namelist(struct namelist*)")&&head)) {
				AKA_mark("lis===256###sois===7480###eois===7498###lif===6###soif===137###eoif===155###ins===true###function===./app-framework-binder/src/afb-monitor.c/reverse_namelist(struct namelist*)");next = head->next;

				AKA_mark("lis===257###sois===7501###eois===7523###lif===7###soif===158###eoif===180###ins===true###function===./app-framework-binder/src/afb-monitor.c/reverse_namelist(struct namelist*)");head->next = previous;

				AKA_mark("lis===258###sois===7526###eois===7542###lif===8###soif===183###eoif===199###ins===true###function===./app-framework-binder/src/afb-monitor.c/reverse_namelist(struct namelist*)");previous = head;

				AKA_mark("lis===259###sois===7545###eois===7557###lif===9###soif===202###eoif===214###ins===true###function===./app-framework-binder/src/afb-monitor.c/reverse_namelist(struct namelist*)");head = next;

	}

		AKA_mark("lis===261###sois===7562###eois===7578###lif===11###soif===219###eoif===235###ins===true###function===./app-framework-binder/src/afb-monitor.c/reverse_namelist(struct namelist*)");return previous;

}

/** Instrumented function add_one_name_to_namelist(struct namelist**,const char*,struct json_object*) */
static void add_one_name_to_namelist(struct namelist **head, const char *name, struct json_object *data)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/add_one_name_to_namelist(struct namelist**,const char*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===266###sois===7690###eois===7723###lif===2###soif===108###eoif===141###ins===true###function===./app-framework-binder/src/afb-monitor.c/add_one_name_to_namelist(struct namelist**,const char*,struct json_object*)");size_t length = strlen(name) + 1;

		AKA_mark("lis===267###sois===7725###eois===7779###lif===3###soif===143###eoif===197###ins===true###function===./app-framework-binder/src/afb-monitor.c/add_one_name_to_namelist(struct namelist**,const char*,struct json_object*)");struct namelist *item = malloc(length + sizeof *item);

		if (AKA_mark("lis===268###sois===7785###eois===7790###lif===4###soif===203###eoif===208###ifc===true###function===./app-framework-binder/src/afb-monitor.c/add_one_name_to_namelist(struct namelist**,const char*,struct json_object*)") && (AKA_mark("lis===268###sois===7785###eois===7790###lif===4###soif===203###eoif===208###isc===true###function===./app-framework-binder/src/afb-monitor.c/add_one_name_to_namelist(struct namelist**,const char*,struct json_object*)")&&!item)) {
		AKA_mark("lis===269###sois===7794###eois===7817###lif===5###soif===212###eoif===235###ins===true###function===./app-framework-binder/src/afb-monitor.c/add_one_name_to_namelist(struct namelist**,const char*,struct json_object*)");ERROR("out of memory");
	}
	else {
				AKA_mark("lis===271###sois===7828###eois===7847###lif===7###soif===246###eoif===265###ins===true###function===./app-framework-binder/src/afb-monitor.c/add_one_name_to_namelist(struct namelist**,const char*,struct json_object*)");item->next = *head;

				AKA_mark("lis===272###sois===7850###eois===7868###lif===8###soif===268###eoif===286###ins===true###function===./app-framework-binder/src/afb-monitor.c/add_one_name_to_namelist(struct namelist**,const char*,struct json_object*)");item->data = data;

				AKA_mark("lis===273###sois===7871###eois===7904###lif===9###soif===289###eoif===322###ins===true###function===./app-framework-binder/src/afb-monitor.c/add_one_name_to_namelist(struct namelist**,const char*,struct json_object*)");memcpy(item->name, name, length);

				AKA_mark("lis===274###sois===7907###eois===7920###lif===10###soif===325###eoif===338###ins===true###function===./app-framework-binder/src/afb-monitor.c/add_one_name_to_namelist(struct namelist**,const char*,struct json_object*)");*head = item;

	}

}

/** Instrumented function get_apis_namelist_of_all_cb(void*,struct afb_apiset*,const char*,int) */
static void get_apis_namelist_of_all_cb(void *closure, struct afb_apiset *set, const char *name, int isalias)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/get_apis_namelist_of_all_cb(void*,struct afb_apiset*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===280###sois===8040###eois===8073###lif===2###soif===113###eoif===146###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist_of_all_cb(void*,struct afb_apiset*,const char*,int)");struct namelist **head = closure;

		AKA_mark("lis===281###sois===8075###eois===8118###lif===3###soif===148###eoif===191###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist_of_all_cb(void*,struct afb_apiset*,const char*,int)");add_one_name_to_namelist(head, name, NULL);

}

/**
 * get apis names as a list accordling to specification in 'spec'
 * @param spec specification of the apis to get
 */
/** Instrumented function get_apis_namelist(struct json_object*) */
static struct namelist *get_apis_namelist(struct json_object *spec)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");AKA_fCall++;
		AKA_mark("lis===290###sois===8315###eois===8324###lif===2###soif===71###eoif===80###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");int i, n;

		AKA_mark("lis===291###sois===8326###eois===8362###lif===3###soif===82###eoif===118###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");struct json_object_iterator it, end;

		AKA_mark("lis===292###sois===8364###eois===8386###lif===4###soif===120###eoif===142###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");struct namelist *head;


		AKA_mark("lis===294###sois===8389###eois===8401###lif===6###soif===145###eoif===157###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");head = NULL;

		if (AKA_mark("lis===295###sois===8407###eois===8450###lif===7###soif===163###eoif===206###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)") && (AKA_mark("lis===295###sois===8407###eois===8450###lif===7###soif===163###eoif===206###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)")&&json_object_is_type(spec, json_type_object))) {
				AKA_mark("lis===296###sois===8456###eois===8490###lif===8###soif===212###eoif===246###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");it = json_object_iter_begin(spec);

				AKA_mark("lis===297###sois===8493###eois===8526###lif===9###soif===249###eoif===282###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");end = json_object_iter_end(spec);

				while (AKA_mark("lis===298###sois===8536###eois===8570###lif===10###soif===292###eoif===326###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)") && (AKA_mark("lis===298###sois===8536###eois===8570###lif===10###soif===292###eoif===326###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)")&&!json_object_iter_equal(&it, &end))) {
						AKA_mark("lis===299###sois===8577###eois===8690###lif===11###soif===333###eoif===446###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");add_one_name_to_namelist(&head,
						 json_object_iter_peek_name(&it),
						 json_object_iter_peek_value(&it));

						AKA_mark("lis===302###sois===8694###eois===8721###lif===14###soif===450###eoif===477###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");json_object_iter_next(&it);

		}

	}
	else {
		if (AKA_mark("lis===304###sois===8738###eois===8780###lif===16###soif===494###eoif===536###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)") && (AKA_mark("lis===304###sois===8738###eois===8780###lif===16###soif===494###eoif===536###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)")&&json_object_is_type(spec, json_type_array))) {
					AKA_mark("lis===305###sois===8786###eois===8826###lif===17###soif===542###eoif===582###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");n = (int)json_object_array_length(spec);

					AKA_mark("lis===306###sois===8834###eois===8841###lif===18###soif===590###eoif===597###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");for (i = 0 ;AKA_mark("lis===306###sois===8842###eois===8847###lif===18###soif===598###eoif===603###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)") && AKA_mark("lis===306###sois===8842###eois===8847###lif===18###soif===598###eoif===603###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)")&&i < n;({AKA_mark("lis===306###sois===8850###eois===8853###lif===18###soif===606###eoif===609###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");i++;})) {
				AKA_mark("lis===307###sois===8858###eois===8979###lif===19###soif===614###eoif===735###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");add_one_name_to_namelist(&head,
						 json_object_get_string(
							 json_object_array_get_idx(spec, i)),
						 NULL);
			}

	}
		else {
			if (AKA_mark("lis===311###sois===8992###eois===9035###lif===23###soif===748###eoif===791###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)") && (AKA_mark("lis===311###sois===8992###eois===9035###lif===23###soif===748###eoif===791###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)")&&json_object_is_type(spec, json_type_string))) {
						AKA_mark("lis===312###sois===9041###eois===9109###lif===24###soif===797###eoif===865###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");add_one_name_to_namelist(&head, json_object_get_string(spec), NULL);

	}
			else {
				if (AKA_mark("lis===313###sois===9122###eois===9151###lif===25###soif===878###eoif===907###ifc===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)") && (AKA_mark("lis===313###sois===9122###eois===9151###lif===25###soif===878###eoif===907###isc===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)")&&json_object_get_boolean(spec))) {
							AKA_mark("lis===314###sois===9157###eois===9224###lif===26###soif===913###eoif===980###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");afb_apiset_enum(target_set, 1, get_apis_namelist_of_all_cb, &head);

	}
				else {AKA_mark("lis===-313-###sois===-9122-###eois===-912229-###lif===-25-###soif===-###eoif===-907-###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");}
			}
		}
	}

		AKA_mark("lis===316###sois===9229###eois===9259###lif===28###soif===985###eoif===1015###ins===true###function===./app-framework-binder/src/afb-monitor.c/get_apis_namelist(struct json_object*)");return reverse_namelist(head);

}

/******************************************************************************
**** Monitoring apis
******************************************************************************/

struct desc_apis {
	struct namelist *names;
	struct json_object *resu;
	struct json_object *apis;
	afb_req_t req;
};

static void describe_first_api(struct desc_apis *desc);

/** Instrumented function on_api_description(void*,struct json_object*) */
static void on_api_description(void *closure, struct json_object *apidesc)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/on_api_description(void*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===334###sois===9698###eois===9731###lif===2###soif===78###eoif===111###ins===true###function===./app-framework-binder/src/afb-monitor.c/on_api_description(void*,struct json_object*)");struct desc_apis *desc = closure;

		AKA_mark("lis===335###sois===9733###eois===9769###lif===3###soif===113###eoif===149###ins===true###function===./app-framework-binder/src/afb-monitor.c/on_api_description(void*,struct json_object*)");struct namelist *head = desc->names;


		if (AKA_mark("lis===337###sois===9776###eois===9831###lif===5###soif===156###eoif===211###ifc===true###function===./app-framework-binder/src/afb-monitor.c/on_api_description(void*,struct json_object*)") && ((AKA_mark("lis===337###sois===9776###eois===9783###lif===5###soif===156###eoif===163###isc===true###function===./app-framework-binder/src/afb-monitor.c/on_api_description(void*,struct json_object*)")&&apidesc)	||(AKA_mark("lis===337###sois===9787###eois===9831###lif===5###soif===167###eoif===211###isc===true###function===./app-framework-binder/src/afb-monitor.c/on_api_description(void*,struct json_object*)")&&afb_apiset_lookup(target_set, head->name, 1)))) {
		AKA_mark("lis===338###sois===9835###eois===9891###lif===6###soif===215###eoif===271###ins===true###function===./app-framework-binder/src/afb-monitor.c/on_api_description(void*,struct json_object*)");json_object_object_add(desc->apis, head->name, apidesc);
	}
	else {AKA_mark("lis===-337-###sois===-9776-###eois===-977655-###lif===-5-###soif===-###eoif===-211-###ins===true###function===./app-framework-binder/src/afb-monitor.c/on_api_description(void*,struct json_object*)");}

		AKA_mark("lis===339###sois===9893###eois===9918###lif===7###soif===273###eoif===298###ins===true###function===./app-framework-binder/src/afb-monitor.c/on_api_description(void*,struct json_object*)");desc->names = head->next;

		AKA_mark("lis===340###sois===9920###eois===9931###lif===8###soif===300###eoif===311###ins===true###function===./app-framework-binder/src/afb-monitor.c/on_api_description(void*,struct json_object*)");free(head);

		AKA_mark("lis===341###sois===9933###eois===9958###lif===9###soif===313###eoif===338###ins===true###function===./app-framework-binder/src/afb-monitor.c/on_api_description(void*,struct json_object*)");describe_first_api(desc);

}

/** Instrumented function describe_first_api(struct desc_apis*) */
static void describe_first_api(struct desc_apis *desc)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/describe_first_api(struct desc_apis*)");AKA_fCall++;
		AKA_mark("lis===346###sois===10020###eois===10056###lif===2###soif===58###eoif===94###ins===true###function===./app-framework-binder/src/afb-monitor.c/describe_first_api(struct desc_apis*)");struct namelist *head = desc->names;

		if (AKA_mark("lis===347###sois===10062###eois===10066###lif===3###soif===100###eoif===104###ifc===true###function===./app-framework-binder/src/afb-monitor.c/describe_first_api(struct desc_apis*)") && (AKA_mark("lis===347###sois===10062###eois===10066###lif===3###soif===100###eoif===104###isc===true###function===./app-framework-binder/src/afb-monitor.c/describe_first_api(struct desc_apis*)")&&head)) {
		AKA_mark("lis===348###sois===10070###eois===10140###lif===4###soif===108###eoif===178###ins===true###function===./app-framework-binder/src/afb-monitor.c/describe_first_api(struct desc_apis*)");afb_apiset_describe(target_set, head->name, on_api_description, desc);
	}
	else {
				AKA_mark("lis===350###sois===10151###eois===10196###lif===6###soif===189###eoif===234###ins===true###function===./app-framework-binder/src/afb-monitor.c/describe_first_api(struct desc_apis*)");afb_req_success(desc->req, desc->resu, NULL);

				AKA_mark("lis===351###sois===10199###eois===10224###lif===7###soif===237###eoif===262###ins===true###function===./app-framework-binder/src/afb-monitor.c/describe_first_api(struct desc_apis*)");afb_req_unref(desc->req);

				AKA_mark("lis===352###sois===10227###eois===10238###lif===8###soif===265###eoif===276###ins===true###function===./app-framework-binder/src/afb-monitor.c/describe_first_api(struct desc_apis*)");free(desc);

	}

}

/** Instrumented function describe_apis(afb_req_t,struct json_object*,struct json_object*) */
static void describe_apis(afb_req_t req, struct json_object *resu, struct json_object *spec)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/describe_apis(afb_req_t,struct json_object*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===358###sois===10341###eois===10364###lif===2###soif===96###eoif===119###ins===true###function===./app-framework-binder/src/afb-monitor.c/describe_apis(afb_req_t,struct json_object*,struct json_object*)");struct desc_apis *desc;


		AKA_mark("lis===360###sois===10367###eois===10395###lif===4###soif===122###eoif===150###ins===true###function===./app-framework-binder/src/afb-monitor.c/describe_apis(afb_req_t,struct json_object*,struct json_object*)");desc = malloc(sizeof *desc);

		if (AKA_mark("lis===361###sois===10401###eois===10406###lif===5###soif===156###eoif===161###ifc===true###function===./app-framework-binder/src/afb-monitor.c/describe_apis(afb_req_t,struct json_object*,struct json_object*)") && (AKA_mark("lis===361###sois===10401###eois===10406###lif===5###soif===156###eoif===161###isc===true###function===./app-framework-binder/src/afb-monitor.c/describe_apis(afb_req_t,struct json_object*,struct json_object*)")&&!desc)) {
		AKA_mark("lis===362###sois===10410###eois===10451###lif===6###soif===165###eoif===206###ins===true###function===./app-framework-binder/src/afb-monitor.c/describe_apis(afb_req_t,struct json_object*,struct json_object*)");afb_req_fail(req, "out-of-memory", NULL);
	}
	else {
				AKA_mark("lis===364###sois===10462###eois===10494###lif===8###soif===217###eoif===249###ins===true###function===./app-framework-binder/src/afb-monitor.c/describe_apis(afb_req_t,struct json_object*,struct json_object*)");desc->req = afb_req_addref(req);

				AKA_mark("lis===365###sois===10497###eois===10515###lif===9###soif===252###eoif===270###ins===true###function===./app-framework-binder/src/afb-monitor.c/describe_apis(afb_req_t,struct json_object*,struct json_object*)");desc->resu = resu;

				AKA_mark("lis===366###sois===10518###eois===10556###lif===10###soif===273###eoif===311###ins===true###function===./app-framework-binder/src/afb-monitor.c/describe_apis(afb_req_t,struct json_object*,struct json_object*)");desc->apis = json_object_new_object();

				AKA_mark("lis===367###sois===10559###eois===10614###lif===11###soif===314###eoif===369###ins===true###function===./app-framework-binder/src/afb-monitor.c/describe_apis(afb_req_t,struct json_object*,struct json_object*)");json_object_object_add(desc->resu, _apis_, desc->apis);

				AKA_mark("lis===368###sois===10617###eois===10655###lif===12###soif===372###eoif===410###ins===true###function===./app-framework-binder/src/afb-monitor.c/describe_apis(afb_req_t,struct json_object*,struct json_object*)");desc->names = get_apis_namelist(spec);

				AKA_mark("lis===369###sois===10658###eois===10683###lif===13###soif===413###eoif===438###ins===true###function===./app-framework-binder/src/afb-monitor.c/describe_apis(afb_req_t,struct json_object*,struct json_object*)");describe_first_api(desc);

	}

}

/******************************************************************************
**** Implementation monitoring verbs
******************************************************************************/

/** Instrumented function f_get(afb_req_t) */
static void f_get(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===379###sois===10924###eois===10946###lif===2###soif===36###eoif===58###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)");struct json_object *r;

		AKA_mark("lis===380###sois===10948###eois===10980###lif===3###soif===60###eoif===92###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)");struct json_object *apis = NULL;

		AKA_mark("lis===381###sois===10982###eois===11019###lif===4###soif===94###eoif===131###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)");struct json_object *verbosity = NULL;


		AKA_mark("lis===383###sois===11022###eois===11113###lif===6###soif===134###eoif===225###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)");wrap_json_unpack(afb_req_json(req), "{s?:o,s?:o}", _verbosity_, &verbosity, _apis_, &apis);

		if (AKA_mark("lis===384###sois===11119###eois===11138###lif===7###soif===231###eoif===250###ifc===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)") && ((AKA_mark("lis===384###sois===11119###eois===11129###lif===7###soif===231###eoif===241###isc===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)")&&!verbosity)	&&(AKA_mark("lis===384###sois===11133###eois===11138###lif===7###soif===245###eoif===250###isc===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)")&&!apis))) {
		AKA_mark("lis===385###sois===11142###eois===11175###lif===8###soif===254###eoif===287###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)");afb_req_success(req, NULL, NULL);
	}
	else {
				AKA_mark("lis===387###sois===11186###eois===11215###lif===10###soif===298###eoif===327###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)");r = json_object_new_object();

				if (AKA_mark("lis===388###sois===11222###eois===11224###lif===11###soif===334###eoif===336###ifc===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)") && (AKA_mark("lis===388###sois===11222###eois===11224###lif===11###soif===334###eoif===336###isc===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)")&&!r)) {
			AKA_mark("lis===389###sois===11229###eois===11270###lif===12###soif===341###eoif===382###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)");afb_req_fail(req, "out-of-memory", NULL);
		}
		else {
						if (AKA_mark("lis===391###sois===11287###eois===11296###lif===14###soif===399###eoif===408###ifc===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)") && (AKA_mark("lis===391###sois===11287###eois===11296###lif===14###soif===399###eoif===408###isc===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)")&&verbosity)) {
								AKA_mark("lis===392###sois===11304###eois===11341###lif===15###soif===416###eoif===453###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)");verbosity = get_verbosity(verbosity);

								AKA_mark("lis===393###sois===11346###eois===11396###lif===16###soif===458###eoif===508###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)");json_object_object_add(r, _verbosity_, verbosity);

			}
			else {AKA_mark("lis===-391-###sois===-11287-###eois===-112879-###lif===-14-###soif===-###eoif===-408-###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)");}

						if (AKA_mark("lis===395###sois===11409###eois===11414###lif===18###soif===521###eoif===526###ifc===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)") && (AKA_mark("lis===395###sois===11409###eois===11414###lif===18###soif===521###eoif===526###isc===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)")&&!apis)) {
				AKA_mark("lis===396###sois===11420###eois===11450###lif===19###soif===532###eoif===562###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)");afb_req_success(req, r, NULL);
			}
			else {
				AKA_mark("lis===398###sois===11463###eois===11491###lif===21###soif===575###eoif===603###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_get(afb_req_t)");describe_apis(req, r, apis);
			}

		}

	}

}

/** Instrumented function f_set(afb_req_t) */
static void f_set(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/f_set(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===405###sois===11538###eois===11575###lif===2###soif===36###eoif===73###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_set(afb_req_t)");struct json_object *verbosity = NULL;


		AKA_mark("lis===407###sois===11578###eois===11649###lif===4###soif===76###eoif===147###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_set(afb_req_t)");wrap_json_unpack(afb_req_json(req), "{s?:o}", _verbosity_, &verbosity);

		if (AKA_mark("lis===408###sois===11655###eois===11664###lif===5###soif===153###eoif===162###ifc===true###function===./app-framework-binder/src/afb-monitor.c/f_set(afb_req_t)") && (AKA_mark("lis===408###sois===11655###eois===11664###lif===5###soif===153###eoif===162###isc===true###function===./app-framework-binder/src/afb-monitor.c/f_set(afb_req_t)")&&verbosity)) {
		AKA_mark("lis===409###sois===11668###eois===11693###lif===6###soif===166###eoif===191###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_set(afb_req_t)");set_verbosity(verbosity);
	}
	else {AKA_mark("lis===-408-###sois===-11655-###eois===-116559-###lif===-5-###soif===-###eoif===-162-###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_set(afb_req_t)");}


		AKA_mark("lis===411###sois===11696###eois===11729###lif===8###soif===194###eoif===227###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_set(afb_req_t)");afb_req_success(req, NULL, NULL);

}

#if WITH_AFB_TRACE
static void *context_create(void *closure)
{
	return afb_trace_create(_afb_binding_monitor.api, NULL);
}

static void context_destroy(void *pointer)
{
	struct afb_trace *trace = pointer;
	afb_trace_unref(trace);
}

static void f_trace(afb_req_t req)
{
	int rc;
	struct json_object *add = NULL;
	struct json_object *drop = NULL;
	struct afb_trace *trace;

	trace = afb_req_context(req, 0, context_create, context_destroy, NULL);
	wrap_json_unpack(afb_req_json(req), "{s?o s?o}", "add", &add, "drop", &drop);
	if (add) {
		rc = afb_trace_add(req, add, trace);
		if (rc)
			goto end;
	}
	if (drop) {
		rc = afb_trace_drop(req, drop, trace);
		if (rc)
			goto end;
	}
	afb_req_success(req, NULL, NULL);
end:
	afb_apiset_update_hooks(target_set, NULL);
	afb_evt_update_hooks();
	return;
}
#else
/** Instrumented function f_trace(afb_req_t) */
static void f_trace(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/f_trace(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===454###sois===12580###eois===12641###lif===2###soif===38###eoif===99###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_trace(afb_req_t)");afb_req_reply(req, NULL, afb_error_text_not_available, NULL);

}
#endif

/** Instrumented function f_session(afb_req_t) */
static void f_session(afb_req_t req)
{AKA_mark("Calling: ./app-framework-binder/src/afb-monitor.c/f_session(afb_req_t)");AKA_fCall++;
		AKA_mark("lis===460###sois===12692###eois===12721###lif===2###soif===40###eoif===69###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_session(afb_req_t)");struct json_object *r = NULL;

		AKA_mark("lis===461###sois===12723###eois===12769###lif===3###soif===71###eoif===117###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_session(afb_req_t)");struct afb_xreq *xreq = xreq_from_req_x2(req);


	/* check right to call it */
		if (AKA_mark("lis===464###sois===12806###eois===12825###lif===6###soif===154###eoif===173###ifc===true###function===./app-framework-binder/src/afb-monitor.c/f_session(afb_req_t)") && (AKA_mark("lis===464###sois===12806###eois===12825###lif===6###soif===154###eoif===173###isc===true###function===./app-framework-binder/src/afb-monitor.c/f_session(afb_req_t)")&&xreq->context.super)) {
				AKA_mark("lis===465###sois===12831###eois===12890###lif===7###soif===179###eoif===238###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_session(afb_req_t)");afb_req_fail(req, "invalid", "reserved to direct clients");

				AKA_mark("lis===466###sois===12893###eois===12900###lif===8###soif===241###eoif===248###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_session(afb_req_t)");return;

	}
	else {AKA_mark("lis===-464-###sois===-12806-###eois===-1280619-###lif===-6-###soif===-###eoif===-173-###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_session(afb_req_t)");}


	/* make the result */
		AKA_mark("lis===470###sois===12929###eois===13137###lif===12###soif===277###eoif===485###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_session(afb_req_t)");wrap_json_pack(&r, "{s:s,s:i,s:i}",
			"uuid", afb_session_uuid(xreq->context.session),
			"timeout", afb_session_timeout(xreq->context.session),
			"remain", afb_session_what_remains(xreq->context.session));

		AKA_mark("lis===474###sois===13139###eois===13169###lif===16###soif===487###eoif===517###ins===true###function===./app-framework-binder/src/afb-monitor.c/f_session(afb_req_t)");afb_req_success(req, r, NULL);

}


#endif

