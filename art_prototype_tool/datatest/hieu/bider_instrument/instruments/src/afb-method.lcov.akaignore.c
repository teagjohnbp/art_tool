/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_METHOD_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_METHOD_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <microhttpd.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_METHOD_H_
#define AKA_INCLUDE__AFB_METHOD_H_
#include "afb-method.lcov.akaignore.h"
#endif


enum afb_method get_method(const char *method)
{AKA_fCall++;
	switch (method[0] & ~' ') {
	case 'C':
		return afb_method_connect;
	case 'D':
		return afb_method_delete;
	case 'G':
		return afb_method_get;
	case 'H':
		return afb_method_head;
	case 'O':
		return afb_method_options;
	case 'P':
		switch (method[1] & ~' ') {
		case 'A':
			return afb_method_patch;
		case 'O':
			return afb_method_post;
		case 'U':
			return afb_method_put;
		}
		break;
	case 'T':
		return afb_method_trace;
	}
	return afb_method_none;
}

#if !defined(MHD_HTTP_METHOD_PATCH)
#define MHD_HTTP_METHOD_PATCH "PATCH"
#endif
const char *get_method_name(enum afb_method method)
{AKA_fCall++;
	switch (method) {
	case afb_method_get:
		return MHD_HTTP_METHOD_GET;
	case afb_method_post:
		return MHD_HTTP_METHOD_POST;
	case afb_method_head:
		return MHD_HTTP_METHOD_HEAD;
	case afb_method_connect:
		return MHD_HTTP_METHOD_CONNECT;
	case afb_method_delete:
		return MHD_HTTP_METHOD_DELETE;
	case afb_method_options:
		return MHD_HTTP_METHOD_OPTIONS;
	case afb_method_patch:
		return MHD_HTTP_METHOD_PATCH;
	case afb_method_put:
		return MHD_HTTP_METHOD_PUT;
	case afb_method_trace:
		return MHD_HTTP_METHOD_TRACE;
	default:
		return NULL;
	}
}



#endif

