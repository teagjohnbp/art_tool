/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_SOCKET_H
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_SOCKET_H
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

struct fdev;

extern int afb_socket_open_scheme(const char *uri, int server, const char *scheme);

extern struct fdev *afb_socket_open_fdev_scheme(const char *uri, int server, const char *scheme);

extern const char *afb_socket_api(const char *uri);

/** Instrumented function afb_socket_open(const char*,int) */
static inline int afb_socket_open(const char *uri, int server)
{AKA_mark("Calling: ./app-framework-binder/src/afb-socket.h/afb_socket_open(const char*,int)");AKA_fCall++;
		AKA_mark("lis===30###sois===974###eois===1020###lif===2###soif===66###eoif===112###ins===true###function===./app-framework-binder/src/afb-socket.h/afb_socket_open(const char*,int)");return afb_socket_open_scheme(uri, server, 0);

}

/** Instrumented function afb_socket_open_fdev(const char*,int) */
static inline struct fdev *afb_socket_open_fdev(const char *uri, int server)
{AKA_mark("Calling: ./app-framework-binder/src/afb-socket.h/afb_socket_open_fdev(const char*,int)");AKA_fCall++;
		AKA_mark("lis===35###sois===1104###eois===1155###lif===2###soif===80###eoif===131###ins===true###function===./app-framework-binder/src/afb-socket.h/afb_socket_open_fdev(const char*,int)");return afb_socket_open_fdev_scheme(uri, server, 0);

}

#endif

