/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_WEBSOCK_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_WEBSOCK_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * This work is a far adaptation of apache-websocket:
 *   origin:  https://github.com/disconnect/apache-websocket
 *   commit:  cfaef071223f11ba016bff7e1e4b7c9e5df45b50
 *   Copyright 2010-2012 self.disconnect (APACHE-2)
 */

#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <sys/uio.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__WEBSOCK_H_
#define AKA_INCLUDE__WEBSOCK_H_
#include "websock.akaignore.h"
#endif


#if !defined(WEBSOCKET_DEFAULT_MAXLENGTH)
#  define WEBSOCKET_DEFAULT_MAXLENGTH 1048500  /* 76 less than 1M, probably enougth for headers */
#endif

/** Instrumented function FRAME_GET_FIN(BYTE) */
#define FRAME_GET_FIN(BYTE)         (((BYTE) >> 7) & 0x01)
/** Instrumented function FRAME_GET_RSV1(BYTE) */
#define FRAME_GET_RSV1(BYTE)        (((BYTE) >> 6) & 0x01)
/** Instrumented function FRAME_GET_RSV2(BYTE) */
#define FRAME_GET_RSV2(BYTE)        (((BYTE) >> 5) & 0x01)
/** Instrumented function FRAME_GET_RSV3(BYTE) */
#define FRAME_GET_RSV3(BYTE)        (((BYTE) >> 4) & 0x01)
/** Instrumented function FRAME_GET_OPCODE(BYTE) */
#define FRAME_GET_OPCODE(BYTE)      ( (BYTE)       & 0x0F)
/** Instrumented function FRAME_GET_MASK(BYTE) */
#define FRAME_GET_MASK(BYTE)        (((BYTE) >> 7) & 0x01)
/** Instrumented function FRAME_GET_PAYLOAD_LEN(BYTE) */
#define FRAME_GET_PAYLOAD_LEN(BYTE) ( (BYTE)       & 0x7F)

/** Instrumented function FRAME_SET_FIN(BYTE) */
#define FRAME_SET_FIN(BYTE)         (((BYTE) & 0x01) << 7)
/** Instrumented function FRAME_SET_RSV1(BYTE) */
#define FRAME_SET_RSV1(BYTE)        (((BYTE) & 0x01) << 6)
/** Instrumented function FRAME_SET_RSV2(BYTE) */
#define FRAME_SET_RSV2(BYTE)        (((BYTE) & 0x01) << 5)
/** Instrumented function FRAME_SET_RSV3(BYTE) */
#define FRAME_SET_RSV3(BYTE)        (((BYTE) & 0x01) << 4)
/** Instrumented function FRAME_SET_OPCODE(BYTE) */
#define FRAME_SET_OPCODE(BYTE)      ((BYTE) & 0x0F)
/** Instrumented function FRAME_SET_MASK(BYTE) */
#define FRAME_SET_MASK(BYTE)        (((BYTE) & 0x01) << 7)
/** Instrumented function FRAME_SET_LENGTH(X64,IDX) */
#define FRAME_SET_LENGTH(X64, IDX)  (unsigned char)((sizeof(X64)) <= (IDX) ? 0 : (((X64) >> ((IDX)*8)) & 0xFF))

#define OPCODE_CONTINUATION 0x0
#define OPCODE_TEXT         0x1
#define OPCODE_BINARY       0x2
#define OPCODE_CLOSE        0x8
#define OPCODE_PING         0x9
#define OPCODE_PONG         0xA

#define STATE_INIT    0
#define STATE_START   1
#define STATE_LENGTH  2
#define STATE_DATA    3

static size_t default_maxlength = WEBSOCKET_DEFAULT_MAXLENGTH;

#ifndef WEBSOCKET_STRUCT
#define WEBSOCKET_STRUCT

struct websock {
	int state;
	uint64_t maxlength;
	int lenhead, szhead;
	uint64_t length;
	uint32_t mask;
	unsigned char header[14];	/* 2 + 8 + 4 */
	const struct websock_itf *itf;
	void *closure;
};

#endif

/** Instrumented function ws_writev(struct websock*,const struct iovec*,int) */
static ssize_t ws_writev(struct websock *ws, const struct iovec *iov, int iovcnt)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/ws_writev(struct websock*,const struct iovec*,int)");AKA_fCall++;
		AKA_mark("lis===79###sois===2619###eois===2668###lif===2###soif===85###eoif===134###ins===true###function===./app-framework-binder/src/websock.c/ws_writev(struct websock*,const struct iovec*,int)");return ws->itf->writev(ws->closure, iov, iovcnt);

}

/** Instrumented function ws_readv(struct websock*,const struct iovec*,int) */
static ssize_t ws_readv(struct websock *ws, const struct iovec *iov, int iovcnt)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/ws_readv(struct websock*,const struct iovec*,int)");AKA_fCall++;
		AKA_mark("lis===84###sois===2756###eois===2804###lif===2###soif===84###eoif===132###ins===true###function===./app-framework-binder/src/websock.c/ws_readv(struct websock*,const struct iovec*,int)");return ws->itf->readv(ws->closure, iov, iovcnt);

}

/** Instrumented function ws_read(struct websock*,void*,size_t) */
static ssize_t ws_read(struct websock *ws, void *buffer, size_t buffer_size)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/ws_read(struct websock*,void*,size_t)");AKA_fCall++;
		AKA_mark("lis===89###sois===2888###eois===2905###lif===2###soif===80###eoif===97###ins===true###function===./app-framework-binder/src/websock.c/ws_read(struct websock*,void*,size_t)");struct iovec iov;

		AKA_mark("lis===90###sois===2907###eois===2929###lif===3###soif===99###eoif===121###ins===true###function===./app-framework-binder/src/websock.c/ws_read(struct websock*,void*,size_t)");iov.iov_base = buffer;

		AKA_mark("lis===91###sois===2931###eois===2957###lif===4###soif===123###eoif===149###ins===true###function===./app-framework-binder/src/websock.c/ws_read(struct websock*,void*,size_t)");iov.iov_len = buffer_size;

		AKA_mark("lis===92###sois===2959###eois===2988###lif===5###soif===151###eoif===180###ins===true###function===./app-framework-binder/src/websock.c/ws_read(struct websock*,void*,size_t)");return ws_readv(ws, &iov, 1);

}

/** Instrumented function websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int) */
static int websock_send_internal_v(struct websock *ws, unsigned char first, const struct iovec *iovec, int count)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");AKA_fCall++;
		AKA_mark("lis===97###sois===3109###eois===3130###lif===2###soif===117###eoif===138###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");struct iovec iov[32];

		AKA_mark("lis===98###sois===3132###eois===3141###lif===3###soif===140###eoif===149###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");int i, j;

		AKA_mark("lis===99###sois===3143###eois===3165###lif===4###soif===151###eoif===173###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");size_t pos, size, len;

		AKA_mark("lis===100###sois===3167###eois===3178###lif===5###soif===175###eoif===186###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");ssize_t rc;

		AKA_mark("lis===101###sois===3180###eois===3205###lif===6###soif===188###eoif===213###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");unsigned char header[32];


	/* checks count */
		if (AKA_mark("lis===104###sois===3232###eois===3291###lif===9###soif===240###eoif===299###ifc===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)") && ((AKA_mark("lis===104###sois===3232###eois===3241###lif===9###soif===240###eoif===249###isc===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)")&&count < 0)	||(AKA_mark("lis===104###sois===3245###eois===3291###lif===9###soif===253###eoif===299###isc===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)")&&(count + 1) > (int)(sizeof iov / sizeof * iov)))) {
				AKA_mark("lis===105###sois===3297###eois===3312###lif===10###soif===305###eoif===320###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");errno = EINVAL;

				AKA_mark("lis===106###sois===3315###eois===3325###lif===11###soif===323###eoif===333###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");return -1;

	}
	else {AKA_mark("lis===-104-###sois===-3232-###eois===-323259-###lif===-9-###soif===-###eoif===-299-###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");}


	/* computes the size */
		AKA_mark("lis===110###sois===3356###eois===3365###lif===15###soif===364###eoif===373###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");size = 0;

		AKA_mark("lis===111###sois===3367###eois===3373###lif===16###soif===375###eoif===381###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");i = 1;

		AKA_mark("lis===112###sois===3380###eois===3387###lif===17###soif===388###eoif===395###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");for (j = 0 ;AKA_mark("lis===112###sois===3388###eois===3397###lif===17###soif===396###eoif===405###ifc===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)") && AKA_mark("lis===112###sois===3388###eois===3397###lif===17###soif===396###eoif===405###isc===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)")&&j < count;({AKA_mark("lis===112###sois===3400###eois===3403###lif===17###soif===408###eoif===411###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");j++;})) {
				AKA_mark("lis===113###sois===3409###eois===3445###lif===18###soif===417###eoif===453###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");iov[i].iov_base = iovec[j].iov_base;

				AKA_mark("lis===114###sois===3448###eois===3471###lif===19###soif===456###eoif===479###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");len = iovec[j].iov_len;

				if (AKA_mark("lis===115###sois===3478###eois===3486###lif===20###soif===486###eoif===494###ifc===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)") && (AKA_mark("lis===115###sois===3478###eois===3486###lif===20###soif===486###eoif===494###isc===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)")&&len != 0)) {
						AKA_mark("lis===116###sois===3493###eois===3514###lif===21###soif===501###eoif===522###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");iov[i].iov_len = len;

						AKA_mark("lis===117###sois===3518###eois===3530###lif===22###soif===526###eoif===538###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");size += len;

						AKA_mark("lis===118###sois===3534###eois===3538###lif===23###soif===542###eoif===546###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");i++;

		}
		else {AKA_mark("lis===-115-###sois===-3478-###eois===-34788-###lif===-20-###soif===-###eoif===-494-###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");}

	}


	/* makes the header */
		AKA_mark("lis===123###sois===3572###eois===3580###lif===28###soif===580###eoif===588###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");pos = 0;

		AKA_mark("lis===124###sois===3582###eois===3604###lif===29###soif===590###eoif===612###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");header[pos++] = first;

		AKA_mark("lis===125###sois===3606###eois===3629###lif===30###soif===614###eoif===637###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");size = (uint64_t) size;

		if (AKA_mark("lis===126###sois===3635###eois===3645###lif===31###soif===643###eoif===653###ifc===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)") && (AKA_mark("lis===126###sois===3635###eois===3645###lif===31###soif===643###eoif===653###isc===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)")&&size < 126)) {
				AKA_mark("lis===127###sois===3651###eois===3713###lif===32###soif===659###eoif===721###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");header[pos++] = FRAME_SET_MASK(0) | FRAME_SET_LENGTH(size, 0);

	}
	else {
				if (AKA_mark("lis===129###sois===3730###eois===3742###lif===34###soif===738###eoif===750###ifc===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)") && (AKA_mark("lis===129###sois===3730###eois===3742###lif===34###soif===738###eoif===750###isc===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)")&&size < 65536)) {
						AKA_mark("lis===130###sois===3749###eois===3789###lif===35###soif===757###eoif===797###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");header[pos++] = FRAME_SET_MASK(0) | 126;

		}
		else {
						AKA_mark("lis===132###sois===3804###eois===3844###lif===37###soif===812###eoif===852###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");header[pos++] = FRAME_SET_MASK(0) | 127;

						AKA_mark("lis===133###sois===3848###eois===3890###lif===38###soif===856###eoif===898###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");header[pos++] = FRAME_SET_LENGTH(size, 7);

						AKA_mark("lis===134###sois===3894###eois===3936###lif===39###soif===902###eoif===944###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");header[pos++] = FRAME_SET_LENGTH(size, 6);

						AKA_mark("lis===135###sois===3940###eois===3982###lif===40###soif===948###eoif===990###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");header[pos++] = FRAME_SET_LENGTH(size, 5);

						AKA_mark("lis===136###sois===3986###eois===4028###lif===41###soif===994###eoif===1036###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");header[pos++] = FRAME_SET_LENGTH(size, 4);

						AKA_mark("lis===137###sois===4032###eois===4074###lif===42###soif===1040###eoif===1082###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");header[pos++] = FRAME_SET_LENGTH(size, 3);

						AKA_mark("lis===138###sois===4078###eois===4120###lif===43###soif===1086###eoif===1128###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");header[pos++] = FRAME_SET_LENGTH(size, 2);

		}

				AKA_mark("lis===140###sois===4127###eois===4169###lif===45###soif===1135###eoif===1177###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");header[pos++] = FRAME_SET_LENGTH(size, 1);

				AKA_mark("lis===141###sois===4172###eois===4214###lif===46###soif===1180###eoif===1222###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");header[pos++] = FRAME_SET_LENGTH(size, 0);

	}


	/* allocates the vec */
		AKA_mark("lis===145###sois===4245###eois===4270###lif===50###soif===1253###eoif===1278###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");iov[0].iov_base = header;

		AKA_mark("lis===146###sois===4272###eois===4293###lif===51###soif===1280###eoif===1301###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");iov[0].iov_len = pos;

		AKA_mark("lis===147###sois===4295###eois===4322###lif===52###soif===1303###eoif===1330###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");rc = ws_writev(ws, iov, i);


		AKA_mark("lis===149###sois===4325###eois===4348###lif===54###soif===1333###eoif===1356###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal_v(struct websock*,unsigned char,const struct iovec*,int)");return rc < 0 ? -1 : 0;

}

/** Instrumented function websock_send_internal(struct websock*,unsigned char,const void*,size_t) */
static int websock_send_internal(struct websock *ws, unsigned char first, const void *buffer, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_send_internal(struct websock*,unsigned char,const void*,size_t)");AKA_fCall++;
		AKA_mark("lis===154###sois===4462###eois===4479###lif===2###soif===110###eoif===127###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal(struct websock*,unsigned char,const void*,size_t)");struct iovec iov;


		AKA_mark("lis===156###sois===4482###eois===4512###lif===4###soif===130###eoif===160###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal(struct websock*,unsigned char,const void*,size_t)");iov.iov_base = (void *)buffer;

		AKA_mark("lis===157###sois===4514###eois===4533###lif===5###soif===162###eoif===181###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal(struct websock*,unsigned char,const void*,size_t)");iov.iov_len = size;

		AKA_mark("lis===158###sois===4535###eois===4586###lif===6###soif===183###eoif===234###ins===true###function===./app-framework-binder/src/websock.c/websock_send_internal(struct websock*,unsigned char,const void*,size_t)");return websock_send_internal_v(ws, first, &iov, 1);

}

/** Instrumented function websock_send_v(struct websock*,int,int,int,int,int,const struct iovec*,int) */
static inline int websock_send_v(struct websock *ws, int last, int rsv1, int rsv2, int rsv3, int opcode, const struct iovec *iovec, int count)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_send_v(struct websock*,int,int,int,int,int,const struct iovec*,int)");AKA_fCall++;
		AKA_mark("lis===163###sois===4736###eois===4907###lif===2###soif===146###eoif===317###ins===true###function===./app-framework-binder/src/websock.c/websock_send_v(struct websock*,int,int,int,int,int,const struct iovec*,int)");unsigned char first = (unsigned char)(FRAME_SET_FIN(last)
				| FRAME_SET_RSV1(rsv1)
				| FRAME_SET_RSV1(rsv2)
				| FRAME_SET_RSV1(rsv3)
				| FRAME_SET_OPCODE(opcode));

		AKA_mark("lis===168###sois===4909###eois===4965###lif===7###soif===319###eoif===375###ins===true###function===./app-framework-binder/src/websock.c/websock_send_v(struct websock*,int,int,int,int,int,const struct iovec*,int)");return websock_send_internal_v(ws, first, iovec, count);

}

/** Instrumented function websock_send(struct websock*,int,int,int,int,int,const void*,size_t) */
static inline int websock_send(struct websock *ws, int last, int rsv1, int rsv2, int rsv3, int opcode, const void *buffer, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_send(struct websock*,int,int,int,int,int,const void*,size_t)");AKA_fCall++;
		AKA_mark("lis===173###sois===5108###eois===5279###lif===2###soif===139###eoif===310###ins===true###function===./app-framework-binder/src/websock.c/websock_send(struct websock*,int,int,int,int,int,const void*,size_t)");unsigned char first = (unsigned char)(FRAME_SET_FIN(last)
				| FRAME_SET_RSV1(rsv1)
				| FRAME_SET_RSV1(rsv2)
				| FRAME_SET_RSV1(rsv3)
				| FRAME_SET_OPCODE(opcode));

		AKA_mark("lis===178###sois===5281###eois===5335###lif===7###soif===312###eoif===366###ins===true###function===./app-framework-binder/src/websock.c/websock_send(struct websock*,int,int,int,int,int,const void*,size_t)");return websock_send_internal(ws, first, buffer, size);

}

/** Instrumented function websock_close_empty(struct websock*) */
int websock_close_empty(struct websock *ws)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_close_empty(struct websock*)");AKA_fCall++;
		AKA_mark("lis===183###sois===5386###eois===5444###lif===2###soif===47###eoif===105###ins===true###function===./app-framework-binder/src/websock.c/websock_close_empty(struct websock*)");return websock_close(ws, WEBSOCKET_CODE_NOT_SET, NULL, 0);

}

/** Instrumented function websock_close(struct websock*,uint16_t,const void*,size_t) */
int websock_close(struct websock *ws, uint16_t code, const void *data, size_t length)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");AKA_fCall++;
		AKA_mark("lis===188###sois===5537###eois===5561###lif===2###soif===89###eoif===113###ins===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");unsigned char buffer[2];

		AKA_mark("lis===189###sois===5563###eois===5583###lif===3###soif===115###eoif===135###ins===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");struct iovec iov[2];


		if (AKA_mark("lis===191###sois===5590###eois===5635###lif===5###soif===142###eoif===187###ifc===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)") && ((AKA_mark("lis===191###sois===5590###eois===5620###lif===5###soif===142###eoif===172###isc===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)")&&code == WEBSOCKET_CODE_NOT_SET)	&&(AKA_mark("lis===191###sois===5624###eois===5635###lif===5###soif===176###eoif===187###isc===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)")&&length == 0))) {
		AKA_mark("lis===192###sois===5639###eois===5698###lif===6###soif===191###eoif===250###ins===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");return websock_send(ws, 1, 0, 0, 0, OPCODE_CLOSE, NULL, 0);
	}
	else {AKA_mark("lis===-191-###sois===-5590-###eois===-559045-###lif===-5-###soif===-###eoif===-187-###ins===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");}


	/* checks the length */
		if (AKA_mark("lis===195###sois===5730###eois===5742###lif===9###soif===282###eoif===294###ifc===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)") && (AKA_mark("lis===195###sois===5730###eois===5742###lif===9###soif===282###eoif===294###isc===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)")&&length > 123)) {
				AKA_mark("lis===196###sois===5748###eois===5763###lif===10###soif===300###eoif===315###ins===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");errno = EINVAL;

				AKA_mark("lis===197###sois===5766###eois===5776###lif===11###soif===318###eoif===328###ins===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");return -1;

	}
	else {AKA_mark("lis===-195-###sois===-5730-###eois===-573012-###lif===-9-###soif===-###eoif===-294-###ins===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");}


	/* prepare the buffer */
		AKA_mark("lis===201###sois===5808###eois===5856###lif===15###soif===360###eoif===408###ins===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");buffer[0] = (unsigned char)((code >> 8) & 0xFF);

		AKA_mark("lis===202###sois===5858###eois===5899###lif===16###soif===410###eoif===451###ins===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");buffer[1] = (unsigned char)(code & 0xFF);


	/* Send server-side closing handshake */
		AKA_mark("lis===205###sois===5944###eois===5977###lif===19###soif===496###eoif===529###ins===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");iov[0].iov_base = (void *)buffer;

		AKA_mark("lis===206###sois===5979###eois===5998###lif===20###soif===531###eoif===550###ins===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");iov[0].iov_len = 2;

		AKA_mark("lis===207###sois===6000###eois===6031###lif===21###soif===552###eoif===583###ins===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");iov[1].iov_base = (void *)data;

		AKA_mark("lis===208###sois===6033###eois===6057###lif===22###soif===585###eoif===609###ins===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");iov[1].iov_len = length;

		AKA_mark("lis===209###sois===6059###eois===6119###lif===23###soif===611###eoif===671###ins===true###function===./app-framework-binder/src/websock.c/websock_close(struct websock*,uint16_t,const void*,size_t)");return websock_send_v(ws, 1, 0, 0, 0, OPCODE_CLOSE, iov, 2);

}

/** Instrumented function websock_ping(struct websock*,const void*,size_t) */
int websock_ping(struct websock *ws, const void *data, size_t length)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_ping(struct websock*,const void*,size_t)");AKA_fCall++;
	/* checks the length */
		if (AKA_mark("lis===215###sois===6225###eois===6237###lif===3###soif===102###eoif===114###ifc===true###function===./app-framework-binder/src/websock.c/websock_ping(struct websock*,const void*,size_t)") && (AKA_mark("lis===215###sois===6225###eois===6237###lif===3###soif===102###eoif===114###isc===true###function===./app-framework-binder/src/websock.c/websock_ping(struct websock*,const void*,size_t)")&&length > 125)) {
				AKA_mark("lis===216###sois===6243###eois===6258###lif===4###soif===120###eoif===135###ins===true###function===./app-framework-binder/src/websock.c/websock_ping(struct websock*,const void*,size_t)");errno = EINVAL;

				AKA_mark("lis===217###sois===6261###eois===6271###lif===5###soif===138###eoif===148###ins===true###function===./app-framework-binder/src/websock.c/websock_ping(struct websock*,const void*,size_t)");return -1;

	}
	else {AKA_mark("lis===-215-###sois===-6225-###eois===-622512-###lif===-3-###soif===-###eoif===-114-###ins===true###function===./app-framework-binder/src/websock.c/websock_ping(struct websock*,const void*,size_t)");}


		AKA_mark("lis===220###sois===6277###eois===6340###lif===8###soif===154###eoif===217###ins===true###function===./app-framework-binder/src/websock.c/websock_ping(struct websock*,const void*,size_t)");return websock_send(ws, 1, 0, 0, 0, OPCODE_PING, data, length);

}

/** Instrumented function websock_pong(struct websock*,const void*,size_t) */
int websock_pong(struct websock *ws, const void *data, size_t length)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_pong(struct websock*,const void*,size_t)");AKA_fCall++;
	/* checks the length */
		if (AKA_mark("lis===226###sois===6446###eois===6458###lif===3###soif===102###eoif===114###ifc===true###function===./app-framework-binder/src/websock.c/websock_pong(struct websock*,const void*,size_t)") && (AKA_mark("lis===226###sois===6446###eois===6458###lif===3###soif===102###eoif===114###isc===true###function===./app-framework-binder/src/websock.c/websock_pong(struct websock*,const void*,size_t)")&&length > 125)) {
				AKA_mark("lis===227###sois===6464###eois===6479###lif===4###soif===120###eoif===135###ins===true###function===./app-framework-binder/src/websock.c/websock_pong(struct websock*,const void*,size_t)");errno = EINVAL;

				AKA_mark("lis===228###sois===6482###eois===6492###lif===5###soif===138###eoif===148###ins===true###function===./app-framework-binder/src/websock.c/websock_pong(struct websock*,const void*,size_t)");return -1;

	}
	else {AKA_mark("lis===-226-###sois===-6446-###eois===-644612-###lif===-3-###soif===-###eoif===-114-###ins===true###function===./app-framework-binder/src/websock.c/websock_pong(struct websock*,const void*,size_t)");}


		AKA_mark("lis===231###sois===6498###eois===6561###lif===8###soif===154###eoif===217###ins===true###function===./app-framework-binder/src/websock.c/websock_pong(struct websock*,const void*,size_t)");return websock_send(ws, 1, 0, 0, 0, OPCODE_PONG, data, length);

}

/** Instrumented function websock_text(struct websock*,int,const void*,size_t) */
int websock_text(struct websock *ws, int last, const void *text, size_t length)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_text(struct websock*,int,const void*,size_t)");AKA_fCall++;
		AKA_mark("lis===236###sois===6648###eois===6714###lif===2###soif===83###eoif===149###ins===true###function===./app-framework-binder/src/websock.c/websock_text(struct websock*,int,const void*,size_t)");return websock_send(ws, last, 0, 0, 0, OPCODE_TEXT, text, length);

}

/** Instrumented function websock_text_v(struct websock*,int,const struct iovec*,int) */
int websock_text_v(struct websock *ws, int last, const struct iovec *iovec, int count)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_text_v(struct websock*,int,const struct iovec*,int)");AKA_fCall++;
		AKA_mark("lis===241###sois===6808###eois===6876###lif===2###soif===90###eoif===158###ins===true###function===./app-framework-binder/src/websock.c/websock_text_v(struct websock*,int,const struct iovec*,int)");return websock_send_v(ws, last, 0, 0, 0, OPCODE_TEXT, iovec, count);

}

/** Instrumented function websock_binary(struct websock*,int,const void*,size_t) */
int websock_binary(struct websock *ws, int last, const void *data, size_t length)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_binary(struct websock*,int,const void*,size_t)");AKA_fCall++;
		AKA_mark("lis===246###sois===6965###eois===7033###lif===2###soif===85###eoif===153###ins===true###function===./app-framework-binder/src/websock.c/websock_binary(struct websock*,int,const void*,size_t)");return websock_send(ws, last, 0, 0, 0, OPCODE_BINARY, data, length);

}

/** Instrumented function websock_binary_v(struct websock*,int,const struct iovec*,int) */
int websock_binary_v(struct websock *ws, int last, const struct iovec *iovec, int count)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_binary_v(struct websock*,int,const struct iovec*,int)");AKA_fCall++;
		AKA_mark("lis===251###sois===7129###eois===7199###lif===2###soif===92###eoif===162###ins===true###function===./app-framework-binder/src/websock.c/websock_binary_v(struct websock*,int,const struct iovec*,int)");return websock_send_v(ws, last, 0, 0, 0, OPCODE_BINARY, iovec, count);

}

/** Instrumented function websock_continue(struct websock*,int,const void*,size_t) */
int websock_continue(struct websock *ws, int last, const void *data, size_t length)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_continue(struct websock*,int,const void*,size_t)");AKA_fCall++;
		AKA_mark("lis===256###sois===7290###eois===7364###lif===2###soif===87###eoif===161###ins===true###function===./app-framework-binder/src/websock.c/websock_continue(struct websock*,int,const void*,size_t)");return websock_send(ws, last, 0, 0, 0, OPCODE_CONTINUATION, data, length);

}

/** Instrumented function websock_continue_v(struct websock*,int,const struct iovec*,int) */
int websock_continue_v(struct websock *ws, int last, const struct iovec *iovec, int count)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_continue_v(struct websock*,int,const struct iovec*,int)");AKA_fCall++;
		AKA_mark("lis===261###sois===7462###eois===7538###lif===2###soif===94###eoif===170###ins===true###function===./app-framework-binder/src/websock.c/websock_continue_v(struct websock*,int,const struct iovec*,int)");return websock_send_v(ws, last, 0, 0, 0, OPCODE_CONTINUATION, iovec, count);

}

/** Instrumented function websock_error(struct websock*,uint16_t,const void*,size_t) */
int websock_error(struct websock *ws, uint16_t code, const void *data, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_error(struct websock*,uint16_t,const void*,size_t)");AKA_fCall++;
		AKA_mark("lis===266###sois===7629###eois===7674###lif===2###soif===87###eoif===132###ins===true###function===./app-framework-binder/src/websock.c/websock_error(struct websock*,uint16_t,const void*,size_t)");int rc = websock_close(ws, code, data, size);

		if (AKA_mark("lis===267###sois===7680###eois===7705###lif===3###soif===138###eoif===163###ifc===true###function===./app-framework-binder/src/websock.c/websock_error(struct websock*,uint16_t,const void*,size_t)") && (AKA_mark("lis===267###sois===7680###eois===7705###lif===3###soif===138###eoif===163###isc===true###function===./app-framework-binder/src/websock.c/websock_error(struct websock*,uint16_t,const void*,size_t)")&&ws->itf->on_error != NULL)) {
		AKA_mark("lis===268###sois===7709###eois===7758###lif===4###soif===167###eoif===216###ins===true###function===./app-framework-binder/src/websock.c/websock_error(struct websock*,uint16_t,const void*,size_t)");ws->itf->on_error(ws->closure, code, data, size);
	}
	else {AKA_mark("lis===-267-###sois===-7680-###eois===-768025-###lif===-3-###soif===-###eoif===-163-###ins===true###function===./app-framework-binder/src/websock.c/websock_error(struct websock*,uint16_t,const void*,size_t)");}

		AKA_mark("lis===269###sois===7760###eois===7770###lif===5###soif===218###eoif===228###ins===true###function===./app-framework-binder/src/websock.c/websock_error(struct websock*,uint16_t,const void*,size_t)");return rc;

}

/** Instrumented function read_header(struct websock*) */
static int read_header(struct websock *ws)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/read_header(struct websock*)");AKA_fCall++;
		if (AKA_mark("lis===274###sois===7824###eois===7848###lif===2###soif===50###eoif===74###ifc===true###function===./app-framework-binder/src/websock.c/read_header(struct websock*)") && (AKA_mark("lis===274###sois===7824###eois===7848###lif===2###soif===50###eoif===74###isc===true###function===./app-framework-binder/src/websock.c/read_header(struct websock*)")&&ws->lenhead < ws->szhead)) {
				AKA_mark("lis===275###sois===7854###eois===7948###lif===3###soif===80###eoif===174###ins===true###function===./app-framework-binder/src/websock.c/read_header(struct websock*)");ssize_t rbc =
		    ws_read(ws, &ws->header[ws->lenhead], (size_t)(ws->szhead - ws->lenhead));

				if (AKA_mark("lis===277###sois===7955###eois===7962###lif===5###soif===181###eoif===188###ifc===true###function===./app-framework-binder/src/websock.c/read_header(struct websock*)") && (AKA_mark("lis===277###sois===7955###eois===7962###lif===5###soif===181###eoif===188###isc===true###function===./app-framework-binder/src/websock.c/read_header(struct websock*)")&&rbc < 0)) {
			AKA_mark("lis===278###sois===7967###eois===7977###lif===6###soif===193###eoif===203###ins===true###function===./app-framework-binder/src/websock.c/read_header(struct websock*)");return -1;
		}
		else {AKA_mark("lis===-277-###sois===-7955-###eois===-79557-###lif===-5-###soif===-###eoif===-188-###ins===true###function===./app-framework-binder/src/websock.c/read_header(struct websock*)");}

				AKA_mark("lis===279###sois===7980###eois===8004###lif===7###soif===206###eoif===230###ins===true###function===./app-framework-binder/src/websock.c/read_header(struct websock*)");ws->lenhead += (int)rbc;

	}
	else {AKA_mark("lis===-274-###sois===-7824-###eois===-782424-###lif===-2-###soif===-###eoif===-74-###ins===true###function===./app-framework-binder/src/websock.c/read_header(struct websock*)");}

		AKA_mark("lis===281###sois===8009###eois===8018###lif===9###soif===235###eoif===244###ins===true###function===./app-framework-binder/src/websock.c/read_header(struct websock*)");return 0;

}

/** Instrumented function check_control_header(struct websock*) */
static int check_control_header(struct websock *ws)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/check_control_header(struct websock*)");AKA_fCall++;
	/* sanity checks */
		if (AKA_mark("lis===287###sois===8102###eois===8136###lif===3###soif===80###eoif===114###ifc===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)") && (AKA_mark("lis===287###sois===8102###eois===8136###lif===3###soif===80###eoif===114###isc===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)")&&FRAME_GET_RSV1(ws->header[0]) != 0)) {
		AKA_mark("lis===288###sois===8140###eois===8149###lif===4###soif===118###eoif===127###ins===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)");return 0;
	}
	else {AKA_mark("lis===-287-###sois===-8102-###eois===-810234-###lif===-3-###soif===-###eoif===-114-###ins===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)");}

		if (AKA_mark("lis===289###sois===8155###eois===8189###lif===5###soif===133###eoif===167###ifc===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)") && (AKA_mark("lis===289###sois===8155###eois===8189###lif===5###soif===133###eoif===167###isc===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)")&&FRAME_GET_RSV2(ws->header[0]) != 0)) {
		AKA_mark("lis===290###sois===8193###eois===8202###lif===6###soif===171###eoif===180###ins===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)");return 0;
	}
	else {AKA_mark("lis===-289-###sois===-8155-###eois===-815534-###lif===-5-###soif===-###eoif===-167-###ins===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)");}

		if (AKA_mark("lis===291###sois===8208###eois===8242###lif===7###soif===186###eoif===220###ifc===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)") && (AKA_mark("lis===291###sois===8208###eois===8242###lif===7###soif===186###eoif===220###isc===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)")&&FRAME_GET_RSV3(ws->header[0]) != 0)) {
		AKA_mark("lis===292###sois===8246###eois===8255###lif===8###soif===224###eoif===233###ins===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)");return 0;
	}
	else {AKA_mark("lis===-291-###sois===-8208-###eois===-820834-###lif===-7-###soif===-###eoif===-220-###ins===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)");}

		if (AKA_mark("lis===293###sois===8261###eois===8303###lif===9###soif===239###eoif===281###ifc===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)") && (AKA_mark("lis===293###sois===8261###eois===8303###lif===9###soif===239###eoif===281###isc===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)")&&FRAME_GET_PAYLOAD_LEN(ws->header[1]) > 125)) {
		AKA_mark("lis===294###sois===8307###eois===8316###lif===10###soif===285###eoif===294###ins===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)");return 0;
	}
	else {AKA_mark("lis===-293-###sois===-8261-###eois===-826142-###lif===-9-###soif===-###eoif===-281-###ins===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)");}

		if (AKA_mark("lis===295###sois===8322###eois===8369###lif===11###soif===300###eoif===347###ifc===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)") && (AKA_mark("lis===295###sois===8322###eois===8369###lif===11###soif===300###eoif===347###isc===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)")&&FRAME_GET_OPCODE(ws->header[0]) == OPCODE_CLOSE)) {
		AKA_mark("lis===296###sois===8373###eois===8422###lif===12###soif===351###eoif===400###ins===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)");return FRAME_GET_PAYLOAD_LEN(ws->header[1]) != 1;
	}
	else {AKA_mark("lis===-295-###sois===-8322-###eois===-832247-###lif===-11-###soif===-###eoif===-347-###ins===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)");}

		AKA_mark("lis===297###sois===8424###eois===8433###lif===13###soif===402###eoif===411###ins===true###function===./app-framework-binder/src/websock.c/check_control_header(struct websock*)");return 1;

}

/** Instrumented function websock_dispatch(struct websock*,int) */
int websock_dispatch(struct websock *ws, int loop)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");AKA_fCall++;
		AKA_mark("lis===302###sois===8491###eois===8505###lif===2###soif===54###eoif===68###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");uint16_t code;

	loop:
	AKA_mark("lis===304###sois===8521###eois===8530###lif===4###soif===84###eoif===93###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");switch(ws->state){
		case STATE_INIT: if(ws->state == STATE_INIT)AKA_mark("lis===305###sois===8535###eois===8551###lif===5###soif===98###eoif===114###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

			AKA_mark("lis===306###sois===8554###eois===8570###lif===6###soif===117###eoif===133###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->lenhead = 0;

			AKA_mark("lis===307###sois===8573###eois===8588###lif===7###soif===136###eoif===151###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->szhead = 2;

			AKA_mark("lis===308###sois===8591###eois===8615###lif===8###soif===154###eoif===178###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->state = STATE_START;

		/*@fallthrough@*/

		case STATE_START: if(ws->state == STATE_START)AKA_mark("lis===311###sois===8638###eois===8655###lif===11###soif===201###eoif===218###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

		/* read the header */
			if (AKA_mark("lis===313###sois===8686###eois===8701###lif===13###soif===249###eoif===264###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===313###sois===8686###eois===8701###lif===13###soif===249###eoif===264###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&read_header(ws))) {
		AKA_mark("lis===314###sois===8706###eois===8716###lif===14###soif===269###eoif===279###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");return -1;
	}
	else {
		if (AKA_mark("lis===315###sois===8728###eois===8752###lif===15###soif===291###eoif===315###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===315###sois===8728###eois===8752###lif===15###soif===291###eoif===315###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&ws->lenhead < ws->szhead)) {
			AKA_mark("lis===316###sois===8757###eois===8766###lif===16###soif===320###eoif===329###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");return 0;
		}
		else {AKA_mark("lis===-315-###sois===-8728-###eois===-872824-###lif===-15-###soif===-###eoif===-315-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}
	}

		/* fast track */
			AKA_mark("lis===318###sois===8796###eois===8827###lif===18###soif===359###eoif===390###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");switch(FRAME_GET_OPCODE(ws->header[0])){
				case OPCODE_CONTINUATION: if(FRAME_GET_OPCODE(ws->header[0]) == OPCODE_CONTINUATION)AKA_mark("lis===319###sois===8833###eois===8858###lif===19###soif===396###eoif===421###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

				case OPCODE_TEXT: if(FRAME_GET_OPCODE(ws->header[0]) == OPCODE_TEXT)AKA_mark("lis===320###sois===8861###eois===8878###lif===20###soif===424###eoif===441###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

				case OPCODE_BINARY: if(FRAME_GET_OPCODE(ws->header[0]) == OPCODE_BINARY)AKA_mark("lis===321###sois===8881###eois===8900###lif===21###soif===444###eoif===463###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					AKA_mark("lis===322###sois===8904###eois===8910###lif===22###soif===467###eoif===473###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");break;

				case OPCODE_CLOSE: if(FRAME_GET_OPCODE(ws->header[0]) == OPCODE_CLOSE)AKA_mark("lis===323###sois===8913###eois===8931###lif===23###soif===476###eoif===494###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					if (AKA_mark("lis===324###sois===8939###eois===8964###lif===24###soif===502###eoif===527###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===324###sois===8939###eois===8964###lif===24###soif===502###eoif===527###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&!check_control_header(ws))) {
			AKA_mark("lis===325###sois===8970###eois===8990###lif===25###soif===533###eoif===553###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");goto protocol_error;
		}
		else {AKA_mark("lis===-324-###sois===-8939-###eois===-893925-###lif===-24-###soif===-###eoif===-527-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}

					if (AKA_mark("lis===326###sois===8998###eois===9034###lif===26###soif===561###eoif===597###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===326###sois===8998###eois===9034###lif===26###soif===561###eoif===597###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&FRAME_GET_PAYLOAD_LEN(ws->header[1]))) {
			AKA_mark("lis===327###sois===9040###eois===9056###lif===27###soif===603###eoif===619###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->szhead += 2;
		}
		else {AKA_mark("lis===-326-###sois===-8998-###eois===-899836-###lif===-26-###soif===-###eoif===-597-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}

					AKA_mark("lis===328###sois===9060###eois===9066###lif===28###soif===623###eoif===629###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");break;

				case OPCODE_PING: if(FRAME_GET_OPCODE(ws->header[0]) == OPCODE_PING)AKA_mark("lis===329###sois===9069###eois===9086###lif===29###soif===632###eoif===649###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

				case OPCODE_PONG: if(FRAME_GET_OPCODE(ws->header[0]) == OPCODE_PONG)AKA_mark("lis===330###sois===9089###eois===9106###lif===30###soif===652###eoif===669###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					if (AKA_mark("lis===331###sois===9114###eois===9139###lif===31###soif===677###eoif===702###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===331###sois===9114###eois===9139###lif===31###soif===677###eoif===702###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&!check_control_header(ws))) {
			AKA_mark("lis===332###sois===9145###eois===9165###lif===32###soif===708###eoif===728###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");goto protocol_error;
		}
		else {AKA_mark("lis===-331-###sois===-9114-###eois===-911425-###lif===-31-###soif===-###eoif===-702-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}

				default: if(FRAME_GET_OPCODE(ws->header[0]) != OPCODE_CONTINUATION && FRAME_GET_OPCODE(ws->header[0]) != OPCODE_TEXT && FRAME_GET_OPCODE(ws->header[0]) != OPCODE_BINARY && FRAME_GET_OPCODE(ws->header[0]) != OPCODE_CLOSE && FRAME_GET_OPCODE(ws->header[0]) != OPCODE_PING && FRAME_GET_OPCODE(ws->header[0]) != OPCODE_PONG)AKA_mark("lis===333###sois===9168###eois===9176###lif===33###soif===731###eoif===739###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					AKA_mark("lis===334###sois===9180###eois===9186###lif===34###soif===743###eoif===749###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");break;

		}

		/* update heading size */
			AKA_mark("lis===337###sois===9229###eois===9265###lif===37###soif===792###eoif===828###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");switch(FRAME_GET_PAYLOAD_LEN(ws->header[1])){
				case 127: if(FRAME_GET_PAYLOAD_LEN(ws->header[1]) == 127)AKA_mark("lis===338###sois===9271###eois===9280###lif===38###soif===834###eoif===843###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					AKA_mark("lis===339###sois===9284###eois===9300###lif===39###soif===847###eoif===863###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->szhead += 6;

			/*@fallthrough@*/
				case 126: if(FRAME_GET_PAYLOAD_LEN(ws->header[1]) == 126)AKA_mark("lis===341###sois===9324###eois===9333###lif===41###soif===887###eoif===896###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					AKA_mark("lis===342###sois===9337###eois===9353###lif===42###soif===900###eoif===916###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->szhead += 2;

			/*@fallthrough@*/
				default: if(FRAME_GET_PAYLOAD_LEN(ws->header[1]) != 127 && FRAME_GET_PAYLOAD_LEN(ws->header[1]) != 126)AKA_mark("lis===344###sois===9377###eois===9385###lif===44###soif===940###eoif===948###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					AKA_mark("lis===345###sois===9389###eois===9437###lif===45###soif===952###eoif===1000###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->szhead += 4 * FRAME_GET_MASK(ws->header[1]);

		}

			AKA_mark("lis===347###sois===9444###eois===9469###lif===47###soif===1007###eoif===1032###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->state = STATE_LENGTH;

		/*@fallthrough@*/

		case STATE_LENGTH: if(ws->state == STATE_LENGTH)AKA_mark("lis===350###sois===9492###eois===9510###lif===50###soif===1055###eoif===1073###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

		/* continue to read the header */
			if (AKA_mark("lis===352###sois===9553###eois===9568###lif===52###soif===1116###eoif===1131###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===352###sois===9553###eois===9568###lif===52###soif===1116###eoif===1131###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&read_header(ws))) {
		AKA_mark("lis===353###sois===9573###eois===9583###lif===53###soif===1136###eoif===1146###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");return -1;
	}
	else {
		if (AKA_mark("lis===354###sois===9595###eois===9619###lif===54###soif===1158###eoif===1182###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===354###sois===9595###eois===9619###lif===54###soif===1158###eoif===1182###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&ws->lenhead < ws->szhead)) {
			AKA_mark("lis===355###sois===9624###eois===9633###lif===55###soif===1187###eoif===1196###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");return 0;
		}
		else {AKA_mark("lis===-354-###sois===-9595-###eois===-959524-###lif===-54-###soif===-###eoif===-1182-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}
	}


		/* compute length */
			AKA_mark("lis===358###sois===9668###eois===9704###lif===58###soif===1231###eoif===1267###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");switch(FRAME_GET_PAYLOAD_LEN(ws->header[1])){
				case 127: if(FRAME_GET_PAYLOAD_LEN(ws->header[1]) == 127)AKA_mark("lis===359###sois===9710###eois===9719###lif===59###soif===1273###eoif===1282###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					AKA_mark("lis===360###sois===9723###eois===10068###lif===60###soif===1286###eoif===1631###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->length = (((uint64_t) ws->header[2]) << 56)
			    | (((uint64_t) ws->header[3]) << 48)
			    | (((uint64_t) ws->header[4]) << 40)
			    | (((uint64_t) ws->header[5]) << 32)
			    | (((uint64_t) ws->header[6]) << 24)
			    | (((uint64_t) ws->header[7]) << 16)
			    | (((uint64_t) ws->header[8]) << 8)
			    | (uint64_t) ws->header[9];

					AKA_mark("lis===368###sois===10072###eois===10078###lif===68###soif===1635###eoif===1641###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");break;

				case 126: if(FRAME_GET_PAYLOAD_LEN(ws->header[1]) == 126)AKA_mark("lis===369###sois===10081###eois===10090###lif===69###soif===1644###eoif===1653###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					AKA_mark("lis===370###sois===10094###eois===10175###lif===70###soif===1657###eoif===1738###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->length = (((uint64_t) ws->header[2]) << 8)
			    | (uint64_t) ws->header[3];

					AKA_mark("lis===372###sois===10179###eois===10185###lif===72###soif===1742###eoif===1748###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");break;

				default: if(FRAME_GET_PAYLOAD_LEN(ws->header[1]) != 127 && FRAME_GET_PAYLOAD_LEN(ws->header[1]) != 126)AKA_mark("lis===373###sois===10188###eois===10196###lif===73###soif===1751###eoif===1759###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					AKA_mark("lis===374###sois===10200###eois===10250###lif===74###soif===1763###eoif===1813###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->length = FRAME_GET_PAYLOAD_LEN(ws->header[1]);

					AKA_mark("lis===375###sois===10254###eois===10260###lif===75###soif===1817###eoif===1823###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");break;

		}

			if (AKA_mark("lis===377###sois===10271###eois===10337###lif===77###soif===1834###eoif===1900###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && ((AKA_mark("lis===377###sois===10271###eois===10318###lif===77###soif===1834###eoif===1881###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&FRAME_GET_OPCODE(ws->header[0]) == OPCODE_CLOSE)	&&(AKA_mark("lis===377###sois===10322###eois===10337###lif===77###soif===1885###eoif===1900###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&ws->length != 0))) {
		AKA_mark("lis===378###sois===10342###eois===10358###lif===78###soif===1905###eoif===1921###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->length -= 2;
	}
	else {AKA_mark("lis===-377-###sois===-10271-###eois===-1027166-###lif===-77-###soif===-###eoif===-1900-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}

			if (AKA_mark("lis===379###sois===10365###eois===10391###lif===79###soif===1928###eoif===1954###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===379###sois===10365###eois===10391###lif===79###soif===1928###eoif===1954###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&ws->length > ws->maxlength)) {
		AKA_mark("lis===380###sois===10396###eois===10416###lif===80###soif===1959###eoif===1979###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");goto too_long_error;
	}
	else {AKA_mark("lis===-379-###sois===-10365-###eois===-1036526-###lif===-79-###soif===-###eoif===-1954-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}


		/* compute mask */
			if (AKA_mark("lis===383###sois===10445###eois===10474###lif===83###soif===2008###eoif===2037###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===383###sois===10445###eois===10474###lif===83###soif===2008###eoif===2037###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&FRAME_GET_MASK(ws->header[1]))) {
					AKA_mark("lis===384###sois===10481###eois===10542###lif===84###soif===2044###eoif===2105###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");((unsigned char *)&ws->mask)[0] = ws->header[ws->szhead - 4];

					AKA_mark("lis===385###sois===10546###eois===10607###lif===85###soif===2109###eoif===2170###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");((unsigned char *)&ws->mask)[1] = ws->header[ws->szhead - 3];

					AKA_mark("lis===386###sois===10611###eois===10672###lif===86###soif===2174###eoif===2235###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");((unsigned char *)&ws->mask)[2] = ws->header[ws->szhead - 2];

					AKA_mark("lis===387###sois===10676###eois===10737###lif===87###soif===2239###eoif===2300###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");((unsigned char *)&ws->mask)[3] = ws->header[ws->szhead - 1];

		}
	else {
		AKA_mark("lis===389###sois===10750###eois===10763###lif===89###soif===2313###eoif===2326###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->mask = 0;
	}


		/* all heading fields are known, process */
			AKA_mark("lis===392###sois===10813###eois===10836###lif===92###soif===2376###eoif===2399###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->state = STATE_DATA;

			if (AKA_mark("lis===393###sois===10843###eois===10872###lif===93###soif===2406###eoif===2435###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===393###sois===10843###eois===10872###lif===93###soif===2406###eoif===2435###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&ws->itf->on_extension != NULL)) {
					if (AKA_mark("lis===394###sois===10883###eois===11124###lif===94###soif===2446###eoif===2687###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===394###sois===10883###eois===11124###lif===94###soif===2446###eoif===2687###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&ws->itf->on_extension(ws->closure,
					FRAME_GET_FIN(ws->header[0]),
					FRAME_GET_RSV1(ws->header[0]),
					FRAME_GET_RSV2(ws->header[0]),
					FRAME_GET_RSV3(ws->header[0]),
					FRAME_GET_OPCODE(ws->header[0]),
					(size_t) ws->length))) {
							AKA_mark("lis===401###sois===11132###eois===11141###lif===101###soif===2695###eoif===2704###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");return 0;

			}
		else {AKA_mark("lis===-394-###sois===-10883-###eois===-10883241-###lif===-94-###soif===-###eoif===-2687-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}

		}
	else {AKA_mark("lis===-393-###sois===-10843-###eois===-1084329-###lif===-93-###soif===-###eoif===-2435-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}


		/* not an extension case */
			if (AKA_mark("lis===406###sois===11188###eois===11222###lif===106###soif===2751###eoif===2785###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===406###sois===11188###eois===11222###lif===106###soif===2751###eoif===2785###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&FRAME_GET_RSV1(ws->header[0]) != 0)) {
		AKA_mark("lis===407###sois===11227###eois===11247###lif===107###soif===2790###eoif===2810###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");goto protocol_error;
	}
	else {AKA_mark("lis===-406-###sois===-11188-###eois===-1118834-###lif===-106-###soif===-###eoif===-2785-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}

			if (AKA_mark("lis===408###sois===11254###eois===11288###lif===108###soif===2817###eoif===2851###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===408###sois===11254###eois===11288###lif===108###soif===2817###eoif===2851###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&FRAME_GET_RSV2(ws->header[0]) != 0)) {
		AKA_mark("lis===409###sois===11293###eois===11313###lif===109###soif===2856###eoif===2876###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");goto protocol_error;
	}
	else {AKA_mark("lis===-408-###sois===-11254-###eois===-1125434-###lif===-108-###soif===-###eoif===-2851-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}

			if (AKA_mark("lis===410###sois===11320###eois===11354###lif===110###soif===2883###eoif===2917###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===410###sois===11320###eois===11354###lif===110###soif===2883###eoif===2917###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&FRAME_GET_RSV3(ws->header[0]) != 0)) {
		AKA_mark("lis===411###sois===11359###eois===11379###lif===111###soif===2922###eoif===2942###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");goto protocol_error;
	}
	else {AKA_mark("lis===-410-###sois===-11320-###eois===-1132034-###lif===-110-###soif===-###eoif===-2917-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}


		/* handle */
			AKA_mark("lis===414###sois===11406###eois===11437###lif===114###soif===2969###eoif===3000###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");switch(FRAME_GET_OPCODE(ws->header[0])){
				case OPCODE_CONTINUATION: if(FRAME_GET_OPCODE(ws->header[0]) == OPCODE_CONTINUATION)AKA_mark("lis===415###sois===11443###eois===11468###lif===115###soif===3006###eoif===3031###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					AKA_mark("lis===416###sois===11472###eois===11577###lif===116###soif===3035###eoif===3140###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->itf->on_continue(ws->closure,
					     FRAME_GET_FIN(ws->header[0]),
					     (size_t) ws->length);

					if (AKA_mark("lis===419###sois===11585###eois===11590###lif===119###soif===3148###eoif===3153###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===419###sois===11585###eois===11590###lif===119###soif===3148###eoif===3153###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&!loop)) {
			AKA_mark("lis===420###sois===11596###eois===11605###lif===120###soif===3159###eoif===3168###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");return 0;
		}
		else {AKA_mark("lis===-419-###sois===-11585-###eois===-115855-###lif===-119-###soif===-###eoif===-3153-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}

					AKA_mark("lis===421###sois===11609###eois===11615###lif===121###soif===3172###eoif===3178###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");	AKA_mark("lis===469###sois===12726###eois===12732###lif===169###soif===4289###eoif===4295###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");break;


				case OPCODE_TEXT: if(FRAME_GET_OPCODE(ws->header[0]) == OPCODE_TEXT)AKA_mark("lis===422###sois===11618###eois===11635###lif===122###soif===3181###eoif===3198###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					AKA_mark("lis===423###sois===11639###eois===11732###lif===123###soif===3202###eoif===3295###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->itf->on_text(ws->closure,
					 FRAME_GET_FIN(ws->header[0]),
					 (size_t) ws->length);

					if (AKA_mark("lis===426###sois===11740###eois===11745###lif===126###soif===3303###eoif===3308###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===426###sois===11740###eois===11745###lif===126###soif===3303###eoif===3308###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&!loop)) {
			AKA_mark("lis===427###sois===11751###eois===11760###lif===127###soif===3314###eoif===3323###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");return 0;
		}
		else {AKA_mark("lis===-426-###sois===-11740-###eois===-117405-###lif===-126-###soif===-###eoif===-3308-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}

					AKA_mark("lis===428###sois===11764###eois===11770###lif===128###soif===3327###eoif===3333###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");break;

				case OPCODE_BINARY: if(FRAME_GET_OPCODE(ws->header[0]) == OPCODE_BINARY)AKA_mark("lis===429###sois===11773###eois===11792###lif===129###soif===3336###eoif===3355###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					AKA_mark("lis===430###sois===11796###eois===11895###lif===130###soif===3359###eoif===3458###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->itf->on_binary(ws->closure,
					   FRAME_GET_FIN(ws->header[0]),
					   (size_t) ws->length);

					if (AKA_mark("lis===433###sois===11903###eois===11908###lif===133###soif===3466###eoif===3471###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===433###sois===11903###eois===11908###lif===133###soif===3466###eoif===3471###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&!loop)) {
			AKA_mark("lis===434###sois===11914###eois===11923###lif===134###soif===3477###eoif===3486###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");return 0;
		}
		else {AKA_mark("lis===-433-###sois===-11903-###eois===-119035-###lif===-133-###soif===-###eoif===-3471-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}

					AKA_mark("lis===435###sois===11927###eois===11933###lif===135###soif===3490###eoif===3496###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");break;

				case OPCODE_CLOSE: if(FRAME_GET_OPCODE(ws->header[0]) == OPCODE_CLOSE)AKA_mark("lis===436###sois===11936###eois===11954###lif===136###soif===3499###eoif===3517###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					if (AKA_mark("lis===437###sois===11962###eois===11977###lif===137###soif===3525###eoif===3540###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===437###sois===11962###eois===11977###lif===137###soif===3525###eoif===3540###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&ws->length == 0)) {
			AKA_mark("lis===438###sois===11983###eois===12013###lif===138###soif===3546###eoif===3576###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");code = WEBSOCKET_CODE_NOT_SET;
		}
		else {
							AKA_mark("lis===440###sois===12028###eois===12081###lif===140###soif===3591###eoif===3644###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");code = (uint16_t)(ws->header[ws->szhead - 2] & 0xff);

							AKA_mark("lis===441###sois===12086###eois===12115###lif===141###soif===3649###eoif===3678###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");code = (uint16_t)(code << 8);

							AKA_mark("lis===442###sois===12120###eois===12192###lif===142###soif===3683###eoif===3755###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");code = (uint16_t)(code | (uint16_t)(ws->header[ws->szhead - 1] & 0xff));

			}

					AKA_mark("lis===444###sois===12201###eois===12259###lif===144###soif===3764###eoif===3822###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->itf->on_close(ws->closure, code, (size_t) ws->length);

					AKA_mark("lis===445###sois===12263###eois===12272###lif===145###soif===3826###eoif===3835###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");return 0;

				case OPCODE_PING: if(FRAME_GET_OPCODE(ws->header[0]) == OPCODE_PING)AKA_mark("lis===446###sois===12275###eois===12292###lif===146###soif===3838###eoif===3855###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					if (AKA_mark("lis===447###sois===12300###eois===12316###lif===147###soif===3863###eoif===3879###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===447###sois===12300###eois===12316###lif===147###soif===3863###eoif===3879###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&ws->itf->on_ping)) {
			AKA_mark("lis===448###sois===12322###eois===12364###lif===148###soif===3885###eoif===3927###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->itf->on_ping(ws->closure, ws->length);
		}
		else {
							AKA_mark("lis===450###sois===12379###eois===12396###lif===150###soif===3942###eoif===3959###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");websock_drop(ws);

							AKA_mark("lis===451###sois===12401###eois===12427###lif===151###soif===3964###eoif===3990###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");websock_pong(ws, NULL, 0);

			}

					AKA_mark("lis===453###sois===12436###eois===12459###lif===153###soif===3999###eoif===4022###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->state = STATE_INIT;

					if (AKA_mark("lis===454###sois===12467###eois===12472###lif===154###soif===4030###eoif===4035###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===454###sois===12467###eois===12472###lif===154###soif===4030###eoif===4035###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&!loop)) {
			AKA_mark("lis===455###sois===12478###eois===12487###lif===155###soif===4041###eoif===4050###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");return 0;
		}
		else {AKA_mark("lis===-454-###sois===-12467-###eois===-124675-###lif===-154-###soif===-###eoif===-4035-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}

					AKA_mark("lis===456###sois===12491###eois===12497###lif===156###soif===4054###eoif===4060###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");break;

				case OPCODE_PONG: if(FRAME_GET_OPCODE(ws->header[0]) == OPCODE_PONG)AKA_mark("lis===457###sois===12500###eois===12517###lif===157###soif===4063###eoif===4080###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					if (AKA_mark("lis===458###sois===12525###eois===12541###lif===158###soif===4088###eoif===4104###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===458###sois===12525###eois===12541###lif===158###soif===4088###eoif===4104###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&ws->itf->on_pong)) {
			AKA_mark("lis===459###sois===12547###eois===12589###lif===159###soif===4110###eoif===4152###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->itf->on_pong(ws->closure, ws->length);
		}
		else {
			AKA_mark("lis===461###sois===12602###eois===12619###lif===161###soif===4165###eoif===4182###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");websock_drop(ws);
		}

					AKA_mark("lis===462###sois===12623###eois===12646###lif===162###soif===4186###eoif===4209###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->state = STATE_INIT;

					if (AKA_mark("lis===463###sois===12654###eois===12659###lif===163###soif===4217###eoif===4222###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===463###sois===12654###eois===12659###lif===163###soif===4217###eoif===4222###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&!loop)) {
			AKA_mark("lis===464###sois===12665###eois===12674###lif===164###soif===4228###eoif===4237###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");return 0;
		}
		else {AKA_mark("lis===-463-###sois===-12654-###eois===-126545-###lif===-163-###soif===-###eoif===-4222-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}

					AKA_mark("lis===465###sois===12678###eois===12684###lif===165###soif===4241###eoif===4247###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");break;

				default: if(FRAME_GET_OPCODE(ws->header[0]) != OPCODE_CONTINUATION && FRAME_GET_OPCODE(ws->header[0]) != OPCODE_TEXT && FRAME_GET_OPCODE(ws->header[0]) != OPCODE_BINARY && FRAME_GET_OPCODE(ws->header[0]) != OPCODE_CLOSE && FRAME_GET_OPCODE(ws->header[0]) != OPCODE_PING && FRAME_GET_OPCODE(ws->header[0]) != OPCODE_PONG)AKA_mark("lis===466###sois===12687###eois===12695###lif===166###soif===4250###eoif===4258###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

					AKA_mark("lis===467###sois===12699###eois===12719###lif===167###soif===4262###eoif===4282###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");goto protocol_error;

		}

		break;

		case STATE_DATA: if(ws->state == STATE_DATA)AKA_mark("lis===471###sois===12735###eois===12751###lif===171###soif===4298###eoif===4314###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");

			if (AKA_mark("lis===472###sois===12758###eois===12768###lif===172###soif===4321###eoif===4331###ifc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)") && (AKA_mark("lis===472###sois===12758###eois===12768###lif===172###soif===4321###eoif===4331###isc===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)")&&ws->length)) {
		AKA_mark("lis===473###sois===12773###eois===12782###lif===173###soif===4336###eoif===4345###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");return 0;
	}
	else {AKA_mark("lis===-472-###sois===-12758-###eois===-1275810-###lif===-172-###soif===-###eoif===-4331-###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");}

			AKA_mark("lis===474###sois===12785###eois===12808###lif===174###soif===4348###eoif===4371###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");ws->state = STATE_INIT;

			AKA_mark("lis===475###sois===12811###eois===12817###lif===175###soif===4374###eoif===4380###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");break;

	}

		AKA_mark("lis===477###sois===12822###eois===12832###lif===177###soif===4385###eoif===4395###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");goto loop;


 	too_long_error:
	AKA_mark("lis===480###sois===12852###eois===12913###lif===180###soif===4415###eoif===4476###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");websock_error(ws, WEBSOCKET_CODE_MESSAGE_TOO_LARGE, NULL, 0);

		AKA_mark("lis===481###sois===12915###eois===12924###lif===181###soif===4478###eoif===4487###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");return 0;


 	protocol_error:
	AKA_mark("lis===484###sois===12944###eois===13002###lif===184###soif===4507###eoif===4565###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");websock_error(ws, WEBSOCKET_CODE_PROTOCOL_ERROR, NULL, 0);

		AKA_mark("lis===485###sois===13004###eois===13013###lif===185###soif===4567###eoif===4576###ins===true###function===./app-framework-binder/src/websock.c/websock_dispatch(struct websock*,int)");return 0;

}

/** Instrumented function unmask(struct websock*,void*,size_t) */
static void unmask(struct websock * ws, void *buffer, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");AKA_fCall++;
		AKA_mark("lis===490###sois===13087###eois===13107###lif===2###soif===70###eoif===90###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");uint32_t mask, *b32;

		AKA_mark("lis===491###sois===13109###eois===13124###lif===3###soif===92###eoif===107###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");uint8_t m, *b8;


		AKA_mark("lis===493###sois===13127###eois===13143###lif===5###soif===110###eoif===126###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");mask = ws->mask;

		AKA_mark("lis===494###sois===13145###eois===13157###lif===6###soif===128###eoif===140###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");b8 = buffer;

		while (AKA_mark("lis===495###sois===13166###eois===13215###lif===7###soif===149###eoif===198###ifc===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)") && ((AKA_mark("lis===495###sois===13166###eois===13170###lif===7###soif===149###eoif===153###isc===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)")&&size)	&&(AKA_mark("lis===495###sois===13175###eois===13214###lif===7###soif===158###eoif===197###isc===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)")&&(sizeof(uint32_t) - 1) & (uintptr_t) b8))) {
				AKA_mark("lis===496###sois===13221###eois===13249###lif===8###soif===204###eoif===232###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");m = ((uint8_t *) & mask)[0];

				AKA_mark("lis===497###sois===13252###eois===13302###lif===9###soif===235###eoif===285###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");((uint8_t *) & mask)[0] = ((uint8_t *) & mask)[1];

				AKA_mark("lis===498###sois===13305###eois===13355###lif===10###soif===288###eoif===338###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");((uint8_t *) & mask)[1] = ((uint8_t *) & mask)[2];

				AKA_mark("lis===499###sois===13358###eois===13408###lif===11###soif===341###eoif===391###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");((uint8_t *) & mask)[2] = ((uint8_t *) & mask)[3];

				AKA_mark("lis===500###sois===13411###eois===13439###lif===12###soif===394###eoif===422###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");((uint8_t *) & mask)[3] = m;

				AKA_mark("lis===501###sois===13442###eois===13453###lif===13###soif===425###eoif===436###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");*b8++ ^= m;

				AKA_mark("lis===502###sois===13456###eois===13463###lif===14###soif===439###eoif===446###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");size--;

	}

		AKA_mark("lis===504###sois===13468###eois===13490###lif===16###soif===451###eoif===473###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");b32 = (uint32_t *) b8;

		while (AKA_mark("lis===505###sois===13499###eois===13523###lif===17###soif===482###eoif===506###ifc===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)") && (AKA_mark("lis===505###sois===13499###eois===13523###lif===17###soif===482###eoif===506###isc===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)")&&size >= sizeof(uint32_t))) {
				AKA_mark("lis===506###sois===13529###eois===13544###lif===18###soif===512###eoif===527###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");*b32++ ^= mask;

				AKA_mark("lis===507###sois===13547###eois===13572###lif===19###soif===530###eoif===555###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");size -= sizeof(uint32_t);

	}

		AKA_mark("lis===509###sois===13577###eois===13598###lif===21###soif===560###eoif===581###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");b8 = (uint8_t *) b32;

		while (AKA_mark("lis===510###sois===13607###eois===13611###lif===22###soif===590###eoif===594###ifc===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)") && (AKA_mark("lis===510###sois===13607###eois===13611###lif===22###soif===590###eoif===594###isc===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)")&&size)) {
				AKA_mark("lis===511###sois===13617###eois===13645###lif===23###soif===600###eoif===628###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");m = ((uint8_t *) & mask)[0];

				AKA_mark("lis===512###sois===13648###eois===13698###lif===24###soif===631###eoif===681###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");((uint8_t *) & mask)[0] = ((uint8_t *) & mask)[1];

				AKA_mark("lis===513###sois===13701###eois===13751###lif===25###soif===684###eoif===734###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");((uint8_t *) & mask)[1] = ((uint8_t *) & mask)[2];

				AKA_mark("lis===514###sois===13754###eois===13804###lif===26###soif===737###eoif===787###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");((uint8_t *) & mask)[2] = ((uint8_t *) & mask)[3];

				AKA_mark("lis===515###sois===13807###eois===13835###lif===27###soif===790###eoif===818###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");((uint8_t *) & mask)[3] = m;

				AKA_mark("lis===516###sois===13838###eois===13849###lif===28###soif===821###eoif===832###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");*b8++ ^= m;

				AKA_mark("lis===517###sois===13852###eois===13859###lif===29###soif===835###eoif===842###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");size--;

	}

		AKA_mark("lis===519###sois===13864###eois===13880###lif===31###soif===847###eoif===863###ins===true###function===./app-framework-binder/src/websock.c/unmask(struct websock*,void*,size_t)");ws->mask = mask;

}

/** Instrumented function websock_read(struct websock*,void*,size_t) */
ssize_t websock_read(struct websock * ws, void *buffer, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)");AKA_fCall++;
		AKA_mark("lis===524###sois===13956###eois===13967###lif===2###soif===72###eoif===83###ins===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)");ssize_t rc;


		if (AKA_mark("lis===526###sois===13974###eois===13997###lif===4###soif===90###eoif===113###ifc===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)") && (AKA_mark("lis===526###sois===13974###eois===13997###lif===4###soif===90###eoif===113###isc===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)")&&ws->state != STATE_DATA)) {
		AKA_mark("lis===527###sois===14001###eois===14010###lif===5###soif===117###eoif===126###ins===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)");return 0;
	}
	else {AKA_mark("lis===-526-###sois===-13974-###eois===-1397423-###lif===-4-###soif===-###eoif===-113-###ins===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)");}


		if (AKA_mark("lis===529###sois===14017###eois===14034###lif===7###soif===133###eoif===150###ifc===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)") && (AKA_mark("lis===529###sois===14017###eois===14034###lif===7###soif===133###eoif===150###isc===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)")&&size > ws->length)) {
		AKA_mark("lis===530###sois===14038###eois===14065###lif===8###soif===154###eoif===181###ins===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)");size = (size_t) ws->length;
	}
	else {AKA_mark("lis===-529-###sois===-14017-###eois===-1401717-###lif===-7-###soif===-###eoif===-150-###ins===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)");}


		AKA_mark("lis===532###sois===14068###eois===14099###lif===10###soif===184###eoif===215###ins===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)");rc = ws_read(ws, buffer, size);

		if (AKA_mark("lis===533###sois===14105###eois===14111###lif===11###soif===221###eoif===227###ifc===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)") && (AKA_mark("lis===533###sois===14105###eois===14111###lif===11###soif===221###eoif===227###isc===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)")&&rc > 0)) {
				AKA_mark("lis===534###sois===14117###eois===14136###lif===12###soif===233###eoif===252###ins===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)");size = (size_t) rc;

				AKA_mark("lis===535###sois===14139###eois===14158###lif===13###soif===255###eoif===274###ins===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)");ws->length -= size;


				if (AKA_mark("lis===537###sois===14166###eois===14179###lif===15###soif===282###eoif===295###ifc===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)") && (AKA_mark("lis===537###sois===14166###eois===14179###lif===15###soif===282###eoif===295###isc===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)")&&ws->mask != 0)) {
			AKA_mark("lis===538###sois===14184###eois===14209###lif===16###soif===300###eoif===325###ins===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)");unmask(ws, buffer, size);
		}
		else {AKA_mark("lis===-537-###sois===-14166-###eois===-1416613-###lif===-15-###soif===-###eoif===-295-###ins===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)");}

	}
	else {AKA_mark("lis===-533-###sois===-14105-###eois===-141056-###lif===-11-###soif===-###eoif===-227-###ins===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)");}

		AKA_mark("lis===540###sois===14214###eois===14224###lif===18###soif===330###eoif===340###ins===true###function===./app-framework-binder/src/websock.c/websock_read(struct websock*,void*,size_t)");return rc;

}

/** Instrumented function websock_drop(struct websock*) */
int websock_drop(struct websock *ws)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_drop(struct websock*)");AKA_fCall++;
		AKA_mark("lis===545###sois===14268###eois===14286###lif===2###soif===40###eoif===58###ins===true###function===./app-framework-binder/src/websock.c/websock_drop(struct websock*)");char buffer[8000];


		while (AKA_mark("lis===547###sois===14296###eois===14306###lif===4###soif===68###eoif===78###ifc===true###function===./app-framework-binder/src/websock.c/websock_drop(struct websock*)") && (AKA_mark("lis===547###sois===14296###eois===14306###lif===4###soif===68###eoif===78###isc===true###function===./app-framework-binder/src/websock.c/websock_drop(struct websock*)")&&ws->length)) {
		if (AKA_mark("lis===548###sois===14314###eois===14357###lif===5###soif===86###eoif===129###ifc===true###function===./app-framework-binder/src/websock.c/websock_drop(struct websock*)") && (AKA_mark("lis===548###sois===14314###eois===14357###lif===5###soif===86###eoif===129###isc===true###function===./app-framework-binder/src/websock.c/websock_drop(struct websock*)")&&websock_read(ws, buffer, sizeof buffer) < 0)) {
			AKA_mark("lis===549###sois===14362###eois===14372###lif===6###soif===134###eoif===144###ins===true###function===./app-framework-binder/src/websock.c/websock_drop(struct websock*)");return -1;
		}
		else {AKA_mark("lis===-548-###sois===-14314-###eois===-1431443-###lif===-5-###soif===-###eoif===-129-###ins===true###function===./app-framework-binder/src/websock.c/websock_drop(struct websock*)");}
	}

		AKA_mark("lis===550###sois===14374###eois===14383###lif===7###soif===146###eoif===155###ins===true###function===./app-framework-binder/src/websock.c/websock_drop(struct websock*)");return 0;

}

/** Instrumented function websock_create_v13(const struct websock_itf*,void*) */
struct websock *websock_create_v13(const struct websock_itf *itf, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_create_v13(const struct websock_itf*,void*)");AKA_fCall++;
		AKA_mark("lis===555###sois===14471###eois===14522###lif===2###soif===84###eoif===135###ins===true###function===./app-framework-binder/src/websock.c/websock_create_v13(const struct websock_itf*,void*)");struct websock *result = calloc(1, sizeof *result);

		if (AKA_mark("lis===556###sois===14528###eois===14534###lif===3###soif===141###eoif===147###ifc===true###function===./app-framework-binder/src/websock.c/websock_create_v13(const struct websock_itf*,void*)") && (AKA_mark("lis===556###sois===14528###eois===14534###lif===3###soif===141###eoif===147###isc===true###function===./app-framework-binder/src/websock.c/websock_create_v13(const struct websock_itf*,void*)")&&result)) {
				AKA_mark("lis===557###sois===14540###eois===14558###lif===4###soif===153###eoif===171###ins===true###function===./app-framework-binder/src/websock.c/websock_create_v13(const struct websock_itf*,void*)");result->itf = itf;

				AKA_mark("lis===558###sois===14561###eois===14587###lif===5###soif===174###eoif===200###ins===true###function===./app-framework-binder/src/websock.c/websock_create_v13(const struct websock_itf*,void*)");result->closure = closure;

				AKA_mark("lis===559###sois===14590###eois===14628###lif===6###soif===203###eoif===241###ins===true###function===./app-framework-binder/src/websock.c/websock_create_v13(const struct websock_itf*,void*)");result->maxlength = default_maxlength;

	}
	else {AKA_mark("lis===-556-###sois===-14528-###eois===-145286-###lif===-3-###soif===-###eoif===-147-###ins===true###function===./app-framework-binder/src/websock.c/websock_create_v13(const struct websock_itf*,void*)");}

		AKA_mark("lis===561###sois===14633###eois===14647###lif===8###soif===246###eoif===260###ins===true###function===./app-framework-binder/src/websock.c/websock_create_v13(const struct websock_itf*,void*)");return result;

}

/** Instrumented function websock_destroy(struct websock*) */
void websock_destroy(struct websock *ws)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_destroy(struct websock*)");AKA_fCall++;
		AKA_mark("lis===566###sois===14695###eois===14704###lif===2###soif===44###eoif===53###ins===true###function===./app-framework-binder/src/websock.c/websock_destroy(struct websock*)");free(ws);

}

/** Instrumented function websock_set_default_max_length(size_t) */
void websock_set_default_max_length(size_t maxlen)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_set_default_max_length(size_t)");AKA_fCall++;
		AKA_mark("lis===571###sois===14762###eois===14789###lif===2###soif===54###eoif===81###ins===true###function===./app-framework-binder/src/websock.c/websock_set_default_max_length(size_t)");default_maxlength = maxlen;

}

/** Instrumented function websock_set_max_length(struct websock*,size_t) */
void websock_set_max_length(struct websock *ws, size_t maxlen)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websock_set_max_length(struct websock*,size_t)");AKA_fCall++;
		AKA_mark("lis===576###sois===14859###eois===14892###lif===2###soif===66###eoif===99###ins===true###function===./app-framework-binder/src/websock.c/websock_set_max_length(struct websock*,size_t)");ws->maxlength = (uint64_t)maxlen;

}

/** Instrumented function websocket_explain_error(uint16_t) */
const char *websocket_explain_error(uint16_t code)
{AKA_mark("Calling: ./app-framework-binder/src/websock.c/websocket_explain_error(uint16_t)");AKA_fCall++;
		AKA_mark("lis===581###sois===14950###eois===15391###lif===2###soif===54###eoif===495###ins===true###function===./app-framework-binder/src/websock.c/websocket_explain_error(uint16_t)");static const char *msgs[] = {
		"OK",                /* 1000 */
		"GOING_AWAY",        /* 1001 */
		"PROTOCOL_ERROR",    /* 1002 */
		"CANT_ACCEPT",       /* 1003 */
		"RESERVED",          /* 1004 */
		"NOT_SET",           /* 1005 */
		"ABNORMAL",          /* 1006 */
		"INVALID_UTF8",      /* 1007 */
		"POLICY_VIOLATION",  /* 1008 */
		"MESSAGE_TOO_LARGE", /* 1009 */
		"EXPECT_EXTENSION",  /* 1010 */
		"INTERNAL_ERROR",    /* 1011 */
	};

		if (AKA_mark("lis===595###sois===15397###eois===15457###lif===16###soif===501###eoif===561###ifc===true###function===./app-framework-binder/src/websock.c/websocket_explain_error(uint16_t)") && ((AKA_mark("lis===595###sois===15397###eois===15408###lif===16###soif===501###eoif===512###isc===true###function===./app-framework-binder/src/websock.c/websocket_explain_error(uint16_t)")&&code < 1000)	||(AKA_mark("lis===595###sois===15412###eois===15457###lif===16###soif===516###eoif===561###isc===true###function===./app-framework-binder/src/websock.c/websocket_explain_error(uint16_t)")&&(code - 1000) >= (sizeof msgs / sizeof *msgs)))) {
		AKA_mark("lis===596###sois===15461###eois===15472###lif===17###soif===565###eoif===576###ins===true###function===./app-framework-binder/src/websock.c/websocket_explain_error(uint16_t)");return "?";
	}
	else {AKA_mark("lis===-595-###sois===-15397-###eois===-1539760-###lif===-16-###soif===-###eoif===-561-###ins===true###function===./app-framework-binder/src/websock.c/websocket_explain_error(uint16_t)");}

		AKA_mark("lis===597###sois===15474###eois===15499###lif===18###soif===578###eoif===603###ins===true###function===./app-framework-binder/src/websock.c/websocket_explain_error(uint16_t)");return msgs[code - 1000];

}

#endif

