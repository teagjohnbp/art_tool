/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_WRAP_JSON_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_WRAP_JSON_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 Copyright (C) 2015-2020 "IoT.bzh"

 author: José Bollo <jose.bollo@iot.bzh>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

#include <string.h>
#include <limits.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__WRAP_JSON_H_
#define AKA_INCLUDE__WRAP_JSON_H_
#include "wrap-json.akaignore.h"
#endif


#define STACKCOUNT  32
#define STRCOUNT    8

enum {
	wrap_json_error_none,
	wrap_json_error_null_object,
	wrap_json_error_truncated,
	wrap_json_error_internal_error,
	wrap_json_error_out_of_memory,
	wrap_json_error_invalid_character,
	wrap_json_error_too_long,
	wrap_json_error_too_deep,
	wrap_json_error_null_spec,
	wrap_json_error_null_key,
	wrap_json_error_null_string,
	wrap_json_error_out_of_range,
	wrap_json_error_incomplete,
	wrap_json_error_missfit_type,
	wrap_json_error_key_not_found,
	wrap_json_error_bad_base64,
	_wrap_json_error_count_
};

static const char ignore_all[] = " \t\n\r,:";
static const char pack_accept_arr[] = "][{snbiIfoOyY";
static const char pack_accept_key[] = "s}";
#define pack_accept_any (&pack_accept_arr[1])

static const char unpack_accept_arr[] = "*!][{snbiIfFoOyY";
static const char unpack_accept_key[] = "*!s}";
#define unpack_accept_any (&unpack_accept_arr[3])

static const char *pack_errors[_wrap_json_error_count_] =
{
	[wrap_json_error_none] = "unknown error",
	[wrap_json_error_null_object] = "null object",
	[wrap_json_error_truncated] = "truncated",
	[wrap_json_error_internal_error] = "internal error",
	[wrap_json_error_out_of_memory] = "out of memory",
	[wrap_json_error_invalid_character] = "invalid character",
	[wrap_json_error_too_long] = "too long",
	[wrap_json_error_too_deep] = "too deep",
	[wrap_json_error_null_spec] = "spec is NULL",
	[wrap_json_error_null_key] = "key is NULL",
	[wrap_json_error_null_string] = "string is NULL",
	[wrap_json_error_out_of_range] = "array too small",
	[wrap_json_error_incomplete] = "incomplete container",
	[wrap_json_error_missfit_type] = "missfit of type",
	[wrap_json_error_key_not_found] = "key not found",
	[wrap_json_error_bad_base64] = "bad base64 encoding"
};

/** Instrumented function wrap_json_get_error_position(int) */
int wrap_json_get_error_position(int rc)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_get_error_position(int)");AKA_fCall++;
		if (AKA_mark("lis===78###sois===2497###eois===2503###lif===2###soif===48###eoif===54###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_position(int)") && (AKA_mark("lis===78###sois===2497###eois===2503###lif===2###soif===48###eoif===54###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_position(int)")&&rc < 0)) {
		AKA_mark("lis===79###sois===2507###eois===2516###lif===3###soif===58###eoif===67###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_position(int)");rc = -rc;
	}
	else {AKA_mark("lis===-78-###sois===-2497-###eois===-24976-###lif===-2-###soif===-###eoif===-54-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_position(int)");}

		AKA_mark("lis===80###sois===2518###eois===2539###lif===4###soif===69###eoif===90###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_position(int)");return (rc >> 4) + 1;

}

/** Instrumented function wrap_json_get_error_code(int) */
int wrap_json_get_error_code(int rc)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_get_error_code(int)");AKA_fCall++;
		if (AKA_mark("lis===85###sois===2587###eois===2593###lif===2###soif===44###eoif===50###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_code(int)") && (AKA_mark("lis===85###sois===2587###eois===2593###lif===2###soif===44###eoif===50###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_code(int)")&&rc < 0)) {
		AKA_mark("lis===86###sois===2597###eois===2606###lif===3###soif===54###eoif===63###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_code(int)");rc = -rc;
	}
	else {AKA_mark("lis===-85-###sois===-2587-###eois===-25876-###lif===-2-###soif===-###eoif===-50-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_code(int)");}

		AKA_mark("lis===87###sois===2608###eois===2623###lif===4###soif===65###eoif===80###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_code(int)");return rc & 15;

}

/** Instrumented function wrap_json_get_error_string(int) */
const char *wrap_json_get_error_string(int rc)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_get_error_string(int)");AKA_fCall++;
		AKA_mark("lis===92###sois===2677###eois===2711###lif===2###soif===50###eoif===84###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_string(int)");rc = wrap_json_get_error_code(rc);

		if (AKA_mark("lis===93###sois===2717###eois===2770###lif===3###soif===90###eoif===143###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_string(int)") && (AKA_mark("lis===93###sois===2717###eois===2770###lif===3###soif===90###eoif===143###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_string(int)")&&rc >= (int)(sizeof pack_errors / sizeof *pack_errors))) {
		AKA_mark("lis===94###sois===2774###eois===2781###lif===4###soif===147###eoif===154###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_string(int)");rc = 0;
	}
	else {AKA_mark("lis===-93-###sois===-2717-###eois===-271753-###lif===-3-###soif===-###eoif===-143-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_string(int)");}

		AKA_mark("lis===95###sois===2783###eois===2806###lif===5###soif===156###eoif===179###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_get_error_string(int)");return pack_errors[rc];

}

/** Instrumented function encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int) */
static int encode_base64(
		const uint8_t *data,
		size_t datalen,
		char **encoded,
		size_t *encodedlen,
		int width,
		int pad,
		int url)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");AKA_fCall++;
		AKA_mark("lis===107###sois===2955###eois===2972###lif===9###soif===145###eoif===162###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");uint16_t u16 = 0;

		AKA_mark("lis===108###sois===2974###eois===2989###lif===10###soif===164###eoif===179###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");uint8_t u8 = 0;

		AKA_mark("lis===109###sois===2991###eois===3032###lif===11###soif===181###eoif===222###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");size_t in, out, rlen, n3, r3, iout, nout;

		AKA_mark("lis===110###sois===3034###eois===3041###lif===12###soif===224###eoif===231###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");int iw;

		AKA_mark("lis===111###sois===3043###eois===3059###lif===13###soif===233###eoif===249###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");char *result, c;


	/* compute unformatted output length */
		AKA_mark("lis===114###sois===3103###eois===3120###lif===16###soif===293###eoif===310###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");n3 = datalen / 3;

		AKA_mark("lis===115###sois===3122###eois===3139###lif===17###soif===312###eoif===329###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");r3 = datalen % 3;

		AKA_mark("lis===116###sois===3141###eois===3167###lif===18###soif===331###eoif===357###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");nout = 4 * n3 + r3 + !!r3;


	/* deduce formatted output length */
		AKA_mark("lis===119###sois===3208###eois===3220###lif===21###soif===398###eoif===410###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");rlen = nout;

		if (AKA_mark("lis===120###sois===3226###eois===3229###lif===22###soif===416###eoif===419###ifc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)") && (AKA_mark("lis===120###sois===3226###eois===3229###lif===22###soif===416###eoif===419###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&pad)) {
		AKA_mark("lis===121###sois===3233###eois===3259###lif===23###soif===423###eoif===449###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");rlen += ((~rlen) + 1) & 3;
	}
	else {AKA_mark("lis===-120-###sois===-3226-###eois===-32263-###lif===-22-###soif===-###eoif===-419-###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");}

		if (AKA_mark("lis===122###sois===3265###eois===3270###lif===24###soif===455###eoif===460###ifc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)") && (AKA_mark("lis===122###sois===3265###eois===3270###lif===24###soif===455###eoif===460###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&width)) {
		AKA_mark("lis===123###sois===3274###eois===3295###lif===25###soif===464###eoif===485###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");rlen += rlen / width;
	}
	else {AKA_mark("lis===-122-###sois===-3265-###eois===-32655-###lif===-24-###soif===-###eoif===-460-###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");}


	/* allocate the output */
		AKA_mark("lis===126###sois===3325###eois===3351###lif===28###soif===515###eoif===541###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");result = malloc(rlen + 1);

		if (AKA_mark("lis===127###sois===3357###eois===3371###lif===29###soif===547###eoif===561###ifc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)") && (AKA_mark("lis===127###sois===3357###eois===3371###lif===29###soif===547###eoif===561###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&result == NULL)) {
		AKA_mark("lis===128###sois===3375###eois===3412###lif===30###soif===565###eoif===602###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");return wrap_json_error_out_of_memory;
	}
	else {AKA_mark("lis===-127-###sois===-3357-###eois===-335714-###lif===-29-###soif===-###eoif===-561-###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");}


	/* compute the formatted output */
		AKA_mark("lis===131###sois===3451###eois===3462###lif===33###soif===641###eoif===652###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");iw = width;

		AKA_mark("lis===132###sois===3469###eois===3490###lif===34###soif===659###eoif===680###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");for (in = out = iout = 0 ;AKA_mark("lis===132###sois===3491###eois===3502###lif===34###soif===681###eoif===692###ifc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)") && AKA_mark("lis===132###sois===3491###eois===3502###lif===34###soif===681###eoif===692###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&iout < nout;({AKA_mark("lis===132###sois===3505###eois===3511###lif===34###soif===695###eoif===701###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");iout++;})) {
		/* get in 'u8' the 6 bits value to add */
				AKA_mark("lis===134###sois===3569###eois===3577###lif===36###soif===759###eoif===767###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");switch(iout & 3){
					case 0: if(iout & 3 == 0)AKA_mark("lis===135###sois===3583###eois===3590###lif===37###soif===773###eoif===780###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");

						AKA_mark("lis===136###sois===3594###eois===3621###lif===38###soif===784###eoif===811###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");u16 = (uint16_t)data[in++];

						AKA_mark("lis===137###sois===3625###eois===3650###lif===39###soif===815###eoif===840###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");u8 = (uint8_t)(u16 >> 2);

						AKA_mark("lis===138###sois===3654###eois===3660###lif===40###soif===844###eoif===850###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");break;

					case 1: if(iout & 3 == 1)AKA_mark("lis===139###sois===3663###eois===3670###lif===41###soif===853###eoif===860###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");

						AKA_mark("lis===140###sois===3674###eois===3701###lif===42###soif===864###eoif===891###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");u16 = (uint16_t)(u16 << 8);

						if (AKA_mark("lis===141###sois===3709###eois===3721###lif===43###soif===899###eoif===911###ifc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)") && (AKA_mark("lis===141###sois===3709###eois===3721###lif===43###soif===899###eoif===911###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&in < datalen)) {
				AKA_mark("lis===142###sois===3727###eois===3762###lif===44###soif===917###eoif===952###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");u16 = (uint16_t)(u16 | data[in++]);
			}
			else {AKA_mark("lis===-141-###sois===-3709-###eois===-370912-###lif===-43-###soif===-###eoif===-911-###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");}

						AKA_mark("lis===143###sois===3766###eois===3791###lif===45###soif===956###eoif===981###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");u8 = (uint8_t)(u16 >> 4);

						AKA_mark("lis===144###sois===3795###eois===3801###lif===46###soif===985###eoif===991###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");break;

					case 2: if(iout & 3 == 2)AKA_mark("lis===145###sois===3804###eois===3811###lif===47###soif===994###eoif===1001###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");

						AKA_mark("lis===146###sois===3815###eois===3842###lif===48###soif===1005###eoif===1032###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");u16 = (uint16_t)(u16 << 8);

						if (AKA_mark("lis===147###sois===3850###eois===3862###lif===49###soif===1040###eoif===1052###ifc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)") && (AKA_mark("lis===147###sois===3850###eois===3862###lif===49###soif===1040###eoif===1052###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&in < datalen)) {
				AKA_mark("lis===148###sois===3868###eois===3903###lif===50###soif===1058###eoif===1093###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");u16 = (uint16_t)(u16 | data[in++]);
			}
			else {AKA_mark("lis===-147-###sois===-3850-###eois===-385012-###lif===-49-###soif===-###eoif===-1052-###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");}

						AKA_mark("lis===149###sois===3907###eois===3932###lif===51###soif===1097###eoif===1122###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");u8 = (uint8_t)(u16 >> 6);

						AKA_mark("lis===150###sois===3936###eois===3942###lif===52###soif===1126###eoif===1132###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");break;

					case 3: if(iout & 3 == 3)AKA_mark("lis===151###sois===3945###eois===3952###lif===53###soif===1135###eoif===1142###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");

						AKA_mark("lis===152###sois===3956###eois===3974###lif===54###soif===1146###eoif===1164###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");u8 = (uint8_t)u16;

						AKA_mark("lis===153###sois===3978###eois===3984###lif===55###soif===1168###eoif===1174###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");break;

		}

				AKA_mark("lis===155###sois===3991###eois===4000###lif===57###soif===1181###eoif===1190###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");u8 &= 63;


		/* encode 'u8' to the char 'c' */
				if (AKA_mark("lis===158###sois===4044###eois===4051###lif===60###soif===1234###eoif===1241###ifc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)") && (AKA_mark("lis===158###sois===4044###eois===4051###lif===60###soif===1234###eoif===1241###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&u8 < 52)) {
						if (AKA_mark("lis===159###sois===4062###eois===4069###lif===61###soif===1252###eoif===1259###ifc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)") && (AKA_mark("lis===159###sois===4062###eois===4069###lif===61###soif===1252###eoif===1259###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&u8 < 26)) {
				AKA_mark("lis===160###sois===4075###eois===4096###lif===62###soif===1265###eoif===1286###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");c = (char)('A' + u8);
			}
			else {
				AKA_mark("lis===162###sois===4109###eois===4135###lif===64###soif===1299###eoif===1325###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");c = (char)('a' + u8 - 26);
			}

		}
		else {
						if (AKA_mark("lis===164###sois===4154###eois===4161###lif===66###soif===1344###eoif===1351###ifc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)") && (AKA_mark("lis===164###sois===4154###eois===4161###lif===66###soif===1344###eoif===1351###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&u8 < 62)) {
				AKA_mark("lis===165###sois===4167###eois===4193###lif===67###soif===1357###eoif===1383###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");c = (char)('0' + u8 - 52);
			}
			else {
				if (AKA_mark("lis===166###sois===4206###eois===4214###lif===68###soif===1396###eoif===1404###ifc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)") && (AKA_mark("lis===166###sois===4206###eois===4214###lif===68###soif===1396###eoif===1404###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&u8 == 62)) {
					AKA_mark("lis===167###sois===4220###eois===4240###lif===69###soif===1410###eoif===1430###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");c = url ? '-' : '+';
				}
				else {
					AKA_mark("lis===169###sois===4253###eois===4273###lif===71###soif===1443###eoif===1463###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");c = url ? '_' : '/';
				}
			}

		}


		/* put to output with format */
				AKA_mark("lis===173###sois===4315###eois===4333###lif===75###soif===1505###eoif===1523###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");result[out++] = c;

				if (AKA_mark("lis===174###sois===4340###eois===4351###lif===76###soif===1530###eoif===1541###ifc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)") && ((AKA_mark("lis===174###sois===4340###eois===4342###lif===76###soif===1530###eoif===1532###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&iw)	&&(AKA_mark("lis===174###sois===4346###eois===4351###lif===76###soif===1536###eoif===1541###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&!--iw))) {
						AKA_mark("lis===175###sois===4358###eois===4379###lif===77###soif===1548###eoif===1569###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");result[out++] = '\n';

						AKA_mark("lis===176###sois===4383###eois===4394###lif===78###soif===1573###eoif===1584###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");iw = width;

		}
		else {AKA_mark("lis===-174-###sois===-4340-###eois===-434011-###lif===-76-###soif===-###eoif===-1541-###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");}

	}


	/* pad the output */
		while (AKA_mark("lis===181###sois===4433###eois===4443###lif===83###soif===1623###eoif===1633###ifc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)") && (AKA_mark("lis===181###sois===4433###eois===4443###lif===83###soif===1623###eoif===1633###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&out < rlen)) {
				AKA_mark("lis===182###sois===4449###eois===4469###lif===84###soif===1639###eoif===1659###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");result[out++] = '=';

				if (AKA_mark("lis===183###sois===4476###eois===4487###lif===85###soif===1666###eoif===1677###ifc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)") && ((AKA_mark("lis===183###sois===4476###eois===4478###lif===85###soif===1666###eoif===1668###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&iw)	&&(AKA_mark("lis===183###sois===4482###eois===4487###lif===85###soif===1672###eoif===1677###isc===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)")&&!--iw))) {
						AKA_mark("lis===184###sois===4494###eois===4515###lif===86###soif===1684###eoif===1705###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");result[out++] = '\n';

						AKA_mark("lis===185###sois===4519###eois===4530###lif===87###soif===1709###eoif===1720###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");iw = width;

		}
		else {AKA_mark("lis===-183-###sois===-4476-###eois===-447611-###lif===-85-###soif===-###eoif===-1677-###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");}

	}


	/* terminate */
		AKA_mark("lis===190###sois===4557###eois===4573###lif===92###soif===1747###eoif===1763###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");result[out] = 0;

		AKA_mark("lis===191###sois===4575###eois===4593###lif===93###soif===1765###eoif===1783###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");*encoded = result;

		AKA_mark("lis===192###sois===4595###eois===4614###lif===94###soif===1785###eoif===1804###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");*encodedlen = rlen;

		AKA_mark("lis===193###sois===4616###eois===4625###lif===95###soif===1806###eoif===1815###ins===true###function===./app-framework-binder/src/wrap-json.c/encode_base64(uint8_t*,size_t,char**,size_t*,int,int,int)");return 0;

}

/** Instrumented function decode_base64(const char*,size_t,uint8_t**,size_t*,int) */
static int decode_base64(
		const char *data,
		size_t datalen,
		uint8_t **decoded,
		size_t *decodedlen,
		int url)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");AKA_fCall++;
		AKA_mark("lis===203###sois===4750###eois===4767###lif===7###soif===121###eoif===138###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");uint16_t u16 = 0;

		AKA_mark("lis===204###sois===4769###eois===4789###lif===8###soif===140###eoif===160###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");uint8_t u8, *result;

		AKA_mark("lis===205###sois===4791###eois===4811###lif===9###soif===162###eoif===182###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");size_t in, out, iin;

		AKA_mark("lis===206###sois===4813###eois===4820###lif===10###soif===184###eoif===191###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");char c;


	/* allocate enougth output */
		AKA_mark("lis===209###sois===4854###eois===4879###lif===13###soif===225###eoif===250###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");result = malloc(datalen);

		if (AKA_mark("lis===210###sois===4885###eois===4899###lif===14###soif===256###eoif===270###ifc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)") && (AKA_mark("lis===210###sois===4885###eois===4899###lif===14###soif===256###eoif===270###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&result == NULL)) {
		AKA_mark("lis===211###sois===4903###eois===4940###lif===15###soif===274###eoif===311###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");return wrap_json_error_out_of_memory;
	}
	else {AKA_mark("lis===-210-###sois===-4885-###eois===-488514-###lif===-14-###soif===-###eoif===-270-###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");}


	/* decode the input */
		AKA_mark("lis===214###sois===4972###eois===4992###lif===18###soif===343###eoif===363###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");for (iin = in = out = 0 ;AKA_mark("lis===214###sois===4993###eois===5005###lif===18###soif===364###eoif===376###ifc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)") && AKA_mark("lis===214###sois===4993###eois===5005###lif===18###soif===364###eoif===376###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&in < datalen;({AKA_mark("lis===214###sois===5008###eois===5012###lif===18###soif===379###eoif===383###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");in++;})) {
				AKA_mark("lis===215###sois===5018###eois===5031###lif===19###soif===389###eoif===402###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");c = data[in];

				if (AKA_mark("lis===216###sois===5038###eois===5072###lif===20###soif===409###eoif===443###ifc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)") && (((AKA_mark("lis===216###sois===5038###eois===5047###lif===20###soif===409###eoif===418###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&c != '\n')	&&(AKA_mark("lis===216###sois===5051###eois===5060###lif===20###soif===422###eoif===431###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&c != '\r'))	&&(AKA_mark("lis===216###sois===5064###eois===5072###lif===20###soif===435###eoif===443###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&c != '='))) {
						if (AKA_mark("lis===217###sois===5083###eois===5103###lif===21###soif===454###eoif===474###ifc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)") && ((AKA_mark("lis===217###sois===5083###eois===5091###lif===21###soif===454###eoif===462###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&'A' <= c)	&&(AKA_mark("lis===217###sois===5095###eois===5103###lif===21###soif===466###eoif===474###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&c <= 'Z'))) {
				AKA_mark("lis===218###sois===5109###eois===5133###lif===22###soif===480###eoif===504###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");u8 = (uint8_t)(c - 'A');
			}
			else {
				if (AKA_mark("lis===219###sois===5146###eois===5166###lif===23###soif===517###eoif===537###ifc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)") && ((AKA_mark("lis===219###sois===5146###eois===5154###lif===23###soif===517###eoif===525###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&'a' <= c)	&&(AKA_mark("lis===219###sois===5158###eois===5166###lif===23###soif===529###eoif===537###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&c <= 'z'))) {
					AKA_mark("lis===220###sois===5172###eois===5201###lif===24###soif===543###eoif===572###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");u8 = (uint8_t)(c - 'a' + 26);
				}
				else {
					if (AKA_mark("lis===221###sois===5214###eois===5234###lif===25###soif===585###eoif===605###ifc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)") && ((AKA_mark("lis===221###sois===5214###eois===5222###lif===25###soif===585###eoif===593###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&'0' <= c)	&&(AKA_mark("lis===221###sois===5226###eois===5234###lif===25###soif===597###eoif===605###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&c <= '9'))) {
						AKA_mark("lis===222###sois===5240###eois===5269###lif===26###soif===611###eoif===640###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");u8 = (uint8_t)(c - '0' + 52);
					}
					else {
						if (AKA_mark("lis===223###sois===5282###eois===5302###lif===27###soif===653###eoif===673###ifc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)") && ((AKA_mark("lis===223###sois===5282###eois===5290###lif===27###soif===653###eoif===661###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&c == '+')	||(AKA_mark("lis===223###sois===5294###eois===5302###lif===27###soif===665###eoif===673###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&c == '-'))) {
							AKA_mark("lis===224###sois===5308###eois===5325###lif===28###soif===679###eoif===696###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");u8 = (uint8_t)62;
						}
						else {
							if (AKA_mark("lis===225###sois===5338###eois===5358###lif===29###soif===709###eoif===729###ifc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)") && ((AKA_mark("lis===225###sois===5338###eois===5346###lif===29###soif===709###eoif===717###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&c == '/')	||(AKA_mark("lis===225###sois===5350###eois===5358###lif===29###soif===721###eoif===729###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&c == '_'))) {
								AKA_mark("lis===226###sois===5364###eois===5381###lif===30###soif===735###eoif===752###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");u8 = (uint8_t)63;
							}
							else {
												AKA_mark("lis===228###sois===5396###eois===5409###lif===32###soif===767###eoif===780###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");free(result);

												AKA_mark("lis===229###sois===5414###eois===5448###lif===33###soif===785###eoif===819###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");return wrap_json_error_bad_base64;

			}
						}
					}
				}
			}

						if (AKA_mark("lis===231###sois===5461###eois===5465###lif===35###soif===832###eoif===836###ifc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)") && (AKA_mark("lis===231###sois===5461###eois===5465###lif===35###soif===832###eoif===836###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&!iin)) {
								AKA_mark("lis===232###sois===5473###eois===5492###lif===36###soif===844###eoif===863###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");u16 = (uint16_t)u8;

								AKA_mark("lis===233###sois===5497###eois===5505###lif===37###soif===868###eoif===876###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");iin = 6;

			}
			else {
								AKA_mark("lis===235###sois===5522###eois===5556###lif===39###soif===893###eoif===927###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");u16 = (uint16_t)((u16 << 6) | u8);

								AKA_mark("lis===236###sois===5561###eois===5570###lif===40###soif===932###eoif===941###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");iin -= 2;

								AKA_mark("lis===237###sois===5575###eois===5602###lif===41###soif===946###eoif===973###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");u8 = (uint8_t)(u16 >> iin);

								AKA_mark("lis===238###sois===5607###eois===5626###lif===42###soif===978###eoif===997###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");result[out++] = u8;

			}

		}
		else {AKA_mark("lis===-216-###sois===-5038-###eois===-503834-###lif===-20-###soif===-###eoif===-443-###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");}

	}


	/* terminate */
		AKA_mark("lis===244###sois===5658###eois===5690###lif===48###soif===1029###eoif===1061###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");*decoded = realloc(result, out);

		if (AKA_mark("lis===245###sois===5696###eois===5719###lif===49###soif===1067###eoif===1090###ifc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)") && ((AKA_mark("lis===245###sois===5696###eois===5699###lif===49###soif===1067###eoif===1070###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&out)	&&(AKA_mark("lis===245###sois===5703###eois===5719###lif===49###soif===1074###eoif===1090###isc===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)")&&*decoded == NULL))) {
				AKA_mark("lis===246###sois===5725###eois===5738###lif===50###soif===1096###eoif===1109###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");free(result);

				AKA_mark("lis===247###sois===5741###eois===5778###lif===51###soif===1112###eoif===1149###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");return wrap_json_error_out_of_memory;

	}
	else {AKA_mark("lis===-245-###sois===-5696-###eois===-569623-###lif===-49-###soif===-###eoif===-1090-###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");}

		AKA_mark("lis===249###sois===5783###eois===5801###lif===53###soif===1154###eoif===1172###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");*decodedlen = out;

		AKA_mark("lis===250###sois===5803###eois===5812###lif===54###soif===1174###eoif===1183###ins===true###function===./app-framework-binder/src/wrap-json.c/decode_base64(const char*,size_t,uint8_t**,size_t*,int)");return 0;

}

/** Instrumented function skip(const char*) */
static inline const char *skip(const char *d)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/skip(const char*)");AKA_fCall++;
		while (AKA_mark("lis===255###sois===5872###eois===5900###lif===2###soif===56###eoif===84###ifc===true###function===./app-framework-binder/src/wrap-json.c/skip(const char*)") && ((AKA_mark("lis===255###sois===5872###eois===5874###lif===2###soif===56###eoif===58###isc===true###function===./app-framework-binder/src/wrap-json.c/skip(const char*)")&&*d)	&&(AKA_mark("lis===255###sois===5878###eois===5900###lif===2###soif===62###eoif===84###isc===true###function===./app-framework-binder/src/wrap-json.c/skip(const char*)")&&strchr(ignore_all, *d)))) {
		AKA_mark("lis===256###sois===5904###eois===5908###lif===3###soif===88###eoif===92###ins===true###function===./app-framework-binder/src/wrap-json.c/skip(const char*)");d++;
	}

		AKA_mark("lis===257###sois===5910###eois===5919###lif===4###soif===94###eoif===103###ins===true###function===./app-framework-binder/src/wrap-json.c/skip(const char*)");return d;

}

/** Instrumented function wrap_json_vpack(struct json_object**,const char*,va_list) */
int wrap_json_vpack(struct json_object **result, const char *desc, va_list args)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");AKA_fCall++;
	/* TODO: the case of structs with key being single char should be optimized */
		AKA_mark("lis===263###sois===6087###eois===6119###lif===3###soif===164###eoif===196###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");int nstr, notnull, nullable, rc;

		AKA_mark("lis===264###sois===6121###eois===6141###lif===4###soif===198###eoif===218###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");size_t sz, dsz, ssz;

		AKA_mark("lis===265###sois===6143###eois===6151###lif===5###soif===220###eoif===228###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");char *s;

		AKA_mark("lis===266###sois===6153###eois===6160###lif===6###soif===230###eoif===237###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");char c;

		AKA_mark("lis===267###sois===6162###eois===6176###lif===7###soif===239###eoif===253###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");const char *d;

		AKA_mark("lis===268###sois===6178###eois===6195###lif===8###soif===255###eoif===272###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");char buffer[256];

		AKA_mark("lis===269###sois===6197###eois===6271###lif===9###soif===274###eoif===348###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");struct { const uint8_t *in; size_t insz; char *out; size_t outsz; } bytes;

		AKA_mark("lis===270###sois===6273###eois===6327###lif===10###soif===350###eoif===404###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");struct { const char *str; size_t sz; } strs[STRCOUNT];

		AKA_mark("lis===271###sois===6329###eois===6424###lif===11###soif===406###eoif===501###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");struct { struct json_object *cont, *key; const char *acc; char type; } stack[STACKCOUNT], *top;

		AKA_mark("lis===272###sois===6426###eois===6450###lif===12###soif===503###eoif===527###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");struct json_object *obj;


		AKA_mark("lis===274###sois===6453###eois===6473###lif===14###soif===530###eoif===550###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");ssz = sizeof buffer;

		AKA_mark("lis===275###sois===6475###eois===6486###lif===15###soif===552###eoif===563###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");s = buffer;

		AKA_mark("lis===276###sois===6488###eois===6500###lif===16###soif===565###eoif===577###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top = stack;

		AKA_mark("lis===277###sois===6502###eois===6518###lif===17###soif===579###eoif===595###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->key = NULL;

		AKA_mark("lis===278###sois===6520###eois===6537###lif===18###soif===597###eoif===614###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->cont = NULL;

		AKA_mark("lis===279###sois===6539###eois===6566###lif===19###soif===616###eoif===643###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->acc = pack_accept_any;

		AKA_mark("lis===280###sois===6568###eois===6582###lif===20###soif===645###eoif===659###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->type = 0;

		AKA_mark("lis===281###sois===6584###eois===6593###lif===21###soif===661###eoif===670###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");d = desc;

		if (AKA_mark("lis===282###sois===6599###eois===6601###lif===22###soif===676###eoif===678###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===282###sois===6599###eois===6601###lif===22###soif===676###eoif===678###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!d)) {
		AKA_mark("lis===283###sois===6605###eois===6620###lif===23###soif===682###eoif===697###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto null_spec;
	}
	else {AKA_mark("lis===-282-###sois===-6599-###eois===-65992-###lif===-22-###soif===-###eoif===-678-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

		AKA_mark("lis===284###sois===6622###eois===6634###lif===24###soif===699###eoif===711###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");d = skip(d);

		for (;;) {
				AKA_mark("lis===286###sois===6648###eois===6655###lif===26###soif===725###eoif===732###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");c = *d;

				if (AKA_mark("lis===287###sois===6662###eois===6664###lif===27###soif===739###eoif===741###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===287###sois===6662###eois===6664###lif===27###soif===739###eoif===741###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!c)) {
			AKA_mark("lis===288###sois===6669###eois===6684###lif===28###soif===746###eoif===761###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto truncated;
		}
		else {AKA_mark("lis===-287-###sois===-6662-###eois===-66622-###lif===-27-###soif===-###eoif===-741-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

				if (AKA_mark("lis===289###sois===6691###eois===6711###lif===29###soif===768###eoif===788###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===289###sois===6691###eois===6711###lif===29###soif===768###eoif===788###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!strchr(top->acc, c))) {
			AKA_mark("lis===290###sois===6716###eois===6739###lif===30###soif===793###eoif===816###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto invalid_character;
		}
		else {AKA_mark("lis===-289-###sois===-6691-###eois===-669120-###lif===-29-###soif===-###eoif===-788-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

				AKA_mark("lis===291###sois===6742###eois===6758###lif===31###soif===819###eoif===835###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");d = skip(d + 1);

				AKA_mark("lis===292###sois===6768###eois===6769###lif===32###soif===845###eoif===846###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");switch(c){
					case 's': if(c == 's')AKA_mark("lis===293###sois===6775###eois===6784###lif===33###soif===852###eoif===861###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

						AKA_mark("lis===294###sois===6788###eois===6801###lif===34###soif===865###eoif===878###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");nullable = 0;

						AKA_mark("lis===295###sois===6805###eois===6817###lif===35###soif===882###eoif===894###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");notnull = 0;

						AKA_mark("lis===296###sois===6821###eois===6830###lif===36###soif===898###eoif===907###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");nstr = 0;

						AKA_mark("lis===297###sois===6834###eois===6841###lif===37###soif===911###eoif===918###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");sz = 0;

						for (;;) {
				/* Cant instrument this following code */
strs[nstr].str = va_arg(args, const char*);
								if (AKA_mark("lis===300###sois===6912###eois===6926###lif===40###soif===989###eoif===1003###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===300###sois===6912###eois===6926###lif===40###soif===989###eoif===1003###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&strs[nstr].str)) {
					AKA_mark("lis===301###sois===6933###eois===6945###lif===41###soif===1010###eoif===1022###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");notnull = 1;
				}
				else {AKA_mark("lis===-300-###sois===-6912-###eois===-691214-###lif===-40-###soif===-###eoif===-1003-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

								if (AKA_mark("lis===302###sois===6954###eois===6963###lif===42###soif===1031###eoif===1040###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===302###sois===6954###eois===6963###lif===42###soif===1031###eoif===1040###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&*d == '?')) {
										AKA_mark("lis===303###sois===6972###eois===6988###lif===43###soif===1049###eoif===1065###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");d = skip(d + 1);

										AKA_mark("lis===304###sois===6994###eois===7007###lif===44###soif===1071###eoif===1084###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");nullable = 1;

				}
				else {AKA_mark("lis===-302-###sois===-6954-###eois===-69549-###lif===-42-###soif===-###eoif===-1040-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

								AKA_mark("lis===306###sois===7025###eois===7027###lif===46###soif===1102###eoif===1104###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");switch(*d){
									case '%': if(*d == '%')AKA_mark("lis===307###sois===7035###eois===7044###lif===47###soif===1112###eoif===1121###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");
 					AKA_mark("lis===307###sois===7045###eois===7082###lif===47###soif===1122###eoif===1159###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");strs[nstr].sz = va_arg(args, size_t);
 					AKA_mark("lis===307###sois===7083###eois===7099###lif===47###soif===1160###eoif===1176###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");d = skip(d + 1);
 					AKA_mark("lis===307###sois===7100###eois===7106###lif===47###soif===1177###eoif===1183###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

									case '#': if(*d == '#')AKA_mark("lis===308###sois===7111###eois===7120###lif===48###soif===1188###eoif===1197###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");
 					AKA_mark("lis===308###sois===7121###eois===7145###lif===48###soif===1198###eoif===1222###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list");strs[nstr].sz = (size_t)
/* Cant instrument this following code */
va_arg(args, int); 					AKA_mark("lis===308###sois===7164###eois===7180###lif===48###soif===1241###eoif===1257###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");d = skip(d + 1);
 					AKA_mark("lis===308###sois===7181###eois===7187###lif===48###soif===1258###eoif===1264###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

									default: if(*d != '%' && *d != '#')AKA_mark("lis===309###sois===7192###eois===7200###lif===49###soif===1269###eoif===1277###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");
 					AKA_mark("lis===309###sois===7201###eois===7261###lif===49###soif===1278###eoif===1338###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");strs[nstr].sz = strs[nstr].str ? strlen(strs[nstr].str) : 0;
 					AKA_mark("lis===309###sois===7262###eois===7268###lif===49###soif===1339###eoif===1345###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

				}

								AKA_mark("lis===311###sois===7279###eois===7301###lif===51###soif===1356###eoif===1378###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");sz += strs[nstr++].sz;

								if (AKA_mark("lis===312###sois===7310###eois===7319###lif===52###soif===1387###eoif===1396###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===312###sois===7310###eois===7319###lif===52###soif===1387###eoif===1396###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&*d == '?')) {
										AKA_mark("lis===313###sois===7328###eois===7344###lif===53###soif===1405###eoif===1421###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");d = skip(d + 1);

										AKA_mark("lis===314###sois===7350###eois===7363###lif===54###soif===1427###eoif===1440###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");nullable = 1;

				}
				else {AKA_mark("lis===-312-###sois===-7310-###eois===-73109-###lif===-52-###soif===-###eoif===-1396-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

								if (AKA_mark("lis===316###sois===7378###eois===7387###lif===56###soif===1455###eoif===1464###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===316###sois===7378###eois===7387###lif===56###soif===1455###eoif===1464###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&*d != '+')) {
					AKA_mark("lis===317###sois===7394###eois===7400###lif===57###soif===1471###eoif===1477###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;
				}
				else {AKA_mark("lis===-316-###sois===-7378-###eois===-73789-###lif===-56-###soif===-###eoif===-1464-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

								if (AKA_mark("lis===318###sois===7409###eois===7425###lif===58###soif===1486###eoif===1502###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===318###sois===7409###eois===7425###lif===58###soif===1486###eoif===1502###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&nstr >= STRCOUNT)) {
					AKA_mark("lis===319###sois===7432###eois===7446###lif===59###soif===1509###eoif===1523###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto too_long;
				}
				else {AKA_mark("lis===-318-###sois===-7409-###eois===-740916-###lif===-58-###soif===-###eoif===-1502-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

								AKA_mark("lis===320###sois===7451###eois===7467###lif===60###soif===1528###eoif===1544###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");d = skip(d + 1);

			}

						if (AKA_mark("lis===322###sois===7480###eois===7489###lif===62###soif===1557###eoif===1566###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===322###sois===7480###eois===7489###lif===62###soif===1557###eoif===1566###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&*d == '*')) {
				AKA_mark("lis===323###sois===7495###eois===7508###lif===63###soif===1572###eoif===1585###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");nullable = 1;
			}
			else {AKA_mark("lis===-322-###sois===-7480-###eois===-74809-###lif===-62-###soif===-###eoif===-1566-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						if (AKA_mark("lis===324###sois===7516###eois===7523###lif===64###soif===1593###eoif===1600###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===324###sois===7516###eois===7523###lif===64###soif===1593###eoif===1600###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&notnull)) {
								if (AKA_mark("lis===325###sois===7535###eois===7543###lif===65###soif===1612###eoif===1620###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===325###sois===7535###eois===7543###lif===65###soif===1612###eoif===1620###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&sz > ssz)) {
										AKA_mark("lis===326###sois===7552###eois===7563###lif===66###soif===1629###eoif===1640###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");ssz += ssz;

										if (AKA_mark("lis===327###sois===7573###eois===7581###lif===67###soif===1650###eoif===1658###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===327###sois===7573###eois===7581###lif===67###soif===1650###eoif===1658###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&ssz < sz)) {
						AKA_mark("lis===328###sois===7589###eois===7598###lif===68###soif===1666###eoif===1675###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");ssz = sz;
					}
					else {AKA_mark("lis===-327-###sois===-7573-###eois===-75738-###lif===-67-###soif===-###eoif===-1658-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

										AKA_mark("lis===329###sois===7604###eois===7619###lif===69###soif===1681###eoif===1696###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");s = alloca(sz);

				}
				else {AKA_mark("lis===-325-###sois===-7535-###eois===-75358-###lif===-65-###soif===-###eoif===-1620-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

								AKA_mark("lis===331###sois===7630###eois===7639###lif===71###soif===1707###eoif===1716###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");dsz = sz;

								while (AKA_mark("lis===332###sois===7651###eois===7655###lif===72###soif===1728###eoif===1732###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===332###sois===7651###eois===7655###lif===72###soif===1728###eoif===1732###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&nstr)) {
										AKA_mark("lis===333###sois===7664###eois===7671###lif===73###soif===1741###eoif===1748###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");nstr--;

										AKA_mark("lis===334###sois===7677###eois===7698###lif===74###soif===1754###eoif===1775###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");dsz -= strs[nstr].sz;

										AKA_mark("lis===335###sois===7704###eois===7751###lif===75###soif===1781###eoif===1828###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");memcpy(&s[dsz], strs[nstr].str, strs[nstr].sz);

				}

								AKA_mark("lis===337###sois===7762###eois===7807###lif===77###soif===1839###eoif===1884###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");obj = json_object_new_string_len(s, (int)sz);

								if (AKA_mark("lis===338###sois===7816###eois===7820###lif===78###soif===1893###eoif===1897###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===338###sois===7816###eois===7820###lif===78###soif===1893###eoif===1897###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!obj)) {
					AKA_mark("lis===339###sois===7827###eois===7846###lif===79###soif===1904###eoif===1923###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto out_of_memory;
				}
				else {AKA_mark("lis===-338-###sois===-7816-###eois===-78164-###lif===-78-###soif===-###eoif===-1897-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

			}
			else {
				if (AKA_mark("lis===340###sois===7861###eois===7869###lif===80###soif===1938###eoif===1946###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===340###sois===7861###eois===7869###lif===80###soif===1938###eoif===1946###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&nullable)) {
					AKA_mark("lis===341###sois===7875###eois===7886###lif===81###soif===1952###eoif===1963###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");obj = NULL;
				}
				else {
					AKA_mark("lis===343###sois===7899###eois===7916###lif===83###soif===1976###eoif===1993###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto null_string;
				}
			}

						AKA_mark("lis===344###sois===7920###eois===7926###lif===84###soif===1997###eoif===2003###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

					case 'n': if(c == 'n')AKA_mark("lis===345###sois===7929###eois===7938###lif===85###soif===2006###eoif===2015###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

						AKA_mark("lis===346###sois===7942###eois===7953###lif===86###soif===2019###eoif===2030###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");obj = NULL;

						AKA_mark("lis===347###sois===7957###eois===7963###lif===87###soif===2034###eoif===2040###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

					case 'b': if(c == 'b')AKA_mark("lis===348###sois===7966###eois===7975###lif===88###soif===2043###eoif===2052###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

			/* Cant instrument this following code */
obj = json_object_new_boolean(va_arg(args, int));
						if (AKA_mark("lis===350###sois===8036###eois===8040###lif===90###soif===2113###eoif===2117###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===350###sois===8036###eois===8040###lif===90###soif===2113###eoif===2117###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!obj)) {
				AKA_mark("lis===351###sois===8046###eois===8065###lif===91###soif===2123###eoif===2142###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto out_of_memory;
			}
			else {AKA_mark("lis===-350-###sois===-8036-###eois===-80364-###lif===-90-###soif===-###eoif===-2117-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						AKA_mark("lis===352###sois===8069###eois===8075###lif===92###soif===2146###eoif===2152###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

					case 'i': if(c == 'i')AKA_mark("lis===353###sois===8078###eois===8087###lif===93###soif===2155###eoif===2164###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

			/* Cant instrument this following code */
obj = json_object_new_int(va_arg(args, int));
						if (AKA_mark("lis===355###sois===8144###eois===8148###lif===95###soif===2221###eoif===2225###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===355###sois===8144###eois===8148###lif===95###soif===2221###eoif===2225###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!obj)) {
				AKA_mark("lis===356###sois===8154###eois===8173###lif===96###soif===2231###eoif===2250###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto out_of_memory;
			}
			else {AKA_mark("lis===-355-###sois===-8144-###eois===-81444-###lif===-95-###soif===-###eoif===-2225-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						AKA_mark("lis===357###sois===8177###eois===8183###lif===97###soif===2254###eoif===2260###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

					case 'I': if(c == 'I')AKA_mark("lis===358###sois===8186###eois===8195###lif===98###soif===2263###eoif===2272###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

						AKA_mark("lis===359###sois===8199###eois===8250###lif===99###soif===2276###eoif===2327###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");obj = json_object_new_int64(va_arg(args, int64_t));

						if (AKA_mark("lis===360###sois===8258###eois===8262###lif===100###soif===2335###eoif===2339###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===360###sois===8258###eois===8262###lif===100###soif===2335###eoif===2339###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!obj)) {
				AKA_mark("lis===361###sois===8268###eois===8287###lif===101###soif===2345###eoif===2364###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto out_of_memory;
			}
			else {AKA_mark("lis===-360-###sois===-8258-###eois===-82584-###lif===-100-###soif===-###eoif===-2339-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						AKA_mark("lis===362###sois===8291###eois===8297###lif===102###soif===2368###eoif===2374###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

					case 'f': if(c == 'f')AKA_mark("lis===363###sois===8300###eois===8309###lif===103###soif===2377###eoif===2386###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

			/* Cant instrument this following code */
obj = json_object_new_double(va_arg(args, double));
						if (AKA_mark("lis===365###sois===8372###eois===8376###lif===105###soif===2449###eoif===2453###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===365###sois===8372###eois===8376###lif===105###soif===2449###eoif===2453###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!obj)) {
				AKA_mark("lis===366###sois===8382###eois===8401###lif===106###soif===2459###eoif===2478###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto out_of_memory;
			}
			else {AKA_mark("lis===-365-###sois===-8372-###eois===-83724-###lif===-105-###soif===-###eoif===-2453-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						AKA_mark("lis===367###sois===8405###eois===8411###lif===107###soif===2482###eoif===2488###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

					case 'o': if(c == 'o')AKA_mark("lis===368###sois===8414###eois===8423###lif===108###soif===2491###eoif===2500###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

					case 'O': if(c == 'O')AKA_mark("lis===369###sois===8426###eois===8435###lif===109###soif===2503###eoif===2512###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

			/* Cant instrument this following code */
obj = va_arg(args, struct json_object*);
						if (AKA_mark("lis===371###sois===8487###eois===8496###lif===111###soif===2564###eoif===2573###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===371###sois===8487###eois===8496###lif===111###soif===2564###eoif===2573###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&*d == '?')) {
				AKA_mark("lis===372###sois===8502###eois===8518###lif===112###soif===2579###eoif===2595###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");d = skip(d + 1);
			}
			else {
				if (AKA_mark("lis===373###sois===8531###eois===8548###lif===113###soif===2608###eoif===2625###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && ((AKA_mark("lis===373###sois===8531###eois===8540###lif===113###soif===2608###eoif===2617###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&*d != '*')	&&(AKA_mark("lis===373###sois===8544###eois===8548###lif===113###soif===2621###eoif===2625###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!obj))) {
					AKA_mark("lis===374###sois===8554###eois===8571###lif===114###soif===2631###eoif===2648###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto null_object;
				}
				else {AKA_mark("lis===-373-###sois===-8531-###eois===-853117-###lif===-113-###soif===-###eoif===-2625-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}
			}

						if (AKA_mark("lis===375###sois===8579###eois===8587###lif===115###soif===2656###eoif===2664###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===375###sois===8579###eois===8587###lif===115###soif===2656###eoif===2664###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&c == 'O')) {
				AKA_mark("lis===376###sois===8593###eois===8614###lif===116###soif===2670###eoif===2691###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");json_object_get(obj);
			}
			else {AKA_mark("lis===-375-###sois===-8579-###eois===-85798-###lif===-115-###soif===-###eoif===-2664-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						AKA_mark("lis===377###sois===8618###eois===8624###lif===117###soif===2695###eoif===2701###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

					case 'y': if(c == 'y')AKA_mark("lis===378###sois===8627###eois===8636###lif===118###soif===2704###eoif===2713###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

					case 'Y': if(c == 'Y')AKA_mark("lis===379###sois===8639###eois===8648###lif===119###soif===2716###eoif===2725###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

			/* Cant instrument this following code */
bytes.in = va_arg(args, const uint8_t*);
						AKA_mark("lis===381###sois===8696###eois===8730###lif===121###soif===2773###eoif===2807###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");bytes.insz = va_arg(args, size_t);

						if (AKA_mark("lis===382###sois===8738###eois===8773###lif===122###soif===2815###eoif===2850###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && ((AKA_mark("lis===382###sois===8738###eois===8754###lif===122###soif===2815###eoif===2831###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&bytes.in == NULL)	||(AKA_mark("lis===382###sois===8758###eois===8773###lif===122###soif===2835###eoif===2850###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&bytes.insz == 0))) {
				AKA_mark("lis===383###sois===8779###eois===8790###lif===123###soif===2856###eoif===2867###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");obj = NULL;
			}
			else {
								AKA_mark("lis===385###sois===8805###eois===8893###lif===125###soif===2882###eoif===2970###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");rc = encode_base64(bytes.in, bytes.insz,
					&bytes.out, &bytes.outsz, 0, 0, c == 'y');

								if (AKA_mark("lis===387###sois===8902###eois===8904###lif===127###soif===2979###eoif===2981###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===387###sois===8902###eois===8904###lif===127###soif===2979###eoif===2981###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&rc)) {
					AKA_mark("lis===388###sois===8911###eois===8922###lif===128###soif===2988###eoif===2999###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto error;
				}
				else {AKA_mark("lis===-387-###sois===-8902-###eois===-89022-###lif===-127-###soif===-###eoif===-2981-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

								AKA_mark("lis===389###sois===8927###eois===8989###lif===129###soif===3004###eoif===3066###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");obj = json_object_new_string_len(bytes.out, (int)bytes.outsz);

								AKA_mark("lis===390###sois===8994###eois===9010###lif===130###soif===3071###eoif===3087###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");free(bytes.out);

								if (AKA_mark("lis===391###sois===9019###eois===9023###lif===131###soif===3096###eoif===3100###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===391###sois===9019###eois===9023###lif===131###soif===3096###eoif===3100###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!obj)) {
					AKA_mark("lis===392###sois===9030###eois===9049###lif===132###soif===3107###eoif===3126###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto out_of_memory;
				}
				else {AKA_mark("lis===-391-###sois===-9019-###eois===-90194-###lif===-131-###soif===-###eoif===-3100-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

			}

						if (AKA_mark("lis===394###sois===9062###eois===9071###lif===134###soif===3139###eoif===3148###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===394###sois===9062###eois===9071###lif===134###soif===3139###eoif===3148###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&*d == '?')) {
				AKA_mark("lis===395###sois===9077###eois===9093###lif===135###soif===3154###eoif===3170###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");d = skip(d + 1);
			}
			else {
				if (AKA_mark("lis===396###sois===9106###eois===9123###lif===136###soif===3183###eoif===3200###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && ((AKA_mark("lis===396###sois===9106###eois===9115###lif===136###soif===3183###eoif===3192###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&*d != '*')	&&(AKA_mark("lis===396###sois===9119###eois===9123###lif===136###soif===3196###eoif===3200###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!obj))) {
									AKA_mark("lis===397###sois===9131###eois===9170###lif===137###soif===3208###eoif===3247###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");obj = json_object_new_string_len(d, 0);

									if (AKA_mark("lis===398###sois===9179###eois===9183###lif===138###soif===3256###eoif===3260###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===398###sois===9179###eois===9183###lif===138###soif===3256###eoif===3260###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!obj)) {
						AKA_mark("lis===399###sois===9190###eois===9209###lif===139###soif===3267###eoif===3286###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto out_of_memory;
					}
					else {AKA_mark("lis===-398-###sois===-9179-###eois===-91794-###lif===-138-###soif===-###eoif===-3260-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

			}
				else {AKA_mark("lis===-396-###sois===-9106-###eois===-910617-###lif===-136-###soif===-###eoif===-3200-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}
			}

						AKA_mark("lis===401###sois===9218###eois===9224###lif===141###soif===3295###eoif===3301###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

					case '[': if(c == '[')AKA_mark("lis===402###sois===9227###eois===9236###lif===142###soif===3304###eoif===3313###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

					case '{': if(c == '{')AKA_mark("lis===403###sois===9239###eois===9248###lif===143###soif===3316###eoif===3325###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

						if (AKA_mark("lis===404###sois===9256###eois===9283###lif===144###soif===3333###eoif===3360###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===404###sois===9256###eois===9283###lif===144###soif===3333###eoif===3360###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&++top >= &stack[STACKCOUNT])) {
				AKA_mark("lis===405###sois===9289###eois===9303###lif===145###soif===3366###eoif===3380###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto too_deep;
			}
			else {AKA_mark("lis===-404-###sois===-9256-###eois===-925627-###lif===-144-###soif===-###eoif===-3360-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						AKA_mark("lis===406###sois===9307###eois===9323###lif===146###soif===3384###eoif===3400###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->key = NULL;

						if (AKA_mark("lis===407###sois===9331###eois===9339###lif===147###soif===3408###eoif===3416###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===407###sois===9331###eois===9339###lif===147###soif===3408###eoif===3416###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&c == '[')) {
								AKA_mark("lis===408###sois===9347###eois===9363###lif===148###soif===3424###eoif===3440###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->type = ']';

								AKA_mark("lis===409###sois===9368###eois===9395###lif===149###soif===3445###eoif===3472###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->acc = pack_accept_arr;

								AKA_mark("lis===410###sois===9400###eois===9436###lif===150###soif===3477###eoif===3513###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->cont = json_object_new_array();

			}
			else {
								AKA_mark("lis===412###sois===9453###eois===9469###lif===152###soif===3530###eoif===3546###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->type = '}';

								AKA_mark("lis===413###sois===9474###eois===9501###lif===153###soif===3551###eoif===3578###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->acc = pack_accept_key;

								AKA_mark("lis===414###sois===9506###eois===9543###lif===154###soif===3583###eoif===3620###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->cont = json_object_new_object();

			}

						if (AKA_mark("lis===416###sois===9556###eois===9566###lif===156###soif===3633###eoif===3643###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===416###sois===9556###eois===9566###lif===156###soif===3633###eoif===3643###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!top->cont)) {
				AKA_mark("lis===417###sois===9572###eois===9591###lif===157###soif===3649###eoif===3668###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto out_of_memory;
			}
			else {AKA_mark("lis===-416-###sois===-9556-###eois===-955610-###lif===-156-###soif===-###eoif===-3643-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						AKA_mark("lis===418###sois===9595###eois===9604###lif===158###soif===3672###eoif===3681###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");continue;

					case '}': if(c == '}')AKA_mark("lis===419###sois===9607###eois===9616###lif===159###soif===3684###eoif===3693###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

					case ']': if(c == ']')AKA_mark("lis===420###sois===9619###eois===9628###lif===160###soif===3696###eoif===3705###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

						if (AKA_mark("lis===421###sois===9636###eois===9666###lif===161###soif===3713###eoif===3743###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && ((AKA_mark("lis===421###sois===9636###eois===9650###lif===161###soif===3713###eoif===3727###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&c != top->type)	||(AKA_mark("lis===421###sois===9654###eois===9666###lif===161###soif===3731###eoif===3743###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&top <= stack))) {
				AKA_mark("lis===422###sois===9672###eois===9692###lif===162###soif===3749###eoif===3769###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto internal_error;
			}
			else {AKA_mark("lis===-421-###sois===-9636-###eois===-963630-###lif===-161-###soif===-###eoif===-3743-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						AKA_mark("lis===423###sois===9696###eois===9716###lif===163###soif===3773###eoif===3793###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");obj = (top--)->cont;

						if (AKA_mark("lis===424###sois===9724###eois===9813###lif===164###soif===3801###eoif===3890###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && ((AKA_mark("lis===424###sois===9724###eois===9733###lif===164###soif===3801###eoif===3810###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&*d == '*')	&&(AKA_mark("lis===424###sois===9737###eois===9813###lif===164###soif===3814###eoif===3890###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!(c == '}' ? json_object_object_length(obj) : json_object_array_length(obj))))) {
								AKA_mark("lis===425###sois===9821###eois===9842###lif===165###soif===3898###eoif===3919###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");json_object_put(obj);

								AKA_mark("lis===426###sois===9847###eois===9858###lif===166###soif===3924###eoif===3935###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");obj = NULL;

			}
			else {AKA_mark("lis===-424-###sois===-9724-###eois===-972489-###lif===-164-###soif===-###eoif===-3890-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						AKA_mark("lis===428###sois===9867###eois===9873###lif===168###soif===3944###eoif===3950###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

					default: if(c != 's' && c != 'n' && c != 'b' && c != 'i' && c != 'I' && c != 'f' && c != 'o' && c != 'O' && c != 'y' && c != 'Y' && c != '[' && c != '{' && c != '}' && c != ']')AKA_mark("lis===429###sois===9876###eois===9884###lif===169###soif===3953###eoif===3961###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

						AKA_mark("lis===430###sois===9888###eois===9908###lif===170###soif===3965###eoif===3985###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto internal_error;

		}

				AKA_mark("lis===432###sois===9923###eois===9932###lif===172###soif===4000###eoif===4009###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");switch(top->type){
					case 0: if(top->type == 0)AKA_mark("lis===433###sois===9938###eois===9945###lif===173###soif===4015###eoif===4022###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

						if (AKA_mark("lis===434###sois===9953###eois===9965###lif===174###soif===4030###eoif===4042###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===434###sois===9953###eois===9965###lif===174###soif===4030###eoif===4042###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&top != stack)) {
				AKA_mark("lis===435###sois===9971###eois===9991###lif===175###soif===4048###eoif===4068###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto internal_error;
			}
			else {AKA_mark("lis===-434-###sois===-9953-###eois===-995312-###lif===-174-###soif===-###eoif===-4042-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						if (AKA_mark("lis===436###sois===9999###eois===10001###lif===176###soif===4076###eoif===4078###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===436###sois===9999###eois===10001###lif===176###soif===4076###eoif===4078###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&*d)) {
				AKA_mark("lis===437###sois===10007###eois===10030###lif===177###soif===4084###eoif===4107###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto invalid_character;
			}
			else {AKA_mark("lis===-436-###sois===-9999-###eois===-99992-###lif===-176-###soif===-###eoif===-4078-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						AKA_mark("lis===438###sois===10034###eois===10048###lif===178###soif===4111###eoif===4125###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");*result = obj;

						AKA_mark("lis===439###sois===10052###eois===10061###lif===179###soif===4129###eoif===4138###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");return 0;

					case ']': if(top->type == ']')AKA_mark("lis===440###sois===10064###eois===10073###lif===180###soif===4141###eoif===4150###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

						if (AKA_mark("lis===441###sois===10081###eois===10097###lif===181###soif===4158###eoif===4174###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && ((AKA_mark("lis===441###sois===10081###eois===10084###lif===181###soif===4158###eoif===4161###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&obj)	||(AKA_mark("lis===441###sois===10088###eois===10097###lif===181###soif===4165###eoif===4174###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&*d != '*'))) {
				AKA_mark("lis===442###sois===10103###eois===10141###lif===182###soif===4180###eoif===4218###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");json_object_array_add(top->cont, obj);
			}
			else {AKA_mark("lis===-441-###sois===-10081-###eois===-1008116-###lif===-181-###soif===-###eoif===-4174-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						if (AKA_mark("lis===443###sois===10149###eois===10158###lif===183###soif===4226###eoif===4235###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===443###sois===10149###eois===10158###lif===183###soif===4226###eoif===4235###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&*d == '*')) {
				AKA_mark("lis===444###sois===10164###eois===10180###lif===184###soif===4241###eoif===4257###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");d = skip(d + 1);
			}
			else {AKA_mark("lis===-443-###sois===-10149-###eois===-101499-###lif===-183-###soif===-###eoif===-4235-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						AKA_mark("lis===445###sois===10184###eois===10190###lif===185###soif===4261###eoif===4267###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

					case '}': if(top->type == '}')AKA_mark("lis===446###sois===10193###eois===10202###lif===186###soif===4270###eoif===4279###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

						if (AKA_mark("lis===447###sois===10210###eois===10214###lif===187###soif===4287###eoif===4291###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===447###sois===10210###eois===10214###lif===187###soif===4287###eoif===4291###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&!obj)) {
				AKA_mark("lis===448###sois===10220###eois===10234###lif===188###soif===4297###eoif===4311###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto null_key;
			}
			else {AKA_mark("lis===-447-###sois===-10210-###eois===-102104-###lif===-187-###soif===-###eoif===-4291-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						AKA_mark("lis===449###sois===10238###eois===10253###lif===189###soif===4315###eoif===4330###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->key = obj;

						AKA_mark("lis===450###sois===10257###eois===10284###lif===190###soif===4334###eoif===4361###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->acc = pack_accept_any;

						AKA_mark("lis===451###sois===10288###eois===10304###lif===191###soif===4365###eoif===4381###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->type = ':';

						AKA_mark("lis===452###sois===10308###eois===10314###lif===192###soif===4385###eoif===4391###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

					case ':': if(top->type == ':')AKA_mark("lis===453###sois===10317###eois===10326###lif===193###soif===4394###eoif===4403###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

						if (AKA_mark("lis===454###sois===10334###eois===10350###lif===194###soif===4411###eoif===4427###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && ((AKA_mark("lis===454###sois===10334###eois===10337###lif===194###soif===4411###eoif===4414###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&obj)	||(AKA_mark("lis===454###sois===10341###eois===10350###lif===194###soif===4418###eoif===4427###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&*d != '*'))) {
				AKA_mark("lis===455###sois===10356###eois===10429###lif===195###soif===4433###eoif===4506###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");json_object_object_add(top->cont, json_object_get_string(top->key), obj);
			}
			else {AKA_mark("lis===-454-###sois===-10334-###eois===-1033416-###lif===-194-###soif===-###eoif===-4427-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						if (AKA_mark("lis===456###sois===10437###eois===10446###lif===196###soif===4514###eoif===4523###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===456###sois===10437###eois===10446###lif===196###soif===4514###eoif===4523###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&*d == '*')) {
				AKA_mark("lis===457###sois===10452###eois===10468###lif===197###soif===4529###eoif===4545###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");d = skip(d + 1);
			}
			else {AKA_mark("lis===-456-###sois===-10437-###eois===-104379-###lif===-196-###soif===-###eoif===-4523-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");}

						AKA_mark("lis===458###sois===10472###eois===10498###lif===198###soif===4549###eoif===4575###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");json_object_put(top->key);

						AKA_mark("lis===459###sois===10502###eois===10518###lif===199###soif===4579###eoif===4595###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->key = NULL;

						AKA_mark("lis===460###sois===10522###eois===10549###lif===200###soif===4599###eoif===4626###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->acc = pack_accept_key;

						AKA_mark("lis===461###sois===10553###eois===10569###lif===201###soif===4630###eoif===4646###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");top->type = '}';

						AKA_mark("lis===462###sois===10573###eois===10579###lif===202###soif===4650###eoif===4656###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");break;

					default: if(top->type != 0 && top->type != ']' && top->type != '}' && top->type != ':')AKA_mark("lis===463###sois===10582###eois===10590###lif===203###soif===4659###eoif===4667###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");

						AKA_mark("lis===464###sois===10594###eois===10614###lif===204###soif===4671###eoif===4691###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto internal_error;

		}

	}


	null_object:
	AKA_mark("lis===469###sois===10637###eois===10670###lif===209###soif===4714###eoif===4747###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");rc = wrap_json_error_null_object;

		AKA_mark("lis===470###sois===10672###eois===10683###lif===210###soif===4749###eoif===4760###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto error;

	truncated:
	AKA_mark("lis===472###sois===10696###eois===10727###lif===212###soif===4773###eoif===4804###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");rc = wrap_json_error_truncated;

		AKA_mark("lis===473###sois===10729###eois===10740###lif===213###soif===4806###eoif===4817###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto error;

	internal_error:
	AKA_mark("lis===475###sois===10758###eois===10794###lif===215###soif===4835###eoif===4871###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");rc = wrap_json_error_internal_error;

		AKA_mark("lis===476###sois===10796###eois===10807###lif===216###soif===4873###eoif===4884###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto error;

	out_of_memory:
	AKA_mark("lis===478###sois===10824###eois===10859###lif===218###soif===4901###eoif===4936###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");rc = wrap_json_error_out_of_memory;

		AKA_mark("lis===479###sois===10861###eois===10872###lif===219###soif===4938###eoif===4949###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto error;

	invalid_character:
	AKA_mark("lis===481###sois===10893###eois===10932###lif===221###soif===4970###eoif===5009###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");rc = wrap_json_error_invalid_character;

		AKA_mark("lis===482###sois===10934###eois===10945###lif===222###soif===5011###eoif===5022###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto error;

	too_long:
	AKA_mark("lis===484###sois===10957###eois===10987###lif===224###soif===5034###eoif===5064###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");rc = wrap_json_error_too_long;

		AKA_mark("lis===485###sois===10989###eois===11000###lif===225###soif===5066###eoif===5077###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto error;

	too_deep:
	AKA_mark("lis===487###sois===11012###eois===11042###lif===227###soif===5089###eoif===5119###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");rc = wrap_json_error_too_deep;

		AKA_mark("lis===488###sois===11044###eois===11055###lif===228###soif===5121###eoif===5132###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto error;

	null_spec:
	AKA_mark("lis===490###sois===11068###eois===11099###lif===230###soif===5145###eoif===5176###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");rc = wrap_json_error_null_spec;

		AKA_mark("lis===491###sois===11101###eois===11112###lif===231###soif===5178###eoif===5189###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto error;

	null_key:
	AKA_mark("lis===493###sois===11124###eois===11154###lif===233###soif===5201###eoif===5231###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");rc = wrap_json_error_null_key;

		AKA_mark("lis===494###sois===11156###eois===11167###lif===234###soif===5233###eoif===5244###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto error;

	null_string:
	AKA_mark("lis===496###sois===11182###eois===11215###lif===236###soif===5259###eoif===5292###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");rc = wrap_json_error_null_string;

		AKA_mark("lis===497###sois===11217###eois===11228###lif===237###soif===5294###eoif===5305###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");goto error;

	error:
	do {
			AKA_mark("lis===500###sois===11244###eois===11270###lif===240###soif===5321###eoif===5347###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");json_object_put(top->key);

			AKA_mark("lis===501###sois===11273###eois===11300###lif===241###soif===5350###eoif===5377###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");json_object_put(top->cont);

	}
while (AKA_mark("lis===502###sois===11311###eois===11325###lif===242###soif===5388###eoif===5402###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)") && (AKA_mark("lis===502###sois===11311###eois===11325###lif===242###soif===5388###eoif===5402###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)")&&--top >= stack));

		AKA_mark("lis===503###sois===11329###eois===11344###lif===243###soif===5406###eoif===5421###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");*result = NULL;

		AKA_mark("lis===504###sois===11346###eois===11379###lif===244###soif===5423###eoif===5456###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");rc = rc | (int)((d - desc) << 4);

		AKA_mark("lis===505###sois===11381###eois===11392###lif===245###soif===5458###eoif===5469###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vpack(struct json_object**,const char*,va_list)");return -rc;

}

/** Instrumented function wrap_json_pack(struct json_object**,const char*) */
int wrap_json_pack(struct json_object **result, const char *desc, ...)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_pack(struct json_object**,const char*)");AKA_fCall++;
		AKA_mark("lis===510###sois===11470###eois===11477###lif===2###soif===74###eoif===81###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_pack(struct json_object**,const char*)");int rc;

		AKA_mark("lis===511###sois===11479###eois===11492###lif===3###soif===83###eoif===96###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_pack(struct json_object**,const char*)");va_list args;


		AKA_mark("lis===513###sois===11495###eois===11516###lif===5###soif===99###eoif===120###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_pack(struct json_object**,const char*)");va_start(args, desc);

		AKA_mark("lis===514###sois===11518###eois===11559###lif===6###soif===122###eoif===163###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_pack(struct json_object**,const char*)");rc = wrap_json_vpack(result, desc, args);

		AKA_mark("lis===515###sois===11561###eois===11574###lif===7###soif===165###eoif===178###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_pack(struct json_object**,const char*)");va_end(args);

		AKA_mark("lis===516###sois===11576###eois===11586###lif===8###soif===180###eoif===190###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_pack(struct json_object**,const char*)");return rc;

}

/** Instrumented function vunpack(struct json_object*,const char*,va_list,int) */
static int vunpack(struct json_object *object, const char *desc, va_list args, int store)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");AKA_fCall++;
		AKA_mark("lis===521###sois===11683###eois===11713###lif===2###soif===93###eoif===123###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");int rc = 0, optionnal, ignore;

		AKA_mark("lis===522###sois===11715###eois===11742###lif===3###soif===125###eoif===152###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");char c, xacc[2] = { 0, 0 };

		AKA_mark("lis===523###sois===11744###eois===11760###lif===4###soif===154###eoif===170###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");const char *acc;

		AKA_mark("lis===524###sois===11762###eois===11789###lif===5###soif===172###eoif===199###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");const char *d, *fit = NULL;

		AKA_mark("lis===525###sois===11791###eois===11814###lif===6###soif===201###eoif===224###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");const char *key = NULL;

		AKA_mark("lis===526###sois===11816###eois===11839###lif===7###soif===226###eoif===249###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");const char **ps = NULL;

		AKA_mark("lis===527###sois===11841###eois===11859###lif===8###soif===251###eoif===269###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");double *pf = NULL;

		AKA_mark("lis===528###sois===11861###eois===11876###lif===9###soif===271###eoif===286###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");int *pi = NULL;

		AKA_mark("lis===529###sois===11878###eois===11897###lif===10###soif===288###eoif===307###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");int64_t *pI = NULL;

		AKA_mark("lis===530###sois===11899###eois===11917###lif===11###soif===309###eoif===327###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");size_t *pz = NULL;

		AKA_mark("lis===531###sois===11919###eois===11939###lif===12###soif===329###eoif===349###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");uint8_t **py = NULL;

		AKA_mark("lis===532###sois===11941###eois===12054###lif===13###soif===351###eoif===464###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");struct { struct json_object *parent; const char *acc; int index; int count; char type; } stack[STACKCOUNT], *top;

		AKA_mark("lis===533###sois===12056###eois===12080###lif===14###soif===466###eoif===490###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");struct json_object *obj;

		AKA_mark("lis===534###sois===12082###eois===12106###lif===15###soif===492###eoif===516###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");struct json_object **po;


		AKA_mark("lis===536###sois===12109###eois===12121###lif===17###soif===519###eoif===531###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");xacc[0] = 0;

		AKA_mark("lis===537###sois===12123###eois===12134###lif===18###soif===533###eoif===544###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");ignore = 0;

		AKA_mark("lis===538###sois===12136###eois===12147###lif===19###soif===546###eoif===557###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");top = NULL;

		AKA_mark("lis===539###sois===12149###eois===12173###lif===20###soif===559###eoif===583###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");acc = unpack_accept_any;

		AKA_mark("lis===540###sois===12175###eois===12184###lif===21###soif===585###eoif===594###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");d = desc;

		if (AKA_mark("lis===541###sois===12190###eois===12192###lif===22###soif===600###eoif===602###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===541###sois===12190###eois===12192###lif===22###soif===600###eoif===602###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!d)) {
		AKA_mark("lis===542###sois===12196###eois===12211###lif===23###soif===606###eoif===621###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto null_spec;
	}
	else {AKA_mark("lis===-541-###sois===-12190-###eois===-121902-###lif===-22-###soif===-###eoif===-602-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

		AKA_mark("lis===543###sois===12213###eois===12225###lif===24###soif===623###eoif===635###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");d = skip(d);

		AKA_mark("lis===544###sois===12227###eois===12240###lif===25###soif===637###eoif===650###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");obj = object;

		for (;;) {
				AKA_mark("lis===546###sois===12254###eois===12262###lif===27###soif===664###eoif===672###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");fit = d;

				AKA_mark("lis===547###sois===12265###eois===12272###lif===28###soif===675###eoif===682###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");c = *d;

				if (AKA_mark("lis===548###sois===12279###eois===12281###lif===29###soif===689###eoif===691###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===548###sois===12279###eois===12281###lif===29###soif===689###eoif===691###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!c)) {
			AKA_mark("lis===549###sois===12286###eois===12301###lif===30###soif===696###eoif===711###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto truncated;
		}
		else {AKA_mark("lis===-548-###sois===-12279-###eois===-122792-###lif===-29-###soif===-###eoif===-691-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

				if (AKA_mark("lis===550###sois===12308###eois===12323###lif===31###soif===718###eoif===733###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===550###sois===12308###eois===12323###lif===31###soif===718###eoif===733###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!strchr(acc, c))) {
			AKA_mark("lis===551###sois===12328###eois===12351###lif===32###soif===738###eoif===761###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto invalid_character;
		}
		else {AKA_mark("lis===-550-###sois===-12308-###eois===-1230815-###lif===-31-###soif===-###eoif===-733-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

				AKA_mark("lis===552###sois===12354###eois===12370###lif===33###soif===764###eoif===780###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");d = skip(d + 1);

				AKA_mark("lis===553###sois===12380###eois===12381###lif===34###soif===790###eoif===791###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");switch(c){
					case 's': if(c == 's')AKA_mark("lis===554###sois===12387###eois===12396###lif===35###soif===797###eoif===806###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

						if (AKA_mark("lis===555###sois===12404###eois===12418###lif===36###soif===814###eoif===828###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===555###sois===12404###eois===12418###lif===36###soif===814###eoif===828###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&xacc[0] == '}')) {
				/* expects a key */
				/* Cant instrument this following code */
key = va_arg(args, const char *);
								if (AKA_mark("lis===558###sois===12492###eois===12496###lif===39###soif===902###eoif===906###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===558###sois===12492###eois===12496###lif===39###soif===902###eoif===906###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!key)) {
					AKA_mark("lis===559###sois===12503###eois===12517###lif===40###soif===913###eoif===927###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto null_key;
				}
				else {AKA_mark("lis===-558-###sois===-12492-###eois===-124924-###lif===-39-###soif===-###eoif===-906-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

								if (AKA_mark("lis===560###sois===12526###eois===12535###lif===41###soif===936###eoif===945###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===560###sois===12526###eois===12535###lif===41###soif===936###eoif===945###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&*d != '?')) {
					AKA_mark("lis===561###sois===12542###eois===12556###lif===42###soif===952###eoif===966###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");optionnal = 0;
				}
				else {
										AKA_mark("lis===563###sois===12573###eois===12587###lif===44###soif===983###eoif===997###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");optionnal = 1;

										AKA_mark("lis===564###sois===12593###eois===12609###lif===45###soif===1003###eoif===1019###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");d = skip(d + 1);

				}

								if (AKA_mark("lis===566###sois===12624###eois===12630###lif===47###soif===1034###eoif===1040###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===566###sois===12624###eois===12630###lif===47###soif===1034###eoif===1040###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&ignore)) {
					AKA_mark("lis===567###sois===12637###eois===12646###lif===48###soif===1047###eoif===1056###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");ignore++;
				}
				else {
										if (AKA_mark("lis===569###sois===12667###eois===12716###lif===50###soif===1077###eoif===1126###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===569###sois===12667###eois===12716###lif===50###soif===1077###eoif===1126###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&json_object_object_get_ex(top->parent, key, &obj))) {
						/* found */
												AKA_mark("lis===571###sois===12744###eois===12757###lif===52###soif===1154###eoif===1167###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");top->index++;

					}
					else {
						/* not found */
												if (AKA_mark("lis===574###sois===12804###eois===12814###lif===55###soif===1214###eoif===1224###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===574###sois===12804###eois===12814###lif===55###soif===1214###eoif===1224###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!optionnal)) {
							AKA_mark("lis===575###sois===12823###eois===12842###lif===56###soif===1233###eoif===1252###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto key_not_found;
						}
						else {AKA_mark("lis===-574-###sois===-12804-###eois===-1280410-###lif===-55-###soif===-###eoif===-1224-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

												AKA_mark("lis===576###sois===12849###eois===12860###lif===57###soif===1259###eoif===1270###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");ignore = 1;

												AKA_mark("lis===577###sois===12867###eois===12878###lif===58###soif===1277###eoif===1288###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");obj = NULL;

					}

				}

								AKA_mark("lis===580###sois===12896###eois===12910###lif===61###soif===1306###eoif===1320###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");xacc[0] = ':';

								AKA_mark("lis===581###sois===12915###eois===12939###lif===62###soif===1325###eoif===1349###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");acc = unpack_accept_any;

								AKA_mark("lis===582###sois===12944###eois===12953###lif===63###soif===1354###eoif===1363###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");continue;

			}
			else {AKA_mark("lis===-555-###sois===-12404-###eois===-1240414-###lif===-36-###soif===-###eoif===-828-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

			/* get a string */
			/* Cant instrument this following code */
if (store)
				ps = va_arg(args, const char **);
						if (AKA_mark("lis===587###sois===13040###eois===13047###lif===68###soif===1450###eoif===1457###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===587###sois===13040###eois===13047###lif===68###soif===1450###eoif===1457###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!ignore)) {
								if (AKA_mark("lis===588###sois===13059###eois===13102###lif===69###soif===1469###eoif===1512###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===588###sois===13059###eois===13102###lif===69###soif===1469###eoif===1512###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!json_object_is_type(obj, json_type_string))) {
					AKA_mark("lis===589###sois===13109###eois===13122###lif===70###soif===1519###eoif===1532###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto missfit;
				}
				else {AKA_mark("lis===-588-###sois===-13059-###eois===-1305943-###lif===-69-###soif===-###eoif===-1512-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

								if (AKA_mark("lis===590###sois===13131###eois===13142###lif===71###soif===1541###eoif===1552###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && ((AKA_mark("lis===590###sois===13131###eois===13136###lif===71###soif===1541###eoif===1546###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&store)	&&(AKA_mark("lis===590###sois===13140###eois===13142###lif===71###soif===1550###eoif===1552###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&ps))) {
					AKA_mark("lis===591###sois===13149###eois===13183###lif===72###soif===1559###eoif===1593###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");*ps = json_object_get_string(obj);
				}
				else {AKA_mark("lis===-590-###sois===-13131-###eois===-1313111-###lif===-71-###soif===-###eoif===-1552-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

			}
			else {AKA_mark("lis===-587-###sois===-13040-###eois===-130407-###lif===-68-###soif===-###eoif===-1457-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						if (AKA_mark("lis===593###sois===13196###eois===13205###lif===74###soif===1606###eoif===1615###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===593###sois===13196###eois===13205###lif===74###soif===1606###eoif===1615###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&*d == '%')) {
								AKA_mark("lis===594###sois===13213###eois===13229###lif===75###soif===1623###eoif===1639###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");d = skip(d + 1);

								if (AKA_mark("lis===595###sois===13238###eois===13243###lif===76###soif===1648###eoif===1653###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===595###sois===13238###eois===13243###lif===76###soif===1648###eoif===1653###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&store)) {
					/* Cant instrument this following code */
pz = va_arg(args, size_t *);
										if (AKA_mark("lis===597###sois===13290###eois===13303###lif===78###soif===1700###eoif===1713###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && ((AKA_mark("lis===597###sois===13290###eois===13297###lif===78###soif===1700###eoif===1707###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!ignore)	&&(AKA_mark("lis===597###sois===13301###eois===13303###lif===78###soif===1711###eoif===1713###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&pz))) {
						AKA_mark("lis===598###sois===13311###eois===13357###lif===79###soif===1721###eoif===1767###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");*pz = (size_t)json_object_get_string_len(obj);
					}
					else {AKA_mark("lis===-597-###sois===-13290-###eois===-1329013-###lif===-78-###soif===-###eoif===-1713-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

				}
				else {AKA_mark("lis===-595-###sois===-13238-###eois===-132385-###lif===-76-###soif===-###eoif===-1653-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

			}
			else {AKA_mark("lis===-593-###sois===-13196-###eois===-131969-###lif===-74-###soif===-###eoif===-1615-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						AKA_mark("lis===601###sois===13372###eois===13378###lif===82###soif===1782###eoif===1788###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");break;

					case 'n': if(c == 'n')AKA_mark("lis===602###sois===13381###eois===13390###lif===83###soif===1791###eoif===1800###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

						if (AKA_mark("lis===603###sois===13398###eois===13450###lif===84###soif===1808###eoif===1860###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && ((AKA_mark("lis===603###sois===13398###eois===13405###lif===84###soif===1808###eoif===1815###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!ignore)	&&(AKA_mark("lis===603###sois===13409###eois===13450###lif===84###soif===1819###eoif===1860###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!json_object_is_type(obj, json_type_null)))) {
				AKA_mark("lis===604###sois===13456###eois===13469###lif===85###soif===1866###eoif===1879###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto missfit;
			}
			else {AKA_mark("lis===-603-###sois===-13398-###eois===-1339852-###lif===-84-###soif===-###eoif===-1860-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						AKA_mark("lis===605###sois===13473###eois===13479###lif===86###soif===1883###eoif===1889###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");break;

					case 'b': if(c == 'b')AKA_mark("lis===606###sois===13482###eois===13491###lif===87###soif===1892###eoif===1901###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

			/* Cant instrument this following code */
if (store)
				pi = va_arg(args, int *);

						if (AKA_mark("lis===610###sois===13544###eois===13551###lif===91###soif===1954###eoif===1961###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===610###sois===13544###eois===13551###lif===91###soif===1954###eoif===1961###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!ignore)) {
								if (AKA_mark("lis===611###sois===13563###eois===13607###lif===92###soif===1973###eoif===2017###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===611###sois===13563###eois===13607###lif===92###soif===1973###eoif===2017###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!json_object_is_type(obj, json_type_boolean))) {
					AKA_mark("lis===612###sois===13614###eois===13627###lif===93###soif===2024###eoif===2037###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto missfit;
				}
				else {AKA_mark("lis===-611-###sois===-13563-###eois===-1356344-###lif===-92-###soif===-###eoif===-2017-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

								if (AKA_mark("lis===613###sois===13636###eois===13647###lif===94###soif===2046###eoif===2057###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && ((AKA_mark("lis===613###sois===13636###eois===13641###lif===94###soif===2046###eoif===2051###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&store)	&&(AKA_mark("lis===613###sois===13645###eois===13647###lif===94###soif===2055###eoif===2057###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&pi))) {
					AKA_mark("lis===614###sois===13654###eois===13689###lif===95###soif===2064###eoif===2099###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");*pi = json_object_get_boolean(obj);
				}
				else {AKA_mark("lis===-613-###sois===-13636-###eois===-1363611-###lif===-94-###soif===-###eoif===-2057-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

			}
			else {AKA_mark("lis===-610-###sois===-13544-###eois===-135447-###lif===-91-###soif===-###eoif===-1961-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						AKA_mark("lis===616###sois===13698###eois===13704###lif===97###soif===2108###eoif===2114###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");break;

					case 'i': if(c == 'i')AKA_mark("lis===617###sois===13707###eois===13716###lif===98###soif===2117###eoif===2126###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

			/* Cant instrument this following code */
if (store)
				pi = va_arg(args, int *);

						if (AKA_mark("lis===621###sois===13769###eois===13776###lif===102###soif===2179###eoif===2186###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===621###sois===13769###eois===13776###lif===102###soif===2179###eoif===2186###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!ignore)) {
								if (AKA_mark("lis===622###sois===13788###eois===13828###lif===103###soif===2198###eoif===2238###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===622###sois===13788###eois===13828###lif===103###soif===2198###eoif===2238###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!json_object_is_type(obj, json_type_int))) {
					AKA_mark("lis===623###sois===13835###eois===13848###lif===104###soif===2245###eoif===2258###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto missfit;
				}
				else {AKA_mark("lis===-622-###sois===-13788-###eois===-1378840-###lif===-103-###soif===-###eoif===-2238-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

								if (AKA_mark("lis===624###sois===13857###eois===13868###lif===105###soif===2267###eoif===2278###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && ((AKA_mark("lis===624###sois===13857###eois===13862###lif===105###soif===2267###eoif===2272###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&store)	&&(AKA_mark("lis===624###sois===13866###eois===13868###lif===105###soif===2276###eoif===2278###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&pi))) {
					AKA_mark("lis===625###sois===13875###eois===13906###lif===106###soif===2285###eoif===2316###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");*pi = json_object_get_int(obj);
				}
				else {AKA_mark("lis===-624-###sois===-13857-###eois===-1385711-###lif===-105-###soif===-###eoif===-2278-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

			}
			else {AKA_mark("lis===-621-###sois===-13769-###eois===-137697-###lif===-102-###soif===-###eoif===-2186-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						AKA_mark("lis===627###sois===13915###eois===13921###lif===108###soif===2325###eoif===2331###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");break;

					case 'I': if(c == 'I')AKA_mark("lis===628###sois===13924###eois===13933###lif===109###soif===2334###eoif===2343###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

			/* Cant instrument this following code */
if (store)
				pI = va_arg(args, int64_t *);

						if (AKA_mark("lis===632###sois===13990###eois===13997###lif===113###soif===2400###eoif===2407###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===632###sois===13990###eois===13997###lif===113###soif===2400###eoif===2407###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!ignore)) {
								if (AKA_mark("lis===633###sois===14009###eois===14049###lif===114###soif===2419###eoif===2459###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===633###sois===14009###eois===14049###lif===114###soif===2419###eoif===2459###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!json_object_is_type(obj, json_type_int))) {
					AKA_mark("lis===634###sois===14056###eois===14069###lif===115###soif===2466###eoif===2479###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto missfit;
				}
				else {AKA_mark("lis===-633-###sois===-14009-###eois===-1400940-###lif===-114-###soif===-###eoif===-2459-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

								if (AKA_mark("lis===635###sois===14078###eois===14089###lif===116###soif===2488###eoif===2499###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && ((AKA_mark("lis===635###sois===14078###eois===14083###lif===116###soif===2488###eoif===2493###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&store)	&&(AKA_mark("lis===635###sois===14087###eois===14089###lif===116###soif===2497###eoif===2499###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&pI))) {
					AKA_mark("lis===636###sois===14096###eois===14129###lif===117###soif===2506###eoif===2539###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");*pI = json_object_get_int64(obj);
				}
				else {AKA_mark("lis===-635-###sois===-14078-###eois===-1407811-###lif===-116-###soif===-###eoif===-2499-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

			}
			else {AKA_mark("lis===-632-###sois===-13990-###eois===-139907-###lif===-113-###soif===-###eoif===-2407-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						AKA_mark("lis===638###sois===14138###eois===14144###lif===119###soif===2548###eoif===2554###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");break;

					case 'f': if(c == 'f')AKA_mark("lis===639###sois===14147###eois===14156###lif===120###soif===2557###eoif===2566###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

					case 'F': if(c == 'F')AKA_mark("lis===640###sois===14159###eois===14168###lif===121###soif===2569###eoif===2578###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

			/* Cant instrument this following code */
if (store)
				pf = va_arg(args, double *);

						if (AKA_mark("lis===644###sois===14224###eois===14231###lif===125###soif===2634###eoif===2641###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===644###sois===14224###eois===14231###lif===125###soif===2634###eoif===2641###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!ignore)) {
								if (AKA_mark("lis===645###sois===14243###eois===14345###lif===126###soif===2653###eoif===2755###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===645###sois===14243###eois===14345###lif===126###soif===2653###eoif===2755###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!(json_object_is_type(obj, json_type_double) || (c == 'F' && json_object_is_type(obj, json_type_int))))) {
					AKA_mark("lis===646###sois===14352###eois===14365###lif===127###soif===2762###eoif===2775###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto missfit;
				}
				else {AKA_mark("lis===-645-###sois===-14243-###eois===-14243102-###lif===-126-###soif===-###eoif===-2755-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

								if (AKA_mark("lis===647###sois===14374###eois===14385###lif===128###soif===2784###eoif===2795###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && ((AKA_mark("lis===647###sois===14374###eois===14379###lif===128###soif===2784###eoif===2789###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&store)	&&(AKA_mark("lis===647###sois===14383###eois===14385###lif===128###soif===2793###eoif===2795###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&pf))) {
					AKA_mark("lis===648###sois===14392###eois===14426###lif===129###soif===2802###eoif===2836###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");*pf = json_object_get_double(obj);
				}
				else {AKA_mark("lis===-647-###sois===-14374-###eois===-1437411-###lif===-128-###soif===-###eoif===-2795-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

			}
			else {AKA_mark("lis===-644-###sois===-14224-###eois===-142247-###lif===-125-###soif===-###eoif===-2641-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						AKA_mark("lis===650###sois===14435###eois===14441###lif===131###soif===2845###eoif===2851###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");break;

					case 'o': if(c == 'o')AKA_mark("lis===651###sois===14444###eois===14453###lif===132###soif===2854###eoif===2863###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

					case 'O': if(c == 'O')AKA_mark("lis===652###sois===14456###eois===14465###lif===133###soif===2866###eoif===2875###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

						if (AKA_mark("lis===653###sois===14473###eois===14478###lif===134###soif===2883###eoif===2888###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===653###sois===14473###eois===14478###lif===134###soif===2883###eoif===2888###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&store)) {
				/* Cant instrument this following code */
po = va_arg(args, struct json_object **);
								if (AKA_mark("lis===655###sois===14536###eois===14549###lif===136###soif===2946###eoif===2959###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && ((AKA_mark("lis===655###sois===14536###eois===14543###lif===136###soif===2946###eoif===2953###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!ignore)	&&(AKA_mark("lis===655###sois===14547###eois===14549###lif===136###soif===2957###eoif===2959###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&po))) {
										if (AKA_mark("lis===656###sois===14562###eois===14570###lif===137###soif===2972###eoif===2980###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===656###sois===14562###eois===14570###lif===137###soif===2972###eoif===2980###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&c == 'O')) {
						AKA_mark("lis===657###sois===14578###eois===14605###lif===138###soif===2988###eoif===3015###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");obj = json_object_get(obj);
					}
					else {AKA_mark("lis===-656-###sois===-14562-###eois===-145628-###lif===-137-###soif===-###eoif===-2980-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

										AKA_mark("lis===658###sois===14611###eois===14621###lif===139###soif===3021###eoif===3031###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");*po = obj;

				}
				else {AKA_mark("lis===-655-###sois===-14536-###eois===-1453613-###lif===-136-###soif===-###eoif===-2959-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

			}
			else {AKA_mark("lis===-653-###sois===-14473-###eois===-144735-###lif===-134-###soif===-###eoif===-2888-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						AKA_mark("lis===661###sois===14636###eois===14642###lif===142###soif===3046###eoif===3052###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");break;

					case 'y': if(c == 'y')AKA_mark("lis===662###sois===14645###eois===14654###lif===143###soif===3055###eoif===3064###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

					case 'Y': if(c == 'Y')AKA_mark("lis===663###sois===14657###eois===14666###lif===144###soif===3067###eoif===3076###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

						if (AKA_mark("lis===664###sois===14674###eois===14679###lif===145###soif===3084###eoif===3089###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===664###sois===14674###eois===14679###lif===145###soif===3084###eoif===3089###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&store)) {
				/* Cant instrument this following code */
py = va_arg(args, uint8_t **);
				/* Cant instrument this following code */
pz = va_arg(args, size_t *);
			}
			else {AKA_mark("lis===-664-###sois===-14674-###eois===-146745-###lif===-145-###soif===-###eoif===-3089-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						if (AKA_mark("lis===668###sois===14763###eois===14770###lif===149###soif===3173###eoif===3180###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===668###sois===14763###eois===14770###lif===149###soif===3173###eoif===3180###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!ignore)) {
								if (AKA_mark("lis===669###sois===14782###eois===14793###lif===150###soif===3192###eoif===3203###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===669###sois===14782###eois===14793###lif===150###soif===3192###eoif===3203###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&obj == NULL)) {
										if (AKA_mark("lis===670###sois===14806###eois===14823###lif===151###soif===3216###eoif===3233###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (((AKA_mark("lis===670###sois===14806###eois===14811###lif===151###soif===3216###eoif===3221###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&store)	&&(AKA_mark("lis===670###sois===14815###eois===14817###lif===151###soif===3225###eoif===3227###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&py))	&&(AKA_mark("lis===670###sois===14821###eois===14823###lif===151###soif===3231###eoif===3233###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&pz))) {
												AKA_mark("lis===671###sois===14833###eois===14844###lif===152###soif===3243###eoif===3254###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");*py = NULL;

												AKA_mark("lis===672###sois===14851###eois===14859###lif===153###soif===3261###eoif===3269###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");*pz = 0;

					}
					else {AKA_mark("lis===-670-###sois===-14806-###eois===-1480617-###lif===-151-###soif===-###eoif===-3233-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

				}
				else {
										if (AKA_mark("lis===675###sois===14889###eois===14932###lif===156###soif===3299###eoif===3342###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===675###sois===14889###eois===14932###lif===156###soif===3299###eoif===3342###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!json_object_is_type(obj, json_type_string))) {
						AKA_mark("lis===676###sois===14940###eois===14953###lif===157###soif===3350###eoif===3363###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto missfit;
					}
					else {AKA_mark("lis===-675-###sois===-14889-###eois===-1488943-###lif===-156-###soif===-###eoif===-3342-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

										if (AKA_mark("lis===677###sois===14963###eois===14980###lif===158###soif===3373###eoif===3390###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (((AKA_mark("lis===677###sois===14963###eois===14968###lif===158###soif===3373###eoif===3378###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&store)	&&(AKA_mark("lis===677###sois===14972###eois===14974###lif===158###soif===3382###eoif===3384###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&py))	&&(AKA_mark("lis===677###sois===14978###eois===14980###lif===158###soif===3388###eoif===3390###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&pz))) {
												AKA_mark("lis===678###sois===14990###eois===15119###lif===159###soif===3400###eoif===3529###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");rc = decode_base64(
							json_object_get_string(obj),
							(size_t)json_object_get_string_len(obj),
							py, pz, c == 'y');

												if (AKA_mark("lis===682###sois===15130###eois===15132###lif===163###soif===3540###eoif===3542###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===682###sois===15130###eois===15132###lif===163###soif===3540###eoif===3542###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&rc)) {
							AKA_mark("lis===683###sois===15141###eois===15152###lif===164###soif===3551###eoif===3562###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto error;
						}
						else {AKA_mark("lis===-682-###sois===-15130-###eois===-151302-###lif===-163-###soif===-###eoif===-3542-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

					}
					else {AKA_mark("lis===-677-###sois===-14963-###eois===-1496317-###lif===-158-###soif===-###eoif===-3390-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

				}

			}
			else {AKA_mark("lis===-668-###sois===-14763-###eois===-147637-###lif===-149-###soif===-###eoif===-3180-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						AKA_mark("lis===687###sois===15174###eois===15180###lif===168###soif===3584###eoif===3590###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");break;


					case '[': if(c == '[')AKA_mark("lis===689###sois===15184###eois===15193###lif===170###soif===3594###eoif===3603###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

					case '{': if(c == '{')AKA_mark("lis===690###sois===15196###eois===15205###lif===171###soif===3606###eoif===3615###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

						if (AKA_mark("lis===691###sois===15213###eois===15217###lif===172###soif===3623###eoif===3627###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===691###sois===15213###eois===15217###lif===172###soif===3623###eoif===3627###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!top)) {
				AKA_mark("lis===692###sois===15223###eois===15235###lif===173###soif===3633###eoif===3645###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");top = stack;
			}
			else {
				if (AKA_mark("lis===693###sois===15248###eois===15276###lif===174###soif===3658###eoif===3686###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===693###sois===15248###eois===15276###lif===174###soif===3658###eoif===3686###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&++top  >= &stack[STACKCOUNT])) {
					AKA_mark("lis===694###sois===15282###eois===15296###lif===175###soif===3692###eoif===3706###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto too_deep;
				}
				else {AKA_mark("lis===-693-###sois===-15248-###eois===-1524828-###lif===-174-###soif===-###eoif===-3686-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}
			}


						AKA_mark("lis===696###sois===15301###eois===15316###lif===177###soif===3711###eoif===3726###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");top->acc = acc;

						AKA_mark("lis===697###sois===15320###eois===15340###lif===178###soif===3730###eoif===3750###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");top->type = xacc[0];

						AKA_mark("lis===698###sois===15344###eois===15359###lif===179###soif===3754###eoif===3769###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");top->index = 0;

						AKA_mark("lis===699###sois===15363###eois===15381###lif===180###soif===3773###eoif===3791###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");top->parent = obj;

						if (AKA_mark("lis===700###sois===15389###eois===15395###lif===181###soif===3799###eoif===3805###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===700###sois===15389###eois===15395###lif===181###soif===3799###eoif===3805###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&ignore)) {
				AKA_mark("lis===701###sois===15401###eois===15410###lif===182###soif===3811###eoif===3820###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");ignore++;
			}
			else {AKA_mark("lis===-700-###sois===-15389-###eois===-153896-###lif===-181-###soif===-###eoif===-3805-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						if (AKA_mark("lis===702###sois===15418###eois===15426###lif===183###soif===3828###eoif===3836###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===702###sois===15418###eois===15426###lif===183###soif===3828###eoif===3836###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&c == '[')) {
								if (AKA_mark("lis===703###sois===15438###eois===15445###lif===184###soif===3848###eoif===3855###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===703###sois===15438###eois===15445###lif===184###soif===3848###eoif===3855###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!ignore)) {
										if (AKA_mark("lis===704###sois===15458###eois===15500###lif===185###soif===3868###eoif===3910###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===704###sois===15458###eois===15500###lif===185###soif===3868###eoif===3910###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!json_object_is_type(obj, json_type_array))) {
						AKA_mark("lis===705###sois===15508###eois===15521###lif===186###soif===3918###eoif===3931###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto missfit;
					}
					else {AKA_mark("lis===-704-###sois===-15458-###eois===-1545842-###lif===-185-###soif===-###eoif===-3910-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

										AKA_mark("lis===706###sois===15527###eois===15575###lif===187###soif===3937###eoif===3985###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");top->count = (int)json_object_array_length(obj);

				}
				else {AKA_mark("lis===-703-###sois===-15438-###eois===-154387-###lif===-184-###soif===-###eoif===-3855-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

								AKA_mark("lis===708###sois===15586###eois===15600###lif===189###soif===3996###eoif===4010###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");xacc[0] = ']';

								AKA_mark("lis===709###sois===15605###eois===15629###lif===190###soif===4015###eoif===4039###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");acc = unpack_accept_arr;

			}
			else {
								if (AKA_mark("lis===711###sois===15650###eois===15657###lif===192###soif===4060###eoif===4067###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===711###sois===15650###eois===15657###lif===192###soif===4060###eoif===4067###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!ignore)) {
										if (AKA_mark("lis===712###sois===15670###eois===15713###lif===193###soif===4080###eoif===4123###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===712###sois===15670###eois===15713###lif===193###soif===4080###eoif===4123###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!json_object_is_type(obj, json_type_object))) {
						AKA_mark("lis===713###sois===15721###eois===15734###lif===194###soif===4131###eoif===4144###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto missfit;
					}
					else {AKA_mark("lis===-712-###sois===-15670-###eois===-1567043-###lif===-193-###soif===-###eoif===-4123-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

										AKA_mark("lis===714###sois===15740###eois===15784###lif===195###soif===4150###eoif===4194###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");top->count = json_object_object_length(obj);

				}
				else {AKA_mark("lis===-711-###sois===-15650-###eois===-156507-###lif===-192-###soif===-###eoif===-4067-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

								AKA_mark("lis===716###sois===15795###eois===15809###lif===197###soif===4205###eoif===4219###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");xacc[0] = '}';

								AKA_mark("lis===717###sois===15814###eois===15838###lif===198###soif===4224###eoif===4248###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");acc = unpack_accept_key;

								AKA_mark("lis===718###sois===15843###eois===15852###lif===199###soif===4253###eoif===4262###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");continue;

			}

						AKA_mark("lis===720###sois===15861###eois===15867###lif===201###soif===4271###eoif===4277###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");break;

					case '}': if(c == '}')AKA_mark("lis===721###sois===15870###eois===15879###lif===202###soif===4280###eoif===4289###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

					case ']': if(c == ']')AKA_mark("lis===722###sois===15882###eois===15891###lif===203###soif===4292###eoif===4301###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

						if (AKA_mark("lis===723###sois===15899###eois===15919###lif===204###soif===4309###eoif===4329###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && ((AKA_mark("lis===723###sois===15899###eois===15903###lif===204###soif===4309###eoif===4313###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!top)	||(AKA_mark("lis===723###sois===15907###eois===15919###lif===204###soif===4317###eoif===4329###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&c != xacc[0]))) {
				AKA_mark("lis===724###sois===15925###eois===15945###lif===205###soif===4335###eoif===4355###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto internal_error;
			}
			else {AKA_mark("lis===-723-###sois===-15899-###eois===-1589920-###lif===-204-###soif===-###eoif===-4329-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						AKA_mark("lis===725###sois===15949###eois===15964###lif===206###soif===4359###eoif===4374###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");acc = top->acc;

						AKA_mark("lis===726###sois===15968###eois===15988###lif===207###soif===4378###eoif===4398###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");xacc[0] = top->type;

						AKA_mark("lis===727###sois===15992###eois===16028###lif===208###soif===4402###eoif===4438###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");top = top == stack ? NULL : top - 1;

						if (AKA_mark("lis===728###sois===16036###eois===16042###lif===209###soif===4446###eoif===4452###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===728###sois===16036###eois===16042###lif===209###soif===4446###eoif===4452###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&ignore)) {
				AKA_mark("lis===729###sois===16048###eois===16057###lif===210###soif===4458###eoif===4467###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");ignore--;
			}
			else {AKA_mark("lis===-728-###sois===-16036-###eois===-160366-###lif===-209-###soif===-###eoif===-4452-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						AKA_mark("lis===730###sois===16061###eois===16067###lif===211###soif===4471###eoif===4477###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");break;

					case '!': if(c == '!')AKA_mark("lis===731###sois===16070###eois===16079###lif===212###soif===4480###eoif===4489###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

						if (AKA_mark("lis===732###sois===16087###eois===16100###lif===213###soif===4497###eoif===4510###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===732###sois===16087###eois===16100###lif===213###soif===4497###eoif===4510###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&*d != xacc[0])) {
				AKA_mark("lis===733###sois===16106###eois===16129###lif===214###soif===4516###eoif===4539###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto invalid_character;
			}
			else {AKA_mark("lis===-732-###sois===-16087-###eois===-1608713-###lif===-213-###soif===-###eoif===-4510-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						if (AKA_mark("lis===734###sois===16137###eois===16172###lif===215###soif===4547###eoif===4582###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && ((AKA_mark("lis===734###sois===16137###eois===16144###lif===215###soif===4547###eoif===4554###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!ignore)	&&(AKA_mark("lis===734###sois===16148###eois===16172###lif===215###soif===4558###eoif===4582###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&top->index != top->count))) {
				AKA_mark("lis===735###sois===16178###eois===16194###lif===216###soif===4588###eoif===4604###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto incomplete;
			}
			else {AKA_mark("lis===-734-###sois===-16137-###eois===-1613735-###lif===-215-###soif===-###eoif===-4582-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

			/*@fallthrough@*/
					case '*': if(c == '*')AKA_mark("lis===737###sois===16218###eois===16227###lif===218###soif===4628###eoif===4637###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

						AKA_mark("lis===738###sois===16231###eois===16242###lif===219###soif===4641###eoif===4652###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");acc = xacc;

						AKA_mark("lis===739###sois===16246###eois===16255###lif===220###soif===4656###eoif===4665###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");continue;

					default: if(c != 's' && c != 'n' && c != 'b' && c != 'i' && c != 'I' && c != 'f' && c != 'F' && c != 'o' && c != 'O' && c != 'y' && c != 'Y' && c != '[' && c != '{' && c != '}' && c != ']' && c != '!' && c != '*')AKA_mark("lis===740###sois===16258###eois===16266###lif===221###soif===4668###eoif===4676###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

						AKA_mark("lis===741###sois===16270###eois===16290###lif===222###soif===4680###eoif===4700###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto internal_error;

		}

				AKA_mark("lis===743###sois===16305###eois===16312###lif===224###soif===4715###eoif===4722###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");switch(xacc[0]){
					case 0: if(xacc[0] == 0)AKA_mark("lis===744###sois===16318###eois===16325###lif===225###soif===4728###eoif===4735###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

						if (AKA_mark("lis===745###sois===16333###eois===16336###lif===226###soif===4743###eoif===4746###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===745###sois===16333###eois===16336###lif===226###soif===4743###eoif===4746###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&top)) {
				AKA_mark("lis===746###sois===16342###eois===16362###lif===227###soif===4752###eoif===4772###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto internal_error;
			}
			else {AKA_mark("lis===-745-###sois===-16333-###eois===-163333-###lif===-226-###soif===-###eoif===-4746-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						if (AKA_mark("lis===747###sois===16370###eois===16372###lif===228###soif===4780###eoif===4782###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===747###sois===16370###eois===16372###lif===228###soif===4780###eoif===4782###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&*d)) {
				AKA_mark("lis===748###sois===16378###eois===16401###lif===229###soif===4788###eoif===4811###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto invalid_character;
			}
			else {AKA_mark("lis===-747-###sois===-16370-###eois===-163702-###lif===-228-###soif===-###eoif===-4782-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						AKA_mark("lis===749###sois===16405###eois===16414###lif===230###soif===4815###eoif===4824###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");return 0;

					case ']': if(xacc[0] == ']')AKA_mark("lis===750###sois===16417###eois===16426###lif===231###soif===4827###eoif===4836###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

						if (AKA_mark("lis===751###sois===16434###eois===16441###lif===232###soif===4844###eoif===4851###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===751###sois===16434###eois===16441###lif===232###soif===4844###eoif===4851###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&!ignore)) {
								AKA_mark("lis===752###sois===16449###eois===16485###lif===233###soif===4859###eoif===4895###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");key = strchr(unpack_accept_arr, *d);

								if (AKA_mark("lis===753###sois===16494###eois===16525###lif===234###soif===4904###eoif===4935###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && ((AKA_mark("lis===753###sois===16494###eois===16497###lif===234###soif===4904###eoif===4907###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&key)	&&(AKA_mark("lis===753###sois===16501###eois===16525###lif===234###soif===4911###eoif===4935###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&key >= unpack_accept_any))) {
										if (AKA_mark("lis===754###sois===16538###eois===16562###lif===235###soif===4948###eoif===4972###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===754###sois===16538###eois===16562###lif===235###soif===4948###eoif===4972###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&top->index >= top->count)) {
						AKA_mark("lis===755###sois===16570###eois===16588###lif===236###soif===4980###eoif===4998###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto out_of_range;
					}
					else {AKA_mark("lis===-754-###sois===-16538-###eois===-1653824-###lif===-235-###soif===-###eoif===-4972-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

										AKA_mark("lis===756###sois===16594###eois===16653###lif===237###soif===5004###eoif===5063###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");obj = json_object_array_get_idx(top->parent, top->index++);

				}
				else {AKA_mark("lis===-753-###sois===-16494-###eois===-1649431-###lif===-234-###soif===-###eoif===-4935-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

			}
			else {AKA_mark("lis===-751-###sois===-16434-###eois===-164347-###lif===-232-###soif===-###eoif===-4851-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						AKA_mark("lis===759###sois===16668###eois===16674###lif===240###soif===5078###eoif===5084###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");break;

					case ':': if(xacc[0] == ':')AKA_mark("lis===760###sois===16677###eois===16686###lif===241###soif===5087###eoif===5096###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

						AKA_mark("lis===761###sois===16690###eois===16714###lif===242###soif===5100###eoif===5124###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");acc = unpack_accept_key;

						AKA_mark("lis===762###sois===16718###eois===16732###lif===243###soif===5128###eoif===5142###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");xacc[0] = '}';

						if (AKA_mark("lis===763###sois===16740###eois===16746###lif===244###soif===5150###eoif===5156###ifc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)") && (AKA_mark("lis===763###sois===16740###eois===16746###lif===244###soif===5150###eoif===5156###isc===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)")&&ignore)) {
				AKA_mark("lis===764###sois===16752###eois===16761###lif===245###soif===5162###eoif===5171###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");ignore--;
			}
			else {AKA_mark("lis===-763-###sois===-16740-###eois===-167406-###lif===-244-###soif===-###eoif===-5156-###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");}

						AKA_mark("lis===765###sois===16765###eois===16771###lif===246###soif===5175###eoif===5181###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");break;

					default: if(xacc[0] != 0 && xacc[0] != ']' && xacc[0] != ':')AKA_mark("lis===766###sois===16774###eois===16782###lif===247###soif===5184###eoif===5192###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");

						AKA_mark("lis===767###sois===16786###eois===16806###lif===248###soif===5196###eoif===5216###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto internal_error;

		}

	}

	truncated:
	AKA_mark("lis===771###sois===16826###eois===16857###lif===252###soif===5236###eoif===5267###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");rc = wrap_json_error_truncated;

		AKA_mark("lis===772###sois===16859###eois===16870###lif===253###soif===5269###eoif===5280###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto error;

	internal_error:
	AKA_mark("lis===774###sois===16888###eois===16924###lif===255###soif===5298###eoif===5334###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");rc = wrap_json_error_internal_error;

		AKA_mark("lis===775###sois===16926###eois===16937###lif===256###soif===5336###eoif===5347###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto error;

	invalid_character:
	AKA_mark("lis===777###sois===16958###eois===16997###lif===258###soif===5368###eoif===5407###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");rc = wrap_json_error_invalid_character;

		AKA_mark("lis===778###sois===16999###eois===17010###lif===259###soif===5409###eoif===5420###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto error;

	too_deep:
	AKA_mark("lis===780###sois===17022###eois===17052###lif===261###soif===5432###eoif===5462###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");rc = wrap_json_error_too_deep;

		AKA_mark("lis===781###sois===17054###eois===17065###lif===262###soif===5464###eoif===5475###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto error;

	null_spec:
	AKA_mark("lis===783###sois===17078###eois===17109###lif===264###soif===5488###eoif===5519###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");rc = wrap_json_error_null_spec;

		AKA_mark("lis===784###sois===17111###eois===17122###lif===265###soif===5521###eoif===5532###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto error;

	null_key:
	AKA_mark("lis===786###sois===17134###eois===17164###lif===267###soif===5544###eoif===5574###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");rc = wrap_json_error_null_key;

		AKA_mark("lis===787###sois===17166###eois===17177###lif===268###soif===5576###eoif===5587###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto error;

	out_of_range:
	AKA_mark("lis===789###sois===17193###eois===17227###lif===270###soif===5603###eoif===5637###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");rc = wrap_json_error_out_of_range;

		AKA_mark("lis===790###sois===17229###eois===17240###lif===271###soif===5639###eoif===5650###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto error;

	incomplete:
	AKA_mark("lis===792###sois===17254###eois===17286###lif===273###soif===5664###eoif===5696###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");rc = wrap_json_error_incomplete;

		AKA_mark("lis===793###sois===17288###eois===17299###lif===274###soif===5698###eoif===5709###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto error;

	missfit:
	AKA_mark("lis===795###sois===17310###eois===17344###lif===276###soif===5720###eoif===5754###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");rc = wrap_json_error_missfit_type;

		AKA_mark("lis===796###sois===17346###eois===17360###lif===277###soif===5756###eoif===5770###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto errorfit;

	key_not_found:
	AKA_mark("lis===798###sois===17377###eois===17412###lif===279###soif===5787###eoif===5822###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");rc = wrap_json_error_key_not_found;

		AKA_mark("lis===799###sois===17414###eois===17425###lif===280###soif===5824###eoif===5835###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");goto error;

	errorfit:
	AKA_mark("lis===801###sois===17437###eois===17445###lif===282###soif===5847###eoif===5855###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");d = fit;

	error:
	AKA_mark("lis===803###sois===17454###eois===17487###lif===284###soif===5864###eoif===5897###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");rc = rc | (int)((d - desc) << 4);

		AKA_mark("lis===804###sois===17489###eois===17500###lif===285###soif===5899###eoif===5910###ins===true###function===./app-framework-binder/src/wrap-json.c/vunpack(struct json_object*,const char*,va_list,int)");return -rc;

}

/** Instrumented function wrap_json_vcheck(struct json_object*,const char*,va_list) */
int wrap_json_vcheck(struct json_object *object, const char *desc, va_list args)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_vcheck(struct json_object*,const char*,va_list)");AKA_fCall++;
		AKA_mark("lis===809###sois===17588###eois===17626###lif===2###soif===84###eoif===122###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vcheck(struct json_object*,const char*,va_list)");return vunpack(object, desc, args, 0);

}

/** Instrumented function wrap_json_check(struct json_object*,const char*) */
int wrap_json_check(struct json_object *object, const char *desc, ...)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_check(struct json_object*,const char*)");AKA_fCall++;
		AKA_mark("lis===814###sois===17704###eois===17711###lif===2###soif===74###eoif===81###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_check(struct json_object*,const char*)");int rc;

		AKA_mark("lis===815###sois===17713###eois===17726###lif===3###soif===83###eoif===96###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_check(struct json_object*,const char*)");va_list args;


		AKA_mark("lis===817###sois===17729###eois===17750###lif===5###soif===99###eoif===120###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_check(struct json_object*,const char*)");va_start(args, desc);

		AKA_mark("lis===818###sois===17752###eois===17794###lif===6###soif===122###eoif===164###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_check(struct json_object*,const char*)");rc = wrap_json_vcheck(object, desc, args);

		AKA_mark("lis===819###sois===17796###eois===17809###lif===7###soif===166###eoif===179###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_check(struct json_object*,const char*)");va_end(args);

		AKA_mark("lis===820###sois===17811###eois===17821###lif===8###soif===181###eoif===191###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_check(struct json_object*,const char*)");return rc;

}

/** Instrumented function wrap_json_vmatch(struct json_object*,const char*,va_list) */
int wrap_json_vmatch(struct json_object *object, const char *desc, va_list args)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_vmatch(struct json_object*,const char*,va_list)");AKA_fCall++;
		AKA_mark("lis===825###sois===17909###eois===17948###lif===2###soif===84###eoif===123###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vmatch(struct json_object*,const char*,va_list)");return !vunpack(object, desc, args, 0);

}

/** Instrumented function wrap_json_match(struct json_object*,const char*) */
int wrap_json_match(struct json_object *object, const char *desc, ...)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_match(struct json_object*,const char*)");AKA_fCall++;
		AKA_mark("lis===830###sois===18026###eois===18033###lif===2###soif===74###eoif===81###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_match(struct json_object*,const char*)");int rc;

		AKA_mark("lis===831###sois===18035###eois===18048###lif===3###soif===83###eoif===96###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_match(struct json_object*,const char*)");va_list args;


		AKA_mark("lis===833###sois===18051###eois===18072###lif===5###soif===99###eoif===120###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_match(struct json_object*,const char*)");va_start(args, desc);

		AKA_mark("lis===834###sois===18074###eois===18116###lif===6###soif===122###eoif===164###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_match(struct json_object*,const char*)");rc = wrap_json_vmatch(object, desc, args);

		AKA_mark("lis===835###sois===18118###eois===18131###lif===7###soif===166###eoif===179###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_match(struct json_object*,const char*)");va_end(args);

		AKA_mark("lis===836###sois===18133###eois===18143###lif===8###soif===181###eoif===191###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_match(struct json_object*,const char*)");return rc;

}

/** Instrumented function wrap_json_vunpack(struct json_object*,const char*,va_list) */
int wrap_json_vunpack(struct json_object *object, const char *desc, va_list args)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_vunpack(struct json_object*,const char*,va_list)");AKA_fCall++;
		AKA_mark("lis===841###sois===18232###eois===18270###lif===2###soif===85###eoif===123###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_vunpack(struct json_object*,const char*,va_list)");return vunpack(object, desc, args, 1);

}

/** Instrumented function wrap_json_unpack(struct json_object*,const char*) */
int wrap_json_unpack(struct json_object *object, const char *desc, ...)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_unpack(struct json_object*,const char*)");AKA_fCall++;
		AKA_mark("lis===846###sois===18349###eois===18356###lif===2###soif===75###eoif===82###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_unpack(struct json_object*,const char*)");int rc;

		AKA_mark("lis===847###sois===18358###eois===18371###lif===3###soif===84###eoif===97###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_unpack(struct json_object*,const char*)");va_list args;


		AKA_mark("lis===849###sois===18374###eois===18395###lif===5###soif===100###eoif===121###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_unpack(struct json_object*,const char*)");va_start(args, desc);

		AKA_mark("lis===850###sois===18397###eois===18433###lif===6###soif===123###eoif===159###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_unpack(struct json_object*,const char*)");rc = vunpack(object, desc, args, 1);

		AKA_mark("lis===851###sois===18435###eois===18448###lif===7###soif===161###eoif===174###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_unpack(struct json_object*,const char*)");va_end(args);

		AKA_mark("lis===852###sois===18450###eois===18460###lif===8###soif===176###eoif===186###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_unpack(struct json_object*,const char*)");return rc;

}

/** Instrumented function object_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*) */
static void object_for_all(struct json_object *object, void (*callback)(void*,struct json_object*,const char*), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/object_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");AKA_fCall++;
		AKA_mark("lis===857###sois===18594###eois===18658###lif===2###soif===130###eoif===194###ins===true###function===./app-framework-binder/src/wrap-json.c/object_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");struct json_object_iterator it = json_object_iter_begin(object);

		AKA_mark("lis===858###sois===18660###eois===18723###lif===3###soif===196###eoif===259###ins===true###function===./app-framework-binder/src/wrap-json.c/object_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");struct json_object_iterator end = json_object_iter_end(object);

		while (AKA_mark("lis===859###sois===18732###eois===18766###lif===4###soif===268###eoif===302###ifc===true###function===./app-framework-binder/src/wrap-json.c/object_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)") && (AKA_mark("lis===859###sois===18732###eois===18766###lif===4###soif===268###eoif===302###isc===true###function===./app-framework-binder/src/wrap-json.c/object_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)")&&!json_object_iter_equal(&it, &end))) {
				AKA_mark("lis===860###sois===18772###eois===18857###lif===5###soif===308###eoif===393###ins===true###function===./app-framework-binder/src/wrap-json.c/object_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");callback(closure, json_object_iter_peek_value(&it), json_object_iter_peek_name(&it));

				AKA_mark("lis===861###sois===18860###eois===18887###lif===6###soif===396###eoif===423###ins===true###function===./app-framework-binder/src/wrap-json.c/object_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");json_object_iter_next(&it);

	}

}

/** Instrumented function array_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*) */
static void array_for_all(struct json_object *object, void (*callback)(void*,struct json_object*), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/array_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)");AKA_fCall++;
		AKA_mark("lis===867###sois===19011###eois===19057###lif===2###soif===117###eoif===163###ins===true###function===./app-framework-binder/src/wrap-json.c/array_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)");int n = (int)json_object_array_length(object);

		AKA_mark("lis===868###sois===19059###eois===19069###lif===3###soif===165###eoif===175###ins===true###function===./app-framework-binder/src/wrap-json.c/array_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)");int i = 0;

		while (AKA_mark("lis===869###sois===19077###eois===19082###lif===4###soif===183###eoif===188###ifc===true###function===./app-framework-binder/src/wrap-json.c/array_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)") && (AKA_mark("lis===869###sois===19077###eois===19082###lif===4###soif===183###eoif===188###isc===true###function===./app-framework-binder/src/wrap-json.c/array_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)")&&i < n)) {
		AKA_mark("lis===870###sois===19086###eois===19144###lif===5###soif===192###eoif===250###ins===true###function===./app-framework-binder/src/wrap-json.c/array_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)");callback(closure, json_object_array_get_idx(object, i++));
	}

}

/** Instrumented function wrap_json_optarray_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*) */
void wrap_json_optarray_for_all(struct json_object *object, void (*callback)(void*,struct json_object*), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_optarray_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)");AKA_fCall++;
		if (AKA_mark("lis===875###sois===19275###eois===19319###lif===2###soif===127###eoif===171###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_optarray_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)") && (AKA_mark("lis===875###sois===19275###eois===19319###lif===2###soif===127###eoif===171###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_optarray_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)")&&json_object_is_type(object, json_type_array))) {
		AKA_mark("lis===876###sois===19323###eois===19364###lif===3###soif===175###eoif===216###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_optarray_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)");array_for_all(object, callback, closure);
	}
	else {
		AKA_mark("lis===878###sois===19373###eois===19399###lif===5###soif===225###eoif===251###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_optarray_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)");callback(closure, object);
	}

}

/** Instrumented function wrap_json_array_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*) */
void wrap_json_array_for_all(struct json_object *object, void (*callback)(void*,struct json_object*), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_array_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)");AKA_fCall++;
		if (AKA_mark("lis===883###sois===19527###eois===19571###lif===2###soif===124###eoif===168###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_array_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)") && (AKA_mark("lis===883###sois===19527###eois===19571###lif===2###soif===124###eoif===168###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_array_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)")&&json_object_is_type(object, json_type_array))) {
		AKA_mark("lis===884###sois===19575###eois===19616###lif===3###soif===172###eoif===213###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_array_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)");array_for_all(object, callback, closure);
	}
	else {AKA_mark("lis===-883-###sois===-19527-###eois===-1952744-###lif===-2-###soif===-###eoif===-168-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_array_for_all(struct json_object*,void(*callback)(void*,struct json_object*),void*)");}

}

/** Instrumented function wrap_json_object_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*) */
void wrap_json_object_for_all(struct json_object *object, void (*callback)(void*,struct json_object*,const char*), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_object_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");AKA_fCall++;
		if (AKA_mark("lis===889###sois===19757###eois===19802###lif===2###soif===137###eoif===182###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)") && (AKA_mark("lis===889###sois===19757###eois===19802###lif===2###soif===137###eoif===182###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)")&&json_object_is_type(object, json_type_object))) {
		AKA_mark("lis===890###sois===19806###eois===19848###lif===3###soif===186###eoif===228###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");object_for_all(object, callback, closure);
	}
	else {AKA_mark("lis===-889-###sois===-19757-###eois===-1975745-###lif===-2-###soif===-###eoif===-182-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");}

}

/** Instrumented function wrap_json_optobject_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*) */
void wrap_json_optobject_for_all(struct json_object *object, void (*callback)(void*,struct json_object*,const char*), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_optobject_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");AKA_fCall++;
		if (AKA_mark("lis===895###sois===19992###eois===20037###lif===2###soif===140###eoif===185###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_optobject_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)") && (AKA_mark("lis===895###sois===19992###eois===20037###lif===2###soif===140###eoif===185###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_optobject_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)")&&json_object_is_type(object, json_type_object))) {
		AKA_mark("lis===896###sois===20041###eois===20083###lif===3###soif===189###eoif===231###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_optobject_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");object_for_all(object, callback, closure);
	}
	else {
		AKA_mark("lis===898###sois===20092###eois===20124###lif===5###soif===240###eoif===272###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_optobject_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");callback(closure, object, NULL);
	}

}

/** Instrumented function wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*) */
void wrap_json_for_all(struct json_object *object, void (*callback)(void*,struct json_object*,const char*), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");AKA_fCall++;
		if (AKA_mark("lis===903###sois===20258###eois===20265###lif===2###soif===130###eoif===137###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)") && (AKA_mark("lis===903###sois===20258###eois===20265###lif===2###soif===130###eoif===137###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)")&&!object)) {
		;
	}
	else {
		if (AKA_mark("lis===905###sois===20297###eois===20342###lif===4###soif===169###eoif===214###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)") && (AKA_mark("lis===905###sois===20297###eois===20342###lif===4###soif===169###eoif===214###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)")&&json_object_is_type(object, json_type_object))) {
			AKA_mark("lis===906###sois===20346###eois===20388###lif===5###soif===218###eoif===260###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");object_for_all(object, callback, closure);
		}
		else {
			if (AKA_mark("lis===907###sois===20399###eois===20444###lif===6###soif===271###eoif===316###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)") && (AKA_mark("lis===907###sois===20399###eois===20444###lif===6###soif===271###eoif===316###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)")&&!json_object_is_type(object, json_type_array))) {
				AKA_mark("lis===908###sois===20448###eois===20480###lif===7###soif===320###eoif===352###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");callback(closure, object, NULL);
			}
			else {
						AKA_mark("lis===910###sois===20491###eois===20537###lif===9###soif===363###eoif===409###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");int n = (int)json_object_array_length(object);

						AKA_mark("lis===911###sois===20540###eois===20550###lif===10###soif===412###eoif===422###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");int i = 0;

						while (AKA_mark("lis===912###sois===20559###eois===20564###lif===11###soif===431###eoif===436###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)") && (AKA_mark("lis===912###sois===20559###eois===20564###lif===11###soif===431###eoif===436###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)")&&i < n)) {
					AKA_mark("lis===913###sois===20569###eois===20633###lif===12###soif===441###eoif===505###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_for_all(struct json_object*,void(*callback)(void*,struct json_object*,const char*),void*)");callback(closure, json_object_array_get_idx(object, i++), NULL);
				}

	}
		}
	}

}

/**
 * Clones the 'object' for the depth 'subdepth'. The object 'object' is
 * duplicated and all its fields are cloned with the depth 'subdepth'.
 *
 * @param object the object to clone. MUST be an **object**.
 * @param subdepth the depth to use when cloning the fields of the object.
 *
 * @return the cloned object.
 */
/** Instrumented function clone_object(struct json_object*,int) */
static struct json_object *clone_object(struct json_object *object, int subdepth)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/clone_object(struct json_object*,int)");AKA_fCall++;
		AKA_mark("lis===928###sois===21048###eois===21097###lif===2###soif===85###eoif===134###ins===true###function===./app-framework-binder/src/wrap-json.c/clone_object(struct json_object*,int)");struct json_object *r = json_object_new_object();

		AKA_mark("lis===929###sois===21099###eois===21163###lif===3###soif===136###eoif===200###ins===true###function===./app-framework-binder/src/wrap-json.c/clone_object(struct json_object*,int)");struct json_object_iterator it = json_object_iter_begin(object);

		AKA_mark("lis===930###sois===21165###eois===21228###lif===4###soif===202###eoif===265###ins===true###function===./app-framework-binder/src/wrap-json.c/clone_object(struct json_object*,int)");struct json_object_iterator end = json_object_iter_end(object);

		while (AKA_mark("lis===931###sois===21237###eois===21271###lif===5###soif===274###eoif===308###ifc===true###function===./app-framework-binder/src/wrap-json.c/clone_object(struct json_object*,int)") && (AKA_mark("lis===931###sois===21237###eois===21271###lif===5###soif===274###eoif===308###isc===true###function===./app-framework-binder/src/wrap-json.c/clone_object(struct json_object*,int)")&&!json_object_iter_equal(&it, &end))) {
				AKA_mark("lis===932###sois===21277###eois===21409###lif===6###soif===314###eoif===446###ins===true###function===./app-framework-binder/src/wrap-json.c/clone_object(struct json_object*,int)");json_object_object_add(r,
			json_object_iter_peek_name(&it),
			wrap_json_clone_depth(json_object_iter_peek_value(&it), subdepth));

				AKA_mark("lis===935###sois===21412###eois===21439###lif===9###soif===449###eoif===476###ins===true###function===./app-framework-binder/src/wrap-json.c/clone_object(struct json_object*,int)");json_object_iter_next(&it);

	}

		AKA_mark("lis===937###sois===21444###eois===21453###lif===11###soif===481###eoif===490###ins===true###function===./app-framework-binder/src/wrap-json.c/clone_object(struct json_object*,int)");return r;

}

/**
 * Clones the 'array' for the depth 'subdepth'. The array 'array' is
 * duplicated and all its fields are cloned with the depth 'subdepth'.
 *
 * @param array the array to clone. MUST be an **array**.
 * @param subdepth the depth to use when cloning the items of the array.
 *
 * @return the cloned array.
 */
/** Instrumented function clone_array(struct json_object*,int) */
static struct json_object *clone_array(struct json_object *array, int subdepth)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/clone_array(struct json_object*,int)");AKA_fCall++;
		AKA_mark("lis===951###sois===21854###eois===21899###lif===2###soif===83###eoif===128###ins===true###function===./app-framework-binder/src/wrap-json.c/clone_array(struct json_object*,int)");int n = (int)json_object_array_length(array);

		AKA_mark("lis===952###sois===21901###eois===21949###lif===3###soif===130###eoif===178###ins===true###function===./app-framework-binder/src/wrap-json.c/clone_array(struct json_object*,int)");struct json_object *r = json_object_new_array();

		while (AKA_mark("lis===953###sois===21958###eois===21959###lif===4###soif===187###eoif===188###ifc===true###function===./app-framework-binder/src/wrap-json.c/clone_array(struct json_object*,int)") && (AKA_mark("lis===953###sois===21958###eois===21959###lif===4###soif===187###eoif===188###isc===true###function===./app-framework-binder/src/wrap-json.c/clone_array(struct json_object*,int)")&&n)) {
				AKA_mark("lis===954###sois===21965###eois===21969###lif===5###soif===194###eoif===198###ins===true###function===./app-framework-binder/src/wrap-json.c/clone_array(struct json_object*,int)");n--;

				AKA_mark("lis===955###sois===21972###eois===22077###lif===6###soif===201###eoif===306###ins===true###function===./app-framework-binder/src/wrap-json.c/clone_array(struct json_object*,int)");json_object_array_put_idx(r, n,
			wrap_json_clone_depth(json_object_array_get_idx(array, n), subdepth));

	}

		AKA_mark("lis===958###sois===22082###eois===22091###lif===9###soif===311###eoif===320###ins===true###function===./app-framework-binder/src/wrap-json.c/clone_array(struct json_object*,int)");return r;

}

/**
 * Clones any json 'item' for the depth 'depth'. The item is duplicated
 * and if 'depth' is not zero, its contents is recursively cloned with
 * the depth 'depth' - 1.
 *
 * Be aware that this implementation doesn't copies the primitive json
 * items (numbers, nulls, booleans, strings) but instead increments their
 * use count. This can cause issues with newer versions of libjson-c that
 * now unfortunately allows to change their values.
 *
 * @param item the item to clone. Can be of any kind.
 * @param depth the depth to use when cloning composites: object or arrays.
 *
 * @return the cloned array.
 *
 * @see wrap_json_clone
 * @see wrap_json_clone_deep
 */
/** Instrumented function wrap_json_clone_depth(struct json_object*,int) */
struct json_object *wrap_json_clone_depth(struct json_object *item, int depth)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_clone_depth(struct json_object*,int)");AKA_fCall++;
		if (AKA_mark("lis===981###sois===22853###eois===22858###lif===2###soif===86###eoif===91###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_clone_depth(struct json_object*,int)") && (AKA_mark("lis===981###sois===22853###eois===22858###lif===2###soif===86###eoif===91###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_clone_depth(struct json_object*,int)")&&depth)) {
				AKA_mark("lis===982###sois===22872###eois===22898###lif===3###soif===105###eoif===131###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_clone_depth(struct json_object*,int)");switch(json_object_get_type(item)){
					case json_type_object: if(json_object_get_type(item) == json_type_object)AKA_mark("lis===983###sois===22904###eois===22926###lif===4###soif===137###eoif===159###function===./app-framework-binder/src/wrap-json.c/wrap_json_clone_depth(struct json_object*,int)");

						AKA_mark("lis===984###sois===22930###eois===22967###lif===5###soif===163###eoif===200###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_clone_depth(struct json_object*,int)");return clone_object(item, depth - 1);

					case json_type_array: if(json_object_get_type(item) == json_type_array)AKA_mark("lis===985###sois===22970###eois===22991###lif===6###soif===203###eoif===224###function===./app-framework-binder/src/wrap-json.c/wrap_json_clone_depth(struct json_object*,int)");

						AKA_mark("lis===986###sois===22995###eois===23031###lif===7###soif===228###eoif===264###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_clone_depth(struct json_object*,int)");return clone_array(item, depth - 1);

					default: if(json_object_get_type(item) != json_type_object && json_object_get_type(item) != json_type_array)AKA_mark("lis===987###sois===23034###eois===23042###lif===8###soif===267###eoif===275###function===./app-framework-binder/src/wrap-json.c/wrap_json_clone_depth(struct json_object*,int)");

						AKA_mark("lis===988###sois===23046###eois===23052###lif===9###soif===279###eoif===285###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_clone_depth(struct json_object*,int)");break;

		}

	}
	else {AKA_mark("lis===-981-###sois===-22853-###eois===-228535-###lif===-2-###soif===-###eoif===-91-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_clone_depth(struct json_object*,int)");}

		AKA_mark("lis===991###sois===23061###eois===23090###lif===12###soif===294###eoif===323###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_clone_depth(struct json_object*,int)");return json_object_get(item);

}

/**
 * Clones the 'object': returns a copy of it. But doesn't clones
 * the content. Synonym of wrap_json_clone_depth(object, 1).
 *
 * Be aware that this implementation doesn't clones content that is deeper
 * than 1 but it does link these contents to the original object and
 * increments their use count. So, everything deeper that 1 is still available.
 *
 * @param object the object to clone
 *
 * @return a copy of the object.
 *
 * @see wrap_json_clone_depth
 * @see wrap_json_clone_deep
 */
/** Instrumented function wrap_json_clone(struct json_object*) */
struct json_object *wrap_json_clone(struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_clone(struct json_object*)");AKA_fCall++;
		AKA_mark("lis===1011###sois===23660###eois===23700###lif===2###soif===67###eoif===107###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_clone(struct json_object*)");return wrap_json_clone_depth(object, 1);

}

/**
 * Clones the 'object': returns a copy of it. Also clones all
 * the content recursively. Synonym of wrap_json_clone_depth(object, INT_MAX).
 *
 * @param object the object to clone
 *
 * @return a copy of the object.
 *
 * @see wrap_json_clone_depth
 * @see wrap_json_clone
 */
/** Instrumented function wrap_json_clone_deep(struct json_object*) */
struct json_object *wrap_json_clone_deep(struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_clone_deep(struct json_object*)");AKA_fCall++;
		AKA_mark("lis===1027###sois===24058###eois===24104###lif===2###soif===72###eoif===118###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_clone_deep(struct json_object*)");return wrap_json_clone_depth(object, INT_MAX);

}

/**
 * Adds the items of the object 'added' to the object 'dest'.
 *
 * @param dest the object to complete this object is modified
 * @added the object containing fields to add
 *
 * @return the destination object 'dest'
 *
 * @example wrap_json_object_add({"a":"a"},{"X":"X"}) -> {"a":"a","X":"X"}
 */
/** Instrumented function wrap_json_object_add(struct json_object*,struct json_object*) */
struct json_object *wrap_json_object_add(struct json_object *dest, struct json_object *added)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_object_add(struct json_object*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===1042###sois===24508###eois===24544###lif===2###soif===97###eoif===133###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_add(struct json_object*,struct json_object*)");struct json_object_iterator it, end;

		if (AKA_mark("lis===1043###sois===24550###eois===24641###lif===3###soif===139###eoif===230###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_add(struct json_object*,struct json_object*)") && ((AKA_mark("lis===1043###sois===24550###eois===24593###lif===3###soif===139###eoif===182###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_add(struct json_object*,struct json_object*)")&&json_object_is_type(dest, json_type_object))	&&(AKA_mark("lis===1043###sois===24597###eois===24641###lif===3###soif===186###eoif===230###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_add(struct json_object*,struct json_object*)")&&json_object_is_type(added, json_type_object)))) {
				AKA_mark("lis===1044###sois===24647###eois===24682###lif===4###soif===236###eoif===271###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_add(struct json_object*,struct json_object*)");it = json_object_iter_begin(added);

				AKA_mark("lis===1045###sois===24685###eois===24719###lif===5###soif===274###eoif===308###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_add(struct json_object*,struct json_object*)");end = json_object_iter_end(added);

				while (AKA_mark("lis===1046###sois===24729###eois===24763###lif===6###soif===318###eoif===352###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_add(struct json_object*,struct json_object*)") && (AKA_mark("lis===1046###sois===24729###eois===24763###lif===6###soif===318###eoif===352###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_add(struct json_object*,struct json_object*)")&&!json_object_iter_equal(&it, &end))) {
						AKA_mark("lis===1047###sois===24770###eois===24891###lif===7###soif===359###eoif===480###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_add(struct json_object*,struct json_object*)");json_object_object_add(dest,
				json_object_iter_peek_name(&it),
				json_object_get(json_object_iter_peek_value(&it)));

						AKA_mark("lis===1050###sois===24895###eois===24922###lif===10###soif===484###eoif===511###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_add(struct json_object*,struct json_object*)");json_object_iter_next(&it);

		}

	}
	else {AKA_mark("lis===-1043-###sois===-24550-###eois===-2455091-###lif===-3-###soif===-###eoif===-230-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_add(struct json_object*,struct json_object*)");}

		AKA_mark("lis===1053###sois===24931###eois===24943###lif===13###soif===520###eoif===532###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_object_add(struct json_object*,struct json_object*)");return dest;

}

/**
 * Sort the 'array' and returns it. Sorting is done accordingly to the
 * order given by the function 'wrap_json_cmp'. If the paramater isn't
 * an array, nothing is done and the parameter is returned unchanged.
 *
 * @param array the array to sort
 *
 * @returns the array sorted
 */
/** Instrumented function wrap_json_sort(struct json_object*) */
struct json_object *wrap_json_sort(struct json_object *array)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_sort(struct json_object*)");AKA_fCall++;
		if (AKA_mark("lis===1067###sois===25305###eois===25348###lif===2###soif===69###eoif===112###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_sort(struct json_object*)") && (AKA_mark("lis===1067###sois===25305###eois===25348###lif===2###soif===69###eoif===112###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_sort(struct json_object*)")&&json_object_is_type(array, json_type_array))) {
		AKA_mark("lis===1068###sois===25352###eois===25431###lif===3###soif===116###eoif===195###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_sort(struct json_object*)");json_object_array_sort(array, (int(*)(const void*, const void*))wrap_json_cmp);
	}
	else {AKA_mark("lis===-1067-###sois===-25305-###eois===-2530543-###lif===-2-###soif===-###eoif===-112-###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_sort(struct json_object*)");}


		AKA_mark("lis===1070###sois===25434###eois===25447###lif===5###soif===198###eoif===211###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_sort(struct json_object*)");return array;

}

/**
 * Returns a json array of the sorted keys of 'object' or null if 'object' has no keys.
 *
 * @param object the object whose keys are to be returned
 *
 * @return either NULL is 'object' isn't an object or a sorted array of the key's strings.
 */
/** Instrumented function wrap_json_keys(struct json_object*) */
struct json_object *wrap_json_keys(struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)");AKA_fCall++;
		AKA_mark("lis===1082###sois===25768###eois===25790###lif===2###soif===66###eoif===88###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)");struct json_object *r;

		AKA_mark("lis===1083###sois===25792###eois===25828###lif===3###soif===90###eoif===126###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)");struct json_object_iterator it, end;

		if (AKA_mark("lis===1084###sois===25834###eois===25880###lif===4###soif===132###eoif===178###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)") && (AKA_mark("lis===1084###sois===25834###eois===25880###lif===4###soif===132###eoif===178###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)")&&!json_object_is_type(object, json_type_object))) {
		AKA_mark("lis===1085###sois===25884###eois===25893###lif===5###soif===182###eoif===191###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)");r = NULL;
	}
	else {
				AKA_mark("lis===1087###sois===25904###eois===25932###lif===7###soif===202###eoif===230###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)");r = json_object_new_array();

				AKA_mark("lis===1088###sois===25935###eois===25971###lif===8###soif===233###eoif===269###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)");it = json_object_iter_begin(object);

				AKA_mark("lis===1089###sois===25974###eois===26009###lif===9###soif===272###eoif===307###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)");end = json_object_iter_end(object);

				while (AKA_mark("lis===1090###sois===26019###eois===26053###lif===10###soif===317###eoif===351###ifc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)") && (AKA_mark("lis===1090###sois===26019###eois===26053###lif===10###soif===317###eoif===351###isc===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)")&&!json_object_iter_equal(&it, &end))) {
						AKA_mark("lis===1091###sois===26060###eois===26142###lif===11###soif===358###eoif===440###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)");json_object_array_add(r, json_object_new_string(json_object_iter_peek_name(&it)));

						AKA_mark("lis===1092###sois===26146###eois===26173###lif===12###soif===444###eoif===471###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)");json_object_iter_next(&it);

		}

				AKA_mark("lis===1094###sois===26180###eois===26198###lif===14###soif===478###eoif===496###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)");wrap_json_sort(r);

	}

		AKA_mark("lis===1096###sois===26203###eois===26212###lif===16###soif===501###eoif===510###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_keys(struct json_object*)");return r;

}

/**
 * Internal comparison of 'x' with 'y'
 *
 * @param x first object to compare
 * @param y second object to compare
 * @param inc boolean true if should test for inclusion of y in x
 * @param sort boolean true if comparison used for sorting
 *
 * @return an integer indicating the computed result. Refer to
 * the table below for meaning of the returned value.
 *
 * inc | sort |  x < y  |  x == y  |  x > y  |  y in x
 * ----+------+---------+----------+---------+---------
 *  0  |  0   |  != 0   |     0    |  != 0   |   > 0
 *  0  |  1   |   < 0   |     0    |   > 0   |   > 0
 *  1  |  0   |  != 0   |     0    |  != 0   |    0
 *  1  |  1   |   < 0   |     0    |   > 0   |    0
 *
 *
 * if 'x' is found, respectively, to be less  than,  to match,
 * or be greater than 'y'. This is valid when 'sort'
 */
/** Instrumented function jcmp(struct json_object*,struct json_object*,int,int) */
static int jcmp(struct json_object *x, struct json_object *y, int inc, int sort)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");AKA_fCall++;
		AKA_mark("lis===1123###sois===27114###eois===27128###lif===2###soif===84###eoif===98###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");double dx, dy;

		AKA_mark("lis===1124###sois===27130###eois===27145###lif===3###soif===100###eoif===115###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");int64_t ix, iy;

		AKA_mark("lis===1125###sois===27147###eois===27167###lif===4###soif===117###eoif===137###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");const char *sx, *sy;

		AKA_mark("lis===1126###sois===27169###eois===27191###lif===5###soif===139###eoif===161###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");enum json_type tx, ty;

		AKA_mark("lis===1127###sois===27193###eois===27210###lif===6###soif===163###eoif===180###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");int r, nx, ny, i;

		AKA_mark("lis===1128###sois===27212###eois===27248###lif===7###soif===182###eoif===218###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");struct json_object_iterator it, end;

		AKA_mark("lis===1129###sois===27250###eois===27278###lif===8###soif===220###eoif===248###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");struct json_object *jx, *jy;


	/* check equality of pointers */
		if (AKA_mark("lis===1132###sois===27319###eois===27325###lif===11###soif===289###eoif===295###ifc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)") && (AKA_mark("lis===1132###sois===27319###eois===27325###lif===11###soif===289###eoif===295###isc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)")&&x == y)) {
		AKA_mark("lis===1133###sois===27329###eois===27338###lif===12###soif===299###eoif===308###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");return 0;
	}
	else {AKA_mark("lis===-1132-###sois===-27319-###eois===-273196-###lif===-11-###soif===-###eoif===-295-###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");}


	/* get the types */
		AKA_mark("lis===1136###sois===27362###eois===27391###lif===15###soif===332###eoif===361###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");tx = json_object_get_type(x);

		AKA_mark("lis===1137###sois===27393###eois===27422###lif===16###soif===363###eoif===392###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");ty = json_object_get_type(y);

		AKA_mark("lis===1138###sois===27424###eois===27446###lif===17###soif===394###eoif===416###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");r = (int)tx - (int)ty;

		if (AKA_mark("lis===1139###sois===27452###eois===27453###lif===18###soif===422###eoif===423###ifc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)") && (AKA_mark("lis===1139###sois===27452###eois===27453###lif===18###soif===422###eoif===423###isc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)")&&r)) {
		AKA_mark("lis===1140###sois===27457###eois===27466###lif===19###soif===427###eoif===436###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");return r;
	}
	else {AKA_mark("lis===-1139-###sois===-27452-###eois===-274521-###lif===-18-###soif===-###eoif===-423-###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");}


	/* compare following the type */
		AKA_mark("lis===1143###sois===27511###eois===27513###lif===22###soif===481###eoif===483###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");switch(tx){
			default: if(tx != json_type_null && tx != json_type_boolean && tx != json_type_double && tx != json_type_int && tx != json_type_object && tx != json_type_array && tx != json_type_string)AKA_mark("lis===1144###sois===27518###eois===27526###lif===23###soif===488###eoif===496###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");

			case json_type_null: if(tx == json_type_null)AKA_mark("lis===1145###sois===27528###eois===27548###lif===24###soif===498###eoif===518###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");

				AKA_mark("lis===1146###sois===27551###eois===27557###lif===25###soif===521###eoif===527###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");break;


			case json_type_boolean: if(tx == json_type_boolean)AKA_mark("lis===1148###sois===27560###eois===27583###lif===27###soif===530###eoif===553###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");

				AKA_mark("lis===1149###sois===27586###eois===27659###lif===28###soif===556###eoif===629###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");r = (int)json_object_get_boolean(x)
			- (int)json_object_get_boolean(y);

				AKA_mark("lis===1151###sois===27662###eois===27668###lif===30###soif===632###eoif===638###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");break;


			case json_type_double: if(tx == json_type_double)AKA_mark("lis===1153###sois===27671###eois===27693###lif===32###soif===641###eoif===663###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");

				AKA_mark("lis===1154###sois===27696###eois===27727###lif===33###soif===666###eoif===697###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");dx = json_object_get_double(x);

				AKA_mark("lis===1155###sois===27730###eois===27761###lif===34###soif===700###eoif===731###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");dy = json_object_get_double(y);

				AKA_mark("lis===1156###sois===27764###eois===27792###lif===35###soif===734###eoif===762###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");r =  dx < dy ? -1 : dx > dy;

				AKA_mark("lis===1157###sois===27795###eois===27801###lif===36###soif===765###eoif===771###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");break;


			case json_type_int: if(tx == json_type_int)AKA_mark("lis===1159###sois===27804###eois===27823###lif===38###soif===774###eoif===793###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");

				AKA_mark("lis===1160###sois===27826###eois===27856###lif===39###soif===796###eoif===826###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");ix = json_object_get_int64(x);

				AKA_mark("lis===1161###sois===27859###eois===27889###lif===40###soif===829###eoif===859###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");iy = json_object_get_int64(y);

				AKA_mark("lis===1162###sois===27892###eois===27919###lif===41###soif===862###eoif===889###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");r = ix < iy ? -1 : ix > iy;

				AKA_mark("lis===1163###sois===27922###eois===27928###lif===42###soif===892###eoif===898###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");break;


			case json_type_object: if(tx == json_type_object)AKA_mark("lis===1165###sois===27931###eois===27953###lif===44###soif===901###eoif===923###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");

				AKA_mark("lis===1166###sois===27956###eois===27987###lif===45###soif===926###eoif===957###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");it = json_object_iter_begin(y);

				AKA_mark("lis===1167###sois===27990###eois===28020###lif===46###soif===960###eoif===990###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");end = json_object_iter_end(y);

				AKA_mark("lis===1168###sois===28023###eois===28057###lif===47###soif===993###eoif===1027###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");nx = json_object_object_length(x);

				AKA_mark("lis===1169###sois===28060###eois===28094###lif===48###soif===1030###eoif===1064###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");ny = json_object_object_length(y);

				AKA_mark("lis===1170###sois===28097###eois===28109###lif===49###soif===1067###eoif===1079###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");r = nx - ny;

				if (AKA_mark("lis===1171###sois===28116###eois===28128###lif===50###soif===1086###eoif===1098###ifc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)") && ((AKA_mark("lis===1171###sois===28116###eois===28121###lif===50###soif===1086###eoif===1091###isc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)")&&r > 0)	&&(AKA_mark("lis===1171###sois===28125###eois===28128###lif===50###soif===1095###eoif===1098###isc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)")&&inc))) {
			AKA_mark("lis===1172###sois===28133###eois===28139###lif===51###soif===1103###eoif===1109###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");r = 0;
		}
		else {AKA_mark("lis===-1171-###sois===-28116-###eois===-2811612-###lif===-50-###soif===-###eoif===-1098-###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");}

				while (AKA_mark("lis===1173###sois===28149###eois===28189###lif===52###soif===1119###eoif===1159###ifc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)") && ((AKA_mark("lis===1173###sois===28149###eois===28151###lif===52###soif===1119###eoif===1121###isc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)")&&!r)	&&(AKA_mark("lis===1173###sois===28155###eois===28189###lif===52###soif===1125###eoif===1159###isc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)")&&!json_object_iter_equal(&it, &end)))) {
						if (AKA_mark("lis===1174###sois===28200###eois===28266###lif===53###soif===1170###eoif===1236###ifc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)") && (AKA_mark("lis===1174###sois===28200###eois===28266###lif===53###soif===1170###eoif===1236###isc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)")&&json_object_object_get_ex(x, json_object_iter_peek_name(&it), &jx))) {
								AKA_mark("lis===1175###sois===28274###eois===28312###lif===54###soif===1244###eoif===1282###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");jy = json_object_iter_peek_value(&it);

								AKA_mark("lis===1176###sois===28317###eois===28344###lif===55###soif===1287###eoif===1314###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");json_object_iter_next(&it);

								AKA_mark("lis===1177###sois===28349###eois===28377###lif===56###soif===1319###eoif===1347###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");r = jcmp(jx, jy, inc, sort);

			}
			else {
				if (AKA_mark("lis===1178###sois===28392###eois===28396###lif===57###soif===1362###eoif===1366###ifc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)") && (AKA_mark("lis===1178###sois===28392###eois===28396###lif===57###soif===1362###eoif===1366###isc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)")&&sort)) {
									AKA_mark("lis===1179###sois===28404###eois===28427###lif===58###soif===1374###eoif===1397###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");jx = wrap_json_keys(x);

									AKA_mark("lis===1180###sois===28432###eois===28455###lif===59###soif===1402###eoif===1425###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");jy = wrap_json_keys(y);

									AKA_mark("lis===1181###sois===28460###eois===28486###lif===60###soif===1430###eoif===1456###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");r = wrap_json_cmp(jx, jy);

									AKA_mark("lis===1182###sois===28491###eois===28511###lif===61###soif===1461###eoif===1481###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");json_object_put(jx);

									AKA_mark("lis===1183###sois===28516###eois===28536###lif===62###soif===1486###eoif===1506###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");json_object_put(jy);

			}
				else {
					AKA_mark("lis===1185###sois===28551###eois===28557###lif===64###soif===1521###eoif===1527###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");r = 1;
				}
			}

		}

				AKA_mark("lis===1187###sois===28564###eois===28570###lif===66###soif===1534###eoif===1540###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");break;


			case json_type_array: if(tx == json_type_array)AKA_mark("lis===1189###sois===28573###eois===28594###lif===68###soif===1543###eoif===1564###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");

				AKA_mark("lis===1190###sois===28597###eois===28635###lif===69###soif===1567###eoif===1605###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");nx = (int)json_object_array_length(x);

				AKA_mark("lis===1191###sois===28638###eois===28676###lif===70###soif===1608###eoif===1646###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");ny = (int)json_object_array_length(y);

				AKA_mark("lis===1192###sois===28679###eois===28691###lif===71###soif===1649###eoif===1661###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");r = nx - ny;

				if (AKA_mark("lis===1193###sois===28698###eois===28710###lif===72###soif===1668###eoif===1680###ifc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)") && ((AKA_mark("lis===1193###sois===28698###eois===28703###lif===72###soif===1668###eoif===1673###isc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)")&&r > 0)	&&(AKA_mark("lis===1193###sois===28707###eois===28710###lif===72###soif===1677###eoif===1680###isc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)")&&inc))) {
			AKA_mark("lis===1194###sois===28715###eois===28721###lif===73###soif===1685###eoif===1691###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");r = 0;
		}
		else {AKA_mark("lis===-1193-###sois===-28698-###eois===-2869812-###lif===-72-###soif===-###eoif===-1680-###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");}

				AKA_mark("lis===1195###sois===28729###eois===28736###lif===74###soif===1699###eoif===1706###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");for (i = 0 ;AKA_mark("lis===1195###sois===28737###eois===28749###lif===74###soif===1707###eoif===1719###ifc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)") && (AKA_mark("lis===1195###sois===28737###eois===28739###lif===74###soif===1707###eoif===1709###isc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)")&&!r)	&&(AKA_mark("lis===1195###sois===28743###eois===28749###lif===74###soif===1713###eoif===1719###isc===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)")&&i < ny);({AKA_mark("lis===1195###sois===28752###eois===28755###lif===74###soif===1722###eoif===1725###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");i++;})) {
						AKA_mark("lis===1196###sois===28762###eois===28799###lif===75###soif===1732###eoif===1769###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");jx = json_object_array_get_idx(x, i);

						AKA_mark("lis===1197###sois===28803###eois===28840###lif===76###soif===1773###eoif===1810###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");jy = json_object_array_get_idx(y, i);

						AKA_mark("lis===1198###sois===28844###eois===28872###lif===77###soif===1814###eoif===1842###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");r = jcmp(jx, jy, inc, sort);

		}

				AKA_mark("lis===1200###sois===28879###eois===28885###lif===79###soif===1849###eoif===1855###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");break;


			case json_type_string: if(tx == json_type_string)AKA_mark("lis===1202###sois===28888###eois===28910###lif===81###soif===1858###eoif===1880###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");

				AKA_mark("lis===1203###sois===28913###eois===28944###lif===82###soif===1883###eoif===1914###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");sx = json_object_get_string(x);

				AKA_mark("lis===1204###sois===28947###eois===28978###lif===83###soif===1917###eoif===1948###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");sy = json_object_get_string(y);

				AKA_mark("lis===1205###sois===28981###eois===29000###lif===84###soif===1951###eoif===1970###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");r = strcmp(sx, sy);

				AKA_mark("lis===1206###sois===29003###eois===29009###lif===85###soif===1973###eoif===1979###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");break;

	}

		AKA_mark("lis===1208###sois===29014###eois===29023###lif===87###soif===1984###eoif===1993###ins===true###function===./app-framework-binder/src/wrap-json.c/jcmp(struct json_object*,struct json_object*,int,int)");return r;

}

/**
 * Compares 'x' with 'y'
 *
 * @param x first object to compare
 * @param y second object to compare
 *
 * @return an integer less than, equal to, or greater than zero
 * if 'x' is found, respectively, to be less than, to match,
 * or be greater than 'y'.
 */
/** Instrumented function wrap_json_cmp(struct json_object*,struct json_object*) */
int wrap_json_cmp(struct json_object *x, struct json_object *y)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_cmp(struct json_object*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===1223###sois===29358###eois===29382###lif===2###soif===67###eoif===91###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_cmp(struct json_object*,struct json_object*)");return jcmp(x, y, 0, 1);

}

/**
 * Searchs wether 'x' equals 'y'
 *
 * @param x first object to compare
 * @param y second object to compare
 *
 * @return an integer equal to zero when 'x' != 'y' or 1 when 'x' == 'y'.
 */
/** Instrumented function wrap_json_equal(struct json_object*,struct json_object*) */
int wrap_json_equal(struct json_object *x, struct json_object *y)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_equal(struct json_object*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===1236###sois===29649###eois===29674###lif===2###soif===69###eoif===94###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_equal(struct json_object*,struct json_object*)");return !jcmp(x, y, 0, 0);

}

/**
 * Searchs wether 'x' contains 'y'
 *
 * @param x first object to compare
 * @param y second object to compare
 *
 * @return an integer equal to 1 when 'y' is a subset of 'x' or zero otherwise
 */
/** Instrumented function wrap_json_contains(struct json_object*,struct json_object*) */
int wrap_json_contains(struct json_object *x, struct json_object *y)
{AKA_mark("Calling: ./app-framework-binder/src/wrap-json.c/wrap_json_contains(struct json_object*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===1249###sois===29951###eois===29976###lif===2###soif===72###eoif===97###ins===true###function===./app-framework-binder/src/wrap-json.c/wrap_json_contains(struct json_object*,struct json_object*)");return !jcmp(x, y, 1, 0);

}

#if defined(WRAP_JSON_TEST)
#include <stdio.h>
#if !defined(JSON_C_TO_STRING_NOSLASHESCAPE)
#define JSON_C_TO_STRING_NOSLASHESCAPE 0
#endif
#define j2t(o) json_object_to_json_string_ext((o), JSON_C_TO_STRING_NOSLASHESCAPE)

void tclone(struct json_object *object)
{
	struct json_object *o;

	o = wrap_json_clone(object);
	if (!wrap_json_equal(object, o))
		printf("ERROR in clone or equal: %s VERSUS %s\n", j2t(object), j2t(o));
	json_object_put(o);

	o = wrap_json_clone_deep(object);
	if (!wrap_json_equal(object, o))
		printf("ERROR in clone_deep or equal: %s VERSUS %s\n", j2t(object), j2t(o));
	json_object_put(o);
}

void p(const char *desc, ...)
{
	int rc;
	va_list args;
	struct json_object *result;

	va_start(args, desc);
	rc = wrap_json_vpack(&result, desc, args);
	va_end(args);
	if (!rc)
		printf("  SUCCESS %s\n\n", j2t(result));
	else
		printf("  ERROR[char %d err %d] %s\n\n", wrap_json_get_error_position(rc), wrap_json_get_error_code(rc), wrap_json_get_error_string(rc));
	tclone(result);
	json_object_put(result);
}

const char *xs[10];
int *xi[10];
int64_t *xI[10];
double *xf[10];
struct json_object *xo[10];
size_t xz[10];
uint8_t *xy[10];

void u(const char *value, const char *desc, ...)
{
	unsigned m, k;
	int rc;
	va_list args;
	struct json_object *object, *o;

	memset(xs, 0, sizeof xs);
	memset(xi, 0, sizeof xi);
	memset(xI, 0, sizeof xI);
	memset(xf, 0, sizeof xf);
	memset(xo, 0, sizeof xo);
	memset(xy, 0, sizeof xy);
	memset(xz, 0, sizeof xz);
	object = json_tokener_parse(value);
	va_start(args, desc);
	rc = wrap_json_vunpack(object, desc, args);
	va_end(args);
	if (rc)
		printf("  ERROR[char %d err %d] %s\n\n", wrap_json_get_error_position(rc), wrap_json_get_error_code(rc), wrap_json_get_error_string(rc));
	else {
		value = NULL;
		printf("  SUCCESS");
		va_start(args, desc);
		k = m = 0;
		while(*desc) {
			switch(*desc) {
			case '{': m = (m << 1) | 1; k = 1; break;
			case '}': m = m >> 1; k = m&1; break;
			case '[': m = m << 1; k = 0; break;
			case ']': m = m >> 1; k = m&1; break;
			case 's': printf(" s:%s", k ? va_arg(args, const char*) : *(va_arg(args, const char**)?:&value)); k ^= m&1; break;
			case '%': printf(" %%:%zu", *va_arg(args, size_t*)); k = m&1; break;
			case 'n': printf(" n"); k = m&1; break;
			case 'b': printf(" b:%d", *va_arg(args, int*)); k = m&1; break;
			case 'i': printf(" i:%d", *va_arg(args, int*)); k = m&1; break;
			case 'I': printf(" I:%lld", *va_arg(args, int64_t*)); k = m&1; break;
			case 'f': printf(" f:%f", *va_arg(args, double*)); k = m&1; break;
			case 'F': printf(" F:%f", *va_arg(args, double*)); k = m&1; break;
			case 'o': printf(" o:%s", j2t(*va_arg(args, struct json_object**))); k = m&1; break;
			case 'O': o = *va_arg(args, struct json_object**); printf(" O:%s", j2t(o)); json_object_put(o); k = m&1; break;
			case 'y':
			case 'Y': {
				uint8_t *p = *va_arg(args, uint8_t**);
				size_t s = *va_arg(args, size_t*);
				printf(" y/%d:%.*s", (int)s, (int)s, (char*)p);
				k ^= m&1;
				break;
				}
			default: break;
			}
			desc++;
		}
		va_end(args);
		printf("\n\n");
	}
	tclone(object);
	json_object_put(object);
}

void c(const char *sx, const char *sy, int e, int c)
{
	int re, rc;
	struct json_object *jx, *jy;

	jx = json_tokener_parse(sx);
	jy = json_tokener_parse(sy);

	re = wrap_json_cmp(jx, jy);
	rc = wrap_json_contains(jx, jy);

	printf("compare(%s)(%s)\n", sx, sy);
	printf("   -> %d / %d\n", re, rc);

	if (!re != !!e)
		printf("  ERROR should be %s\n", e ? "equal" : "different");
	if (!rc != !c)
		printf("  ERROR should %scontain\n", c ? "" : "not ");

	printf("\n");
}

#define P(...) do{ printf("pack(%s)\n",#__VA_ARGS__); p(__VA_ARGS__); } while(0)
#define U(...) do{ printf("unpack(%s)\n",#__VA_ARGS__); u(__VA_ARGS__); } while(0)

int main()
{
	char buffer[4] = {'t', 'e', 's', 't'};

	P("n");
	P("b", 1);
	P("b", 0);
	P("i", 1);
	P("I", (uint64_t)0x123456789abcdef);
	P("f", 3.14);
	P("s", "test");
	P("s?", "test");
	P("s?", NULL);
	P("s#", "test asdf", 4);
	P("s%", "test asdf", (size_t)4);
	P("s#", buffer, 4);
	P("s%", buffer, (size_t)4);
	P("s++", "te", "st", "ing");
	P("s#+#+", "test", 1, "test", 2, "test");
	P("s%+%+", "test", (size_t)1, "test", (size_t)2, "test");
	P("{}", 1.0);
	P("[]", 1.0);
	P("o", json_object_new_int(1));
	P("o?", json_object_new_int(1));
	P("o?", NULL);
	P("O", json_object_new_int(1));
	P("O?", json_object_new_int(1));
	P("O?", NULL);
	P("{s:[]}", "foo");
	P("{s+#+: []}", "foo", "barbar", 3, "baz");
	P("{s:s,s:o,s:O}", "a", NULL, "b", NULL, "c", NULL);
	P("{s:**}", "a", NULL);
	P("{s:s*,s:o*,s:O*}", "a", NULL, "b", NULL, "c", NULL);
	P("[i,i,i]", 0, 1, 2);
	P("[s,o,O]", NULL, NULL, NULL);
	P("[**]", NULL);
	P("[s*,o*,O*]", NULL, NULL, NULL);
	P(" s ", "test");
	P("[ ]");
	P("[ i , i,  i ] ", 1, 2, 3);
	P("{\n\n1");
	P("[}");
	P("{]");
	P("[");
	P("{");
	P("[i]a", 42);
	P("ia", 42);
	P("s", NULL);
	P("+", NULL);
	P(NULL);
	P("{s:i}", NULL, 1);
	P("{ {}: s }", "foo");
	P("{ s: {},  s:[ii{} }", "foo", "bar", 12, 13);
	P("[[[[[   [[[[[  [[[[ }]]]] ]]]] ]]]]]");
	P("y", "???????hello>>>>>>>", (size_t)19);
	P("Y", "???????hello>>>>>>>", (size_t)19);
	P("{sy?}", "foo", "hi", (size_t)2);
	P("{sy?}", "foo", NULL, 0);
	P("{sy*}", "foo", "hi", (size_t)2);
	P("{sy*}", "foo", NULL, 0);

	U("true", "b", &xi[0]);
	U("false", "b", &xi[0]);
	U("null", "n");
	U("42", "i", &xi[0]);
	U("123456789", "I", &xI[0]);
	U("3.14", "f", &xf[0]);
	U("12345", "F", &xf[0]);
	U("3.14", "F", &xf[0]);
	U("\"foo\"", "s", &xs[0]);
	U("\"foo\"", "s%", &xs[0], &xz[0]);
	U("{}", "{}");
	U("[]", "[]");
	U("{}", "o", &xo[0]);
	U("{}", "O", &xo[0]);
	U("{\"foo\":42}", "{si}", "foo", &xi[0]);
	U("[1,2,3]", "[i,i,i]", &xi[0], &xi[1], &xi[2]);
	U("{\"a\":1,\"b\":2,\"c\":3}", "{s:i, s:i, s:i}", "a", &xi[0], "b", &xi[1], "c", &xi[2]);
	U("42", "z");
	U("null", "[i]");
	U("[]", "[}");
	U("{}", "{]");
	U("[]", "[");
	U("{}", "{");
	U("[42]", "[i]a", &xi[0]);
	U("42", "ia", &xi[0]);
	U("[]", NULL);
	U("\"foo\"", "s", NULL);
	U("42", "s", NULL);
	U("42", "n");
	U("42", "b", NULL);
	U("42", "f", NULL);
	U("42", "[i]", NULL);
	U("42", "{si}", "foo", NULL);
	U("\"foo\"", "n");
	U("\"foo\"", "b", NULL);
	U("\"foo\"", "i", NULL);
	U("\"foo\"", "I", NULL);
	U("\"foo\"", "f", NULL);
	U("\"foo\"", "F", NULL);
	U("true", "s", NULL);
	U("true", "n");
	U("true", "i", NULL);
	U("true", "I", NULL);
	U("true", "f", NULL);
	U("true", "F", NULL);
	U("[42]", "[ii]", &xi[0], &xi[1]);
	U("{\"foo\":42}", "{si}", NULL, &xi[0]);
	U("{\"foo\":42}", "{si}", "baz", &xi[0]);
	U("[1,2,3]", "[iii!]", &xi[0], &xi[1], &xi[2]);
	U("[1,2,3]", "[ii!]", &xi[0], &xi[1]);
	U("[1,2,3]", "[ii]", &xi[0], &xi[1]);
	U("[1,2,3]", "[ii*]", &xi[0], &xi[1]);
	U("{\"foo\":42,\"baz\":45}", "{sisi}", "baz", &xi[0], "foo", &xi[1]);
	U("{\"foo\":42,\"baz\":45}", "{sisi*}", "baz", &xi[0], "foo", &xi[1]);
	U("{\"foo\":42,\"baz\":45}", "{sisi!}", "baz", &xi[0], "foo", &xi[1]);
	U("{\"foo\":42,\"baz\":45}", "{si}", "baz", &xi[0], "foo", &xi[1]);
	U("{\"foo\":42,\"baz\":45}", "{si*}", "baz", &xi[0], "foo", &xi[1]);
	U("{\"foo\":42,\"baz\":45}", "{si!}", "baz", &xi[0], "foo", &xi[1]);
	U("[1,{\"foo\":2,\"bar\":null},[3,4]]", "[i{sisn}[ii]]", &xi[0], "foo", &xi[1], "bar", &xi[2], &xi[3]);
	U("[1,2,3]", "[ii!i]", &xi[0], &xi[1], &xi[2]);
	U("[1,2,3]", "[ii*i]", &xi[0], &xi[1], &xi[2]);
	U("{\"foo\":1,\"bar\":2}", "{si!si}", "foo", &xi[1], "bar", &xi[2]);
	U("{\"foo\":1,\"bar\":2}", "{si*si}", "foo", &xi[1], "bar", &xi[2]);
	U("{\"foo\":{\"baz\":null,\"bar\":null}}", "{s{sn!}}", "foo", "bar");
	U("[[1,2,3]]", "[[ii!]]", &xi[0], &xi[1]);
	U("{}", "{s?i}", "foo", &xi[0]);
	U("{\"foo\":1}", "{s?i}", "foo", &xi[0]);
	U("{}", "{s?[ii]s?{s{si!}}}", "foo", &xi[0], &xi[1], "bar", "baz", "quux", &xi[2]);
	U("{\"foo\":[1,2]}", "{s?[ii]s?{s{si!}}}", "foo", &xi[0], &xi[1], "bar", "baz", "quux", &xi[2]);
	U("{\"bar\":{\"baz\":{\"quux\":15}}}", "{s?[ii]s?{s{si!}}}", "foo", &xi[0], &xi[1], "bar", "baz", "quux", &xi[2]);
	U("{\"foo\":{\"bar\":4}}", "{s?{s?i}}", "foo", "bar", &xi[0]);
	U("{\"foo\":{}}", "{s?{s?i}}", "foo", "bar", &xi[0]);
	U("{}", "{s?{s?i}}", "foo", "bar", &xi[0]);
	U("{\"foo\":42,\"baz\":45}", "{s?isi!}", "baz", &xi[0], "foo", &xi[1]);
	U("{\"foo\":42}", "{s?isi!}", "baz", &xi[0], "foo", &xi[1]);

	U("\"Pz8_Pz8_P2hlbGxvPj4-Pj4-Pg\"", "y", &xy[0], &xz[0]);
	U("\"\"", "y", &xy[0], &xz[0]);
	U("null", "y", &xy[0], &xz[0]);
	U("{\"foo\":\"Pz8_Pz8_P2hlbGxvPj4-Pj4-Pg\"}", "{s?y}", "foo", &xy[0], &xz[0]);
	U("{\"foo\":\"\"}", "{s?y}", "foo", &xy[0], &xz[0]);
	U("{}", "{s?y}", "foo", &xy[0], &xz[0]);

	c("null", "null", 1, 1);
	c("true", "true", 1, 1);
	c("false", "false", 1, 1);
	c("1", "1", 1, 1);
	c("1.0", "1.0", 1, 1);
	c("\"\"", "\"\"", 1, 1);
	c("\"hi\"", "\"hi\"", 1, 1);
	c("{}", "{}", 1, 1);
	c("{\"a\":true,\"b\":false}", "{\"b\":false,\"a\":true}", 1, 1);
	c("[]", "[]", 1, 1);
	c("[1,true,null]", "[1,true,null]", 1, 1);

	c("null", "true", 0, 0);
	c("null", "false", 0, 0);
	c("0", "1", 0, 0);
	c("1", "0", 0, 0);
	c("0", "true", 0, 0);
	c("0", "false", 0, 0);
	c("0", "null", 0, 0);

	c("\"hi\"", "\"hello\"", 0, 0);
	c("\"hello\"", "\"hi\"", 0, 0);

	c("{}", "null", 0, 0);
	c("{}", "true", 0, 0);
	c("{}", "1", 0, 0);
	c("{}", "1.0", 0, 0);
	c("{}", "[]", 0, 0);
	c("{}", "\"x\"", 0, 0);

	c("[1,true,null]", "[1,true]", 0, 1);
	c("{\"a\":true,\"b\":false}", "{\"a\":true}", 0, 1);
	c("{\"a\":true,\"b\":false}", "{\"a\":true,\"c\":false}", 0, 0);
	c("{\"a\":true,\"c\":false}", "{\"a\":true,\"b\":false}", 0, 0);
	return 0;
}

#endif

#if 0


    /* Unpack the same item twice */
    j = json_pack("{s:s, s:i, s:b}", "foo", "bar", "baz", 42, "quux", 1);
    if(!json_unpack_ex(j, &error, 0, "{s:s,s:s!}", "foo", &s, "foo", &s))
        fail("json_unpack object with strict validation failed");
    {
        const char *possible_errors[] = {
            "2 object item(s) left unpacked: baz, quux",
            "2 object item(s) left unpacked: quux, baz"
        };
        check_errors(possible_errors, 2, "<validation>", 1, 10, 10);
    }
    json_decref(j);

#endif

#endif

