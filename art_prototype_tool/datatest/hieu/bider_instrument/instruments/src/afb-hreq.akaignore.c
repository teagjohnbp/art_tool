/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_HREQ_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_HREQ_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <microhttpd.h>
#include <json-c/json.h>
#if !defined(JSON_C_TO_STRING_NOSLASHESCAPE)
#define JSON_C_TO_STRING_NOSLASHESCAPE 0
#endif

#if defined(USE_MAGIC_MIME_TYPE)
#include <magic.h>
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_METHOD_H_
#define AKA_INCLUDE__AFB_METHOD_H_
#include "afb-method.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_MSG_JSON_H_
#define AKA_INCLUDE__AFB_MSG_JSON_H_
#include "afb-msg-json.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CONTEXT_H_
#define AKA_INCLUDE__AFB_CONTEXT_H_
#include "afb-context.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HREQ_H_
#define AKA_INCLUDE__AFB_HREQ_H_
#include "afb-hreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HSRV_H_
#define AKA_INCLUDE__AFB_HSRV_H_
#include "afb-hsrv.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SESSION_H_
#define AKA_INCLUDE__AFB_SESSION_H_
#include "afb-session.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_TOKEN_H_
#define AKA_INCLUDE__AFB_TOKEN_H_
#include "afb-token.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_ERROR_TEXT_H_
#define AKA_INCLUDE__AFB_ERROR_TEXT_H_
#include "afb-error-text.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__LOCALE_ROOT_H_
#define AKA_INCLUDE__LOCALE_ROOT_H_
#include "locale-root.akaignore.h"
#endif


#define SIZE_RESPONSE_BUFFER   8192

static int global_reqids = 0;

static char empty_string[] = "";

static const char long_key_for_uuid[] = "x-afb-uuid";
static const char short_key_for_uuid[] = "uuid";

static const char long_key_for_token[] = "x-afb-token";
static const char short_key_for_token[] = "token";

static const char long_key_for_reqid[] = "x-afb-reqid";
static const char short_key_for_reqid[] = "reqid";

static const char key_for_bearer[] = "Bearer";
static const char key_for_access_token[] = "access_token";

static char *cookie_name = NULL;
static char *cookie_setter = NULL;
static char *tmp_pattern = NULL;

/*
 * Structure for storing key/values read from POST requests
 */
struct hreq_data {
	struct hreq_data *next;	/* chain to next data */
	char *key;		/* key name */
	size_t length;		/* length of the value (used for appending) */
	char *value;		/* the value (or original filename) */
	char *path;		/* path of the file saved */
};

static struct json_object *req_json(struct afb_xreq *xreq);
static struct afb_arg req_get(struct afb_xreq *xreq, const char *name);
static void req_reply(struct afb_xreq *xreq, struct json_object *object, const char *error, const char *info);
static void req_destroy(struct afb_xreq *xreq);

const struct afb_xreq_query_itf afb_hreq_xreq_query_itf = {
	.json = req_json,
	.get = req_get,
	.reply = req_reply,
	.unref = req_destroy
};

/** Instrumented function get_data(struct afb_hreq*,const char*,int) */
static struct hreq_data *get_data(struct afb_hreq *hreq, const char *key, int create)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===97###sois===2753###eois===2789###lif===2###soif===89###eoif===125###ins===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)");struct hreq_data *data = hreq->data;

		while (AKA_mark("lis===98###sois===2798###eois===2810###lif===3###soif===134###eoif===146###ifc===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)") && (AKA_mark("lis===98###sois===2798###eois===2810###lif===3###soif===134###eoif===146###isc===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)")&&data != NULL)) {
				if (AKA_mark("lis===99###sois===2820###eois===2847###lif===4###soif===156###eoif===183###ifc===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)") && (AKA_mark("lis===99###sois===2820###eois===2847###lif===4###soif===156###eoif===183###isc===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)")&&!strcasecmp(data->key, key))) {
			AKA_mark("lis===100###sois===2852###eois===2864###lif===5###soif===188###eoif===200###ins===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)");return data;
		}
		else {AKA_mark("lis===-99-###sois===-2820-###eois===-282027-###lif===-4-###soif===-###eoif===-183-###ins===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)");}

				AKA_mark("lis===101###sois===2867###eois===2885###lif===6###soif===203###eoif===221###ins===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)");data = data->next;

	}

		if (AKA_mark("lis===103###sois===2894###eois===2900###lif===8###soif===230###eoif===236###ifc===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)") && (AKA_mark("lis===103###sois===2894###eois===2900###lif===8###soif===230###eoif===236###isc===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)")&&create)) {
				AKA_mark("lis===104###sois===2906###eois===2937###lif===9###soif===242###eoif===273###ins===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)");data = calloc(1, sizeof *data);

				if (AKA_mark("lis===105###sois===2944###eois===2956###lif===10###soif===280###eoif===292###ifc===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)") && (AKA_mark("lis===105###sois===2944###eois===2956###lif===10###soif===280###eoif===292###isc===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)")&&data != NULL)) {
						AKA_mark("lis===106###sois===2963###eois===2987###lif===11###soif===299###eoif===323###ins===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)");data->key = strdup(key);

						if (AKA_mark("lis===107###sois===2995###eois===3012###lif===12###soif===331###eoif===348###ifc===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)") && (AKA_mark("lis===107###sois===2995###eois===3012###lif===12###soif===331###eoif===348###isc===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)")&&data->key == NULL)) {
								AKA_mark("lis===108###sois===3020###eois===3031###lif===13###soif===356###eoif===367###ins===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)");free(data);

								AKA_mark("lis===109###sois===3036###eois===3048###lif===14###soif===372###eoif===384###ins===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)");data = NULL;

			}
			else {
								AKA_mark("lis===111###sois===3065###eois===3089###lif===16###soif===401###eoif===425###ins===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)");data->next = hreq->data;

								AKA_mark("lis===112###sois===3094###eois===3112###lif===17###soif===430###eoif===448###ins===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)");hreq->data = data;

			}

		}
		else {AKA_mark("lis===-105-###sois===-2944-###eois===-294412-###lif===-10-###soif===-###eoif===-292-###ins===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)");}

	}
	else {AKA_mark("lis===-103-###sois===-2894-###eois===-28946-###lif===-8-###soif===-###eoif===-236-###ins===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)");}

		AKA_mark("lis===116###sois===3126###eois===3138###lif===21###soif===462###eoif===474###ins===true###function===./app-framework-binder/src/afb-hreq.c/get_data(struct afb_hreq*,const char*,int)");return data;

}

/* a valid subpath is a relative path not looking deeper than root using .. */
/** Instrumented function validsubpath(const char*) */
static int validsubpath(const char *subpath)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");AKA_fCall++;
		AKA_mark("lis===122###sois===3269###eois===3286###lif===2###soif===48###eoif===65###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");int l = 0, i = 0;


		while (AKA_mark("lis===124###sois===3296###eois===3306###lif===4###soif===75###eoif===85###ifc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)") && (AKA_mark("lis===124###sois===3296###eois===3306###lif===4###soif===75###eoif===85###isc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)")&&subpath[i])) {
				AKA_mark("lis===125###sois===3320###eois===3332###lif===5###soif===99###eoif===111###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");switch(subpath[i++]){
					case '.': if(subpath[i++] == '.')AKA_mark("lis===126###sois===3338###eois===3347###lif===6###soif===117###eoif===126###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");

						if (AKA_mark("lis===127###sois===3355###eois===3366###lif===7###soif===134###eoif===145###ifc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)") && (AKA_mark("lis===127###sois===3355###eois===3366###lif===7###soif===134###eoif===145###isc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)")&&!subpath[i])) {
				AKA_mark("lis===128###sois===3372###eois===3378###lif===8###soif===151###eoif===157###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");break;
			}
			else {AKA_mark("lis===-127-###sois===-3355-###eois===-335511-###lif===-7-###soif===-###eoif===-145-###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");}

						if (AKA_mark("lis===129###sois===3386###eois===3403###lif===9###soif===165###eoif===182###ifc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)") && (AKA_mark("lis===129###sois===3386###eois===3403###lif===9###soif===165###eoif===182###isc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)")&&subpath[i] == '/')) {
								AKA_mark("lis===130###sois===3411###eois===3415###lif===10###soif===190###eoif===194###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");i++;

								AKA_mark("lis===131###sois===3420###eois===3426###lif===11###soif===199###eoif===205###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");break;

			}
			else {AKA_mark("lis===-129-###sois===-3386-###eois===-338617-###lif===-9-###soif===-###eoif===-182-###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");}

						if (AKA_mark("lis===133###sois===3439###eois===3458###lif===13###soif===218###eoif===237###ifc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)") && (AKA_mark("lis===133###sois===3439###eois===3458###lif===13###soif===218###eoif===237###isc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)")&&subpath[i++] == '.')) {
								if (AKA_mark("lis===134###sois===3470###eois===3481###lif===14###soif===249###eoif===260###ifc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)") && (AKA_mark("lis===134###sois===3470###eois===3481###lif===14###soif===249###eoif===260###isc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)")&&!subpath[i])) {
										if (AKA_mark("lis===135###sois===3494###eois===3501###lif===15###soif===273###eoif===280###ifc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)") && (AKA_mark("lis===135###sois===3494###eois===3501###lif===15###soif===273###eoif===280###isc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)")&&--l < 0)) {
						AKA_mark("lis===136###sois===3509###eois===3518###lif===16###soif===288###eoif===297###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");return 0;
					}
					else {AKA_mark("lis===-135-###sois===-3494-###eois===-34947-###lif===-15-###soif===-###eoif===-280-###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");}

										AKA_mark("lis===137###sois===3524###eois===3530###lif===17###soif===303###eoif===309###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");break;

				}
				else {AKA_mark("lis===-134-###sois===-3470-###eois===-347011-###lif===-14-###soif===-###eoif===-260-###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");}

								if (AKA_mark("lis===139###sois===3545###eois===3564###lif===19###soif===324###eoif===343###ifc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)") && (AKA_mark("lis===139###sois===3545###eois===3564###lif===19###soif===324###eoif===343###isc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)")&&subpath[i++] == '/')) {
										if (AKA_mark("lis===140###sois===3577###eois===3584###lif===20###soif===356###eoif===363###ifc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)") && (AKA_mark("lis===140###sois===3577###eois===3584###lif===20###soif===356###eoif===363###isc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)")&&--l < 0)) {
						AKA_mark("lis===141###sois===3592###eois===3601###lif===21###soif===371###eoif===380###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");return 0;
					}
					else {AKA_mark("lis===-140-###sois===-3577-###eois===-35777-###lif===-20-###soif===-###eoif===-363-###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");}

										AKA_mark("lis===142###sois===3607###eois===3613###lif===22###soif===386###eoif===392###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");break;

				}
				else {AKA_mark("lis===-139-###sois===-3545-###eois===-354519-###lif===-19-###soif===-###eoif===-343-###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");}

			}
			else {AKA_mark("lis===-133-###sois===-3439-###eois===-343919-###lif===-13-###soif===-###eoif===-237-###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");}

					default: if(subpath[i++] != '.' && subpath[i++] != '/')AKA_mark("lis===145###sois===3627###eois===3635###lif===25###soif===406###eoif===414###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");

						while (AKA_mark("lis===146###sois===3646###eois===3677###lif===26###soif===425###eoif===456###ifc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)") && ((AKA_mark("lis===146###sois===3646###eois===3656###lif===26###soif===425###eoif===435###isc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)")&&subpath[i])	&&(AKA_mark("lis===146###sois===3660###eois===3677###lif===26###soif===439###eoif===456###isc===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)")&&subpath[i] != '/'))) {
				AKA_mark("lis===147###sois===3683###eois===3687###lif===27###soif===462###eoif===466###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");i++;
			}

						AKA_mark("lis===148###sois===3691###eois===3695###lif===28###soif===470###eoif===474###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");l++;

					case '/': if(subpath[i++] == '/')AKA_mark("lis===149###sois===3698###eois===3707###lif===29###soif===477###eoif===486###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");

						AKA_mark("lis===150###sois===3711###eois===3717###lif===30###soif===490###eoif===496###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");break;

		}

	}

		AKA_mark("lis===153###sois===3726###eois===3735###lif===33###soif===505###eoif===514###ins===true###function===./app-framework-binder/src/afb-hreq.c/validsubpath(const char*)");return 1;

}

/** Instrumented function afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list) */
static void afb_hreq_reply_v(struct afb_hreq *hreq, unsigned status, struct MHD_Response *response, va_list args)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");AKA_fCall++;
		AKA_mark("lis===158###sois===3856###eois===3869###lif===2###soif===117###eoif===130###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");char *cookie;

		AKA_mark("lis===159###sois===3871###eois===3889###lif===3###soif===132###eoif===150###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");const char *k, *v;


		if (AKA_mark("lis===161###sois===3896###eois===3914###lif===5###soif===157###eoif===175###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)") && (AKA_mark("lis===161###sois===3896###eois===3914###lif===5###soif===157###eoif===175###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)")&&hreq->replied != 0)) {
		AKA_mark("lis===162###sois===3918###eois===3925###lif===6###soif===179###eoif===186###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");return;
	}
	else {AKA_mark("lis===-161-###sois===-3896-###eois===-389618-###lif===-5-###soif===-###eoif===-175-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");}


	/* Cant instrument this following code */
k = va_arg(args, const char *);
		while (AKA_mark("lis===165###sois===3968###eois===3977###lif===9###soif===229###eoif===238###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)") && (AKA_mark("lis===165###sois===3968###eois===3977###lif===9###soif===229###eoif===238###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)")&&k != NULL)) {
		/* Cant instrument this following code */
v = va_arg(args, const char *);
				AKA_mark("lis===167###sois===4017###eois===4057###lif===11###soif===278###eoif===318###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");MHD_add_response_header(response, k, v);

		/* Cant instrument this following code */
k = va_arg(args, const char *);
	}


		AKA_mark("lis===171###sois===4097###eois===4139###lif===15###soif===358###eoif===400###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");v = afb_context_uuid(&hreq->xreq.context);

		if (AKA_mark("lis===172###sois===4145###eois===4197###lif===16###soif===406###eoif===458###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)") && ((AKA_mark("lis===172###sois===4145###eois===4154###lif===16###soif===406###eoif===415###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)")&&v != NULL)	&&(AKA_mark("lis===172###sois===4158###eois===4197###lif===16###soif===419###eoif===458###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)")&&asprintf(&cookie, cookie_setter, v) > 0))) {
				AKA_mark("lis===173###sois===4203###eois===4273###lif===17###soif===464###eoif===534###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");MHD_add_response_header(response, MHD_HTTP_HEADER_SET_COOKIE, cookie);

				AKA_mark("lis===174###sois===4276###eois===4289###lif===18###soif===537###eoif===550###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");free(cookie);

	}
	else {AKA_mark("lis===-172-###sois===-4145-###eois===-414552-###lif===-16-###soif===-###eoif===-458-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");}

		AKA_mark("lis===176###sois===4294###eois===4349###lif===20###soif===555###eoif===610###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");MHD_queue_response(hreq->connection, status, response);

		AKA_mark("lis===177###sois===4351###eois===4382###lif===21###soif===612###eoif===643###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");MHD_destroy_response(response);


		AKA_mark("lis===179###sois===4385###eois===4403###lif===23###soif===646###eoif===664###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");hreq->replied = 1;

		if (AKA_mark("lis===180###sois===4409###eois===4429###lif===24###soif===670###eoif===690###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)") && (AKA_mark("lis===180###sois===4409###eois===4429###lif===24###soif===670###eoif===690###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)")&&hreq->suspended != 0)) {
				AKA_mark("lis===181###sois===4435###eois===4476###lif===25###soif===696###eoif===737###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");MHD_resume_connection (hreq->connection);

				AKA_mark("lis===182###sois===4479###eois===4499###lif===26###soif===740###eoif===760###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");hreq->suspended = 0;

				AKA_mark("lis===183###sois===4502###eois===4527###lif===27###soif===763###eoif===788###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");afb_hsrv_run(hreq->hsrv);

	}
	else {AKA_mark("lis===-180-###sois===-4409-###eois===-440920-###lif===-24-###soif===-###eoif===-690-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_v(struct afb_hreq*,unsigned,struct MHD_Response*,va_list)");}

}

/** Instrumented function afb_hreq_reply(struct afb_hreq*,unsigned,struct MHD_Response*) */
void afb_hreq_reply(struct afb_hreq *hreq, unsigned status, struct MHD_Response *response, ...)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_reply(struct afb_hreq*,unsigned,struct MHD_Response*)");AKA_fCall++;
		AKA_mark("lis===189###sois===4633###eois===4646###lif===2###soif===99###eoif===112###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply(struct afb_hreq*,unsigned,struct MHD_Response*)");va_list args;

		AKA_mark("lis===190###sois===4648###eois===4673###lif===3###soif===114###eoif===139###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply(struct afb_hreq*,unsigned,struct MHD_Response*)");va_start(args, response);

		AKA_mark("lis===191###sois===4675###eois===4722###lif===4###soif===141###eoif===188###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply(struct afb_hreq*,unsigned,struct MHD_Response*)");afb_hreq_reply_v(hreq, status, response, args);

		AKA_mark("lis===192###sois===4724###eois===4737###lif===5###soif===190###eoif===203###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply(struct afb_hreq*,unsigned,struct MHD_Response*)");va_end(args);

}

/** Instrumented function afb_hreq_reply_empty(struct afb_hreq*,unsigned) */
void afb_hreq_reply_empty(struct afb_hreq *hreq, unsigned status, ...)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_empty(struct afb_hreq*,unsigned)");AKA_fCall++;
		AKA_mark("lis===197###sois===4815###eois===4828###lif===2###soif===74###eoif===87###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_empty(struct afb_hreq*,unsigned)");va_list args;

		AKA_mark("lis===198###sois===4830###eois===4853###lif===3###soif===89###eoif===112###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_empty(struct afb_hreq*,unsigned)");va_start(args, status);

		AKA_mark("lis===199###sois===4855###eois===4958###lif===4###soif===114###eoif===217###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_empty(struct afb_hreq*,unsigned)");afb_hreq_reply_v(hreq, status, MHD_create_response_from_buffer(0, NULL, MHD_RESPMEM_PERSISTENT), args);

		AKA_mark("lis===200###sois===4960###eois===4973###lif===5###soif===219###eoif===232###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_empty(struct afb_hreq*,unsigned)");va_end(args);

}

/** Instrumented function afb_hreq_reply_static(struct afb_hreq*,unsigned,size_t,const char*) */
void afb_hreq_reply_static(struct afb_hreq *hreq, unsigned status, size_t size, const char *buffer, ...)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_static(struct afb_hreq*,unsigned,size_t,const char*)");AKA_fCall++;
		AKA_mark("lis===205###sois===5085###eois===5098###lif===2###soif===108###eoif===121###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_static(struct afb_hreq*,unsigned,size_t,const char*)");va_list args;

		AKA_mark("lis===206###sois===5100###eois===5123###lif===3###soif===123###eoif===146###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_static(struct afb_hreq*,unsigned,size_t,const char*)");va_start(args, buffer);

		AKA_mark("lis===207###sois===5125###eois===5250###lif===4###soif===148###eoif===273###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_static(struct afb_hreq*,unsigned,size_t,const char*)");afb_hreq_reply_v(hreq, status, MHD_create_response_from_buffer((unsigned)size, (char*)buffer, MHD_RESPMEM_PERSISTENT), args);

		AKA_mark("lis===208###sois===5252###eois===5265###lif===5###soif===275###eoif===288###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_static(struct afb_hreq*,unsigned,size_t,const char*)");va_end(args);

}

/** Instrumented function afb_hreq_reply_copy(struct afb_hreq*,unsigned,size_t,const char*) */
void afb_hreq_reply_copy(struct afb_hreq *hreq, unsigned status, size_t size, const char *buffer, ...)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_copy(struct afb_hreq*,unsigned,size_t,const char*)");AKA_fCall++;
		AKA_mark("lis===213###sois===5375###eois===5388###lif===2###soif===106###eoif===119###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_copy(struct afb_hreq*,unsigned,size_t,const char*)");va_list args;

		AKA_mark("lis===214###sois===5390###eois===5413###lif===3###soif===121###eoif===144###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_copy(struct afb_hreq*,unsigned,size_t,const char*)");va_start(args, buffer);

		AKA_mark("lis===215###sois===5415###eois===5539###lif===4###soif===146###eoif===270###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_copy(struct afb_hreq*,unsigned,size_t,const char*)");afb_hreq_reply_v(hreq, status, MHD_create_response_from_buffer((unsigned)size, (char*)buffer, MHD_RESPMEM_MUST_COPY), args);

		AKA_mark("lis===216###sois===5541###eois===5554###lif===5###soif===272###eoif===285###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_copy(struct afb_hreq*,unsigned,size_t,const char*)");va_end(args);

}

/** Instrumented function afb_hreq_reply_free(struct afb_hreq*,unsigned,size_t,char*) */
void afb_hreq_reply_free(struct afb_hreq *hreq, unsigned status, size_t size, char *buffer, ...)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_free(struct afb_hreq*,unsigned,size_t,char*)");AKA_fCall++;
		AKA_mark("lis===221###sois===5658###eois===5671###lif===2###soif===100###eoif===113###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_free(struct afb_hreq*,unsigned,size_t,char*)");va_list args;

		AKA_mark("lis===222###sois===5673###eois===5696###lif===3###soif===115###eoif===138###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_free(struct afb_hreq*,unsigned,size_t,char*)");va_start(args, buffer);

		AKA_mark("lis===223###sois===5698###eois===5815###lif===4###soif===140###eoif===257###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_free(struct afb_hreq*,unsigned,size_t,char*)");afb_hreq_reply_v(hreq, status, MHD_create_response_from_buffer((unsigned)size, buffer, MHD_RESPMEM_MUST_FREE), args);

		AKA_mark("lis===224###sois===5817###eois===5830###lif===5###soif===259###eoif===272###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_free(struct afb_hreq*,unsigned,size_t,char*)");va_end(args);

}

#if defined(USE_MAGIC_MIME_TYPE)

#if !defined(MAGIC_DB)
#define MAGIC_DB "/usr/share/misc/magic.mgc"
#endif

static magic_t lazy_libmagic()
{
	static int done = 0;
	static magic_t result = NULL;

	if (!done) {
		done = 1;
		/* MAGIC_MIME tells magic to return a mime of the file,
			 but you can specify different things */
		INFO("Loading mimetype default magic database");
		result = magic_open(MAGIC_MIME_TYPE);
		if (result == NULL) {
			ERROR("unable to initialize magic library");
		}
		/* Warning: should not use NULL for DB
				[libmagic bug wont pass efence check] */
		else if (magic_load(result, MAGIC_DB) != 0) {
			ERROR("cannot load magic database: %s", magic_error(result));
			magic_close(result);
			result = NULL;
		}
	}

	return result;
}

static const char *magic_mimetype_fd(int fd)
{
	magic_t lib = lazy_libmagic();
	return lib ? magic_descriptor(lib, fd) : NULL;
}

#endif

/** Instrumented function mimetype_fd_name(int,const char*) */
static const char *mimetype_fd_name(int fd, const char *filename)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/mimetype_fd_name(int,const char*)");AKA_fCall++;
		AKA_mark("lis===269###sois===6801###eois===6827###lif===2###soif===69###eoif===95###ins===true###function===./app-framework-binder/src/afb-hreq.c/mimetype_fd_name(int,const char*)");const char *result = NULL;


#if defined(INFER_EXTENSION)
	/*
	 * Set some well-known extensions
	 * Note that it is mandatory for example for css files in order to provide
	 * right mimetype that must be text/css (otherwise chrome browser will not
	 * load correctly css file) while libmagic returns text/plain.
	 */
	const char *extension = strrchr(filename, '.');
	if (extension) {
		static const char *const known[][2] = {
			/* keep it sorted for dichotomic search */
			{ ".css",	"text/css" },
			{ ".gif",	"image/gif" },
			{ ".html",	"text/html" },
			{ ".htm",	"text/html" },
			{ ".ico",	"image/x-icon"},
			{ ".jpeg",	"image/jpeg" },
			{ ".jpg",	"image/jpeg" },
			{ ".js",	"text/javascript" },
			{ ".json",	"application/json" },
			{ ".mp3",	"audio/mpeg" },
			{ ".png",	"image/png" },
			{ ".svg",	"image/svg+xml" },
			{ ".ttf",	"application/x-font-ttf"},
			{ ".txt",	"text/plain" },
			{ ".wav",	"audio/x-wav" },
			{ ".xht",	"application/xhtml+xml" },
			{ ".xhtml",	"application/xhtml+xml" },
			{ ".xml",	"application/xml" }
		};
		int i, c, l = 0, u = sizeof known / sizeof *known;
		while (l < u) {
			i = (l + u) >> 1;
			c = strcasecmp(extension, known[i][0]);
			if (!c) {
				result = known[i][1];
				break;
			}
			if (c < 0)
				u = i;
			else
				l = i + 1;
		}
	}
#endif
#if defined(USE_MAGIC_MIME_TYPE)
	if (result == NULL)
		result = magic_mimetype_fd(fd);
#endif
		AKA_mark("lis===320###sois===8199###eois===8213###lif===53###soif===1467###eoif===1481###ins===true###function===./app-framework-binder/src/afb-hreq.c/mimetype_fd_name(int,const char*)");return result;

}

/** Instrumented function req_destroy(struct afb_xreq*) */
static void req_destroy(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===325###sois===8267###eois===8282###lif===2###soif===50###eoif===65###ins===true###function===./app-framework-binder/src/afb-hre/* Cant instrument this following code */
q.c/req_destroy(struct afb_xreq*)");struct afb_hreq
 /* Cant instrument this following code */
*hreq = CONTAINER_OF_XREQ(struct afb_hreq, xreq);
		AKA_mark("lis===326###sois===8334###eois===8357###lif===3###soif===117###eoif===140###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");struct hreq_data *data;


		if (AKA_mark("lis===328###sois===8364###eois===8386###lif===5###soif===147###eoif===169###ifc===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)") && (AKA_mark("lis===328###sois===8364###eois===8386###lif===5###soif===147###eoif===169###isc===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)")&&hreq->postform != NULL)) {
		AKA_mark("lis===329###sois===8390###eois===8433###lif===6###soif===173###eoif===216###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");MHD_destroy_post_processor(hreq->postform);
	}
	else {AKA_mark("lis===-328-###sois===-8364-###eois===-836422-###lif===-5-###soif===-###eoif===-169-###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");}

		if (AKA_mark("lis===330###sois===8439###eois===8460###lif===7###soif===222###eoif===243###ifc===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)") && (AKA_mark("lis===330###sois===8439###eois===8460###lif===7###soif===222###eoif===243###isc===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)")&&hreq->tokener != NULL)) {
		AKA_mark("lis===331###sois===8464###eois===8497###lif===8###soif===247###eoif===280###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");json_tokener_free(hreq->tokener);
	}
	else {AKA_mark("lis===-330-###sois===-8439-###eois===-843921-###lif===-7-###soif===-###eoif===-243-###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");}


		AKA_mark("lis===333###sois===8505###eois===8523###lif===10###soif===288###eoif===306###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");for (data = hreq->data;AKA_mark("lis===333###sois===8524###eois===8528###lif===10###soif===307###eoif===311###ifc===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)") && AKA_mark("lis===333###sois===8524###eois===8528###lif===10###soif===307###eoif===311###isc===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)")&&data;({AKA_mark("lis===333###sois===8530###eois===8547###lif===10###soif===313###eoif===330###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");data = hreq->data;})) {
				AKA_mark("lis===334###sois===8553###eois===8577###lif===11###soif===336###eoif===360###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");hreq->data = data->next;

				if (AKA_mark("lis===335###sois===8584###eois===8594###lif===12###soif===367###eoif===377###ifc===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)") && (AKA_mark("lis===335###sois===8584###eois===8594###lif===12###soif===367###eoif===377###isc===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)")&&data->path)) {
						AKA_mark("lis===336###sois===8601###eois===8620###lif===13###soif===384###eoif===403###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");unlink(data->path);

						AKA_mark("lis===337###sois===8624###eois===8641###lif===14###soif===407###eoif===424###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");free(data->path);

		}
		else {AKA_mark("lis===-335-###sois===-8584-###eois===-858410-###lif===-12-###soif===-###eoif===-377-###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");}

				AKA_mark("lis===339###sois===8648###eois===8664###lif===16###soif===431###eoif===447###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");free(data->key);

				AKA_mark("lis===340###sois===8667###eois===8685###lif===17###soif===450###eoif===468###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");free(data->value);

				AKA_mark("lis===341###sois===8688###eois===8699###lif===18###soif===471###eoif===482###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");free(data);

	}

		AKA_mark("lis===343###sois===8704###eois===8748###lif===20###soif===487###eoif===531###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");afb_context_disconnect(&hreq->xreq.context);

		AKA_mark("lis===344###sois===8750###eois===8778###lif===21###soif===533###eoif===561###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");json_object_put(hreq->json);

		AKA_mark("lis===345###sois===8780###eois===8823###lif===22###soif===563###eoif===606###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");free((char*)hreq->xreq.request.called_api);

		AKA_mark("lis===346###sois===8825###eois===8869###lif===23###soif===608###eoif===652###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");free((char*)hreq->xreq.request.called_verb);

		AKA_mark("lis===347###sois===8871###eois===8882###lif===24###soif===654###eoif===665###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_destroy(struct afb_xreq*)");free(hreq);

}

/** Instrumented function afb_hreq_addref(struct afb_hreq*) */
void afb_hreq_addref(struct afb_hreq *hreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_addref(struct afb_hreq*)");AKA_fCall++;
		AKA_mark("lis===352###sois===8933###eois===8971###lif===2###soif===47###eoif===85###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_addref(struct afb_hreq*)");afb_xreq_unhooked_addref(&hreq->xreq);

}

/** Instrumented function afb_hreq_unref(struct afb_hreq*) */
void afb_hreq_unref(struct afb_hreq *hreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_unref(struct afb_hreq*)");AKA_fCall++;
		if (AKA_mark("lis===357###sois===9025###eois===9038###lif===2###soif===50###eoif===63###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unref(struct afb_hreq*)") && (AKA_mark("lis===357###sois===9025###eois===9038###lif===2###soif===50###eoif===63###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unref(struct afb_hreq*)")&&hreq->replied)) {
		AKA_mark("lis===358###sois===9042###eois===9065###lif===3###soif===67###eoif===90###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unref(struct afb_hreq*)");hreq->xreq.replied = 1;
	}
	else {AKA_mark("lis===-357-###sois===-9025-###eois===-902513-###lif===-2-###soif===-###eoif===-63-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unref(struct afb_hreq*)");}

		AKA_mark("lis===359###sois===9067###eois===9104###lif===4###soif===92###eoif===129###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unref(struct afb_hreq*)");afb_xreq_unhooked_unref(&hreq->xreq);

}

/*
 * Removes the 'prefix' of 'length' from the tail of 'hreq'
 * if and only if the prefix exists and is terminated by a leading
 * slash
 */
/** Instrumented function afb_hreq_unprefix(struct afb_hreq*,const char*,size_t) */
int afb_hreq_unprefix(struct afb_hreq *hreq, const char *prefix, size_t length)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)");AKA_fCall++;
	/* check the prefix ? */
		if (AKA_mark("lis===370###sois===9364###eois===9487###lif===3###soif===113###eoif===236###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)") && (((AKA_mark("lis===370###sois===9364###eois===9386###lif===3###soif===113###eoif===135###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)")&&length > hreq->lentail)	||((AKA_mark("lis===370###sois===9391###eois===9409###lif===3###soif===140###eoif===158###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)")&&hreq->tail[length])	&&(AKA_mark("lis===370###sois===9413###eois===9438###lif===3###soif===162###eoif===187###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)")&&hreq->tail[length] != '/')))	||(AKA_mark("lis===371###sois===9448###eois===9487###lif===4###soif===197###eoif===236###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)")&&strncasecmp(prefix, hreq->tail, length)))) {
		AKA_mark("lis===372###sois===9491###eois===9500###lif===5###soif===240###eoif===249###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)");return 0;
	}
	else {AKA_mark("lis===-370-###sois===-9364-###eois===-9364123-###lif===-3-###soif===-###eoif===-236-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)");}


	/* removes successives / */
		while (AKA_mark("lis===375###sois===9539###eois===9594###lif===8###soif===288###eoif===343###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)") && ((AKA_mark("lis===375###sois===9539###eois===9561###lif===8###soif===288###eoif===310###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)")&&length < hreq->lentail)	&&(AKA_mark("lis===375###sois===9565###eois===9594###lif===8###soif===314###eoif===343###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)")&&hreq->tail[length + 1] == '/'))) {
		AKA_mark("lis===376###sois===9598###eois===9607###lif===9###soif===347###eoif===356###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)");length++;
	}


	/* update the tail */
		AKA_mark("lis===379###sois===9633###eois===9657###lif===12###soif===382###eoif===406###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)");hreq->lentail -= length;

		AKA_mark("lis===380###sois===9659###eois===9680###lif===13###soif===408###eoif===429###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)");hreq->tail += length;

		AKA_mark("lis===381###sois===9682###eois===9691###lif===14###soif===431###eoif===440###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_unprefix(struct afb_hreq*,const char*,size_t)");return 1;

}

/** Instrumented function afb_hreq_valid_tail(struct afb_hreq*) */
int afb_hreq_valid_tail(struct afb_hreq *hreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_valid_tail(struct afb_hreq*)");AKA_fCall++;
		AKA_mark("lis===386###sois===9745###eois===9777###lif===2###soif===50###eoif===82###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_valid_tail(struct afb_hreq*)");return validsubpath(hreq->tail);

}

/** Instrumented function afb_hreq_reply_error(struct afb_hreq*,unsigned int) */
void afb_hreq_reply_error(struct afb_hreq *hreq, unsigned int status)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_error(struct afb_hreq*,unsigned int)");AKA_fCall++;
		AKA_mark("lis===391###sois===9854###eois===9895###lif===2###soif===73###eoif===114###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_error(struct afb_hreq*,unsigned int)");afb_hreq_reply_empty(hreq, status, NULL);

}

/** Instrumented function afb_hreq_redirect_to_ending_slash_if_needed(struct afb_hreq*) */
int afb_hreq_redirect_to_ending_slash_if_needed(struct afb_hreq *hreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to_ending_slash_if_needed(struct afb_hreq*)");AKA_fCall++;
		AKA_mark("lis===396###sois===9973###eois===9985###lif===2###soif===74###eoif===86###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to_ending_slash_if_needed(struct afb_hreq*)");char *tourl;


		if (AKA_mark("lis===398###sois===9992###eois===10026###lif===4###soif===93###eoif===127###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to_ending_slash_if_needed(struct afb_hreq*)") && (AKA_mark("lis===398###sois===9992###eois===10026###lif===4###soif===93###eoif===127###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to_ending_slash_if_needed(struct afb_hreq*)")&&hreq->url[hreq->lenurl - 1] == '/')) {
		AKA_mark("lis===399###sois===10030###eois===10039###lif===5###soif===131###eoif===140###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to_ending_slash_if_needed(struct afb_hreq*)");return 0;
	}
	else {AKA_mark("lis===-398-###sois===-9992-###eois===-999234-###lif===-4-###soif===-###eoif===-127-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to_ending_slash_if_needed(struct afb_hreq*)");}


	/* the redirect is needed for reliability of relative path */
		AKA_mark("lis===402###sois===10105###eois===10138###lif===8###soif===206###eoif===239###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to_ending_slash_if_needed(struct afb_hreq*)");tourl = alloca(hreq->lenurl + 2);

		AKA_mark("lis===403###sois===10140###eois===10179###lif===9###soif===241###eoif===280###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to_ending_slash_if_needed(struct afb_hreq*)");memcpy(tourl, hreq->url, hreq->lenurl);

		AKA_mark("lis===404###sois===10181###eois===10207###lif===10###soif===282###eoif===308###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to_ending_slash_if_needed(struct afb_hreq*)");tourl[hreq->lenurl] = '/';

		AKA_mark("lis===405###sois===10209###eois===10237###lif===11###soif===310###eoif===338###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to_ending_slash_if_needed(struct afb_hreq*)");tourl[hreq->lenurl + 1] = 0;

		AKA_mark("lis===406###sois===10239###eois===10276###lif===12###soif===340###eoif===377###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to_ending_slash_if_needed(struct afb_hreq*)");afb_hreq_redirect_to(hreq, tourl, 1);

		AKA_mark("lis===407###sois===10278###eois===10287###lif===13###soif===379###eoif===388###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to_ending_slash_if_needed(struct afb_hreq*)");return 1;

}

/** Instrumented function afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*) */
int afb_hreq_reply_file_if_exist(struct afb_hreq *hreq, int dirfd, const char *filename)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");AKA_fCall++;
		AKA_mark("lis===412###sois===10383###eois===10390###lif===2###soif===92###eoif===99###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");int rc;

		AKA_mark("lis===413###sois===10392###eois===10399###lif===3###soif===101###eoif===108###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");int fd;

		AKA_mark("lis===414###sois===10401###eois===10421###lif===4###soif===110###eoif===130###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");unsigned int status;

		AKA_mark("lis===415###sois===10423###eois===10438###lif===5###soif===132###eoif===147###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");struct stat st;

		AKA_mark("lis===416###sois===10440###eois===10461###lif===6###soif===149###eoif===170###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");char etag[1 + 2 * 8];

		AKA_mark("lis===417###sois===10463###eois===10479###lif===7###soif===172###eoif===188###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");const char *inm;

		AKA_mark("lis===418###sois===10481###eois===10511###lif===8###soif===190###eoif===220###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");struct MHD_Response *response;

		AKA_mark("lis===419###sois===10513###eois===10534###lif===9###soif===222###eoif===243###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");const char *mimetype;


	/* Opens the file or directory */
		if (AKA_mark("lis===422###sois===10576###eois===10587###lif===12###soif===285###eoif===296###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)") && (AKA_mark("lis===422###sois===10576###eois===10587###lif===12###soif===285###eoif===296###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&filename[0])) {
				AKA_mark("lis===423###sois===10593###eois===10632###lif===13###soif===302###eoif===341###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");fd = openat(dirfd, filename, O_RDONLY);

				if (AKA_mark("lis===424###sois===10639###eois===10645###lif===14###soif===348###eoif===354###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)") && (AKA_mark("lis===424###sois===10639###eois===10645###lif===14###soif===348###eoif===354###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&fd < 0)) {
						if (AKA_mark("lis===425###sois===10656###eois===10671###lif===15###soif===365###eoif===380###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)") && (AKA_mark("lis===425###sois===10656###eois===10671###lif===15###soif===365###eoif===380###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&errno == ENOENT)) {
				AKA_mark("lis===426###sois===10677###eois===10686###lif===16###soif===386###eoif===395###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");return 0;
			}
			else {AKA_mark("lis===-425-###sois===-10656-###eois===-1065615-###lif===-15-###soif===-###eoif===-380-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");}

						AKA_mark("lis===427###sois===10690###eois===10737###lif===17###soif===399###eoif===446###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");afb_hreq_reply_error(hreq, MHD_HTTP_FORBIDDEN);

						AKA_mark("lis===428###sois===10741###eois===10750###lif===18###soif===450###eoif===459###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");return 1;

		}
		else {AKA_mark("lis===-424-###sois===-10639-###eois===-106396-###lif===-14-###soif===-###eoif===-354-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");}

	}
	else {
				AKA_mark("lis===431###sois===10767###eois===10783###lif===21###soif===476###eoif===492###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");fd = dup(dirfd);

				if (AKA_mark("lis===432###sois===10790###eois===10796###lif===22###soif===499###eoif===505###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)") && (AKA_mark("lis===432###sois===10790###eois===10796###lif===22###soif===499###eoif===505###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&fd < 0)) {
						AKA_mark("lis===433###sois===10803###eois===10862###lif===23###soif===512###eoif===571###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");afb_hreq_reply_error(hreq, MHD_HTTP_INTERNAL_SERVER_ERROR);

						AKA_mark("lis===434###sois===10866###eois===10875###lif===24###soif===575###eoif===584###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");return 1;

		}
		else {AKA_mark("lis===-432-###sois===-10790-###eois===-107906-###lif===-22-###soif===-###eoif===-505-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");}

	}


	/* Retrieves file's status */
		if (AKA_mark("lis===439###sois===10920###eois===10939###lif===29###soif===629###eoif===648###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)") && (AKA_mark("lis===439###sois===10920###eois===10939###lif===29###soif===629###eoif===648###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&fstat(fd, &st) != 0)) {
				AKA_mark("lis===440###sois===10945###eois===10955###lif===30###soif===654###eoif===664###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");close(fd);

				AKA_mark("lis===441###sois===10958###eois===11017###lif===31###soif===667###eoif===726###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");afb_hreq_reply_error(hreq, MHD_HTTP_INTERNAL_SERVER_ERROR);

				AKA_mark("lis===442###sois===11020###eois===11029###lif===32###soif===729###eoif===738###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");return 1;

	}
	else {AKA_mark("lis===-439-###sois===-10920-###eois===-1092019-###lif===-29-###soif===-###eoif===-648-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");}


	/* serve directory */
		if (AKA_mark("lis===446###sois===11062###eois===11081###lif===36###soif===771###eoif===790###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)") && (AKA_mark("lis===446###sois===11062###eois===11081###lif===36###soif===771###eoif===790###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&S_ISDIR(st.st_mode))) {
				AKA_mark("lis===447###sois===11087###eois===11142###lif===37###soif===796###eoif===851###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");rc = afb_hreq_redirect_to_ending_slash_if_needed(hreq);

				if (AKA_mark("lis===448###sois===11149###eois===11156###lif===38###soif===858###eoif===865###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)") && (AKA_mark("lis===448###sois===11149###eois===11156###lif===38###soif===858###eoif===865###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&rc == 0)) {
						AKA_mark("lis===449###sois===11163###eois===11217###lif===39###soif===872###eoif===926###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");static const char *indexes[] = { "index.html", NULL };

						AKA_mark("lis===450###sois===11221###eois===11231###lif===40###soif===930###eoif===940###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");int i = 0;

						while (AKA_mark("lis===451###sois===11242###eois===11260###lif===41###soif===951###eoif===969###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)") && (AKA_mark("lis===451###sois===11242###eois===11260###lif===41###soif===951###eoif===969###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&indexes[i] != NULL)) {
								if (AKA_mark("lis===452###sois===11272###eois===11311###lif===42###soif===981###eoif===1020###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)") && (AKA_mark("lis===452###sois===11272###eois===11311###lif===42###soif===981###eoif===1020###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&faccessat(fd, indexes[i], R_OK, 0) == 0)) {
										AKA_mark("lis===453###sois===11320###eois===11376###lif===43###soif===1029###eoif===1085###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");rc = afb_hreq_reply_file_if_exist(hreq, fd, indexes[i]);

										AKA_mark("lis===454###sois===11382###eois===11388###lif===44###soif===1091###eoif===1097###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");break;

				}
				else {AKA_mark("lis===-452-###sois===-11272-###eois===-1127239-###lif===-42-###soif===-###eoif===-1020-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");}

								AKA_mark("lis===456###sois===11399###eois===11403###lif===46###soif===1108###eoif===1112###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");i++;

			}

		}
		else {AKA_mark("lis===-448-###sois===-11149-###eois===-111497-###lif===-38-###soif===-###eoif===-865-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");}

				AKA_mark("lis===459###sois===11415###eois===11425###lif===49###soif===1124###eoif===1134###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");close(fd);

				AKA_mark("lis===460###sois===11428###eois===11438###lif===50###soif===1137###eoif===1147###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");return rc;

	}
	else {AKA_mark("lis===-446-###sois===-11062-###eois===-1106219-###lif===-36-###soif===-###eoif===-790-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");}


	/* Don't serve special files */
		if (AKA_mark("lis===464###sois===11481###eois===11501###lif===54###soif===1190###eoif===1210###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)") && (AKA_mark("lis===464###sois===11481###eois===11501###lif===54###soif===1190###eoif===1210###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&!S_ISREG(st.st_mode))) {
				AKA_mark("lis===465###sois===11507###eois===11517###lif===55###soif===1216###eoif===1226###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");close(fd);

				AKA_mark("lis===466###sois===11520###eois===11567###lif===56###soif===1229###eoif===1276###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");afb_hreq_reply_error(hreq, MHD_HTTP_FORBIDDEN);

				AKA_mark("lis===467###sois===11570###eois===11579###lif===57###soif===1279###eoif===1288###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");return 1;

	}
	else {AKA_mark("lis===-464-###sois===-11481-###eois===-1148120-###lif===-54-###soif===-###eoif===-1210-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");}


	/* Check the method */
		if (AKA_mark("lis===471###sois===11613###eois===11669###lif===61###soif===1322###eoif===1378###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)") && (AKA_mark("lis===471###sois===11613###eois===11669###lif===61###soif===1322###eoif===1378###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&(hreq->method & (afb_method_get | afb_method_head)) == 0)) {
				AKA_mark("lis===472###sois===11675###eois===11685###lif===62###soif===1384###eoif===1394###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");close(fd);

				AKA_mark("lis===473###sois===11688###eois===11744###lif===63###soif===1397###eoif===1453###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");afb_hreq_reply_error(hreq, MHD_HTTP_METHOD_NOT_ALLOWED);

				AKA_mark("lis===474###sois===11747###eois===11756###lif===64###soif===1456###eoif===1465###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");return 1;

	}
	else {AKA_mark("lis===-471-###sois===-11613-###eois===-1161356-###lif===-61-###soif===-###eoif===-1378-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");}


	/* computes the etag */
		AKA_mark("lis===478###sois===11787###eois===11888###lif===68###soif===1496###eoif===1597###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");sprintf(etag, "%08X%08X", ((int)(st.st_mtim.tv_sec) ^ (int)(st.st_mtim.tv_nsec)), (int)(st.st_size));


	/* checks the etag */
		AKA_mark("lis===481###sois===11914###eois===12014###lif===71###soif===1623###eoif===1723###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");inm = MHD_lookup_connection_value(hreq->connection, MHD_HEADER_KIND, MHD_HTTP_HEADER_IF_NONE_MATCH);

		if (AKA_mark("lis===482###sois===12020###eois===12049###lif===72###soif===1729###eoif===1758###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)") && ((AKA_mark("lis===482###sois===12020###eois===12023###lif===72###soif===1729###eoif===1732###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&inm)	&&(AKA_mark("lis===482###sois===12027###eois===12049###lif===72###soif===1736###eoif===1758###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&0 == strcmp(inm, etag)))) {
		/* etag ok, return NOT MODIFIED */
				AKA_mark("lis===484###sois===12092###eois===12102###lif===74###soif===1801###eoif===1811###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");close(fd);

				AKA_mark("lis===485###sois===12105###eois===12143###lif===75###soif===1814###eoif===1852###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");DEBUG("Not Modified: [%s]", filename);

				AKA_mark("lis===486###sois===12146###eois===12230###lif===76###soif===1855###eoif===1939###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");response = MHD_create_response_from_buffer(0, empty_string, MHD_RESPMEM_PERSISTENT);

				AKA_mark("lis===487###sois===12233###eois===12264###lif===77###soif===1942###eoif===1973###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");status = MHD_HTTP_NOT_MODIFIED;

	}
	else {
		/* check the size */
				if (AKA_mark("lis===490###sois===12304###eois===12345###lif===80###soif===2013###eoif===2054###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)") && (AKA_mark("lis===490###sois===12304###eois===12345###lif===80###soif===2013###eoif===2054###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&st.st_size != (off_t) (size_t) st.st_size)) {
						AKA_mark("lis===491###sois===12352###eois===12362###lif===81###soif===2061###eoif===2071###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");close(fd);

						AKA_mark("lis===492###sois===12366###eois===12425###lif===82###soif===2075###eoif===2134###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");afb_hreq_reply_error(hreq, MHD_HTTP_INTERNAL_SERVER_ERROR);

						AKA_mark("lis===493###sois===12429###eois===12438###lif===83###soif===2138###eoif===2147###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");return 1;

		}
		else {AKA_mark("lis===-490-###sois===-12304-###eois===-1230441-###lif===-80-###soif===-###eoif===-2054-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");}


		/* create the response */
				AKA_mark("lis===497###sois===12474###eois===12538###lif===87###soif===2183###eoif===2247###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");response = MHD_create_response_from_fd((size_t) st.st_size, fd);

				AKA_mark("lis===498###sois===12541###eois===12562###lif===88###soif===2250###eoif===2271###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");status = MHD_HTTP_OK;


		/* set the type */
				AKA_mark("lis===501###sois===12587###eois===12629###lif===91###soif===2296###eoif===2338###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");mimetype = mimetype_fd_name(fd, filename);

				if (AKA_mark("lis===502###sois===12636###eois===12652###lif===92###soif===2345###eoif===2361###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)") && (AKA_mark("lis===502###sois===12636###eois===12652###lif===92###soif===2345###eoif===2361###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)")&&mimetype != NULL)) {
			AKA_mark("lis===503###sois===12657###eois===12731###lif===93###soif===2366###eoif===2440###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");MHD_add_response_header(response, MHD_HTTP_HEADER_CONTENT_TYPE, mimetype);
		}
		else {AKA_mark("lis===-502-###sois===-12636-###eois===-1263616-###lif===-92-###soif===-###eoif===-2361-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");}

	}


	/* fills the value and send */
		AKA_mark("lis===507###sois===12769###eois===12902###lif===97###soif===2478###eoif===2611###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");afb_hreq_reply(hreq, status, response,
			MHD_HTTP_HEADER_CACHE_CONTROL, hreq->cacheTimeout,
			MHD_HTTP_HEADER_ETAG, etag,
			NULL);

		AKA_mark("lis===511###sois===12904###eois===12913###lif===101###soif===2613###eoif===2622###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file_if_exist(struct afb_hreq*,int,const char*)");return 1;

}

/** Instrumented function afb_hreq_reply_file(struct afb_hreq*,int,const char*) */
int afb_hreq_reply_file(struct afb_hreq *hreq, int dirfd, const char *filename)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file(struct afb_hreq*,int,const char*)");AKA_fCall++;
		AKA_mark("lis===516###sois===13000###eois===13061###lif===2###soif===83###eoif===144###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file(struct afb_hreq*,int,const char*)");int rc = afb_hreq_reply_file_if_exist(hreq, dirfd, filename);

		if (AKA_mark("lis===517###sois===13067###eois===13074###lif===3###soif===150###eoif===157###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file(struct afb_hreq*,int,const char*)") && (AKA_mark("lis===517###sois===13067###eois===13074###lif===3###soif===150###eoif===157###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file(struct afb_hreq*,int,const char*)")&&rc == 0)) {
		AKA_mark("lis===518###sois===13078###eois===13125###lif===4###soif===161###eoif===208###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file(struct afb_hreq*,int,const char*)");afb_hreq_reply_error(hreq, MHD_HTTP_NOT_FOUND);
	}
	else {AKA_mark("lis===-517-###sois===-13067-###eois===-130677-###lif===-3-###soif===-###eoif===-157-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file(struct afb_hreq*,int,const char*)");}

		AKA_mark("lis===519###sois===13127###eois===13136###lif===5###soif===210###eoif===219###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_file(struct afb_hreq*,int,const char*)");return 1;

}

/** Instrumented function afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*) */
int afb_hreq_reply_locale_file_if_exist(struct afb_hreq *hreq, struct locale_search *search, const char *filename)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");AKA_fCall++;
		AKA_mark("lis===524###sois===13258###eois===13265###lif===2###soif===118###eoif===125###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");int rc;

		AKA_mark("lis===525###sois===13267###eois===13274###lif===3###soif===127###eoif===134###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");int fd;

		AKA_mark("lis===526###sois===13276###eois===13296###lif===4###soif===136###eoif===156###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");unsigned int status;

		AKA_mark("lis===527###sois===13298###eois===13313###lif===5###soif===158###eoif===173###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");struct stat st;

		AKA_mark("lis===528###sois===13315###eois===13336###lif===6###soif===175###eoif===196###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");char etag[1 + 2 * 8];

		AKA_mark("lis===529###sois===13338###eois===13354###lif===7###soif===198###eoif===214###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");const char *inm;

		AKA_mark("lis===530###sois===13356###eois===13386###lif===8###soif===216###eoif===246###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");struct MHD_Response *response;

		AKA_mark("lis===531###sois===13388###eois===13409###lif===9###soif===248###eoif===269###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");const char *mimetype;


	/* Opens the file or directory */
		AKA_mark("lis===534###sois===13447###eois===13519###lif===12###soif===307###eoif===379###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");fd = locale_search_open(search, filename[0] ? filename : ".", O_RDONLY);

		if (AKA_mark("lis===535###sois===13525###eois===13531###lif===13###soif===385###eoif===391###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)") && (AKA_mark("lis===535###sois===13525###eois===13531###lif===13###soif===385###eoif===391###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&fd < 0)) {
				if (AKA_mark("lis===536###sois===13541###eois===13556###lif===14###soif===401###eoif===416###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)") && (AKA_mark("lis===536###sois===13541###eois===13556###lif===14###soif===401###eoif===416###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&errno == ENOENT)) {
			AKA_mark("lis===537###sois===13561###eois===13570###lif===15###soif===421###eoif===430###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");return 0;
		}
		else {AKA_mark("lis===-536-###sois===-13541-###eois===-1354115-###lif===-14-###soif===-###eoif===-416-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");}

				AKA_mark("lis===538###sois===13573###eois===13620###lif===16###soif===433###eoif===480###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");afb_hreq_reply_error(hreq, MHD_HTTP_FORBIDDEN);

				AKA_mark("lis===539###sois===13623###eois===13632###lif===17###soif===483###eoif===492###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");return 1;

	}
	else {AKA_mark("lis===-535-###sois===-13525-###eois===-135256-###lif===-13-###soif===-###eoif===-391-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");}


	/* Retrieves file's status */
		if (AKA_mark("lis===543###sois===13673###eois===13692###lif===21###soif===533###eoif===552###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)") && (AKA_mark("lis===543###sois===13673###eois===13692###lif===21###soif===533###eoif===552###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&fstat(fd, &st) != 0)) {
				AKA_mark("lis===544###sois===13698###eois===13708###lif===22###soif===558###eoif===568###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");close(fd);

				AKA_mark("lis===545###sois===13711###eois===13770###lif===23###soif===571###eoif===630###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");afb_hreq_reply_error(hreq, MHD_HTTP_INTERNAL_SERVER_ERROR);

				AKA_mark("lis===546###sois===13773###eois===13782###lif===24###soif===633###eoif===642###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");return 1;

	}
	else {AKA_mark("lis===-543-###sois===-13673-###eois===-1367319-###lif===-21-###soif===-###eoif===-552-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");}


	/* serve directory */
		if (AKA_mark("lis===550###sois===13815###eois===13834###lif===28###soif===675###eoif===694###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)") && (AKA_mark("lis===550###sois===13815###eois===13834###lif===28###soif===675###eoif===694###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&S_ISDIR(st.st_mode))) {
				AKA_mark("lis===551###sois===13840###eois===13895###lif===29###soif===700###eoif===755###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");rc = afb_hreq_redirect_to_ending_slash_if_needed(hreq);

				if (AKA_mark("lis===552###sois===13902###eois===13909###lif===30###soif===762###eoif===769###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)") && (AKA_mark("lis===552###sois===13902###eois===13909###lif===30###soif===762###eoif===769###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&rc == 0)) {
						AKA_mark("lis===553###sois===13916###eois===13970###lif===31###soif===776###eoif===830###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");static const char *indexes[] = { "index.html", NULL };

						AKA_mark("lis===554###sois===13974###eois===13984###lif===32###soif===834###eoif===844###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");int i = 0;

						AKA_mark("lis===555###sois===13988###eois===14021###lif===33###soif===848###eoif===881###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");size_t length = strlen(filename);

						AKA_mark("lis===556###sois===14025###eois===14061###lif===34###soif===885###eoif===921###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");char *extname = alloca(length + 30);
 /* 30 is enough to old data of indexes */
						AKA_mark("lis===557###sois===14107###eois===14141###lif===35###soif===967###eoif===1001###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");memcpy(extname, filename, length);

						if (AKA_mark("lis===558###sois===14149###eois===14185###lif===36###soif===1009###eoif===1045###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)") && ((AKA_mark("lis===558###sois===14149###eois===14155###lif===36###soif===1009###eoif===1015###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&length)	&&(AKA_mark("lis===558###sois===14159###eois===14185###lif===36###soif===1019###eoif===1045###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&extname[length - 1] != '/'))) {
				AKA_mark("lis===559###sois===14191###eois===14215###lif===37###soif===1051###eoif===1075###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");extname[length++] = '/';
			}
			else {AKA_mark("lis===-558-###sois===-14149-###eois===-1414936-###lif===-36-###soif===-###eoif===-1045-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");}

						while (AKA_mark("lis===560###sois===14226###eois===14255###lif===38###soif===1086###eoif===1115###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)") && ((AKA_mark("lis===560###sois===14226###eois===14233###lif===38###soif===1086###eoif===1093###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&rc == 0)	&&(AKA_mark("lis===560###sois===14237###eois===14255###lif===38###soif===1097###eoif===1115###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&indexes[i] != NULL))) {
								AKA_mark("lis===561###sois===14263###eois===14302###lif===39###soif===1123###eoif===1162###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");strcpy(extname + length, indexes[i++]);

								AKA_mark("lis===562###sois===14307###eois===14371###lif===40###soif===1167###eoif===1231###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");rc = afb_hreq_reply_locale_file_if_exist(hreq, search, extname);

			}

		}
		else {AKA_mark("lis===-552-###sois===-13902-###eois===-139027-###lif===-30-###soif===-###eoif===-769-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");}

				AKA_mark("lis===565###sois===14383###eois===14393###lif===43###soif===1243###eoif===1253###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");close(fd);

				AKA_mark("lis===566###sois===14396###eois===14406###lif===44###soif===1256###eoif===1266###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");return rc;

	}
	else {AKA_mark("lis===-550-###sois===-13815-###eois===-1381519-###lif===-28-###soif===-###eoif===-694-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");}


	/* Don't serve special files */
		if (AKA_mark("lis===570###sois===14449###eois===14469###lif===48###soif===1309###eoif===1329###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)") && (AKA_mark("lis===570###sois===14449###eois===14469###lif===48###soif===1309###eoif===1329###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&!S_ISREG(st.st_mode))) {
				AKA_mark("lis===571###sois===14475###eois===14485###lif===49###soif===1335###eoif===1345###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");close(fd);

				AKA_mark("lis===572###sois===14488###eois===14535###lif===50###soif===1348###eoif===1395###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");afb_hreq_reply_error(hreq, MHD_HTTP_FORBIDDEN);

				AKA_mark("lis===573###sois===14538###eois===14547###lif===51###soif===1398###eoif===1407###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");return 1;

	}
	else {AKA_mark("lis===-570-###sois===-14449-###eois===-1444920-###lif===-48-###soif===-###eoif===-1329-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");}


	/* Check the method */
		if (AKA_mark("lis===577###sois===14581###eois===14637###lif===55###soif===1441###eoif===1497###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)") && (AKA_mark("lis===577###sois===14581###eois===14637###lif===55###soif===1441###eoif===1497###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&(hreq->method & (afb_method_get | afb_method_head)) == 0)) {
				AKA_mark("lis===578###sois===14643###eois===14653###lif===56###soif===1503###eoif===1513###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");close(fd);

				AKA_mark("lis===579###sois===14656###eois===14712###lif===57###soif===1516###eoif===1572###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");afb_hreq_reply_error(hreq, MHD_HTTP_METHOD_NOT_ALLOWED);

				AKA_mark("lis===580###sois===14715###eois===14724###lif===58###soif===1575###eoif===1584###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");return 1;

	}
	else {AKA_mark("lis===-577-###sois===-14581-###eois===-1458156-###lif===-55-###soif===-###eoif===-1497-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");}


	/* computes the etag */
		AKA_mark("lis===584###sois===14755###eois===14856###lif===62###soif===1615###eoif===1716###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");sprintf(etag, "%08X%08X", ((int)(st.st_mtim.tv_sec) ^ (int)(st.st_mtim.tv_nsec)), (int)(st.st_size));


	/* checks the etag */
		AKA_mark("lis===587###sois===14882###eois===14982###lif===65###soif===1742###eoif===1842###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");inm = MHD_lookup_connection_value(hreq->connection, MHD_HEADER_KIND, MHD_HTTP_HEADER_IF_NONE_MATCH);

		if (AKA_mark("lis===588###sois===14988###eois===15017###lif===66###soif===1848###eoif===1877###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)") && ((AKA_mark("lis===588###sois===14988###eois===14991###lif===66###soif===1848###eoif===1851###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&inm)	&&(AKA_mark("lis===588###sois===14995###eois===15017###lif===66###soif===1855###eoif===1877###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&0 == strcmp(inm, etag)))) {
		/* etag ok, return NOT MODIFIED */
				AKA_mark("lis===590###sois===15060###eois===15070###lif===68###soif===1920###eoif===1930###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");close(fd);

				AKA_mark("lis===591###sois===15073###eois===15111###lif===69###soif===1933###eoif===1971###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");DEBUG("Not Modified: [%s]", filename);

				AKA_mark("lis===592###sois===15114###eois===15198###lif===70###soif===1974###eoif===2058###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");response = MHD_create_response_from_buffer(0, empty_string, MHD_RESPMEM_PERSISTENT);

				AKA_mark("lis===593###sois===15201###eois===15232###lif===71###soif===2061###eoif===2092###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");status = MHD_HTTP_NOT_MODIFIED;

	}
	else {
		/* check the size */
				if (AKA_mark("lis===596###sois===15272###eois===15313###lif===74###soif===2132###eoif===2173###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)") && (AKA_mark("lis===596###sois===15272###eois===15313###lif===74###soif===2132###eoif===2173###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&st.st_size != (off_t) (size_t) st.st_size)) {
						AKA_mark("lis===597###sois===15320###eois===15330###lif===75###soif===2180###eoif===2190###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");close(fd);

						AKA_mark("lis===598###sois===15334###eois===15393###lif===76###soif===2194###eoif===2253###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");afb_hreq_reply_error(hreq, MHD_HTTP_INTERNAL_SERVER_ERROR);

						AKA_mark("lis===599###sois===15397###eois===15406###lif===77###soif===2257###eoif===2266###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");return 1;

		}
		else {AKA_mark("lis===-596-###sois===-15272-###eois===-1527241-###lif===-74-###soif===-###eoif===-2173-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");}


		/* create the response */
				AKA_mark("lis===603###sois===15442###eois===15506###lif===81###soif===2302###eoif===2366###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");response = MHD_create_response_from_fd((size_t) st.st_size, fd);

				AKA_mark("lis===604###sois===15509###eois===15530###lif===82###soif===2369###eoif===2390###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");status = MHD_HTTP_OK;


		/* set the type */
				AKA_mark("lis===607###sois===15555###eois===15597###lif===85###soif===2415###eoif===2457###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");mimetype = mimetype_fd_name(fd, filename);

				if (AKA_mark("lis===608###sois===15604###eois===15620###lif===86###soif===2464###eoif===2480###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)") && (AKA_mark("lis===608###sois===15604###eois===15620###lif===86###soif===2464###eoif===2480###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)")&&mimetype != NULL)) {
			AKA_mark("lis===609###sois===15625###eois===15699###lif===87###soif===2485###eoif===2559###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");MHD_add_response_header(response, MHD_HTTP_HEADER_CONTENT_TYPE, mimetype);
		}
		else {AKA_mark("lis===-608-###sois===-15604-###eois===-1560416-###lif===-86-###soif===-###eoif===-2480-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");}

	}


	/* fills the value and send */
		AKA_mark("lis===613###sois===15737###eois===15870###lif===91###soif===2597###eoif===2730###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");afb_hreq_reply(hreq, status, response,
			MHD_HTTP_HEADER_CACHE_CONTROL, hreq->cacheTimeout,
			MHD_HTTP_HEADER_ETAG, etag,
			NULL);

		AKA_mark("lis===617###sois===15872###eois===15881###lif===95###soif===2732###eoif===2741###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file_if_exist(struct afb_hreq*,struct locale_search*,const char*)");return 1;

}

/** Instrumented function afb_hreq_reply_locale_file(struct afb_hreq*,struct locale_search*,const char*) */
int afb_hreq_reply_locale_file(struct afb_hreq *hreq, struct locale_search *search, const char *filename)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file(struct afb_hreq*,struct locale_search*,const char*)");AKA_fCall++;
		AKA_mark("lis===622###sois===15994###eois===16063###lif===2###soif===109###eoif===178###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file(struct afb_hreq*,struct locale_search*,const char*)");int rc = afb_hreq_reply_locale_file_if_exist(hreq, search, filename);

		if (AKA_mark("lis===623###sois===16069###eois===16076###lif===3###soif===184###eoif===191###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file(struct afb_hreq*,struct locale_search*,const char*)") && (AKA_mark("lis===623###sois===16069###eois===16076###lif===3###soif===184###eoif===191###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file(struct afb_hreq*,struct locale_search*,const char*)")&&rc == 0)) {
		AKA_mark("lis===624###sois===16080###eois===16127###lif===4###soif===195###eoif===242###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file(struct afb_hreq*,struct locale_search*,const char*)");afb_hreq_reply_error(hreq, MHD_HTTP_NOT_FOUND);
	}
	else {AKA_mark("lis===-623-###sois===-16069-###eois===-160697-###lif===-3-###soif===-###eoif===-191-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file(struct afb_hreq*,struct locale_search*,const char*)");}

		AKA_mark("lis===625###sois===16129###eois===16138###lif===5###soif===244###eoif===253###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_reply_locale_file(struct afb_hreq*,struct locale_search*,const char*)");return 1;

}

struct _mkq_ {
	int count;
	size_t length;
	size_t alloc;
	char *text;
};

/** Instrumented function _mkq_add_(struct _mkq_*,char) */
static void _mkq_add_(struct _mkq_ *mkq, char value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)");AKA_fCall++;
		AKA_mark("lis===637###sois===16273###eois===16296###lif===2###soif===56###eoif===79###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)");char *text = mkq->text;

		if (AKA_mark("lis===638###sois===16302###eois===16314###lif===3###soif===85###eoif===97###ifc===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)") && (AKA_mark("lis===638###sois===16302###eois===16314###lif===3###soif===85###eoif===97###isc===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)")&&text != NULL)) {
				if (AKA_mark("lis===639###sois===16324###eois===16349###lif===4###soif===107###eoif===132###ifc===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)") && (AKA_mark("lis===639###sois===16324###eois===16349###lif===4###soif===107###eoif===132###isc===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)")&&mkq->length == mkq->alloc)) {
						AKA_mark("lis===640###sois===16356###eois===16374###lif===5###soif===139###eoif===157###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)");mkq->alloc += 100;

						AKA_mark("lis===641###sois===16378###eois===16411###lif===6###soif===161###eoif===194###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)");text = realloc(text, mkq->alloc);

						if (AKA_mark("lis===642###sois===16419###eois===16431###lif===7###soif===202###eoif===214###ifc===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)") && (AKA_mark("lis===642###sois===16419###eois===16431###lif===7###soif===202###eoif===214###isc===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)")&&text == NULL)) {
								AKA_mark("lis===643###sois===16439###eois===16455###lif===8###soif===222###eoif===238###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)");free(mkq->text);

								AKA_mark("lis===644###sois===16460###eois===16477###lif===9###soif===243###eoif===260###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)");mkq->text = NULL;

								AKA_mark("lis===645###sois===16482###eois===16489###lif===10###soif===265###eoif===272###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)");return;

			}
			else {AKA_mark("lis===-642-###sois===-16419-###eois===-1641912-###lif===-7-###soif===-###eoif===-214-###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)");}

						AKA_mark("lis===647###sois===16498###eois===16515###lif===12###soif===281###eoif===298###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)");mkq->text = text;

		}
		else {AKA_mark("lis===-639-###sois===-16324-###eois===-1632425-###lif===-4-###soif===-###eoif===-132-###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)");}

				AKA_mark("lis===649###sois===16522###eois===16550###lif===14###soif===305###eoif===333###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)");text[mkq->length++] = value;

	}
	else {AKA_mark("lis===-638-###sois===-16302-###eois===-1630212-###lif===-3-###soif===-###eoif===-97-###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_(struct _mkq_*,char)");}

}

/** Instrumented function _mkq_add_hex_(struct _mkq_*,char) */
static void _mkq_add_hex_(struct _mkq_ *mkq, char value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/_mkq_add_hex_(struct _mkq_*,char)");AKA_fCall++;
		AKA_mark("lis===655###sois===16617###eois===16685###lif===2###soif===60###eoif===128###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_hex_(struct _mkq_*,char)");_mkq_add_(mkq, (char)(value < 10 ? value + '0' : value + 'A' - 10));

}

/** Instrumented function _mkq_add_esc_(struct _mkq_*,char) */
static void _mkq_add_esc_(struct _mkq_ *mkq, char value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/_mkq_add_esc_(struct _mkq_*,char)");AKA_fCall++;
		AKA_mark("lis===660###sois===16749###eois===16769###lif===2###soif===60###eoif===80###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_esc_(struct _mkq_*,char)");_mkq_add_(mkq, '%');

		AKA_mark("lis===661###sois===16771###eois===16817###lif===3###soif===82###eoif===128###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_esc_(struct _mkq_*,char)");_mkq_add_hex_(mkq, (char)((value >> 4) & 15));

		AKA_mark("lis===662###sois===16819###eois===16858###lif===4###soif===130###eoif===169###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_esc_(struct _mkq_*,char)");_mkq_add_hex_(mkq, (char)(value & 15));

}

/** Instrumented function _mkq_add_char_(struct _mkq_*,char) */
static void _mkq_add_char_(struct _mkq_ *mkq, char value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/_mkq_add_char_(struct _mkq_*,char)");AKA_fCall++;
		if (AKA_mark("lis===667###sois===16927###eois===16955###lif===2###soif===65###eoif===93###ifc===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_char_(struct _mkq_*,char)") && ((AKA_mark("lis===667###sois===16927###eois===16939###lif===2###soif===65###eoif===77###isc===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_char_(struct _mkq_*,char)")&&value <= ' ')	||(AKA_mark("lis===667###sois===16943###eois===16955###lif===2###soif===81###eoif===93###isc===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_char_(struct _mkq_*,char)")&&value >= 127))) {
		AKA_mark("lis===668###sois===16959###eois===16985###lif===3###soif===97###eoif===123###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_char_(struct _mkq_*,char)");_mkq_add_esc_(mkq, value);
	}
	else {
		AKA_mark("lis===670###sois===17001###eois===17006###lif===5###soif===139###eoif===144###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_char_(struct _mkq_*,char)");switch(value){
					case '=': if(value == '=')AKA_mark("lis===671###sois===17012###eois===17021###lif===6###soif===150###eoif===159###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_char_(struct _mkq_*,char)");

					case '&': if(value == '&')AKA_mark("lis===672###sois===17024###eois===17033###lif===7###soif===162###eoif===171###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_char_(struct _mkq_*,char)");

					case '%': if(value == '%')AKA_mark("lis===673###sois===17036###eois===17045###lif===8###soif===174###eoif===183###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_char_(struct _mkq_*,char)");

						AKA_mark("lis===674###sois===17049###eois===17075###lif===9###soif===187###eoif===213###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_char_(struct _mkq_*,char)");_mkq_add_esc_(mkq, value);

						AKA_mark("lis===675###sois===17079###eois===17085###lif===10###soif===217###eoif===223###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_char_(struct _mkq_*,char)");break;

					default: if(value != '=' && value != '&' && value != '%')AKA_mark("lis===676###sois===17088###eois===17096###lif===11###soif===226###eoif===234###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_char_(struct _mkq_*,char)");

						AKA_mark("lis===677###sois===17100###eois===17122###lif===12###soif===238###eoif===260###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_add_char_(struct _mkq_*,char)");_mkq_add_(mkq, value);

		}
	}

}

/** Instrumented function _mkq_append_(struct _mkq_*,const char*) */
static void _mkq_append_(struct _mkq_ *mkq, const char *value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/_mkq_append_(struct _mkq_*,const char*)");AKA_fCall++;
		while (AKA_mark("lis===683###sois===17202###eois===17208###lif===2###soif===72###eoif===78###ifc===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_append_(struct _mkq_*,const char*)") && (AKA_mark("lis===683###sois===17202###eois===17208###lif===2###soif===72###eoif===78###isc===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_append_(struct _mkq_*,const char*)")&&*value)) {
		AKA_mark("lis===684###sois===17212###eois===17242###lif===3###soif===82###eoif===112###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkq_append_(struct _mkq_*,const char*)");_mkq_add_char_(mkq, *value++);
	}

}

/** Instrumented function _mkquery_(struct _mkq_*,enum MHD_ValueKind,const char*,const char*) */
static int _mkquery_(struct _mkq_ *mkq, enum MHD_ValueKind kind, const char *key, const char *value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/_mkquery_(struct _mkq_*,enum MHD_ValueKind,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===689###sois===17350###eois===17391###lif===2###soif===104###eoif===145###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkquery_(struct _mkq_*,enum MHD_ValueKind,const char*,const char*)");_mkq_add_(mkq, mkq->count++ ? '&' : '?');

		AKA_mark("lis===690###sois===17393###eois===17416###lif===3###soif===147###eoif===170###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkquery_(struct _mkq_*,enum MHD_ValueKind,const char*,const char*)");_mkq_append_(mkq, key);

		if (AKA_mark("lis===691###sois===17422###eois===17435###lif===4###soif===176###eoif===189###ifc===true###function===./app-framework-binder/src/afb-hreq.c/_mkquery_(struct _mkq_*,enum MHD_ValueKind,const char*,const char*)") && (AKA_mark("lis===691###sois===17422###eois===17435###lif===4###soif===176###eoif===189###isc===true###function===./app-framework-binder/src/afb-hreq.c/_mkquery_(struct _mkq_*,enum MHD_ValueKind,const char*,const char*)")&&value != NULL)) {
				AKA_mark("lis===692###sois===17441###eois===17461###lif===5###soif===195###eoif===215###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkquery_(struct _mkq_*,enum MHD_ValueKind,const char*,const char*)");_mkq_add_(mkq, '=');

				AKA_mark("lis===693###sois===17464###eois===17489###lif===6###soif===218###eoif===243###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkquery_(struct _mkq_*,enum MHD_ValueKind,const char*,const char*)");_mkq_append_(mkq, value);

	}
	else {AKA_mark("lis===-691-###sois===-17422-###eois===-1742213-###lif===-4-###soif===-###eoif===-189-###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkquery_(struct _mkq_*,enum MHD_ValueKind,const char*,const char*)");}

		AKA_mark("lis===695###sois===17494###eois===17503###lif===8###soif===248###eoif===257###ins===true###function===./app-framework-binder/src/afb-hreq.c/_mkquery_(struct _mkq_*,enum MHD_ValueKind,const char*,const char*)");return 1;

}

/** Instrumented function url_with_query(struct afb_hreq*,const char*) */
static char *url_with_query(struct afb_hreq *hreq, const char *url)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/url_with_query(struct afb_hreq*,const char*)");AKA_fCall++;
		AKA_mark("lis===700###sois===17578###eois===17595###lif===2###soif===71###eoif===88###ins===true###function===./app-framework-binder/src/afb-hreq.c/url_with_query(struct afb_hreq*,const char*)");struct _mkq_ mkq;


		AKA_mark("lis===702###sois===17598###eois===17612###lif===4###soif===91###eoif===105###ins===true###function===./app-framework-binder/src/afb-hreq.c/url_with_query(struct afb_hreq*,const char*)");mkq.count = 0;

		AKA_mark("lis===703###sois===17614###eois===17639###lif===5###soif===107###eoif===132###ins===true###function===./app-framework-binder/src/afb-hreq.c/url_with_query(struct afb_hreq*,const char*)");mkq.length = strlen(url);

		AKA_mark("lis===704###sois===17641###eois===17671###lif===6###soif===134###eoif===164###ins===true###function===./app-framework-binder/src/afb-hreq.c/url_with_query(struct afb_hreq*,const char*)");mkq.alloc = mkq.length + 1000;

		AKA_mark("lis===705###sois===17673###eois===17702###lif===7###soif===166###eoif===195###ins===true###function===./app-framework-binder/src/afb-hreq.c/url_with_query(struct afb_hreq*,const char*)");mkq.text = malloc(mkq.alloc);

		if (AKA_mark("lis===706###sois===17708###eois===17724###lif===8###soif===201###eoif===217###ifc===true###function===./app-framework-binder/src/afb-hreq.c/url_with_query(struct afb_hreq*,const char*)") && (AKA_mark("lis===706###sois===17708###eois===17724###lif===8###soif===201###eoif===217###isc===true###function===./app-framework-binder/src/afb-hreq.c/url_with_query(struct afb_hreq*,const char*)")&&mkq.text != NULL)) {
				AKA_mark("lis===707###sois===17730###eois===17752###lif===9###soif===223###eoif===245###ins===true###function===./app-framework-binder/src/afb-hreq.c/url_with_query(struct afb_hreq*,const char*)");strcpy(mkq.text, url);

				AKA_mark("lis===708###sois===17755###eois===17846###lif===10###soif===248###eoif===339###ins===true###function===./app-framework-binder/src/afb-hreq.c/url_with_query(struct afb_hreq*,const char*)");MHD_get_connection_values(hreq->connection, MHD_GET_ARGUMENT_KIND, (void*)_mkquery_, &mkq);

				AKA_mark("lis===709###sois===17849###eois===17868###lif===11###soif===342###eoif===361###ins===true###function===./app-framework-binder/src/afb-hreq.c/url_with_query(struct afb_hreq*,const char*)");_mkq_add_(&mkq, 0);

	}
	else {AKA_mark("lis===-706-###sois===-17708-###eois===-1770816-###lif===-8-###soif===-###eoif===-217-###ins===true###function===./app-framework-binder/src/afb-hreq.c/url_with_query(struct afb_hreq*,const char*)");}

		AKA_mark("lis===711###sois===17873###eois===17889###lif===13###soif===366###eoif===382###ins===true###function===./app-framework-binder/src/afb-hreq.c/url_with_query(struct afb_hreq*,const char*)");return mkq.text;

}

/** Instrumented function afb_hreq_redirect_to(struct afb_hreq*,const char*,int) */
void afb_hreq_redirect_to(struct afb_hreq *hreq, const char *url, int add_query_part)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to(struct afb_hreq*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===716###sois===17982###eois===17997###lif===2###soif===89###eoif===104###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to(struct afb_hreq*,const char*,int)");const char *to;

		AKA_mark("lis===717###sois===17999###eois===18009###lif===3###soif===106###eoif===116###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to(struct afb_hreq*,const char*,int)");char *wqp;


		AKA_mark("lis===719###sois===18012###eois===18068###lif===5###soif===119###eoif===175###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to(struct afb_hreq*,const char*,int)");wqp = add_query_part ? url_with_query(hreq, url) : NULL;

		AKA_mark("lis===720###sois===18070###eois===18087###lif===6###soif===177###eoif===194###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to(struct afb_hreq*,const char*,int)");to = wqp ? : url;

		AKA_mark("lis===721###sois===18089###eois===18193###lif===7###soif===196###eoif===300###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to(struct afb_hreq*,const char*,int)");afb_hreq_reply_static(hreq, MHD_HTTP_MOVED_PERMANENTLY, 0, NULL,
			MHD_HTTP_HEADER_LOCATION, to, NULL);

		AKA_mark("lis===723###sois===18195###eois===18247###lif===9###soif===302###eoif===354###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to(struct afb_hreq*,const char*,int)");DEBUG("redirect from [%s] to [%s]", hreq->url, url);

		AKA_mark("lis===724###sois===18249###eois===18259###lif===10###soif===356###eoif===366###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_redirect_to(struct afb_hreq*,const char*,int)");free(wqp);

}

/** Instrumented function afb_hreq_get_cookie(struct afb_hreq*,const char*) */
const char *afb_hreq_get_cookie(struct afb_hreq *hreq, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_get_cookie(struct afb_hreq*,const char*)");AKA_fCall++;
		AKA_mark("lis===729###sois===18339###eois===18415###lif===2###soif===76###eoif===152###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_cookie(struct afb_hreq*,const char*)");return MHD_lookup_connection_value(hreq->connection, MHD_COOKIE_KIND, name);

}

/** Instrumented function afb_hreq_get_argument(struct afb_hreq*,const char*) */
const char *afb_hreq_get_argument(struct afb_hreq *hreq, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_get_argument(struct afb_hreq*,const char*)");AKA_fCall++;
		AKA_mark("lis===734###sois===18497###eois===18546###lif===2###soif===78###eoif===127###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_argument(struct afb_hreq*,const char*)");struct hreq_data *data = get_data(hreq, name, 0);

		AKA_mark("lis===735###sois===18548###eois===18651###lif===3###soif===129###eoif===232###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_argument(struct afb_hreq*,const char*)");return data ? data->value : MHD_lookup_connection_value(hreq->connection, MHD_GET_ARGUMENT_KIND, name);

}

/** Instrumented function afb_hreq_get_header(struct afb_hreq*,const char*) */
const char *afb_hreq_get_header(struct afb_hreq *hreq, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_get_header(struct afb_hreq*,const char*)");AKA_fCall++;
		AKA_mark("lis===740###sois===18731###eois===18807###lif===2###soif===76###eoif===152###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_header(struct afb_hreq*,const char*)");return MHD_lookup_connection_value(hreq->connection, MHD_HEADER_KIND, name);

}

/** Instrumented function afb_hreq_get_authorization_bearer(struct afb_hreq*) */
const char *afb_hreq_get_authorization_bearer(struct afb_hreq *hreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)");AKA_fCall++;
		AKA_mark("lis===745###sois===18883###eois===18960###lif===2###soif===72###eoif===149###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)");const char *value = afb_hreq_get_header(hreq, MHD_HTTP_HEADER_AUTHORIZATION);

		if (AKA_mark("lis===746###sois===18966###eois===18971###lif===3###soif===155###eoif===160###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)") && (AKA_mark("lis===746###sois===18966###eois===18971###lif===3###soif===155###eoif===160###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)")&&value)) {
				if (AKA_mark("lis===747###sois===18981###eois===19047###lif===4###soif===170###eoif===236###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)") && (AKA_mark("lis===747###sois===18981###eois===19047###lif===4###soif===170###eoif===236###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)")&&strncasecmp(value, key_for_bearer, sizeof key_for_bearer - 1) == 0)) {
						AKA_mark("lis===748###sois===19054###eois===19089###lif===5###soif===243###eoif===278###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)");value += sizeof key_for_bearer - 1;

						if (AKA_mark("lis===749###sois===19097###eois===19114###lif===6###soif===286###eoif===303###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)") && (AKA_mark("lis===749###sois===19097###eois===19114###lif===6###soif===286###eoif===303###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)")&&isblank(*value++))) {
								while (AKA_mark("lis===750###sois===19129###eois===19144###lif===7###soif===318###eoif===333###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)") && (AKA_mark("lis===750###sois===19129###eois===19144###lif===7###soif===318###eoif===333###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)")&&isblank(*value))) {
					AKA_mark("lis===751###sois===19151###eois===19159###lif===8###soif===340###eoif===348###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)");value++;
				}

								if (AKA_mark("lis===752###sois===19168###eois===19174###lif===9###soif===357###eoif===363###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)") && (AKA_mark("lis===752###sois===19168###eois===19174###lif===9###soif===357###eoif===363###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)")&&*value)) {
					AKA_mark("lis===753###sois===19181###eois===19194###lif===10###soif===370###eoif===383###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)");return value;
				}
				else {AKA_mark("lis===-752-###sois===-19168-###eois===-191686-###lif===-9-###soif===-###eoif===-363-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)");}

			}
			else {AKA_mark("lis===-749-###sois===-19097-###eois===-1909717-###lif===-6-###soif===-###eoif===-303-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)");}

		}
		else {AKA_mark("lis===-747-###sois===-18981-###eois===-1898166-###lif===-4-###soif===-###eoif===-236-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)");}

	}
	else {AKA_mark("lis===-746-###sois===-18966-###eois===-189665-###lif===-3-###soif===-###eoif===-160-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)");}

		AKA_mark("lis===757###sois===19208###eois===19220###lif===14###soif===397###eoif===409###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_get_authorization_bearer(struct afb_hreq*)");return NULL;

}

/** Instrumented function afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t) */
int afb_hreq_post_add(struct afb_hreq *hreq, const char *key, const char *data, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)");AKA_fCall++;
		AKA_mark("lis===762###sois===19320###eois===19328###lif===2###soif===96###eoif===104###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)");void *p;

		AKA_mark("lis===763###sois===19330###eois===19378###lif===3###soif===106###eoif===154###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)");struct hreq_data *hdat = get_data(hreq, key, 1);

		if (AKA_mark("lis===764###sois===19384###eois===19402###lif===4###soif===160###eoif===178###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)") && (AKA_mark("lis===764###sois===19384###eois===19402###lif===4###soif===160###eoif===178###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)")&&hdat->path != NULL)) {
				AKA_mark("lis===765###sois===19408###eois===19417###lif===5###soif===184###eoif===193###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)");return 0;

	}
	else {AKA_mark("lis===-764-###sois===-19384-###eois===-1938418-###lif===-4-###soif===-###eoif===-178-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)");}

		AKA_mark("lis===767###sois===19422###eois===19472###lif===7###soif===198###eoif===248###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)");p = realloc(hdat->value, hdat->length + size + 1);

		if (AKA_mark("lis===768###sois===19478###eois===19487###lif===8###soif===254###eoif===263###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)") && (AKA_mark("lis===768###sois===19478###eois===19487###lif===8###soif===254###eoif===263###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)")&&p == NULL)) {
				AKA_mark("lis===769###sois===19493###eois===19502###lif===9###soif===269###eoif===278###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)");return 0;

	}
	else {AKA_mark("lis===-768-###sois===-19478-###eois===-194789-###lif===-8-###soif===-###eoif===-263-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)");}

		AKA_mark("lis===771###sois===19507###eois===19523###lif===11###soif===283###eoif===299###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)");hdat->value = p;

		AKA_mark("lis===772###sois===19525###eois===19572###lif===12###soif===301###eoif===348###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)");memcpy(&hdat->value[hdat->length], data, size);

		AKA_mark("lis===773###sois===19574###eois===19595###lif===13###soif===350###eoif===371###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)");hdat->length += size;

		AKA_mark("lis===774###sois===19597###eois===19627###lif===14###soif===373###eoif===403###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)");hdat->value[hdat->length] = 0;

		AKA_mark("lis===775###sois===19629###eois===19638###lif===15###soif===405###eoif===414###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add(struct afb_hreq*,const char*,const char*,size_t)");return 1;

}

/** Instrumented function afb_hreq_init_download_path(const char*) */
int afb_hreq_init_download_path(const char *directory)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");AKA_fCall++;
		AKA_mark("lis===780###sois===19700###eois===19715###lif===2###soif===58###eoif===73###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");struct stat st;

		AKA_mark("lis===781###sois===19717###eois===19726###lif===3###soif===75###eoif===84###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");size_t n;

		AKA_mark("lis===782###sois===19728###eois===19736###lif===4###soif===86###eoif===94###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");char *p;


		if (AKA_mark("lis===784###sois===19743###eois===19771###lif===6###soif===101###eoif===129###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)") && (AKA_mark("lis===784###sois===19743###eois===19771###lif===6###soif===101###eoif===129###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)")&&access(directory, R_OK|W_OK))) {
		/* no read/write access */
				AKA_mark("lis===786###sois===19806###eois===19816###lif===8###soif===164###eoif===174###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");return -1;

	}
	else {AKA_mark("lis===-784-###sois===-19743-###eois===-1974328-###lif===-6-###soif===-###eoif===-129-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");}

		if (AKA_mark("lis===788###sois===19825###eois===19845###lif===10###soif===183###eoif===203###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)") && (AKA_mark("lis===788###sois===19825###eois===19845###lif===10###soif===183###eoif===203###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)")&&stat(directory, &st))) {
		/* can't get info */
				AKA_mark("lis===790###sois===19874###eois===19884###lif===12###soif===232###eoif===242###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");return -1;

	}
	else {AKA_mark("lis===-788-###sois===-19825-###eois===-1982520-###lif===-10-###soif===-###eoif===-203-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");}

		if (AKA_mark("lis===792###sois===19893###eois===19913###lif===14###soif===251###eoif===271###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)") && (AKA_mark("lis===792###sois===19893###eois===19913###lif===14###soif===251###eoif===271###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)")&&!S_ISDIR(st.st_mode))) {
		/* not a directory */
				AKA_mark("lis===794###sois===19943###eois===19959###lif===16###soif===301###eoif===317###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");errno = ENOTDIR;

				AKA_mark("lis===795###sois===19962###eois===19972###lif===17###soif===320###eoif===330###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");return -1;

	}
	else {AKA_mark("lis===-792-###sois===-19893-###eois===-1989320-###lif===-14-###soif===-###eoif===-271-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");}

		AKA_mark("lis===797###sois===19977###eois===19999###lif===19###soif===335###eoif===357###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");n = strlen(directory);

		while (AKA_mark("lis===798###sois===20007###eois===20037###lif===20###soif===365###eoif===395###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)") && ((AKA_mark("lis===798###sois===20007###eois===20012###lif===20###soif===365###eoif===370###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)")&&n > 1)	&&(AKA_mark("lis===798###sois===20016###eois===20037###lif===20###soif===374###eoif===395###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)")&&directory[n-1] == '/'))) {
		AKA_mark("lis===798###sois===20039###eois===20043###lif===20###soif===397###eoif===401###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");n--;
	}

		AKA_mark("lis===799###sois===20045###eois===20063###lif===21###soif===403###eoif===421###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");p = malloc(n + 8);

		if (AKA_mark("lis===800###sois===20069###eois===20078###lif===22###soif===427###eoif===436###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)") && (AKA_mark("lis===800###sois===20069###eois===20078###lif===22###soif===427###eoif===436###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)")&&p == NULL)) {
		/* can't allocate memory */
				AKA_mark("lis===802###sois===20114###eois===20129###lif===24###soif===472###eoif===487###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");errno = ENOMEM;

				AKA_mark("lis===803###sois===20132###eois===20142###lif===25###soif===490###eoif===500###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");return -1;

	}
	else {AKA_mark("lis===-800-###sois===-20069-###eois===-200699-###lif===-22-###soif===-###eoif===-436-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");}

		AKA_mark("lis===805###sois===20147###eois===20171###lif===27###soif===505###eoif===529###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");memcpy(p, directory, n);

		AKA_mark("lis===806###sois===20173###eois===20186###lif===28###soif===531###eoif===544###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");p[n++] = '/';

		AKA_mark("lis===807###sois===20188###eois===20201###lif===29###soif===546###eoif===559###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");	AKA_mark("lis===808###sois===20203###eois===20216###lif===30###soif===561###eoif===574###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");	AKA_mark("lis===809###sois===20218###eois===20231###lif===31###soif===576###eoif===589###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");	AKA_mark("lis===810###sois===20233###eois===20246###lif===32###soif===591###eoif===604###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");	AKA_mark("lis===811###sois===20248###eois===20261###lif===33###soif===606###eoif===619###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");	AKA_mark("lis===812###sois===20263###eois===20276###lif===34###soif===621###eoif===634###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");p[n++] = 'X';






	p[n++] = 'X';
	p[n++] = 'X';
	p[n++] = 'X';
	p[n++] = 'X';
	p[n++] = 'X';
		AKA_mark("lis===813###sois===20278###eois===20287###lif===35###soif===636###eoif===645###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");p[n] = 0;

		AKA_mark("lis===814###sois===20289###eois===20307###lif===36###soif===647###eoif===665###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");free(tmp_pattern);

		AKA_mark("lis===815###sois===20309###eois===20325###lif===37###soif===667###eoif===683###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");tmp_pattern = p;

		AKA_mark("lis===816###sois===20327###eois===20336###lif===38###soif===685###eoif===694###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_download_path(const char*)");return 0;

}

/** Instrumented function opentempfile(char**) */
static int opentempfile(char **path)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/opentempfile(char**)");AKA_fCall++;
		AKA_mark("lis===821###sois===20380###eois===20387###lif===2###soif===40###eoif===47###ins===true###function===./app-framework-binder/src/afb-hreq.c/opentempfile(char**)");int fd;

		AKA_mark("lis===822###sois===20389###eois===20401###lif===3###soif===49###eoif===61###ins===true###function===./app-framework-binder/src/afb-hreq.c/opentempfile(char**)");char *fname;


		AKA_mark("lis===824###sois===20404###eois===20445###lif===5###soif===64###eoif===105###ins===true###function===./app-framework-binder/src/afb-hreq.c/opentempfile(char**)");fname = strdup(tmp_pattern ? : "XXXXXX");
 /* TODO improve the path */
		if (AKA_mark("lis===825###sois===20479###eois===20492###lif===6###soif===139###eoif===152###ifc===true###function===./app-framework-binder/src/afb-hreq.c/opentempfile(char**)") && (AKA_mark("lis===825###sois===20479###eois===20492###lif===6###soif===139###eoif===152###isc===true###function===./app-framework-binder/src/afb-hreq.c/opentempfile(char**)")&&fname == NULL)) {
		AKA_mark("lis===826###sois===20496###eois===20506###lif===7###soif===156###eoif===166###ins===true###function===./app-framework-binder/src/afb-hreq.c/opentempfile(char**)");return -1;
	}
	else {AKA_mark("lis===-825-###sois===-20479-###eois===-2047913-###lif===-6-###soif===-###eoif===-152-###ins===true###function===./app-framework-binder/src/afb-hreq.c/opentempfile(char**)");}


		AKA_mark("lis===828###sois===20509###eois===20550###lif===9###soif===169###eoif===210###ins===true###function===./app-framework-binder/src/afb-hreq.c/opentempfile(char**)");fd = mkostemp(fname, O_CLOEXEC|O_WRONLY);

		if (AKA_mark("lis===829###sois===20556###eois===20562###lif===10###soif===216###eoif===222###ifc===true###function===./app-framework-binder/src/afb-hreq.c/opentempfile(char**)") && (AKA_mark("lis===829###sois===20556###eois===20562###lif===10###soif===216###eoif===222###isc===true###function===./app-framework-binder/src/afb-hreq.c/opentempfile(char**)")&&fd < 0)) {
		AKA_mark("lis===830###sois===20566###eois===20578###lif===11###soif===226###eoif===238###ins===true###function===./app-framework-binder/src/afb-hreq.c/opentempfile(char**)");free(fname);
	}
	else {
		AKA_mark("lis===832###sois===20587###eois===20601###lif===13###soif===247###eoif===261###ins===true###function===./app-framework-binder/src/afb-hreq.c/opentempfile(char**)");*path = fname;
	}

		AKA_mark("lis===833###sois===20603###eois===20613###lif===14###soif===263###eoif===273###ins===true###function===./app-framework-binder/src/afb-hreq.c/opentempfile(char**)");return fd;

}

/** Instrumented function afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t) */
int afb_hreq_post_add_file(struct afb_hreq *hreq, const char *key, const char *file, const char *data, size_t size)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");AKA_fCall++;
		AKA_mark("lis===838###sois===20736###eois===20743###lif===2###soif===119###eoif===126###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");int fd;

		AKA_mark("lis===839###sois===20745###eois===20756###lif===3###soif===128###eoif===139###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");ssize_t sz;

		AKA_mark("lis===840###sois===20758###eois===20806###lif===4###soif===141###eoif===189###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");struct hreq_data *hdat = get_data(hreq, key, 1);


		if (AKA_mark("lis===842###sois===20813###eois===20832###lif===6###soif===196###eoif===215###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)") && (AKA_mark("lis===842###sois===20813###eois===20832###lif===6###soif===196###eoif===215###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)")&&hdat->value == NULL)) {
				AKA_mark("lis===843###sois===20838###eois===20865###lif===7###soif===221###eoif===248###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");hdat->value = strdup(file);

				if (AKA_mark("lis===844###sois===20872###eois===20891###lif===8###soif===255###eoif===274###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)") && (AKA_mark("lis===844###sois===20872###eois===20891###lif===8###soif===255###eoif===274###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)")&&hdat->value == NULL)) {
			AKA_mark("lis===845###sois===20896###eois===20905###lif===9###soif===279###eoif===288###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");return 0;
		}
		else {AKA_mark("lis===-844-###sois===-20872-###eois===-2087219-###lif===-8-###soif===-###eoif===-274-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");}

				AKA_mark("lis===846###sois===20908###eois===20939###lif===10###soif===291###eoif===322###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");fd = opentempfile(&hdat->path);

	}
	else {
		if (AKA_mark("lis===847###sois===20952###eois===20999###lif===11###soif===335###eoif===382###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)") && ((AKA_mark("lis===847###sois===20952###eois===20977###lif===11###soif===335###eoif===360###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)")&&strcmp(hdat->value, file))	||(AKA_mark("lis===847###sois===20981###eois===20999###lif===11###soif===364###eoif===382###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)")&&hdat->path == NULL))) {
					AKA_mark("lis===848###sois===21005###eois===21014###lif===12###soif===388###eoif===397###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");return 0;

	}
		else {
					AKA_mark("lis===850###sois===21027###eois===21068###lif===14###soif===410###eoif===451###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");fd = open(hdat->path, O_WRONLY|O_APPEND);

	}
	}

		if (AKA_mark("lis===852###sois===21077###eois===21083###lif===16###soif===460###eoif===466###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)") && (AKA_mark("lis===852###sois===21077###eois===21083###lif===16###soif===460###eoif===466###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)")&&fd < 0)) {
		AKA_mark("lis===853###sois===21087###eois===21096###lif===17###soif===470###eoif===479###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");return 0;
	}
	else {AKA_mark("lis===-852-###sois===-21077-###eois===-210776-###lif===-16-###soif===-###eoif===-466-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");}

		while (AKA_mark("lis===854###sois===21105###eois===21109###lif===18###soif===488###eoif===492###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)") && (AKA_mark("lis===854###sois===21105###eois===21109###lif===18###soif===488###eoif===492###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)")&&size)) {
				AKA_mark("lis===855###sois===21115###eois===21142###lif===19###soif===498###eoif===525###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");sz = write(fd, data, size);

				if (AKA_mark("lis===856###sois===21149###eois===21156###lif===20###soif===532###eoif===539###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)") && (AKA_mark("lis===856###sois===21149###eois===21156###lif===20###soif===532###eoif===539###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)")&&sz >= 0)) {
						AKA_mark("lis===857###sois===21163###eois===21190###lif===21###soif===546###eoif===573###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");hdat->length += (size_t)sz;

						AKA_mark("lis===858###sois===21194###eois===21213###lif===22###soif===577###eoif===596###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");size -= (size_t)sz;

						AKA_mark("lis===859###sois===21217###eois===21228###lif===23###soif===600###eoif===611###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");data += sz;

		}
		else {
			if (AKA_mark("lis===860###sois===21242###eois===21256###lif===24###soif===625###eoif===639###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)") && (AKA_mark("lis===860###sois===21242###eois===21256###lif===24###soif===625###eoif===639###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)")&&errno != EINTR)) {
				AKA_mark("lis===861###sois===21261###eois===21267###lif===25###soif===644###eoif===650###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");break;
			}
			else {AKA_mark("lis===-860-###sois===-21242-###eois===-2124214-###lif===-24-###soif===-###eoif===-639-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");}
		}

	}

		AKA_mark("lis===863###sois===21272###eois===21282###lif===27###soif===655###eoif===665###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");close(fd);

		AKA_mark("lis===864###sois===21284###eois===21297###lif===28###soif===667###eoif===680###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_post_add_file(struct afb_hreq*,const char*,const char*,const char*,size_t)");return !size;

}

/** Instrumented function req_get(struct afb_xreq*,const char*) */
static struct afb_arg req_get(struct afb_xreq *xreq, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/req_get(struct afb_xreq*,const char*)");AKA_fCall++;
		AKA_mark("lis===869###sois===21375###eois===21393###lif===2###soif===74###eoif===92###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_get(struct afb_xreq*,const char*)");const char *value;

		AKA_mark("lis===870###sois===21395###eois===21410###lif===3###soif===94###eoif===109###ins===true###function===./app-framework-binder/src/afb-hre/* Cant instrument this following code */
q.c/req_get(struct afb_xreq*,const char*)");struct afb_hreq
 /* Cant instrument this following code */
*hreq = CONTAINER_OF_XREQ(struct afb_hreq, xreq);
		AKA_mark("lis===871###sois===21462###eois===21511###lif===4###soif===161###eoif===210###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_get(struct afb_xreq*,const char*)");struct hreq_data *hdat = get_data(hreq, name, 0);

	/* Cant instrument this following code */
if (hdat)
		return (struct afb_arg){
			.name = hdat->key,
			.value = hdat->value,
			.path = hdat->path
		};

		AKA_mark("lis===879###sois===21626###eois===21709###lif===12###soif===325###eoif===408###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_get(struct afb_xreq*,const char*)");value = MHD_lookup_connection_value(hreq->connection, MHD_GET_ARGUMENT_KIND, name);

	/* Cant instrument this following code */
return (struct afb_arg){
		.name = value == NULL ? NULL : name,
		.value = value,
		.path = NULL
	};
}

/** Instrumented function _iterargs_(struct json_object*,enum MHD_ValueKind,const char*,const char*) */
static int _iterargs_(struct json_object *obj, enum MHD_ValueKind kind, const char *key, const char *value)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/_iterargs_(struct json_object*,enum MHD_ValueKind,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===889###sois===21926###eois===22005###lif===2###soif===111###eoif===190###ins===true###function===./app-framework-binder/src/afb-hreq.c/_iterargs_(struct json_object*,enum MHD_ValueKind,const char*,const char*)");json_object_object_add(obj, key, value ? json_object_new_string(value) : NULL);

		AKA_mark("lis===890###sois===22007###eois===22016###lif===3###soif===192###eoif===201###ins===true###function===./app-framework-binder/src/afb-hreq.c/_iterargs_(struct json_object*,enum MHD_ValueKind,const char*,const char*)");return 1;

}

/** Instrumented function req_json(struct afb_xreq*) */
static struct json_object *req_json(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===895###sois===22082###eois===22105###lif===2###soif===62###eoif===85###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");struct hreq_data *hdat;

		AKA_mark("lis===896###sois===22107###eois===22137###lif===3###soif===87###eoif===117###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");struct json_object *obj, *val;

		AKA_mark("lis===897###sois===22139###eois===22154###lif===4###soif===119###eoif===134###ins===true###function===./app-framework-binder/src/afb-hre/* Cant instrument this following code */
q.c/req_json(struct afb_xreq*)");struct afb_hreq
 /* Cant instrument this following code */
*hreq = CONTAINER_OF_XREQ(struct afb_hreq, xreq);

		AKA_mark("lis===899###sois===22207###eois===22224###lif===6###soif===187###eoif===204###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");obj = hreq->json;

		if (AKA_mark("lis===900###sois===22230###eois===22241###lif===7###soif===210###eoif===221###ifc===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)") && (AKA_mark("lis===900###sois===22230###eois===22241###lif===7###soif===210###eoif===221###isc===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)")&&obj == NULL)) {
				AKA_mark("lis===901###sois===22247###eois===22291###lif===8###soif===227###eoif===271###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");hreq->json = obj = json_object_new_object();

				if (AKA_mark("lis===902###sois===22298###eois===22309###lif===9###soif===278###eoif===289###ifc===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)") && (AKA_mark("lis===902###sois===22298###eois===22309###lif===9###soif===278###eoif===289###isc===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)")&&obj == NULL)) {AKA_mark("lis===+902+###sois===+22298+###eois===+2229811+###lif===+9+###soif===+###eoif===+289+###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");}
		else {
						AKA_mark("lis===904###sois===22327###eois===22419###lif===11###soif===307###eoif===399###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");MHD_get_connection_values (hreq->connection, MHD_GET_ARGUMENT_KIND, (void*)_iterargs_, obj);

						AKA_mark("lis===905###sois===22428###eois===22447###lif===12###soif===408###eoif===427###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");for (hdat = hreq->data ;AKA_mark("lis===905###sois===22448###eois===22452###lif===12###soif===428###eoif===432###ifc===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)") && AKA_mark("lis===905###sois===22448###eois===22452###lif===12###soif===428###eoif===432###isc===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)")&&hdat;({AKA_mark("lis===905###sois===22455###eois===22472###lif===12###soif===435###eoif===452###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");hdat = hdat->next;})) {
								if (AKA_mark("lis===906###sois===22484###eois===22502###lif===13###soif===464###eoif===482###ifc===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)") && (AKA_mark("lis===906###sois===22484###eois===22502###lif===13###soif===464###eoif===482###isc===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)")&&hdat->path == NULL)) {
					AKA_mark("lis===907###sois===22509###eois===22572###lif===14###soif===489###eoif===552###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");val = hdat->value ? json_object_new_string(hdat->value) : NULL;
				}
				else {
										AKA_mark("lis===909###sois===22589###eois===22620###lif===16###soif===569###eoif===600###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");val = json_object_new_object();

										if (AKA_mark("lis===910###sois===22630###eois===22641###lif===17###soif===610###eoif===621###ifc===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)") && (AKA_mark("lis===910###sois===22630###eois===22641###lif===17###soif===610###eoif===621###isc===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)")&&val == NULL)) {AKA_mark("lis===+910+###sois===+22630+###eois===+2263011+###lif===+17+###soif===+###eoif===+621+###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");}
					else {
												AKA_mark("lis===912###sois===22665###eois===22738###lif===19###soif===645###eoif===718###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");json_object_object_add(val, "file", json_object_new_string(hdat->value));

												AKA_mark("lis===913###sois===22745###eois===22817###lif===20###soif===725###eoif===797###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");json_object_object_add(val, "path", json_object_new_string(hdat->path));

					}

				}

								AKA_mark("lis===916###sois===22835###eois===22879###lif===23###soif===815###eoif===859###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");json_object_object_add(obj, hdat->key, val);

			}

		}

	}
	else {AKA_mark("lis===-900-###sois===-22230-###eois===-2223011-###lif===-7-###soif===-###eoif===-221-###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");}

		AKA_mark("lis===920###sois===22893###eois===22904###lif===27###soif===873###eoif===884###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_json(struct afb_xreq*)");return obj;

}

/** Instrumented function get_json_string(json_object*) */
static inline const char *get_json_string(json_object *obj)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/get_json_string(json_object*)");AKA_fCall++;
		AKA_mark("lis===925###sois===22971###eois===23069###lif===2###soif===63###eoif===161###ins===true###function===./app-framework-binder/src/afb-hreq.c/get_json_string(json_object*)");return json_object_to_json_string_ext(obj, JSON_C_TO_STRING_PLAIN|JSON_C_TO_STRING_NOSLASHESCAPE);

}
/** Instrumented function send_json_cb(json_object*,uint64_t,char*,size_t) */
static ssize_t send_json_cb(json_object *obj, uint64_t pos, char *buf, size_t max)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/send_json_cb(json_object*,uint64_t,char*,size_t)");AKA_fCall++;
		AKA_mark("lis===929###sois===23158###eois===23222###lif===2###soif===86###eoif===150###ins===true###function===./app-framework-binder/src/afb-hreq.c/send_json_cb(json_object*,uint64_t,char*,size_t)");ssize_t len = stpncpy(buf, get_json_string(obj)+pos, max) - buf;

		AKA_mark("lis===930###sois===23224###eois===23281###lif===3###soif===152###eoif===209###ins===true###function===./app-framework-binder/src/afb-hreq.c/send_json_cb(json_object*,uint64_t,char*,size_t)");return len ? : (ssize_t)MHD_CONTENT_READER_END_OF_STREAM;

}

/** Instrumented function req_reply(struct afb_xreq*,struct json_object*,const char*,const char*) */
static void req_reply(struct afb_xreq *xreq, struct json_object *object, const char *error, const char *info)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===935###sois===23398###eois===23413###lif===2###soif===113###eoif===128###ins===true###function===./app-framework-binder/src/afb-hre/* Cant instrument this following code */
q.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");struct afb_hreq
 /* Cant instrument this following code */
*hreq = CONTAINER_OF_XREQ(struct afb_hreq, xreq);
		AKA_mark("lis===936###sois===23465###eois===23497###lif===3###soif===180###eoif===212###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");struct json_object *sub, *reply;

		AKA_mark("lis===937###sois===23499###eois===23517###lif===4###soif===214###eoif===232###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");const char *reqid;

		AKA_mark("lis===938###sois===23519###eois===23549###lif===5###soif===234###eoif===264###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");struct MHD_Response *response;


	/* create the reply */
		AKA_mark("lis===941###sois===23576###eois===23640###lif===8###soif===291###eoif===355###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");reply = afb_msg_json_reply(object, error, info, &xreq->context);


	/* append the req id on need */
		AKA_mark("lis===944###sois===23676###eois===23732###lif===11###soif===391###eoif===447###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");reqid = afb_hreq_get_argument(hreq, long_key_for_reqid);

		if (AKA_mark("lis===945###sois===23738###eois===23751###lif===12###soif===453###eoif===466###ifc===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)") && (AKA_mark("lis===945###sois===23738###eois===23751###lif===12###soif===453###eoif===466###isc===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)")&&reqid == NULL)) {
		AKA_mark("lis===946###sois===23755###eois===23812###lif===13###soif===470###eoif===527###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");reqid = afb_hreq_get_argument(hreq, short_key_for_reqid);
	}
	else {AKA_mark("lis===-945-###sois===-23738-###eois===-2373813-###lif===-12-###soif===-###eoif===-466-###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");}

		if (AKA_mark("lis===947###sois===23818###eois===23884###lif===14###soif===533###eoif===599###ifc===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)") && ((AKA_mark("lis===947###sois===23818###eois===23831###lif===14###soif===533###eoif===546###isc===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)")&&reqid != NULL)	&&(AKA_mark("lis===947###sois===23835###eois===23884###lif===14###soif===550###eoif===599###isc===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)")&&json_object_object_get_ex(reply, "request", &sub)))) {
		AKA_mark("lis===948###sois===23888###eois===23956###lif===15###soif===603###eoif===671###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");json_object_object_add(sub, "reqid", json_object_new_string(reqid));
	}
	else {AKA_mark("lis===-947-###sois===-23818-###eois===-2381866-###lif===-14-###soif===-###eoif===-599-###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");}


		AKA_mark("lis===950###sois===23959###eois===24136###lif===17###soif===674###eoif===851###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");response = MHD_create_response_from_callback(
			(uint64_t)strlen(get_json_string(reply)),
			SIZE_RESPONSE_BUFFER,
			(void*)send_json_cb,
			reply,
			(void*)json_object_put);


	/* handle authorisation feedback */
		if (AKA_mark("lis===958###sois===24180###eois===24217###lif===25###soif===895###eoif===932###ifc===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)") && (AKA_mark("lis===958###sois===24180###eois===24217###lif===25###soif===895###eoif===932###isc===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)")&&error == afb_error_text_invalid_token)) {
		AKA_mark("lis===959###sois===24221###eois===24342###lif===26###soif===936###eoif===1057###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");afb_hreq_reply(hreq, MHD_HTTP_UNAUTHORIZED, response, MHD_HTTP_HEADER_WWW_AUTHENTICATE, "error=\"invalid_token\"", NULL);
	}
	else {
		if (AKA_mark("lis===960###sois===24353###eois===24395###lif===27###soif===1068###eoif===1110###ifc===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)") && (AKA_mark("lis===960###sois===24353###eois===24395###lif===27###soif===1068###eoif===1110###isc===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)")&&error == afb_error_text_insufficient_scope)) {
			AKA_mark("lis===961###sois===24399###eois===24522###lif===28###soif===1114###eoif===1237###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");afb_hreq_reply(hreq, MHD_HTTP_FORBIDDEN, response, MHD_HTTP_HEADER_WWW_AUTHENTICATE, "error=\"insufficient_scope\"", NULL);
		}
		else {
			AKA_mark("lis===963###sois===24531###eois===24581###lif===30###soif===1246###eoif===1296###ins===true###function===./app-framework-binder/src/afb-hreq.c/req_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");afb_hreq_reply(hreq, MHD_HTTP_OK, response, NULL);
		}
	}

}

/** Instrumented function afb_hreq_call(struct afb_hreq*,struct afb_apiset*,const char*,size_t,const char*,size_t) */
void afb_hreq_call(struct afb_hreq *hreq, struct afb_apiset *apiset, const char *api, size_t lenapi, const char *verb, size_t lenverb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_call(struct afb_hreq*,struct afb_apiset*,const char*,size_t,const char*,size_t)");AKA_fCall++;
		AKA_mark("lis===968###sois===24723###eois===24776###lif===2###soif===138###eoif===191###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_call(struct afb_hreq*,struct afb_apiset*,const char*,size_t,const char*,size_t)");hreq->xreq.request.called_api = strndup(api, lenapi);

		AKA_mark("lis===969###sois===24778###eois===24834###lif===3###soif===193###eoif===249###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_call(struct afb_hreq*,struct afb_apiset*,const char*,size_t,const char*,size_t)");hreq->xreq.request.called_verb = strndup(verb, lenverb);

		if (AKA_mark("lis===970###sois===24840###eois===24919###lif===4###soif===255###eoif===334###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_call(struct afb_hreq*,struct afb_apiset*,const char*,size_t,const char*,size_t)") && ((AKA_mark("lis===970###sois===24840###eois===24877###lif===4###soif===255###eoif===292###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_call(struct afb_hreq*,struct afb_apiset*,const char*,size_t,const char*,size_t)")&&hreq->xreq.request.called_api == NULL)	||(AKA_mark("lis===970###sois===24881###eois===24919###lif===4###soif===296###eoif===334###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_call(struct afb_hreq*,struct afb_apiset*,const char*,size_t,const char*,size_t)")&&hreq->xreq.request.called_verb == NULL))) {
				AKA_mark("lis===971###sois===24925###eois===24948###lif===5###soif===340###eoif===363###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_call(struct afb_hreq*,struct afb_apiset*,const char*,size_t,const char*,size_t)");ERROR("Out of memory");

				AKA_mark("lis===972###sois===24951###eois===25010###lif===6###soif===366###eoif===425###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_call(struct afb_hreq*,struct afb_apiset*,const char*,size_t,const char*,size_t)");afb_hreq_reply_error(hreq, MHD_HTTP_INTERNAL_SERVER_ERROR);

	}
	else {
		if (AKA_mark("lis===973###sois===25023###eois===25054###lif===7###soif===438###eoif===469###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_call(struct afb_hreq*,struct afb_apiset*,const char*,size_t,const char*,size_t)") && (AKA_mark("lis===973###sois===25023###eois===25054###lif===7###soif===438###eoif===469###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_call(struct afb_hreq*,struct afb_apiset*,const char*,size_t,const char*,size_t)")&&afb_hreq_init_context(hreq) < 0)) {
					AKA_mark("lis===974###sois===25060###eois===25119###lif===8###soif===475###eoif===534###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_call(struct afb_hreq*,struct afb_apiset*,const char*,size_t,const char*,size_t)");afb_hreq_reply_error(hreq, MHD_HTTP_INTERNAL_SERVER_ERROR);

	}
		else {
					AKA_mark("lis===976###sois===25132###eois===25170###lif===10###soif===547###eoif===585###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_call(struct afb_hreq*,struct afb_apiset*,const char*,size_t,const char*,size_t)");afb_xreq_unhooked_addref(&hreq->xreq);

					AKA_mark("lis===977###sois===25173###eois===25211###lif===11###soif===588###eoif===626###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_call(struct afb_hreq*,struct afb_apiset*,const char*,size_t,const char*,size_t)");afb_xreq_process(&hreq->xreq, apiset);

	}
	}

}

/** Instrumented function afb_hreq_init_context(struct afb_hreq*) */
int afb_hreq_init_context(struct afb_hreq *hreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");AKA_fCall++;
		AKA_mark("lis===983###sois===25270###eois===25287###lif===2###soif===52###eoif===69###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");const char *uuid;

		AKA_mark("lis===984###sois===25289###eois===25307###lif===3###soif===71###eoif===89###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");const char *token;

		AKA_mark("lis===985###sois===25309###eois===25331###lif===4###soif===91###eoif===113###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");struct afb_token *tok;


		if (AKA_mark("lis===987###sois===25338###eois===25372###lif===6###soif===120###eoif===154###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)") && (AKA_mark("lis===987###sois===25338###eois===25372###lif===6###soif===120###eoif===154###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)")&&hreq->xreq.context.session != NULL)) {
		AKA_mark("lis===988###sois===25376###eois===25385###lif===7###soif===158###eoif===167###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");return 0;
	}
	else {AKA_mark("lis===-987-###sois===-25338-###eois===-2533834-###lif===-6-###soif===-###eoif===-154-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");}


	/* get the uuid of the session */
		AKA_mark("lis===991###sois===25423###eois===25475###lif===10###soif===205###eoif===257###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");uuid = afb_hreq_get_header(hreq, long_key_for_uuid);

		if (AKA_mark("lis===992###sois===25481###eois===25493###lif===11###soif===263###eoif===275###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)") && (AKA_mark("lis===992###sois===25481###eois===25493###lif===11###soif===263###eoif===275###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)")&&uuid == NULL)) {
				AKA_mark("lis===993###sois===25499###eois===25553###lif===12###soif===281###eoif===335###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");uuid = afb_hreq_get_argument(hreq, long_key_for_uuid);

				if (AKA_mark("lis===994###sois===25560###eois===25572###lif===13###soif===342###eoif===354###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)") && (AKA_mark("lis===994###sois===25560###eois===25572###lif===13###soif===342###eoif===354###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)")&&uuid == NULL)) {
						AKA_mark("lis===995###sois===25579###eois===25625###lif===14###soif===361###eoif===407###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");uuid = afb_hreq_get_cookie(hreq, cookie_name);

						if (AKA_mark("lis===996###sois===25633###eois===25645###lif===15###soif===415###eoif===427###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)") && (AKA_mark("lis===996###sois===25633###eois===25645###lif===15###soif===415###eoif===427###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)")&&uuid == NULL)) {
				AKA_mark("lis===997###sois===25651###eois===25706###lif===16###soif===433###eoif===488###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");uuid = afb_hreq_get_argument(hreq, short_key_for_uuid);
			}
			else {AKA_mark("lis===-996-###sois===-25633-###eois===-2563312-###lif===-15-###soif===-###eoif===-427-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");}

		}
		else {AKA_mark("lis===-994-###sois===-25560-###eois===-2556012-###lif===-13-###soif===-###eoif===-354-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");}

	}
	else {AKA_mark("lis===-992-###sois===-25481-###eois===-2548112-###lif===-11-###soif===-###eoif===-275-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");}


	/* get the authorisation token */
		AKA_mark("lis===1002###sois===25751###eois===25799###lif===21###soif===533###eoif===581###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");token = afb_hreq_get_authorization_bearer(hreq);

		if (AKA_mark("lis===1003###sois===25805###eois===25818###lif===22###soif===587###eoif===600###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)") && (AKA_mark("lis===1003###sois===25805###eois===25818###lif===22###soif===587###eoif===600###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)")&&token == NULL)) {
				AKA_mark("lis===1004###sois===25824###eois===25882###lif===23###soif===606###eoif===664###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");token = afb_hreq_get_argument(hreq, key_for_access_token);

				if (AKA_mark("lis===1005###sois===25889###eois===25902###lif===24###soif===671###eoif===684###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)") && (AKA_mark("lis===1005###sois===25889###eois===25902###lif===24###soif===671###eoif===684###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)")&&token == NULL)) {
						AKA_mark("lis===1006###sois===25909###eois===25963###lif===25###soif===691###eoif===745###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");token = afb_hreq_get_header(hreq, long_key_for_token);

						if (AKA_mark("lis===1007###sois===25971###eois===25984###lif===26###soif===753###eoif===766###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)") && (AKA_mark("lis===1007###sois===25971###eois===25984###lif===26###soif===753###eoif===766###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)")&&token == NULL)) {
								AKA_mark("lis===1008###sois===25992###eois===26048###lif===27###soif===774###eoif===830###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");token = afb_hreq_get_argument(hreq, long_key_for_token);

								if (AKA_mark("lis===1009###sois===26057###eois===26070###lif===28###soif===839###eoif===852###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)") && (AKA_mark("lis===1009###sois===26057###eois===26070###lif===28###soif===839###eoif===852###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)")&&token == NULL)) {
					AKA_mark("lis===1010###sois===26077###eois===26134###lif===29###soif===859###eoif===916###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");token = afb_hreq_get_argument(hreq, short_key_for_token);
				}
				else {AKA_mark("lis===-1009-###sois===-26057-###eois===-2605713-###lif===-28-###soif===-###eoif===-852-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");}

			}
			else {AKA_mark("lis===-1007-###sois===-25971-###eois===-2597113-###lif===-26-###soif===-###eoif===-766-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");}

		}
		else {AKA_mark("lis===-1005-###sois===-25889-###eois===-2588913-###lif===-24-###soif===-###eoif===-684-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");}

	}
	else {AKA_mark("lis===-1003-###sois===-25805-###eois===-2580513-###lif===-22-###soif===-###eoif===-600-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");}

		AKA_mark("lis===1014###sois===26148###eois===26159###lif===33###soif===930###eoif===941###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");tok = NULL;

		if (AKA_mark("lis===1015###sois===26165###eois===26170###lif===34###soif===947###eoif===952###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)") && (AKA_mark("lis===1015###sois===26165###eois===26170###lif===34###soif===947###eoif===952###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)")&&token)) {
		AKA_mark("lis===1016###sois===26174###eois===26201###lif===35###soif===956###eoif===983###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");afb_token_get(&tok, token);
	}
	else {AKA_mark("lis===-1015-###sois===-26165-###eois===-261655-###lif===-34-###soif===-###eoif===-952-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");}


		AKA_mark("lis===1018###sois===26204###eois===26269###lif===37###soif===986###eoif===1051###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_context(struct afb_hreq*)");return afb_context_connect(&hreq->xreq.context, uuid, tok, NULL);

}

/** Instrumented function afb_hreq_init_cookie(int,const char*,int) */
int afb_hreq_init_cookie(int port, const char *path, int maxage)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)");AKA_fCall++;
		AKA_mark("lis===1023###sois===26341###eois===26348###lif===2###soif===68###eoif===75###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)");int rc;


		AKA_mark("lis===1025###sois===26351###eois===26369###lif===4###soif===78###eoif===96###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)");free(cookie_name);

		AKA_mark("lis===1026###sois===26371###eois===26391###lif===5###soif===98###eoif===118###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)");free(cookie_setter);

		AKA_mark("lis===1027###sois===26393###eois===26412###lif===6###soif===120###eoif===139###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)");cookie_name = NULL;

		AKA_mark("lis===1028###sois===26414###eois===26435###lif===7###soif===141###eoif===162###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)");cookie_setter = NULL;


		AKA_mark("lis===1030###sois===26438###eois===26458###lif===9###soif===165###eoif===185###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)");path = path ? : "/";

		AKA_mark("lis===1031###sois===26460###eois===26522###lif===10###soif===187###eoif===249###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)");rc = asprintf(&cookie_name, "%s-%d", long_key_for_uuid, port);

		if (AKA_mark("lis===1032###sois===26528###eois===26534###lif===11###soif===255###eoif===261###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)") && (AKA_mark("lis===1032###sois===26528###eois===26534###lif===11###soif===255###eoif===261###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)")&&rc < 0)) {
		AKA_mark("lis===1033###sois===26538###eois===26547###lif===12###soif===265###eoif===274###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)");return 0;
	}
	else {AKA_mark("lis===-1032-###sois===-26528-###eois===-265286-###lif===-11-###soif===-###eoif===-261-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)");}

		AKA_mark("lis===1034###sois===26549###eois===26650###lif===13###soif===276###eoif===377###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)");rc = asprintf(&cookie_setter, "%s=%%s; Path=%s; Max-Age=%d; HttpOnly",
			cookie_name, path, maxage);

		if (AKA_mark("lis===1036###sois===26656###eois===26662###lif===15###soif===383###eoif===389###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)") && (AKA_mark("lis===1036###sois===26656###eois===26662###lif===15###soif===383###eoif===389###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)")&&rc < 0)) {
		AKA_mark("lis===1037###sois===26666###eois===26675###lif===16###soif===393###eoif===402###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)");return 0;
	}
	else {AKA_mark("lis===-1036-###sois===-26656-###eois===-266566-###lif===-15-###soif===-###eoif===-389-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)");}

		AKA_mark("lis===1038###sois===26677###eois===26686###lif===17###soif===404###eoif===413###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_init_cookie(int,const char*,int)");return 1;

}

/** Instrumented function afb_hreq_to_xreq(struct afb_hreq*) */
struct afb_xreq *afb_hreq_to_xreq(struct afb_hreq *hreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_to_xreq(struct afb_hreq*)");AKA_fCall++;
		AKA_mark("lis===1043###sois===26750###eois===26769###lif===2###soif===60###eoif===79###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_to_xreq(struct afb_hreq*)");return &hreq->xreq;

}

/** Instrumented function afb_hreq_create() */
struct afb_hreq *afb_hreq_create()
{AKA_mark("Calling: ./app-framework-binder/src/afb-hreq.c/afb_hreq_create()");AKA_fCall++;
		AKA_mark("lis===1048###sois===26811###eois===26859###lif===2###soif===38###eoif===86###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_create()");struct afb_hreq *hreq = calloc(1, sizeof *hreq);

		if (AKA_mark("lis===1049###sois===26865###eois===26869###lif===3###soif===92###eoif===96###ifc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_create()") && (AKA_mark("lis===1049###sois===26865###eois===26869###lif===3###soif===92###eoif===96###isc===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_create()")&&hreq)) {
		/* init the request */
				AKA_mark("lis===1051###sois===26900###eois===26953###lif===5###soif===127###eoif===180###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_create()");afb_xreq_init(&hreq->xreq, &afb_hreq_xreq_query_itf);

				AKA_mark("lis===1052###sois===26956###eois===26986###lif===6###soif===183###eoif===213###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_create()");hreq->reqid = ++global_reqids;

	}
	else {AKA_mark("lis===-1049-###sois===-26865-###eois===-268654-###lif===-3-###soif===-###eoif===-96-###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_create()");}

		AKA_mark("lis===1054###sois===26991###eois===27003###lif===8###soif===218###eoif===230###ins===true###function===./app-framework-binder/src/afb-hreq.c/afb_hreq_create()");return hreq;

}


#endif

