/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_EXPORT_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_EXPORT_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fnmatch.h>
#include <ctype.h>

#include <json-c/json.h>
#if !defined(JSON_C_TO_STRING_NOSLASHESCAPE)
#define JSON_C_TO_STRING_NOSLASHESCAPE 0
#endif

#define AFB_BINDING_VERSION 0
#include <afb/afb-binding.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_H_
#define AKA_INCLUDE__AFB_API_H_
#include "afb-api.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_APISET_H_
#define AKA_INCLUDE__AFB_APISET_H_
#include "afb-apiset.akaignore.h"
#endif

#if WITH_LEGACY_BINDING_V1
#include "afb-api-so-v1.h"
#endif
#if WITH_LEGACY_BINDING_V2
#include "afb-api-so-v2.h"
#endif
/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_V3_H_
#define AKA_INCLUDE__AFB_API_V3_H_
#include "afb-api-v3.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_COMMON_H_
#define AKA_INCLUDE__AFB_COMMON_H_
#include "afb-common.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_EVT_H_
#define AKA_INCLUDE__AFB_EVT_H_
#include "afb-evt.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_EXPORT_H_
#define AKA_INCLUDE__AFB_EXPORT_H_
#include "afb-export.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HOOK_H_
#define AKA_INCLUDE__AFB_HOOK_H_
#include "afb-hook.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_MSG_JSON_H_
#define AKA_INCLUDE__AFB_MSG_JSON_H_
#include "afb-msg-json.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SESSION_H_
#define AKA_INCLUDE__AFB_SESSION_H_
#include "afb-session.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_XREQ_H_
#define AKA_INCLUDE__AFB_XREQ_H_
#include "afb-xreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CALLS_H_
#define AKA_INCLUDE__AFB_CALLS_H_
#include "afb-calls.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_ERROR_TEXT_H_
#define AKA_INCLUDE__AFB_ERROR_TEXT_H_
#include "afb-error-text.akaignore.h"
#endif


/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__SYSTEMD_H_
#define AKA_INCLUDE__SYSTEMD_H_
#include "systemd.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__JOBS_H_
#define AKA_INCLUDE__JOBS_H_
#include "jobs.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__GLOBSET_H_
#define AKA_INCLUDE__GLOBSET_H_
#include "globset.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__SIG_MONITOR_H_
#define AKA_INCLUDE__SIG_MONITOR_H_
#include "sig-monitor.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__WRAP_JSON_H_
#define AKA_INCLUDE__WRAP_JSON_H_
#include "wrap-json.akaignore.h"
#endif


/*************************************************************************
 * internal types
 ************************************************************************/

/*
 * Actually supported versions
 */
enum afb_api_version
{
	Api_Version_None = 0,
#if WITH_LEGACY_BINDING_V1
	Api_Version_1 = 1,
#endif
#if WITH_LEGACY_BINDING_V2
	Api_Version_2 = 2,
#endif
	Api_Version_3 = 3
};

/*
 * The states of exported APIs
 */
enum afb_api_state
{
	Api_State_Pre_Init,
	Api_State_Init,
	Api_State_Run
};

/*
 * structure of the exported API
 */
struct afb_export
{
	/* keep it first */
	struct afb_api_x3 api;

	/* reference count */
	int refcount;

	/* version of the api */
	unsigned version: 4;

	/* current state */
	unsigned state: 4;

	/* declared */
	unsigned declared: 1;

	/* unsealed */
	unsigned unsealed: 1;

#if WITH_AFB_HOOK
	/* hooking flags */
	int hookditf;
	int hooksvc;
#endif

	/* session for service */
	struct afb_session *session;

	/* apiset the API is declared in */
	struct afb_apiset *declare_set;

	/* apiset for calls */
	struct afb_apiset *call_set;

	/* event listener for service or NULL */
	struct afb_evt_listener *listener;

	/* event handler list */
	struct globset *event_handlers;

	/* creator if any */
	struct afb_export *creator;

	/* path indication if any */
	const char *path;

	/* settings */
	struct json_object *settings;

	/* internal descriptors */
	union {
#if WITH_LEGACY_BINDING_V1
		struct afb_binding_v1 *v1;
#endif
		const struct afb_binding_v2 *v2;
		struct afb_api_v3 *v3;
	} desc;

	/* start function */
	union {
#if WITH_LEGACY_BINDING_V1
		int (*v1)(struct afb_service_x1);
#endif
		int (*v2)();
		int (*v3)(struct afb_api_x3 *api);
	} init;

	/* event handling */
	void (*on_any_event_v12)(const char *event, struct json_object *object);
	void (*on_any_event_v3)(struct afb_api_x3 *api, const char *event, struct json_object *object);

	/* exported data */
	union {
#if WITH_LEGACY_BINDING_V1
		struct afb_binding_interface_v1 v1;
#endif
		struct afb_binding_data_v2 *v2;
	} export;

	/* initial name */
	char name[];
};

/*****************************************************************************/

static inline struct afb_api_x3 *to_api_x3(struct afb_export *export)
{
	return (struct afb_api_x3*)export;
}

/** Instrumented function from_api_x3(struct afb_api_x3*) */
static inline struct afb_export *from_api_x3(struct afb_api_x3 *api)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/from_api_x3(struct afb_api_x3*)");AKA_fCall++;
		AKA_mark("lis===185###sois===3822###eois===3853###lif===2###soif===72###eoif===103###ins===true###function===./app-framework-binder/src/afb-export.c/from_api_x3(struct afb_api_x3*)");return (struct afb_export*)api;

}

/** Instrumented function afb_export_from_api_x3(struct afb_api_x3*) */
struct afb_export *afb_export_from_api_x3(struct afb_api_x3 *api)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/afb_export_from_api_x3(struct afb_api_x3*)");AKA_fCall++;
		AKA_mark("lis===190###sois===3926###eois===3950###lif===2###soif===69###eoif===93###ins===true###function===./app-framework-binder/src/afb-export.c/afb_export_from_api_x3(struct afb_api_x3*)");return from_api_x3(api);

}

struct afb_api_x3 *afb_export_to_api_x3(struct afb_export *export)
{
	return to_api_x3(export);
}

/******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
	SETTINGS
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************/

static struct json_object *configuration;

/** Instrumented function afb_export_set_config(struct json_object*) */
void afb_export_set_config(struct json_object *config)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/afb_export_set_config(struct json_object*)");AKA_fCall++;
		AKA_mark("lis===212###sois===4806###eois===4847###lif===2###soif===58###eoif===99###ins===true###function===./app-framework-binder/src/afb-export.c/afb_export_set_config(struct json_object*)");struct json_object *save = configuration;

		AKA_mark("lis===213###sois===4849###eois===4889###lif===3###soif===101###eoif===141###ins===true###function===./app-framework-binder/src/afb-export.c/afb_export_set_config(struct json_object*)");configuration = json_object_get(config);

		AKA_mark("lis===214###sois===4891###eois===4913###lif===4###soif===143###eoif===165###ins===true###function===./app-framework-binder/src/afb-export.c/afb_export_set_config(struct json_object*)");json_object_put(save);

}

static struct json_object *make_settings(struct afb_export *export)
{
	struct json_object *result;
	struct json_object *obj;
	struct afb_export *iter;
	char *path;

	/* clone the globals */
	if (json_object_object_get_ex(configuration, "*", &obj))
		result = wrap_json_clone(obj);
	else
		result = json_object_new_object();

	/* add locals */
	if (json_object_object_get_ex(configuration, export->name, &obj))
		wrap_json_object_add(result, obj);

	/* add library path */
	for (iter = export ; iter && !iter->path ; iter = iter->creator);
	if (iter) {
		path = realpath(iter->path, NULL);
		json_object_object_add(result, "binding-path", json_object_new_string(path));
		free(path);
	}

	export->settings = result;
	return result;
}

/******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
                                           F R O M     D I T F
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************/

/**********************************************
* normal flow
**********************************************/
/** Instrumented function vverbose_cb(struct afb_api_x3*,int,const char*,int,const char*,const char*,va_list) */
static void vverbose_cb(struct afb_api_x3 *closure, int level, const char *file, int line, const char *function, const char *fmt, va_list args)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/vverbose_cb(struct afb_api_x3*,int,const char*,int,const char*,const char*,va_list)");AKA_fCall++;
		AKA_mark("lis===261###sois===6613###eois===6621###lif===2###soif===147###eoif===155###ins===true###function===./app-framework-binder/src/afb-export.c/vverbose_cb(struct afb_api_x3*,int,const char*,int,const char*,const char*,va_list)");char *p;

		AKA_mark("lis===262###sois===6623###eois===6640###lif===3###soif===157###eoif===174###ins===true###function===./app-framework-binder/src/afb-export.c/vverbose_cb(struct afb_api_x3*,int,const char*,int,const char*,const char*,va_list)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(closure);

		if (AKA_mark("lis===264###sois===6679###eois===6715###lif===5###soif===213###eoif===249###ifc===true###function===./app-framework-binder/src/afb-export.c/vverbose_cb(struct afb_api_x3*,int,const char*,int,const char*,const char*,va_list)") && ((AKA_mark("lis===264###sois===6679###eois===6683###lif===5###soif===213###eoif===217###isc===true###function===./app-framework-binder/src/afb-export.c/vverbose_cb(struct afb_api_x3*,int,const char*,int,const char*,const char*,va_list)")&&!fmt)	||(AKA_mark("lis===264###sois===6687###eois===6715###lif===5###soif===221###eoif===249###isc===true###function===./app-framework-binder/src/afb-export.c/vverbose_cb(struct afb_api_x3*,int,const char*,int,const char*,const char*,va_list)")&&vasprintf(&p, fmt, args) < 0))) {
		AKA_mark("lis===265###sois===6719###eois===6768###lif===6###soif===253###eoif===302###ins===true###function===./app-framework-binder/src/afb-export.c/vverbose_cb(struct afb_api_x3*,int,const char*,int,const char*,const char*,va_list)");vverbose(level, file, line, function, fmt, args);
	}
	else {
		/* Cant instrument this following code */
verbose(level, file, line, function, (verbose_is_colorized() == 0 ? "[API %s] %s" : COLOR_API "[API %s]" COLOR_DEFAULT " %s"), export->api.apiname, p);
				AKA_mark("lis===268###sois===6933###eois===6941###lif===9###soif===467###eoif===475###ins===true###function===./app-framework-binder/src/afb-export.c/vverbose_cb(struct afb_api_x3*,int,const char*,int,const char*,const char*,va_list)");free(p);

	}

}

/** Instrumented function event_x2_make_cb(struct afb_api_x3*,const char*) */
static struct afb_event_x2 *event_x2_make_cb(struct afb_api_x3 *closure, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/event_x2_make_cb(struct afb_api_x3*,const char*)");AKA_fCall++;
		AKA_mark("lis===274###sois===7042###eois===7059###lif===2###soif===94###eoif===111###ins===true###function===./app-framework-binder/src/afb-export.c/event_x2_make_cb(struct afb_api_x3*,const char*)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(closure);

	/* check daemon state */
		if (AKA_mark("lis===277###sois===7124###eois===7159###lif===5###soif===176###eoif===211###ifc===true###function===./app-framework-binder/src/afb-export.c/event_x2_make_cb(struct afb_api_x3*,const char*)") && (AKA_mark("lis===277###sois===7124###eois===7159###lif===5###soif===176###eoif===211###isc===true###function===./app-framework-binder/src/afb-export.c/event_x2_make_cb(struct afb_api_x3*,const char*)")&&export->state == Api_State_Pre_Init)) {
		/* Cant instrument this following code */
ERROR("[API %s] Bad call to 'afb_daemon_event_make(%s)', must not be in PreInit", export->api.apiname, name);
				AKA_mark("lis===279###sois===7277###eois===7292###lif===7###soif===329###eoif===344###ins===true###function===./app-framework-binder/src/afb-export.c/event_x2_make_cb(struct afb_api_x3*,const char*)");errno = EINVAL;

				AKA_mark("lis===280###sois===7295###eois===7307###lif===8###soif===347###eoif===359###ins===true###function===./app-framework-binder/src/afb-export.c/event_x2_make_cb(struct afb_api_x3*,const char*)");return NULL;

	}
	else {AKA_mark("lis===-277-###sois===-7124-###eois===-712435-###lif===-5-###soif===-###eoif===-211-###ins===true###function===./app-framework-binder/src/afb-export.c/event_x2_make_cb(struct afb_api_x3*,const char*)");}


	/* create the event */
	/* Cant instrument this following code */
return afb_evt_event_x2_create2(export->api.apiname, name);
}

/** Instrumented function event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*) */
static int event_broadcast_cb(struct afb_api_x3 *closure, const char *name, struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===289###sois===7507###eois===7525###lif===2###soif===107###eoif===125###ins===true###function===./app-framework-binder/src/afb-export.c/event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*)");size_t plen, nlen;

		AKA_mark("lis===290###sois===7527###eois===7539###lif===3###soif===127###eoif===139###ins===true###function===./app-framework-binder/src/afb-export.c/event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*)");char *event;

		AKA_mark("lis===291###sois===7541###eois===7558###lif===4###soif===141###eoif===158###ins===true###function===./app-framework-binder/src/afb-export.c/event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(closure);

	/* check daemon state */
		if (AKA_mark("lis===294###sois===7623###eois===7658###lif===7###soif===223###eoif===258###ifc===true###function===./app-framework-binder/src/afb-export.c/event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*)") && (AKA_mark("lis===294###sois===7623###eois===7658###lif===7###soif===223###eoif===258###isc===true###function===./app-framework-binder/src/afb-export.c/event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*)")&&export->state == Api_State_Pre_Init)) {
		/* Cant instrument this following code */
ERROR("[API %s] Bad call to 'afb_daemon_event_broadcast(%s, %s)', must not be in PreInit",
			export->api.apiname, name, json_object_to_json_string_ext(object, JSON_C_TO_STRING_NOSLASHESCAPE));
				AKA_mark("lis===297###sois===7860###eois===7875###lif===10###soif===460###eoif===475###ins===true###function===./app-framework-binder/src/afb-export.c/event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*)");errno = EINVAL;

				AKA_mark("lis===298###sois===7878###eois===7887###lif===11###soif===478###eoif===487###ins===true###function===./app-framework-binder/src/afb-export.c/event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*)");return 0;

	}
	else {AKA_mark("lis===-294-###sois===-7623-###eois===-762335-###lif===-7-###soif===-###eoif===-258-###ins===true###function===./app-framework-binder/src/afb-export.c/event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*)");}


	/* makes the event name */
	/* Cant instrument this following code */
plen = strlen(export->api.apiname);
		AKA_mark("lis===303###sois===7958###eois===7978###lif===16###soif===558###eoif===578###ins===true###function===./app-framework-binder/src/afb-export.c/event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*)");nlen = strlen(name);

		AKA_mark("lis===304###sois===7980###eois===8012###lif===17###soif===580###eoif===612###ins===true###function===./app-framework-binder/src/afb-export.c/event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*)");event = alloca(nlen + plen + 2);

	/* Cant instrument this following code */
memcpy(event, export->api.apiname, plen);
		AKA_mark("lis===306###sois===8057###eois===8075###lif===19###soif===657###eoif===675###ins===true###function===./app-framework-binder/src/afb-export.c/event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*)");event[plen] = '/';

		AKA_mark("lis===307###sois===8077###eois===8118###lif===20###soif===677###eoif===718###ins===true###function===./app-framework-binder/src/afb-export.c/event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*)");memcpy(event + plen + 1, name, nlen + 1);


	/* broadcast the event */
		AKA_mark("lis===310###sois===8148###eois===8188###lif===23###soif===748###eoif===788###ins===true###function===./app-framework-binder/src/afb-export.c/event_broadcast_cb(struct afb_api_x3*,const char*,struct json_object*)");return afb_evt_broadcast(event, object);

}

/** Instrumented function get_event_loop(struct afb_api_x3*) */
static struct sd_event *get_event_loop(struct afb_api_x3 *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/get_event_loop(struct afb_api_x3*)");AKA_fCall++;
		AKA_mark("lis===315###sois===8262###eois===8291###lif===2###soif===70###eoif===99###ins===true###function===./app-framework-binder/src/afb-export.c/get_event_loop(struct afb_api_x3*)");jobs_acquire_event_manager();

		AKA_mark("lis===316###sois===8293###eois===8325###lif===3###soif===101###eoif===133###ins===true###function===./app-framework-binder/src/afb-export.c/get_event_loop(struct afb_api_x3*)");return systemd_get_event_loop();

}

/** Instrumented function get_user_bus(struct afb_api_x3*) */
static struct sd_bus *get_user_bus(struct afb_api_x3 *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/get_user_bus(struct afb_api_x3*)");AKA_fCall++;
		AKA_mark("lis===321###sois===8395###eois===8424###lif===2###soif===66###eoif===95###ins===true###function===./app-framework-binder/src/afb-export.c/get_user_bus(struct afb_api_x3*)");jobs_acquire_event_manager();

		AKA_mark("lis===322###sois===8426###eois===8456###lif===3###soif===97###eoif===127###ins===true###function===./app-framework-binder/src/afb-export.c/get_user_bus(struct afb_api_x3*)");return systemd_get_user_bus();

}

/** Instrumented function get_system_bus(struct afb_api_x3*) */
static struct sd_bus *get_system_bus(struct afb_api_x3 *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/get_system_bus(struct afb_api_x3*)");AKA_fCall++;
		AKA_mark("lis===327###sois===8528###eois===8557###lif===2###soif===68###eoif===97###ins===true###function===./app-framework-binder/src/afb-export.c/get_system_bus(struct afb_api_x3*)");jobs_acquire_event_manager();

		AKA_mark("lis===328###sois===8559###eois===8591###lif===3###soif===99###eoif===131###ins===true###function===./app-framework-binder/src/afb-export.c/get_system_bus(struct afb_api_x3*)");return systemd_get_system_bus();

}

/** Instrumented function rootdir_open_locale_cb(struct afb_api_x3*,const char*,int,const char*) */
static int rootdir_open_locale_cb(struct afb_api_x3 *closure, const char *filename, int flags, const char *locale)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/rootdir_open_locale_cb(struct afb_api_x3*,const char*,int,const char*)");AKA_fCall++;
		AKA_mark("lis===333###sois===8713###eois===8776###lif===2###soif===118###eoif===181###ins===true###function===./app-framework-binder/src/afb-export.c/rootdir_open_locale_cb(struct afb_api_x3*,const char*,int,const char*)");return afb_common_rootdir_open_locale(filename, flags, locale);

}

/** Instrumented function queue_job_cb(struct afb_api_x3*,void(*callback)(int signum, void*arg),void*,void*,int) */
static int queue_job_cb(struct afb_api_x3 *closure, void (*callback)(int signum, void *arg), void *argument, void *group, int timeout)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/queue_job_cb(struct afb_api_x3*,void(*callback)(int signum, void*arg),void*,void*,int)");AKA_fCall++;
		AKA_mark("lis===338###sois===8918###eois===8972###lif===2###soif===138###eoif===192###ins===true###function===./app-framework-binder/src/afb-export.c/queue_job_cb(struct afb_api_x3*,void(*callback)(int signum, void*arg),void*,void*,int)");return jobs_queue(group, timeout, callback, argument);

}

/** Instrumented function require_api_cb(struct afb_api_x3*,const char*,int) */
static int require_api_cb(struct afb_api_x3 *closure, const char *name, int initialized)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===343###sois===9068###eois===9085###lif===2###soif===92###eoif===109###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(closure);
		AKA_mark("lis===344###sois===9119###eois===9131###lif===3###soif===143###eoif===155###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");int rc, rc2;

		AKA_mark("lis===345###sois===9133###eois===9156###lif===4###soif===157###eoif===180###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");char *iter, *end, save;


	/* emit a warning about unexpected require in preinit */
		if (AKA_mark("lis===348###sois===9221###eois===9271###lif===7###soif===245###eoif===295###ifc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)") && (AKA_mark("lis===348###sois===9221###eois===9271###lif===7###soif===245###eoif===295###isc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)")&&export->state == Api_State_Pre_Init && initialized)) {
		/* Cant instrument this following code */
ERROR("[API %s] requiring initialized apis in pre-init is forbiden", export->api.apiname);
				AKA_mark("lis===350###sois===9370###eois===9385###lif===9###soif===394###eoif===409###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");errno = EINVAL;

				AKA_mark("lis===351###sois===9388###eois===9398###lif===10###soif===412###eoif===422###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");return -1;

	}
	else {AKA_mark("lis===-348-###sois===-9221-###eois===-922150-###lif===-7-###soif===-###eoif===-295-###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");}


	/* scan the names in a local copy */
		AKA_mark("lis===355###sois===9442###eois===9449###lif===14###soif===466###eoif===473###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");rc = 0;

		AKA_mark("lis===356###sois===9451###eois===9472###lif===15###soif===475###eoif===496###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");iter = strdupa(name);

		for (;;) {
		/* skip any space */
				AKA_mark("lis===359###sois===9509###eois===9522###lif===18###soif===533###eoif===546###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");save = *iter;

				while (AKA_mark("lis===360###sois===9531###eois===9544###lif===19###soif===555###eoif===568###ifc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)") && (AKA_mark("lis===360###sois===9531###eois===9544###lif===19###soif===555###eoif===568###isc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)")&&isspace(save))) {
			AKA_mark("lis===361###sois===9549###eois===9564###lif===20###soif===573###eoif===588###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");save = *++iter;
		}

				if (AKA_mark("lis===362###sois===9571###eois===9576###lif===21###soif===595###eoif===600###ifc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)") && (AKA_mark("lis===362###sois===9571###eois===9576###lif===21###soif===595###eoif===600###isc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)")&&!save)) {
			AKA_mark("lis===363###sois===9595###eois===9605###lif===22###soif===619###eoif===629###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");return rc;
		}
		else {AKA_mark("lis===-362-###sois===-9571-###eois===-95715-###lif===-21-###soif===-###eoif===-600-###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");}


		/* search for the end */
				AKA_mark("lis===366###sois===9636###eois===9647###lif===25###soif===660###eoif===671###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");end = iter;

				while (AKA_mark("lis===367###sois===9657###eois===9679###lif===26###soif===681###eoif===703###ifc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)") && ((AKA_mark("lis===367###sois===9657###eois===9661###lif===26###soif===681###eoif===685###isc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)")&&save)	&&(AKA_mark("lis===367###sois===9665###eois===9679###lif===26###soif===689###eoif===703###isc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)")&&!isspace(save)))) {
			AKA_mark("lis===368###sois===9684###eois===9698###lif===27###soif===708###eoif===722###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");save = *++end;
		}

				AKA_mark("lis===369###sois===9701###eois===9710###lif===28###soif===725###eoif===734###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");*end = 0;


		/* check the required api */
				if (AKA_mark("lis===372###sois===9749###eois===9784###lif===31###soif===773###eoif===808###ifc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)") && (AKA_mark("lis===372###sois===9749###eois===9784###lif===31###soif===773###eoif===808###isc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)")&&export->state == Api_State_Pre_Init)) {
			/* Cant instrument this following code */
rc2 = afb_apiset_require(export->declare_set, export->api.apiname, iter);
						if (AKA_mark("lis===374###sois===9872###eois===9879###lif===33###soif===896###eoif===903###ifc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)") && (AKA_mark("lis===374###sois===9872###eois===9879###lif===33###soif===896###eoif===903###isc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)")&&rc2 < 0)) {
				/* Cant instrument this following code */
if (rc == 0)
					WARNING("[API %s] requiring apis pre-init may lead to unexpected result", export->api.apiname);
				/* Cant instrument this following code */
ERROR("[API %s] requiring api %s in pre-init failed", export->api.apiname, iter);
			}
			else {AKA_mark("lis===-374-###sois===-9872-###eois===-98727-###lif===-33-###soif===-###eoif===-903-###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");}

		}
		else {
			/* Cant instrument this following code */
rc2 = -!((initialized ? afb_apiset_lookup_started : afb_apiset_lookup)(export->call_set, iter, 1));
						if (AKA_mark("lis===381###sois===10213###eois===10220###lif===40###soif===1237###eoif===1244###ifc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)") && (AKA_mark("lis===381###sois===10213###eois===10220###lif===40###soif===1237###eoif===1244###isc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)")&&rc2 < 0)) {
				/* Cant instrument this following code */
ERROR("[API %s] requiring api %s%s failed", export->api.apiname,
					 iter, initialized ? " initialized" : "");
			}
			else {AKA_mark("lis===-381-###sois===-10213-###eois===-102137-###lif===-40-###soif===-###eoif===-1244-###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");}

		}

				if (AKA_mark("lis===386###sois===10356###eois===10363###lif===45###soif===1380###eoif===1387###ifc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)") && (AKA_mark("lis===386###sois===10356###eois===10363###lif===45###soif===1380###eoif===1387###isc===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)")&&rc2 < 0)) {
			AKA_mark("lis===387###sois===10368###eois===10377###lif===46###soif===1392###eoif===1401###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");rc = rc2;
		}
		else {AKA_mark("lis===-386-###sois===-10356-###eois===-103567-###lif===-45-###soif===-###eoif===-1387-###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");}


				AKA_mark("lis===389###sois===10381###eois===10393###lif===48###soif===1405###eoif===1417###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");*end = save;

				AKA_mark("lis===390###sois===10396###eois===10407###lif===49###soif===1420###eoif===1431###ins===true###function===./app-framework-binder/src/afb-export.c/require_api_cb(struct afb_api_x3*,const char*,int)");iter = end;

	}

}

/** Instrumented function add_alias_cb(struct afb_api_x3*,const char*,const char*) */
static int add_alias_cb(struct afb_api_x3 *closure, const char *apiname, const char *aliasname)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/add_alias_cb(struct afb_api_x3*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===396###sois===10513###eois===10530###lif===2###soif===99###eoif===116###ins===true###function===./app-framework-binder/src/afb-export.c/add_alias_cb(struct afb_api_x3*,const char*,const char*)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(closure);
		if (AKA_mark("lis===397###sois===10568###eois===10601###lif===3###soif===154###eoif===187###ifc===true###function===./app-framework-binder/src/afb-export.c/add_alias_cb(struct afb_api_x3*,const char*,const char*)") && (AKA_mark("lis===397###sois===10568###eois===10601###lif===3###soif===154###eoif===187###isc===true###function===./app-framework-binder/src/afb-export.c/add_alias_cb(struct afb_api_x3*,const char*,const char*)")&&!afb_api_is_valid_name(aliasname))) {
		/* Cant instrument this following code */
ERROR("[API %s] Can't add alias to %s: bad API name", export->api.apiname, aliasname);
				AKA_mark("lis===399###sois===10696###eois===10711###lif===5###soif===282###eoif===297###ins===true###function===./app-framework-binder/src/afb-export.c/add_alias_cb(struct afb_api_x3*,const char*,const char*)");errno = EINVAL;

				AKA_mark("lis===400###sois===10714###eois===10724###lif===6###soif===300###eoif===310###ins===true###function===./app-framework-binder/src/afb-export.c/add_alias_cb(struct afb_api_x3*,const char*,const char*)");return -1;

	}
	else {AKA_mark("lis===-397-###sois===-10568-###eois===-1056833-###lif===-3-###soif===-###eoif===-187-###ins===true###function===./app-framework-binder/src/afb-export.c/add_alias_cb(struct afb_api_x3*,const char*,const char*)");}

	/* Cant instrument this following code */
NOTICE("[API %s] aliasing [API %s] to [API %s]", export->api.apiname, apiname?:"<null>", aliasname);
	/* Cant instrument this following code */
afb_export_add_alias(export, apiname, aliasname);
		AKA_mark("lis===404###sois===10882###eois===10891###lif===10###soif===468###eoif===477###ins===true###function===./app-framework-binder/src/afb-export.c/add_alias_cb(struct afb_api_x3*,const char*,const char*)");return 0;

}

/** Instrumented function api_new_api_cb(struct afb_api_x3*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*) */
static struct afb_api_x3 *api_new_api_cb(
		struct afb_api_x3 *closure,
		const char *api,
		const char *info,
		int noconcurrency,
		int (*preinit)(void*, struct afb_api_x3 *),
		void *preinit_closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/api_new_api_cb(struct afb_api_x3*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*)");AKA_fCall++;
		AKA_mark("lis===415###sois===11101###eois===11118###lif===8###soif===206###eoif===223###ins===true###function===./app-framework-binder/src/afb-export.c/api_new_api_cb(struct afb_api_x3*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(closure);
		AKA_mark("lis===416###sois===11152###eois===11169###lif===9###soif===257###eoif===274###ins===true###function===./app-framework-binder/src/afb-export.c/api_new_api_cb(struct afb_api_x/* Cant instrument this following code */
3*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*)");struct afb_api_v3
 /* Cant instrument this following code */
*apiv3 = afb_api_v3_create(
					export->declare_set, export->call_set,
					api, info, noconcurrency,
					preinit, preinit_closure, 1,
					export, NULL);
		AKA_mark("lis===421###sois===11328###eois===11386###lif===14###soif===433###eoif===491###ins===true###function===./app-framework-binder/src/afb-export.c/api_new_api_cb(struct afb_api_x3*,const char*,const char*,int,int(*preinit)(void*, struct afb_api_x3*),void*)");return apiv3 ? to_api_x3(afb_api_v3_export(apiv3)) : NULL;

}

#if WITH_LEGACY_BINDING_V1 || WITH_LEGACY_BINDING_V2

static void legacy_vverbose_v1_cb(struct afb_api_x3 *closure, int level, const char *file, int line, const char *fmt, va_list args)
{
	vverbose_cb(closure, level, file, line, NULL, fmt, args);
}

static struct afb_event_x1 legacy_event_x1_make_cb(struct afb_api_x3 *closure, const char *name)
{
	struct afb_event_x2 *event = event_x2_make_cb(closure, name);
	return afb_evt_event_from_evtid(afb_evt_event_x2_to_evtid(event));
}

static struct afb_req_x1 legacy_unstore_req_cb(struct afb_api_x3 *closure, struct afb_stored_req *sreq)
{
	return afb_xreq_unstore(sreq);
}

static const struct afb_daemon_itf_x1 daemon_itf = {
	.vverbose_v1 = legacy_vverbose_v1_cb,
	.vverbose_v2 = vverbose_cb,
	.event_make = legacy_event_x1_make_cb,
	.event_broadcast = event_broadcast_cb,
	.get_event_loop = get_event_loop,
	.get_user_bus = get_user_bus,
	.get_system_bus = get_system_bus,
	.rootdir_get_fd = afb_common_rootdir_get_fd,
	.rootdir_open_locale = rootdir_open_locale_cb,
	.queue_job = queue_job_cb,
	.unstore_req = legacy_unstore_req_cb,
	.require_api = require_api_cb,
	.add_alias = add_alias_cb,
	.new_api = api_new_api_cb,
};
#endif

#if WITH_AFB_HOOK
static void hooked_vverbose_cb(struct afb_api_x3 *closure, int level, const char *file, int line, const char *function, const char *fmt, va_list args)
{
	struct afb_export *export = from_api_x3(closure);
	va_list ap;
	va_copy(ap, args);
	vverbose_cb(closure, level, file, line, function, fmt, args);
	afb_hook_api_vverbose(export, level, file, line, function, fmt, ap);
	va_end(ap);
}

static struct afb_event_x2 *hooked_event_x2_make_cb(struct afb_api_x3 *closure, const char *name)
{
	struct afb_export *export = from_api_x3(closure);
	struct afb_event_x2 *r = event_x2_make_cb(closure, name);
	afb_hook_api_event_make(export, name, r);
	return r;
}

static int hooked_event_broadcast_cb(struct afb_api_x3 *closure, const char *name, struct json_object *object)
{
	int r;
	struct afb_export *export = from_api_x3(closure);
	json_object_get(object);
	afb_hook_api_event_broadcast_before(export, name, json_object_get(object));
	r = event_broadcast_cb(closure, name, object);
	afb_hook_api_event_broadcast_after(export, name, object, r);
	json_object_put(object);
	return r;
}

static struct sd_event *hooked_get_event_loop(struct afb_api_x3 *closure)
{
	struct afb_export *export = from_api_x3(closure);
	struct sd_event *r;

	jobs_acquire_event_manager();
	r = get_event_loop(closure);
	return afb_hook_api_get_event_loop(export, r);
}

static struct sd_bus *hooked_get_user_bus(struct afb_api_x3 *closure)
{
	struct afb_export *export = from_api_x3(closure);
	struct sd_bus *r;

	jobs_acquire_event_manager();
	r = get_user_bus(closure);
	return afb_hook_api_get_user_bus(export, r);
}

static struct sd_bus *hooked_get_system_bus(struct afb_api_x3 *closure)
{
	struct afb_export *export = from_api_x3(closure);
	struct sd_bus *r;

	jobs_acquire_event_manager();
	r = get_system_bus(closure);
	return afb_hook_api_get_system_bus(export, r);
}

static int hooked_rootdir_get_fd(struct afb_api_x3 *closure)
{
	struct afb_export *export = from_api_x3(closure);
	int r = afb_common_rootdir_get_fd();
	return afb_hook_api_rootdir_get_fd(export, r);
}

static int hooked_rootdir_open_locale_cb(struct afb_api_x3 *closure, const char *filename, int flags, const char *locale)
{
	struct afb_export *export = from_api_x3(closure);
	int r = rootdir_open_locale_cb(closure, filename, flags, locale);
	return afb_hook_api_rootdir_open_locale(export, filename, flags, locale, r);
}

static int hooked_queue_job_cb(struct afb_api_x3 *closure, void (*callback)(int signum, void *arg), void *argument, void *group, int timeout)
{
	struct afb_export *export = from_api_x3(closure);
	int r = queue_job_cb(closure, callback, argument, group, timeout);
	return afb_hook_api_queue_job(export, callback, argument, group, timeout, r);
}

static int hooked_require_api_cb(struct afb_api_x3 *closure, const char *name, int initialized)
{
	int result;
	struct afb_export *export = from_api_x3(closure);
	afb_hook_api_require_api(export, name, initialized);
	result = require_api_cb(closure, name, initialized);
	return afb_hook_api_require_api_result(export, name, initialized, result);
}

static int hooked_add_alias_cb(struct afb_api_x3 *closure, const char *apiname, const char *aliasname)
{
	struct afb_export *export = from_api_x3(closure);
	int result = add_alias_cb(closure, apiname, aliasname);
	return afb_hook_api_add_alias(export, apiname, aliasname, result);
}

static struct afb_api_x3 *hooked_api_new_api_cb(
		struct afb_api_x3 *closure,
		const char *api,
		const char *info,
		int noconcurrency,
		int (*preinit)(void*, struct afb_api_x3 *),
		void *preinit_closure)
{
	struct afb_api_x3 *result;
	struct afb_export *export = from_api_x3(closure);
	afb_hook_api_new_api_before(export, api, info, noconcurrency);
	result = api_new_api_cb(closure, api, info, noconcurrency, preinit, preinit_closure);
	afb_hook_api_new_api_after(export, -!result, api);
	return result;
}

#if WITH_LEGACY_BINDING_V1 || WITH_LEGACY_BINDING_V2

static void legacy_hooked_vverbose_v1_cb(struct afb_api_x3 *closure, int level, const char *file, int line, const char *fmt, va_list args)
{
	hooked_vverbose_cb(closure, level, file, line, NULL, fmt, args);
}

static struct afb_event_x1 legacy_hooked_event_x1_make_cb(struct afb_api_x3 *closure, const char *name)
{
	struct afb_event_x2 *event = hooked_event_x2_make_cb(closure, name);
	struct afb_event_x1 e;
	e.closure = event;
	e.itf = event ? event->itf : NULL;
	return e;
}

static struct afb_req_x1 legacy_hooked_unstore_req_cb(struct afb_api_x3 *closure, struct afb_stored_req *sreq)
{
	struct afb_export *export = from_api_x3(closure);
	afb_hook_api_legacy_unstore_req(export, sreq);
	return legacy_unstore_req_cb(closure, sreq);
}

static const struct afb_daemon_itf_x1 hooked_daemon_itf = {
	.vverbose_v1 = legacy_hooked_vverbose_v1_cb,
	.vverbose_v2 = hooked_vverbose_cb,
	.event_make = legacy_hooked_event_x1_make_cb,
	.event_broadcast = hooked_event_broadcast_cb,
	.get_event_loop = hooked_get_event_loop,
	.get_user_bus = hooked_get_user_bus,
	.get_system_bus = hooked_get_system_bus,
	.rootdir_get_fd = hooked_rootdir_get_fd,
	.rootdir_open_locale = hooked_rootdir_open_locale_cb,
	.queue_job = hooked_queue_job_cb,
	.unstore_req = legacy_hooked_unstore_req_cb,
	.require_api = hooked_require_api_cb,
	.add_alias = hooked_add_alias_cb,
	.new_api = hooked_api_new_api_cb,
};
#endif

#endif

/******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
                                           F R O M     S V C
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************/

/* the common session for services sharing their session */
static struct afb_session *common_session;

/******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
                                           F R O M     S V C
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************/

/** Instrumented function call_x3(struct afb_api_x3*,const char*,const char*,struct json_object*,void(*callback)(void*, struct json_object*, const char*error, const char*info, struct afb_api_x3*),void*) */
static void call_x3(
		struct afb_api_x3 *apix3,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, struct json_object*, const char *error, const char *info, struct afb_api_x3*),
		void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/call_x3(struct afb_api_x3*,const char*,const char*,struct json_object*,void(*callback)(void*, struct json_object*, const char*error, const char*info, struct afb_api_x3*),void*)");AKA_fCall++;
		AKA_mark("lis===648###sois===19668###eois===19685###lif===8###soif===241###eoif===258###ins===true###function===./app-framework-binder/src/afb-export.c/call_x3(struct afb_api_x3*,const char*,const char*,struct json_object*,void(*callback)(void*, struct json_object*, const char*error, const char*info, struct afb_api_x3*),void*)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(apix3);
	/* Cant instrument this following code */
return afb_calls_call(export, api, verb, args, callback, closure);
}

/** Instrumented function call_sync_x3(struct afb_api_x3*,const char*,const char*,struct json_object*,struct json_object**,char**,char**) */
static int call_sync_x3(
		struct afb_api_x3 *apix3,
		const char *api,
		const char *verb,
		struct json_object *args,
		struct json_object **object,
		char **error,
		char **info)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/call_sync_x3(struct afb_api_x3*,const char*,const char*,struct json_object*,struct json_object**,char**,char**)");AKA_fCall++;
		AKA_mark("lis===661###sois===19972###eois===19989###lif===9###soif===185###eoif===202###ins===true###function===./app-framework-binder/src/afb-export.c/call_sync_x3(struct afb_api_x3*,const char*,const char*,struct json_object*,struct json_object**,char**,char**)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(apix3);
	/* Cant instrument this following code */
return afb_calls_call_sync(export, api, verb, args, object, error, info);
}

/** Instrumented function legacy_call_x3(struct afb_api_x3*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_api_x3*),void*) */
static void legacy_call_x3(
		struct afb_api_x3 *apix3,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, int, struct json_object*, struct afb_api_x3*),
		void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/legacy_call_x3(struct afb_api_x3*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_api_x3*),void*)");AKA_fCall++;
		AKA_mark("lis===673###sois===20314###eois===20331###lif===8###soif===216###eoif===233###ins===true###function===./app-framework-binder/src/afb-export.c/legacy_call_x3(struct afb_api_x3*,const char*,const char*,struct json_object*,void(*callback)(void*, int, struct json_object*, struct afb_api_x3*),void*)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(apix3);
	/* Cant instrument this following code */
afb_calls_legacy_call_v3(export, api, verb, args, callback, closure);
}

/** Instrumented function legacy_call_sync(struct afb_api_x3*,const char*,const char*,struct json_object*,struct json_object**) */
static int legacy_call_sync(
		struct afb_api_x3 *apix3,
		const char *api,
		const char *verb,
		struct json_object *args,
		struct json_object **result)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/legacy_call_sync(struct afb_api_x3*,const char*,const char*,struct json_object*,struct json_object**)");AKA_fCall++;
		AKA_mark("lis===684###sois===20594###eois===20611###lif===7###soif===158###eoif===175###ins===true###function===./app-framework-binder/src/afb-export.c/legacy_call_sync(struct afb_api_x3*,const char*,const char*,struct json_object*,struct json_object**)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(apix3);
	/* Cant instrument this following code */
return afb_calls_legacy_call_sync(export, api, verb, args, result);
}

#if WITH_LEGACY_BINDING_V1 || WITH_LEGACY_BINDING_V2

static void legacy_call_v12(
		struct afb_api_x3 *apix3,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, int, struct json_object*),
		void *closure)
{
	struct afb_export *export = from_api_x3(apix3);
	afb_calls_legacy_call_v12(export, api, verb, args, callback, closure);
}

/* the interface for services */
static const struct afb_service_itf_x1 service_itf = {
	.call = legacy_call_v12,
	.call_sync = legacy_call_sync
};
#endif

#if WITH_AFB_HOOK
static void hooked_call_x3(
		struct afb_api_x3 *apix3,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, struct json_object*, const char*, const char*, struct afb_api_x3*),
		void *closure)
{
	struct afb_export *export = from_api_x3(apix3);
	afb_calls_hooked_call(export, api, verb, args, callback, closure);
}

static int hooked_call_sync_x3(
		struct afb_api_x3 *apix3,
		const char *api,
		const char *verb,
		struct json_object *args,
		struct json_object **object,
		char **error,
		char **info)
{
	struct afb_export *export = from_api_x3(apix3);
	return afb_calls_hooked_call_sync(export, api, verb, args, object, error, info);
}

static void legacy_hooked_call_x3(
		struct afb_api_x3 *apix3,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, int, struct json_object*, struct afb_api_x3*),
		void *closure)
{
	struct afb_export *export = from_api_x3(apix3);
	afb_calls_legacy_hooked_call_v3(export, api, verb, args, callback, closure);
}

static int legacy_hooked_call_sync(
		struct afb_api_x3 *apix3,
		const char *api,
		const char *verb,
		struct json_object *args,
		struct json_object **result)
{
	struct afb_export *export = from_api_x3(apix3);
	return afb_calls_legacy_hooked_call_sync(export, api, verb, args, result);
}

#if WITH_LEGACY_BINDING_V1 || WITH_LEGACY_BINDING_V2

static void legacy_hooked_call_v12(
		struct afb_api_x3 *apix3,
		const char *api,
		const char *verb,
		struct json_object *args,
		void (*callback)(void*, int, struct json_object*),
		void *closure)
{
	struct afb_export *export = from_api_x3(apix3);
	afb_calls_legacy_hooked_call_v12(export, api, verb, args, callback, closure);
}

/* the interface for services */
static const struct afb_service_itf_x1 hooked_service_itf = {
	.call = legacy_hooked_call_v12,
	.call_sync = legacy_hooked_call_sync
};
#endif

#endif

/******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
                                           F R O M     D Y N A P I
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************/

/** Instrumented function api_set_verbs_v2_cb(struct afb_api_x3*,const struct afb_verb_v2*) */
static int api_set_verbs_v2_cb(
		struct afb_api_x3 *api,
		const struct afb_verb_v2 *verbs)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/api_set_verbs_v2_cb(struct afb_api_x3*,const struct afb_verb_v2*)");AKA_fCall++;
#if WITH_LEGACY_BINDING_V2
	struct afb_export *export = from_api_x3(api);

	if (export->unsealed) {
		afb_api_v3_set_verbs_v2(export->desc.v3, verbs);
		return 0;
	}

	errno = EPERM;
#else
		AKA_mark("lis===805###sois===24154###eois===24172###lif===14###soif===285###eoif===303###ins===true###function===./app-framework-binder/src/afb-export.c/api_set_verbs_v2_cb(struct afb_api_x3*,const struct afb_verb_v2*)");errno = ECANCELED;

#endif
		AKA_mark("lis===807###sois===24181###eois===24191###lif===16###soif===312###eoif===322###ins===true###function===./app-framework-binder/src/afb-export.c/api_set_verbs_v2_cb(struct afb_api_x3*,const struct afb_verb_v2*)");return -1;

}

/** Instrumented function api_set_verbs_v3_cb(struct afb_api_x3*,const struct afb_verb_v3*) */
static int api_set_verbs_v3_cb(
		struct afb_api_x3 *api,
		const struct afb_verb_v3 *verbs)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/api_set_verbs_v3_cb(struct afb_api_x3*,const struct afb_verb_v3*)");AKA_fCall++;
		AKA_mark("lis===814###sois===24291###eois===24308###lif===4###soif===96###eoif===113###ins===true###function===./app-framework-binder/src/afb-export.c/api_set_verbs_v3_cb(struct afb_api_x3*,const struct afb_verb_v3*)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(api);

		if (AKA_mark("lis===816###sois===24343###eois===24360###lif===6###soif===148###eoif===165###ifc===true###function===./app-framework-binder/src/afb-export.c/api_set_verbs_v3_cb(struct afb_api_x3*,const struct afb_verb_v3*)") && (AKA_mark("lis===816###sois===24343###eois===24360###lif===6###soif===148###eoif===165###isc===true###function===./app-framework-binder/src/afb-export.c/api_set_verbs_v3_cb(struct afb_api_x3*,const struct afb_verb_v3*)")&&!export->unsealed)) {
				AKA_mark("lis===817###sois===24366###eois===24380###lif===7###soif===171###eoif===185###ins===true###function===./app-framework-binder/src/afb-export.c/api_set_verbs_v3_cb(struct afb_api_x3*,const struct afb_verb_v3*)");errno = EPERM;

				AKA_mark("lis===818###sois===24383###eois===24393###lif===8###soif===188###eoif===198###ins===true###function===./app-framework-binder/src/afb-export.c/api_set_verbs_v3_cb(struct afb_api_x3*,const struct afb_verb_v3*)");return -1;

	}
	else {AKA_mark("lis===-816-###sois===-24343-###eois===-2434317-###lif===-6-###soif===-###eoif===-165-###ins===true###function===./app-framework-binder/src/afb-export.c/api_set_verbs_v3_cb(struct afb_api_x3*,const struct afb_verb_v3*)");}


	/* Cant instrument this following code */
afb_api_v3_set_verbs_v3(export->desc.v3, verbs);
		AKA_mark("lis===822###sois===24449###eois===24458###lif===12###soif===254###eoif===263###ins===true###function===./app-framework-binder/src/afb-export.c/api_set_verbs_v3_cb(struct afb_api_x3*,const struct afb_verb_v3*)");return 0;

}

/** Instrumented function api_add_verb_cb(struct afb_api_x3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint32_t,int) */
static int api_add_verb_cb(
		struct afb_api_x3 *api,
		const char *verb,
		const char *info,
		void (*callback)(struct afb_req_x2 *req),
		void *vcbdata,
		const struct afb_auth *auth,
		uint32_t session,
		int glob)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/api_add_verb_cb(struct afb_api_x3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint32_t,int)");AKA_fCall++;
		AKA_mark("lis===835###sois===24683###eois===24700###lif===10###soif===221###eoif===238###ins===true###function===./app-framework-binder/src/afb-export.c/api_add_verb_cb(struct afb_api_x3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint32_t,int)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(api);

		if (AKA_mark("lis===837###sois===24735###eois===24752###lif===12###soif===273###eoif===290###ifc===true###function===./app-framework-binder/src/afb-export.c/api_add_verb_cb(struct afb_api_x3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint32_t,int)") && (AKA_mark("lis===837###sois===24735###eois===24752###lif===12###soif===273###eoif===290###isc===true###function===./app-framework-binder/src/afb-export.c/api_add_verb_cb(struct afb_api_x3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint32_t,int)")&&!export->unsealed)) {
				AKA_mark("lis===838###sois===24758###eois===24772###lif===13###soif===296###eoif===310###ins===true###function===./app-framework-binder/src/afb-export.c/api_add_verb_cb(struct afb_api_x3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint32_t,int)");errno = EPERM;

				AKA_mark("lis===839###sois===24775###eois===24785###lif===14###soif===313###eoif===323###ins===true###function===./app-framework-binder/src/afb-export.c/api_add_verb_cb(struct afb_api_x3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint32_t,int)");return -1;

	}
	else {AKA_mark("lis===-837-###sois===-24735-###eois===-2473517-###lif===-12-###soif===-###eoif===-290-###ins===true###function===./app-framework-binder/src/afb-export.c/api_add_verb_cb(struct afb_api_x3*,const char*,const char*,void(*callback)(struct afb_req_x2*req),void*,const struct afb_auth*,uint32_t,int)");}


	/* Cant instrument this following code */
return afb_api_v3_add_verb(export->desc.v3, verb, info, callback, vcbdata, auth, (uint16_t)session, glob);
}

/** Instrumented function api_del_verb_cb(struct afb_api_x3*,const char*,void**) */
static int api_del_verb_cb(
		struct afb_api_x3 *api,
		const char *verb,
		void **vcbdata)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/api_del_verb_cb(struct afb_api_x3*,const char*,void**)");AKA_fCall++;
		AKA_mark("lis===850###sois===24996###eois===25013###lif===5###soif===95###eoif===112###ins===true###function===./app-framework-binder/src/afb-export.c/api_del_verb_cb(struct afb_api_x3*,const char*,void**)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(api);

		if (AKA_mark("lis===852###sois===25048###eois===25065###lif===7###soif===147###eoif===164###ifc===true###function===./app-framework-binder/src/afb-export.c/api_del_verb_cb(struct afb_api_x3*,const char*,void**)") && (AKA_mark("lis===852###sois===25048###eois===25065###lif===7###soif===147###eoif===164###isc===true###function===./app-framework-binder/src/afb-export.c/api_del_verb_cb(struct afb_api_x3*,const char*,void**)")&&!export->unsealed)) {
				AKA_mark("lis===853###sois===25071###eois===25085###lif===8###soif===170###eoif===184###ins===true###function===./app-framework-binder/src/afb-export.c/api_del_verb_cb(struct afb_api_x3*,const char*,void**)");errno = EPERM;

				AKA_mark("lis===854###sois===25088###eois===25098###lif===9###soif===187###eoif===197###ins===true###function===./app-framework-binder/src/afb-export.c/api_del_verb_cb(struct afb_api_x3*,const char*,void**)");return -1;

	}
	else {AKA_mark("lis===-852-###sois===-25048-###eois===-2504817-###lif===-7-###soif===-###eoif===-164-###ins===true###function===./app-framework-binder/src/afb-export.c/api_del_verb_cb(struct afb_api_x3*,const char*,void**)");}


	/* Cant instrument this following code */
return afb_api_v3_del_verb(export->desc.v3, verb, vcbdata);
}

/** Instrumented function api_set_on_event_cb(struct afb_api_x3*,void(*onevent)(struct afb_api_x3*api, const char*event, struct json_object*object)) */
static int api_set_on_event_cb(
		struct afb_api_x3 *api,
		void (*onevent)(struct afb_api_x3 *api, const char *event, struct json_object *object))
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/api_set_on_event_cb(struct afb_api_x3*,void(*onevent)(struct afb_api_x3*api, const char*event, struct json_object*object))");AKA_fCall++;
		AKA_mark("lis===864###sois===25318###eois===25335###lif===4###soif===151###eoif===168###ins===true###function===./app-framework-binder/src/afb-export.c/api_set_on_event_cb(struct afb_api_x3*,void(*onevent)(struct afb_api_x3*api, const char*event, struct json_object*object))");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(api);
	/* Cant instrument this following code */
return afb_export_handle_events_v3(export, onevent);
}

/** Instrumented function api_set_on_init_cb(struct afb_api_x3*,int(*oninit)(struct afb_api_x3*api)) */
static int api_set_on_init_cb(
		struct afb_api_x3 *api,
		int (*oninit)(struct afb_api_x3 *api))
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/api_set_on_init_cb(struct afb_api_x3*,int(*oninit)(struct afb_api_x3*api))");AKA_fCall++;
		AKA_mark("lis===872###sois===25522###eois===25539###lif===4###soif===101###eoif===118###ins===true###function===./app-framework-binder/src/afb-export.c/api_set_on_init_cb(struct afb_api_x3*,int(*oninit)(struct afb_api_x3*api))");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(api);

	/* Cant instrument this following code */
return afb_export_handle_init_v3(export, oninit);
}

/** Instrumented function api_seal_cb(struct afb_api_x3*) */
static void api_seal_cb(
		struct afb_api_x3 *api)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/api_seal_cb(struct afb_api_x3*)");AKA_fCall++;
		AKA_mark("lis===880###sois===25677###eois===25694###lif===3###soif===54###eoif===71###ins===true###function===./app-framework-binder/src/afb-export.c/api_seal_cb(struct afb_api_x3*)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(api);

	/* Cant instrument this following code */
export->unsealed = 0;
}

/** Instrumented function event_handler_add_cb(struct afb_api_x3*,const char*,void(*callback)(void*, const char*, struct json_object*, struct afb_api_x3*),void*) */
static int event_handler_add_cb(
		struct afb_api_x3 *api,
		const char *pattern,
		void (*callback)(void *, const char*, struct json_object*, struct afb_api_x3*),
		void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/event_handler_add_cb(struct afb_api_x3*,const char*,void(*callback)(void*, const char*, struct json_object*, struct afb_api_x3*),void*)");AKA_fCall++;
		AKA_mark("lis===891###sois===25934###eois===25951###lif===6###soif===184###eoif===201###ins===true###function===./app-framework-binder/src/afb-export.c/event_handler_add_cb(struct afb_api_x3*,const char*,void(*callback)(void*, const char*, struct json_object*, struct afb_api_x3*),void*)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(api);

	/* Cant instrument this following code */
return afb_export_event_handler_add(export, pattern, callback, closure);
}

/** Instrumented function event_handler_del_cb(struct afb_api_x3*,const char*,void**) */
static int event_handler_del_cb(
		struct afb_api_x3 *api,
		const char *pattern,
		void **closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/event_handler_del_cb(struct afb_api_x3*,const char*,void**)");AKA_fCall++;
		AKA_mark("lis===901###sois===26161###eois===26178###lif===5###soif===103###eoif===120###ins===true###function===./app-framework-binder/src/afb-export.c/event_handler_del_cb(struct afb_api_x3*,const char*,void**)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(api);

	/* Cant instrument this following code */
return afb_export_event_handler_del(export, pattern, closure);
}

/** Instrumented function class_provide_cb(struct afb_api_x3*,const char*) */
static int class_provide_cb(struct afb_api_x3 *api, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");AKA_fCall++;
		AKA_mark("lis===908###sois===26348###eois===26365###lif===2###soif===73###eoif===90###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(api);

		AKA_mark("lis===910###sois===26396###eois===26412###lif===4###soif===121###eoif===137###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");int rc = 0, rc2;

		AKA_mark("lis===911###sois===26414###eois===26437###lif===5###soif===139###eoif===162###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");char *iter, *end, save;


		AKA_mark("lis===913###sois===26440###eois===26461###lif===7###soif===165###eoif===186###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");iter = strdupa(name);

		for (;;) {
		/* skip any space */
				AKA_mark("lis===916###sois===26498###eois===26511###lif===10###soif===223###eoif===236###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");save = *iter;

				while (AKA_mark("lis===917###sois===26520###eois===26533###lif===11###soif===245###eoif===258###ifc===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)") && (AKA_mark("lis===917###sois===26520###eois===26533###lif===11###soif===245###eoif===258###isc===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)")&&isspace(save))) {
			AKA_mark("lis===918###sois===26538###eois===26553###lif===12###soif===263###eoif===278###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");save = *++iter;
		}

				if (AKA_mark("lis===919###sois===26560###eois===26565###lif===13###soif===285###eoif===290###ifc===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)") && (AKA_mark("lis===919###sois===26560###eois===26565###lif===13###soif===285###eoif===290###isc===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)")&&!save)) {
			AKA_mark("lis===920###sois===26584###eois===26594###lif===14###soif===309###eoif===319###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");return rc;
		}
		else {AKA_mark("lis===-919-###sois===-26560-###eois===-265605-###lif===-13-###soif===-###eoif===-290-###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");}


		/* search for the end */
				AKA_mark("lis===923###sois===26625###eois===26636###lif===17###soif===350###eoif===361###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");end = iter;

				while (AKA_mark("lis===924###sois===26646###eois===26668###lif===18###soif===371###eoif===393###ifc===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)") && ((AKA_mark("lis===924###sois===26646###eois===26650###lif===18###soif===371###eoif===375###isc===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)")&&save)	&&(AKA_mark("lis===924###sois===26654###eois===26668###lif===18###soif===379###eoif===393###isc===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)")&&!isspace(save)))) {
			AKA_mark("lis===925###sois===26673###eois===26687###lif===19###soif===398###eoif===412###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");save = *++end;
		}

				AKA_mark("lis===926###sois===26690###eois===26699###lif===20###soif===415###eoif===424###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");*end = 0;


		/* Cant instrument this following code */
rc2 = afb_apiset_provide_class(export->declare_set, api->apiname, iter);
				if (AKA_mark("lis===929###sois===26782###eois===26789###lif===23###soif===507###eoif===514###ifc===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)") && (AKA_mark("lis===929###sois===26782###eois===26789###lif===23###soif===507###eoif===514###isc===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)")&&rc2 < 0)) {
			AKA_mark("lis===930###sois===26794###eois===26803###lif===24###soif===519###eoif===528###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");rc = rc2;
		}
		else {AKA_mark("lis===-929-###sois===-26782-###eois===-267827-###lif===-23-###soif===-###eoif===-514-###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");}


				AKA_mark("lis===932###sois===26807###eois===26819###lif===26###soif===532###eoif===544###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");*end = save;

				AKA_mark("lis===933###sois===26822###eois===26833###lif===27###soif===547###eoif===558###ins===true###function===./app-framework-binder/src/afb-export.c/class_provide_cb(struct afb_api_x3*,const char*)");iter = end;

	}

}

/** Instrumented function class_require_cb(struct afb_api_x3*,const char*) */
static int class_require_cb(struct afb_api_x3 *api, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");AKA_fCall++;
		AKA_mark("lis===939###sois===26913###eois===26930###lif===2###soif===73###eoif===90###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(api);

		AKA_mark("lis===941###sois===26961###eois===26977###lif===4###soif===121###eoif===137###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");int rc = 0, rc2;

		AKA_mark("lis===942###sois===26979###eois===27002###lif===5###soif===139###eoif===162###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");char *iter, *end, save;


		AKA_mark("lis===944###sois===27005###eois===27026###lif===7###soif===165###eoif===186###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");iter = strdupa(name);

		for (;;) {
		/* skip any space */
				AKA_mark("lis===947###sois===27063###eois===27076###lif===10###soif===223###eoif===236###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");save = *iter;

				while (AKA_mark("lis===948###sois===27085###eois===27098###lif===11###soif===245###eoif===258###ifc===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)") && (AKA_mark("lis===948###sois===27085###eois===27098###lif===11###soif===245###eoif===258###isc===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)")&&isspace(save))) {
			AKA_mark("lis===949###sois===27103###eois===27118###lif===12###soif===263###eoif===278###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");save = *++iter;
		}

				if (AKA_mark("lis===950###sois===27125###eois===27130###lif===13###soif===285###eoif===290###ifc===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)") && (AKA_mark("lis===950###sois===27125###eois===27130###lif===13###soif===285###eoif===290###isc===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)")&&!save)) {
			AKA_mark("lis===951###sois===27149###eois===27159###lif===14###soif===309###eoif===319###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");return rc;
		}
		else {AKA_mark("lis===-950-###sois===-27125-###eois===-271255-###lif===-13-###soif===-###eoif===-290-###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");}


		/* search for the end */
				AKA_mark("lis===954###sois===27190###eois===27201###lif===17###soif===350###eoif===361###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");end = iter;

				while (AKA_mark("lis===955###sois===27211###eois===27233###lif===18###soif===371###eoif===393###ifc===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)") && ((AKA_mark("lis===955###sois===27211###eois===27215###lif===18###soif===371###eoif===375###isc===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)")&&save)	&&(AKA_mark("lis===955###sois===27219###eois===27233###lif===18###soif===379###eoif===393###isc===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)")&&!isspace(save)))) {
			AKA_mark("lis===956###sois===27238###eois===27252###lif===19###soif===398###eoif===412###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");save = *++end;
		}

				AKA_mark("lis===957###sois===27255###eois===27264###lif===20###soif===415###eoif===424###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");*end = 0;


		/* Cant instrument this following code */
rc2 = afb_apiset_require_class(export->declare_set, api->apiname, iter);
				if (AKA_mark("lis===960###sois===27347###eois===27354###lif===23###soif===507###eoif===514###ifc===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)") && (AKA_mark("lis===960###sois===27347###eois===27354###lif===23###soif===507###eoif===514###isc===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)")&&rc2 < 0)) {
			AKA_mark("lis===961###sois===27359###eois===27368###lif===24###soif===519###eoif===528###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");rc = rc2;
		}
		else {AKA_mark("lis===-960-###sois===-27347-###eois===-273477-###lif===-23-###soif===-###eoif===-514-###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");}


				AKA_mark("lis===963###sois===27372###eois===27384###lif===26###soif===532###eoif===544###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");*end = save;

				AKA_mark("lis===964###sois===27387###eois===27398###lif===27###soif===547###eoif===558###ins===true###function===./app-framework-binder/src/afb-export.c/class_require_cb(struct afb_api_x3*,const char*)");iter = end;

	}

}

/** Instrumented function delete_api_cb(struct afb_api_x3*) */
static int delete_api_cb(struct afb_api_x3 *api)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/delete_api_cb(struct afb_api_x3*)");AKA_fCall++;
		AKA_mark("lis===970###sois===27457###eois===27474###lif===2###soif===52###eoif===69###ins===true###function===./app-framework-binder/src/afb-export.c/delete_api_cb(struct afb_api_x3*)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(api);

		if (AKA_mark("lis===972###sois===27509###eois===27526###lif===4###soif===104###eoif===121###ifc===true###function===./app-framework-binder/src/afb-export.c/delete_api_cb(struct afb_api_x3*)") && (AKA_mark("lis===972###sois===27509###eois===27526###lif===4###soif===104###eoif===121###isc===true###function===./app-framework-binder/src/afb-export.c/delete_api_cb(struct afb_api_x3*)")&&!export->unsealed)) {
				AKA_mark("lis===973###sois===27532###eois===27546###lif===5###soif===127###eoif===141###ins===true###function===./app-framework-binder/src/afb-export.c/delete_api_cb(struct afb_api_x3*)");errno = EPERM;

				AKA_mark("lis===974###sois===27549###eois===27559###lif===6###soif===144###eoif===154###ins===true###function===./app-framework-binder/src/afb-export.c/delete_api_cb(struct afb_api_x3*)");return -1;

	}
	else {AKA_mark("lis===-972-###sois===-27509-###eois===-2750917-###lif===-4-###soif===-###eoif===-121-###ins===true###function===./app-framework-binder/src/afb-export.c/delete_api_cb(struct afb_api_x3*)");}


	/* Cant instrument this following code */
afb_export_undeclare(export);
	/* Cant instrument this following code */
afb_export_unref(export);
		AKA_mark("lis===979###sois===27623###eois===27632###lif===11###soif===218###eoif===227###ins===true###function===./app-framework-binder/src/afb-export.c/delete_api_cb(struct afb_api_x3*)");return 0;

}

/** Instrumented function settings_cb(struct afb_api_x3*) */
static struct json_object *settings_cb(struct afb_api_x3 *api)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/settings_cb(struct afb_api_x3*)");AKA_fCall++;
		AKA_mark("lis===984###sois===27702###eois===27719###lif===2###soif===66###eoif===83###ins===true###function===./app-framework-binder/src/afb-export.c/settings_cb(struct afb_api_x3*)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(api);
		AKA_mark("lis===985###sois===27749###eois===27767###lif===3###soif===113###eoif===131###ins===true###function===./app-framework-binder/src/afb-export.c/settings_cb(struct afb_api_x3*)");struct json_object
 /* Cant instrument this following code */
*result = export->settings;
	/* Cant instrument this following code */
if (!result)
		result = make_settings(export);
		AKA_mark("lis===988###sois===27845###eois===27859###lif===6###soif===209###eoif===223###ins===true###function===./app-framework-binder/src/afb-export.c/settings_cb(struct afb_api_x3*)");return result;

}

static const struct afb_api_x3_itf api_x3_itf = {

	.vverbose = (void*)vverbose_cb,

	.get_event_loop = get_event_loop,
	.get_user_bus = get_user_bus,
	.get_system_bus = get_system_bus,
	.rootdir_get_fd = afb_common_rootdir_get_fd,
	.rootdir_open_locale = rootdir_open_locale_cb,
	.queue_job = queue_job_cb,

	.require_api = require_api_cb,
	.add_alias = add_alias_cb,

	.event_broadcast = event_broadcast_cb,
	.event_make = event_x2_make_cb,

	.legacy_call = legacy_call_x3,
	.legacy_call_sync = legacy_call_sync,

	.api_new_api = api_new_api_cb,
	.api_set_verbs_v2 = api_set_verbs_v2_cb,
	.api_add_verb = api_add_verb_cb,
	.api_del_verb = api_del_verb_cb,
	.api_set_on_event = api_set_on_event_cb,
	.api_set_on_init = api_set_on_init_cb,
	.api_seal = api_seal_cb,
	.api_set_verbs_v3 = api_set_verbs_v3_cb,
	.event_handler_add = event_handler_add_cb,
	.event_handler_del = event_handler_del_cb,

	.call = call_x3,
	.call_sync = call_sync_x3,

	.class_provide = class_provide_cb,
	.class_require = class_require_cb,

	.delete_api = delete_api_cb,
	.settings = settings_cb,
};

#if WITH_AFB_HOOK
static int hooked_api_set_verbs_v2_cb(
		struct afb_api_x3 *api,
		const struct afb_verb_v2 *verbs)
{
	struct afb_export *export = from_api_x3(api);
	int result = api_set_verbs_v2_cb(api, verbs);
	return afb_hook_api_api_set_verbs_v2(export, result, verbs);
}

static int hooked_api_set_verbs_v3_cb(
		struct afb_api_x3 *api,
		const struct afb_verb_v3 *verbs)
{
	struct afb_export *export = from_api_x3(api);
	int result = api_set_verbs_v3_cb(api, verbs);
	return afb_hook_api_api_set_verbs_v3(export, result, verbs);
}

static int hooked_api_add_verb_cb(
		struct afb_api_x3 *api,
		const char *verb,
		const char *info,
		void (*callback)(struct afb_req_x2 *req),
		void *vcbdata,
		const struct afb_auth *auth,
		uint32_t session,
		int glob)
{
	struct afb_export *export = from_api_x3(api);
	int result = api_add_verb_cb(api, verb, info, callback, vcbdata, auth, session, glob);
	return afb_hook_api_api_add_verb(export, result, verb, info, glob);
}

static int hooked_api_del_verb_cb(
		struct afb_api_x3 *api,
		const char *verb,
		void **vcbdata)
{
	struct afb_export *export = from_api_x3(api);
	int result = api_del_verb_cb(api, verb, vcbdata);
	return afb_hook_api_api_del_verb(export, result, verb);
}

static int hooked_api_set_on_event_cb(
		struct afb_api_x3 *api,
		void (*onevent)(struct afb_api_x3 *api, const char *event, struct json_object *object))
{
	struct afb_export *export = from_api_x3(api);
	int result = api_set_on_event_cb(api, onevent);
	return afb_hook_api_api_set_on_event(export, result);
}

static int hooked_api_set_on_init_cb(
		struct afb_api_x3 *api,
		int (*oninit)(struct afb_api_x3 *api))
{
	struct afb_export *export = from_api_x3(api);
	int result = api_set_on_init_cb(api, oninit);
	return afb_hook_api_api_set_on_init(export, result);
}

static void hooked_api_seal_cb(
		struct afb_api_x3 *api)
{
	struct afb_export *export = from_api_x3(api);
	afb_hook_api_api_seal(export);
	api_seal_cb(api);
}

static int hooked_event_handler_add_cb(
		struct afb_api_x3 *api,
		const char *pattern,
		void (*callback)(void *, const char*, struct json_object*, struct afb_api_x3*),
		void *closure)
{
	struct afb_export *export = from_api_x3(api);
	int result = event_handler_add_cb(api, pattern, callback, closure);
	return afb_hook_api_event_handler_add(export, result, pattern);
}

static int hooked_event_handler_del_cb(
		struct afb_api_x3 *api,
		const char *pattern,
		void **closure)
{
	struct afb_export *export = from_api_x3(api);
	int result = event_handler_del_cb(api, pattern, closure);
	return afb_hook_api_event_handler_del(export, result, pattern);
}

static int hooked_class_provide_cb(struct afb_api_x3 *api, const char *name)
{
	struct afb_export *export = from_api_x3(api);
	int result = class_provide_cb(api, name);
	return afb_hook_api_class_provide(export, result, name);
}

static int hooked_class_require_cb(struct afb_api_x3 *api, const char *name)
{
	struct afb_export *export = from_api_x3(api);
	int result = class_require_cb(api, name);
	return afb_hook_api_class_require(export, result, name);
}

static int hooked_delete_api_cb(struct afb_api_x3 *api)
{
	struct afb_export *export = afb_export_addref(from_api_x3(api));
	int result = delete_api_cb(api);
	result = afb_hook_api_delete_api(export, result);
	afb_export_unref(export);
	return result;
}

static struct json_object *hooked_settings_cb(struct afb_api_x3 *api)
{
	struct afb_export *export = from_api_x3(api);
	struct json_object *result = settings_cb(api);
	result = afb_hook_api_settings(export, result);
	return result;
}

static const struct afb_api_x3_itf hooked_api_x3_itf = {

	.vverbose = hooked_vverbose_cb,

	.get_event_loop = hooked_get_event_loop,
	.get_user_bus = hooked_get_user_bus,
	.get_system_bus = hooked_get_system_bus,
	.rootdir_get_fd = hooked_rootdir_get_fd,
	.rootdir_open_locale = hooked_rootdir_open_locale_cb,
	.queue_job = hooked_queue_job_cb,

	.require_api = hooked_require_api_cb,
	.add_alias = hooked_add_alias_cb,

	.event_broadcast = hooked_event_broadcast_cb,
	.event_make = hooked_event_x2_make_cb,

	.legacy_call = legacy_hooked_call_x3,
	.legacy_call_sync = legacy_hooked_call_sync,

	.api_new_api = hooked_api_new_api_cb,
	.api_set_verbs_v2 = hooked_api_set_verbs_v2_cb,
	.api_add_verb = hooked_api_add_verb_cb,
	.api_del_verb = hooked_api_del_verb_cb,
	.api_set_on_event = hooked_api_set_on_event_cb,
	.api_set_on_init = hooked_api_set_on_init_cb,
	.api_seal = hooked_api_seal_cb,
	.api_set_verbs_v3 = hooked_api_set_verbs_v3_cb,
	.event_handler_add = hooked_event_handler_add_cb,
	.event_handler_del = hooked_event_handler_del_cb,

	.call = hooked_call_x3,
	.call_sync = hooked_call_sync_x3,

	.class_provide = hooked_class_provide_cb,
	.class_require = hooked_class_require_cb,

	.delete_api = hooked_delete_api_cb,
	.settings = hooked_settings_cb,
};
#endif

/******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
                      L I S T E N E R S
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************/

/*
 * Propagates the event to the service
 */
/** Instrumented function listener_of_events(void*,const char*,uint16_t,struct json_object*) */
static void listener_of_events(void *closure, const char *event, uint16_t eventid, struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/listener_of_events(void*,const char*,uint16_t,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===1211###sois===34628###eois===34666###lif===2###soif===114###eoif===152###ins===true###function===./app-framework-binder/src/afb-export.c/listener_of_events(void*,const char*,uint16_t,struct json_object*)");const struct globset_handler *handler;

		AKA_mark("lis===1212###sois===34668###eois===34747###lif===3###soif===154###eoif===233###ins===true###function===./app-framework-binder/src/afb-export.c/listener_of_events(void*,const char*,uint16_t,struct json_object*)");void (*callback)(void *, const char*, struct json_object*, struct afb_api_x3*);

		AKA_mark("lis===1213###sois===34749###eois===34766###lif===4###soif===235###eoif===252###ins===true###function===./app-framework-binder/src/afb-export.c/listener_of_events(void*,const char*,uint16_t,struct json_object*)");struct afb_export
 /* Cant instrument this following code */
*export = from_api_x3(closure);

#if WITH_AFB_HOOK
	/* hook the event before */
	if (export->hooksvc & afb_hook_flag_api_on_event)
		afb_hook_api_on_event_before(export, event, eventid, object);
#endif

	/* transmit to specific handlers */
	/* search the handler */
	/* Cant instrument this following code */
handler = export->event_handlers ? globset_match(export->event_handlers, event) : NULL;
		if (AKA_mark("lis===1224###sois===35127###eois===35134###lif===15###soif===613###eoif===620###ifc===true###function===./app-framework-binder/src/afb-export.c/listener_of_events(void*,const char*,uint16_t,struct json_object*)") && (AKA_mark("lis===1224###sois===35127###eois===35134###lif===15###soif===613###eoif===620###isc===true###function===./app-framework-binder/src/afb-export.c/listener_of_events(void*,const char*,uint16_t,struct json_object*)")&&handler)) {
				AKA_mark("lis===1225###sois===35140###eois===35169###lif===16###soif===626###eoif===655###ins===true###function===./app-framework-binder/src/afb-export.c/listener_of_events(void*,const char*,uint16_t,struct json_object*)");callback = handler->callback;

#if WITH_AFB_HOOK
		if (!(export->hooksvc & afb_hook_flag_api_on_event_handler))
#endif
			/* Cant instrument this following code */
callback(handler->closure, event, object, to_api_x3(export));
#if WITH_AFB_HOOK
		else {
			afb_hook_api_on_event_handler_before(export, event, eventid, object, handler->pattern);
			callback(handler->closure, event, object, to_api_x3(export));
			afb_hook_api_on_event_handler_after(export, event, eventid, object, handler->pattern);
		}
#endif
	}
	else {
		/* transmit to default handler */
		/* Cant instrument this following code */
if (export->on_any_event_v3)
			export->on_any_event_v3(to_api_x3(export), event, object);
		/* Cant instrument this following code */
else if (export->on_any_event_v12)
			export->on_any_event_v12(event, object);
	}


#if WITH_AFB_HOOK
	/* hook the event after */
	if (export->hooksvc & afb_hook_flag_api_on_event)
		afb_hook_api_on_event_after(export, event, eventid, object);
#endif
		AKA_mark("lis===1250###sois===35999###eois===36023###lif===41###soif===1485###eoif===1509###ins===true###function===./app-framework-binder/src/afb-export.c/listener_of_events(void*,const char*,uint16_t,struct json_object*)");json_object_put(object);

}

/** Instrumented function listener_of_pushed_events(void*,const char*,uint16_t,struct json_object*) */
static void listener_of_pushed_events(void *closure, const char *event, uint16_t eventid, struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/listener_of_pushed_events(void*,const char*,uint16_t,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===1255###sois===36148###eois===36200###lif===2###soif===121###eoif===173###ins===true###function===./app-framework-binder/src/afb-export.c/listener_of_pushed_events(void*,const char*,uint16_t,struct json_object*)");listener_of_events(closure, event, eventid, object);

}

/** Instrumented function listener_of_broadcasted_events(void*,const char*,struct json_object*,uuid_binary_t,uint8_t) */
static void listener_of_broadcasted_events(void *closure, const char *event, struct json_object *object, const uuid_binary_t uuid, uint8_t hop)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/listener_of_broadcasted_events(void*,const char*,struct json_object*,uuid_binary_t,uint8_t)");AKA_fCall++;
		AKA_mark("lis===1260###sois===36351###eois===36397###lif===2###soif===147###eoif===193###ins===true###function===./app-framework-binder/src/afb-export.c/listener_of_broadcasted_events(void*,const char*,struct json_object*,uuid_binary_t,uint8_t)");listener_of_events(closure, event, 0, object);

}

/* the interface for events */
static const struct afb_evt_itf evt_itf = {
	.broadcast = listener_of_broadcasted_events,
	.push = listener_of_pushed_events
};

/* ensure an existing listener */
static int ensure_listener(struct afb_export *export)
{
	if (!export->listener) {
		export->listener = afb_evt_listener_create(&evt_itf, export);
		if (export->listener == NULL)
			return -1;
	}
	return 0;
}

int afb_export_event_handler_add(
			struct afb_export *export,
			const char *pattern,
			void (*callback)(void *, const char*, struct json_object*, struct afb_api_x3*),
			void *closure)
{
	int rc;

	/* ensure the listener */
	rc = ensure_listener(export);
	if (rc < 0)
		return rc;

	/* ensure the globset for event handling */
	if (!export->event_handlers) {
		export->event_handlers = globset_create();
		if (!export->event_handlers)
			goto oom_error;
	}

	/* add the handler */
	rc = globset_add(export->event_handlers, pattern, callback, closure);
	if (rc == 0)
		return 0;

	if (errno == EEXIST) {
		ERROR("[API %s] event handler %s already exists", export->api.apiname, pattern);
		return -1;
	}

oom_error:
	ERROR("[API %s] can't allocate event handler %s", export->api.apiname, pattern);
	return -1;
}

int afb_export_event_handler_del(
			struct afb_export *export,
			const char *pattern,
			void **closure)
{
	if (export->event_handlers
	&& !globset_del(export->event_handlers, pattern, closure))
		return 0;

	ERROR("[API %s] event handler %s not found", export->api.apiname, pattern);
	errno = ENOENT;
	return -1;
}

/******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
                                           M E R G E D
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************/

static void set_interfaces(struct afb_export *export)
{
#if WITH_AFB_HOOK
	export->hookditf = afb_hook_flags_api(export->api.apiname);
	export->hooksvc = afb_hook_flags_api(export->api.apiname);
	export->api.itf = export->hookditf|export->hooksvc ? &hooked_api_x3_itf : &api_x3_itf;

	switch (export->version) {
#if WITH_LEGACY_BINDING_V1
	case Api_Version_1:
		export->export.v1.daemon.itf = export->hookditf ? &hooked_daemon_itf : &daemon_itf;
		break;
#endif
#if WITH_LEGACY_BINDING_V2
	case Api_Version_2:
		export->export.v2->daemon.itf = export->hookditf ? &hooked_daemon_itf : &daemon_itf;
		export->export.v2->service.itf = export->hooksvc ? &hooked_service_itf : &service_itf;
		break;
#endif
	}
#else

	export->api.itf = &api_x3_itf;

	switch (export->version) {
#if WITH_LEGACY_BINDING_V1
	case Api_Version_1:
		export->export.v1.daemon.itf = &daemon_itf;
		break;
#endif
#if WITH_LEGACY_BINDING_V2
	case Api_Version_2:
		export->export.v2->daemon.itf = &daemon_itf;
		export->export.v2->service.itf = &service_itf;
		break;
#endif
	}

#endif
}

/** Instrumented function create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version) */
static struct afb_export *create(
				struct afb_apiset *declare_set,
				struct afb_apiset *call_set,
				const char *apiname,
				const char *path,
				enum afb_api_version version)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)");AKA_fCall++;
		AKA_mark("lis===1387###sois===39879###eois===39896###lif===7###soif===187###eoif===204###ins===true###function===./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)");struct afb_export
 /* Cant instrument this following code */
*export;
		AKA_mark("lis===1388###sois===39907###eois===39921###lif===8###soif===215###eoif===229###ins===true###function===./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)");size_t lenapi;


	/* session shared with other exports */
		if (AKA_mark("lis===1391###sois===39969###eois===39991###lif===11###soif===277###eoif===299###ifc===true###function===./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)") && (AKA_mark("lis===1391###sois===39969###eois===39991###lif===11###soif===277###eoif===299###isc===true###function===./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)")&&common_session == NULL)) {
				AKA_mark("lis===1392###sois===39997###eois===40037###lif===12###soif===305###eoif===345###ins===true###function===./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)");common_session = afb_session_create (0);

				if (AKA_mark("lis===1393###sois===40044###eois===40066###lif===13###soif===352###eoif===374###ifc===true###function===./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)") && (AKA_mark("lis===1393###sois===40044###eois===40066###lif===13###soif===352###eoif===374###isc===true###function===./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)")&&common_session == NULL)) {
			AKA_mark("lis===1394###sois===40071###eois===40083###lif===14###soif===379###eoif===391###ins===true###function===./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)");return NULL;
		}
		else {AKA_mark("lis===-1393-###sois===-40044-###eois===-4004422-###lif===-13-###soif===-###eoif===-374-###ins===true###function===./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)");}

	}
	else {AKA_mark("lis===-1391-###sois===-39969-###eois===-3996922-###lif===-11-###soif===-###eoif===-299-###ins===true###function===./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)");}

		AKA_mark("lis===1396###sois===40088###eois===40113###lif===16###soif===396###eoif===421###ins===true###function===./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)");lenapi = strlen(apiname);

	/* Cant instrument this following code */
export = calloc(1, sizeof *export + 1 + lenapi + (path == apiname || !path ? 0 : 1 + strlen(path)));
		if (AKA_mark("lis===1398###sois===40221###eois===40228###lif===18###soif===529###eoif===536###ifc===true###function===./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)") && (AKA_mark("lis===1398###sois===40221###eois===40228###lif===18###soif===529###eoif===536###isc===true###function===./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)")&&!export)) {
		AKA_mark("lis===1399###sois===40232###eois===40247###lif===19###soif===540###eoif===555###ins===true###function===./app-framework-binder/src/afb-export.c/create(struct afb_apiset*,struct afb_apiset*,const char*,const char*,enum afb_api_version)");errno = ENOMEM;
	}
	else {
		/* Cant instrument this following code */
export->refcount = 1;
		/* Cant instrument this following code */
strcpy(export->name, apiname);
		/* Cant instrument this following code */
export->api.apiname = export->name;
		/* Cant instrument this following code */
if (path == apiname)
			export->path = export->name;
		/* Cant instrument this following code */
else if (path)
			export->path = strcpy(&export->name[lenapi + 1], path);
		/* Cant instrument this following code */
export->version = version;
		/* Cant instrument this following code */
export->state = Api_State_Pre_Init;
		/* Cant instrument this following code */
export->session = afb_session_addref(common_session);
		/* Cant instrument this following code */
export->declare_set = afb_apiset_addref(declare_set);
		/* Cant instrument this following code */
export->call_set = afb_apiset_addref(call_set);
	}

	/* Cant instrument this following code */
return export;
}

struct afb_export *afb_export_addref(struct afb_export *export)
{
	if (export)
		__atomic_add_fetch(&export->refcount, 1, __ATOMIC_RELAXED);
	return export;
}

void afb_export_unref(struct afb_export *export)
{
	if (export && !__atomic_sub_fetch(&export->refcount, 1, __ATOMIC_RELAXED))
		afb_export_destroy(export);
}

void afb_export_destroy(struct afb_export *export)
{
	if (export) {
		if (export->event_handlers)
			globset_destroy(export->event_handlers);
		if (export->listener != NULL)
			afb_evt_listener_unref(export->listener);
		afb_session_unref(export->session);
		afb_apiset_unref(export->declare_set);
		afb_apiset_unref(export->call_set);
		json_object_put(export->settings);
		afb_export_unref(export->creator);
		if (export->api.apiname != export->name)
			free((void*)export->api.apiname);
		free(export);
	}
}

/** Instrumented function afb_export_create_none_for_path(struct afb_apiset*,struct afb_apiset*,const char*,int(*creator)(void*, struct afb_api_x3*),void*) */
struct afb_export *afb_export_create_none_for_path(
			struct afb_apiset *declare_set,
			struct afb_apiset *call_set,
			const char *path,
			int (*creator)(void*, struct afb_api_x3*),
			void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/afb_export_create_none_for_path(struct afb_apiset*,struct afb_apiset*,const char*,int(*creator)(void*, struct afb_api_x3*),void*)");AKA_fCall++;
		AKA_mark("lis===1455###sois===41772###eois===41789###lif===7###soif===207###eoif===224###ins===true###function===./app-framework-binder/src/afb-export.c/afb_export_create_none_for_path(struct afb_apiset*,struct afb_apiset*,const char*,int(*creator)(void*, struct afb_api_x3*),void*)");struct afb_export
 /* Cant instrument this following code */
*export = create(declare_set, call_set, path, path, Api_Version_None);
		if (AKA_mark("lis===1456###sois===41866###eois===41872###lif===8###soif===301###eoif===307###ifc===true###function===./app-framework-binder/src/afb-export.c/afb_export_create_none_for_path(struct afb_apiset*,struct afb_apiset*,const char*,int(*creator)(void*, struct afb_api_x3*),void*)") && (AKA_mark("lis===1456###sois===41866###eois===41872###lif===8###soif===301###eoif===307###isc===true###function===./app-framework-binder/src/afb-export.c/afb_export_create_none_for_path(struct afb_apiset*,struct afb_apiset*,const char*,int(*creator)(void*, struct afb_api_x3*),void*)")&&export)) {
		/* Cant instrument this following code */
afb_export_logmask_set(export, logmask);
		/* Cant instrument this following code */
set_interfaces(export);
				if (AKA_mark("lis===1459###sois===41951###eois===42001###lif===11###soif===386###eoif===436###ifc===true###function===./app-framework-binder/src/afb-export.c/afb_export_create_none_for_path(struct afb_apiset*,struct afb_apiset*,const char*,int(*creator)(void*, struct afb_api_x3*),void*)") && (AKA_mark("lis===1459###sois===41951###eois===42001###lif===11###soif===386###eoif===436###isc===true###function===./app-framework-binder/src/afb-export.c/afb_export_create_none_for_path(struct afb_apiset*,struct afb_apiset*,const char*,int(*creator)(void*, struct afb_api_x3*),void*)")&&creator && creator(closure, to_api_x3(export)) < 0)) {
			/* Cant instrument this following code */
afb_export_unref(export);
			/* Cant instrument this following code */
export = NULL;
		}
		else {AKA_mark("lis===-1459-###sois===-41951-###eois===-4195150-###lif===-11-###soif===-###eoif===-436-###ins===true###function===./app-framework-binder/src/afb-export.c/afb_export_create_none_for_path(struct afb_apiset*,struct afb_apiset*,const char*,int(*creator)(void*, struct afb_api_x3*),void*)");}

	}
	else {AKA_mark("lis===-1456-###sois===-41866-###eois===-418666-###lif===-8-###soif===-###eoif===-307-###ins===true###function===./app-framework-binder/src/afb-export.c/afb_export_create_none_for_path(struct afb_apiset*,struct afb_apiset*,const char*,int(*creator)(void*, struct afb_api_x3*),void*)");}

	/* Cant instrument this following code */
return export;
}

#if WITH_LEGACY_BINDING_V1
struct afb_export *afb_export_create_v1(struct afb_apiset *declare_set,
			struct afb_apiset *call_set,
			const char *apiname,
			int (*init)(struct afb_service_x1),
			void (*onevent)(const char*, struct json_object*),
			const char* path)
{
	struct afb_export *export = create(declare_set, call_set, apiname, path, Api_Version_1);
	if (export) {
		export->init.v1 = init;
		export->on_any_event_v12 = onevent;
		export->export.v1.mode = AFB_MODE_LOCAL;
		export->export.v1.daemon.closure = to_api_x3(export);
		afb_export_logmask_set(export, logmask);
		set_interfaces(export);
	}
	return export;
}
#endif

#if WITH_LEGACY_BINDING_V2
struct afb_export *afb_export_create_v2(struct afb_apiset *declare_set,
			struct afb_apiset *call_set,
			const char *apiname,
			const struct afb_binding_v2 *binding,
			struct afb_binding_data_v2 *data,
			int (*init)(),
			void (*onevent)(const char*, struct json_object*),
			const char* path)
{
	struct afb_export *export = create(declare_set, call_set, apiname, path, Api_Version_2);
	if (export) {
		export->init.v2 = init;
		export->on_any_event_v12 = onevent;
		export->desc.v2 = binding;
		export->export.v2 = data;
		data->daemon.closure = to_api_x3(export);
		data->service.closure = to_api_x3(export);
		afb_export_logmask_set(export, logmask);
		set_interfaces(export);
	}
	return export;
}
#endif

/** Instrumented function afb_export_create_v3(struct afb_apiset*,struct afb_apiset*,const char*,struct afb_api_v3*,struct afb_export*,const char*) */
struct afb_export *afb_export_create_v3(struct afb_apiset *declare_set,
			struct afb_apiset *call_set,
			const char *apiname,
			struct afb_api_v3 *apiv3,
			struct afb_export* creator,
			const char* path)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/afb_export_create_v3(struct afb_apiset*,struct afb_apiset*,const char*,struct afb_api_v3*,struct afb_export*,const char*)");AKA_fCall++;
		AKA_mark("lis===1520###sois===43668###eois===43685###lif===7###soif===212###eoif===229###ins===true###function===./app-framework-binder/src/afb-export.c/afb_export_create_v3(struct afb_apiset*,struct afb_apiset*,const char*,struct afb_api_v3*,struct afb_export*,const char*)");struct afb_export
 /* Cant instrument this following code */
*export = create(declare_set, call_set, apiname, path, Api_Version_3);
		if (AKA_mark("lis===1521###sois===43762###eois===43768###lif===8###soif===306###eoif===312###ifc===true###function===./app-framework-binder/src/afb-export.c/afb_export_create_v3(struct afb_apiset*,struct afb_apiset*,const char*,struct afb_api_v3*,struct afb_export*,const char*)") && (AKA_mark("lis===1521###sois===43762###eois===43768###lif===8###soif===306###eoif===312###isc===true###function===./app-framework-binder/src/afb-export.c/afb_export_create_v3(struct afb_apiset*,struct afb_apiset*,const char*,struct afb_api_v3*,struct afb_export*,const char*)")&&export)) {
		/* Cant instrument this following code */
export->unsealed = 1;
		/* Cant instrument this following code */
export->desc.v3 = apiv3;
		/* Cant instrument this following code */
export->creator = afb_export_addref(creator);
		/* Cant instrument this following code */
afb_export_logmask_set(export, logmask);
		/* Cant instrument this following code */
set_interfaces(export);
	}
	else {AKA_mark("lis===-1521-###sois===-43762-###eois===-437626-###lif===-8-###soif===-###eoif===-312-###ins===true###function===./app-framework-binder/src/afb-export.c/afb_export_create_v3(struct afb_apiset*,struct afb_apiset*,const char*,struct afb_api_v3*,struct afb_export*,const char*)");}

	/* Cant instrument this following code */
return export;
}

int afb_export_add_alias(struct afb_export *export, const char *apiname, const char *aliasname)
{
	return afb_apiset_add_alias(export->declare_set, apiname ?: export->api.apiname, aliasname);
}

int afb_export_rename(struct afb_export *export, const char *apiname)
{
	char *name;

	if (export->declared) {
		errno = EBUSY;
		return -1;
	}

	/* copy the name locally */
	name = strdup(apiname);
	if (!name) {
		errno = ENOMEM;
		return -1;
	}

	if (export->api.apiname != export->name)
		free((void*)export->api.apiname);
	export->api.apiname = name;

	set_interfaces(export);
	return 0;
}

const char *afb_export_apiname(const struct afb_export *export)
{
	return export->api.apiname;
}

int afb_export_unshare_session(struct afb_export *export)
{
	if (export->session == common_session) {
		export->session = afb_session_create (0);
		if (export->session)
			afb_session_unref(common_session);
		else {
			export->session = common_session;
			return -1;
		}
	}
	return 0;
}

#if WITH_LEGACY_BINDING_V1 || WITH_LEGACY_BINDING_V2
int afb_export_handle_events_v12(struct afb_export *export, void (*on_event)(const char *event, struct json_object *object))
{
	/* check version */
	switch (export->version) {
#if WITH_LEGACY_BINDING_V1
	case Api_Version_1:
#endif
#if WITH_LEGACY_BINDING_V2
	case Api_Version_2:
		break;
#endif
	default:
		ERROR("invalid version 12 for API %s", export->api.apiname);
		errno = EINVAL;
		return -1;
	}

	export->on_any_event_v12 = on_event;
	return ensure_listener(export);
}
#endif

int afb_export_handle_events_v3(struct afb_export *export, void (*on_event)(struct afb_api_x3 *api, const char *event, struct json_object *object))
{
	/* check version */
	switch (export->version) {
	case Api_Version_3: break;
	default:
		ERROR("invalid version Dyn for API %s", export->api.apiname);
		errno = EINVAL;
		return -1;
	}

	export->on_any_event_v3 = on_event;
	return ensure_listener(export);
}

int afb_export_handle_init_v3(struct afb_export *export, int (*oninit)(struct afb_api_x3 *api))
{
	if (export->state != Api_State_Pre_Init) {
		ERROR("[API %s] Bad call to 'afb_api_x3_on_init', must be in PreInit", export->api.apiname);
		errno = EINVAL;
		return -1;
	}

	export->init.v3  = oninit;
	return 0;
}

#if WITH_LEGACY_BINDING_V1
/*
 * Starts a new service (v1)
 */
struct afb_binding_v1 *afb_export_register_v1(struct afb_export *export, struct afb_binding_v1 *(*regfun)(const struct afb_binding_interface_v1*))
{
	return export->desc.v1 = regfun(&export->export.v1);
}
#endif

int afb_export_preinit_x3(
		struct afb_export *export,
		int (*preinit)(void*, struct afb_api_x3*),
		void *closure)
{
	return preinit(closure, to_api_x3(export));
}

int afb_export_logmask_get(const struct afb_export *export)
{
	return export->api.logmask;
}

void afb_export_logmask_set(struct afb_export *export, int mask)
{
	export->api.logmask = mask;
	switch (export->version) {
#if WITH_LEGACY_BINDING_V1
	case Api_Version_1: export->export.v1.verbosity = verbosity_from_mask(mask); break;
#endif
#if WITH_LEGACY_BINDING_V2
	case Api_Version_2: export->export.v2->verbosity = verbosity_from_mask(mask); break;
#endif
	}
}

void *afb_export_userdata_get(const struct afb_export *export)
{
	return export->api.userdata;
}

void afb_export_userdata_set(struct afb_export *export, void *data)
{
	export->api.userdata = data;
}

/******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
                                           N E W
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************/

struct init
{
	int return_code;
	struct afb_export *export;
};

/** Instrumented function do_init(int,void*) */
static void do_init(int sig, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/do_init(int,void*)");AKA_fCall++;
		AKA_mark("lis===1693###sois===48108###eois===48120###lif===2###soif===47###eoif===59###ins===true###function===./app-framework-binder/src/afb-export.c/do_init(int,void*)");int rc = -1;

		AKA_mark("lis===1694###sois===48122###eois===48150###lif===3###soif===61###eoif===89###ins===true###function===./app-framework-binder/src/afb-export.c/do_init(int,void*)");struct init *init = closure;

		AKA_mark("lis===1695###sois===48152###eois===48169###lif===4###soif===91###eoif===108###ins===true###function===./app-framework-binder/src/afb-export.c/do_init(int,void*)");struct afb_export
 /* Cant instrument this following code */
*export;

		if (AKA_mark("lis===1697###sois===48185###eois===48188###lif===6###soif===124###eoif===127###ifc===true###function===./app-framework-binder/src/afb-export.c/do_init(int,void*)") && (AKA_mark("lis===1697###sois===48185###eois===48188###lif===6###soif===124###eoif===127###isc===true###function===./app-framework-binder/src/afb-export.c/do_init(int,void*)")&&sig)) {
		AKA_mark("lis===1698###sois===48192###eois===48207###lif===7###soif===131###eoif===146###ins===true###function===./app-framework-binder/src/afb-export.c/do_init(int,void*)");errno = EFAULT;
	}
	else {
		/* Cant instrument this following code */
export = init->export;
				AKA_mark("lis===1701###sois===48251###eois===48266###lif===10###soif===190###eoif===205###ins===true###function===./app-framework-binder/src/afb-export.c/do_init(int,void*)");switch(export->version){
#if WITH_LEGACY_BINDING_V1
		case Api_Version_1:
			rc = export->init.v1 ? export->init.v1(
				(struct afb_service_x1){
#if WITH_AFB_HOOK
					.itf = &hooked_service_itf,
#else
					.itf = &service_itf,
#endif
					.closure = to_api_x3(export) }) : 0;
			break;
#endif
#if WITH_LEGACY_BINDING_V2
		case Api_Version_2:
			rc = export->init.v2 ? export->init.v2() : 0;
			break;
#endif
					case Api_Version_3: if(export->version == Api_Version_3)AKA_mark("lis===1719###sois===48657###eois===48676###lif===28###soif===596###eoif===615###function===./app-framework-binder/src/afb-export.c/do_init(int,void*)");

			/* Cant instrument this following code */
rc = export->init.v3 ? export->init.v3(to_api_x3(export)) : 0;
						AKA_mark("lis===1721###sois===48746###eois===48752###lif===30###soif===685###eoif===691###ins===true###function===./app-framework-binder/src/afb-export.c/do_init(int,void*)");break;

					default: if(export->version != Api_Version_3)AKA_mark("lis===1722###sois===48755###eois===48763###lif===31###soif===694###eoif===702###function===./app-framework-binder/src/afb-export.c/do_init(int,void*)");

						AKA_mark("lis===1723###sois===48767###eois===48782###lif===32###soif===706###eoif===721###ins===true###function===./app-framework-binder/src/afb-export.c/do_init(int,void*)");errno = EINVAL;

						AKA_mark("lis===1724###sois===48786###eois===48792###lif===33###soif===725###eoif===731###ins===true###function===./app-framework-binder/src/afb-export.c/do_init(int,void*)");break;

		}

	}

		AKA_mark("lis===1727###sois===48801###eois===48824###lif===36###soif===740###eoif===763###ins===true###function===./app-framework-binder/src/afb-export.c/do_init(int,void*)");init->return_code = rc;

};


int afb_export_start(struct afb_export *export)
{
	struct init init;
	int rc;

	/* check state */
	switch (export->state) {
	case Api_State_Run:
		return 0;

	case Api_State_Init:
		/* starting in progress: it is an error */
		ERROR("Service of API %s required started while starting", export->api.apiname);
		return -1;

	default:
		break;
	}

	/* set event handling */
#if WITH_LEGACY_BINDING_V1 || WITH_LEGACY_BINDING_V2
	switch (export->version) {
#if WITH_LEGACY_BINDING_V1
	case Api_Version_1:
#endif
#if WITH_LEGACY_BINDING_V2
	case Api_Version_2:
#endif
		if (export->on_any_event_v12) {
			rc = afb_export_handle_events_v12(export, export->on_any_event_v12);
			break;
		}
		/*@fallthrough@*/
	default:
		rc = 0;
		break;
	}
	if (rc < 0) {
		ERROR("Can't set event handler for %s", export->api.apiname);
		return -1;
	}
#endif

#if WITH_AFB_HOOK
	/* Starts the service */
	if (export->hooksvc & afb_hook_flag_api_start)
		afb_hook_api_start_before(export);
#endif

	export->state = Api_State_Init;
	init.export = export;
	sig_monitor(0, do_init, &init);
	rc = init.return_code;
	export->state = Api_State_Run;

#if WITH_AFB_HOOK
	if (export->hooksvc & afb_hook_flag_api_start)
		afb_hook_api_start_after(export, rc);
#endif

	if (rc < 0) {
		/* initialisation error */
		ERROR("Initialisation of service API %s failed (%d): %m", export->api.apiname, rc);
		return rc;
	}

	return 0;
}

/** Instrumented function api_call_cb(void*,struct afb_xreq*) */
static void api_call_cb(void *closure, struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/api_call_cb(void*,struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===1802###sois===50290###eois===50307###lif===2###soif===65###eoif===82###ins===true###function===./app-framework-binder/src/afb-export.c/api_call_cb(void*,struct afb_xreq*)");struct afb_export
 /* Cant instrument this following code */
*export = closure;

	/* Cant instrument this following code */
xreq->request.api = to_api_x3(export);

		AKA_mark("lis===1806###sois===50378###eois===50393###lif===6###soif===153###eoif===168###ins===true###function===./app-framework-binder/src/afb-export.c/api_call_cb(void*,struct afb_xreq*)");switch(export->version){
#if WITH_LEGACY_BINDING_V1
	case Api_Version_1:
		afb_api_so_v1_process_call(export->desc.v1, xreq);
		break;
#endif
#if WITH_LEGACY_BINDING_V2
	case Api_Version_2:
		afb_api_so_v2_process_call(export->desc.v2, xreq);
		break;
#endif
			case Api_Version_3: if(export->version == Api_Version_3)AKA_mark("lis===1817###sois===50632###eois===50651###lif===17###soif===407###eoif===426###function===./app-framework-binder/src/afb-export.c/api_call_cb(void*,struct afb_xreq*)");

		/* Cant instrument this following code */
afb_api_v3_process_call(export->desc.v3, xreq);
				AKA_mark("lis===1819###sois===50704###eois===50710###lif===19###soif===479###eoif===485###ins===true###function===./app-framework-binder/src/afb-export.c/api_call_cb(void*,struct afb_xreq*)");break;

			default: if(export->version != Api_Version_3)AKA_mark("lis===1820###sois===50712###eois===50720###lif===20###soif===487###eoif===495###function===./app-framework-binder/src/afb-export.c/api_call_cb(void*,struct afb_xreq*)");

				AKA_mark("lis===1821###sois===50723###eois===50787###lif===21###soif===498###eoif===562###ins===true###function===./app-framework-binder/src/afb-export.c/api_call_cb(void*,struct afb_xreq*)");afb_xreq_reply(xreq, NULL, afb_error_text_internal_error, NULL);

				AKA_mark("lis===1822###sois===50790###eois===50796###lif===22###soif===565###eoif===571###ins===true###function===./app-framework-binder/src/afb-export.c/api_call_cb(void*,struct afb_xreq*)");break;

	}

}

/** Instrumented function api_describe_cb(void*,void(*describecb)(void*, struct json_object*),void*) */
static void api_describe_cb(void *closure, void (*describecb)(void *, struct json_object *), void *clocb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/api_describe_cb(void*,void(*describecb)(void*, struct json_object*),void*)");AKA_fCall++;
		AKA_mark("lis===1828###sois===50912###eois===50929###lif===2###soif===109###eoif===126###ins===true###function===./app-framework-binder/src/afb-export.c/api_describe_cb(void*,void(*describecb)(void*, struct json_object*),void*)");struct afb_export
 /* Cant instrument this following code */
*export = closure;
		AKA_mark("lis===1829###sois===50950###eois===50977###lif===3###soif===147###eoif===174###ins===true###function===./app-framework-binder/src/afb-export.c/api_describe_cb(void*,void(*describecb)(void*, struct json_object*),void*)");struct json_object *result;


		AKA_mark("lis===1831###sois===50988###eois===51003###lif===5###soif===185###eoif===200###ins===true###function===./app-framework-binder/src/afb-export.c/api_describe_cb(void*,void(*describecb)(void*, struct json_object*),void*)");switch(export->version){
#if WITH_LEGACY_BINDING_V1
	case Api_Version_1:
		result = afb_api_so_v1_make_description_openAPIv3(export->desc.v1, export->api.apiname);
		break;
#endif
#if WITH_LEGACY_BINDING_V2
	case Api_Version_2:
		result = afb_api_so_v2_make_description_openAPIv3(export->desc.v2, export->api.apiname);
		break;
#endif
			case Api_Version_3: if(export->version == Api_Version_3)AKA_mark("lis===1842###sois===51318###eois===51337###lif===16###soif===515###eoif===534###function===./app-framework-binder/src/afb-export.c/api_describe_cb(void*,void(*describecb)(void*, struct json_object*),void*)");

		/* Cant instrument this following code */
result = afb_api_v3_make_description_openAPIv3(export->desc.v3, export->api.apiname);
				AKA_mark("lis===1844###sois===51428###eois===51434###lif===18###soif===625###eoif===631###ins===true###function===./app-framework-binder/src/afb-export.c/api_describe_cb(void*,void(*describecb)(void*, struct json_object*),void*)");break;

			default: if(export->version != Api_Version_3)AKA_mark("lis===1845###sois===51436###eois===51444###lif===19###soif===633###eoif===641###function===./app-framework-binder/src/afb-export.c/api_describe_cb(void*,void(*describecb)(void*, struct json_object*),void*)");

				AKA_mark("lis===1846###sois===51447###eois===51461###lif===20###soif===644###eoif===658###ins===true###function===./app-framework-binder/src/afb-export.c/api_describe_cb(void*,void(*describecb)(void*, struct json_object*),void*)");result = NULL;

				AKA_mark("lis===1847###sois===51464###eois===51470###lif===21###soif===661###eoif===667###ins===true###function===./app-framework-binder/src/afb-export.c/api_describe_cb(void*,void(*describecb)(void*, struct json_object*),void*)");break;

	}

		AKA_mark("lis===1849###sois===51475###eois===51501###lif===23###soif===672###eoif===698###ins===true###function===./app-framework-binder/src/afb-export.c/api_describe_cb(void*,void(*describecb)(void*, struct json_object*),void*)");describecb(clocb, result);

}

/** Instrumented function api_service_start_cb(void*) */
static int api_service_start_cb(void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/api_service_start_cb(void*)");AKA_fCall++;
		AKA_mark("lis===1854###sois===51555###eois===51572###lif===2###soif===50###eoif===67###ins===true###function===./app-framework-binder/src/afb-export.c/api_service_start_cb(void*)");struct afb_export
 /* Cant instrument this following code */
*export = closure;

	/* Cant instrument this following code */
return afb_export_start(export);
}

#if WITH_AFB_HOOK
static void api_update_hooks_cb(void *closure)
	__attribute__((alias("set_interfaces")));

void afb_export_update_hooks(struct afb_export *export)
	__attribute__((alias("set_interfaces")));
#endif

/** Instrumented function api_get_logmask_cb(void*) */
static int api_get_logmask_cb(void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/api_get_logmask_cb(void*)");AKA_fCall++;
		AKA_mark("lis===1869###sois===51894###eois===51911###lif===2###soif===48###eoif===65###ins===true###function===./app-framework-binder/src/afb-export.c/api_get_logmask_cb(void*)");struct afb_export
 /* Cant instrument this following code */
*export = closure;

	/* Cant instrument this following code */
return afb_export_logmask_get(export);
}

/** Instrumented function api_set_logmask_cb(void*,int) */
static void api_set_logmask_cb(void *closure, int level)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/api_set_logmask_cb(void*,int)");AKA_fCall++;
		AKA_mark("lis===1876###sois===52035###eois===52052###lif===2###soif===60###eoif===77###ins===true###function===./app-framework-binder/src/afb-export.c/api_set_logmask_cb(void*,int)");struct afb_export
 /* Cant instrument this following code */
*export = closure;

	/* Cant instrument this following code */
afb_export_logmask_set(export, level);
}

/** Instrumented function api_unref_cb(void*) */
static void api_unref_cb(void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-export.c/api_unref_cb(void*)");AKA_fCall++;
		AKA_mark("lis===1883###sois===52159###eois===52176###lif===2###soif===43###eoif===60###ins===true###function===./app-framework-binder/src/afb-export.c/api_unref_cb(void*)");struct afb_export
 /* Cant instrument this following code */
*export = closure;

	/* Cant instrument this following code */
afb_export_unref(export);
}

static struct afb_api_itf export_api_itf =
{
	.call = api_call_cb,
	.service_start = api_service_start_cb,
#if WITH_AFB_HOOK
	.update_hooks = api_update_hooks_cb,
#endif
	.get_logmask = api_get_logmask_cb,
	.set_logmask = api_set_logmask_cb,
	.describe = api_describe_cb,
	.unref = api_unref_cb
};

int afb_export_declare(struct afb_export *export,
			int noconcurrency)
{
	int rc;
	struct afb_api_item afb_api;

	if (export->declared)
		rc = 0;
	else {
		/* init the record structure */
		afb_api.closure = afb_export_addref(export);
		afb_api.itf = &export_api_itf;
		afb_api.group = noconcurrency ? export : NULL;

		/* records the binding */
		rc = afb_apiset_add(export->declare_set, export->api.apiname, afb_api);
		if (rc >= 0)
			export->declared = 1;
		else {
			ERROR("can't declare export %s to set %s, ABORTING it!",
				export->api.apiname,
				afb_apiset_name(export->declare_set));
			afb_export_unref(export);
		}
	}

	return rc;
}

void afb_export_undeclare(struct afb_export *export)
{
	if (export->declared) {
		export->declared = 0;
		afb_apiset_del(export->declare_set, export->api.apiname);
	}
}

int afb_export_subscribe(struct afb_export *export, struct afb_event_x2 *event)
{
	return afb_evt_listener_watch_x2(export->listener, event);
}

int afb_export_unsubscribe(struct afb_export *export, struct afb_event_x2 *event)
{
	return afb_evt_listener_unwatch_x2(export->listener, event);
}

void afb_export_process_xreq(struct afb_export *export, struct afb_xreq *xreq)
{
	afb_xreq_process(xreq, export->call_set);
}

void afb_export_context_init(struct afb_export *export, struct afb_context *context)
{
	afb_context_init_validated(context, export->session, NULL, NULL);
}


#endif

