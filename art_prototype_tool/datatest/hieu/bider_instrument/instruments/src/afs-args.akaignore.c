/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFS_ARGS_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFS_ARGS_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <limits.h>
#include <unistd.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFS_ARGS_H_
#define AKA_INCLUDE__AFS_ARGS_H_
#include "afs-args.akaignore.h"
#endif


#if !defined(AFB_VERSION)
#  error "you should define AFB_VERSION"
#endif

#if !defined(AFS_SUPERVISOR_TOKEN)
#  define AFS_SUPERVISOR_TOKEN    ""
#endif
#if !defined(AFS_SUPERVISOR_PORT)
#  define AFS_SUPERVISOR_PORT     1619
#endif
#define _STRINGIFY_(x) #x
/** Instrumented function STRINGIFY(x) */
#define STRINGIFY(x) _STRINGIFY_(x)

// default
#define DEFLT_CNTX_TIMEOUT  32000000	// default Client Connection
					// Timeout: few more than one year
#define DEFLT_API_TIMEOUT   20		// default Plugin API Timeout [0=NoLimit
					// for Debug Only]
#define DEFLT_CACHE_TIMEOUT 100000	// default Static File Chache
					// [Client Side Cache
					// 100000~=1day]
#define CTX_NBCLIENTS       10		// allow a default of 10 authenticated
					// clients


// Define command line option
#define SET_ROOT_DIR       6
#define SET_ROOT_BASE      7
#define SET_ROOT_API       8

#define SET_CACHE_TIMEOUT  10
#define SET_SESSION_DIR    11

#define SET_APITIMEOUT     14
#define SET_CNTXTIMEOUT    15

#define SET_SESSIONMAX     23

#define SET_ROOT_HTTP      26

#define DISPLAY_HELP       'h'
#define SET_NAME           'n'
#define SET_TCP_PORT       'p'
#define SET_QUIET          'q'
#define WS_SERVICE         's'
#define SET_AUTH_TOKEN     't'
#define SET_UPLOAD_DIR     'u'
#define DISPLAY_VERSION    'V'
#define SET_VERBOSE        'v'
#define SET_WORK_DIR       'w'

const char shortopts[] =
	"hn:p:qrs:t:u:Vvw:"
;

// Command line structure hold cli --command + help text
typedef struct {
	int val;		// command number within application
	int has_arg;		// command number within application
	char *name;		// command as used in --xxxx cli
	char *help;		// help text
} AFB_options;

// Supported option
/** Guard statement to avoid multiple declaration */
#ifndef AKA_GLOBAL_HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFS_ARGS_C_CLIOPTIONS
#define AKA_GLOBAL_HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFS_ARGS_C_CLIOPTIONS
static AFB_options cliOptions[] = {
/* *INDENT-OFF* */
	{SET_VERBOSE,       0, "verbose",     "Verbose Mode, repeat to increase verbosity"},
	{SET_QUIET,         0, "quiet",       "Quiet Mode, repeat to decrease verbosity"},

	{SET_NAME,          1, "name",        "Set the visible name"},

	{SET_TCP_PORT,      1, "port",        "HTTP listening TCP port  [default " STRINGIFY(AFS_SUPERVISOR_PORT) "]"},
	{SET_ROOT_HTTP,     1, "roothttp",    "HTTP Root Directory [default no root http (files not served but apis still available)]"},
	{SET_ROOT_BASE,     1, "rootbase",    "Angular Base Root URL [default /opa]"},
	{SET_ROOT_API,      1, "rootapi",     "HTML Root API URL [default /api]"},

	{SET_APITIMEOUT,    1, "apitimeout",  "Binding API timeout in seconds [default 10]"},
	{SET_CNTXTIMEOUT,   1, "cntxtimeout", "Client Session Context Timeout [default 900]"},
	{SET_CACHE_TIMEOUT, 1, "cache-eol",   "Client cache end of live [default 3600]"},

	{SET_WORK_DIR,      1, "workdir",     "Set the working directory [default: $PWD or current working directory]"},
	{SET_UPLOAD_DIR,    1, "uploaddir",   "Directory for uploading files [default: workdir]"},
	{SET_ROOT_DIR,      1, "rootdir",     "Root Directory of the application [default: workdir]"},
	{SET_SESSION_DIR,   1, "sessiondir",  "OBSOLETE (was: Sessions file path)"},

	{SET_AUTH_TOKEN,    1, "token",       "Initial Secret [default=" AFS_SUPERVISOR_TOKEN ", use --token="" to allow any token]"},

	{WS_SERVICE,        1, "ws-server",   "Provide supervisor as websocket"},
	{DISPLAY_VERSION,   0, "version",     "Display version and copyright"},
	{DISPLAY_HELP,      0, "help",        "Display this help"},

	{SET_SESSIONMAX,    1, "session-max", "Max count of session simultaneously [default 10]"},

	{0, 0, NULL, NULL}
/* *INDENT-ON* */
};
#endif


struct enumdesc
{
	const char *name;
	int value;
};

/*----------------------------------------------------------
 | printversion
 |   print version and copyright
 +--------------------------------------------------------- */
/** Instrumented function printVersion(FILE*) */
static void printVersion(FILE * file)
{AKA_mark("Calling: ./app-framework-binder/src/afs-args.c/printVersion(FILE*)");AKA_fCall++;
		AKA_mark("lis===140###sois===4561###eois===4821###lif===2###soif===41###eoif===301###ins===true###function===./app-framework-binder/src/afs-args.c/printVersion(FILE*)");static const char version[] =
		"\n"
		"  afs-supervisor [Application Framework Supervisor] version=\"AFB_VERSION\"\n"
		"\n"
		"  Copyright (C) 2015-2020 \"IoT.bzh\"\n"
		"  afs-supervisor comes with ABSOLUTELY NO WARRANTY.\n"
		"  Licence Apache 2\n"
		"\n";


		AKA_mark("lis===149###sois===4824###eois===4853###lif===11###soif===304###eoif===333###ins===true###function===./app-framework-binder/src/afs-args.c/printVersion(FILE*)");fprintf(file, "%s", version);

}

/*----------------------------------------------------------
 | printHelp
 |   print information from long option array
 +--------------------------------------------------------- */

/** Instrumented function printHelp(FILE*,const char*) */
static void printHelp(FILE * file, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)");AKA_fCall++;
		AKA_mark("lis===159###sois===5097###eois===5105###lif===2###soif===56###eoif===64###ins===true###function===./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)");int ind;

		AKA_mark("lis===160###sois===5107###eois===5124###lif===3###soif===66###eoif===83###ins===true###function===./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)");char command[50];


		AKA_mark("lis===162###sois===5127###eois===5173###lif===5###soif===86###eoif===132###ins===true###function===./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)");fprintf(file, "%s:\nallowed options\n", name);

		AKA_mark("lis===163###sois===5180###eois===5188###lif===6###soif===139###eoif===147###ins===true###function===./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)");for (ind = 0;AKA_mark("lis===163###sois===5189###eois===5217###lif===6###soif===148###eoif===176###ifc===true###function===./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)") && AKA_mark("lis===163###sois===5189###eois===5217###lif===6###soif===148###eoif===176###isc===true###function===./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)")&&cliOptions[ind].name != NULL;({AKA_mark("lis===163###sois===5219###eois===5224###lif===6###soif===178###eoif===183###ins===true###function===./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)");ind++;})) {
				AKA_mark("lis===164###sois===5230###eois===5268###lif===7###soif===189###eoif===227###ins===true###function===./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)");strcpy(command, cliOptions[ind].name);

				if (AKA_mark("lis===165###sois===5275###eois===5298###lif===8###soif===234###eoif===257###ifc===true###function===./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)") && (AKA_mark("lis===165###sois===5275###eois===5298###lif===8###soif===234###eoif===257###isc===true###function===./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)")&&cliOptions[ind].has_arg)) {
			AKA_mark("lis===166###sois===5303###eois===5328###lif===9###soif===262###eoif===287###ins===true###function===./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)");strcat(command, "=xxxx");
		}
		else {AKA_mark("lis===-165-###sois===-5275-###eois===-527523-###lif===-8-###soif===-###eoif===-257-###ins===true###function===./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)");}

				AKA_mark("lis===167###sois===5331###eois===5394###lif===10###soif===290###eoif===353###ins===true###function===./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)");fprintf(file, "  --%-15s %s\n", command, cliOptions[ind].help);

	}

		AKA_mark("lis===169###sois===5399###eois===5483###lif===12###soif===358###eoif===442###ins===true###function===./app-framework-binder/src/afs-args.c/printHelp(FILE*,const char*)");fprintf(file,
		"Example:\n  %s  --verbose --port=1234 --token='azerty'\n",
		name);

}


/*---------------------------------------------------------
 |   helpers for argument scanning
 +--------------------------------------------------------- */

/** Instrumented function name_of_option(int) */
static const char *name_of_option(int optc)
{AKA_mark("Calling: ./app-framework-binder/src/afs-args.c/name_of_option(int)");AKA_fCall++;
		AKA_mark("lis===181###sois===5694###eois===5722###lif===2###soif===47###eoif===75###ins===true###function===./app-framework-binder/src/afs-args.c/name_of_option(int)");AFB_options *o = cliOptions;

		while (AKA_mark("lis===182###sois===5731###eois===5756###lif===3###soif===84###eoif===109###ifc===true###function===./app-framework-binder/src/afs-args.c/name_of_option(int)") && ((AKA_mark("lis===182###sois===5731###eois===5738###lif===3###soif===84###eoif===91###isc===true###function===./app-framework-binder/src/afs-args.c/name_of_option(int)")&&o->name)	&&(AKA_mark("lis===182###sois===5742###eois===5756###lif===3###soif===95###eoif===109###isc===true###function===./app-framework-binder/src/afs-args.c/name_of_option(int)")&&o->val != optc))) {
		AKA_mark("lis===183###sois===5760###eois===5764###lif===4###soif===113###eoif===117###ins===true###function===./app-framework-binder/src/afs-args.c/name_of_option(int)");o++;
	}

		AKA_mark("lis===184###sois===5766###eois===5809###lif===5###soif===119###eoif===162###ins===true###function===./app-framework-binder/src/afs-args.c/name_of_option(int)");return o->name ? : "<unknown-option-name>";

}

/** Instrumented function current_argument(int) */
static const char *current_argument(int optc)
{AKA_mark("Calling: ./app-framework-binder/src/afs-args.c/current_argument(int)");AKA_fCall++;
		if (AKA_mark("lis===189###sois===5866###eois===5877###lif===2###soif===53###eoif===64###ifc===true###function===./app-framework-binder/src/afs-args.c/current_argument(int)") && (AKA_mark("lis===189###sois===5866###eois===5877###lif===2###soif===53###eoif===64###isc===true###function===./app-framework-binder/src/afs-args.c/current_argument(int)")&&optarg == 0)) {
				AKA_mark("lis===190###sois===5883###eois===5986###lif===3###soif===70###eoif===173###ins===true###function===./app-framework-binder/src/afs-args.c/current_argument(int)");ERROR("option [--%s] needs a value i.e. --%s=xxx",
		      name_of_option(optc), name_of_option(optc));

				AKA_mark("lis===192###sois===5989###eois===5997###lif===5###soif===176###eoif===184###ins===true###function===./app-framework-binder/src/afs-args.c/current_argument(int)");exit(1);

	}
	else {AKA_mark("lis===-189-###sois===-5866-###eois===-586611-###lif===-2-###soif===-###eoif===-64-###ins===true###function===./app-framework-binder/src/afs-args.c/current_argument(int)");}

		AKA_mark("lis===194###sois===6002###eois===6016###lif===7###soif===189###eoif===203###ins===true###function===./app-framework-binder/src/afs-args.c/current_argument(int)");return optarg;

}

/** Instrumented function argvalstr(int) */
static char *argvalstr(int optc)
{AKA_mark("Calling: ./app-framework-binder/src/afs-args.c/argvalstr(int)");AKA_fCall++;
		AKA_mark("lis===199###sois===6056###eois===6102###lif===2###soif===36###eoif===82###ins===true###function===./app-framework-binder/src/afs-args.c/argvalstr(int)");char *result = strdup(current_argument(optc));

		if (AKA_mark("lis===200###sois===6108###eois===6122###lif===3###soif===88###eoif===102###ifc===true###function===./app-framework-binder/src/afs-args.c/argvalstr(int)") && (AKA_mark("lis===200###sois===6108###eois===6122###lif===3###soif===88###eoif===102###isc===true###function===./app-framework-binder/src/afs-args.c/argvalstr(int)")&&result == NULL)) {
				AKA_mark("lis===201###sois===6128###eois===6156###lif===4###soif===108###eoif===136###ins===true###function===./app-framework-binder/src/afs-args.c/argvalstr(int)");ERROR("can't alloc memory");

				AKA_mark("lis===202###sois===6159###eois===6167###lif===5###soif===139###eoif===147###ins===true###function===./app-framework-binder/src/afs-args.c/argvalstr(int)");exit(1);

	}
	else {AKA_mark("lis===-200-###sois===-6108-###eois===-610814-###lif===-3-###soif===-###eoif===-102-###ins===true###function===./app-framework-binder/src/afs-args.c/argvalstr(int)");}

		AKA_mark("lis===204###sois===6172###eois===6186###lif===7###soif===152###eoif===166###ins===true###function===./app-framework-binder/src/afs-args.c/argvalstr(int)");return result;

}

/** Instrumented function argvalint(int,int,int,int) */
static int argvalint(int optc, int mini, int maxi, int base)
{AKA_mark("Calling: ./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)");AKA_fCall++;
		AKA_mark("lis===209###sois===6254###eois===6276###lif===2###soif===64###eoif===86###ins===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)");const char *beg, *end;

		AKA_mark("lis===210###sois===6278###eois===6291###lif===3###soif===88###eoif===101###ins===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)");long int val;

		AKA_mark("lis===211###sois===6293###eois===6322###lif===4###soif===103###eoif===132###ins===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)");beg = current_argument(optc);

		AKA_mark("lis===212###sois===6324###eois===6362###lif===5###soif===134###eoif===172###ins===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)");val = strtol(beg, (char**)&end, base);

		if (AKA_mark("lis===213###sois===6368###eois===6386###lif===6###soif===178###eoif===196###ifc===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)") && ((AKA_mark("lis===213###sois===6368###eois===6372###lif===6###soif===178###eoif===182###isc===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)")&&*end)	||(AKA_mark("lis===213###sois===6376###eois===6386###lif===6###soif===186###eoif===196###isc===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)")&&end == beg))) {
				AKA_mark("lis===214###sois===6392###eois===6481###lif===7###soif===202###eoif===291###ins===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)");ERROR("option [--%s] requires a valid integer (found %s)",
			name_of_option(optc), beg);

				AKA_mark("lis===216###sois===6484###eois===6492###lif===9###soif===294###eoif===302###ins===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)");exit(1);

	}
	else {AKA_mark("lis===-213-###sois===-6368-###eois===-636818-###lif===-6-###soif===-###eoif===-196-###ins===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)");}

		if (AKA_mark("lis===218###sois===6501###eois===6545###lif===11###soif===311###eoif===355###ifc===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)") && ((AKA_mark("lis===218###sois===6501###eois===6521###lif===11###soif===311###eoif===331###isc===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)")&&val < (long int)mini)	||(AKA_mark("lis===218###sois===6525###eois===6545###lif===11###soif===335###eoif===355###isc===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)")&&val > (long int)maxi))) {
				AKA_mark("lis===219###sois===6551###eois===6654###lif===12###soif===361###eoif===464###ins===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)");ERROR("option [--%s] value out of bounds (not %d<=%ld<=%d)",
			name_of_option(optc), mini, val, maxi);

				AKA_mark("lis===221###sois===6657###eois===6665###lif===14###soif===467###eoif===475###ins===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)");exit(1);

	}
	else {AKA_mark("lis===-218-###sois===-6501-###eois===-650144-###lif===-11-###soif===-###eoif===-355-###ins===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)");}

		AKA_mark("lis===223###sois===6670###eois===6686###lif===16###soif===480###eoif===496###ins===true###function===./app-framework-binder/src/afs-args.c/argvalint(int,int,int,int)");return (int)val;

}

/** Instrumented function argvalintdec(int,int,int) */
static int argvalintdec(int optc, int mini, int maxi)
{AKA_mark("Calling: ./app-framework-binder/src/afs-args.c/argvalintdec(int,int,int)");AKA_fCall++;
		AKA_mark("lis===228###sois===6747###eois===6786###lif===2###soif===57###eoif===96###ins===true###function===./app-framework-binder/src/afs-args.c/argvalintdec(int,int,int)");return argvalint(optc, mini, maxi, 10);

}

/** Instrumented function noarg(int) */
static void noarg(int optc)
{AKA_mark("Calling: ./app-framework-binder/src/afs-args.c/noarg(int)");AKA_fCall++;
		if (AKA_mark("lis===233###sois===6825###eois===6836###lif===2###soif===35###eoif===46###ifc===true###function===./app-framework-binder/src/afs-args.c/noarg(int)") && (AKA_mark("lis===233###sois===6825###eois===6836###lif===2###soif===35###eoif===46###isc===true###function===./app-framework-binder/src/afs-args.c/noarg(int)")&&optarg != 0)) {
				AKA_mark("lis===234###sois===6842###eois===6920###lif===3###soif===52###eoif===130###ins===true###function===./app-framework-binder/src/afs-args.c/noarg(int)");ERROR("option [--%s] need no value (found %s)", name_of_option(optc), optarg);

				AKA_mark("lis===235###sois===6923###eois===6931###lif===4###soif===133###eoif===141###ins===true###function===./app-framework-binder/src/afs-args.c/noarg(int)");exit(1);

	}
	else {AKA_mark("lis===-233-###sois===-6825-###eois===-682511-###lif===-2-###soif===-###eoif===-46-###ins===true###function===./app-framework-binder/src/afs-args.c/noarg(int)");}

}

/*---------------------------------------------------------
 |   Parse option and launch action
 +--------------------------------------------------------- */

/** Instrumented function parse_arguments(int,char**,struct afs_args*) */
static void parse_arguments(int argc, char **argv, struct afs_args *config)
{AKA_mark("Calling: ./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");AKA_fCall++;
		AKA_mark("lis===245###sois===7177###eois===7205###lif===2###soif===79###eoif===107###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");char *programName = argv[0];

		AKA_mark("lis===246###sois===7207###eois===7221###lif===3###soif===109###eoif===123###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");int optc, ind;

		AKA_mark("lis===247###sois===7223###eois===7233###lif===4###soif===125###eoif===135###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");int nbcmd;

		AKA_mark("lis===248###sois===7235###eois===7261###lif===5###soif===137###eoif===163###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");struct option *gnuOptions;


	// ------------------ Process Command Line -----------------------

	// build GNU getopt info from cliOptions
		AKA_mark("lis===253###sois===7375###eois===7424###lif===10###soif===277###eoif===326###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");nbcmd = sizeof(cliOptions) / sizeof(AFB_options);

		AKA_mark("lis===254###sois===7426###eois===7485###lif===11###soif===328###eoif===387###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");gnuOptions = malloc(sizeof(*gnuOptions) * (unsigned)nbcmd);

		AKA_mark("lis===255###sois===7492###eois===7500###lif===12###soif===394###eoif===402###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");for (ind = 0;AKA_mark("lis===255###sois===7501###eois===7512###lif===12###soif===403###eoif===414###ifc===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)") && AKA_mark("lis===255###sois===7501###eois===7512###lif===12###soif===403###eoif===414###isc===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)")&&ind < nbcmd;({AKA_mark("lis===255###sois===7514###eois===7519###lif===12###soif===416###eoif===421###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");ind++;})) {
				AKA_mark("lis===256###sois===7525###eois===7569###lif===13###soif===427###eoif===471###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");gnuOptions[ind].name = cliOptions[ind].name;

				AKA_mark("lis===257###sois===7572###eois===7622###lif===14###soif===474###eoif===524###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");gnuOptions[ind].has_arg = cliOptions[ind].has_arg;

				AKA_mark("lis===258###sois===7625###eois===7650###lif===15###soif===527###eoif===552###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");gnuOptions[ind].flag = 0;

				AKA_mark("lis===259###sois===7653###eois===7695###lif===16###soif===555###eoif===597###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");gnuOptions[ind].val = cliOptions[ind].val;

	}


	// get all options from command line
		while (AKA_mark("lis===263###sois===7746###eois===7814###lif===20###soif===648###eoif===716###ifc===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)") && (AKA_mark("lis===263###sois===7746###eois===7814###lif===20###soif===648###eoif===716###isc===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)")&&(optc = getopt_long(argc, argv, shortopts, gnuOptions, NULL)) != EOF)) {
				AKA_mark("lis===264###sois===7828###eois===7832###lif===21###soif===730###eoif===734###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");switch(optc){
					case SET_VERBOSE: if(optc == SET_VERBOSE)AKA_mark("lis===265###sois===7838###eois===7855###lif===22###soif===740###eoif===757###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===266###sois===7859###eois===7873###lif===23###soif===761###eoif===775###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");verbose_inc();

						AKA_mark("lis===267###sois===7877###eois===7883###lif===24###soif===779###eoif===785###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case SET_QUIET: if(optc == SET_QUIET)AKA_mark("lis===269###sois===7887###eois===7902###lif===26###soif===789###eoif===804###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===270###sois===7906###eois===7920###lif===27###soif===808###eoif===822###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");verbose_dec();

						AKA_mark("lis===271###sois===7924###eois===7930###lif===28###soif===826###eoif===832###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case SET_TCP_PORT: if(optc == SET_TCP_PORT)AKA_mark("lis===273###sois===7934###eois===7952###lif===30###soif===836###eoif===854###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===274###sois===7956###eois===8008###lif===31###soif===858###eoif===910###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");config->httpdPort = argvalintdec(optc, 1024, 32767);

						AKA_mark("lis===275###sois===8012###eois===8018###lif===32###soif===914###eoif===920###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case SET_APITIMEOUT: if(optc == SET_APITIMEOUT)AKA_mark("lis===277###sois===8022###eois===8042###lif===34###soif===924###eoif===944###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===278###sois===8046###eois===8098###lif===35###soif===948###eoif===1000###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");config->apiTimeout = argvalintdec(optc, 0, INT_MAX);

						AKA_mark("lis===279###sois===8102###eois===8108###lif===36###soif===1004###eoif===1010###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case SET_CNTXTIMEOUT: if(optc == SET_CNTXTIMEOUT)AKA_mark("lis===281###sois===8112###eois===8133###lif===38###soif===1014###eoif===1035###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===282###sois===8137###eois===8190###lif===39###soif===1039###eoif===1092###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");config->cntxTimeout = argvalintdec(optc, 0, INT_MAX);

						AKA_mark("lis===283###sois===8194###eois===8200###lif===40###soif===1096###eoif===1102###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case SET_ROOT_DIR: if(optc == SET_ROOT_DIR)AKA_mark("lis===285###sois===8204###eois===8222###lif===42###soif===1106###eoif===1124###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===286###sois===8226###eois===8260###lif===43###soif===1128###eoif===1162###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");config->rootdir = argvalstr(optc);

						AKA_mark("lis===287###sois===8264###eois===8308###lif===44###soif===1166###eoif===1210###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");INFO("Forcing Rootdir=%s", config->rootdir);

						AKA_mark("lis===288###sois===8312###eois===8318###lif===45###soif===1214###eoif===1220###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case SET_ROOT_HTTP: if(optc == SET_ROOT_HTTP)AKA_mark("lis===290###sois===8322###eois===8341###lif===47###soif===1224###eoif===1243###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===291###sois===8345###eois===8380###lif===48###soif===1247###eoif===1282###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");config->roothttp = argvalstr(optc);

						AKA_mark("lis===292###sois===8384###eois===8431###lif===49###soif===1286###eoif===1333###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");INFO("Forcing Root HTTP=%s", config->roothttp);

						AKA_mark("lis===293###sois===8435###eois===8441###lif===50###soif===1337###eoif===1343###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case SET_ROOT_BASE: if(optc == SET_ROOT_BASE)AKA_mark("lis===295###sois===8445###eois===8464###lif===52###soif===1347###eoif===1366###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===296###sois===8468###eois===8503###lif===53###soif===1370###eoif===1405###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");config->rootbase = argvalstr(optc);

						AKA_mark("lis===297###sois===8507###eois===8553###lif===54###soif===1409###eoif===1455###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");INFO("Forcing Rootbase=%s", config->rootbase);

						AKA_mark("lis===298###sois===8557###eois===8563###lif===55###soif===1459###eoif===1465###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case SET_ROOT_API: if(optc == SET_ROOT_API)AKA_mark("lis===300###sois===8567###eois===8585###lif===57###soif===1469###eoif===1487###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===301###sois===8589###eois===8623###lif===58###soif===1491###eoif===1525###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");config->rootapi = argvalstr(optc);

						AKA_mark("lis===302###sois===8627###eois===8671###lif===59###soif===1529###eoif===1573###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");INFO("Forcing Rootapi=%s", config->rootapi);

						AKA_mark("lis===303###sois===8675###eois===8681###lif===60###soif===1577###eoif===1583###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case SET_AUTH_TOKEN: if(optc == SET_AUTH_TOKEN)AKA_mark("lis===305###sois===8685###eois===8705###lif===62###soif===1587###eoif===1607###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===306###sois===8709###eois===8741###lif===63###soif===1611###eoif===1643###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");config->token = argvalstr(optc);

						AKA_mark("lis===307###sois===8745###eois===8751###lif===64###soif===1647###eoif===1653###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case SET_UPLOAD_DIR: if(optc == SET_UPLOAD_DIR)AKA_mark("lis===309###sois===8755###eois===8775###lif===66###soif===1657###eoif===1677###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===310###sois===8779###eois===8815###lif===67###soif===1681###eoif===1717###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");config->uploaddir = argvalstr(optc);

						AKA_mark("lis===311###sois===8819###eois===8825###lif===68###soif===1721###eoif===1727###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case SET_WORK_DIR: if(optc == SET_WORK_DIR)AKA_mark("lis===313###sois===8829###eois===8847###lif===70###soif===1731###eoif===1749###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===314###sois===8851###eois===8885###lif===71###soif===1753###eoif===1787###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");config->workdir = argvalstr(optc);

						AKA_mark("lis===315###sois===8889###eois===8895###lif===72###soif===1791###eoif===1797###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case SET_CACHE_TIMEOUT: if(optc == SET_CACHE_TIMEOUT)AKA_mark("lis===317###sois===8899###eois===8922###lif===74###soif===1801###eoif===1824###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===318###sois===8926###eois===8980###lif===75###soif===1828###eoif===1882###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");config->cacheTimeout = argvalintdec(optc, 0, INT_MAX);

						AKA_mark("lis===319###sois===8984###eois===8990###lif===76###soif===1886###eoif===1892###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case SET_SESSIONMAX: if(optc == SET_SESSIONMAX)AKA_mark("lis===321###sois===8994###eois===9014###lif===78###soif===1896###eoif===1916###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===322###sois===9018###eois===9072###lif===79###soif===1920###eoif===1974###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");config->nbSessionMax = argvalintdec(optc, 1, INT_MAX);

						AKA_mark("lis===323###sois===9076###eois===9082###lif===80###soif===1978###eoif===1984###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case SET_NAME: if(optc == SET_NAME)AKA_mark("lis===325###sois===9086###eois===9100###lif===82###soif===1988###eoif===2002###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===326###sois===9104###eois===9135###lif===83###soif===2006###eoif===2037###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");config->name = argvalstr(optc);

						AKA_mark("lis===327###sois===9139###eois===9145###lif===84###soif===2041###eoif===2047###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case WS_SERVICE: if(optc == WS_SERVICE)AKA_mark("lis===329###sois===9149###eois===9165###lif===86###soif===2051###eoif===2067###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===330###sois===9169###eois===9205###lif===87###soif===2071###eoif===2107###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");config->ws_server = argvalstr(optc);

						AKA_mark("lis===331###sois===9209###eois===9215###lif===88###soif===2111###eoif===2117###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");break;


					case DISPLAY_VERSION: if(optc == DISPLAY_VERSION)AKA_mark("lis===333###sois===9219###eois===9240###lif===90###soif===2121###eoif===2142###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===334###sois===9244###eois===9256###lif===91###soif===2146###eoif===2158###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");noarg(optc);

						AKA_mark("lis===335###sois===9260###eois===9281###lif===92###soif===2162###eoif===2183###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");printVersion(stdout);

						AKA_mark("lis===336###sois===9285###eois===9293###lif===93###soif===2187###eoif===2195###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");exit(0);


					case DISPLAY_HELP: if(optc == DISPLAY_HELP)AKA_mark("lis===338###sois===9297###eois===9315###lif===95###soif===2199###eoif===2217###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===339###sois===9319###eois===9350###lif===96###soif===2221###eoif===2252###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");printHelp(stdout, programName);

						AKA_mark("lis===340###sois===9354###eois===9362###lif===97###soif===2256###eoif===2264###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");exit(0);


					default: if(optc != SET_VERBOSE && optc != SET_QUIET && optc != SET_TCP_PORT && optc != SET_APITIMEOUT && optc != SET_CNTXTIMEOUT && optc != SET_ROOT_DIR && optc != SET_ROOT_HTTP && optc != SET_ROOT_BASE && optc != SET_ROOT_API && optc != SET_AUTH_TOKEN && optc != SET_UPLOAD_DIR && optc != SET_WORK_DIR && optc != SET_CACHE_TIMEOUT && optc != SET_SESSIONMAX && optc != SET_NAME && optc != WS_SERVICE && optc != DISPLAY_VERSION && optc != DISPLAY_HELP)AKA_mark("lis===342###sois===9366###eois===9374###lif===99###soif===2268###eoif===2276###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");

						AKA_mark("lis===343###sois===9378###eois===9386###lif===100###soif===2280###eoif===2288###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");exit(1);

		}

	}

		AKA_mark("lis===346###sois===9395###eois===9412###lif===103###soif===2297###eoif===2314###ins===true###function===./app-framework-binder/src/afs-args.c/parse_arguments(int,char**,struct afs_args*)");free(gnuOptions);

}

/** Instrumented function fulfill_config(struct afs_args*) */
static void fulfill_config(struct afs_args *config)
{AKA_mark("Calling: ./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");AKA_fCall++;
	// default HTTP port
		if (AKA_mark("lis===352###sois===9497###eois===9519###lif===3###soif===81###eoif===103###ifc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)") && (AKA_mark("lis===352###sois===9497###eois===9519###lif===3###soif===81###eoif===103###isc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)")&&config->httpdPort == 0)) {
		AKA_mark("lis===353###sois===9523###eois===9563###lif===4###soif===107###eoif===147###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");config->httpdPort = AFS_SUPERVISOR_PORT;
	}
	else {AKA_mark("lis===-352-###sois===-9497-###eois===-949722-###lif===-3-###soif===-###eoif===-103-###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");}


	// default binding API timeout
		if (AKA_mark("lis===356###sois===9602###eois===9625###lif===7###soif===186###eoif===209###ifc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)") && (AKA_mark("lis===356###sois===9602###eois===9625###lif===7###soif===186###eoif===209###isc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)")&&config->apiTimeout == 0)) {
		AKA_mark("lis===357###sois===9629###eois===9668###lif===8###soif===213###eoif===252###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");config->apiTimeout = DEFLT_API_TIMEOUT;
	}
	else {AKA_mark("lis===-356-###sois===-9602-###eois===-960223-###lif===-7-###soif===-###eoif===-209-###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");}


	// cache timeout default one hour
		if (AKA_mark("lis===360###sois===9710###eois===9735###lif===11###soif===294###eoif===319###ifc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)") && (AKA_mark("lis===360###sois===9710###eois===9735###lif===11###soif===294###eoif===319###isc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)")&&config->cacheTimeout == 0)) {
		AKA_mark("lis===361###sois===9739###eois===9782###lif===12###soif===323###eoif===366###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");config->cacheTimeout = DEFLT_CACHE_TIMEOUT;
	}
	else {AKA_mark("lis===-360-###sois===-9710-###eois===-971025-###lif===-11-###soif===-###eoif===-319-###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");}


	// cache timeout default one hour
		if (AKA_mark("lis===364###sois===9824###eois===9848###lif===15###soif===408###eoif===432###ifc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)") && (AKA_mark("lis===364###sois===9824###eois===9848###lif===15###soif===408###eoif===432###isc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)")&&config->cntxTimeout == 0)) {
		AKA_mark("lis===365###sois===9852###eois===9893###lif===16###soif===436###eoif===477###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");config->cntxTimeout = DEFLT_CNTX_TIMEOUT;
	}
	else {AKA_mark("lis===-364-###sois===-9824-###eois===-982424-###lif===-15-###soif===-###eoif===-432-###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");}


	// max count of sessions
		if (AKA_mark("lis===368###sois===9926###eois===9951###lif===19###soif===510###eoif===535###ifc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)") && (AKA_mark("lis===368###sois===9926###eois===9951###lif===19###soif===510###eoif===535###isc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)")&&config->nbSessionMax == 0)) {
		AKA_mark("lis===369###sois===9955###eois===9992###lif===20###soif===539###eoif===576###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");config->nbSessionMax = CTX_NBCLIENTS;
	}
	else {AKA_mark("lis===-368-###sois===-9926-###eois===-992625-###lif===-19-###soif===-###eoif===-535-###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");}


	/* set directories */
		if (AKA_mark("lis===372###sois===10022###eois===10045###lif===23###soif===606###eoif===629###ifc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)") && (AKA_mark("lis===372###sois===10022###eois===10045###lif===23###soif===606###eoif===629###isc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)")&&config->workdir == NULL)) {
		AKA_mark("lis===373###sois===10049###eois===10071###lif===24###soif===633###eoif===655###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");config->workdir = ".";
	}
	else {AKA_mark("lis===-372-###sois===-10022-###eois===-1002223-###lif===-23-###soif===-###eoif===-629-###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");}


		if (AKA_mark("lis===375###sois===10078###eois===10101###lif===26###soif===662###eoif===685###ifc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)") && (AKA_mark("lis===375###sois===10078###eois===10101###lif===26###soif===662###eoif===685###isc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)")&&config->rootdir == NULL)) {
		AKA_mark("lis===376###sois===10105###eois===10127###lif===27###soif===689###eoif===711###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");config->rootdir = ".";
	}
	else {AKA_mark("lis===-375-###sois===-10078-###eois===-1007823-###lif===-26-###soif===-###eoif===-685-###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");}


		if (AKA_mark("lis===378###sois===10134###eois===10159###lif===29###soif===718###eoif===743###ifc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)") && (AKA_mark("lis===378###sois===10134###eois===10159###lif===29###soif===718###eoif===743###isc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)")&&config->uploaddir == NULL)) {
		AKA_mark("lis===379###sois===10163###eois===10187###lif===30###soif===747###eoif===771###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");config->uploaddir = ".";
	}
	else {AKA_mark("lis===-378-###sois===-10134-###eois===-1013425-###lif===-29-###soif===-###eoif===-743-###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");}


	// if no Angular/HTML5 rootbase let's try '/' as default
		if (AKA_mark("lis===382###sois===10252###eois===10276###lif===33###soif===836###eoif===860###ifc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)") && (AKA_mark("lis===382###sois===10252###eois===10276###lif===33###soif===836###eoif===860###isc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)")&&config->rootbase == NULL)) {
		AKA_mark("lis===383###sois===10280###eois===10306###lif===34###soif===864###eoif===890###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");config->rootbase = "/opa";
	}
	else {AKA_mark("lis===-382-###sois===-10252-###eois===-1025224-###lif===-33-###soif===-###eoif===-860-###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");}


		if (AKA_mark("lis===385###sois===10313###eois===10336###lif===36###soif===897###eoif===920###ifc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)") && (AKA_mark("lis===385###sois===10313###eois===10336###lif===36###soif===897###eoif===920###isc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)")&&config->rootapi == NULL)) {
		AKA_mark("lis===386###sois===10340###eois===10365###lif===37###soif===924###eoif===949###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");config->rootapi = "/api";
	}
	else {AKA_mark("lis===-385-###sois===-10313-###eois===-1031323-###lif===-36-###soif===-###eoif===-920-###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");}


		if (AKA_mark("lis===388###sois===10372###eois===10393###lif===39###soif===956###eoif===977###ifc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)") && (AKA_mark("lis===388###sois===10372###eois===10393###lif===39###soif===956###eoif===977###isc===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)")&&config->token == NULL)) {
		AKA_mark("lis===389###sois===10397###eois===10434###lif===40###soif===981###eoif===1018###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");config->token = AFS_SUPERVISOR_TOKEN;
	}
	else {AKA_mark("lis===-388-###sois===-10372-###eois===-1037221-###lif===-39-###soif===-###eoif===-977-###ins===true###function===./app-framework-binder/src/afs-args.c/fulfill_config(struct afs_args*)");}

}

/** Instrumented function dump(struct afs_args*) */
static void dump(struct afs_args *config)
{AKA_mark("Calling: ./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");AKA_fCall++;
/** Instrumented function NN(x) */
#define NN(x)   (x)?:""
/** Instrumented function P(...) */
#define P(...)  fprintf(stderr, __VA_ARGS__)
#define PF(x)   P("-- %15s: ", #x)
#define PE      P("\n")
/** Instrumented function S(x) */
#define S(x)	PF(x);P("%s",NN(config->x));PE;
/** Instrumented function D(x) */
#define D(x)	PF(x);P("%d",config->x);PE;

		AKA_mark("lis===401###sois===10698###eois===10727###lif===9###soif===260###eoif===289###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");P("---BEGIN-OF-CONFIG---\n");

		AKA_mark("lis===402###sois===10729###eois===10739###lif===10###soif===291###eoif===301###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===402###sois===10729###eois===10739###lif===10###soif===291###eoif===301###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===402###sois===10729###eois===10739###lif===10###soif===291###eoif===301###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");S(rootdir)



		AKA_mark("lis===403###sois===10741###eois===10752###lif===11###soif===303###eoif===314###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===403###sois===10741###eois===10752###lif===11###soif===303###eoif===314###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===403###sois===10741###eois===10752###lif===11###soif===303###eoif===314###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");S(roothttp)



		AKA_mark("lis===404###sois===10754###eois===10765###lif===12###soif===316###eoif===327###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===404###sois===10754###eois===10765###lif===12###soif===316###eoif===327###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===404###sois===10754###eois===10765###lif===12###soif===316###eoif===327###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");S(rootbase)



		AKA_mark("lis===405###sois===10767###eois===10777###lif===13###soif===329###eoif===339###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===405###sois===10767###eois===10777###lif===13###soif===329###eoif===339###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===405###sois===10767###eois===10777###lif===13###soif===329###eoif===339###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");S(rootapi)



		AKA_mark("lis===406###sois===10779###eois===10789###lif===14###soif===341###eoif===351###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===406###sois===10779###eois===10789###lif===14###soif===341###eoif===351###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===406###sois===10779###eois===10789###lif===14###soif===341###eoif===351###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");S(workdir)



		AKA_mark("lis===407###sois===10791###eois===10803###lif===15###soif===353###eoif===365###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===407###sois===10791###eois===10803###lif===15###soif===353###eoif===365###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===407###sois===10791###eois===10803###lif===15###soif===353###eoif===365###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");S(uploaddir)



		AKA_mark("lis===408###sois===10805###eois===10813###lif===16###soif===367###eoif===375###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===408###sois===10805###eois===10813###lif===16###soif===367###eoif===375###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===408###sois===10805###eois===10813###lif===16###soif===367###eoif===375###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");S(token)



		AKA_mark("lis===409###sois===10815###eois===10822###lif===17###soif===377###eoif===384###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===409###sois===10815###eois===10822###lif===17###soif===377###eoif===384###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===409###sois===10815###eois===10822###lif===17###soif===377###eoif===384###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");S(name)



		AKA_mark("lis===410###sois===10824###eois===10836###lif===18###soif===386###eoif===398###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===410###sois===10824###eois===10836###lif===18###soif===386###eoif===398###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===410###sois===10824###eois===10836###lif===18###soif===386###eoif===398###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");S(ws_server)




		AKA_mark("lis===412###sois===10839###eois===10851###lif===20###soif===401###eoif===413###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===412###sois===10839###eois===10851###lif===20###soif===401###eoif===413###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===412###sois===10839###eois===10851###lif===20###soif===401###eoif===413###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");D(httpdPort)



		AKA_mark("lis===413###sois===10853###eois===10868###lif===21###soif===415###eoif===430###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===413###sois===10853###eois===10868###lif===21###soif===415###eoif===430###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===413###sois===10853###eois===10868###lif===21###soif===415###eoif===430###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");D(cacheTimeout)



		AKA_mark("lis===414###sois===10870###eois===10883###lif===22###soif===432###eoif===445###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===414###sois===10870###eois===10883###lif===22###soif===432###eoif===445###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===414###sois===10870###eois===10883###lif===22###soif===432###eoif===445###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");D(apiTimeout)



		AKA_mark("lis===415###sois===10885###eois===10899###lif===23###soif===447###eoif===461###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===415###sois===10885###eois===10899###lif===23###soif===447###eoif===461###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===415###sois===10885###eois===10899###lif===23###soif===447###eoif===461###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");D(cntxTimeout)



		AKA_mark("lis===416###sois===10901###eois===10916###lif===24###soif===463###eoif===478###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===416###sois===10901###eois===10916###lif===24###soif===463###eoif===478###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");	AKA_mark("lis===416###sois===10901###eois===10916###lif===24###soif===463###eoif===478###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");D(nbSessionMax)



		AKA_mark("lis===417###sois===10918###eois===10945###lif===25###soif===480###eoif===507###ins===true###function===./app-framework-binder/src/afs-args.c/dump(struct afs_args*)");P("---END-OF-CONFIG---\n");


#undef V
#undef E
#undef L
#undef B
#undef D
#undef S
#undef PE
#undef PF
#undef P
#undef NN
}

/** Instrumented function parse_environment(struct afs_args*) */
static void parse_environment(struct afs_args *config)
{AKA_mark("Calling: ./app-framework-binder/src/afs-args.c/parse_environment(struct afs_args*)");AKA_fCall++;
}

/** Instrumented function afs_args_parse(int,char**) */
struct afs_args *afs_args_parse(int argc, char **argv)
{AKA_mark("Calling: ./app-framework-binder/src/afs-args.c/afs_args_parse(int,char**)");AKA_fCall++;
		AKA_mark("lis===437###sois===11161###eois===11185###lif===2###soif===58###eoif===82###ins===true###function===./app-framework-binder/src/afs-args.c/afs_args_parse(int,char**)");struct afs_args *result;


		AKA_mark("lis===439###sois===11188###eois===11223###lif===4###soif===85###eoif===120###ins===true###function===./app-framework-binder/src/afs-args.c/afs_args_parse(int,char**)");result = calloc(1, sizeof *result);


		AKA_mark("lis===441###sois===11226###eois===11252###lif===6###soif===123###eoif===149###ins===true###function===./app-framework-binder/src/afs-args.c/afs_args_parse(int,char**)");parse_environment(result);

		AKA_mark("lis===442###sois===11254###eois===11290###lif===7###soif===151###eoif===187###ins===true###function===./app-framework-binder/src/afs-args.c/afs_args_parse(int,char**)");parse_arguments(argc, argv, result);

		AKA_mark("lis===443###sois===11292###eois===11315###lif===8###soif===189###eoif===212###ins===true###function===./app-framework-binder/src/afs-args.c/afs_args_parse(int,char**)");fulfill_config(result);

		AKA_mark("lis===444###sois===11321###eois===11350###lif===9###soif===218###eoif===247###function===./app-framework-binder/src/afs-args.c/afs_args_parse(int,char**)");if (verbose_wants(Log_Level_Info)){
		AKA_mark("lis===445###sois===11354###eois===11367###lif===10###soif===251###eoif===264###ins===true###function===./app-framework-binder/src/afs-args.c/afs_args_parse(int,char**)");dump(result);
	}
	else {AKA_mark("");}

		AKA_mark("lis===446###sois===11369###eois===11383###lif===11###soif===266###eoif===280###ins===true###function===./app-framework-binder/src/afs-args.c/afs_args_parse(int,char**)");return result;

}


#endif

