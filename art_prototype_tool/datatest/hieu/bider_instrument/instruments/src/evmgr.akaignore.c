/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_EVMGR_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_EVMGR_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <sys/syscall.h>
#include <pthread.h>
#include <errno.h>
#include <assert.h>
#include <sys/eventfd.h>

#include <systemd/sd-event.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__EVMGR_H_
#define AKA_INCLUDE__EVMGR_H_
#include "evmgr.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__SYSTEMD_H_
#define AKA_INCLUDE__SYSTEMD_H_
#include "systemd.akaignore.h"
#endif


/** Description of handled event loops */
struct evmgr
{
	unsigned state;        /**< encoded state */
	int efd;               /**< event notification */
	void *holder;          /**< holder of the evmgr */
	struct sd_event *sdev; /**< the systemd event loop */
};

#define EVLOOP_STATE_WAIT           1U
#define EVLOOP_STATE_RUN            2U

/**
 * prepare the evmgr to run
 */
/** Instrumented function evmgr_prepare_run(struct evmgr*) */
void evmgr_prepare_run(struct evmgr *evmgr)
{AKA_mark("Calling: ./app-framework-binder/src/evmgr.c/evmgr_prepare_run(struct evmgr*)");AKA_fCall++;
		AKA_mark("lis===55###sois===1413###eois===1463###lif===2###soif===47###eoif===97###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_prepare_run(struct evmgr*)");evmgr->state = EVLOOP_STATE_WAIT|EVLOOP_STATE_RUN;

}

/**
 * Run the event loop is set.
 */
/** Instrumented function evmgr_run(struct evmgr*) */
void evmgr_run(struct evmgr *evmgr)
{AKA_mark("Calling: ./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");AKA_fCall++;
		AKA_mark("lis===63###sois===1544###eois===1551###lif===2###soif===39###eoif===46###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");int rc;

		AKA_mark("lis===64###sois===1553###eois===1573###lif===3###soif===48###eoif===68###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");struct sd_event *se;


		AKA_mark("lis===66###sois===1576###eois===1626###lif===5###soif===71###eoif===121###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");evmgr->state = EVLOOP_STATE_WAIT|EVLOOP_STATE_RUN;

		AKA_mark("lis===67###sois===1628###eois===1645###lif===6###soif===123###eoif===140###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");se = evmgr->sdev;

		AKA_mark("lis===68###sois===1647###eois===1673###lif===7###soif===142###eoif===168###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");rc = sd_event_prepare(se);

		if (AKA_mark("lis===69###sois===1679###eois===1685###lif===8###soif===174###eoif===180###ifc===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)") && (AKA_mark("lis===69###sois===1679###eois===1685###lif===8###soif===174###eoif===180###isc===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)")&&rc < 0)) {
				AKA_mark("lis===70###sois===1691###eois===1703###lif===9###soif===186###eoif===198###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");errno = -rc;

				AKA_mark("lis===71###sois===1706###eois===1793###lif===10###soif===201###eoif===288###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");CRITICAL("sd_event_prepare returned an error (state: %d): %m", sd_event_get_state(se));

				AKA_mark("lis===72###sois===1796###eois===1804###lif===11###soif===291###eoif===299###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");abort();

	}
	else {
				if (AKA_mark("lis===74###sois===1821###eois===1828###lif===13###soif===316###eoif===323###ifc===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)") && (AKA_mark("lis===74###sois===1821###eois===1828###lif===13###soif===316###eoif===323###isc===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)")&&rc == 0)) {
						AKA_mark("lis===75###sois===1835###eois===1881###lif===14###soif===330###eoif===376###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");rc = sd_event_wait(se, (uint64_t)(int64_t)-1);

						if (AKA_mark("lis===76###sois===1889###eois===1895###lif===15###soif===384###eoif===390###ifc===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)") && (AKA_mark("lis===76###sois===1889###eois===1895###lif===15###soif===384###eoif===390###isc===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)")&&rc < 0)) {
								AKA_mark("lis===77###sois===1903###eois===1915###lif===16###soif===398###eoif===410###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");errno = -rc;

								AKA_mark("lis===78###sois===1920###eois===2001###lif===17###soif===415###eoif===496###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");ERROR("sd_event_wait returned an error (state: %d): %m", sd_event_get_state(se));

			}
			else {AKA_mark("lis===-76-###sois===-1889-###eois===-18896-###lif===-15-###soif===-###eoif===-390-###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");}

		}
		else {AKA_mark("lis===-74-###sois===-1821-###eois===-18217-###lif===-13-###soif===-###eoif===-323-###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");}

				AKA_mark("lis===81###sois===2013###eois===2045###lif===20###soif===508###eoif===540###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");evmgr->state = EVLOOP_STATE_RUN;

				if (AKA_mark("lis===82###sois===2052###eois===2058###lif===21###soif===547###eoif===553###ifc===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)") && (AKA_mark("lis===82###sois===2052###eois===2058###lif===21###soif===547###eoif===553###isc===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)")&&rc > 0)) {
						AKA_mark("lis===83###sois===2065###eois===2092###lif===22###soif===560###eoif===587###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");rc = sd_event_dispatch(se);

						if (AKA_mark("lis===84###sois===2100###eois===2106###lif===23###soif===595###eoif===601###ifc===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)") && (AKA_mark("lis===84###sois===2100###eois===2106###lif===23###soif===595###eoif===601###isc===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)")&&rc < 0)) {
								AKA_mark("lis===85###sois===2114###eois===2126###lif===24###soif===609###eoif===621###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");errno = -rc;

								AKA_mark("lis===86###sois===2131###eois===2216###lif===25###soif===626###eoif===711###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");ERROR("sd_event_dispatch returned an error (state: %d): %m", sd_event_get_state(se));

			}
			else {AKA_mark("lis===-84-###sois===-2100-###eois===-21006-###lif===-23-###soif===-###eoif===-601-###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");}

		}
		else {AKA_mark("lis===-82-###sois===-2052-###eois===-20526-###lif===-21-###soif===-###eoif===-553-###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");}

	}

		AKA_mark("lis===90###sois===2230###eois===2247###lif===29###soif===725###eoif===742###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_run(struct evmgr*)");evmgr->state = 0;

}

/** Instrumented function evmgr_job_run(int,struct evmgr*) */
void evmgr_job_run(int signum, struct evmgr *evmgr)
{AKA_mark("Calling: ./app-framework-binder/src/evmgr.c/evmgr_job_run(int,struct evmgr*)");AKA_fCall++;
		if (AKA_mark("lis===95###sois===2310###eois===2316###lif===2###soif===59###eoif===65###ifc===true###function===./app-framework-binder/src/evmgr.c/evmgr_job_run(int,struct evmgr*)") && (AKA_mark("lis===95###sois===2310###eois===2316###lif===2###soif===59###eoif===65###isc===true###function===./app-framework-binder/src/evmgr.c/evmgr_job_run(int,struct evmgr*)")&&signum)) {
		AKA_mark("lis===96###sois===2320###eois===2337###lif===3###soif===69###eoif===86###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_job_run(int,struct evmgr*)");evmgr->state = 0;
	}
	else {
		AKA_mark("lis===98###sois===2346###eois===2363###lif===5###soif===95###eoif===112###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_job_run(int,struct evmgr*)");evmgr_run(evmgr);
	}

}

/** Instrumented function evmgr_can_run(struct evmgr*) */
int evmgr_can_run(struct evmgr *evmgr)
{AKA_mark("Calling: ./app-framework-binder/src/evmgr.c/evmgr_can_run(struct evmgr*)");AKA_fCall++;
		AKA_mark("lis===103###sois===2409###eois===2430###lif===2###soif===42###eoif===63###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_can_run(struct evmgr*)");return !evmgr->state;

}

/**
 * Internal callback for evmgr management.
 * The effect of this function is hidden: it exits
 * the waiting poll if any.
 */
/** Instrumented function evmgr_on_efd_event(struct evmgr*) */
static void evmgr_on_efd_event(struct evmgr *evmgr)
{AKA_mark("Calling: ./app-framework-binder/src/evmgr.c/evmgr_on_efd_event(struct evmgr*)");AKA_fCall++;
		AKA_mark("lis===113###sois===2619###eois===2630###lif===2###soif===55###eoif===66###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_on_efd_event(struct evmgr*)");uint64_t x;

		AKA_mark("lis===114###sois===2632###eois===2663###lif===3###soif===68###eoif===99###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_on_efd_event(struct evmgr*)");read(evmgr->efd, &x, sizeof x);

}

/**
 * wakeup the event loop if needed by sending
 * an event.
 */
/** Instrumented function evmgr_wakeup(struct evmgr*) */
void evmgr_wakeup(struct evmgr *evmgr)
{AKA_mark("Calling: ./app-framework-binder/src/evmgr.c/evmgr_wakeup(struct evmgr*)");AKA_fCall++;
		AKA_mark("lis===123###sois===2776###eois===2787###lif===2###soif===42###eoif===53###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_wakeup(struct evmgr*)");uint64_t x;


		if (AKA_mark("lis===125###sois===2794###eois===2826###lif===4###soif===60###eoif===92###ifc===true###function===./app-framework-binder/src/evmgr.c/evmgr_wakeup(struct evmgr*)") && (AKA_mark("lis===125###sois===2794###eois===2826###lif===4###soif===60###eoif===92###isc===true###function===./app-framework-binder/src/evmgr.c/evmgr_wakeup(struct evmgr*)")&&evmgr->state & EVLOOP_STATE_WAIT)) {
				AKA_mark("lis===126###sois===2832###eois===2838###lif===5###soif===98###eoif===104###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_wakeup(struct evmgr*)");x = 1;

				AKA_mark("lis===127###sois===2841###eois===2873###lif===6###soif===107###eoif===139###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_wakeup(struct evmgr*)");write(evmgr->efd, &x, sizeof x);

	}
	else {AKA_mark("lis===-125-###sois===-2794-###eois===-279432-###lif===-4-###soif===-###eoif===-92-###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_wakeup(struct evmgr*)");}

}

/**
 */
/** Instrumented function evmgr_holder(struct evmgr*) */
void *evmgr_holder(struct evmgr *evmgr)
{AKA_mark("Calling: ./app-framework-binder/src/evmgr.c/evmgr_holder(struct evmgr*)");AKA_fCall++;
		AKA_mark("lis===135###sois===2931###eois===2952###lif===2###soif===43###eoif===64###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_holder(struct evmgr*)");return evmgr->holder;

}

/**
 */
/** Instrumented function evmgr_release_if(struct evmgr*,void*) */
int evmgr_release_if(struct evmgr *evmgr, void *holder)
{AKA_mark("Calling: ./app-framework-binder/src/evmgr.c/evmgr_release_if(struct evmgr*,void*)");AKA_fCall++;
		if (AKA_mark("lis===142###sois===3027###eois===3050###lif===2###soif===63###eoif===86###ifc===true###function===./app-framework-binder/src/evmgr.c/evmgr_release_if(struct evmgr*,void*)") && (AKA_mark("lis===142###sois===3027###eois===3050###lif===2###soif===63###eoif===86###isc===true###function===./app-framework-binder/src/evmgr.c/evmgr_release_if(struct evmgr*,void*)")&&evmgr->holder != holder)) {
		AKA_mark("lis===143###sois===3054###eois===3063###lif===3###soif===90###eoif===99###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_release_if(struct evmgr*,void*)");return 0;
	}
	else {AKA_mark("lis===-142-###sois===-3027-###eois===-302723-###lif===-2-###soif===-###eoif===-86-###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_release_if(struct evmgr*,void*)");}

		AKA_mark("lis===144###sois===3065###eois===3083###lif===4###soif===101###eoif===119###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_release_if(struct evmgr*,void*)");evmgr->holder = 0;

		AKA_mark("lis===145###sois===3085###eois===3094###lif===5###soif===121###eoif===130###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_release_if(struct evmgr*,void*)");return 1;

}

/**
 */
/** Instrumented function evmgr_try_hold(struct evmgr*,void*) */
int evmgr_try_hold(struct evmgr *evmgr, void *holder)
{AKA_mark("Calling: ./app-framework-binder/src/evmgr.c/evmgr_try_hold(struct evmgr*,void*)");AKA_fCall++;
		if (AKA_mark("lis===152###sois===3167###eois===3181###lif===2###soif===61###eoif===75###ifc===true###function===./app-framework-binder/src/evmgr.c/evmgr_try_hold(struct evmgr*,void*)") && (AKA_mark("lis===152###sois===3167###eois===3181###lif===2###soif===61###eoif===75###isc===true###function===./app-framework-binder/src/evmgr.c/evmgr_try_hold(struct evmgr*,void*)")&&!evmgr->holder)) {
		AKA_mark("lis===153###sois===3185###eois===3208###lif===3###soif===79###eoif===102###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_try_hold(struct evmgr*,void*)");evmgr->holder = holder;
	}
	else {AKA_mark("lis===-152-###sois===-3167-###eois===-316714-###lif===-2-###soif===-###eoif===-75-###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_try_hold(struct evmgr*,void*)");}

		AKA_mark("lis===154###sois===3210###eois===3241###lif===4###soif===104###eoif===135###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_try_hold(struct evmgr*,void*)");return evmgr->holder == holder;

}

/******************************************************************************/
/******************************************************************************/
/******  SYSTEM D                                                        ******/
/******************************************************************************/
/******************************************************************************/

/**
 * Internal callback for evmgr management.
 * The effect of this function is hidden: it exits
 * the waiting poll if any. Then it wakes up a thread
 * awaiting the evmgr using signal.
 */
/** Instrumented function on_evmgr_efd(sd_event_source*,int,uint32_t,void*) */
static int on_evmgr_efd(sd_event_source *s, int fd, uint32_t revents, void *userdata)
{AKA_mark("Calling: ./app-framework-binder/src/evmgr.c/on_evmgr_efd(sd_event_source*,int,uint32_t,void*)");AKA_fCall++;
		AKA_mark("lis===171###sois===3932###eois===3963###lif===2###soif===89###eoif===120###ins===true###function===./app-framework-binder/src/evmgr.c/on_evmgr_efd(sd_event_source*,int,uint32_t,void*)");struct evmgr *evmgr = userdata;

		AKA_mark("lis===172###sois===3965###eois===3991###lif===3###soif===122###eoif===148###ins===true###function===./app-framework-binder/src/evmgr.c/on_evmgr_efd(sd_event_source*,int,uint32_t,void*)");evmgr_on_efd_event(evmgr);

		AKA_mark("lis===173###sois===3993###eois===4002###lif===4###soif===150###eoif===159###ins===true###function===./app-framework-binder/src/evmgr.c/on_evmgr_efd(sd_event_source*,int,uint32_t,void*)");return 1;

}

/**
 * Gets a sd_event item for the current thread.
 * @return a sd_event or NULL in case of error
 */
/** Instrumented function evmgr_create(struct evmgr**) */
int evmgr_create(struct evmgr **result)
{AKA_mark("Calling: ./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");AKA_fCall++;
		AKA_mark("lis===182###sois===4152###eois===4159###lif===2###soif===43###eoif===50###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");int rc;

		AKA_mark("lis===183###sois===4161###eois===4181###lif===3###soif===52###eoif===72###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");struct evmgr *evmgr;


	/* creates the evmgr on need */
		AKA_mark("lis===186###sois===4217###eois===4247###lif===6###soif===108###eoif===138###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");evmgr = malloc(sizeof *evmgr);

		if (AKA_mark("lis===187###sois===4253###eois===4259###lif===7###soif===144###eoif===150###ifc===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)") && (AKA_mark("lis===187###sois===4253###eois===4259###lif===7###soif===144###eoif===150###isc===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)")&&!evmgr)) {
				AKA_mark("lis===188###sois===4265###eois===4288###lif===8###soif===156###eoif===179###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");ERROR("out of memory");

				AKA_mark("lis===189###sois===4291###eois===4304###lif===9###soif===182###eoif===195###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");rc = -ENOMEM;

				AKA_mark("lis===190###sois===4307###eois===4318###lif===10###soif===198###eoif===209###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");goto error;

	}
	else {AKA_mark("lis===-187-###sois===-4253-###eois===-42536-###lif===-7-###soif===-###eoif===-150-###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");}


	/* creates the eventfd for waking up polls */
		AKA_mark("lis===194###sois===4371###eois===4422###lif===14###soif===262###eoif===313###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");evmgr->efd = eventfd(0, EFD_CLOEXEC|EFD_SEMAPHORE);

		if (AKA_mark("lis===195###sois===4428###eois===4442###lif===15###soif===319###eoif===333###ifc===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)") && (AKA_mark("lis===195###sois===4428###eois===4442###lif===15###soif===319###eoif===333###isc===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)")&&evmgr->efd < 0)) {
				AKA_mark("lis===196###sois===4448###eois===4460###lif===16###soif===339###eoif===351###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");rc = -errno;

				AKA_mark("lis===197###sois===4463###eois===4502###lif===17###soif===354###eoif===393###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");ERROR("can't make eventfd for events");

				AKA_mark("lis===198###sois===4505###eois===4517###lif===18###soif===396###eoif===408###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");goto error1;

	}
	else {AKA_mark("lis===-195-###sois===-4428-###eois===-442814-###lif===-15-###soif===-###eoif===-333-###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");}

	/* create the systemd event loop */
		AKA_mark("lis===201###sois===4559###eois===4598###lif===21###soif===450###eoif===489###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");evmgr->sdev = systemd_get_event_loop();

		if (AKA_mark("lis===202###sois===4604###eois===4616###lif===22###soif===495###eoif===507###ifc===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)") && (AKA_mark("lis===202###sois===4604###eois===4616###lif===22###soif===495###eoif===507###isc===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)")&&!evmgr->sdev)) {
				AKA_mark("lis===203###sois===4622###eois===4634###lif===23###soif===513###eoif===525###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");rc = -errno;

				AKA_mark("lis===204###sois===4637###eois===4672###lif===24###soif===528###eoif===563###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");ERROR("can't make new event loop");

				AKA_mark("lis===205###sois===4675###eois===4687###lif===25###soif===566###eoif===578###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");goto error2;

	}
	else {AKA_mark("lis===-202-###sois===-4604-###eois===-460412-###lif===-22-###soif===-###eoif===-507-###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");}

	/* put the eventfd in the event loop */
		AKA_mark("lis===208###sois===4733###eois===4815###lif===28###soif===624###eoif===706###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");rc = sd_event_add_io(evmgr->sdev, NULL, evmgr->efd, EPOLLIN, on_evmgr_efd, evmgr);

		if (AKA_mark("lis===209###sois===4821###eois===4827###lif===29###soif===712###eoif===718###ifc===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)") && (AKA_mark("lis===209###sois===4821###eois===4827###lif===29###soif===712###eoif===718###isc===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)")&&rc < 0)) {
				AKA_mark("lis===210###sois===4833###eois===4865###lif===30###soif===724###eoif===756###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");ERROR("can't register eventfd");

				AKA_mark("lis===211###sois===4868###eois===4880###lif===31###soif===759###eoif===771###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");goto error2;

	}
	else {AKA_mark("lis===-209-###sois===-4821-###eois===-48216-###lif===-29-###soif===-###eoif===-718-###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");}


	/* start the creation */
		AKA_mark("lis===215###sois===4912###eois===4929###lif===35###soif===803###eoif===820###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");evmgr->state = 0;

		AKA_mark("lis===216###sois===4931###eois===4949###lif===36###soif===822###eoif===840###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");evmgr->holder = 0;

		AKA_mark("lis===217###sois===4951###eois===4967###lif===37###soif===842###eoif===858###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");*result = evmgr;

		AKA_mark("lis===218###sois===4969###eois===4978###lif===38###soif===860###eoif===869###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");return 0;



	error2:
	AKA_mark("lis===222###sois===4990###eois===5008###lif===42###soif===881###eoif===899###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");close(evmgr->efd);

	error1:
	AKA_mark("lis===224###sois===5018###eois===5030###lif===44###soif===909###eoif===921###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");free(evmgr);

	error:
	AKA_mark("lis===226###sois===5039###eois===5051###lif===46###soif===930###eoif===942###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");*result = 0;

		AKA_mark("lis===227###sois===5053###eois===5063###lif===47###soif===944###eoif===954###ins===true###function===./app-framework-binder/src/evmgr.c/evmgr_create(struct evmgr**)");return rc;

}


#endif

