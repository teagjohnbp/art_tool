/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_API_WS_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_API_WS_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <endian.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_H_
#define AKA_INCLUDE__AFB_API_H_
#include "afb-api.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_APISET_H_
#define AKA_INCLUDE__AFB_APISET_H_
#include "afb-apiset.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_WS_H_
#define AKA_INCLUDE__AFB_API_WS_H_
#include "afb-api-ws.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_FDEV_H_
#define AKA_INCLUDE__AFB_FDEV_H_
#include "afb-fdev.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SOCKET_H_
#define AKA_INCLUDE__AFB_SOCKET_H_
#include "afb-socket.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_STUB_WS_H_
#define AKA_INCLUDE__AFB_STUB_WS_H_
#include "afb-stub-ws.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_H_
#define AKA_INCLUDE__FDEV_H_
#include "fdev.akaignore.h"
#endif


struct api_ws_server
{
	struct afb_apiset *apiset;	/* the apiset for calling */
	struct fdev *fdev;		/* fdev handler */
	uint16_t offapi;		/* api name of the interface */
	char uri[];			/* the uri of the server socket */
};

/******************************************************************************/
/***       C L I E N T                                                      ***/
/******************************************************************************/

/** Instrumented function reopen_client(void*) */
static struct fdev *reopen_client(void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-ws.c/reopen_client(void*)");AKA_fCall++;
		AKA_mark("lis===55###sois===1590###eois===1616###lif===2###soif===52###eoif===78###ins===true###function===./app-framework-binder/src/afb-api-ws.c/reopen_client(void*)");const char *uri = closure;

		AKA_mark("lis===56###sois===1618###eois===1654###lif===3###soif===80###eoif===116###ins===true###function===./app-framework-binder/src/afb-api-ws.c/reopen_client(void*)");return afb_socket_open_fdev(uri, 0);

}

/** Instrumented function afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int) */
int afb_api_ws_add_client(const char *uri, struct afb_apiset *declare_set, struct afb_apiset *call_set, int strong)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");AKA_fCall++;
		AKA_mark("lis===61###sois===1777###eois===1804###lif===2###soif===119###eoif===146###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");struct afb_stub_ws *stubws;

		AKA_mark("lis===62###sois===1806###eois===1824###lif===3###soif===148###eoif===166###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");struct fdev *fdev;

		AKA_mark("lis===63###sois===1826###eois===1842###lif===4###soif===168###eoif===184###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");const char *api;


	/* check the api name */
		AKA_mark("lis===66###sois===1871###eois===1897###lif===7###soif===213###eoif===239###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");api = afb_socket_api(uri);

		if (AKA_mark("lis===67###sois===1903###eois===1945###lif===8###soif===245###eoif===287###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)") && ((AKA_mark("lis===67###sois===1903###eois===1914###lif===8###soif===245###eoif===256###isc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)")&&api == NULL)	||(AKA_mark("lis===67###sois===1918###eois===1945###lif===8###soif===260###eoif===287###isc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)")&&!afb_api_is_valid_name(api)))) {
				AKA_mark("lis===68###sois===1951###eois===2001###lif===9###soif===293###eoif===343###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");ERROR("invalid (too long) ws client uri %s", uri);

				AKA_mark("lis===69###sois===2004###eois===2019###lif===10###soif===346###eoif===361###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");errno = EINVAL;

				AKA_mark("lis===70###sois===2022###eois===2033###lif===11###soif===364###eoif===375###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");goto error;

	}
	else {AKA_mark("lis===-67-###sois===-1903-###eois===-190342-###lif===-8-###soif===-###eoif===-287-###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");}


	/* open the socket */
		AKA_mark("lis===74###sois===2062###eois===2098###lif===15###soif===404###eoif===440###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");fdev = afb_socket_open_fdev(uri, 0);

		if (AKA_mark("lis===75###sois===2104###eois===2108###lif===16###soif===446###eoif===450###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)") && (AKA_mark("lis===75###sois===2104###eois===2108###lif===16###soif===446###eoif===450###isc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)")&&fdev)) {
		/* create the client stub */
				AKA_mark("lis===77###sois===2145###eois===2201###lif===18###soif===487###eoif===543###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");stubws = afb_stub_ws_create_client(fdev, api, call_set);

				if (AKA_mark("lis===78###sois===2208###eois===2215###lif===19###soif===550###eoif===557###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)") && (AKA_mark("lis===78###sois===2208###eois===2215###lif===19###soif===550###eoif===557###isc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)")&&!stubws)) {
						AKA_mark("lis===79###sois===2222###eois===2272###lif===20###soif===564###eoif===614###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");ERROR("can't setup client ws service to %s", uri);

						AKA_mark("lis===80###sois===2276###eois===2293###lif===21###soif===618###eoif===635###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");fdev_unref(fdev);

		}
		else {
						if (AKA_mark("lis===82###sois===2312###eois===2360###lif===23###soif===654###eoif===702###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)") && (AKA_mark("lis===82###sois===2312###eois===2360###lif===23###soif===654###eoif===702###isc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)")&&afb_stub_ws_client_add(stubws, declare_set) >= 0)) {
#if 1
				/* it is asserted here that uri is never released */
								AKA_mark("lis===85###sois===2431###eois===2501###lif===26###soif===773###eoif===843###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");afb_stub_ws_client_robustify(stubws, reopen_client, (void*)uri, NULL);

#else
				/* it is asserted here that uri is released, so use a copy */
				afb_stub_ws_client_robustify(stubws, reopen_client, strdup(uri), free);
#endif
								AKA_mark("lis===90###sois===2661###eois===2670###lif===31###soif===1003###eoif===1012###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");return 0;

			}
			else {AKA_mark("lis===-82-###sois===-2312-###eois===-231248-###lif===-23-###soif===-###eoif===-702-###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");}

						AKA_mark("lis===92###sois===2679###eois===2743###lif===33###soif===1021###eoif===1085###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");ERROR("can't add the client to the apiset for service %s", uri);

						AKA_mark("lis===93###sois===2747###eois===2773###lif===34###soif===1089###eoif===1115###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");afb_stub_ws_unref(stubws);

		}

	}
	else {AKA_mark("lis===-75-###sois===-2104-###eois===-21044-###lif===-16-###soif===-###eoif===-450-###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");}

	error:
	AKA_mark("lis===97###sois===2789###eois===2806###lif===38###soif===1131###eoif===1148###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client(const char*,struct afb_apiset*,struct afb_apiset*,int)");return -!!strong;

}

/** Instrumented function afb_api_ws_add_client_strong(const char*,struct afb_apiset*,struct afb_apiset*) */
int afb_api_ws_add_client_strong(const char *uri, struct afb_apiset *declare_set, struct afb_apiset *call_set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client_strong(const char*,struct afb_apiset*,struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===102###sois===2924###eois===2984###lif===2###soif===114###eoif===174###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client_strong(const char*,struct afb_apiset*,struct afb_apiset*)");return afb_api_ws_add_client(uri, declare_set, call_set, 1);

}

/** Instrumented function afb_api_ws_add_client_weak(const char*,struct afb_apiset*,struct afb_apiset*) */
int afb_api_ws_add_client_weak(const char *uri, struct afb_apiset *declare_set, struct afb_apiset *call_set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client_weak(const char*,struct afb_apiset*,struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===107###sois===3100###eois===3160###lif===2###soif===112###eoif===172###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_client_weak(const char*,struct afb_apiset*,struct afb_apiset*)");return afb_api_ws_add_client(uri, declare_set, call_set, 0);

}

/*****************************************************************************/
/***       S E R V E R                                                      ***/
/******************************************************************************/

/** Instrumented function api_ws_server_accept(struct api_ws_server*) */
static void api_ws_server_accept(struct api_ws_server *apiws)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");AKA_fCall++;
		AKA_mark("lis===116###sois===3472###eois===3479###lif===2###soif===65###eoif===72###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");int fd;

		AKA_mark("lis===117###sois===3481###eois===3502###lif===3###soif===74###eoif===95###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");struct sockaddr addr;

		AKA_mark("lis===118###sois===3504###eois===3522###lif===4###soif===97###eoif===115###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");socklen_t lenaddr;

		AKA_mark("lis===119###sois===3524###eois===3542###lif===5###soif===117###eoif===135###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");struct fdev *fdev;

		AKA_mark("lis===120###sois===3544###eois===3571###lif===6###soif===137###eoif===164###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");struct afb_stub_ws *server;


		AKA_mark("lis===122###sois===3574###eois===3607###lif===8###soif===167###eoif===200###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");lenaddr = (socklen_t)sizeof addr;

		AKA_mark("lis===123###sois===3609###eois===3660###lif===9###soif===202###eoif===253###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");fd = accept(fdev_fd(apiws->fdev), &addr, &lenaddr);

		if (AKA_mark("lis===124###sois===3666###eois===3672###lif===10###soif===259###eoif===265###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)") && (AKA_mark("lis===124###sois===3666###eois===3672###lif===10###soif===259###eoif===265###isc===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)")&&fd < 0)) {
				AKA_mark("lis===125###sois===3678###eois===3733###lif===11###soif===271###eoif===326###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");ERROR("can't accept connection to %s: %m", apiws->uri);

	}
	else {
				AKA_mark("lis===127###sois===3746###eois===3773###lif===13###soif===339###eoif===366###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");fdev = afb_fdev_create(fd);

				if (AKA_mark("lis===128###sois===3780###eois===3785###lif===14###soif===373###eoif===378###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)") && (AKA_mark("lis===128###sois===3780###eois===3785###lif===14###soif===373###eoif===378###isc===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)")&&!fdev)) {
						AKA_mark("lis===129###sois===3792###eois===3854###lif===15###soif===385###eoif===447###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");ERROR("can't hold accepted connection to %s: %m", apiws->uri);

						AKA_mark("lis===130###sois===3858###eois===3868###lif===16###soif===451###eoif===461###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");close(fd);

		}
		else {
						AKA_mark("lis===132###sois===3883###eois===3967###lif===18###soif===476###eoif===560###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");server = afb_stub_ws_create_server(fdev, &apiws->uri[apiws->offapi], apiws->apiset);

						if (AKA_mark("lis===133###sois===3975###eois===3981###lif===19###soif===568###eoif===574###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)") && (AKA_mark("lis===133###sois===3975###eois===3981###lif===19###soif===568###eoif===574###isc===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)")&&server)) {
				AKA_mark("lis===134###sois===3987###eois===4040###lif===20###soif===580###eoif===633###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");afb_stub_ws_set_on_hangup(server, afb_stub_ws_unref);
			}
			else {
				AKA_mark("lis===136###sois===4053###eois===4116###lif===22###soif===646###eoif===709###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_accept(struct api_ws_server*)");ERROR("can't serve accepted connection to %s: %m", apiws->uri);
			}

		}

	}

}

static int api_ws_server_connect(struct api_ws_server *apiws);

/** Instrumented function api_ws_server_listen_callback(void*,uint32_t,struct fdev*) */
static void api_ws_server_listen_callback(void *closure, uint32_t revents, struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-ws.c/api_ws_server_listen_callback(void*,uint32_t,struct fdev*)");AKA_fCall++;
		AKA_mark("lis===145###sois===4288###eois===4326###lif===2###soif===97###eoif===135###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_listen_callback(void*,uint32_t,struct fdev*)");struct api_ws_server *apiws = closure;


		if (AKA_mark("lis===147###sois===4333###eois===4358###lif===4###soif===142###eoif===167###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_listen_callback(void*,uint32_t,struct fdev*)") && (AKA_mark("lis===147###sois===4333###eois===4358###lif===4###soif===142###eoif===167###isc===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_listen_callback(void*,uint32_t,struct fdev*)")&&(revents & EPOLLHUP) != 0)) {
		AKA_mark("lis===148###sois===4362###eois===4391###lif===5###soif===171###eoif===200###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_listen_callback(void*,uint32_t,struct fdev*)");api_ws_server_connect(apiws);
	}
	else {
		if (AKA_mark("lis===149###sois===4402###eois===4426###lif===6###soif===211###eoif===235###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_listen_callback(void*,uint32_t,struct fdev*)") && (AKA_mark("lis===149###sois===4402###eois===4426###lif===6###soif===211###eoif===235###isc===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_listen_callback(void*,uint32_t,struct fdev*)")&&(revents & EPOLLIN) != 0)) {
			AKA_mark("lis===150###sois===4430###eois===4458###lif===7###soif===239###eoif===267###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_listen_callback(void*,uint32_t,struct fdev*)");api_ws_server_accept(apiws);
		}
		else {AKA_mark("lis===-149-###sois===-4402-###eois===-440224-###lif===-6-###soif===-###eoif===-235-###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_listen_callback(void*,uint32_t,struct fdev*)");}
	}

}

/** Instrumented function api_ws_server_disconnect(struct api_ws_server*) */
static void api_ws_server_disconnect(struct api_ws_server *apiws)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-ws.c/api_ws_server_disconnect(struct api_ws_server*)");AKA_fCall++;
		AKA_mark("lis===155###sois===4531###eois===4555###lif===2###soif===69###eoif===93###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_disconnect(struct api_ws_server*)");fdev_unref(apiws->fdev);

		AKA_mark("lis===156###sois===4557###eois===4573###lif===3###soif===95###eoif===111###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_disconnect(struct api_ws_server*)");apiws->fdev = 0;

}

/** Instrumented function api_ws_server_connect(struct api_ws_server*) */
static int api_ws_server_connect(struct api_ws_server *apiws)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-ws.c/api_ws_server_connect(struct api_ws_server*)");AKA_fCall++;
	/* ensure disconnected */
		AKA_mark("lis===162###sois===4669###eois===4701###lif===3###soif===92###eoif===124###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_connect(struct api_ws_server*)");api_ws_server_disconnect(apiws);


	/* request the service object name */
		AKA_mark("lis===165###sois===4743###eois===4793###lif===6###soif===166###eoif===216###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_connect(struct api_ws_server*)");apiws->fdev = afb_socket_open_fdev(apiws->uri, 1);

		if (AKA_mark("lis===166###sois===4799###eois===4811###lif===7###soif===222###eoif===234###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_connect(struct api_ws_server*)") && (AKA_mark("lis===166###sois===4799###eois===4811###lif===7###soif===222###eoif===234###isc===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_connect(struct api_ws_server*)")&&!apiws->fdev)) {
		AKA_mark("lis===167###sois===4815###eois===4859###lif===8###soif===238###eoif===282###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_connect(struct api_ws_server*)");ERROR("can't create socket %s", apiws->uri);
	}
	else {
		/* listen for service */
				AKA_mark("lis===170###sois===4897###eois===4935###lif===11###soif===320###eoif===358###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_connect(struct api_ws_server*)");fdev_set_events(apiws->fdev, EPOLLIN);

				AKA_mark("lis===171###sois===4938###eois===5007###lif===12###soif===361###eoif===430###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_connect(struct api_ws_server*)");fdev_set_callback(apiws->fdev, api_ws_server_listen_callback, apiws);

				AKA_mark("lis===172###sois===5010###eois===5019###lif===13###soif===433###eoif===442###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_connect(struct api_ws_server*)");return 0;

	}

		AKA_mark("lis===174###sois===5024###eois===5034###lif===15###soif===447###eoif===457###ins===true###function===./app-framework-binder/src/afb-api-ws.c/api_ws_server_connect(struct api_ws_server*)");return -1;

}

/* create the service */
/** Instrumented function afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*) */
int afb_api_ws_add_server(const char *uri, struct afb_apiset *declare_set, struct afb_apiset *call_set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===180###sois===5170###eois===5177###lif===2###soif===107###eoif===114###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");int rc;

		AKA_mark("lis===181###sois===5179###eois===5195###lif===3###soif===116###eoif===132###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");const char *api;

		AKA_mark("lis===182###sois===5197###eois===5225###lif===4###soif===134###eoif===162###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");struct api_ws_server *apiws;

		AKA_mark("lis===183###sois===5227###eois===5252###lif===5###soif===164###eoif===189###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");size_t luri, lapi, extra;


	/* check the size */
		AKA_mark("lis===186###sois===5277###eois===5296###lif===8###soif===214###eoif===233###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");luri = strlen(uri);

		if (AKA_mark("lis===187###sois===5302###eois===5313###lif===9###soif===239###eoif===250###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)") && (AKA_mark("lis===187###sois===5302###eois===5313###lif===9###soif===239###eoif===250###isc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)")&&luri > 4000)) {
				AKA_mark("lis===188###sois===5319###eois===5356###lif===10###soif===256###eoif===293###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");ERROR("can't create socket %s", uri);

				AKA_mark("lis===189###sois===5359###eois===5373###lif===11###soif===296###eoif===310###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");errno = E2BIG;

				AKA_mark("lis===190###sois===5376###eois===5386###lif===12###soif===313###eoif===323###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");return -1;

	}
	else {AKA_mark("lis===-187-###sois===-5302-###eois===-530211-###lif===-9-###soif===-###eoif===-250-###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");}


	/* check the api name */
		AKA_mark("lis===194###sois===5418###eois===5444###lif===16###soif===355###eoif===381###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");api = afb_socket_api(uri);

		if (AKA_mark("lis===195###sois===5450###eois===5492###lif===17###soif===387###eoif===429###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)") && ((AKA_mark("lis===195###sois===5450###eois===5461###lif===17###soif===387###eoif===398###isc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)")&&api == NULL)	||(AKA_mark("lis===195###sois===5465###eois===5492###lif===17###soif===402###eoif===429###isc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)")&&!afb_api_is_valid_name(api)))) {
				AKA_mark("lis===196###sois===5498###eois===5542###lif===18###soif===435###eoif===479###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");ERROR("invalid api name in ws uri %s", uri);

				AKA_mark("lis===197###sois===5545###eois===5560###lif===19###soif===482###eoif===497###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");errno = EINVAL;

				AKA_mark("lis===198###sois===5563###eois===5574###lif===20###soif===500###eoif===511###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");goto error;

	}
	else {AKA_mark("lis===-195-###sois===-5450-###eois===-545042-###lif===-17-###soif===-###eoif===-429-###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");}


	/* check api name */
		if (AKA_mark("lis===202###sois===5606###eois===5642###lif===24###soif===543###eoif===579###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)") && (AKA_mark("lis===202###sois===5606###eois===5642###lif===24###soif===543###eoif===579###isc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)")&&!afb_apiset_lookup(call_set, api, 1))) {
				AKA_mark("lis===203###sois===5648###eois===5724###lif===25###soif===585###eoif===661###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");ERROR("Can't provide ws-server for URI %s: API %s doesn't exist", uri, api);

				AKA_mark("lis===204###sois===5727###eois===5742###lif===26###soif===664###eoif===679###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");errno = ENOENT;

				AKA_mark("lis===205###sois===5745###eois===5756###lif===27###soif===682###eoif===693###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");goto error;

	}
	else {AKA_mark("lis===-202-###sois===-5606-###eois===-560636-###lif===-24-###soif===-###eoif===-579-###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");}


	/* make the structure */
		AKA_mark("lis===209###sois===5788###eois===5807###lif===31###soif===725###eoif===744###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");lapi = strlen(api);

		AKA_mark("lis===210###sois===5809###eois===5859###lif===32###soif===746###eoif===796###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");extra = luri == (api - uri) + lapi ? 0 : lapi + 1;

		AKA_mark("lis===211###sois===5861###eois===5911###lif===33###soif===798###eoif===848###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");apiws = malloc(sizeof * apiws + 1 + luri + extra);

		if (AKA_mark("lis===212###sois===5917###eois===5923###lif===34###soif===854###eoif===860###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)") && (AKA_mark("lis===212###sois===5917###eois===5923###lif===34###soif===854###eoif===860###isc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)")&&!apiws)) {
				AKA_mark("lis===213###sois===5929###eois===5952###lif===35###soif===866###eoif===889###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");ERROR("out of memory");

				AKA_mark("lis===214###sois===5955###eois===5970###lif===36###soif===892###eoif===907###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");errno = ENOMEM;

				AKA_mark("lis===215###sois===5973###eois===5984###lif===37###soif===910###eoif===921###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");goto error;

	}
	else {AKA_mark("lis===-212-###sois===-5917-###eois===-59176-###lif===-34-###soif===-###eoif===-860-###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");}


		AKA_mark("lis===218###sois===5990###eois===6034###lif===40###soif===927###eoif===971###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");apiws->apiset = afb_apiset_addref(call_set);

		AKA_mark("lis===219###sois===6036###eois===6052###lif===41###soif===973###eoif===989###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");apiws->fdev = 0;

		AKA_mark("lis===220###sois===6054###eois===6078###lif===42###soif===991###eoif===1015###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");strcpy(apiws->uri, uri);

		if (AKA_mark("lis===221###sois===6084###eois===6090###lif===43###soif===1021###eoif===1027###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)") && (AKA_mark("lis===221###sois===6084###eois===6090###lif===43###soif===1021###eoif===1027###isc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)")&&!extra)) {
		AKA_mark("lis===222###sois===6094###eois===6132###lif===44###soif===1031###eoif===1069###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");apiws->offapi = (uint16_t)(api - uri);
	}
	else {
				AKA_mark("lis===224###sois===6143###eois===6180###lif===46###soif===1080###eoif===1117###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");apiws->offapi = (uint16_t)(luri + 1);

				AKA_mark("lis===225###sois===6183###eois===6223###lif===47###soif===1120###eoif===1160###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");strcpy(&apiws->uri[apiws->offapi], api);

	}


	/* connect for serving */
		AKA_mark("lis===229###sois===6256###eois===6290###lif===51###soif===1193###eoif===1227###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");rc = api_ws_server_connect(apiws);

		if (AKA_mark("lis===230###sois===6296###eois===6303###lif===52###soif===1233###eoif===1240###ifc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)") && (AKA_mark("lis===230###sois===6296###eois===6303###lif===52###soif===1233###eoif===1240###isc===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)")&&rc >= 0)) {
		AKA_mark("lis===231###sois===6307###eois===6316###lif===53###soif===1244###eoif===1253###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");return 0;
	}
	else {AKA_mark("lis===-230-###sois===-6296-###eois===-62967-###lif===-52-###soif===-###eoif===-1240-###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");}


		AKA_mark("lis===233###sois===6319###eois===6351###lif===55###soif===1256###eoif===1288###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");afb_apiset_unref(apiws->apiset);

		AKA_mark("lis===234###sois===6353###eois===6365###lif===56###soif===1290###eoif===1302###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");free(apiws);

	error:
	AKA_mark("lis===236###sois===6374###eois===6384###lif===58###soif===1311###eoif===1321###ins===true###function===./app-framework-binder/src/afb-api-ws.c/afb_api_ws_add_server(const char*,struct afb_apiset*,struct afb_apiset*)");return -1;

}

#endif

