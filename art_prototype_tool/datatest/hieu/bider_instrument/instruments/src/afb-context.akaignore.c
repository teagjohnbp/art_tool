/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_CONTEXT_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_CONTEXT_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author "Fulup Ar Foll"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SESSION_H_
#define AKA_INCLUDE__AFB_SESSION_H_
#include "afb-session.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CONTEXT_H_
#define AKA_INCLUDE__AFB_CONTEXT_H_
#include "afb-context.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_TOKEN_H_
#define AKA_INCLUDE__AFB_TOKEN_H_
#include "afb-token.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CRED_H_
#define AKA_INCLUDE__AFB_CRED_H_
#include "afb-cred.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_PERM_H_
#define AKA_INCLUDE__AFB_PERM_H_
#include "afb-perm.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_PERMISSION_TEXT_H_
#define AKA_INCLUDE__AFB_PERMISSION_TEXT_H_
#include "afb-permission-text.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif


/** Instrumented function init_context(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*) */
static void init_context(struct afb_context *context, struct afb_session *session, struct afb_token *token, struct afb_cred *cred)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/init_context(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*)");AKA_fCall++;
		AKA_mark("lis===36###sois===1076###eois===1100###lif===2###soif===134###eoif===158###ins===true###function===./app-framework-binder/src/afb-context.c/init_context(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*)");assert(session != NULL);


	/* reset the context for the session */
		AKA_mark("lis===39###sois===1144###eois===1171###lif===5###soif===202###eoif===229###ins===true###function===./app-framework-binder/src/afb-context.c/init_context(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*)");context->session = session;

		AKA_mark("lis===40###sois===1173###eois===1192###lif===6###soif===231###eoif===250###ins===true###function===./app-framework-binder/src/afb-context.c/init_context(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*)");context->flags = 0;

		AKA_mark("lis===41###sois===1194###eois===1216###lif===7###soif===252###eoif===274###ins===true###function===./app-framework-binder/src/afb-context.c/init_context(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*)");context->super = NULL;

		AKA_mark("lis===42###sois===1218###eois===1242###lif===8###soif===276###eoif===300###ins===true###function===./app-framework-binder/src/afb-context.c/init_context(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*)");context->api_key = NULL;

		AKA_mark("lis===43###sois===1244###eois===1285###lif===9###soif===302###eoif===343###ins===true###function===./app-framework-binder/src/afb-context.c/init_context(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*)");context->token = afb_token_addref(token);

		AKA_mark("lis===44###sois===1287###eois===1332###lif===10###soif===345###eoif===390###ins===true###function===./app-framework-binder/src/afb-context.c/init_context(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*)");context->credentials = afb_cred_addref(cred);

}

/** Instrumented function afb_context_subinit(struct afb_context*,struct afb_context*) */
void afb_context_subinit(struct afb_context *context, struct afb_context *super)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_subinit(struct afb_context*,struct afb_context*)");AKA_fCall++;
		AKA_mark("lis===49###sois===1420###eois===1474###lif===2###soif===84###eoif===138###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_subinit(struct afb_context*,struct afb_context*)");context->session = afb_session_addref(super->session);

		AKA_mark("lis===50###sois===1476###eois===1495###lif===3###soif===140###eoif===159###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_subinit(struct afb_context*,struct afb_context*)");context->flags = 0;

		AKA_mark("lis===51###sois===1497###eois===1520###lif===4###soif===161###eoif===184###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_subinit(struct afb_context*,struct afb_context*)");context->super = super;

		AKA_mark("lis===52###sois===1522###eois===1546###lif===5###soif===186###eoif===210###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_subinit(struct afb_context*,struct afb_context*)");context->api_key = NULL;

		AKA_mark("lis===53###sois===1548###eois===1596###lif===6###soif===212###eoif===260###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_subinit(struct afb_context*,struct afb_context*)");context->token = afb_token_addref(super->token);

		AKA_mark("lis===54###sois===1598###eois===1657###lif===7###soif===262###eoif===321###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_subinit(struct afb_context*,struct afb_context*)");context->credentials = afb_cred_addref(super->credentials);

}

/** Instrumented function afb_context_init(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*) */
void afb_context_init(struct afb_context *context, struct afb_session *session, struct afb_token *token, struct afb_cred *cred)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_init(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*)");AKA_fCall++;
		AKA_mark("lis===59###sois===1792###eois===1856###lif===2###soif===131###eoif===195###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_init(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*)");init_context(context, afb_session_addref(session), token, cred);

}

/** Instrumented function afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*) */
int afb_context_connect(struct afb_context *context, const char *uuid, struct afb_token *token, struct afb_cred *cred)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");AKA_fCall++;
		AKA_mark("lis===64###sois===1982###eois===1994###lif===2###soif===122###eoif===134###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");int created;

		AKA_mark("lis===65###sois===1996###eois===2024###lif===3###soif===136###eoif===164###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");struct afb_session *session;


		AKA_mark("lis===67###sois===2027###eois===2099###lif===5###soif===167###eoif===239###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");session = afb_session_get (uuid, AFB_SESSION_TIMEOUT_DEFAULT, &created);

		if (AKA_mark("lis===68###sois===2105###eois===2120###lif===6###soif===245###eoif===260###ifc===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)") && (AKA_mark("lis===68###sois===2105###eois===2120###lif===6###soif===245###eoif===260###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)")&&session == NULL)) {
		AKA_mark("lis===69###sois===2124###eois===2134###lif===7###soif===264###eoif===274###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");return -1;
	}
	else {AKA_mark("lis===-68-###sois===-2105-###eois===-210515-###lif===-6-###soif===-###eoif===-260-###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");}

		AKA_mark("lis===70###sois===2136###eois===2180###lif===8###soif===276###eoif===320###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");init_context(context, session, token, cred);

		if (AKA_mark("lis===71###sois===2186###eois===2193###lif===9###soif===326###eoif===333###ifc===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)") && (AKA_mark("lis===71###sois===2186###eois===2193###lif===9###soif===326###eoif===333###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)")&&created)) {
				AKA_mark("lis===72###sois===2199###eois===2220###lif===10###soif===339###eoif===360###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");context->created = 1;

	}
	else {AKA_mark("lis===-71-###sois===-2186-###eois===-21867-###lif===-9-###soif===-###eoif===-333-###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");}

		AKA_mark("lis===74###sois===2225###eois===2234###lif===12###soif===365###eoif===374###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");return 0;

}

/** Instrumented function afb_context_connect_validated(struct afb_context*,const char*,struct afb_token*,struct afb_cred*) */
int afb_context_connect_validated(struct afb_context *context, const char *uuid, struct afb_token *token, struct afb_cred *cred)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_connect_validated(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");AKA_fCall++;
		AKA_mark("lis===79###sois===2370###eois===2427###lif===2###soif===132###eoif===189###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect_validated(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");int rc = afb_context_connect(context, uuid, token, cred);

		if (AKA_mark("lis===80###sois===2433###eois===2436###lif===3###soif===195###eoif===198###ifc===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect_validated(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)") && (AKA_mark("lis===80###sois===2433###eois===2436###lif===3###soif===195###eoif===198###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect_validated(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)")&&!rc)) {
		AKA_mark("lis===81###sois===2440###eois===2463###lif===4###soif===202###eoif===225###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect_validated(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");context->validated = 1;
	}
	else {AKA_mark("lis===-80-###sois===-2433-###eois===-24333-###lif===-3-###soif===-###eoif===-198-###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect_validated(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");}

		AKA_mark("lis===82###sois===2465###eois===2475###lif===5###soif===227###eoif===237###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_connect_validated(struct afb_context*,const char*,struct afb_token*,struct afb_cred*)");return rc;

}

/** Instrumented function afb_context_init_validated(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*) */
void afb_context_init_validated(struct afb_context *context, struct afb_session *session, struct afb_token *token, struct afb_cred *cred)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_init_validated(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*)");AKA_fCall++;
		AKA_mark("lis===87###sois===2620###eois===2668###lif===2###soif===141###eoif===189###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_init_validated(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*)");afb_context_init(context, session, token, cred);

		AKA_mark("lis===88###sois===2670###eois===2693###lif===3###soif===191###eoif===214###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_init_validated(struct afb_context*,struct afb_session*,struct afb_token*,struct afb_cred*)");context->validated = 1;

}

/** Instrumented function afb_context_disconnect(struct afb_context*) */
void afb_context_disconnect(struct afb_context *context)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)");AKA_fCall++;
		if (AKA_mark("lis===93###sois===2761###eois===2836###lif===2###soif===64###eoif===139###ifc===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)") && ((((AKA_mark("lis===93###sois===2761###eois===2777###lif===2###soif===64###eoif===80###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)")&&context->session)	&&(AKA_mark("lis===93###sois===2781###eois===2796###lif===2###soif===84###eoif===99###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)")&&!context->super))	&&(AKA_mark("lis===93###sois===2800###eois===2816###lif===2###soif===103###eoif===119###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)")&&context->closing))	&&(AKA_mark("lis===93###sois===2820###eois===2836###lif===2###soif===123###eoif===139###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)")&&!context->closed))) {
				AKA_mark("lis===94###sois===2842###eois===2877###lif===3###soif===145###eoif===180###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)");afb_context_change_loa(context, 0);

				AKA_mark("lis===95###sois===2880###eois===2917###lif===4###soif===183###eoif===220###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)");afb_context_set(context, NULL, NULL);

				AKA_mark("lis===96###sois===2920###eois===2940###lif===5###soif===223###eoif===243###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)");context->closed = 1;

	}
	else {AKA_mark("lis===-93-###sois===-2761-###eois===-276175-###lif===-2-###soif===-###eoif===-139-###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)");}

		AKA_mark("lis===98###sois===2945###eois===2981###lif===7###soif===248###eoif===284###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)");afb_session_unref(context->session);

		AKA_mark("lis===99###sois===2983###eois===3007###lif===8###soif===286###eoif===310###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)");context->session = NULL;

		AKA_mark("lis===100###sois===3009###eois===3046###lif===9###soif===312###eoif===349###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)");afb_cred_unref(context->credentials);

		AKA_mark("lis===101###sois===3048###eois===3076###lif===10###soif===351###eoif===379###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)");context->credentials = NULL;

		AKA_mark("lis===102###sois===3078###eois===3110###lif===11###soif===381###eoif===413###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)");afb_token_unref(context->token);

		AKA_mark("lis===103###sois===3112###eois===3134###lif===12###soif===415###eoif===437###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_disconnect(struct afb_context*)");context->token = NULL;

}

/** Instrumented function afb_context_change_cred(struct afb_context*,struct afb_cred*) */
void afb_context_change_cred(struct afb_context *context, struct afb_cred *cred)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_change_cred(struct afb_context*,struct afb_cred*)");AKA_fCall++;
		AKA_mark("lis===108###sois===3222###eois===3268###lif===2###soif===84###eoif===130###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_cred(struct afb_context*,struct afb_cred*)");struct afb_cred *ocred = context->credentials;

		if (AKA_mark("lis===109###sois===3274###eois===3287###lif===3###soif===136###eoif===149###ifc===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_cred(struct afb_context*,struct afb_cred*)") && (AKA_mark("lis===109###sois===3274###eois===3287###lif===3###soif===136###eoif===149###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_cred(struct afb_context*,struct afb_cred*)")&&ocred != cred)) {
				AKA_mark("lis===110###sois===3293###eois===3338###lif===4###soif===155###eoif===200###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_cred(struct afb_context*,struct afb_cred*)");context->credentials = afb_cred_addref(cred);

				AKA_mark("lis===111###sois===3341###eois===3363###lif===5###soif===203###eoif===225###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_cred(struct afb_context*,struct afb_cred*)");afb_cred_unref(ocred);

	}
	else {AKA_mark("lis===-109-###sois===-3274-###eois===-327413-###lif===-3-###soif===-###eoif===-149-###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_cred(struct afb_context*,struct afb_cred*)");}

}

/** Instrumented function afb_context_change_token(struct afb_context*,struct afb_token*) */
void afb_context_change_token(struct afb_context *context, struct afb_token *token)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_change_token(struct afb_context*,struct afb_token*)");AKA_fCall++;
		AKA_mark("lis===117###sois===3457###eois===3499###lif===2###soif===87###eoif===129###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_token(struct afb_context*,struct afb_token*)");struct afb_token *otoken = context->token;

		if (AKA_mark("lis===118###sois===3505###eois===3520###lif===3###soif===135###eoif===150###ifc===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_token(struct afb_context*,struct afb_token*)") && (AKA_mark("lis===118###sois===3505###eois===3520###lif===3###soif===135###eoif===150###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_token(struct afb_context*,struct afb_token*)")&&otoken != token)) {
				AKA_mark("lis===119###sois===3526###eois===3567###lif===4###soif===156###eoif===197###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_token(struct afb_context*,struct afb_token*)");context->token = afb_token_addref(token);

				AKA_mark("lis===120###sois===3570###eois===3594###lif===5###soif===200###eoif===224###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_token(struct afb_context*,struct afb_token*)");afb_token_unref(otoken);

	}
	else {AKA_mark("lis===-118-###sois===-3505-###eois===-350515-###lif===-3-###soif===-###eoif===-150-###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_token(struct afb_context*,struct afb_token*)");}

}

/** Instrumented function afb_context_on_behalf_export(struct afb_context*) */
const char *afb_context_on_behalf_export(struct afb_context *context)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_on_behalf_export(struct afb_context*)");AKA_fCall++;
		AKA_mark("lis===126###sois===3674###eois===3749###lif===2###soif===73###eoif===148###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_export(struct afb_context*)");return context->credentials ? afb_cred_export(context->credentials) : NULL;

}

/** Instrumented function afb_context_on_behalf_import(struct afb_context*,const char*) */
int afb_context_on_behalf_import(struct afb_context *context, const char *exported)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)");AKA_fCall++;
		AKA_mark("lis===131###sois===3840###eois===3847###lif===2###soif===87###eoif===94###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)");int rc;

		AKA_mark("lis===132###sois===3849###eois===3883###lif===3###soif===96###eoif===130###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)");struct afb_cred *imported, *ocred;


		if (AKA_mark("lis===134###sois===3890###eois===3913###lif===5###soif===137###eoif===160###ifc===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)") && ((AKA_mark("lis===134###sois===3890###eois===3899###lif===5###soif===137###eoif===146###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)")&&!exported)	||(AKA_mark("lis===134###sois===3903###eois===3913###lif===5###soif===150###eoif===160###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)")&&!*exported))) {
		AKA_mark("lis===135###sois===3917###eois===3924###lif===6###soif===164###eoif===171###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)");rc = 0;
	}
	else {
				if (AKA_mark("lis===137###sois===3939###eois===4011###lif===8###soif===186###eoif===258###ifc===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)") && (AKA_mark("lis===137###sois===3939###eois===4011###lif===8###soif===186###eoif===258###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)")&&afb_context_has_permission(context, afb_permission_on_behalf_credential))) {
						AKA_mark("lis===138###sois===4018###eois===4055###lif===9###soif===265###eoif===302###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)");imported = afb_cred_import(exported);

						if (AKA_mark("lis===139###sois===4063###eois===4072###lif===10###soif===310###eoif===319###ifc===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)") && (AKA_mark("lis===139###sois===4063###eois===4072###lif===10###soif===310###eoif===319###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)")&&!imported)) {
								AKA_mark("lis===140###sois===4080###eois===4128###lif===11###soif===327###eoif===375###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)");ERROR("Can't import on behalf credentials: %m");

								AKA_mark("lis===141###sois===4133###eois===4141###lif===12###soif===380###eoif===388###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)");rc = -1;

			}
			else {
								AKA_mark("lis===143###sois===4158###eois===4187###lif===14###soif===405###eoif===434###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)");ocred = context->credentials;

								AKA_mark("lis===144###sois===4192###eois===4224###lif===15###soif===439###eoif===471###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)");context->credentials = imported;

								AKA_mark("lis===145###sois===4229###eois===4251###lif===16###soif===476###eoif===498###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)");afb_cred_unref(ocred);

								AKA_mark("lis===146###sois===4256###eois===4263###lif===17###soif===503###eoif===510###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)");rc = 0;

			}

		}
		else {
						AKA_mark("lis===149###sois===4283###eois===4322###lif===20###soif===530###eoif===569###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)");ERROR("On behalf credentials refused");

						AKA_mark("lis===150###sois===4326###eois===4334###lif===21###soif===573###eoif===581###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)");rc = -1;

		}

	}

		AKA_mark("lis===153###sois===4343###eois===4353###lif===24###soif===590###eoif===600###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_import(struct afb_context*,const char*)");return rc;

}

/** Instrumented function afb_context_on_behalf_other_context(struct afb_context*,struct afb_context*) */
void afb_context_on_behalf_other_context(struct afb_context *context, struct afb_context *other)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_on_behalf_other_context(struct afb_context*,struct afb_context*)");AKA_fCall++;
		AKA_mark("lis===158###sois===4457###eois===4510###lif===2###soif===100###eoif===153###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_other_context(struct afb_context*,struct afb_context*)");afb_context_change_cred(context, other->credentials);

		AKA_mark("lis===159###sois===4512###eois===4560###lif===3###soif===155###eoif===203###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_on_behalf_other_context(struct afb_context*,struct afb_context*)");afb_context_change_token(context, other->token);

}

/** Instrumented function afb_context_has_permission(struct afb_context*,const char*) */
int afb_context_has_permission(struct afb_context *context, const char *permission)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_has_permission(struct afb_context*,const char*)");AKA_fCall++;
		AKA_mark("lis===164###sois===4651###eois===4694###lif===2###soif===87###eoif===130###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_has_permission(struct afb_context*,const char*)");return afb_perm_check(context, permission);

}

/** Instrumented function afb_context_has_permission_async(struct afb_context*,const char*,void(*callback)(void*_closure, int _status),void*) */
void afb_context_has_permission_async(
	struct afb_context *context,
	const char *permission,
	void (*callback)(void *_closure, int _status),
	void *closure
)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_has_permission_async(struct afb_context*,const char*,void(*callback)(void*_closure, int _status),void*)");AKA_fCall++;
		AKA_mark("lis===174###sois===4860###eois===4928###lif===7###soif===162###eoif===230###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_has_permission_async(struct afb_context*,const char*,void(*callback)(void*_closure, int _status),void*)");return afb_perm_check_async(context, permission, callback, closure);

}

/** Instrumented function afb_context_uuid(struct afb_context*) */
const char *afb_context_uuid(struct afb_context *context)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_uuid(struct afb_context*)");AKA_fCall++;
		AKA_mark("lis===179###sois===4993###eois===5061###lif===2###soif===61###eoif===129###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_uuid(struct afb_context*)");return context->session ? afb_session_uuid(context->session) : NULL;

}

/** Instrumented function afb_context_make(struct afb_context*,int,void*(*make_value)(void*closure),void(*free_value)(void*item),void*) */
void *afb_context_make(struct afb_context *context, int replace, void *(*make_value)(void *closure), void (*free_value)(void *item), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_make(struct afb_context*,int,void*(*make_value)(void*closure),void(*free_value)(void*item),void*)");AKA_fCall++;
		AKA_mark("lis===184###sois===5216###eois===5249###lif===2###soif===151###eoif===184###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_make(struct afb_context*,int,void*(*make_value)(void*closure),void(*free_value)(void*item),void*)");assert(context->session != NULL);

		AKA_mark("lis===185###sois===5251###eois===5355###lif===3###soif===186###eoif===290###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_make(struct afb_context*,int,void*(*make_value)(void*closure),void(*free_value)(void*item),void*)");return afb_session_cookie(context->session, context->api_key, make_value, free_value, closure, replace);

}

/** Instrumented function afb_context_get(struct afb_context*) */
void *afb_context_get(struct afb_context *context)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_get(struct afb_context*)");AKA_fCall++;
		AKA_mark("lis===190###sois===5413###eois===5446###lif===2###soif===54###eoif===87###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_get(struct afb_context*)");assert(context->session != NULL);

		AKA_mark("lis===191###sois===5448###eois===5514###lif===3###soif===89###eoif===155###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_get(struct afb_context*)");return afb_session_get_cookie(context->session, context->api_key);

}

/** Instrumented function afb_context_set(struct afb_context*,void*,void(*free_value)(void*)) */
int afb_context_set(struct afb_context *context, void *value, void (*free_value)(void*))
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_set(struct afb_context*,void*,void(*free_value)(void*))");AKA_fCall++;
		AKA_mark("lis===196###sois===5610###eois===5643###lif===2###soif===92###eoif===125###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_set(struct afb_context*,void*,void(*free_value)(void*))");assert(context->session != NULL);

		AKA_mark("lis===197###sois===5645###eois===5730###lif===3###soif===127###eoif===212###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_set(struct afb_context*,void*,void(*free_value)(void*))");return afb_session_set_cookie(context->session, context->api_key, value, free_value);

}

/** Instrumented function afb_context_close(struct afb_context*) */
void afb_context_close(struct afb_context *context)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_close(struct afb_context*)");AKA_fCall++;
		AKA_mark("lis===202###sois===5789###eois===5810###lif===2###soif===55###eoif===76###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_close(struct afb_context*)");context->closing = 1;

}

struct chkctx {
	struct afb_context *context;
	void (*callback)(void *_closure, int _status);
	void *closure;
};

/** Instrumented function check_context_cb(void*,int) */
static void check_context_cb(void *closure_chkctx, int status)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/check_context_cb(void*,int)");AKA_fCall++;
		AKA_mark("lis===213###sois===5994###eois===6029###lif===2###soif===66###eoif===101###ins===true###function===./app-framework-binder/src/afb-context.c/check_context_cb(void*,int)");struct chkctx *cc = closure_chkctx;

		AKA_mark("lis===214###sois===6031###eois===6073###lif===3###soif===103###eoif===145###ins===true###function===./app-framework-binder/src/afb-context.c/check_context_cb(void*,int)");struct afb_context *context = cc->context;

		AKA_mark("lis===215###sois===6075###eois===6118###lif===4###soif===147###eoif===190###ins===true###function===./app-framework-binder/src/afb-context.c/check_context_cb(void*,int)");void (*callback)(void*,int) = cc->callback;

		AKA_mark("lis===216###sois===6120###eois===6148###lif===5###soif===192###eoif===220###ins===true###function===./app-framework-binder/src/afb-context.c/check_context_cb(void*,int)");void *closure = cc->closure;


		AKA_mark("lis===218###sois===6151###eois===6160###lif===7###soif===223###eoif===232###ins===true###function===./app-framework-binder/src/afb-context.c/check_context_cb(void*,int)");free(cc);

		if (AKA_mark("lis===219###sois===6166###eois===6172###lif===8###soif===238###eoif===244###ifc===true###function===./app-framework-binder/src/afb-context.c/check_context_cb(void*,int)") && (AKA_mark("lis===219###sois===6166###eois===6172###lif===8###soif===238###eoif===244###isc===true###function===./app-framework-binder/src/afb-context.c/check_context_cb(void*,int)")&&status)) {
		AKA_mark("lis===220###sois===6176###eois===6199###lif===9###soif===248###eoif===271###ins===true###function===./app-framework-binder/src/afb-context.c/check_context_cb(void*,int)");context->validated = 1;
	}
	else {
		AKA_mark("lis===222###sois===6208###eois===6233###lif===11###soif===280###eoif===305###ins===true###function===./app-framework-binder/src/afb-context.c/check_context_cb(void*,int)");context->invalidated = 1;
	}

		AKA_mark("lis===223###sois===6235###eois===6261###lif===12###soif===307###eoif===333###ins===true###function===./app-framework-binder/src/afb-context.c/check_context_cb(void*,int)");callback(closure, status);

}

/** Instrumented function check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*) */
static int check_context(
	struct afb_context *context,
	void (*callback)(void *_closure, int _status),
	void *closure
) {AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");AKA_fCall++;
		AKA_mark("lis===231###sois===6389###eois===6395###lif===5###soif===124###eoif===130###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");int r;

		AKA_mark("lis===232###sois===6397###eois===6415###lif===6###soif===132###eoif===150###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");struct chkctx *cc;


		if (AKA_mark("lis===234###sois===6422###eois===6440###lif===8###soif===157###eoif===175###ifc===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)") && (AKA_mark("lis===234###sois===6422###eois===6440###lif===8###soif===157###eoif===175###isc===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)")&&context->validated)) {
		AKA_mark("lis===235###sois===6444###eois===6450###lif===9###soif===179###eoif===185###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");r = 1;
	}
	else {
		if (AKA_mark("lis===236###sois===6461###eois===6481###lif===10###soif===196###eoif===216###ifc===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)") && (AKA_mark("lis===236###sois===6461###eois===6481###lif===10###soif===196###eoif===216###isc===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)")&&context->invalidated)) {
			AKA_mark("lis===237###sois===6485###eois===6491###lif===11###soif===220###eoif===226###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");r = 0;
		}
		else {
					if (AKA_mark("lis===239###sois===6506###eois===6520###lif===13###soif===241###eoif===255###ifc===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)") && (AKA_mark("lis===239###sois===6506###eois===6520###lif===13###soif===241###eoif===255###isc===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)")&&context->super)) {
				AKA_mark("lis===240###sois===6525###eois===6578###lif===14###soif===260###eoif===313###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");r = check_context(context->super, callback, closure);
			}
			else {
				if (AKA_mark("lis===241###sois===6590###eois===6599###lif===15###soif===325###eoif===334###ifc===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)") && (AKA_mark("lis===241###sois===6590###eois===6599###lif===15###soif===325###eoif===334###isc===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)")&&!callback)) {
					AKA_mark("lis===242###sois===6604###eois===6672###lif===16###soif===339###eoif===407###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");r = afb_context_has_permission(context, afb_permission_token_valid);
				}
				else {
								AKA_mark("lis===244###sois===6685###eois===6709###lif===18###soif===420###eoif===444###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");cc = malloc(sizeof *cc);

								if (AKA_mark("lis===245###sois===6717###eois===6719###lif===19###soif===452###eoif===454###ifc===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)") && (AKA_mark("lis===245###sois===6717###eois===6719###lif===19###soif===452###eoif===454###isc===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)")&&cc)) {
										AKA_mark("lis===246###sois===6727###eois===6749###lif===20###soif===462###eoif===484###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");cc->context = context;

										AKA_mark("lis===247###sois===6754###eois===6778###lif===21###soif===489###eoif===513###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");cc->callback = callback;

										AKA_mark("lis===248###sois===6783###eois===6805###lif===22###soif===518###eoif===540###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");cc->closure = closure;

										AKA_mark("lis===249###sois===6810###eois===6902###lif===23###soif===545###eoif===637###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");afb_context_has_permission_async(context, afb_permission_token_valid, check_context_cb, cc);

										AKA_mark("lis===250###sois===6907###eois===6917###lif===24###soif===642###eoif===652###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");return -1;

			}
					else {AKA_mark("lis===-245-###sois===-6717-###eois===-67172-###lif===-19-###soif===-###eoif===-454-###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");}

								AKA_mark("lis===252###sois===6926###eois===6949###lif===26###soif===661###eoif===684###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");ERROR("out-of-memory");

								AKA_mark("lis===253###sois===6953###eois===6959###lif===27###soif===688###eoif===694###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");r = 0;

		}
			}

					if (AKA_mark("lis===255###sois===6970###eois===6971###lif===29###soif===705###eoif===706###ifc===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)") && (AKA_mark("lis===255###sois===6970###eois===6971###lif===29###soif===705###eoif===706###isc===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)")&&r)) {
				AKA_mark("lis===256###sois===6976###eois===6999###lif===30###soif===711###eoif===734###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");context->validated = 1;
			}
			else {
				AKA_mark("lis===258###sois===7010###eois===7035###lif===32###soif===745###eoif===770###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");context->invalidated = 1;
			}

	}
	}

		AKA_mark("lis===260###sois===7040###eois===7049###lif===34###soif===775###eoif===784###ins===true###function===./app-framework-binder/src/afb-context.c/check_context(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");return r;

}

/** Instrumented function afb_context_check(struct afb_context*) */
int afb_context_check(struct afb_context *context)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_check(struct afb_context*)");AKA_fCall++;
		AKA_mark("lis===265###sois===7107###eois===7143###lif===2###soif===54###eoif===90###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_check(struct afb_context*)");return check_context(context, 0, 0);

}

/** Instrumented function afb_context_check_async(struct afb_context*,void(*callback)(void*_closure, int _status),void*) */
void afb_context_check_async(
	struct afb_context *context,
	void (*callback)(void *_closure, int _status),
	void *closure
) {AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_check_async(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");AKA_fCall++;
		AKA_mark("lis===273###sois===7275###eois===7325###lif===5###soif===128###eoif===178###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_check_async(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");int r = check_context(context, callback, closure);

		if (AKA_mark("lis===274###sois===7331###eois===7337###lif===6###soif===184###eoif===190###ifc===true###function===./app-framework-binder/src/afb-context.c/afb_context_check_async(struct afb_context*,void(*callback)(void*_closure, int _status),void*)") && (AKA_mark("lis===274###sois===7331###eois===7337###lif===6###soif===184###eoif===190###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_check_async(struct afb_context*,void(*callback)(void*_closure, int _status),void*)")&&r >= 0)) {
		AKA_mark("lis===275###sois===7341###eois===7362###lif===7###soif===194###eoif===215###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_check_async(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");callback(closure, r);
	}
	else {AKA_mark("lis===-274-###sois===-7331-###eois===-73316-###lif===-6-###soif===-###eoif===-190-###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_check_async(struct afb_context*,void(*callback)(void*_closure, int _status),void*)");}

}

/** Instrumented function loa_key(struct afb_context*) */
static inline const void *loa_key(struct afb_context *context)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/loa_key(struct afb_context*)");AKA_fCall++;
		AKA_mark("lis===280###sois===7432###eois===7485###lif===2###soif===66###eoif===119###ins===true###function===./app-framework-binder/src/afb-context.c/loa_key(struct afb_context*)");return (const void*)(1+(intptr_t)(context->api_key));

}

/** Instrumented function loa2ptr(unsigned) */
static inline void *loa2ptr(unsigned loa)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/loa2ptr(unsigned)");AKA_fCall++;
		AKA_mark("lis===285###sois===7534###eois===7562###lif===2###soif===45###eoif===73###ins===true###function===./app-framework-binder/src/afb-context.c/loa2ptr(unsigned)");return (void*)(intptr_t)loa;

}

/** Instrumented function ptr2loa(void*) */
static inline unsigned ptr2loa(void *ptr)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/ptr2loa(void*)");AKA_fCall++;
		AKA_mark("lis===290###sois===7611###eois===7642###lif===2###soif===45###eoif===76###ins===true###function===./app-framework-binder/src/afb-context.c/ptr2loa(void*)");return (unsigned)(intptr_t)ptr;

}

/** Instrumented function afb_context_change_loa(struct afb_context*,unsigned) */
int afb_context_change_loa(struct afb_context *context, unsigned loa)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_change_loa(struct afb_context*,unsigned)");AKA_fCall++;
		if (AKA_mark("lis===295###sois===7723###eois===7730###lif===2###soif===77###eoif===84###ifc===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_loa(struct afb_context*,unsigned)") && (AKA_mark("lis===295###sois===7723###eois===7730###lif===2###soif===77###eoif===84###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_loa(struct afb_context*,unsigned)")&&loa > 7)) {
				AKA_mark("lis===296###sois===7736###eois===7751###lif===3###soif===90###eoif===105###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_loa(struct afb_context*,unsigned)");errno = EINVAL;

				AKA_mark("lis===297###sois===7754###eois===7764###lif===4###soif===108###eoif===118###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_loa(struct afb_context*,unsigned)");return -1;

	}
	else {AKA_mark("lis===-295-###sois===-7723-###eois===-77237-###lif===-2-###soif===-###eoif===-84-###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_loa(struct afb_context*,unsigned)");}

		if (AKA_mark("lis===299###sois===7773###eois===7800###lif===6###soif===127###eoif===154###ifc===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_loa(struct afb_context*,unsigned)") && (AKA_mark("lis===299###sois===7773###eois===7800###lif===6###soif===127###eoif===154###isc===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_loa(struct afb_context*,unsigned)")&&!afb_context_check(context))) {
				AKA_mark("lis===300###sois===7806###eois===7820###lif===7###soif===160###eoif===174###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_loa(struct afb_context*,unsigned)");errno = EPERM;

				AKA_mark("lis===301###sois===7823###eois===7833###lif===8###soif===177###eoif===187###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_loa(struct afb_context*,unsigned)");return -1;

	}
	else {AKA_mark("lis===-299-###sois===-7773-###eois===-777327-###lif===-6-###soif===-###eoif===-154-###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_loa(struct afb_context*,unsigned)");}


		AKA_mark("lis===304###sois===7839###eois===7925###lif===11###soif===193###eoif===279###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_change_loa(struct afb_context*,unsigned)");return afb_session_set_cookie(context->session, loa_key(context), loa2ptr(loa), NULL);

}

/** Instrumented function afb_context_get_loa(struct afb_context*) */
unsigned afb_context_get_loa(struct afb_context *context)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_get_loa(struct afb_context*)");AKA_fCall++;
		AKA_mark("lis===309###sois===7990###eois===8023###lif===2###soif===61###eoif===94###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_get_loa(struct afb_context*)");assert(context->session != NULL);

		AKA_mark("lis===310###sois===8025###eois===8100###lif===3###soif===96###eoif===171###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_get_loa(struct afb_context*)");return ptr2loa(afb_session_get_cookie(context->session, loa_key(context)));

}

/** Instrumented function afb_context_check_loa(struct afb_context*,unsigned) */
int afb_context_check_loa(struct afb_context *context, unsigned loa)
{AKA_mark("Calling: ./app-framework-binder/src/afb-context.c/afb_context_check_loa(struct afb_context*,unsigned)");AKA_fCall++;
		AKA_mark("lis===315###sois===8176###eois===8219###lif===2###soif===72###eoif===115###ins===true###function===./app-framework-binder/src/afb-context.c/afb_context_check_loa(struct afb_context*,unsigned)");return afb_context_get_loa(context) >= loa;

}

#endif

