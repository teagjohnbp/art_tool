/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_SESSION_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_SESSION_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author "Fulup Ar Foll"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <string.h>
#include <errno.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SESSION_H_
#define AKA_INCLUDE__AFB_SESSION_H_
#include "afb-session.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HOOK_H_
#define AKA_INCLUDE__AFB_HOOK_H_
#include "afb-hook.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__PEARSON_H_
#define AKA_INCLUDE__PEARSON_H_
#include "pearson.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__UUID_H_
#define AKA_INCLUDE__UUID_H_
#include "uuid.akaignore.h"
#endif


#define SESSION_COUNT_MIN 5
#define SESSION_COUNT_MAX 1000

/*
 * Handling of  cookies.
 * Cookies are stored by session.
 * The cookie count COOKIECOUNT must be a power of 2, possible values: 1, 2, 4, 8, 16, 32, 64, ...
 * For low memory profile, small values are better, 1 is possible.
 */
#define COOKIECOUNT	8
#define COOKIEMASK	(COOKIECOUNT - 1)

#define _MAXEXP_	((time_t)(~(time_t)0))
#define _MAXEXP2_	((time_t)((((unsigned long long)_MAXEXP_) >> 1)))
#define MAX_EXPIRATION	(_MAXEXP_ >= 0 ? _MAXEXP_ : _MAXEXP2_)
#define NOW		(time_now())

/**
 * structure for a cookie added to sessions
 */
struct cookie
{
	struct cookie *next;	/**< link to next cookie */
	const void *key;	/**< pointer key */
	void *value;		/**< value */
	void (*freecb)(void*);	/**< function to call when session is closed */
};

/**
 * structure for session
 */
struct afb_session
{
	struct afb_session *next; /**< link to the next */
	uint16_t refcount;      /**< count of reference to the session */
	uint16_t id;		/**< local id of the session */
	int timeout;            /**< timeout of the session */
	time_t expiration;	/**< expiration time of the session */
	pthread_mutex_t mutex;  /**< mutex of the session */
	struct cookie *cookies[COOKIECOUNT]; /**< cookies of the session */
	char *lang;		/**< current language setting for the session */
	uint8_t closed: 1;      /**< is the session closed ? */
	uint8_t autoclose: 1;   /**< close the session when unreferenced */
	uint8_t notinset: 1;	/**< session removed from the set of sessions */
	uint8_t hash;		/**< hash value of the uuid */
	uuid_stringz_t uuid;	/**< identification of client session */
};

/**
 * structure for managing sessions
 */
static struct {
	uint16_t count;		/**< current number of sessions */
	uint16_t max;		/**< maximum count of sessions */
	uint16_t genid;		/**< for generating ids */
	int timeout;		/**< common initial timeout */
	struct afb_session *first; /**< sessions */
	pthread_mutex_t mutex;	/**< declare a mutex to protect hash table */
} sessions = {
	.count = 0,
	.max = 10,
	.genid = 1,
	.timeout = 3600,
	.first = 0,
	.mutex = PTHREAD_MUTEX_INITIALIZER
};

/**
 * Get the actual raw time
 */
/** Instrumented function time_now() */
static inline time_t time_now()
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/time_now()");AKA_fCall++;
		AKA_mark("lis===107###sois===3160###eois===3179###lif===2###soif===35###eoif===54###ins===true###function===./app-framework-binder/src/afb-session.c/time_now()");struct timespec ts;

		AKA_mark("lis===108###sois===3181###eois===3221###lif===3###soif===56###eoif===96###ins===true###function===./app-framework-binder/src/afb-session.c/time_now()");clock_gettime(CLOCK_MONOTONIC_RAW, &ts);

		AKA_mark("lis===109###sois===3223###eois===3240###lif===4###soif===98###eoif===115###ins===true###function===./app-framework-binder/src/afb-session.c/time_now()");return ts.tv_sec;

}

/* lock the set of sessions for exclusive access */
/** Instrumented function sessionset_lock() */
static inline void sessionset_lock()
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/sessionset_lock()");AKA_fCall++;
		AKA_mark("lis===115###sois===3336###eois===3372###lif===2###soif===40###eoif===76###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_lock()");pthread_mutex_lock(&sessions.mutex);

}

/* unlock the set of sessions of exclusive access */
/** Instrumented function sessionset_unlock() */
static inline void sessionset_unlock()
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/sessionset_unlock()");AKA_fCall++;
		AKA_mark("lis===121###sois===3471###eois===3509###lif===2###soif===42###eoif===80###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_unlock()");pthread_mutex_unlock(&sessions.mutex);

}

/*
 * search within the set of sessions the session of 'uuid'.
 * 'hashidx' is the precomputed hash for 'uuid'
 * return the session or NULL
 */
/** Instrumented function sessionset_search(const char*,uint8_t) */
static struct afb_session *sessionset_search(const char *uuid, uint8_t hashidx)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/sessionset_search(const char*,uint8_t)");AKA_fCall++;
		AKA_mark("lis===131###sois===3741###eois===3769###lif===2###soif===83###eoif===111###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_search(const char*,uint8_t)");struct afb_session *session;


		AKA_mark("lis===133###sois===3772###eois===3797###lif===4###soif===114###eoif===139###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_search(const char*,uint8_t)");session = sessions.first;

		while (AKA_mark("lis===134###sois===3806###eois===3872###lif===5###soif===148###eoif===214###ifc===true###function===./app-framework-binder/src/afb-session.c/sessionset_search(const char*,uint8_t)") && (((AKA_mark("lis===134###sois===3806###eois===3813###lif===5###soif===148###eoif===155###isc===true###function===./app-framework-binder/src/afb-session.c/sessionset_search(const char*,uint8_t)")&&session)	&&(AKA_mark("lis===134###sois===3817###eois===3841###lif===5###soif===159###eoif===183###isc===true###function===./app-framework-binder/src/afb-session.c/sessionset_search(const char*,uint8_t)")&&hashidx != session->hash))	&&(AKA_mark("lis===134###sois===3845###eois===3872###lif===5###soif===187###eoif===214###isc===true###function===./app-framework-binder/src/afb-session.c/sessionset_search(const char*,uint8_t)")&&strcmp(uuid, session->uuid)))) {
		AKA_mark("lis===135###sois===3876###eois===3900###lif===6###soif===218###eoif===242###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_search(const char*,uint8_t)");session = session->next;
	}


		AKA_mark("lis===137###sois===3903###eois===3918###lif===8###soif===245###eoif===260###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_search(const char*,uint8_t)");return session;

}

/*
 * search within the set of sessions the session of 'id'.
 * return the session or NULL
 */
/** Instrumented function sessionset_search_id(uint16_t) */
static struct afb_session *sessionset_search_id(uint16_t id)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/sessionset_search_id(uint16_t)");AKA_fCall++;
		AKA_mark("lis===146###sois===4081###eois===4109###lif===2###soif===64###eoif===92###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_search_id(uint16_t)");struct afb_session *session;


		AKA_mark("lis===148###sois===4112###eois===4137###lif===4###soif===95###eoif===120###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_search_id(uint16_t)");session = sessions.first;

		while (AKA_mark("lis===149###sois===4146###eois===4174###lif===5###soif===129###eoif===157###ifc===true###function===./app-framework-binder/src/afb-session.c/sessionset_search_id(uint16_t)") && ((AKA_mark("lis===149###sois===4146###eois===4153###lif===5###soif===129###eoif===136###isc===true###function===./app-framework-binder/src/afb-session.c/sessionset_search_id(uint16_t)")&&session)	&&(AKA_mark("lis===149###sois===4157###eois===4174###lif===5###soif===140###eoif===157###isc===true###function===./app-framework-binder/src/afb-session.c/sessionset_search_id(uint16_t)")&&id != session->id))) {
		AKA_mark("lis===150###sois===4178###eois===4202###lif===6###soif===161###eoif===185###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_search_id(uint16_t)");session = session->next;
	}


		AKA_mark("lis===152###sois===4205###eois===4220###lif===8###soif===188###eoif===203###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_search_id(uint16_t)");return session;

}

/* add 'session' to the set of sessions */
/** Instrumented function sessionset_add(struct afb_session*,uint8_t) */
static int sessionset_add(struct afb_session *session, uint8_t hashidx)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/sessionset_add(struct afb_session*,uint8_t)");AKA_fCall++;
	/* check availability */
		if (AKA_mark("lis===159###sois===4372###eois===4418###lif===3###soif===105###eoif===151###ifc===true###function===./app-framework-binder/src/afb-session.c/sessionset_add(struct afb_session*,uint8_t)") && ((AKA_mark("lis===159###sois===4372###eois===4384###lif===3###soif===105###eoif===117###isc===true###function===./app-framework-binder/src/afb-session.c/sessionset_add(struct afb_session*,uint8_t)")&&sessions.max)	&&(AKA_mark("lis===159###sois===4388###eois===4418###lif===3###soif===121###eoif===151###isc===true###function===./app-framework-binder/src/afb-session.c/sessionset_add(struct afb_session*,uint8_t)")&&sessions.count >= sessions.max))) {
				AKA_mark("lis===160###sois===4424###eois===4438###lif===4###soif===157###eoif===171###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_add(struct afb_session*,uint8_t)");errno = EBUSY;

				AKA_mark("lis===161###sois===4441###eois===4451###lif===5###soif===174###eoif===184###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_add(struct afb_session*,uint8_t)");return -1;

	}
	else {AKA_mark("lis===-159-###sois===-4372-###eois===-437246-###lif===-3-###soif===-###eoif===-151-###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_add(struct afb_session*,uint8_t)");}


	/* add the session */
		AKA_mark("lis===165###sois===4480###eois===4511###lif===9###soif===213###eoif===244###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_add(struct afb_session*,uint8_t)");session->next = sessions.first;

		AKA_mark("lis===166###sois===4513###eois===4538###lif===10###soif===246###eoif===271###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_add(struct afb_session*,uint8_t)");sessions.first = session;

		AKA_mark("lis===167###sois===4540###eois===4557###lif===11###soif===273###eoif===290###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_add(struct afb_session*,uint8_t)");sessions.count++;

		AKA_mark("lis===168###sois===4559###eois===4568###lif===12###soif===292###eoif===301###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_add(struct afb_session*,uint8_t)");return 0;

}

/* make a new uuid not used in the set of sessions */
/** Instrumented function sessionset_make_uuid(uuid_stringz_t) */
static uint8_t sessionset_make_uuid (uuid_stringz_t uuid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/sessionset_make_uuid(uuid_stringz_t)");AKA_fCall++;
		AKA_mark("lis===174###sois===4687###eois===4703###lif===2###soif===61###eoif===77###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_make_uuid(uuid_stringz_t)");uint8_t hashidx;


		do {
				AKA_mark("lis===177###sois===4713###eois===4736###lif===5###soif===87###eoif===110###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_make_uuid(uuid_stringz_t)");uuid_new_stringz(uuid);

				AKA_mark("lis===178###sois===4739###eois===4764###lif===6###soif===113###eoif===138###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_make_uuid(uuid_stringz_t)");hashidx = pearson4(uuid);

	}
	while (AKA_mark("lis===179###sois===4774###eois===4806###lif===7###soif===148###eoif===180###ifc===true###function===./app-framework-binder/src/afb-session.c/sessionset_make_uuid(uuid_stringz_t)") && (AKA_mark("lis===179###sois===4774###eois===4806###lif===7###soif===148###eoif===180###isc===true###function===./app-framework-binder/src/afb-session.c/sessionset_make_uuid(uuid_stringz_t)")&&sessionset_search(uuid, hashidx)));

		AKA_mark("lis===180###sois===4810###eois===4825###lif===8###soif===184###eoif===199###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_make_uuid(uuid_stringz_t)");return hashidx;

}

/* lock the 'session' for exclusive access */
/** Instrumented function session_lock(struct afb_session*) */
static inline void session_lock(struct afb_session *session)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/session_lock(struct afb_session*)");AKA_fCall++;
		AKA_mark("lis===186###sois===4939###eois===4975###lif===2###soif===64###eoif===100###ins===true###function===./app-framework-binder/src/afb-session.c/session_lock(struct afb_session*)");pthread_mutex_lock(&session->mutex);

}

/* unlock the 'session' of exclusive access */
/** Instrumented function session_unlock(struct afb_session*) */
static inline void session_unlock(struct afb_session *session)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/session_unlock(struct afb_session*)");AKA_fCall++;
		AKA_mark("lis===192###sois===5092###eois===5130###lif===2###soif===66###eoif===104###ins===true###function===./app-framework-binder/src/afb-session.c/session_unlock(struct afb_session*)");pthread_mutex_unlock(&session->mutex);

}

/* close the 'session' */
/** Instrumented function session_close(struct afb_session*) */
static void session_close(struct afb_session *session)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)");AKA_fCall++;
		AKA_mark("lis===198###sois===5218###eois===5226###lif===2###soif===58###eoif===66###ins===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)");int idx;

		AKA_mark("lis===199###sois===5228###eois===5250###lif===3###soif===68###eoif===90###ins===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)");struct cookie *cookie;


	/* close only one time */
		if (AKA_mark("lis===202###sois===5284###eois===5300###lif===6###soif===124###eoif===140###ifc===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)") && (AKA_mark("lis===202###sois===5284###eois===5300###lif===6###soif===124###eoif===140###isc===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)")&&!session->closed)) {
		/* close it now */
				AKA_mark("lis===204###sois===5327###eois===5347###lif===8###soif===167###eoif===187###ins===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)");session->closed = 1;


#if WITH_AFB_HOOK
		/* emit the hook */
		afb_hook_session_close(session);
#endif

		/* release cookies */
				AKA_mark("lis===212###sois===5463###eois===5472###lif===16###soif===303###eoif===312###ins===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)");for (idx = 0 ;AKA_mark("lis===212###sois===5473###eois===5490###lif===16###soif===313###eoif===330###ifc===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)") && AKA_mark("lis===212###sois===5473###eois===5490###lif===16###soif===313###eoif===330###isc===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)")&&idx < COOKIECOUNT;({AKA_mark("lis===212###sois===5493###eois===5498###lif===16###soif===333###eoif===338###ins===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)");idx++;})) {
						while (AKA_mark("lis===213###sois===5512###eois===5544###lif===17###soif===352###eoif===384###ifc===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)") && (cookie=(session->cookies[idx]) && AKA_mark("lis===213###sois===5513###eois===5543###lif===17###soif===353###eoif===383###isc===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)"))) {
								AKA_mark("lis===214###sois===5552###eois===5589###lif===18###soif===392###eoif===429###ins===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)");session->cookies[idx] = cookie->next;

								if (AKA_mark("lis===215###sois===5598###eois===5620###lif===19###soif===438###eoif===460###ifc===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)") && (AKA_mark("lis===215###sois===5598###eois===5620###lif===19###soif===438###eoif===460###isc===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)")&&cookie->freecb != NULL)) {
					AKA_mark("lis===216###sois===5627###eois===5657###lif===20###soif===467###eoif===497###ins===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)");cookie->freecb(cookie->value);
				}
				else {AKA_mark("lis===-215-###sois===-5598-###eois===-559822-###lif===-19-###soif===-###eoif===-460-###ins===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)");}

								AKA_mark("lis===217###sois===5662###eois===5675###lif===21###soif===502###eoif===515###ins===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)");free(cookie);

			}

		}

	}
	else {AKA_mark("lis===-202-###sois===-5284-###eois===-528416-###lif===-6-###soif===-###eoif===-140-###ins===true###function===./app-framework-binder/src/afb-session.c/session_close(struct afb_session*)");}

}

/* destroy the 'session' */
/** Instrumented function session_destroy(struct afb_session*) */
static void session_destroy (struct afb_session *session)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/session_destroy(struct afb_session*)");AKA_fCall++;
#if WITH_AFB_HOOK
	afb_hook_session_destroy(session);
#endif
		AKA_mark("lis===229###sois===5841###eois===5880###lif===5###soif===122###eoif===161###ins===true###function===./app-framework-binder/src/afb-session.c/session_destroy(struct afb_session*)");pthread_mutex_destroy(&session->mutex);

		AKA_mark("lis===230###sois===5882###eois===5902###lif===6###soif===163###eoif===183###ins===true###function===./app-framework-binder/src/afb-session.c/session_destroy(struct afb_session*)");free(session->lang);

		AKA_mark("lis===231###sois===5904###eois===5918###lif===7###soif===185###eoif===199###ins===true###function===./app-framework-binder/src/afb-session.c/session_destroy(struct afb_session*)");free(session);

}

/* update expiration of 'session' according to 'now' */
/** Instrumented function session_update_expiration(struct afb_session*,time_t) */
static void session_update_expiration(struct afb_session *session, time_t now)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/session_update_expiration(struct afb_session*,time_t)");AKA_fCall++;
		AKA_mark("lis===237###sois===6060###eois===6078###lif===2###soif===82###eoif===100###ins===true###function===./app-framework-binder/src/afb-session.c/session_update_expiration(struct afb_session*,time_t)");time_t expiration;


	/* compute expiration */
		AKA_mark("lis===240###sois===6107###eois===6155###lif===5###soif===129###eoif===177###ins===true###function===./app-framework-binder/src/afb-session.c/session_update_expiration(struct afb_session*,time_t)");expiration = now + afb_session_timeout(session);

		if (AKA_mark("lis===241###sois===6161###eois===6175###lif===6###soif===183###eoif===197###ifc===true###function===./app-framework-binder/src/afb-session.c/session_update_expiration(struct afb_session*,time_t)") && (AKA_mark("lis===241###sois===6161###eois===6175###lif===6###soif===183###eoif===197###isc===true###function===./app-framework-binder/src/afb-session.c/session_update_expiration(struct afb_session*,time_t)")&&expiration < 0)) {
		AKA_mark("lis===242###sois===6179###eois===6207###lif===7###soif===201###eoif===229###ins===true###function===./app-framework-binder/src/afb-session.c/session_update_expiration(struct afb_session*,time_t)");expiration = MAX_EXPIRATION;
	}
	else {AKA_mark("lis===-241-###sois===-6161-###eois===-616114-###lif===-6-###soif===-###eoif===-197-###ins===true###function===./app-framework-binder/src/afb-session.c/session_update_expiration(struct afb_session*,time_t)");}


	/* record the expiration */
		AKA_mark("lis===245###sois===6239###eois===6272###lif===10###soif===261###eoif===294###ins===true###function===./app-framework-binder/src/afb-session.c/session_update_expiration(struct afb_session*,time_t)");session->expiration = expiration;

}

/*
 * Add a new session with the 'uuid' (of 'hashidx')
 * and the 'timeout' starting from 'now'.
 * Add it to the set of sessions
 * Return the created session
 */
/** Instrumented function session_add(const char*,int,time_t,uint8_t) */
static struct afb_session *session_add(const char *uuid, int timeout, time_t now, uint8_t hashidx)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");AKA_fCall++;
		AKA_mark("lis===256###sois===6542###eois===6570###lif===2###soif===102###eoif===130###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");struct afb_session *session;


	/* check arguments */
		if (AKA_mark("lis===259###sois===6600###eois===6690###lif===5###soif===160###eoif===250###ifc===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)") && ((AKA_mark("lis===259###sois===6600###eois===6638###lif===5###soif===160###eoif===198###isc===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)")&&!AFB_SESSION_TIMEOUT_IS_VALID(timeout))	||((AKA_mark("lis===260###sois===6645###eois===6649###lif===6###soif===205###eoif===209###isc===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)")&&uuid)	&&(AKA_mark("lis===260###sois===6653###eois===6689###lif===6###soif===213###eoif===249###isc===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)")&&strlen(uuid) >= sizeof session->uuid)))) {
				AKA_mark("lis===261###sois===6696###eois===6711###lif===7###soif===256###eoif===271###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");errno = EINVAL;

				AKA_mark("lis===262###sois===6714###eois===6726###lif===8###soif===274###eoif===286###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");return NULL;

	}
	else {AKA_mark("lis===-259-###sois===-6600-###eois===-660090-###lif===-5-###soif===-###eoif===-250-###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");}


	/* allocates a new one */
		AKA_mark("lis===266###sois===6759###eois===6796###lif===12###soif===319###eoif===356###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");session = calloc(1, sizeof *session);

		if (AKA_mark("lis===267###sois===6802###eois===6817###lif===13###soif===362###eoif===377###ifc===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)") && (AKA_mark("lis===267###sois===6802###eois===6817###lif===13###soif===362###eoif===377###isc===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)")&&session == NULL)) {
				AKA_mark("lis===268###sois===6823###eois===6838###lif===14###soif===383###eoif===398###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");errno = ENOMEM;

				AKA_mark("lis===269###sois===6841###eois===6853###lif===15###soif===401###eoif===413###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");return NULL;

	}
	else {AKA_mark("lis===-267-###sois===-6802-###eois===-680215-###lif===-13-###soif===-###eoif===-377-###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");}


	/* initialize */
		AKA_mark("lis===273###sois===6877###eois===6919###lif===19###soif===437###eoif===479###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");pthread_mutex_init(&session->mutex, NULL);

		AKA_mark("lis===274###sois===6921###eois===6943###lif===20###soif===481###eoif===503###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");session->refcount = 1;

		AKA_mark("lis===275###sois===6945###eois===6973###lif===21###soif===505###eoif===533###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");strcpy(session->uuid, uuid);

		AKA_mark("lis===276###sois===6975###eois===7002###lif===22###soif===535###eoif===562###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");session->timeout = timeout;

		AKA_mark("lis===277###sois===7004###eois===7044###lif===23###soif===564###eoif===604###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");session_update_expiration(session, now);

		AKA_mark("lis===278###sois===7046###eois===7077###lif===24###soif===606###eoif===637###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");session->id = ++sessions.genid;

		while (AKA_mark("lis===279###sois===7086###eois===7147###lif===25###soif===646###eoif===707###ifc===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)") && ((AKA_mark("lis===279###sois===7086###eois===7102###lif===25###soif===646###eoif===662###isc===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)")&&session->id == 0)	||(AKA_mark("lis===279###sois===7106###eois===7147###lif===25###soif===666###eoif===707###isc===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)")&&sessionset_search_id(session->id) != NULL))) {
		AKA_mark("lis===280###sois===7151###eois===7182###lif===26###soif===711###eoif===742###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");session->id = ++sessions.genid;
	}


	/* add */
		if (AKA_mark("lis===283###sois===7200###eois===7232###lif===29###soif===760###eoif===792###ifc===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)") && (AKA_mark("lis===283###sois===7200###eois===7232###lif===29###soif===760###eoif===792###isc===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)")&&sessionset_add(session, hashidx))) {
				AKA_mark("lis===284###sois===7238###eois===7252###lif===30###soif===798###eoif===812###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");free(session);

				AKA_mark("lis===285###sois===7255###eois===7267###lif===31###soif===815###eoif===827###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");return NULL;

	}
	else {AKA_mark("lis===-283-###sois===-7200-###eois===-720032-###lif===-29-###soif===-###eoif===-792-###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");}


#if WITH_AFB_HOOK
	afb_hook_session_create(session);
#endif

		AKA_mark("lis===292###sois===7334###eois===7349###lif===38###soif===894###eoif===909###ins===true###function===./app-framework-binder/src/afb-session.c/session_add(const char*,int,time_t,uint8_t)");return session;

}

/* Remove expired sessions and return current time (now) */
/** Instrumented function sessionset_cleanup(int) */
static time_t sessionset_cleanup (int force)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");AKA_fCall++;
		AKA_mark("lis===298###sois===7461###eois===7496###lif===2###soif===48###eoif===83###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");struct afb_session *session, **prv;

		AKA_mark("lis===299###sois===7498###eois===7509###lif===3###soif===85###eoif===96###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");time_t now;


	/* Loop on Sessions Table and remove anything that is older than timeout */
		AKA_mark("lis===302###sois===7589###eois===7599###lif===6###soif===176###eoif===186###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");now = NOW;

		AKA_mark("lis===303###sois===7601###eois===7623###lif===7###soif===188###eoif===210###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");prv = &sessions.first;

		while (AKA_mark("lis===304###sois===7632###eois===7648###lif===8###soif===219###eoif===235###ifc===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)") && (session=(*prv) && AKA_mark("lis===304###sois===7633###eois===7647###lif===8###soif===220###eoif===234###isc===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)"))) {
				AKA_mark("lis===305###sois===7654###eois===7676###lif===9###soif===241###eoif===263###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");session_lock(session);

				if (AKA_mark("lis===306###sois===7683###eois===7717###lif===10###soif===270###eoif===304###ifc===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)") && ((AKA_mark("lis===306###sois===7683###eois===7688###lif===10###soif===270###eoif===275###isc===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)")&&force)	||(AKA_mark("lis===306###sois===7692###eois===7717###lif===10###soif===279###eoif===304###isc===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)")&&session->expiration < now))) {
			AKA_mark("lis===307###sois===7722###eois===7745###lif===11###soif===309###eoif===332###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");session_close(session);
		}
		else {AKA_mark("lis===-306-###sois===-7683-###eois===-768334-###lif===-10-###soif===-###eoif===-304-###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");}

				if (AKA_mark("lis===308###sois===7752###eois===7768###lif===12###soif===339###eoif===355###ifc===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)") && (AKA_mark("lis===308###sois===7752###eois===7768###lif===12###soif===339###eoif===355###isc===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)")&&!session->closed)) {
						AKA_mark("lis===309###sois===7775###eois===7796###lif===13###soif===362###eoif===383###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");prv = &session->next;

						AKA_mark("lis===310###sois===7800###eois===7824###lif===14###soif===387###eoif===411###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");session_unlock(session);

		}
		else {
						AKA_mark("lis===312###sois===7839###eois===7860###lif===16###soif===426###eoif===447###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");*prv = session->next;

						AKA_mark("lis===313###sois===7864###eois===7881###lif===17###soif===451###eoif===468###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");sessions.count--;

						AKA_mark("lis===314###sois===7885###eois===7907###lif===18###soif===472###eoif===494###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");session->notinset = 1;

						if (AKA_mark("lis===315###sois===7915###eois===7932###lif===19###soif===502###eoif===519###ifc===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)") && (AKA_mark("lis===315###sois===7915###eois===7932###lif===19###soif===502###eoif===519###isc===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)")&&session->refcount)) {
				AKA_mark("lis===316###sois===7938###eois===7962###lif===20###soif===525###eoif===549###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");session_unlock(session);
			}
			else {
				AKA_mark("lis===318###sois===7975###eois===8000###lif===22###soif===562###eoif===587###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");session_destroy(session);
			}

		}

	}

		AKA_mark("lis===321###sois===8009###eois===8020###lif===25###soif===596###eoif===607###ins===true###function===./app-framework-binder/src/afb-session.c/sessionset_cleanup(int)");return now;

}

/**
 * Initialize the session manager with a 'max_session_count',
 * an initial common 'timeout'
 *
 * @param max_session_count  maximum allowed session count in the same time
 * @param timeout            the initial default timeout of sessions
 */
/** Instrumented function afb_session_init(int,int) */
int afb_session_init (int max_session_count, int timeout)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_init(int,int)");AKA_fCall++;
	/* init the sessionset (after cleanup) */
		AKA_mark("lis===334###sois===8377###eois===8395###lif===3###soif===104###eoif===122###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_init(int,int)");sessionset_lock();

		AKA_mark("lis===335###sois===8397###eois===8419###lif===4###soif===124###eoif===146###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_init(int,int)");sessionset_cleanup(1);

		if (AKA_mark("lis===336###sois===8425###eois===8462###lif===5###soif===152###eoif===189###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_init(int,int)") && (AKA_mark("lis===336###sois===8425###eois===8462###lif===5###soif===152###eoif===189###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_init(int,int)")&&max_session_count > SESSION_COUNT_MAX)) {
		AKA_mark("lis===337###sois===8466###eois===8499###lif===6###soif===193###eoif===226###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_init(int,int)");sessions.max = SESSION_COUNT_MAX;
	}
	else {
		if (AKA_mark("lis===338###sois===8510###eois===8547###lif===7###soif===237###eoif===274###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_init(int,int)") && (AKA_mark("lis===338###sois===8510###eois===8547###lif===7###soif===237###eoif===274###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_init(int,int)")&&max_session_count < SESSION_COUNT_MIN)) {
			AKA_mark("lis===339###sois===8551###eois===8584###lif===8###soif===278###eoif===311###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_init(int,int)");sessions.max = SESSION_COUNT_MIN;
		}
		else {
			AKA_mark("lis===341###sois===8593###eois===8636###lif===10###soif===320###eoif===363###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_init(int,int)");sessions.max = (uint16_t)max_session_count;
		}
	}

		AKA_mark("lis===342###sois===8638###eois===8665###lif===11###soif===365###eoif===392###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_init(int,int)");sessions.timeout = timeout;

		AKA_mark("lis===343###sois===8667###eois===8687###lif===12###soif===394###eoif===414###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_init(int,int)");sessionset_unlock();

		AKA_mark("lis===344###sois===8689###eois===8698###lif===13###soif===416###eoif===425###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_init(int,int)");return 0;

}

/**
 * Iterate the sessions and call 'callback' with
 * the 'closure' for each session.
 */
/** Instrumented function afb_session_foreach(void(*callback)(void*closure, struct afb_session*session),void*) */
void afb_session_foreach(void (*callback)(void *closure, struct afb_session *session), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_foreach(void(*callback)(void*closure, struct afb_session*session),void*)");AKA_fCall++;
		AKA_mark("lis===353###sois===8899###eois===8927###lif===2###soif===105###eoif===133###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_foreach(void(*callback)(void*closure, struct afb_session*session),void*)");struct afb_session *session;


	/* Loop on Sessions Table and remove anything that is older than timeout */
		AKA_mark("lis===356###sois===9007###eois===9025###lif===5###soif===213###eoif===231###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_foreach(void(*callback)(void*closure, struct afb_session*session),void*)");sessionset_lock();

		AKA_mark("lis===357###sois===9027###eois===9052###lif===6###soif===233###eoif===258###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_foreach(void(*callback)(void*closure, struct afb_session*session),void*)");session = sessions.first;

		while (AKA_mark("lis===358###sois===9061###eois===9068###lif===7###soif===267###eoif===274###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_foreach(void(*callback)(void*closure, struct afb_session*session),void*)") && (AKA_mark("lis===358###sois===9061###eois===9068###lif===7###soif===267###eoif===274###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_foreach(void(*callback)(void*closure, struct afb_session*session),void*)")&&session)) {
				if (AKA_mark("lis===359###sois===9078###eois===9094###lif===8###soif===284###eoif===300###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_foreach(void(*callback)(void*closure, struct afb_session*session),void*)") && (AKA_mark("lis===359###sois===9078###eois===9094###lif===8###soif===284###eoif===300###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_foreach(void(*callback)(void*closure, struct afb_session*session),void*)")&&!session->closed)) {
			AKA_mark("lis===360###sois===9099###eois===9126###lif===9###soif===305###eoif===332###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_foreach(void(*callback)(void*closure, struct afb_session*session),void*)");callback(closure, session);
		}
		else {AKA_mark("lis===-359-###sois===-9078-###eois===-907816-###lif===-8-###soif===-###eoif===-300-###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_foreach(void(*callback)(void*closure, struct afb_session*session),void*)");}

				AKA_mark("lis===361###sois===9129###eois===9153###lif===10###soif===335###eoif===359###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_foreach(void(*callback)(void*closure, struct afb_session*session),void*)");session = session->next;

	}

		AKA_mark("lis===363###sois===9158###eois===9178###lif===12###soif===364###eoif===384###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_foreach(void(*callback)(void*closure, struct afb_session*session),void*)");sessionset_unlock();

}

/**
 * Cleanup the sessionset of its closed or expired sessions
 */
/** Instrumented function afb_session_purge() */
void afb_session_purge()
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_purge()");AKA_fCall++;
		AKA_mark("lis===371###sois===9278###eois===9296###lif===2###soif===28###eoif===46###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_purge()");sessionset_lock();

		AKA_mark("lis===372###sois===9298###eois===9320###lif===3###soif===48###eoif===70###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_purge()");sessionset_cleanup(0);

		AKA_mark("lis===373###sois===9322###eois===9342###lif===4###soif===72###eoif===92###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_purge()");sessionset_unlock();

}

/* Searchs the session of 'uuid' */
/** Instrumented function afb_session_search(const char*) */
struct afb_session *afb_session_search (const char *uuid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_search(const char*)");AKA_fCall++;
		AKA_mark("lis===379###sois===9443###eois===9471###lif===2###soif===61###eoif===89###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_search(const char*)");struct afb_session *session;


		AKA_mark("lis===381###sois===9474###eois===9492###lif===4###soif===92###eoif===110###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_search(const char*)");sessionset_lock();

		AKA_mark("lis===382###sois===9494###eois===9516###lif===5###soif===112###eoif===134###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_search(const char*)");sessionset_cleanup(0);

		AKA_mark("lis===383###sois===9518###eois===9568###lif===6###soif===136###eoif===186###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_search(const char*)");session = sessionset_search(uuid, pearson4(uuid));

		AKA_mark("lis===384###sois===9570###eois===9608###lif===7###soif===188###eoif===226###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_search(const char*)");session = afb_session_addref(session);

		AKA_mark("lis===385###sois===9610###eois===9630###lif===8###soif===228###eoif===248###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_search(const char*)");sessionset_unlock();

		AKA_mark("lis===386###sois===9632###eois===9647###lif===9###soif===250###eoif===265###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_search(const char*)");return session;


}

/**
 * Creates a new session with 'timeout'
 */
/** Instrumented function afb_session_create(int) */
struct afb_session *afb_session_create (int timeout)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_create(int)");AKA_fCall++;
		AKA_mark("lis===395###sois===9756###eois===9800###lif===2###soif===56###eoif===100###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_create(int)");return afb_session_get(NULL, timeout, NULL);

}

/**
 * Returns the timeout of 'session' in seconds
 */
/** Instrumented function afb_session_timeout(struct afb_session*) */
int afb_session_timeout(struct afb_session *session)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_timeout(struct afb_session*)");AKA_fCall++;
		AKA_mark("lis===403###sois===9915###eois===9927###lif===2###soif===56###eoif===68###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_timeout(struct afb_session*)");int timeout;


	/* compute timeout */
		AKA_mark("lis===406###sois===9953###eois===9980###lif===5###soif===94###eoif===121###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_timeout(struct afb_session*)");timeout = session->timeout;

		if (AKA_mark("lis===407###sois===9986###eois===10024###lif===6###soif===127###eoif===165###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_timeout(struct afb_session*)") && (AKA_mark("lis===407###sois===9986###eois===10024###lif===6###soif===127###eoif===165###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_timeout(struct afb_session*)")&&timeout == AFB_SESSION_TIMEOUT_DEFAULT)) {
		AKA_mark("lis===408###sois===10028###eois===10055###lif===7###soif===169###eoif===196###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_timeout(struct afb_session*)");timeout = sessions.timeout;
	}
	else {AKA_mark("lis===-407-###sois===-9986-###eois===-998638-###lif===-6-###soif===-###eoif===-165-###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_timeout(struct afb_session*)");}

		if (AKA_mark("lis===409###sois===10061###eois===10072###lif===8###soif===202###eoif===213###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_timeout(struct afb_session*)") && (AKA_mark("lis===409###sois===10061###eois===10072###lif===8###soif===202###eoif===213###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_timeout(struct afb_session*)")&&timeout < 0)) {
		AKA_mark("lis===410###sois===10076###eois===10094###lif===9###soif===217###eoif===235###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_timeout(struct afb_session*)");timeout = INT_MAX;
	}
	else {AKA_mark("lis===-409-###sois===-10061-###eois===-1006111-###lif===-8-###soif===-###eoif===-213-###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_timeout(struct afb_session*)");}

		AKA_mark("lis===411###sois===10096###eois===10111###lif===10###soif===237###eoif===252###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_timeout(struct afb_session*)");return timeout;

}

/**
 * Returns the second remaining before expiration of 'session'
 */
/** Instrumented function afb_session_what_remains(struct afb_session*) */
int afb_session_what_remains(struct afb_session *session)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_what_remains(struct afb_session*)");AKA_fCall++;
		AKA_mark("lis===419###sois===10247###eois===10291###lif===2###soif===61###eoif===105###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_what_remains(struct afb_session*)");int diff = (int)(session->expiration - NOW);

		AKA_mark("lis===420###sois===10293###eois===10320###lif===3###soif===107###eoif===134###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_what_remains(struct afb_session*)");return diff < 0 ? 0 : diff;

}

/* This function will return exiting session or newly created session */
/** Instrumented function afb_session_get(const char*,int,int*) */
struct afb_session *afb_session_get (const char *uuid, int timeout, int *created)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");AKA_fCall++;
		AKA_mark("lis===426###sois===10482###eois===10504###lif===2###soif===85###eoif===107###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");uuid_stringz_t _uuid_;

		AKA_mark("lis===427###sois===10506###eois===10522###lif===3###soif===109###eoif===125###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");uint8_t hashidx;

		AKA_mark("lis===428###sois===10524###eois===10552###lif===4###soif===127###eoif===155###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");struct afb_session *session;

		AKA_mark("lis===429###sois===10554###eois===10565###lif===5###soif===157###eoif===168###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");time_t now;

		AKA_mark("lis===430###sois===10567###eois===10573###lif===6###soif===170###eoif===176###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");int c;


	/* cleaning */
		AKA_mark("lis===433###sois===10592###eois===10610###lif===9###soif===195###eoif===213###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");sessionset_lock();

		AKA_mark("lis===434###sois===10612###eois===10640###lif===10###soif===215###eoif===243###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");now = sessionset_cleanup(0);


	/* search for an existing one not too old */
		if (AKA_mark("lis===437###sois===10693###eois===10698###lif===13###soif===296###eoif===301###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)") && (AKA_mark("lis===437###sois===10693###eois===10698###lif===13###soif===296###eoif===301###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)")&&!uuid)) {
				AKA_mark("lis===438###sois===10704###eois===10743###lif===14###soif===307###eoif===346###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");hashidx = sessionset_make_uuid(_uuid_);

				AKA_mark("lis===439###sois===10746###eois===10760###lif===15###soif===349###eoif===363###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");uuid = _uuid_;

	}
	else {
				AKA_mark("lis===441###sois===10773###eois===10798###lif===17###soif===376###eoif===401###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");hashidx = pearson4(uuid);

				AKA_mark("lis===442###sois===10801###eois===10844###lif===18###soif===404###eoif===447###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");session = sessionset_search(uuid, hashidx);

				if (AKA_mark("lis===443###sois===10851###eois===10858###lif===19###soif===454###eoif===461###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)") && (AKA_mark("lis===443###sois===10851###eois===10858###lif===19###soif===454###eoif===461###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)")&&session)) {
			/* session found */
						AKA_mark("lis===445###sois===10888###eois===10916###lif===21###soif===491###eoif===519###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");afb_session_addref(session);

						AKA_mark("lis===446###sois===10920###eois===10926###lif===22###soif===523###eoif===529###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");c = 0;

						AKA_mark("lis===447###sois===10930###eois===10939###lif===23###soif===533###eoif===542###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");goto end;

		}
		else {AKA_mark("lis===-443-###sois===-10851-###eois===-108517-###lif===-19-###soif===-###eoif===-461-###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");}

	}

	/* create the session */
		AKA_mark("lis===451###sois===10974###eois===11025###lif===27###soif===577###eoif===628###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");session = session_add(uuid, timeout, now, hashidx);

		AKA_mark("lis===452###sois===11027###eois===11033###lif===28###soif===630###eoif===636###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");c = 1;

	end:
	AKA_mark("lis===454###sois===11040###eois===11060###lif===30###soif===643###eoif===663###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");sessionset_unlock();

		if (AKA_mark("lis===455###sois===11066###eois===11073###lif===31###soif===669###eoif===676###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)") && (AKA_mark("lis===455###sois===11066###eois===11073###lif===31###soif===669###eoif===676###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)")&&created)) {
		AKA_mark("lis===456###sois===11077###eois===11090###lif===32###soif===680###eoif===693###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");*created = c;
	}
	else {AKA_mark("lis===-455-###sois===-11066-###eois===-110667-###lif===-31-###soif===-###eoif===-676-###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");}


		AKA_mark("lis===458###sois===11093###eois===11108###lif===34###soif===696###eoif===711###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get(const char*,int,int*)");return session;

}

/* increase the use count on 'session' (can be NULL) */
/** Instrumented function afb_session_addref(struct afb_session*) */
struct afb_session *afb_session_addref(struct afb_session *session)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_addref(struct afb_session*)");AKA_fCall++;
		if (AKA_mark("lis===464###sois===11243###eois===11258###lif===2###soif===75###eoif===90###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_addref(struct afb_session*)") && (AKA_mark("lis===464###sois===11243###eois===11258###lif===2###soif===75###eoif===90###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_addref(struct afb_session*)")&&session != NULL)) {
#if WITH_AFB_HOOK
		afb_hook_session_addref(session);
#endif
				AKA_mark("lis===468###sois===11325###eois===11347###lif===6###soif===157###eoif===179###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_addref(struct afb_session*)");session_lock(session);

				AKA_mark("lis===469###sois===11350###eois===11370###lif===7###soif===182###eoif===202###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_addref(struct afb_session*)");session->refcount++;

				AKA_mark("lis===470###sois===11373###eois===11397###lif===8###soif===205###eoif===229###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_addref(struct afb_session*)");session_unlock(session);

	}
	else {AKA_mark("lis===-464-###sois===-11243-###eois===-1124315-###lif===-2-###soif===-###eoif===-90-###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_addref(struct afb_session*)");}

		AKA_mark("lis===472###sois===11402###eois===11417###lif===10###soif===234###eoif===249###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_addref(struct afb_session*)");return session;

}

/* decrease the use count of 'session' (can be NULL) */
/** Instrumented function afb_session_unref(struct afb_session*) */
void afb_session_unref(struct afb_session *session)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)");AKA_fCall++;
		if (AKA_mark("lis===478###sois===11536###eois===11551###lif===2###soif===59###eoif===74###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)") && (AKA_mark("lis===478###sois===11536###eois===11551###lif===2###soif===59###eoif===74###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)")&&session == NULL)) {
		AKA_mark("lis===479###sois===11555###eois===11562###lif===3###soif===78###eoif===85###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)");return;
	}
	else {AKA_mark("lis===-478-###sois===-11536-###eois===-1153615-###lif===-2-###soif===-###eoif===-74-###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)");}


#if WITH_AFB_HOOK
	afb_hook_session_unref(session);
#endif
		AKA_mark("lis===484###sois===11624###eois===11646###lif===8###soif===147###eoif===169###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)");session_lock(session);

		if (AKA_mark("lis===485###sois===11652###eois===11672###lif===9###soif===175###eoif===195###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)") && (AKA_mark("lis===485###sois===11652###eois===11672###lif===9###soif===175###eoif===195###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)")&&!--session->refcount)) {
				if (AKA_mark("lis===486###sois===11682###eois===11700###lif===10###soif===205###eoif===223###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)") && (AKA_mark("lis===486###sois===11682###eois===11700###lif===10###soif===205###eoif===223###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)")&&session->autoclose)) {
			AKA_mark("lis===487###sois===11705###eois===11728###lif===11###soif===228###eoif===251###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)");session_close(session);
		}
		else {AKA_mark("lis===-486-###sois===-11682-###eois===-1168218-###lif===-10-###soif===-###eoif===-223-###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)");}

				if (AKA_mark("lis===488###sois===11735###eois===11752###lif===12###soif===258###eoif===275###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)") && (AKA_mark("lis===488###sois===11735###eois===11752###lif===12###soif===258###eoif===275###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)")&&session->notinset)) {
						AKA_mark("lis===489###sois===11759###eois===11784###lif===13###soif===282###eoif===307###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)");session_destroy(session);

						AKA_mark("lis===490###sois===11788###eois===11795###lif===14###soif===311###eoif===318###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)");return;

		}
		else {AKA_mark("lis===-488-###sois===-11735-###eois===-1173517-###lif===-12-###soif===-###eoif===-275-###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)");}

	}
	else {AKA_mark("lis===-485-###sois===-11652-###eois===-1165220-###lif===-9-###soif===-###eoif===-195-###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)");}

		AKA_mark("lis===493###sois===11804###eois===11828###lif===17###soif===327###eoif===351###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_unref(struct afb_session*)");session_unlock(session);

}

/* close 'session' */
/** Instrumented function afb_session_close(struct afb_session*) */
void afb_session_close (struct afb_session *session)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_close(struct afb_session*)");AKA_fCall++;
		AKA_mark("lis===499###sois===11910###eois===11932###lif===2###soif===56###eoif===78###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_close(struct afb_session*)");session_lock(session);

		AKA_mark("lis===500###sois===11934###eois===11957###lif===3###soif===80###eoif===103###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_close(struct afb_session*)");session_close(session);

		AKA_mark("lis===501###sois===11959###eois===11983###lif===4###soif===105###eoif===129###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_close(struct afb_session*)");session_unlock(session);

}

/**
 * Set the 'autoclose' flag of the 'session'
 *
 * A session whose autoclose flag is true will close as
 * soon as it is no more referenced.
 *
 * @param session    the session to set
 * @param autoclose  the value to set
 */
/** Instrumented function afb_session_set_autoclose(struct afb_session*,int) */
void afb_session_set_autoclose(struct afb_session *session, int autoclose)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_set_autoclose(struct afb_session*,int)");AKA_fCall++;
		AKA_mark("lis===515###sois===12295###eois===12328###lif===2###soif===78###eoif===111###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_set_autoclose(struct afb_session*,int)");session->autoclose = !!autoclose;

}

/* is 'session' closed? */
/** Instrumented function afb_session_is_closed(struct afb_session*) */
int afb_session_is_closed (struct afb_session *session)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_is_closed(struct afb_session*)");AKA_fCall++;
		AKA_mark("lis===521###sois===12418###eois===12441###lif===2###soif===59###eoif===82###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_is_closed(struct afb_session*)");return session->closed;

}

/* Returns the uuid of 'session' */
/** Instrumented function afb_session_uuid(struct afb_session*) */
const char *afb_session_uuid (struct afb_session *session)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_uuid(struct afb_session*)");AKA_fCall++;
		AKA_mark("lis===527###sois===12543###eois===12564###lif===2###soif===62###eoif===83###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_uuid(struct afb_session*)");return session->uuid;

}

/* Returns the local id of 'session' */
/** Instrumented function afb_session_id(struct afb_session*) */
uint16_t afb_session_id (struct afb_session *session)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_id(struct afb_session*)");AKA_fCall++;
		AKA_mark("lis===533###sois===12665###eois===12684###lif===2###soif===57###eoif===76###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_id(struct afb_session*)");return session->id;

}

/**
 * Get the index of the 'key' in the cookies array.
 * @param key the key to scan
 * @return the index of the list for key within cookies
 */
#if COOKIEMASK
/** Instrumented function cookeyidx(const void*) */
static int cookeyidx(const void *key)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/cookeyidx(const void*)");AKA_fCall++;
		AKA_mark("lis===544###sois===12890###eois===12917###lif===2###soif===41###eoif===68###ins===true###function===./app-framework-binder/src/afb-session.c/cookeyidx(const void*)");intptr_t x = (intptr_t)key;

		AKA_mark("lis===545###sois===12919###eois===12965###lif===3###soif===70###eoif===116###ins===true###function===./app-framework-binder/src/afb-session.c/cookeyidx(const void*)");unsigned r = (unsigned)((x >> 5) ^ (x >> 15));

		AKA_mark("lis===546###sois===12967###eois===12989###lif===4###soif===118###eoif===140###ins===true###function===./app-framework-binder/src/afb-session.c/cookeyidx(const void*)");return r & COOKIEMASK;

}
#else
#  define cookeyidx(key) 0
#endif

/**
 * Set, get, replace, remove a cookie of 'key' for the 'session'
 *
 * The behaviour of this function depends on its parameters:
 *
 * @param session	the session
 * @param key		the key of the cookie
 * @param makecb	the creation function or NULL
 * @param freecb	the release function or NULL
 * @param closure	an argument for makecb or the value if makecb==NULL
 * @param replace	a boolean enforcing replacement of the previous value
 *
 * @return the value of the cookie
 *
 * The 'key' is a pointer and compared as pointers.
 *
 * For getting the current value of the cookie:
 *
 *   afb_session_cookie(session, key, NULL, NULL, NULL, 0)
 *
 * For storing the value of the cookie
 *
 *   afb_session_cookie(session, key, NULL, NULL, value, 1)
 */
/** Instrumented function afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int) */
void *afb_session_cookie(struct afb_session *session, const void *key, void *(*makecb)(void *closure), void (*freecb)(void *item), void *closure, int replace)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");AKA_fCall++;
		AKA_mark("lis===578###sois===13948###eois===13956###lif===2###soif===162###eoif===170###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");int idx;

		AKA_mark("lis===579###sois===13958###eois===13970###lif===3###soif===172###eoif===184###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");void *value;

		AKA_mark("lis===580###sois===13972###eois===14001###lif===4###soif===186###eoif===215###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");struct cookie *cookie, **prv;


	/* get key hashed index */
		AKA_mark("lis===583###sois===14032###eois===14053###lif===7###soif===246###eoif===267###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");idx = cookeyidx(key);


	/* lock session and search for the cookie of 'key' */
		AKA_mark("lis===586###sois===14111###eois===14133###lif===10###soif===325###eoif===347###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");session_lock(session);

		AKA_mark("lis===587###sois===14135###eois===14164###lif===11###soif===349###eoif===378###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");prv = &session->cookies[idx];

		for (;;) {
				AKA_mark("lis===589###sois===14179###eois===14193###lif===13###soif===393###eoif===407###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");cookie = *prv;

				if (AKA_mark("lis===590###sois===14200###eois===14207###lif===14###soif===414###eoif===421###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)") && (AKA_mark("lis===590###sois===14200###eois===14207###lif===14###soif===414###eoif===421###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)")&&!cookie)) {
			/* 'key' not found, create value using 'closure' and 'makecb' */
						AKA_mark("lis===592###sois===14282###eois===14325###lif===16###soif===496###eoif===539###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");value = makecb ? makecb(closure) : closure;

			/* store the the only if it has some meaning */
						if (AKA_mark("lis===594###sois===14384###eois===14411###lif===18###soif===598###eoif===625###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)") && (((AKA_mark("lis===594###sois===14384###eois===14391###lif===18###soif===598###eoif===605###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)")&&replace)	||(AKA_mark("lis===594###sois===14395###eois===14401###lif===18###soif===609###eoif===615###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)")&&makecb))	||(AKA_mark("lis===594###sois===14405###eois===14411###lif===18###soif===619###eoif===625###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)")&&freecb))) {
								AKA_mark("lis===595###sois===14419###eois===14451###lif===19###soif===633###eoif===665###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");cookie = malloc(sizeof *cookie);

								if (AKA_mark("lis===596###sois===14460###eois===14467###lif===20###soif===674###eoif===681###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)") && (AKA_mark("lis===596###sois===14460###eois===14467###lif===20###soif===674###eoif===681###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)")&&!cookie)) {
										AKA_mark("lis===597###sois===14476###eois===14491###lif===21###soif===690###eoif===705###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");errno = ENOMEM;

					/* calling freecb if there is no makecb may have issue */
										if (AKA_mark("lis===599###sois===14564###eois===14580###lif===23###soif===778###eoif===794###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)") && ((AKA_mark("lis===599###sois===14564###eois===14570###lif===23###soif===778###eoif===784###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)")&&makecb)	&&(AKA_mark("lis===599###sois===14574###eois===14580###lif===23###soif===788###eoif===794###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)")&&freecb))) {
						AKA_mark("lis===600###sois===14588###eois===14602###lif===24###soif===802###eoif===816###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");freecb(value);
					}
					else {AKA_mark("lis===-599-###sois===-14564-###eois===-1456416-###lif===-23-###soif===-###eoif===-794-###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");}

										AKA_mark("lis===601###sois===14608###eois===14621###lif===25###soif===822###eoif===835###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");value = NULL;

				}
				else {
										AKA_mark("lis===603###sois===14640###eois===14658###lif===27###soif===854###eoif===872###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");cookie->key = key;

										AKA_mark("lis===604###sois===14664###eois===14686###lif===28###soif===878###eoif===900###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");cookie->value = value;

										AKA_mark("lis===605###sois===14692###eois===14716###lif===29###soif===906###eoif===930###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");cookie->freecb = freecb;

										AKA_mark("lis===606###sois===14722###eois===14742###lif===30###soif===936###eoif===956###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");cookie->next = NULL;

										AKA_mark("lis===607###sois===14748###eois===14762###lif===31###soif===962###eoif===976###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");*prv = cookie;

				}

			}
			else {AKA_mark("lis===-594-###sois===-14384-###eois===-1438427-###lif===-18-###soif===-###eoif===-625-###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");}

						AKA_mark("lis===610###sois===14777###eois===14783###lif===34###soif===991###eoif===997###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");break;

		}
		else {
			if (AKA_mark("lis===611###sois===14797###eois===14815###lif===35###soif===1011###eoif===1029###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)") && (AKA_mark("lis===611###sois===14797###eois===14815###lif===35###soif===1011###eoif===1029###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)")&&cookie->key == key)) {
			/* cookie of key found */
							if (AKA_mark("lis===613###sois===14855###eois===14863###lif===37###soif===1069###eoif===1077###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)") && (AKA_mark("lis===613###sois===14855###eois===14863###lif===37###soif===1069###eoif===1077###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)")&&!replace)) {
					AKA_mark("lis===615###sois===14908###eois===14930###lif===39###soif===1122###eoif===1144###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");value = cookie->value;
				}
				else {
				/* create value using 'closure' and 'makecb' */
									AKA_mark("lis===618###sois===14997###eois===15040###lif===42###soif===1211###eoif===1254###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");value = makecb ? makecb(closure) : closure;


				/* free previous value is needed */
									if (AKA_mark("lis===621###sois===15090###eois===15130###lif===45###soif===1304###eoif===1344###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)") && ((AKA_mark("lis===621###sois===15090###eois===15112###lif===45###soif===1304###eoif===1326###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)")&&cookie->value != value)	&&(AKA_mark("lis===621###sois===15116###eois===15130###lif===45###soif===1330###eoif===1344###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)")&&cookie->freecb))) {
						AKA_mark("lis===622###sois===15137###eois===15167###lif===46###soif===1351###eoif===1381###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");cookie->freecb(cookie->value);
					}
					else {AKA_mark("lis===-621-###sois===-15090-###eois===-1509040-###lif===-45-###soif===-###eoif===-1344-###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");}


				/* if both value and freecb are NULL drop the cookie */
									if (AKA_mark("lis===625###sois===15237###eois===15254###lif===49###soif===1451###eoif===1468###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)") && ((AKA_mark("lis===625###sois===15237###eois===15243###lif===49###soif===1451###eoif===1457###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)")&&!value)	&&(AKA_mark("lis===625###sois===15247###eois===15254###lif===49###soif===1461###eoif===1468###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)")&&!freecb))) {
											AKA_mark("lis===626###sois===15263###eois===15283###lif===50###soif===1477###eoif===1497###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");*prv = cookie->next;

											AKA_mark("lis===627###sois===15289###eois===15302###lif===51###soif===1503###eoif===1516###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");free(cookie);

				}
					else {
					/* store the value and its releaser */
											AKA_mark("lis===630###sois===15365###eois===15387###lif===54###soif===1579###eoif===1601###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");cookie->value = value;

											AKA_mark("lis===631###sois===15393###eois===15417###lif===55###soif===1607###eoif===1631###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");cookie->freecb = freecb;

				}

			}

							AKA_mark("lis===634###sois===15432###eois===15438###lif===58###soif===1646###eoif===1652###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");break;

		}
			else {
							AKA_mark("lis===636###sois===15453###eois===15475###lif===60###soif===1667###eoif===1689###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");prv = &(cookie->next);

		}
		}

	}


	/* unlock the session and return the value */
		AKA_mark("lis===641###sois===15532###eois===15556###lif===65###soif===1746###eoif===1770###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");session_unlock(session);

		AKA_mark("lis===642###sois===15558###eois===15571###lif===66###soif===1772###eoif===1785###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_cookie(struct afb_session*,const void*,void*(*makecb)(void*closure),void(*freecb)(void*item),void*,int)");return value;

}

/**
 * Get the cookie of 'key' in the 'session'.
 *
 * @param session  the session to search in
 * @param key      the key of the data to retrieve
 *
 * @return the data staored for the key or NULL if the key isn't found
 */
/** Instrumented function afb_session_get_cookie(struct afb_session*,const void*) */
void *afb_session_get_cookie(struct afb_session *session, const void *key)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_get_cookie(struct afb_session*,const void*)");AKA_fCall++;
		AKA_mark("lis===655###sois===15878###eois===15939###lif===2###soif===78###eoif===139###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get_cookie(struct afb_session*,const void*)");return afb_session_cookie(session, key, NULL, NULL, NULL, 0);

}

/**
 * Set the cookie of 'key' in the 'session' to the 'value' that can be
 * cleaned using 'freecb' (if not null).
 *
 * @param session  the session to set
 * @param key      the key of the data to store
 * @param value    the value to store at key
 * @param freecb   a function to use when the cookie value is to remove (or null)
 *
 * @return 0 in case of success or -1 in case of error
 */
/** Instrumented function afb_session_set_cookie(struct afb_session*,const void*,void*,void(*freecb)(void*)) */
int afb_session_set_cookie(struct afb_session *session, const void *key, void *value, void (*freecb)(void*))
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_set_cookie(struct afb_session*,const void*,void*,void(*freecb)(void*))");AKA_fCall++;
		AKA_mark("lis===671###sois===16449###eois===16525###lif===2###soif===112###eoif===188###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_set_cookie(struct afb_session*,const void*,void*,void(*freecb)(void*))");return -(value != afb_session_cookie(session, key, NULL, freecb, value, 1));

}

/**
 * Set the language attached to the session
 *
 * @param session the session to set
 * @param lang    the language specifiction to set to session
 *
 * @return 0 in case of success or -1 in case of error
 */
/** Instrumented function afb_session_set_language(struct afb_session*,const char*) */
int afb_session_set_language(struct afb_session *session, const char *lang)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_set_language(struct afb_session*,const char*)");AKA_fCall++;
		AKA_mark("lis===684###sois===16820###eois===16838###lif===2###soif===79###eoif===97###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_set_language(struct afb_session*,const char*)");char *oldl, *newl;


		AKA_mark("lis===686###sois===16841###eois===16861###lif===4###soif===100###eoif===120###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_set_language(struct afb_session*,const char*)");newl = strdup(lang);

		if (AKA_mark("lis===687###sois===16867###eois===16879###lif===5###soif===126###eoif===138###ifc===true###function===./app-framework-binder/src/afb-session.c/afb_session_set_language(struct afb_session*,const char*)") && (AKA_mark("lis===687###sois===16867###eois===16879###lif===5###soif===126###eoif===138###isc===true###function===./app-framework-binder/src/afb-session.c/afb_session_set_language(struct afb_session*,const char*)")&&newl == NULL)) {
		AKA_mark("lis===688###sois===16883###eois===16893###lif===6###soif===142###eoif===152###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_set_language(struct afb_session*,const char*)");return -1;
	}
	else {AKA_mark("lis===-687-###sois===-16867-###eois===-1686712-###lif===-5-###soif===-###eoif===-138-###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_set_language(struct afb_session*,const char*)");}


		AKA_mark("lis===690###sois===16896###eois===16917###lif===8###soif===155###eoif===176###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_set_language(struct afb_session*,const char*)");oldl = session->lang;

		AKA_mark("lis===691###sois===16919###eois===16940###lif===9###soif===178###eoif===199###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_set_language(struct afb_session*,const char*)");session->lang = newl;

		AKA_mark("lis===692###sois===16942###eois===16953###lif===10###soif===201###eoif===212###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_set_language(struct afb_session*,const char*)");free(oldl);

		AKA_mark("lis===693###sois===16955###eois===16964###lif===11###soif===214###eoif===223###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_set_language(struct afb_session*,const char*)");return 0;

}

/**
 * Get the language attached to the session
 *
 * @param session the session to query
 * @param lang    a default language specifiction
 *
 * @return the langauage specification to use for session
 */
/** Instrumented function afb_session_get_language(struct afb_session*,const char*) */
const char *afb_session_get_language(struct afb_session *session, const char *lang)
{AKA_mark("Calling: ./app-framework-binder/src/afb-session.c/afb_session_get_language(struct afb_session*,const char*)");AKA_fCall++;
		AKA_mark("lis===706###sois===17260###eois===17289###lif===2###soif===87###eoif===116###ins===true###function===./app-framework-binder/src/afb-session.c/afb_session_get_language(struct afb_session*,const char*)");return session->lang ?: lang;

}

#endif

