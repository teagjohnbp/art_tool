/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_PERM_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_PERM_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CONTEXT_H_
#define AKA_INCLUDE__AFB_CONTEXT_H_
#include "afb-context.lcov.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CRED_H_
#define AKA_INCLUDE__AFB_CRED_H_
#include "afb-cred.lcov.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_TOKEN_H_
#define AKA_INCLUDE__AFB_TOKEN_H_
#include "afb-token.lcov.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SESSION_H_
#define AKA_INCLUDE__AFB_SESSION_H_
#include "afb-session.lcov.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.lcov.akaignore.h"
#endif


/*********************************************************************************/

static inline const char *session_of_context(struct afb_context *context)
{AKA_fCall++;
	return context->token ? afb_token_string(context->token)
                : context->session ? afb_session_uuid(context->session)
                : "";
}

/*********************************************************************************/
#ifdef BACKEND_PERMISSION_IS_CYNARA

#include <pthread.h>
#include <cynara-client.h>

static cynara *handle;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int afb_perm_check(struct afb_context *context, const char *permission)
{
	int rc;

	if (!context->credentials) {
		/* case of permission for self */
		return 1;
	}
	if (!permission) {
		ERROR("Got a null permission!");
		return 0;
	}

	/* cynara isn't reentrant */
	pthread_mutex_lock(&mutex);

	/* lazy initialisation */
	if (!handle) {
		rc = cynara_initialize(&handle, NULL);
		if (rc != CYNARA_API_SUCCESS) {
			handle = NULL;
			ERROR("cynara initialisation failed with code %d", rc);
			return 0;
		}
	}

	/* query cynara permission */
	rc = cynara_check(handle, context->credentials->label, session_of_context(context), context->credentials->user, permission);

	pthread_mutex_unlock(&mutex);
	return rc == CYNARA_API_ACCESS_ALLOWED;
}
/*********************************************************************************/
#else
int afb_perm_check(struct afb_context *context, const char *permission)
{AKA_fCall++;
	NOTICE("Granting permission %s by default of backend", permission ?: "(null)");
	return !!permission;
}
#endif

void afb_perm_check_async(
	struct afb_context *context,
	const char *permission,
	void (*callback)(void *closure, int status),
	void *closure
)
{AKA_fCall++;
	callback(closure, afb_perm_check(context, permission));
}

#endif

