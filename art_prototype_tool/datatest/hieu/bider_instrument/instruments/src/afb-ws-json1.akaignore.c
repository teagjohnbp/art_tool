/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_WS_JSON1_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_WS_JSON1_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <string.h>

#include <json-c/json.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_WSJ1_H_
#define AKA_INCLUDE__AFB_WSJ1_H_
#include "afb-wsj1.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_WS_JSON1_H_
#define AKA_INCLUDE__AFB_WS_JSON1_H_
#include "afb-ws-json1.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_MSG_JSON_H_
#define AKA_INCLUDE__AFB_MSG_JSON_H_
#include "afb-msg-json.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SESSION_H_
#define AKA_INCLUDE__AFB_SESSION_H_
#include "afb-session.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CRED_H_
#define AKA_INCLUDE__AFB_CRED_H_
#include "afb-cred.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_APISET_H_
#define AKA_INCLUDE__AFB_APISET_H_
#include "afb-apiset.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_XREQ_H_
#define AKA_INCLUDE__AFB_XREQ_H_
#include "afb-xreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CONTEXT_H_
#define AKA_INCLUDE__AFB_CONTEXT_H_
#include "afb-context.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_EVT_H_
#define AKA_INCLUDE__AFB_EVT_H_
#include "afb-evt.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_TOKEN_H_
#define AKA_INCLUDE__AFB_TOKEN_H_
#include "afb-token.akaignore.h"
#endif


/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__SYSTEMD_H_
#define AKA_INCLUDE__SYSTEMD_H_
#include "systemd.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_H_
#define AKA_INCLUDE__FDEV_H_
#include "fdev.akaignore.h"
#endif


/* predeclaration of structures */
struct afb_ws_json1;
struct afb_wsreq;

/* predeclaration of websocket callbacks */
static void aws_on_hangup_cb(void *closure, struct afb_wsj1 *wsj1);
static void aws_on_call_cb(void *closure, const char *api, const char *verb, struct afb_wsj1_msg *msg);
static void aws_on_push_cb(void *closure, const char *event, uint16_t eventid, struct json_object *object);
static void aws_on_broadcast_cb(void *closure, const char *event, struct json_object *object, const uuid_binary_t uuid, uint8_t hop);

/* predeclaration of wsreq callbacks */
static void wsreq_destroy(struct afb_xreq *xreq);
static void wsreq_reply(struct afb_xreq *xreq, struct json_object *object, const char *error, const char *info);
static int wsreq_subscribe(struct afb_xreq *xreq, struct afb_event_x2 *event);
static int wsreq_unsubscribe(struct afb_xreq *xreq, struct afb_event_x2 *event);

/* declaration of websocket structure */
struct afb_ws_json1
{
	int refcount;
	void (*cleanup)(void*);
	void *cleanup_closure;
	struct afb_session *session;
	struct afb_token *token;
	struct afb_evt_listener *listener;
	struct afb_wsj1 *wsj1;
	struct afb_cred *cred;
	struct afb_apiset *apiset;
};

/* declaration of wsreq structure */
struct afb_wsreq
{
	struct afb_xreq xreq;
	struct afb_ws_json1 *aws;
	struct afb_wsreq *next;
	struct afb_wsj1_msg *msgj1;
};

/* interface for afb_ws_json1 / afb_wsj1 */
static struct afb_wsj1_itf wsj1_itf = {
	.on_hangup = aws_on_hangup_cb,
	.on_call = aws_on_call_cb
};

/* interface for xreq */
const struct afb_xreq_query_itf afb_ws_json1_xreq_itf = {
	.reply = wsreq_reply,
	.subscribe = wsreq_subscribe,
	.unsubscribe = wsreq_unsubscribe,
	.unref = wsreq_destroy
};

/* the interface for events */
static const struct afb_evt_itf evt_itf = {
	.broadcast = aws_on_broadcast_cb,
	.push = aws_on_push_cb
};

/***************************************************************
****************************************************************
**
**  functions of afb_ws_json1 / afb_wsj1
**
****************************************************************
***************************************************************/

/** Instrumented function afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*) */
struct afb_ws_json1 *afb_ws_json1_create(struct fdev *fdev, struct afb_apiset *apiset, struct afb_context *context, void (*cleanup)(void*), void *cleanup_closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");AKA_fCall++;
		AKA_mark("lis===112###sois===3409###eois===3437###lif===2###soif===166###eoif===194###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");struct afb_ws_json1 *result;


		AKA_mark("lis===114###sois===3440###eois===3453###lif===4###soif===197###eoif===210###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");assert(fdev);

		AKA_mark("lis===115###sois===3455###eois===3479###lif===5###soif===212###eoif===236###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");assert(context != NULL);


		AKA_mark("lis===117###sois===3482###eois===3515###lif===7###soif===239###eoif===272###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");result = malloc(sizeof * result);

		if (AKA_mark("lis===118###sois===3521###eois===3535###lif===8###soif===278###eoif===292###ifc===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)") && (AKA_mark("lis===118###sois===3521###eois===3535###lif===8###soif===278###eoif===292###isc===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)")&&result == NULL)) {
		AKA_mark("lis===119###sois===3539###eois===3550###lif===9###soif===296###eoif===307###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");goto error;
	}
	else {AKA_mark("lis===-118-###sois===-3521-###eois===-352114-###lif===-8-###soif===-###eoif===-292-###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");}


		AKA_mark("lis===121###sois===3553###eois===3574###lif===11###soif===310###eoif===331###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");result->refcount = 1;

		AKA_mark("lis===122###sois===3576###eois===3602###lif===12###soif===333###eoif===359###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");result->cleanup = cleanup;

		AKA_mark("lis===123###sois===3604###eois===3646###lif===13###soif===361###eoif===403###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");result->cleanup_closure = cleanup_closure;

		AKA_mark("lis===124###sois===3648###eois===3703###lif===14###soif===405###eoif===460###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");result->session = afb_session_addref(context->session);

		AKA_mark("lis===125###sois===3705###eois===3754###lif===15###soif===462###eoif===511###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");result->token = afb_token_addref(context->token);

		if (AKA_mark("lis===126###sois===3760###eois===3783###lif===16###soif===517###eoif===540###ifc===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)") && (AKA_mark("lis===126###sois===3760###eois===3783###lif===16###soif===517###eoif===540###isc===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)")&&result->session == NULL)) {
		AKA_mark("lis===127###sois===3787###eois===3799###lif===17###soif===544###eoif===556###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");goto error2;
	}
	else {AKA_mark("lis===-126-###sois===-3760-###eois===-376023-###lif===-16-###soif===-###eoif===-540-###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");}


		AKA_mark("lis===129###sois===3802###eois===3858###lif===19###soif===559###eoif===615###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");result->wsj1 = afb_wsj1_create(fdev, &wsj1_itf, result);

		if (AKA_mark("lis===130###sois===3864###eois===3884###lif===20###soif===621###eoif===641###ifc===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)") && (AKA_mark("lis===130###sois===3864###eois===3884###lif===20###soif===621###eoif===641###isc===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)")&&result->wsj1 == NULL)) {
		AKA_mark("lis===131###sois===3888###eois===3900###lif===21###soif===645###eoif===657###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");goto error3;
	}
	else {AKA_mark("lis===-130-###sois===-3864-###eois===-386420-###lif===-20-###soif===-###eoif===-641-###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");}


		AKA_mark("lis===133###sois===3903###eois===3964###lif===23###soif===660###eoif===721###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");result->listener = afb_evt_listener_create(&evt_itf, result);

		if (AKA_mark("lis===134###sois===3970###eois===3994###lif===24###soif===727###eoif===751###ifc===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)") && (AKA_mark("lis===134###sois===3970###eois===3994###lif===24###soif===727###eoif===751###isc===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)")&&result->listener == NULL)) {
		AKA_mark("lis===135###sois===3998###eois===4010###lif===25###soif===755###eoif===767###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");goto error4;
	}
	else {AKA_mark("lis===-134-###sois===-3970-###eois===-397024-###lif===-24-###soif===-###eoif===-751-###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");}


		AKA_mark("lis===137###sois===4013###eois===4070###lif===27###soif===770###eoif===827###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");result->cred = afb_cred_create_for_socket(fdev_fd(fdev));

		AKA_mark("lis===138###sois===4072###eois===4115###lif===28###soif===829###eoif===872###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");result->apiset = afb_apiset_addref(apiset);

		AKA_mark("lis===139###sois===4117###eois===4131###lif===29###soif===874###eoif===888###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");return result;


	error4:
	AKA_mark("lis===142###sois===4142###eois===4171###lif===32###soif===899###eoif===928###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");afb_wsj1_unref(result->wsj1);

	error3:
	AKA_mark("lis===144###sois===4181###eois===4216###lif===34###soif===938###eoif===973###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");afb_session_unref(result->session);

		AKA_mark("lis===145###sois===4218###eois===4249###lif===35###soif===975###eoif===1006###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");afb_token_unref(result->token);

	error2:
	AKA_mark("lis===147###sois===4259###eois===4272###lif===37###soif===1016###eoif===1029###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");free(result);

	error:
	AKA_mark("lis===149###sois===4281###eois===4298###lif===39###soif===1038###eoif===1055###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");fdev_unref(fdev);

		AKA_mark("lis===150###sois===4300###eois===4312###lif===40###soif===1057###eoif===1069###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_create(struct fdev*,struct afb_apiset*,struct afb_context*,void(*cleanup)(void*),void*)");return NULL;

}

/** Instrumented function afb_ws_json1_addref(struct afb_ws_json1*) */
struct afb_ws_json1 *afb_ws_json1_addref(struct afb_ws_json1 *ws)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_addref(struct afb_ws_json1*)");AKA_fCall++;
		AKA_mark("lis===155###sois===4385###eois===4440###lif===2###soif===69###eoif===124###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_addref(struct afb_ws_json1*)");__atomic_add_fetch(&ws->refcount, 1, __ATOMIC_RELAXED);

		AKA_mark("lis===156###sois===4442###eois===4452###lif===3###soif===126###eoif===136###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_addref(struct afb_ws_json1*)");return ws;

}

/** Instrumented function afb_ws_json1_unref(struct afb_ws_json1*) */
void afb_ws_json1_unref(struct afb_ws_json1 *ws)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)");AKA_fCall++;
		if (AKA_mark("lis===161###sois===4512###eois===4567###lif===2###soif===56###eoif===111###ifc===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)") && (AKA_mark("lis===161###sois===4512###eois===4567###lif===2###soif===56###eoif===111###isc===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)")&&!__atomic_sub_fetch(&ws->refcount, 1, __ATOMIC_RELAXED))) {
				AKA_mark("lis===162###sois===4573###eois===4610###lif===3###soif===117###eoif===154###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)");afb_evt_listener_unref(ws->listener);

				AKA_mark("lis===163###sois===4613###eois===4638###lif===4###soif===157###eoif===182###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)");afb_wsj1_unref(ws->wsj1);

				if (AKA_mark("lis===164###sois===4645###eois===4664###lif===5###soif===189###eoif===208###ifc===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)") && (AKA_mark("lis===164###sois===4645###eois===4664###lif===5###soif===189###eoif===208###isc===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)")&&ws->cleanup != NULL)) {
			AKA_mark("lis===165###sois===4669###eois===4702###lif===6###soif===213###eoif===246###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)");ws->cleanup(ws->cleanup_closure);
		}
		else {AKA_mark("lis===-164-###sois===-4645-###eois===-464519-###lif===-5-###soif===-###eoif===-208-###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)");}

				AKA_mark("lis===166###sois===4705###eois===4732###lif===7###soif===249###eoif===276###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)");afb_token_unref(ws->token);

				AKA_mark("lis===167###sois===4735###eois===4766###lif===8###soif===279###eoif===310###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)");afb_session_unref(ws->session);

				AKA_mark("lis===168###sois===4769###eois===4794###lif===9###soif===313###eoif===338###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)");afb_cred_unref(ws->cred);

				AKA_mark("lis===169###sois===4797###eois===4826###lif===10###soif===341###eoif===370###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)");afb_apiset_unref(ws->apiset);

				AKA_mark("lis===170###sois===4829###eois===4838###lif===11###soif===373###eoif===382###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)");free(ws);

	}
	else {AKA_mark("lis===-161-###sois===-4512-###eois===-451255-###lif===-2-###soif===-###eoif===-111-###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/afb_ws_json1_unref(struct afb_ws_json1*)");}

}

/** Instrumented function aws_on_hangup_cb(void*,struct afb_wsj1*) */
static void aws_on_hangup_cb(void *closure, struct afb_wsj1 *wsj1)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-json1.c/aws_on_hangup_cb(void*,struct afb_wsj1*)");AKA_fCall++;
		AKA_mark("lis===176###sois===4915###eois===4949###lif===2###soif===70###eoif===104###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_hangup_cb(void*,struct afb_wsj1*)");struct afb_ws_json1 *ws = closure;

		AKA_mark("lis===177###sois===4951###eois===4974###lif===3###soif===106###eoif===129###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_hangup_cb(void*,struct afb_wsj1*)");afb_ws_json1_unref(ws);

}

/** Instrumented function aws_new_token(struct afb_ws_json1*,const char*) */
static int aws_new_token(struct afb_ws_json1 *ws, const char *new_token_string)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-json1.c/aws_new_token(struct afb_ws_json1*,const char*)");AKA_fCall++;
		AKA_mark("lis===182###sois===5061###eois===5068###lif===2###soif===83###eoif===90###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_new_token(struct afb_ws_json1*,const char*)");int rc;

		AKA_mark("lis===183###sois===5070###eois===5104###lif===3###soif===92###eoif===126###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_new_token(struct afb_ws_json1*,const char*)");struct afb_token *newtok, *oldtok;


		AKA_mark("lis===185###sois===5107###eois===5153###lif===5###soif===129###eoif===175###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_new_token(struct afb_ws_json1*,const char*)");rc = afb_token_get(&newtok, new_token_string);

		if (AKA_mark("lis===186###sois===5159###eois===5166###lif===6###soif===181###eoif===188###ifc===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_new_token(struct afb_ws_json1*,const char*)") && (AKA_mark("lis===186###sois===5159###eois===5166###lif===6###soif===181###eoif===188###isc===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_new_token(struct afb_ws_json1*,const char*)")&&rc >= 0)) {
				AKA_mark("lis===187###sois===5172###eois===5191###lif===7###soif===194###eoif===213###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_new_token(struct afb_ws_json1*,const char*)");oldtok = ws->token;

				AKA_mark("lis===188###sois===5194###eois===5213###lif===8###soif===216###eoif===235###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_new_token(struct afb_ws_json1*,const char*)");ws->token = newtok;

				AKA_mark("lis===189###sois===5216###eois===5240###lif===9###soif===238###eoif===262###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_new_token(struct afb_ws_json1*,const char*)");afb_token_unref(oldtok);

	}
	else {AKA_mark("lis===-186-###sois===-5159-###eois===-51597-###lif===-6-###soif===-###eoif===-188-###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_new_token(struct afb_ws_json1*,const char*)");}

		AKA_mark("lis===191###sois===5245###eois===5255###lif===11###soif===267###eoif===277###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_new_token(struct afb_ws_json1*,const char*)");return rc;

}

/** Instrumented function aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*) */
static void aws_on_call_cb(void *closure, const char *api, const char *verb, struct afb_wsj1_msg *msg)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");AKA_fCall++;
		AKA_mark("lis===196###sois===5365###eois===5399###lif===2###soif===106###eoif===140###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");struct afb_ws_json1 *ws = closure;

		AKA_mark("lis===197###sois===5401###eois===5425###lif===3###soif===142###eoif===166###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");struct afb_wsreq *wsreq;

		AKA_mark("lis===198###sois===5427###eois===5443###lif===4###soif===168###eoif===184###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");const char *tok;


		AKA_mark("lis===200###sois===5446###eois===5535###lif===6###soif===187###eoif===276###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");DEBUG("received websocket request for %s/%s: %s", api, verb, afb_wsj1_msg_object_s(msg));


	/* handle new tokens */
		AKA_mark("lis===203###sois===5563###eois===5593###lif===9###soif===304###eoif===334###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");tok = afb_wsj1_msg_token(msg);

		if (AKA_mark("lis===204###sois===5599###eois===5602###lif===10###soif===340###eoif===343###ifc===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)") && (AKA_mark("lis===204###sois===5599###eois===5602###lif===10###soif===340###eoif===343###isc===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)")&&tok)) {
		AKA_mark("lis===205###sois===5606###eois===5629###lif===11###soif===347###eoif===370###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");aws_new_token(ws, tok);
	}
	else {AKA_mark("lis===-204-###sois===-5599-###eois===-55993-###lif===-10-###soif===-###eoif===-343-###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");}


	/* allocate */
		AKA_mark("lis===208###sois===5648###eois===5681###lif===14###soif===389###eoif===422###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");wsreq = calloc(1, sizeof *wsreq);

		if (AKA_mark("lis===209###sois===5687###eois===5700###lif===15###soif===428###eoif===441###ifc===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)") && (AKA_mark("lis===209###sois===5687###eois===5700###lif===15###soif===428###eoif===441###isc===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)")&&wsreq == NULL)) {
				AKA_mark("lis===210###sois===5706###eois===5743###lif===16###soif===447###eoif===484###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");afb_wsj1_close(ws->wsj1, 1008, NULL);

				AKA_mark("lis===211###sois===5746###eois===5753###lif===17###soif===487###eoif===494###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");return;

	}
	else {AKA_mark("lis===-209-###sois===-5687-###eois===-568713-###lif===-15-###soif===-###eoif===-441-###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");}


	/* init the context */
		AKA_mark("lis===215###sois===5783###eois===5835###lif===21###soif===524###eoif===576###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");afb_xreq_init(&wsreq->xreq, &afb_ws_json1_xreq_itf);

		AKA_mark("lis===216###sois===5837###eois===5910###lif===22###soif===578###eoif===651###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");afb_context_init(&wsreq->xreq.context, ws->session, ws->token, ws->cred);


	/* fill and record the request */
		AKA_mark("lis===219###sois===5948###eois===5973###lif===25###soif===689###eoif===714###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");afb_wsj1_msg_addref(msg);

		AKA_mark("lis===220###sois===5975###eois===5994###lif===26###soif===716###eoif===735###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");wsreq->msgj1 = msg;

		AKA_mark("lis===221###sois===5996###eois===6033###lif===27###soif===737###eoif===774###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");wsreq->xreq.request.called_api = api;

		AKA_mark("lis===222###sois===6035###eois===6074###lif===28###soif===776###eoif===815###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");wsreq->xreq.request.called_verb = verb;

		AKA_mark("lis===223###sois===6076###eois===6131###lif===29###soif===817###eoif===872###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");wsreq->xreq.json = afb_wsj1_msg_object_j(wsreq->msgj1);

		AKA_mark("lis===224###sois===6133###eois===6170###lif===30###soif===874###eoif===911###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");wsreq->aws = afb_ws_json1_addref(ws);


	/* emits the call */
		AKA_mark("lis===227###sois===6195###eois===6238###lif===33###soif===936###eoif===979###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_call_cb(void*,const char*,const char*,struct afb_wsj1_msg*)");afb_xreq_process(&wsreq->xreq, ws->apiset);

}

/** Instrumented function aws_on_event(struct afb_ws_json1*,const char*,struct json_object*) */
static void aws_on_event(struct afb_ws_json1 *aws, const char *event, struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-json1.c/aws_on_event(struct afb_ws_json1*,const char*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===232###sois===6343###eois===6418###lif===2###soif===101###eoif===176###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_event(struct afb_ws_json1*,const char*,struct json_object*)");afb_wsj1_send_event_j(aws->wsj1, event, afb_msg_json_event(event, object));

}

/** Instrumented function aws_on_push_cb(void*,const char*,uint16_t,struct json_object*) */
static void aws_on_push_cb(void *closure, const char *event, uint16_t eventid, struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-json1.c/aws_on_push_cb(void*,const char*,uint16_t,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===237###sois===6532###eois===6569###lif===2###soif===110###eoif===147###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_push_cb(void*,const char*,uint16_t,struct json_object*)");aws_on_event(closure, event, object);

}

/** Instrumented function aws_on_broadcast_cb(void*,const char*,struct json_object*,uuid_binary_t,uint8_t) */
static void aws_on_broadcast_cb(void *closure, const char *event, struct json_object *object, const uuid_binary_t uuid, uint8_t hop)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-json1.c/aws_on_broadcast_cb(void*,const char*,struct json_object*,uuid_binary_t,uint8_t)");AKA_fCall++;
		AKA_mark("lis===242###sois===6709###eois===6773###lif===2###soif===136###eoif===200###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/aws_on_broadcast_cb(void*,const char*,struct json_object*,uuid_binary_t,uint8_t)");aws_on_event(closure, event, afb_msg_json_event(event, object));

}

/***************************************************************
****************************************************************
**
**  functions of wsreq / afb_req
**
****************************************************************
***************************************************************/

/** Instrumented function wsreq_destroy(struct afb_xreq*) */
static void wsreq_destroy(struct afb_xreq *xreq)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-json1.c/wsreq_destroy(struct afb_xreq*)");AKA_fCall++;
		AKA_mark("lis===255###sois===7129###eois===7145###lif===2###soif===52###eoif===68###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsre/* Cant instrument this following code */
q_destroy(struct afb_xreq*)");struct afb_wsreq
 /* Cant instrument this following code */
*wsreq = CONTAINER_OF_XREQ(struct afb_wsreq, xreq);

		AKA_mark("lis===257###sois===7200###eois===7245###lif===4###soif===123###eoif===168###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsreq_destroy(struct afb_xreq*)");afb_context_disconnect(&wsreq->xreq.context);

		AKA_mark("lis===258###sois===7247###eois===7280###lif===5###soif===170###eoif===203###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsreq_destroy(struct afb_xreq*)");afb_wsj1_msg_unref(wsreq->msgj1);

		AKA_mark("lis===259###sois===7282###eois===7313###lif===6###soif===205###eoif===236###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsreq_destroy(struct afb_xreq*)");afb_ws_json1_unref(wsreq->aws);

		AKA_mark("lis===260###sois===7315###eois===7327###lif===7###soif===238###eoif===250###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsreq_destroy(struct afb_xreq*)");free(wsreq);

}

/** Instrumented function wsreq_reply(struct afb_xreq*,struct json_object*,const char*,const char*) */
static void wsreq_reply(struct afb_xreq *xreq, struct json_object *object, const char *error, const char *info)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-json1.c/wsreq_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===265###sois===7446###eois===7462###lif===2###soif===115###eoif===131###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsre/* Cant instrument this following code */
q_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");struct afb_wsreq
 /* Cant instrument this following code */
*wsreq = CONTAINER_OF_XREQ(struct afb_wsreq, xreq);
		AKA_mark("lis===266###sois===7516###eois===7523###lif===3###soif===185###eoif===192###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsreq_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");int rc;

		AKA_mark("lis===267###sois===7525###eois===7551###lif===4###soif===194###eoif===220###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsreq_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");struct json_object *reply;


	/* create the reply */
		AKA_mark("lis===270###sois===7578###eois===7642###lif===7###soif===247###eoif===311###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsreq_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");reply = afb_msg_json_reply(object, error, info, &xreq->context);


		AKA_mark("lis===272###sois===7645###eois===7736###lif===9###soif===314###eoif===405###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsreq_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");rc = (error ? afb_wsj1_reply_error_j : afb_wsj1_reply_ok_j)(
			wsreq->msgj1, reply, NULL);

		if (AKA_mark("lis===274###sois===7742###eois===7744###lif===11###soif===411###eoif===413###ifc===true###function===./app-framework-binder/src/afb-ws-json1.c/wsreq_reply(struct afb_xreq*,struct json_object*,const char*,const char*)") && (AKA_mark("lis===274###sois===7742###eois===7744###lif===11###soif===411###eoif===413###isc===true###function===./app-framework-binder/src/afb-ws-json1.c/wsreq_reply(struct afb_xreq*,struct json_object*,const char*,const char*)")&&rc)) {
		AKA_mark("lis===275###sois===7748###eois===7778###lif===12###soif===417###eoif===447###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsreq_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");ERROR("Can't send reply: %m");
	}
	else {AKA_mark("lis===-274-###sois===-7742-###eois===-77422-###lif===-11-###soif===-###eoif===-413-###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsreq_reply(struct afb_xreq*,struct json_object*,const char*,const char*)");}

}

/** Instrumented function wsreq_subscribe(struct afb_xreq*,struct afb_event_x2*) */
static int wsreq_subscribe(struct afb_xreq *xreq, struct afb_event_x2 *event)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-json1.c/wsreq_subscribe(struct afb_xreq*,struct afb_event_x2*)");AKA_fCall++;
		AKA_mark("lis===280###sois===7863###eois===7879###lif===2###soif===81###eoif===97###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsre/* Cant instrument this following code */
q_subscribe(struct afb_xreq*,struct afb_event_x2*)");struct afb_wsreq
 /* Cant instrument this following code */
*wsreq = CONTAINER_OF_XREQ(struct afb_wsreq, xreq);

		AKA_mark("lis===282###sois===7934###eois===7996###lif===4###soif===152###eoif===214###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsreq_subscribe(struct afb_xreq*,struct afb_event_x2*)");return afb_evt_listener_watch_x2(wsreq->aws->listener, event);

}

/** Instrumented function wsreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*) */
static int wsreq_unsubscribe(struct afb_xreq *xreq, struct afb_event_x2 *event)
{AKA_mark("Calling: ./app-framework-binder/src/afb-ws-json1.c/wsreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*)");AKA_fCall++;
		AKA_mark("lis===287###sois===8083###eois===8099###lif===2###soif===83###eoif===99###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsre/* Cant instrument this following code */
q_unsubscribe(struct afb_xreq*,struct afb_event_x2*)");struct afb_wsreq
 /* Cant instrument this following code */
*wsreq = CONTAINER_OF_XREQ(struct afb_wsreq, xreq);

		AKA_mark("lis===289###sois===8154###eois===8218###lif===4###soif===154###eoif===218###ins===true###function===./app-framework-binder/src/afb-ws-json1.c/wsreq_unsubscribe(struct afb_xreq*,struct afb_event_x2*)");return afb_evt_listener_unwatch_x2(wsreq->aws->listener, event);

}


#endif

