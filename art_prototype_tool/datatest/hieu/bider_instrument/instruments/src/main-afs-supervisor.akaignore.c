/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_MAIN_AFS_SUPERVISOR_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_MAIN_AFS_SUPERVISOR_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

#include <systemd/sd-daemon.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_COMMON_H_
#define AKA_INCLUDE__AFB_COMMON_H_
#include "afb-common.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HSRV_H_
#define AKA_INCLUDE__AFB_HSRV_H_
#include "afb-hsrv.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HSWITCH_H_
#define AKA_INCLUDE__AFB_HSWITCH_H_
#include "afb-hswitch.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HREQ_H_
#define AKA_INCLUDE__AFB_HREQ_H_
#include "afb-hreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_APISET_H_
#define AKA_INCLUDE__AFB_APISET_H_
#include "afb-apiset.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_WS_H_
#define AKA_INCLUDE__AFB_API_WS_H_
#include "afb-api-ws.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SESSION_H_
#define AKA_INCLUDE__AFB_SESSION_H_
#include "afb-session.akaignore.h"
#endif


/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFS_SUPERVISOR_H_
#define AKA_INCLUDE__AFS_SUPERVISOR_H_
#include "afs-supervisor.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFS_ARGS_H_
#define AKA_INCLUDE__AFS_ARGS_H_
#include "afs-args.akaignore.h"
#endif


/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__JOBS_H_
#define AKA_INCLUDE__JOBS_H_
#include "jobs.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__PROCESS_NAME_H_
#define AKA_INCLUDE__PROCESS_NAME_H_
#include "process-name.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__WATCHDOG_H_
#define AKA_INCLUDE__WATCHDOG_H_
#include "watchdog.akaignore.h"
#endif


#if !defined(DEFAULT_SUPERVISOR_INTERFACE)
#  define DEFAULT_SUPERVISOR_INTERFACE NULL
#endif

/* the main config */
struct afs_args *main_config;

/* the main apiset */
struct afb_apiset *main_apiset;

/*************************************************************************************/

/** Instrumented function init_http_server(struct afb_hsrv*) */
static int init_http_server(struct afb_hsrv *hsrv)
{AKA_mark("Calling: ./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)");AKA_fCall++;
		if (AKA_mark("lis===58###sois===1455###eois===1557###lif===2###soif===58###eoif===160###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)") && (AKA_mark("lis===58###sois===1455###eois===1557###lif===2###soif===58###eoif===160###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)")&&!afb_hsrv_add_handler
	    (hsrv, main_config->rootapi, afb_hswitch_websocket_switch, main_apiset, 20))) {
		AKA_mark("lis===60###sois===1561###eois===1570###lif===4###soif===164###eoif===173###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)");return 0;
	}
	else {AKA_mark("lis===-58-###sois===-1455-###eois===-1455102-###lif===-2-###soif===-###eoif===-160-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)");}


		if (AKA_mark("lis===62###sois===1577###eois===1667###lif===6###soif===180###eoif===270###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)") && (AKA_mark("lis===62###sois===1577###eois===1667###lif===6###soif===180###eoif===270###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)")&&!afb_hsrv_add_handler
	    (hsrv, main_config->rootapi, afb_hswitch_apis, main_apiset, 10))) {
		AKA_mark("lis===64###sois===1671###eois===1680###lif===8###soif===274###eoif===283###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)");return 0;
	}
	else {AKA_mark("lis===-62-###sois===-1577-###eois===-157790-###lif===-6-###soif===-###eoif===-270-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)");}


		if (AKA_mark("lis===66###sois===1687###eois===1716###lif===10###soif===290###eoif===319###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)") && (AKA_mark("lis===66###sois===1687###eois===1716###lif===10###soif===290###eoif===319###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)")&&main_config->roothttp != NULL)) {
				if (AKA_mark("lis===67###sois===1726###eois===1829###lif===11###soif===329###eoif===432###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)") && (AKA_mark("lis===67###sois===1726###eois===1829###lif===11###soif===329###eoif===432###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)")&&!afb_hsrv_add_alias
		    (hsrv, "", afb_common_rootdir_get_fd(), main_config->roothttp,
		     -10, 1))) {
			AKA_mark("lis===70###sois===1834###eois===1843###lif===14###soif===437###eoif===446###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)");return 0;
		}
		else {AKA_mark("lis===-67-###sois===-1726-###eois===-1726103-###lif===-11-###soif===-###eoif===-432-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)");}

	}
	else {AKA_mark("lis===-66-###sois===-1687-###eois===-168729-###lif===-10-###soif===-###eoif===-319-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)");}


		if (AKA_mark("lis===73###sois===1853###eois===1961###lif===17###soif===456###eoif===564###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)") && (AKA_mark("lis===73###sois===1853###eois===1961###lif===17###soif===456###eoif===564###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)")&&!afb_hsrv_add_handler
	    (hsrv, main_config->rootbase, afb_hswitch_one_page_api_redirect, NULL,
	     -20))) {
		AKA_mark("lis===76###sois===1965###eois===1974###lif===20###soif===568###eoif===577###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)");return 0;
	}
	else {AKA_mark("lis===-73-###sois===-1853-###eois===-1853108-###lif===-17-###soif===-###eoif===-564-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)");}


		AKA_mark("lis===78###sois===1977###eois===1986###lif===22###soif===580###eoif===589###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/init_http_server(struct afb_hsrv*)");return 1;

}

/** Instrumented function start_http_server() */
static struct afb_hsrv *start_http_server()
{AKA_mark("Calling: ./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");AKA_fCall++;
		AKA_mark("lis===83###sois===2037###eois===2044###lif===2###soif===47###eoif===54###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");int rc;

		AKA_mark("lis===84###sois===2046###eois===2068###lif===3###soif===56###eoif===78###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");struct afb_hsrv *hsrv;


		if (AKA_mark("lis===86###sois===2075###eois===2126###lif===5###soif===85###eoif===136###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()") && (AKA_mark("lis===86###sois===2075###eois===2126###lif===5###soif===85###eoif===136###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()")&&afb_hreq_init_download_path(main_config->uploaddir))) {
				AKA_mark("lis===87###sois===2132###eois===2203###lif===6###soif===142###eoif===213###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");ERROR("unable to set the upload directory %s", main_config->uploaddir);

				AKA_mark("lis===88###sois===2206###eois===2218###lif===7###soif===216###eoif===228###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");return NULL;

	}
	else {AKA_mark("lis===-86-###sois===-2075-###eois===-207551-###lif===-5-###soif===-###eoif===-136-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");}


		AKA_mark("lis===91###sois===2224###eois===2249###lif===10###soif===234###eoif===259###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");hsrv = afb_hsrv_create();

		if (AKA_mark("lis===92###sois===2255###eois===2267###lif===11###soif===265###eoif===277###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()") && (AKA_mark("lis===92###sois===2255###eois===2267###lif===11###soif===265###eoif===277###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()")&&hsrv == NULL)) {
				AKA_mark("lis===93###sois===2273###eois===2308###lif===12###soif===283###eoif===318###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");ERROR("memory allocation failure");

				AKA_mark("lis===94###sois===2311###eois===2323###lif===13###soif===321###eoif===333###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");return NULL;

	}
	else {AKA_mark("lis===-92-###sois===-2255-###eois===-225512-###lif===-11-###soif===-###eoif===-277-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");}


		if (AKA_mark("lis===97###sois===2333###eois===2425###lif===16###soif===343###eoif===435###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()") && ((AKA_mark("lis===97###sois===2333###eois===2393###lif===16###soif===343###eoif===403###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()")&&!afb_hsrv_set_cache_timeout(hsrv, main_config->cacheTimeout))	||(AKA_mark("lis===98###sois===2402###eois===2425###lif===17###soif===412###eoif===435###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()")&&!init_http_server(hsrv)))) {
				AKA_mark("lis===99###sois===2431###eois===2471###lif===18###soif===441###eoif===481###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");ERROR("initialisation of httpd failed");

				AKA_mark("lis===100###sois===2474###eois===2493###lif===19###soif===484###eoif===503###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");afb_hsrv_put(hsrv);

				AKA_mark("lis===101###sois===2496###eois===2508###lif===20###soif===506###eoif===518###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");return NULL;

	}
	else {AKA_mark("lis===-97-###sois===-2333-###eois===-233392-###lif===-16-###soif===-###eoif===-435-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");}


		AKA_mark("lis===104###sois===2514###eois===2597###lif===23###soif===524###eoif===607###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");NOTICE("Waiting port=%d rootdir=%s", main_config->httpdPort, main_config->rootdir);

		AKA_mark("lis===105###sois===2599###eois===2666###lif===24###soif===609###eoif===676###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");NOTICE("Browser URL= http://localhost:%d", main_config->httpdPort);


		AKA_mark("lis===107###sois===2669###eois===2699###lif===26###soif===679###eoif===709###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");rc = afb_hsrv_start(hsrv, 15);

		if (AKA_mark("lis===108###sois===2705###eois===2708###lif===27###soif===715###eoif===718###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()") && (AKA_mark("lis===108###sois===2705###eois===2708###lif===27###soif===715###eoif===718###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()")&&!rc)) {
				AKA_mark("lis===109###sois===2714###eois===2748###lif===28###soif===724###eoif===758###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");ERROR("starting of httpd failed");

				AKA_mark("lis===110###sois===2751###eois===2770###lif===29###soif===761###eoif===780###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");afb_hsrv_put(hsrv);

				AKA_mark("lis===111###sois===2773###eois===2785###lif===30###soif===783###eoif===795###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");return NULL;

	}
	else {AKA_mark("lis===-108-###sois===-2705-###eois===-27053-###lif===-27-###soif===-###eoif===-718-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");}


		AKA_mark("lis===114###sois===2791###eois===2894###lif===33###soif===801###eoif===904###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");rc = afb_hsrv_add_interface_tcp(hsrv, DEFAULT_SUPERVISOR_INTERFACE, (uint16_t) main_config->httpdPort);

		if (AKA_mark("lis===115###sois===2900###eois===2903###lif===34###soif===910###eoif===913###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()") && (AKA_mark("lis===115###sois===2900###eois===2903###lif===34###soif===910###eoif===913###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()")&&!rc)) {
				AKA_mark("lis===116###sois===2909###eois===2943###lif===35###soif===919###eoif===953###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");ERROR("setting interface failed");

				AKA_mark("lis===117###sois===2946###eois===2965###lif===36###soif===956###eoif===975###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");afb_hsrv_put(hsrv);

				AKA_mark("lis===118###sois===2968###eois===2980###lif===37###soif===978###eoif===990###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");return NULL;

	}
	else {AKA_mark("lis===-115-###sois===-2900-###eois===-29003-###lif===-34-###soif===-###eoif===-913-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");}


		AKA_mark("lis===121###sois===2986###eois===2998###lif===40###soif===996###eoif===1008###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start_http_server()");return hsrv;

}

/** Instrumented function start(int,void*) */
static void start(int signum, void *arg)
{AKA_mark("Calling: ./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");AKA_fCall++;
		AKA_mark("lis===126###sois===3046###eois===3068###lif===2###soif===44###eoif===66###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");struct afb_hsrv *hsrv;

		AKA_mark("lis===127###sois===3070###eois===3077###lif===3###soif===68###eoif===75###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");int rc;


	/* check illness */
		if (AKA_mark("lis===130###sois===3105###eois===3111###lif===6###soif===103###eoif===109###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)") && (AKA_mark("lis===130###sois===3105###eois===3111###lif===6###soif===103###eoif===109###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)")&&signum)) {
				AKA_mark("lis===131###sois===3117###eois===3179###lif===7###soif===115###eoif===177###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");ERROR("start aborted: received signal %s", strsignal(signum));

				AKA_mark("lis===132###sois===3182###eois===3190###lif===8###soif===180###eoif===188###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");exit(1);

	}
	else {AKA_mark("lis===-130-###sois===-3105-###eois===-31056-###lif===-6-###soif===-###eoif===-109-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");}


	/* set the directories */
		AKA_mark("lis===136###sois===3223###eois===3280###lif===12###soif===221###eoif===278###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");mkdir(main_config->workdir, S_IRWXU | S_IRGRP | S_IXGRP);

		if (AKA_mark("lis===137###sois===3286###eois===3317###lif===13###soif===284###eoif===315###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)") && (AKA_mark("lis===137###sois===3286###eois===3317###lif===13###soif===284###eoif===315###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)")&&chdir(main_config->workdir) < 0)) {
				AKA_mark("lis===138###sois===3323###eois===3381###lif===14###soif===321###eoif===379###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");ERROR("Can't enter working dir %s", main_config->workdir);

				AKA_mark("lis===139###sois===3384###eois===3395###lif===15###soif===382###eoif===393###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");goto error;

	}
	else {AKA_mark("lis===-137-###sois===-3286-###eois===-328631-###lif===-13-###soif===-###eoif===-315-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");}

		if (AKA_mark("lis===141###sois===3404###eois===3452###lif===17###soif===402###eoif===450###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)") && (AKA_mark("lis===141###sois===3404###eois===3452###lif===17###soif===402###eoif===450###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)")&&afb_common_rootdir_set(main_config->rootdir) < 0)) {
				AKA_mark("lis===142###sois===3458###eois===3503###lif===18###soif===456###eoif===501###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");ERROR("failed to set common root directory");

				AKA_mark("lis===143###sois===3506###eois===3517###lif===19###soif===504###eoif===515###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");goto error;

	}
	else {AKA_mark("lis===-141-###sois===-3404-###eois===-340448-###lif===-17-###soif===-###eoif===-450-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");}


	/* configure the daemon */
		if (AKA_mark("lis===147###sois===3555###eois===3624###lif===23###soif===553###eoif===622###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)") && (AKA_mark("lis===147###sois===3555###eois===3624###lif===23###soif===553###eoif===622###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)")&&afb_session_init(main_config->nbSessionMax, main_config->cntxTimeout))) {
				AKA_mark("lis===148###sois===3630###eois===3680###lif===24###soif===628###eoif===678###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");ERROR("initialisation of session manager failed");

				AKA_mark("lis===149###sois===3683###eois===3694###lif===25###soif===681###eoif===692###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");goto error;

	}
	else {AKA_mark("lis===-147-###sois===-3555-###eois===-355569-###lif===-23-###soif===-###eoif===-622-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");}


		AKA_mark("lis===152###sois===3700###eois===3765###lif===28###soif===698###eoif===763###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");main_apiset = afb_apiset_create("main", main_config->apiTimeout);

		if (AKA_mark("lis===153###sois===3771###eois===3783###lif===29###soif===769###eoif===781###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)") && (AKA_mark("lis===153###sois===3771###eois===3783###lif===29###soif===769###eoif===781###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)")&&!main_apiset)) {
				AKA_mark("lis===154###sois===3789###eois===3823###lif===30###soif===787###eoif===821###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");ERROR("can't create main apiset");

				AKA_mark("lis===155###sois===3826###eois===3837###lif===31###soif===824###eoif===835###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");goto error;

	}
	else {AKA_mark("lis===-153-###sois===-3771-###eois===-377112-###lif===-29-###soif===-###eoif===-781-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");}


	/* init the main apiset */
		AKA_mark("lis===159###sois===3871###eois===3921###lif===35###soif===869###eoif===919###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");rc = afs_supervisor_add(main_apiset, main_apiset);

		if (AKA_mark("lis===160###sois===3927###eois===3933###lif===36###soif===925###eoif===931###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)") && (AKA_mark("lis===160###sois===3927###eois===3933###lif===36###soif===925###eoif===931###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)")&&rc < 0)) {
				AKA_mark("lis===161###sois===3939###eois===3986###lif===37###soif===937###eoif===984###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");ERROR("Can't create supervision's apiset: %m");

				AKA_mark("lis===162###sois===3989###eois===4000###lif===38###soif===987###eoif===998###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");goto error;

	}
	else {AKA_mark("lis===-160-###sois===-3927-###eois===-39276-###lif===-36-###soif===-###eoif===-931-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");}


	/* export the service if required */
		if (AKA_mark("lis===166###sois===4048###eois===4070###lif===42###soif===1046###eoif===1068###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)") && (AKA_mark("lis===166###sois===4048###eois===4070###lif===42###soif===1046###eoif===1068###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)")&&main_config->ws_server)) {
				AKA_mark("lis===167###sois===4076###eois===4153###lif===43###soif===1074###eoif===1151###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");rc = afb_api_ws_add_server(main_config->ws_server, main_apiset, main_apiset);

				if (AKA_mark("lis===168###sois===4160###eois===4166###lif===44###soif===1158###eoif===1164###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)") && (AKA_mark("lis===168###sois===4160###eois===4166###lif===44###soif===1158###eoif===1164###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)")&&rc < 0)) {
						AKA_mark("lis===169###sois===4173###eois===4242###lif===45###soif===1171###eoif===1240###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");ERROR("Can't export (ws-server) api %s: %m", main_config->ws_server);

						AKA_mark("lis===170###sois===4246###eois===4257###lif===46###soif===1244###eoif===1255###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");goto error;

		}
		else {AKA_mark("lis===-168-###sois===-4160-###eois===-41606-###lif===-44-###soif===-###eoif===-1164-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");}

	}
	else {AKA_mark("lis===-166-###sois===-4048-###eois===-404822-###lif===-42-###soif===-###eoif===-1068-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");}


	/* start the services */
		if (AKA_mark("lis===175###sois===4297###eois===4343###lif===51###soif===1295###eoif===1341###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)") && (AKA_mark("lis===175###sois===4297###eois===4343###lif===51###soif===1295###eoif===1341###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)")&&afb_apiset_start_all_services(main_apiset) < 0)) {
		AKA_mark("lis===176###sois===4347###eois===4358###lif===52###soif===1345###eoif===1356###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");goto error;
	}
	else {AKA_mark("lis===-175-###sois===-4297-###eois===-429746-###lif===-51-###soif===-###eoif===-1341-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");}


	/* start the HTTP server */
		if (AKA_mark("lis===179###sois===4394###eois===4421###lif===55###soif===1392###eoif===1419###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)") && (AKA_mark("lis===179###sois===4394###eois===4421###lif===55###soif===1392###eoif===1419###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)")&&main_config->httpdPort <= 0)) {
				AKA_mark("lis===180###sois===4427###eois===4455###lif===56###soif===1425###eoif===1453###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");ERROR("no port is defined");

				AKA_mark("lis===181###sois===4458###eois===4469###lif===57###soif===1456###eoif===1467###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");goto error;

	}
	else {AKA_mark("lis===-179-###sois===-4394-###eois===-439427-###lif===-55-###soif===-###eoif===-1419-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");}


		if (AKA_mark("lis===184###sois===4479###eois===4572###lif===60###soif===1477###eoif===1570###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)") && (AKA_mark("lis===184###sois===4479###eois===4572###lif===60###soif===1477###eoif===1570###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)")&&!afb_hreq_init_cookie(main_config->httpdPort, main_config->rootapi, main_config->cntxTimeout))) {
				AKA_mark("lis===185###sois===4578###eois===4625###lif===61###soif===1576###eoif===1623###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");ERROR("initialisation of HTTP cookies failed");

				AKA_mark("lis===186###sois===4628###eois===4639###lif===62###soif===1626###eoif===1637###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");goto error;

	}
	else {AKA_mark("lis===-184-###sois===-4479-###eois===-447993-###lif===-60-###soif===-###eoif===-1570-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");}


		AKA_mark("lis===189###sois===4645###eois===4672###lif===65###soif===1643###eoif===1670###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");hsrv = start_http_server();

		if (AKA_mark("lis===190###sois===4678###eois===4690###lif===66###soif===1676###eoif===1688###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)") && (AKA_mark("lis===190###sois===4678###eois===4690###lif===66###soif===1676###eoif===1688###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)")&&hsrv == NULL)) {
		AKA_mark("lis===191###sois===4694###eois===4705###lif===67###soif===1692###eoif===1703###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");goto error;
	}
	else {AKA_mark("lis===-190-###sois===-4678-###eois===-467812-###lif===-66-###soif===-###eoif===-1688-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");}


	/* ready */
		AKA_mark("lis===194###sois===4721###eois===4745###lif===70###soif===1719###eoif===1743###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");sd_notify(1, "READY=1");


	/* activate the watchdog */
#if HAS_WATCHDOG
		if (AKA_mark("lis===198###sois===4798###eois===4821###lif===74###soif===1796###eoif===1819###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)") && (AKA_mark("lis===198###sois===4798###eois===4821###lif===74###soif===1796###eoif===1819###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)")&&watchdog_activate() < 0)) {
		AKA_mark("lis===199###sois===4825###eois===4859###lif===75###soif===1823###eoif===1857###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");ERROR("can't start the watchdog");
	}
	else {AKA_mark("lis===-198-###sois===-4798-###eois===-479823-###lif===-74-###soif===-###eoif===-1819-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");}

#endif

	/* discover binders */
		AKA_mark("lis===203###sois===4893###eois===4919###lif===79###soif===1891###eoif===1917###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");afs_supervisor_discover();

		AKA_mark("lis===204###sois===4921###eois===4928###lif===80###soif===1919###eoif===1926###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");return;

	error:
	AKA_mark("lis===206###sois===4937###eois===4945###lif===82###soif===1935###eoif===1943###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/start(int,void*)");exit(1);

}

/**
 * initialize the supervision
 */
/** Instrumented function main(int,char**) */
int AKA_MAIN(int ac, char **av)
{AKA_mark("Calling: ./app-framework-binder/src/main-afs-supervisor.c/main(int,char**)");AKA_fCall++;
	/* scan arguments */
		AKA_mark("lis===215###sois===5040###eois===5077###lif===3###soif===53###eoif===90###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/main(int,char**)");main_config = afs_args_parse(ac, av);

		if (AKA_mark("lis===216###sois===5083###eois===5100###lif===4###soif===96###eoif===113###ifc===true###function===./app-framework-binder/src/main-afs-supervisor.c/main(int,char**)") && (AKA_mark("lis===216###sois===5083###eois===5100###lif===4###soif===96###eoif===113###isc===true###function===./app-framework-binder/src/main-afs-supervisor.c/main(int,char**)")&&main_config->name)) {
				AKA_mark("lis===217###sois===5106###eois===5145###lif===5###soif===119###eoif===158###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/main(int,char**)");verbose_set_name(main_config->name, 0);

				AKA_mark("lis===218###sois===5148###eois===5189###lif===6###soif===161###eoif===202###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/main(int,char**)");process_name_set_name(main_config->name);

				AKA_mark("lis===219###sois===5192###eois===5244###lif===7###soif===205###eoif===257###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/main(int,char**)");process_name_replace_cmdline(av, main_config->name);

	}
	else {AKA_mark("lis===-216-###sois===-5083-###eois===-508317-###lif===-4-###soif===-###eoif===-113-###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/main(int,char**)");}

	/* enter job processing */
		AKA_mark("lis===222###sois===5277###eois===5312###lif===10###soif===290###eoif===325###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/main(int,char**)");jobs_start(3, 0, 10, start, av[1]);

		AKA_mark("lis===223###sois===5314###eois===5370###lif===11###soif===327###eoif===383###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/main(int,char**)");WARNING("hoops returned from jobs_enter! [report bug]");

		AKA_mark("lis===224###sois===5372###eois===5381###lif===12###soif===385###eoif===394###ins===true###function===./app-framework-binder/src/main-afs-supervisor.c/main(int,char**)");return 1;

}


#endif

