/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_APISET_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_APISET_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_H_
#define AKA_INCLUDE__AFB_API_H_
#include "afb-api.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_APISET_H_
#define AKA_INCLUDE__AFB_APISET_H_
#include "afb-apiset.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_CONTEXT_H_
#define AKA_INCLUDE__AFB_CONTEXT_H_
#include "afb-context.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_XREQ_H_
#define AKA_INCLUDE__AFB_XREQ_H_
#include "afb-xreq.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__JOBS_H_
#define AKA_INCLUDE__JOBS_H_
#include "jobs.akaignore.h"
#endif


#define INCR		8	/* CAUTION: must be a power of 2 */
#define NOT_STARTED	-1

struct afb_apiset;
struct api_desc;
struct api_class;
struct api_alias;
struct api_depend;

/**
 * array of items
 */
struct api_array {
	int count;			/* count of items */
	union {
		void **anys;
		struct api_desc **apis;
		struct api_class **classes;
		struct api_alias **aliases;
		struct api_depend **depends;
	};
};

/**
 * Internal description of an api
 */
struct api_desc
{
	struct api_desc *next;
	const char *name;		/**< name of the api */
	int status;			/**< initialisation status:
						- NOT_STARTED not started,
						- 0 started without error,
						- greater than 0, error number of start */
	struct afb_api_item api;	/**< handler of the api */
	struct {
		struct api_array classes;
		struct api_array apis;
	} require;
};

/**
 * internal description of aliases
 */
struct api_alias
{
	struct api_alias *next;
	struct api_desc *api;
	char name[];
};

/**
 *
 */
struct api_class
{
	struct api_class *next;
	struct api_array providers;
	char name[];
};

/**
 *
 */
struct api_depend
{
	struct afb_apiset *set;
	char name[];
};

/**
 * Data structure for apiset
 */
struct afb_apiset
{
	struct api_array apis;		/**< the apis */
	struct api_alias *aliases;	/**< the aliases */
	struct afb_apiset *subset;	/**< subset if any */
	struct {
		int (*callback)(void*, struct afb_apiset*, const char*); /* not found handler */
		void *closure;
		void (*cleanup)(void*);
	} onlack;			/** not found handler */
	int timeout;			/**< the timeout in second for the apiset */
	int refcount;			/**< reference count for freeing resources */
	char name[];			/**< name of the apiset */
};

/**
 * global apis
 */
static struct api_desc *all_apis;

/**
 * global classes
 */
static struct api_class *all_classes;

/**
 * Ensure enough room in 'array' for 'count' items
 */
/** Instrumented function api_array_ensure_count(struct api_array*,int) */
static int api_array_ensure_count(struct api_array *array, int count)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/api_array_ensure_count(struct api_array*,int)");AKA_fCall++;
		AKA_mark("lis===136###sois===2811###eois===2817###lif===2###soif===73###eoif===79###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_ensure_count(struct api_array*,int)");int c;

		AKA_mark("lis===137###sois===2819###eois===2831###lif===3###soif===81###eoif===93###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_ensure_count(struct api_array*,int)");void **anys;


		AKA_mark("lis===139###sois===2834###eois===2871###lif===5###soif===96###eoif===133###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_ensure_count(struct api_array*,int)");c = (count + INCR - 1) & ~(INCR - 1);

		AKA_mark("lis===140###sois===2873###eois===2919###lif===6###soif===135###eoif===181###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_ensure_count(struct api_array*,int)");anys = realloc(array->anys, c * sizeof *anys);

		if (AKA_mark("lis===141###sois===2925###eois===2930###lif===7###soif===187###eoif===192###ifc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_ensure_count(struct api_array*,int)") && (AKA_mark("lis===141###sois===2925###eois===2930###lif===7###soif===187###eoif===192###isc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_ensure_count(struct api_array*,int)")&&!anys)) {
				AKA_mark("lis===142###sois===2936###eois===2951###lif===8###soif===198###eoif===213###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_ensure_count(struct api_array*,int)");errno = ENOMEM;

				AKA_mark("lis===143###sois===2954###eois===2964###lif===9###soif===216###eoif===226###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_ensure_count(struct api_array*,int)");return -1;

	}
	else {AKA_mark("lis===-141-###sois===-2925-###eois===-29255-###lif===-7-###soif===-###eoif===-192-###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_ensure_count(struct api_array*,int)");}


		AKA_mark("lis===146###sois===2970###eois===2991###lif===12###soif===232###eoif===253###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_ensure_count(struct api_array*,int)");array->count = count;

		AKA_mark("lis===147###sois===2993###eois===3012###lif===13###soif===255###eoif===274###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_ensure_count(struct api_array*,int)");array->anys = anys;

		AKA_mark("lis===148###sois===3014###eois===3023###lif===14###soif===276###eoif===285###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_ensure_count(struct api_array*,int)");return 0;

}

/**
 * Insert in 'array' the item 'any' at the 'index'
 */
/** Instrumented function api_array_insert(struct api_array*,void*,int) */
static int api_array_insert(struct api_array *array, void *any, int index)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/api_array_insert(struct api_array*,void*,int)");AKA_fCall++;
		AKA_mark("lis===156###sois===3164###eois===3185###lif===2###soif===78###eoif===99###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_insert(struct api_array*,void*,int)");int n = array->count;


		if (AKA_mark("lis===158###sois===3192###eois===3232###lif===4###soif===106###eoif===146###ifc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_insert(struct api_array*,void*,int)") && (AKA_mark("lis===158###sois===3192###eois===3232###lif===4###soif===106###eoif===146###isc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_insert(struct api_array*,void*,int)")&&api_array_ensure_count(array, n + 1) < 0)) {
		AKA_mark("lis===159###sois===3236###eois===3246###lif===5###soif===150###eoif===160###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_insert(struct api_array*,void*,int)");return -1;
	}
	else {AKA_mark("lis===-158-###sois===-3192-###eois===-319240-###lif===-4-###soif===-###eoif===-146-###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_insert(struct api_array*,void*,int)");}


		while (AKA_mark("lis===161###sois===3256###eois===3265###lif===7###soif===170###eoif===179###ifc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_insert(struct api_array*,void*,int)") && (AKA_mark("lis===161###sois===3256###eois===3265###lif===7###soif===170###eoif===179###isc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_insert(struct api_array*,void*,int)")&&index < n)) {
				AKA_mark("lis===162###sois===3271###eois===3307###lif===8###soif===185###eoif===221###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_insert(struct api_array*,void*,int)");array->anys[n] = array->anys[n - 1];

				AKA_mark("lis===163###sois===3310###eois===3314###lif===9###soif===224###eoif===228###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_insert(struct api_array*,void*,int)");n--;

	}


		AKA_mark("lis===166###sois===3320###eois===3345###lif===12###soif===234###eoif===259###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_insert(struct api_array*,void*,int)");array->anys[index] = any;

		AKA_mark("lis===167###sois===3347###eois===3356###lif===13###soif===261###eoif===270###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_insert(struct api_array*,void*,int)");return 0;

}

/**
 * Add the item 'any' to the 'array'
 */
/** Instrumented function api_array_add(struct api_array*,void*) */
static int api_array_add(struct api_array *array, void *any)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)");AKA_fCall++;
		AKA_mark("lis===175###sois===3469###eois===3493###lif===2###soif===64###eoif===88###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)");int i, n = array->count;


		AKA_mark("lis===177###sois===3501###eois===3508###lif===4###soif===96###eoif===103###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)");for (i = 0 ;AKA_mark("lis===177###sois===3509###eois===3514###lif===4###soif===104###eoif===109###ifc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)") && AKA_mark("lis===177###sois===3509###eois===3514###lif===4###soif===104###eoif===109###isc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)")&&i < n;({AKA_mark("lis===177###sois===3517###eois===3520###lif===4###soif===112###eoif===115###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)");i++;})) {
				if (AKA_mark("lis===178###sois===3530###eois===3551###lif===5###soif===125###eoif===146###ifc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)") && (AKA_mark("lis===178###sois===3530###eois===3551###lif===5###soif===125###eoif===146###isc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)")&&array->anys[i] == any)) {
			AKA_mark("lis===179###sois===3556###eois===3565###lif===6###soif===151###eoif===160###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)");return 0;
		}
		else {AKA_mark("lis===-178-###sois===-3530-###eois===-353021-###lif===-5-###soif===-###eoif===-146-###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)");}

	}


		if (AKA_mark("lis===182###sois===3575###eois===3615###lif===9###soif===170###eoif===210###ifc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)") && (AKA_mark("lis===182###sois===3575###eois===3615###lif===9###soif===170###eoif===210###isc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)")&&api_array_ensure_count(array, n + 1) < 0)) {
		AKA_mark("lis===183###sois===3619###eois===3629###lif===10###soif===214###eoif===224###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)");return -1;
	}
	else {AKA_mark("lis===-182-###sois===-3575-###eois===-357540-###lif===-9-###soif===-###eoif===-210-###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)");}


		AKA_mark("lis===185###sois===3632###eois===3653###lif===12###soif===227###eoif===248###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)");array->anys[n] = any;

		AKA_mark("lis===186###sois===3655###eois===3664###lif===13###soif===250###eoif===259###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_add(struct api_array*,void*)");return 0;

}

/**
 * Delete the 'api' from the 'array'
 * Returns 1 if delete or 0 if not found
 */
/** Instrumented function api_array_del(struct api_array*,void*) */
static int api_array_del(struct api_array *array, void *any)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/api_array_del(struct api_array*,void*)");AKA_fCall++;
		AKA_mark("lis===195###sois===3818###eois===3839###lif===2###soif===64###eoif===85###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_del(struct api_array*,void*)");int i = array->count;

		while (AKA_mark("lis===196###sois===3848###eois===3849###lif===3###soif===94###eoif===95###ifc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_del(struct api_array*,void*)") && (AKA_mark("lis===196###sois===3848###eois===3849###lif===3###soif===94###eoif===95###isc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_del(struct api_array*,void*)")&&i)) {
				if (AKA_mark("lis===197###sois===3859###eois===3882###lif===4###soif===105###eoif===128###ifc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_del(struct api_array*,void*)") && (AKA_mark("lis===197###sois===3859###eois===3882###lif===4###soif===105###eoif===128###isc===true###function===./app-framework-binder/src/afb-apiset.c/api_array_del(struct api_array*,void*)")&&array->anys[--i] == any)) {
						AKA_mark("lis===198###sois===3889###eois===3934###lif===5###soif===135###eoif===180###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_del(struct api_array*,void*)");array->anys[i] = array->anys[--array->count];

						AKA_mark("lis===199###sois===3938###eois===3947###lif===6###soif===184###eoif===193###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_del(struct api_array*,void*)");return 1;

		}
		else {AKA_mark("lis===-197-###sois===-3859-###eois===-385923-###lif===-4-###soif===-###eoif===-128-###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_del(struct api_array*,void*)");}

	}

		AKA_mark("lis===202###sois===3956###eois===3965###lif===9###soif===202###eoif===211###ins===true###function===./app-framework-binder/src/afb-apiset.c/api_array_del(struct api_array*,void*)");return 0;

}

/**
 * Search the class of 'name' and return it.
 * In case where the class of 'namle' isn't found, it returns
 * NULL when 'create' is null or a fresh created instance if 'create' isn't
 * zero (but NULL on allocation failure).
 */
/** Instrumented function class_search(const char*,int) */
static struct api_class *class_search(const char *name, int create)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)");AKA_fCall++;
		AKA_mark("lis===213###sois===4273###eois===4293###lif===2###soif===71###eoif===91###ins===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)");struct api_class *c;


		AKA_mark("lis===215###sois===4301###eois===4317###lif===4###soif===99###eoif===115###ins===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)");for (c= all_classes ;AKA_mark("lis===215###sois===4318###eois===4319###lif===4###soif===116###eoif===117###ifc===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)") && AKA_mark("lis===215###sois===4318###eois===4319###lif===4###soif===116###eoif===117###isc===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)")&&c;({AKA_mark("lis===215###sois===4322###eois===4333###lif===4###soif===120###eoif===131###ins===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)");c = c->next;})) {
				if (AKA_mark("lis===216###sois===4343###eois===4369###lif===5###soif===141###eoif===167###ifc===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)") && (AKA_mark("lis===216###sois===4343###eois===4369###lif===5###soif===141###eoif===167###isc===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)")&&!strcasecmp(name, c->name))) {
			AKA_mark("lis===217###sois===4374###eois===4383###lif===6###soif===172###eoif===181###ins===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)");return c;
		}
		else {AKA_mark("lis===-216-###sois===-4343-###eois===-434326-###lif===-5-###soif===-###eoif===-167-###ins===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)");}

	}


		if (AKA_mark("lis===220###sois===4393###eois===4400###lif===9###soif===191###eoif===198###ifc===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)") && (AKA_mark("lis===220###sois===4393###eois===4400###lif===9###soif===191###eoif===198###isc===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)")&&!create)) {
		AKA_mark("lis===221###sois===4404###eois===4416###lif===10###soif===202###eoif===214###ins===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)");return NULL;
	}
	else {AKA_mark("lis===-220-###sois===-4393-###eois===-43937-###lif===-9-###soif===-###eoif===-198-###ins===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)");}


		AKA_mark("lis===223###sois===4419###eois===4463###lif===12###soif===217###eoif===261###ins===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)");c = calloc(1, strlen(name) + 1 + sizeof *c);

		if (AKA_mark("lis===224###sois===4469###eois===4471###lif===13###soif===267###eoif===269###ifc===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)") && (AKA_mark("lis===224###sois===4469###eois===4471###lif===13###soif===267###eoif===269###isc===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)")&&!c)) {
		AKA_mark("lis===225###sois===4475###eois===4490###lif===14###soif===273###eoif===288###ins===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)");errno = ENOMEM;
	}
	else {
				AKA_mark("lis===227###sois===4501###eois===4523###lif===16###soif===299###eoif===321###ins===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)");strcpy(c->name, name);

				AKA_mark("lis===228###sois===4526###eois===4548###lif===17###soif===324###eoif===346###ins===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)");c->next = all_classes;

				AKA_mark("lis===229###sois===4551###eois===4567###lif===18###soif===349###eoif===365###ins===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)");all_classes = c;

	}

		AKA_mark("lis===231###sois===4572###eois===4581###lif===20###soif===370###eoif===379###ins===true###function===./app-framework-binder/src/afb-apiset.c/class_search(const char*,int)");return c;

}

/**
 * Search the api of 'name'.
 * @param set the api set
 * @param name the api name to search
 * @return the descriptor if found or NULL otherwise
 */
/** Instrumented function search(struct afb_apiset*,const char*) */
static struct api_desc *search(struct afb_apiset *set, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");AKA_fCall++;
		AKA_mark("lis===242###sois===4815###eois===4832###lif===2###soif===76###eoif===93###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");int i, c, up, lo;

		AKA_mark("lis===243###sois===4834###eois===4853###lif===3###soif===95###eoif===114###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");struct api_desc *a;

		AKA_mark("lis===244###sois===4855###eois===4881###lif===4###soif===116###eoif===142###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");struct api_alias *aliases;


	/* dichotomic search of the api */
	/* initial slice */
		AKA_mark("lis===248###sois===4941###eois===4948###lif===8###soif===202###eoif===209###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");lo = 0;

		AKA_mark("lis===249###sois===4950###eois===4971###lif===9###soif===211###eoif===232###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");up = set->apis.count;

		while (AKA_mark("lis===250###sois===4980###eois===4987###lif===10###soif===241###eoif===248###ifc===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)") && (AKA_mark("lis===250###sois===4980###eois===4987###lif===10###soif===241###eoif===248###isc===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)")&&lo < up)) {
		/* check the mid of the slice */
				AKA_mark("lis===252###sois===5028###eois===5047###lif===12###soif===289###eoif===308###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");i = (lo + up) >> 1;

				AKA_mark("lis===253###sois===5050###eois===5072###lif===13###soif===311###eoif===333###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");a = set->apis.apis[i];

				AKA_mark("lis===254###sois===5075###eois===5105###lif===14###soif===336###eoif===366###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");c = strcasecmp(a->name, name);

				if (AKA_mark("lis===255###sois===5112###eois===5118###lif===15###soif===373###eoif===379###ifc===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)") && (AKA_mark("lis===255###sois===5112###eois===5118###lif===15###soif===373###eoif===379###isc===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)")&&c == 0)) {
			/* found */
						AKA_mark("lis===257###sois===5140###eois===5149###lif===17###soif===401###eoif===410###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");return a;

		}
		else {AKA_mark("lis===-255-###sois===-5112-###eois===-51126-###lif===-15-###soif===-###eoif===-379-###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");}

		/* update the slice */
				if (AKA_mark("lis===260###sois===5185###eois===5190###lif===20###soif===446###eoif===451###ifc===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)") && (AKA_mark("lis===260###sois===5185###eois===5190###lif===20###soif===446###eoif===451###isc===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)")&&c < 0)) {
			AKA_mark("lis===261###sois===5195###eois===5206###lif===21###soif===456###eoif===467###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");lo = i + 1;
		}
		else {
			AKA_mark("lis===263###sois===5217###eois===5224###lif===23###soif===478###eoif===485###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");up = i;
		}

	}


	/* linear search of aliases */
		AKA_mark("lis===267###sois===5262###eois===5285###lif===27###soif===523###eoif===546###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");aliases = set->aliases;

		for (;;) {
				if (AKA_mark("lis===269###sois===5303###eois===5311###lif===29###soif===564###eoif===572###ifc===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)") && (AKA_mark("lis===269###sois===5303###eois===5311###lif===29###soif===564###eoif===572###isc===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)")&&!aliases)) {
			AKA_mark("lis===270###sois===5316###eois===5322###lif===30###soif===577###eoif===583###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");break;
		}
		else {AKA_mark("lis===-269-###sois===-5303-###eois===-53038-###lif===-29-###soif===-###eoif===-572-###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");}

				AKA_mark("lis===271###sois===5325###eois===5361###lif===31###soif===586###eoif===622###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");c = strcasecmp(aliases->name, name);

				if (AKA_mark("lis===272###sois===5368###eois===5370###lif===32###soif===629###eoif===631###ifc===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)") && (AKA_mark("lis===272###sois===5368###eois===5370###lif===32###soif===629###eoif===631###isc===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)")&&!c)) {
			AKA_mark("lis===273###sois===5375###eois===5395###lif===33###soif===636###eoif===656###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");return aliases->api;
		}
		else {AKA_mark("lis===-272-###sois===-5368-###eois===-53682-###lif===-32-###soif===-###eoif===-631-###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");}

				if (AKA_mark("lis===274###sois===5402###eois===5407###lif===34###soif===663###eoif===668###ifc===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)") && (AKA_mark("lis===274###sois===5402###eois===5407###lif===34###soif===663###eoif===668###isc===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)")&&c > 0)) {
			AKA_mark("lis===275###sois===5412###eois===5418###lif===35###soif===673###eoif===679###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");break;
		}
		else {AKA_mark("lis===-274-###sois===-5402-###eois===-54025-###lif===-34-###soif===-###eoif===-668-###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");}

				AKA_mark("lis===276###sois===5421###eois===5445###lif===36###soif===682###eoif===706###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");aliases = aliases->next;

	}

		AKA_mark("lis===278###sois===5450###eois===5462###lif===38###soif===711###eoif===723###ins===true###function===./app-framework-binder/src/afb-apiset.c/search(struct afb_apiset*,const char*)");return NULL;

}

/**
 * Search the api of 'name' in the apiset and in its subsets.
 * @param set the api set
 * @param name the api name to search
 * @return the descriptor if found or NULL otherwise
 */
/** Instrumented function searchrec(struct afb_apiset*,const char*) */
static struct api_desc *searchrec(struct afb_apiset *set, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/searchrec(struct afb_apiset*,const char*)");AKA_fCall++;
		AKA_mark("lis===289###sois===5732###eois===5756###lif===2###soif===79###eoif===103###ins===true###function===./app-framework-binder/src/afb-apiset.c/searchrec(struct afb_apiset*,const char*)");struct api_desc *result;


		do {
				AKA_mark("lis===292###sois===5766###eois===5793###lif===5###soif===113###eoif===140###ins===true###function===./app-framework-binder/src/afb-apiset.c/searchrec(struct afb_apiset*,const char*)");result = search(set, name);

	}
	while (AKA_mark("lis===293###sois===5804###eois===5849###lif===6###soif===151###eoif===196###ifc===true###function===./app-framework-binder/src/afb-apiset.c/searchrec(struct afb_apiset*,const char*)") && ((AKA_mark("lis===293###sois===5804###eois===5818###lif===6###soif===151###eoif===165###isc===true###function===./app-framework-binder/src/afb-apiset.c/searchrec(struct afb_apiset*,const char*)")&&result == NULL)	&&(AKA_mark("lis===293###sois===5822###eois===5849###lif===6###soif===169###eoif===196###isc===true###function===./app-framework-binder/src/afb-apiset.c/searchrec(struct afb_apiset*,const char*)")&&(set = set->subset) != NULL)));


		AKA_mark("lis===295###sois===5854###eois===5868###lif===8###soif===201###eoif===215###ins===true###function===./app-framework-binder/src/afb-apiset.c/searchrec(struct afb_apiset*,const char*)");return result;

}

/**
 * Increases the count of references to the apiset and return its address
 * @param set the set whose reference count is to be increased
 * @return the given apiset
 */
/** Instrumented function afb_apiset_addref(struct afb_apiset*) */
struct afb_apiset *afb_apiset_addref(struct afb_apiset *set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_addref(struct afb_apiset*)");AKA_fCall++;
		if (AKA_mark("lis===305###sois===6113###eois===6116###lif===2###soif===68###eoif===71###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_addref(struct afb_apiset*)") && (AKA_mark("lis===305###sois===6113###eois===6116###lif===2###soif===68###eoif===71###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_addref(struct afb_apiset*)")&&set)) {
		AKA_mark("lis===306###sois===6120###eois===6176###lif===3###soif===75###eoif===131###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_addref(struct afb_apiset*)");__atomic_add_fetch(&set->refcount, 1, __ATOMIC_RELAXED);
	}
	else {AKA_mark("lis===-305-###sois===-6113-###eois===-61133-###lif===-2-###soif===-###eoif===-71-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_addref(struct afb_apiset*)");}

		AKA_mark("lis===307###sois===6178###eois===6189###lif===4###soif===133###eoif===144###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_addref(struct afb_apiset*)");return set;

}

/**
 * Decreases the count of references to the apiset and frees its
 * resources when no more references exists.
 * @param set the set to unrefrence
 */
/** Instrumented function afb_apiset_unref(struct afb_apiset*) */
void afb_apiset_unref(struct afb_apiset *set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===317###sois===6396###eois===6416###lif===2###soif===49###eoif===69###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");struct api_alias *a;

		AKA_mark("lis===318###sois===6418###eois===6437###lif===3###soif===71###eoif===90###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");struct api_desc *d;


		if (AKA_mark("lis===320###sois===6444###eois===6507###lif===5###soif===97###eoif===160###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)") && ((AKA_mark("lis===320###sois===6444###eois===6447###lif===5###soif===97###eoif===100###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)")&&set)	&&(AKA_mark("lis===320###sois===6451###eois===6507###lif===5###soif===104###eoif===160###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)")&&!__atomic_sub_fetch(&set->refcount, 1, __ATOMIC_RELAXED)))) {
				AKA_mark("lis===321###sois===6513###eois===6543###lif===6###soif===166###eoif===196###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");afb_apiset_unref(set->subset);

				if (AKA_mark("lis===322###sois===6550###eois===6569###lif===7###soif===203###eoif===222###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)") && (AKA_mark("lis===322###sois===6550###eois===6569###lif===7###soif===203###eoif===222###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)")&&set->onlack.cleanup)) {
			AKA_mark("lis===323###sois===6574###eois===6615###lif===8###soif===227###eoif===268###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");set->onlack.cleanup(set->onlack.closure);
		}
		else {AKA_mark("lis===-322-###sois===-6550-###eois===-655019-###lif===-7-###soif===-###eoif===-222-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");}

				while (AKA_mark("lis===324###sois===6624###eois===6642###lif===9###soif===277###eoif===295###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)") && (a=(set->aliases) && AKA_mark("lis===324###sois===6625###eois===6641###lif===9###soif===278###eoif===294###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)"))) {
						AKA_mark("lis===325###sois===6649###eois===6672###lif===10###soif===302###eoif===325###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");set->aliases = a->next;

						AKA_mark("lis===326###sois===6676###eois===6684###lif===11###soif===329###eoif===337###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");free(a);

		}

				while (AKA_mark("lis===328###sois===6698###eois===6713###lif===13###soif===351###eoif===366###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)") && (AKA_mark("lis===328###sois===6698###eois===6713###lif===13###soif===351###eoif===366###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)")&&set->apis.count)) {
						AKA_mark("lis===329###sois===6720###eois===6758###lif===14###soif===373###eoif===411###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");d = set->apis.apis[--set->apis.count];

						if (AKA_mark("lis===330###sois===6766###eois===6783###lif===15###soif===419###eoif===436###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)") && (AKA_mark("lis===330###sois===6766###eois===6783###lif===15###soif===419###eoif===436###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)")&&d->api.itf->unref)) {
				AKA_mark("lis===331###sois===6789###eois===6823###lif===16###soif===442###eoif===476###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");d->api.itf->unref(d->api.closure);
			}
			else {AKA_mark("lis===-330-###sois===-6766-###eois===-676617-###lif===-15-###soif===-###eoif===-436-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");}

						AKA_mark("lis===332###sois===6827###eois===6835###lif===17###soif===480###eoif===488###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");free(d);

		}

				AKA_mark("lis===334###sois===6842###eois===6863###lif===19###soif===495###eoif===516###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");free(set->apis.apis);

				AKA_mark("lis===335###sois===6866###eois===6876###lif===20###soif===519###eoif===529###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");free(set);

	}
	else {AKA_mark("lis===-320-###sois===-6444-###eois===-644463-###lif===-5-###soif===-###eoif===-160-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unref(struct afb_apiset*)");}

}

/**
 * Create an apiset
 * @param name the name of the apiset
 * @param timeout the default timeout in seconds for the apiset
 * @return the created apiset or NULL in case of error
 */
/** Instrumented function afb_apiset_create(const char*,int) */
struct afb_apiset *afb_apiset_create(const char *name, int timeout)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_create(const char*,int)");AKA_fCall++;
		AKA_mark("lis===347###sois===7139###eois===7162###lif===2###soif===71###eoif===94###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create(const char*,int)");struct afb_apiset *set;


		AKA_mark("lis===349###sois===7165###eois===7226###lif===4###soif===97###eoif===158###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create(const char*,int)");set = calloc(1, (name ? strlen(name) : 0) + 1 + sizeof *set);

		if (AKA_mark("lis===350###sois===7232###eois===7235###lif===5###soif===164###eoif===167###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create(const char*,int)") && (AKA_mark("lis===350###sois===7232###eois===7235###lif===5###soif===164###eoif===167###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create(const char*,int)")&&set)) {
				AKA_mark("lis===351###sois===7241###eois===7264###lif===6###soif===173###eoif===196###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create(const char*,int)");set->timeout = timeout;

				AKA_mark("lis===352###sois===7267###eois===7285###lif===7###soif===199###eoif===217###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create(const char*,int)");set->refcount = 1;

				if (AKA_mark("lis===353###sois===7292###eois===7296###lif===8###soif===224###eoif===228###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create(const char*,int)") && (AKA_mark("lis===353###sois===7292###eois===7296###lif===8###soif===224###eoif===228###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create(const char*,int)")&&name)) {
			AKA_mark("lis===354###sois===7301###eois===7325###lif===9###soif===233###eoif===257###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create(const char*,int)");strcpy(set->name, name);
		}
		else {AKA_mark("lis===-353-###sois===-7292-###eois===-72924-###lif===-8-###soif===-###eoif===-228-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create(const char*,int)");}

	}
	else {AKA_mark("lis===-350-###sois===-7232-###eois===-72323-###lif===-5-###soif===-###eoif===-167-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create(const char*,int)");}

		AKA_mark("lis===356###sois===7330###eois===7341###lif===11###soif===262###eoif===273###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create(const char*,int)");return set;

}

/**
 * Create an apiset being the last subset of 'set'
 * @param set     the set to extend with the created subset (can be NULL)
 * @param name    the name of the created apiset (can be NULL)
 * @param timeout the default timeout in seconds for the created apiset
 * @return the created apiset or NULL in case of error
 */
/** Instrumented function afb_apiset_create_subset_last(struct afb_apiset*,const char*,int) */
struct afb_apiset *afb_apiset_create_subset_last(struct afb_apiset *set, const char *name, int timeout)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_last(struct afb_apiset*,const char*,int)");AKA_fCall++;
		if (AKA_mark("lis===368###sois===7779###eois===7782###lif===2###soif===111###eoif===114###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_last(struct afb_apiset*,const char*,int)") && (AKA_mark("lis===368###sois===7779###eois===7782###lif===2###soif===111###eoif===114###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_last(struct afb_apiset*,const char*,int)")&&set)) {
		while (AKA_mark("lis===369###sois===7793###eois===7804###lif===3###soif===125###eoif===136###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_last(struct afb_apiset*,const char*,int)") && (AKA_mark("lis===369###sois===7793###eois===7804###lif===3###soif===125###eoif===136###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_last(struct afb_apiset*,const char*,int)")&&set->subset)) {
			AKA_mark("lis===370###sois===7809###eois===7827###lif===4###soif===141###eoif===159###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_last(struct afb_apiset*,const char*,int)");set = set->subset;
		}
	}
	else {AKA_mark("lis===-368-###sois===-7779-###eois===-77793-###lif===-2-###soif===-###eoif===-114-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_last(struct afb_apiset*,const char*,int)");}

		AKA_mark("lis===371###sois===7829###eois===7887###lif===5###soif===161###eoif===219###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_last(struct afb_apiset*,const char*,int)");return afb_apiset_create_subset_first(set, name, timeout);

}

/**
 * Create an apiset being the first subset of 'set'
 * @param set     the set to extend with the created subset (can be NULL)
 * @param name    the name of the created apiset (can be NULL)
 * @param timeout the default timeout in seconds for the created apiset
 * @return the created apiset or NULL in case of error
 */
/** Instrumented function afb_apiset_create_subset_first(struct afb_apiset*,const char*,int) */
struct afb_apiset *afb_apiset_create_subset_first(struct afb_apiset *set, const char *name, int timeout)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_first(struct afb_apiset*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===383###sois===8323###eois===8384###lif===2###soif===108###eoif===169###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_first(struct afb_apiset*,const char*,int)");struct afb_apiset *result = afb_apiset_create(name, timeout);

		if (AKA_mark("lis===384###sois===8390###eois===8403###lif===3###soif===175###eoif===188###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_first(struct afb_apiset*,const char*,int)") && ((AKA_mark("lis===384###sois===8390###eois===8396###lif===3###soif===175###eoif===181###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_first(struct afb_apiset*,const char*,int)")&&result)	&&(AKA_mark("lis===384###sois===8400###eois===8403###lif===3###soif===185###eoif===188###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_first(struct afb_apiset*,const char*,int)")&&set))) {
				AKA_mark("lis===385###sois===8409###eois===8438###lif===4###soif===194###eoif===223###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_first(struct afb_apiset*,const char*,int)");result->subset = set->subset;

				AKA_mark("lis===386###sois===8441###eois===8462###lif===5###soif===226###eoif===247###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_first(struct afb_apiset*,const char*,int)");set->subset = result;

	}
	else {AKA_mark("lis===-384-###sois===-8390-###eois===-839013-###lif===-3-###soif===-###eoif===-188-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_first(struct afb_apiset*,const char*,int)");}

		AKA_mark("lis===388###sois===8467###eois===8481###lif===7###soif===252###eoif===266###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_create_subset_first(struct afb_apiset*,const char*,int)");return result;

}

/**
 * the name of the apiset
 * @param set the api set
 * @return the name of the set
 */
/** Instrumented function afb_apiset_name(struct afb_apiset*) */
const char *afb_apiset_name(struct afb_apiset *set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_name(struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===398###sois===8631###eois===8648###lif===2###soif===55###eoif===72###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_name(struct afb_apiset*)");return set->name;

}

/**
 * Get the API timeout of the set
 * @param set the api set
 * @return the timeout in seconds
 */
/** Instrumented function afb_apiset_timeout_get(struct afb_apiset*) */
int afb_apiset_timeout_get(struct afb_apiset *set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_timeout_get(struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===408###sois===8808###eois===8828###lif===2###soif===54###eoif===74###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_timeout_get(struct afb_apiset*)");return set->timeout;

}

/**
 * Set the API timeout of the set
 * @param set the api set
 * @param to the timeout in seconds
 */
/** Instrumented function afb_apiset_timeout_set(struct afb_apiset*,int) */
void afb_apiset_timeout_set(struct afb_apiset *set, int to)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_timeout_set(struct afb_apiset*,int)");AKA_fCall++;
		AKA_mark("lis===418###sois===8999###eois===9017###lif===2###soif===63###eoif===81###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_timeout_set(struct afb_apiset*,int)");set->timeout = to;

}

/**
 * Get the subset of the set
 * @param set the api set
 * @return the subset of set
 */
/** Instrumented function afb_apiset_subset_get(struct afb_apiset*) */
struct afb_apiset *afb_apiset_subset_get(struct afb_apiset *set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_get(struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===428###sois===9181###eois===9200###lif===2###soif===68###eoif===87###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_get(struct afb_apiset*)");return set->subset;

}

/**
 * Set the subset of the set
 * @param set the api set
 * @param subset the subset to set
 *
 * @return 0 in case of success or -1 if it had created a loop
 */
/** Instrumented function afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*) */
int afb_apiset_subset_set(struct afb_apiset *set, struct afb_apiset *subset)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===440###sois===9448###eois===9471###lif===2###soif===80###eoif===103###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*)");struct afb_apiset *tmp;


	/* avoid infinite loop */
		AKA_mark("lis===443###sois===9506###eois===9520###lif===5###soif===138###eoif===152###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*)");for (tmp = subset ;AKA_mark("lis===443###sois===9521###eois===9524###lif===5###soif===153###eoif===156###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*)") && AKA_mark("lis===443###sois===9521###eois===9524###lif===5###soif===153###eoif===156###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*)")&&tmp;({AKA_mark("lis===443###sois===9527###eois===9544###lif===5###soif===159###eoif===176###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*)");tmp = tmp->subset;})) {
		if (AKA_mark("lis===444###sois===9552###eois===9562###lif===6###soif===184###eoif===194###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*)") && (AKA_mark("lis===444###sois===9552###eois===9562###lif===6###soif===184###eoif===194###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*)")&&tmp == set)) {
			AKA_mark("lis===445###sois===9567###eois===9577###lif===7###soif===199###eoif===209###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*)");return -1;
		}
		else {AKA_mark("lis===-444-###sois===-9552-###eois===-955210-###lif===-6-###soif===-###eoif===-194-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*)");}
	}


		AKA_mark("lis===447###sois===9580###eois===9598###lif===9###soif===212###eoif===230###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*)");tmp = set->subset;

		AKA_mark("lis===448###sois===9600###eois===9640###lif===10###soif===232###eoif===272###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*)");set->subset = afb_apiset_addref(subset);

		AKA_mark("lis===449###sois===9642###eois===9664###lif===11###soif===274###eoif===296###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*)");afb_apiset_unref(tmp);


		AKA_mark("lis===451###sois===9667###eois===9676###lif===13###soif===299###eoif===308###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_subset_set(struct afb_apiset*,struct afb_apiset*)");return 0;

}

/** Instrumented function afb_apiset_onlack_set(struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*),void*,void(*cleanup)(void*)) */
void afb_apiset_onlack_set(struct afb_apiset *set, int (*callback)(void*, struct afb_apiset*, const char*), void *closure, void (*cleanup)(void*))
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_onlack_set(struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*),void*,void(*cleanup)(void*))");AKA_fCall++;
		if (AKA_mark("lis===456###sois===9834###eois===9853###lif===2###soif===154###eoif===173###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_onlack_set(struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*),void*,void(*cleanup)(void*))") && (AKA_mark("lis===456###sois===9834###eois===9853###lif===2###soif===154###eoif===173###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_onlack_set(struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*),void*,void(*cleanup)(void*))")&&set->onlack.cleanup)) {
		AKA_mark("lis===457###sois===9857###eois===9898###lif===3###soif===177###eoif===218###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_onlack_set(struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*),void*,void(*cleanup)(void*))");set->onlack.cleanup(set->onlack.closure);
	}
	else {AKA_mark("lis===-456-###sois===-9834-###eois===-983419-###lif===-2-###soif===-###eoif===-173-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_onlack_set(struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*),void*,void(*cleanup)(void*))");}

		AKA_mark("lis===458###sois===9900###eois===9932###lif===4###soif===220###eoif===252###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_onlack_set(struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*),void*,void(*cleanup)(void*))");set->onlack.callback = callback;

		AKA_mark("lis===459###sois===9934###eois===9964###lif===5###soif===254###eoif===284###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_onlack_set(struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*),void*,void(*cleanup)(void*))");set->onlack.closure = closure;

		AKA_mark("lis===460###sois===9966###eois===9996###lif===6###soif===286###eoif===316###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_onlack_set(struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*),void*,void(*cleanup)(void*))");set->onlack.cleanup = cleanup;

}

/**
 * Adds the api of 'name' described by 'api'.
 * @param set the api set
 * @param name the name of the api to add (have to survive, not copied!)
 * @param api the api
 * @returns 0 in case of success or -1 in case
 * of error with errno set:
 *   - EEXIST if name already registered
 *   - ENOMEM when out of memory
 */
/** Instrumented function afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item) */
int afb_apiset_add(struct afb_apiset *set, const char *name, struct afb_api_item api)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");AKA_fCall++;
		AKA_mark("lis===475###sois===10413###eois===10435###lif===2###soif===89###eoif===111###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");struct api_desc *desc;

		AKA_mark("lis===476###sois===10437###eois===10446###lif===3###soif===113###eoif===122###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");int i, c;


	/* check whether it exists already */
		if (AKA_mark("lis===479###sois===10492###eois===10509###lif===6###soif===168###eoif===185###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)") && (AKA_mark("lis===479###sois===10492###eois===10509###lif===6###soif===168###eoif===185###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)")&&search(set, name))) {
				AKA_mark("lis===480###sois===10515###eois===10560###lif===7###soif===191###eoif===236###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");ERROR("api of name %s already exists", name);

				AKA_mark("lis===481###sois===10563###eois===10578###lif===8###soif===239###eoif===254###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");errno = EEXIST;

				AKA_mark("lis===482###sois===10581###eois===10592###lif===9###soif===257###eoif===268###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");goto error;

	}
	else {AKA_mark("lis===-479-###sois===-10492-###eois===-1049217-###lif===-6-###soif===-###eoif===-185-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");}


	/* search insertion place */
		AKA_mark("lis===486###sois===10633###eois===10640###lif===13###soif===309###eoif===316###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");for (i = 0 ;AKA_mark("lis===486###sois===10641###eois===10660###lif===13###soif===317###eoif===336###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)") && AKA_mark("lis===486###sois===10641###eois===10660###lif===13###soif===317###eoif===336###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)")&&i < set->apis.count;({AKA_mark("lis===486###sois===10663###eois===10666###lif===13###soif===339###eoif===342###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");i++;})) {
				AKA_mark("lis===487###sois===10672###eois===10718###lif===14###soif===348###eoif===394###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");c = strcasecmp(set->apis.apis[i]->name, name);

				if (AKA_mark("lis===488###sois===10725###eois===10730###lif===15###soif===401###eoif===406###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)") && (AKA_mark("lis===488###sois===10725###eois===10730###lif===15###soif===401###eoif===406###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)")&&c > 0)) {
			AKA_mark("lis===489###sois===10735###eois===10741###lif===16###soif===411###eoif===417###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");break;
		}
		else {AKA_mark("lis===-488-###sois===-10725-###eois===-107255-###lif===-15-###soif===-###eoif===-406-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");}

	}


	/* allocates memory */
		AKA_mark("lis===493###sois===10771###eois===10802###lif===20###soif===447###eoif===478###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");desc = calloc(1, sizeof *desc);

		if (AKA_mark("lis===494###sois===10808###eois===10813###lif===21###soif===484###eoif===489###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)") && (AKA_mark("lis===494###sois===10808###eois===10813###lif===21###soif===484###eoif===489###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)")&&!desc)) {
		AKA_mark("lis===495###sois===10817###eois===10826###lif===22###soif===493###eoif===502###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");goto oom;
	}
	else {AKA_mark("lis===-494-###sois===-10808-###eois===-108085-###lif===-21-###soif===-###eoif===-489-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");}


		AKA_mark("lis===497###sois===10829###eois===10856###lif===24###soif===505###eoif===532###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");desc->status = NOT_STARTED;

		AKA_mark("lis===498###sois===10858###eois===10874###lif===25###soif===534###eoif===550###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");desc->api = api;

		AKA_mark("lis===499###sois===10876###eois===10894###lif===26###soif===552###eoif===570###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");desc->name = name;


		if (AKA_mark("lis===501###sois===10901###eois===10942###lif===28###soif===577###eoif===618###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)") && (AKA_mark("lis===501###sois===10901###eois===10942###lif===28###soif===577###eoif===618###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)")&&api_array_insert(&set->apis, desc, i) < 0)) {
				AKA_mark("lis===502###sois===10948###eois===10959###lif===29###soif===624###eoif===635###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");free(desc);

				AKA_mark("lis===503###sois===10962###eois===10973###lif===30###soif===638###eoif===649###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");goto error;

	}
	else {AKA_mark("lis===-501-###sois===-10901-###eois===-1090141-###lif===-28-###soif===-###eoif===-618-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");}


		AKA_mark("lis===506###sois===10979###eois===11001###lif===33###soif===655###eoif===677###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");desc->next = all_apis;

		AKA_mark("lis===507###sois===11003###eois===11019###lif===34###soif===679###eoif===695###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");all_apis = desc;


		if (AKA_mark("lis===509###sois===11026###eois===11049###lif===36###soif===702###eoif===725###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)") && (AKA_mark("lis===509###sois===11026###eois===11049###lif===36###soif===702###eoif===725###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)")&&afb_api_is_public(name))) {
		AKA_mark("lis===510###sois===11053###eois===11080###lif===37###soif===729###eoif===756###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");INFO("API %s added", name);
	}
	else {AKA_mark("lis===-509-###sois===-11026-###eois===-1102623-###lif===-36-###soif===-###eoif===-725-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");}


		AKA_mark("lis===512###sois===11083###eois===11092###lif===39###soif===759###eoif===768###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");return 0;


	oom:
	AKA_mark("lis===515###sois===11100###eois===11123###lif===42###soif===776###eoif===799###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");ERROR("out of memory");

		AKA_mark("lis===516###sois===11125###eois===11140###lif===43###soif===801###eoif===816###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");errno = ENOMEM;

	error:
	AKA_mark("lis===518###sois===11149###eois===11159###lif===45###soif===825###eoif===835###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add(struct afb_apiset*,const char*,struct afb_api_item)");return -1;

}

/**
 * Adds a the 'alias' name to the api of 'name'.
 * @params set the api set
 * @param name the name of the api to alias
 * @param alias the aliased name to add to the api of name
 * @returns 0 in case of success or -1 in case
 * of error with errno set:
 *   - ENOENT if the api doesn't exist
 *   - EEXIST if name (of alias) already registered
 *   - ENOMEM when out of memory
 */
/** Instrumented function afb_apiset_add_alias(struct afb_apiset*,const char*,const char*) */
int afb_apiset_add_alias(struct afb_apiset *set, const char *name, const char *alias)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===534###sois===11638###eois===11659###lif===2###soif===89###eoif===110###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");struct api_desc *api;

		AKA_mark("lis===535###sois===11661###eois===11691###lif===3###soif===112###eoif===142###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");struct api_alias *ali, **pali;


	/* check alias doesn't already exist */
		if (AKA_mark("lis===538###sois===11739###eois===11757###lif===6###soif===190###eoif===208###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)") && (AKA_mark("lis===538###sois===11739###eois===11757###lif===6###soif===190###eoif===208###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)")&&search(set, alias))) {
				AKA_mark("lis===539###sois===11763###eois===11809###lif===7###soif===214###eoif===260###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");ERROR("api of name %s already exists", alias);

				AKA_mark("lis===540###sois===11812###eois===11827###lif===8###soif===263###eoif===278###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");errno = EEXIST;

				AKA_mark("lis===541###sois===11830###eois===11841###lif===9###soif===281###eoif===292###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");goto error;

	}
	else {AKA_mark("lis===-538-###sois===-11739-###eois===-1173918-###lif===-6-###soif===-###eoif===-208-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");}


	/* check aliased api exists */
		AKA_mark("lis===545###sois===11879###eois===11903###lif===13###soif===330###eoif===354###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");api = search(set, name);

		if (AKA_mark("lis===546###sois===11909###eois===11920###lif===14###soif===360###eoif===371###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)") && (AKA_mark("lis===546###sois===11909###eois===11920###lif===14###soif===360###eoif===371###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)")&&api == NULL)) {
				AKA_mark("lis===547###sois===11926###eois===11971###lif===15###soif===377###eoif===422###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");ERROR("api of name %s doesn't exists", name);

				AKA_mark("lis===548###sois===11974###eois===11989###lif===16###soif===425###eoif===440###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");errno = ENOENT;

				AKA_mark("lis===549###sois===11992###eois===12003###lif===17###soif===443###eoif===454###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");goto error;

	}
	else {AKA_mark("lis===-546-###sois===-11909-###eois===-1190911-###lif===-14-###soif===-###eoif===-371-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");}


	/* allocates and init the struct */
		AKA_mark("lis===553###sois===12046###eois===12092###lif===21###soif===497###eoif===543###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");ali = malloc(sizeof *ali + strlen(alias) + 1);

		if (AKA_mark("lis===554###sois===12098###eois===12109###lif===22###soif===549###eoif===560###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)") && (AKA_mark("lis===554###sois===12098###eois===12109###lif===22###soif===549###eoif===560###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)")&&ali == NULL)) {
				AKA_mark("lis===555###sois===12115###eois===12138###lif===23###soif===566###eoif===589###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");ERROR("out of memory");

				AKA_mark("lis===556###sois===12141###eois===12156###lif===24###soif===592###eoif===607###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");errno = ENOMEM;

				AKA_mark("lis===557###sois===12159###eois===12170###lif===25###soif===610###eoif===621###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");goto error;

	}
	else {AKA_mark("lis===-554-###sois===-12098-###eois===-1209811-###lif===-22-###soif===-###eoif===-560-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");}

		AKA_mark("lis===559###sois===12175###eois===12190###lif===27###soif===626###eoif===641###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");ali->api = api;

		AKA_mark("lis===560###sois===12192###eois===12217###lif===28###soif===643###eoif===668###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");strcpy(ali->name, alias);


	/* insert the alias in the sorted order */
		AKA_mark("lis===563###sois===12264###eois===12285###lif===31###soif===715###eoif===736###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");pali = &set->aliases;

		while (AKA_mark("lis===564###sois===12293###eois===12334###lif===32###soif===744###eoif===785###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)") && ((AKA_mark("lis===564###sois===12293###eois===12298###lif===32###soif===744###eoif===749###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)")&&*pali)	&&(AKA_mark("lis===564###sois===12302###eois===12334###lif===32###soif===753###eoif===785###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)")&&strcmp((*pali)->name, alias) < 0))) {
		AKA_mark("lis===565###sois===12338###eois===12360###lif===33###soif===789###eoif===811###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");pali = &(*pali)->next;
	}

		AKA_mark("lis===566###sois===12362###eois===12380###lif===34###soif===813###eoif===831###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");ali->next = *pali;

		AKA_mark("lis===567###sois===12382###eois===12394###lif===35###soif===833###eoif===845###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");*pali = ali;

		AKA_mark("lis===568###sois===12396###eois===12405###lif===36###soif===847###eoif===856###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");return 0;

	error:
	AKA_mark("lis===570###sois===12414###eois===12424###lif===38###soif===865###eoif===875###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_add_alias(struct afb_apiset*,const char*,const char*)");return -1;

}

/** Instrumented function afb_apiset_is_alias(struct afb_apiset*,const char*) */
int afb_apiset_is_alias(struct afb_apiset *set, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_is_alias(struct afb_apiset*,const char*)");AKA_fCall++;
		AKA_mark("lis===575###sois===12497###eois===12541###lif===2###soif===69###eoif===113###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_is_alias(struct afb_apiset*,const char*)");struct api_desc *api = searchrec(set, name);

		AKA_mark("lis===576###sois===12543###eois===12585###lif===3###soif===115###eoif===157###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_is_alias(struct afb_apiset*,const char*)");return api && strcasecmp(api->name, name);

}

/** Instrumented function afb_apiset_unalias(struct afb_apiset*,const char*) */
const char *afb_apiset_unalias(struct afb_apiset *set, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_unalias(struct afb_apiset*,const char*)");AKA_fCall++;
		AKA_mark("lis===581###sois===12665###eois===12709###lif===2###soif===76###eoif===120###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unalias(struct afb_apiset*,const char*)");struct api_desc *api = searchrec(set, name);

		AKA_mark("lis===582###sois===12711###eois===12741###lif===3###soif===122###eoif===152###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_unalias(struct afb_apiset*,const char*)");return api ? api->name : NULL;

}

/**
 * Delete from the 'set' the api of 'name'.
 * @param set the set to be changed
 * @param name the name of the API to remove
 * @return 0 in case of success or -1 in case where the API doesn't exist.
 */
/** Instrumented function afb_apiset_del(struct afb_apiset*,const char*) */
int afb_apiset_del(struct afb_apiset *set, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");AKA_fCall++;
		AKA_mark("lis===593###sois===13017###eois===13039###lif===2###soif===64###eoif===86###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");struct api_class *cla;

		AKA_mark("lis===594###sois===13041###eois===13071###lif===3###soif===88###eoif===118###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");struct api_alias *ali, **pali;

		AKA_mark("lis===595###sois===13073###eois===13112###lif===4###soif===120###eoif===159###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");struct api_desc *desc, **pdesc, *odesc;

		AKA_mark("lis===596###sois===13114###eois===13123###lif===5###soif===161###eoif===170###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");int i, c;


	/* search the alias */
		AKA_mark("lis===599###sois===13150###eois===13171###lif===8###soif===197###eoif===218###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");pali = &set->aliases;

		while (AKA_mark("lis===600###sois===13180###eois===13193###lif===9###soif===227###eoif===240###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)") && (ali=(*pali) && AKA_mark("lis===600###sois===13181###eois===13192###lif===9###soif===228###eoif===239###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)"))) {
				AKA_mark("lis===601###sois===13199###eois===13231###lif===10###soif===246###eoif===278###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");c = strcasecmp(ali->name, name);

				if (AKA_mark("lis===602###sois===13238###eois===13240###lif===11###soif===285###eoif===287###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)") && (AKA_mark("lis===602###sois===13238###eois===13240###lif===11###soif===285###eoif===287###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)")&&!c)) {
						AKA_mark("lis===603###sois===13247###eois===13265###lif===12###soif===294###eoif===312###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");*pali = ali->next;

						AKA_mark("lis===604###sois===13269###eois===13279###lif===13###soif===316###eoif===326###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");free(ali);

						AKA_mark("lis===605###sois===13283###eois===13292###lif===14###soif===330###eoif===339###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");return 0;

		}
		else {AKA_mark("lis===-602-###sois===-13238-###eois===-132382-###lif===-11-###soif===-###eoif===-287-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");}

				if (AKA_mark("lis===607###sois===13303###eois===13308###lif===16###soif===350###eoif===355###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)") && (AKA_mark("lis===607###sois===13303###eois===13308###lif===16###soif===350###eoif===355###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)")&&c > 0)) {
			AKA_mark("lis===608###sois===13313###eois===13319###lif===17###soif===360###eoif===366###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");break;
		}
		else {AKA_mark("lis===-607-###sois===-13303-###eois===-133035-###lif===-16-###soif===-###eoif===-355-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");}

				AKA_mark("lis===609###sois===13322###eois===13340###lif===18###soif===369###eoif===387###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");pali = &ali->next;

	}


	/* search the api */
		AKA_mark("lis===613###sois===13373###eois===13380###lif===22###soif===420###eoif===427###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");for (i = 0 ;AKA_mark("lis===613###sois===13381###eois===13400###lif===22###soif===428###eoif===447###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)") && AKA_mark("lis===613###sois===13381###eois===13400###lif===22###soif===428###eoif===447###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)")&&i < set->apis.count;({AKA_mark("lis===613###sois===13403###eois===13406###lif===22###soif===450###eoif===453###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");i++;})) {
				AKA_mark("lis===614###sois===13412###eois===13437###lif===23###soif===459###eoif===484###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");desc = set->apis.apis[i];

				AKA_mark("lis===615###sois===13440###eois===13473###lif===24###soif===487###eoif===520###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");c = strcasecmp(desc->name, name);

				if (AKA_mark("lis===616###sois===13480###eois===13486###lif===25###soif===527###eoif===533###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)") && (AKA_mark("lis===616###sois===13480###eois===13486###lif===25###soif===527###eoif===533###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)")&&c == 0)) {
			/* remove from classes */
						AKA_mark("lis===618###sois===13527###eois===13546###lif===27###soif===574###eoif===593###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");for (cla = all_classes ;AKA_mark("lis===618###sois===13547###eois===13550###lif===27###soif===594###eoif===597###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)") && AKA_mark("lis===618###sois===13547###eois===13550###lif===27###soif===594###eoif===597###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)")&&cla;({AKA_mark("lis===618###sois===13553###eois===13568###lif===27###soif===600###eoif===615###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");cla = cla->next;})) {
				AKA_mark("lis===619###sois===13574###eois===13611###lif===28###soif===621###eoif===658###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");api_array_del(&cla->providers, desc);
			}


			/* unlink from the whole set and their requires */
						AKA_mark("lis===622###sois===13670###eois===13688###lif===31###soif===717###eoif===735###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");pdesc = &all_apis;

						while (AKA_mark("lis===623###sois===13699###eois===13723###lif===32###soif===746###eoif===770###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)") && (AKA_mark("lis===623###sois===13699###eois===13723###lif===32###soif===746###eoif===770###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)")&&(odesc = *pdesc) != desc)) {
								AKA_mark("lis===624###sois===13731###eois===13752###lif===33###soif===778###eoif===799###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");pdesc = &odesc->next;

			}

						AKA_mark("lis===626###sois===13761###eois===13789###lif===35###soif===808###eoif===836###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");*pdesc = odesc = desc->next;

						while (AKA_mark("lis===627###sois===13800###eois===13805###lif===36###soif===847###eoif===852###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)") && (AKA_mark("lis===627###sois===13800###eois===13805###lif===36###soif===847###eoif===852###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)")&&odesc)) {
								AKA_mark("lis===628###sois===13813###eois===13833###lif===37###soif===860###eoif===880###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");odesc = odesc->next;

			}


			/* remove references from classes */
						AKA_mark("lis===632###sois===13883###eois===13919###lif===41###soif===930###eoif===966###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");free(desc->require.classes.classes);


			/* drop the aliases */
						AKA_mark("lis===635###sois===13950###eois===13971###lif===44###soif===997###eoif===1018###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");pali = &set->aliases;

						while (AKA_mark("lis===636###sois===13982###eois===13995###lif===45###soif===1029###eoif===1042###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)") && (ali=(*pali) && AKA_mark("lis===636###sois===13983###eois===13994###lif===45###soif===1030###eoif===1041###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)"))) {
								if (AKA_mark("lis===637###sois===14007###eois===14023###lif===46###soif===1054###eoif===1070###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)") && (AKA_mark("lis===637###sois===14007###eois===14023###lif===46###soif===1054###eoif===1070###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)")&&ali->api != desc)) {
					AKA_mark("lis===638###sois===14030###eois===14048###lif===47###soif===1077###eoif===1095###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");pali = &ali->next;
				}
				else {
										AKA_mark("lis===640###sois===14065###eois===14083###lif===49###soif===1112###eoif===1130###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");*pali = ali->next;

										AKA_mark("lis===641###sois===14089###eois===14099###lif===50###soif===1136###eoif===1146###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");free(ali);

				}

			}


			/* unref the api */
						if (AKA_mark("lis===646###sois===14142###eois===14162###lif===55###soif===1189###eoif===1209###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)") && (AKA_mark("lis===646###sois===14142###eois===14162###lif===55###soif===1189###eoif===1209###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)")&&desc->api.itf->unref)) {
				AKA_mark("lis===647###sois===14168###eois===14208###lif===56###soif===1215###eoif===1255###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");desc->api.itf->unref(desc->api.closure);
			}
			else {AKA_mark("lis===-646-###sois===-14142-###eois===-1414220-###lif===-55-###soif===-###eoif===-1209-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");}


						AKA_mark("lis===649###sois===14213###eois===14231###lif===58###soif===1260###eoif===1278###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");set->apis.count--;

						while (AKA_mark("lis===650###sois===14241###eois===14260###lif===59###soif===1288###eoif===1307###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)") && (AKA_mark("lis===650###sois===14241###eois===14260###lif===59###soif===1288###eoif===1307###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)")&&i < set->apis.count)) {
								AKA_mark("lis===651###sois===14268###eois===14310###lif===60###soif===1315###eoif===1357###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");set->apis.apis[i] = set->apis.apis[i + 1];

								AKA_mark("lis===652###sois===14315###eois===14319###lif===61###soif===1362###eoif===1366###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");i++;

			}

						AKA_mark("lis===654###sois===14328###eois===14339###lif===63###soif===1375###eoif===1386###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");free(desc);

						AKA_mark("lis===655###sois===14343###eois===14352###lif===64###soif===1390###eoif===1399###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");return 0;

		}
		else {AKA_mark("lis===-616-###sois===-13480-###eois===-134806-###lif===-25-###soif===-###eoif===-533-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");}

				if (AKA_mark("lis===657###sois===14363###eois===14368###lif===66###soif===1410###eoif===1415###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)") && (AKA_mark("lis===657###sois===14363###eois===14368###lif===66###soif===1410###eoif===1415###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)")&&c > 0)) {
			AKA_mark("lis===658###sois===14373###eois===14379###lif===67###soif===1420###eoif===1426###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");break;
		}
		else {AKA_mark("lis===-657-###sois===-14363-###eois===-143635-###lif===-66-###soif===-###eoif===-1415-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");}

	}

		AKA_mark("lis===660###sois===14384###eois===14399###lif===69###soif===1431###eoif===1446###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");errno = ENOENT;

		AKA_mark("lis===661###sois===14401###eois===14411###lif===70###soif===1448###eoif===1458###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_del(struct afb_apiset*,const char*)");return -1;

}

/**
 * Get from the 'set' the API of 'name' in 'api' with fallback to subset or default api
 * @param set the set of API
 * @param name the name of the API to get
 * @param rec if not zero look also recursively in subsets
 * @return the api pointer in case of success or NULL in case of error
 */
/** Instrumented function lookup(struct afb_apiset*,const char*,int) */
static struct api_desc *lookup(struct afb_apiset *set, const char *name, int rec)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===673###sois===14797###eois===14821###lif===2###soif===85###eoif===109###ins===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)");struct api_desc *result;


		AKA_mark("lis===675###sois===14824###eois===14851###lif===4###soif===112###eoif===139###ins===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)");result = search(set, name);

		while (AKA_mark("lis===676###sois===14860###eois===14867###lif===5###soif===148###eoif===155###ifc===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)") && (AKA_mark("lis===676###sois===14860###eois===14867###lif===5###soif===148###eoif===155###isc===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)")&&!result)) {
		/* lacking the api, try onlack behaviour */
				if (AKA_mark("lis===678###sois===14923###eois===15003###lif===7###soif===211###eoif===291###ifc===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)") && ((AKA_mark("lis===678###sois===14923###eois===14943###lif===7###soif===211###eoif===231###isc===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)")&&set->onlack.callback)	&&(AKA_mark("lis===678###sois===14947###eois===15003###lif===7###soif===235###eoif===291###isc===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)")&&set->onlack.callback(set->onlack.closure, set, name) > 0))) {
						AKA_mark("lis===679###sois===15010###eois===15037###lif===8###soif===298###eoif===325###ins===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)");result = search(set, name);

						if (AKA_mark("lis===680###sois===15045###eois===15051###lif===9###soif===333###eoif===339###ifc===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)") && (AKA_mark("lis===680###sois===15045###eois===15051###lif===9###soif===333###eoif===339###isc===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)")&&result)) {
				AKA_mark("lis===681###sois===15057###eois===15063###lif===10###soif===345###eoif===351###ins===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)");break;
			}
			else {AKA_mark("lis===-680-###sois===-15045-###eois===-150456-###lif===-9-###soif===-###eoif===-339-###ins===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)");}

		}
		else {AKA_mark("lis===-678-###sois===-14923-###eois===-1492380-###lif===-7-###soif===-###eoif===-291-###ins===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)");}

				if (AKA_mark("lis===683###sois===15074###eois===15102###lif===12###soif===362###eoif===390###ifc===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)") && ((AKA_mark("lis===683###sois===15074###eois===15078###lif===12###soif===362###eoif===366###isc===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)")&&!rec)	||(AKA_mark("lis===683###sois===15082###eois===15102###lif===12###soif===370###eoif===390###isc===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)")&&!(set = set->subset)))) {
			AKA_mark("lis===684###sois===15107###eois===15113###lif===13###soif===395###eoif===401###ins===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)");break;
		}
		else {AKA_mark("lis===-683-###sois===-15074-###eois===-1507428-###lif===-12-###soif===-###eoif===-390-###ins===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)");}

				AKA_mark("lis===685###sois===15116###eois===15143###lif===14###soif===404###eoif===431###ins===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)");result = search(set, name);

	}

		AKA_mark("lis===687###sois===15148###eois===15162###lif===16###soif===436###eoif===450###ins===true###function===./app-framework-binder/src/afb-apiset.c/lookup(struct afb_apiset*,const char*,int)");return result;

}

/**
 * Get from the 'set' the API of 'name' in 'api'
 * @param set the set of API
 * @param name the name of the API to get
 * @param rec if not zero look also recursively in subsets
 * @return the api pointer in case of success or NULL in case of error
 */
/** Instrumented function afb_apiset_lookup(struct afb_apiset*,const char*,int) */
const struct afb_api_item *afb_apiset_lookup(struct afb_apiset *set, const char *name, int rec)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup(struct afb_apiset*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===699###sois===15523###eois===15542###lif===2###soif===99###eoif===118###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup(struct afb_apiset*,const char*,int)");struct api_desc *i;


		AKA_mark("lis===701###sois===15545###eois===15572###lif===4###soif===121###eoif===148###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup(struct afb_apiset*,const char*,int)");i = lookup(set, name, rec);

		if (AKA_mark("lis===702###sois===15578###eois===15579###lif===5###soif===154###eoif===155###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup(struct afb_apiset*,const char*,int)") && (AKA_mark("lis===702###sois===15578###eois===15579###lif===5###soif===154###eoif===155###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup(struct afb_apiset*,const char*,int)")&&i)) {
		AKA_mark("lis===703###sois===15583###eois===15598###lif===6###soif===159###eoif===174###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup(struct afb_apiset*,const char*,int)");return &i->api;
	}
	else {AKA_mark("lis===-702-###sois===-15578-###eois===-155781-###lif===-5-###soif===-###eoif===-155-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup(struct afb_apiset*,const char*,int)");}

		AKA_mark("lis===704###sois===15600###eois===15615###lif===7###soif===176###eoif===191###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup(struct afb_apiset*,const char*,int)");errno = ENOENT;

		AKA_mark("lis===705###sois===15617###eois===15629###lif===8###soif===193###eoif===205###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup(struct afb_apiset*,const char*,int)");return NULL;

}

static int start_api(struct api_desc *api);

/**
 * Start the apis of the 'array'
 */
/** Instrumented function start_array_apis(struct api_array*) */
static int start_array_apis(struct api_array *array)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)");AKA_fCall++;
		AKA_mark("lis===715###sois===15775###eois===15794###lif===2###soif===56###eoif===75###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)");int i, rc = 0, rc2;


		AKA_mark("lis===717###sois===15797###eois===15803###lif===4###soif===78###eoif===84###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)");i = 0;

		while (AKA_mark("lis===718###sois===15812###eois===15828###lif===5###soif===93###eoif===109###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)") && (AKA_mark("lis===718###sois===15812###eois===15828###lif===5###soif===93###eoif===109###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)")&&i < array->count)) {
				AKA_mark("lis===719###sois===15834###eois===15863###lif===6###soif===115###eoif===144###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)");rc2 = array->apis[i]->status;

				if (AKA_mark("lis===720###sois===15870###eois===15888###lif===7###soif===151###eoif===169###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)") && (AKA_mark("lis===720###sois===15870###eois===15888###lif===7###soif===151###eoif===169###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)")&&rc2 == NOT_STARTED)) {
						AKA_mark("lis===721###sois===15895###eois===15927###lif===8###soif===176###eoif===208###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)");rc2 = start_api(array->apis[i]);

						if (AKA_mark("lis===722###sois===15935###eois===15942###lif===9###soif===216###eoif===223###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)") && (AKA_mark("lis===722###sois===15935###eois===15942###lif===9###soif===216###eoif===223###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)")&&rc2 < 0)) {
				AKA_mark("lis===723###sois===15948###eois===15957###lif===10###soif===229###eoif===238###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)");rc = rc2;
			}
			else {AKA_mark("lis===-722-###sois===-15935-###eois===-159357-###lif===-9-###soif===-###eoif===-223-###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)");}

						AKA_mark("lis===724###sois===15961###eois===15967###lif===11###soif===242###eoif===248###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)");i = 0;

		}
		else {
						if (AKA_mark("lis===726###sois===15986###eois===15989###lif===13###soif===267###eoif===270###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)") && (AKA_mark("lis===726###sois===15986###eois===15989###lif===13###soif===267###eoif===270###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)")&&rc2)) {
				AKA_mark("lis===727###sois===15995###eois===16003###lif===14###soif===276###eoif===284###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)");rc = -1;
			}
			else {AKA_mark("lis===-726-###sois===-15986-###eois===-159863-###lif===-13-###soif===-###eoif===-270-###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)");}

						AKA_mark("lis===728###sois===16007###eois===16011###lif===15###soif===288###eoif===292###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)");i++;

		}

	}

		AKA_mark("lis===731###sois===16020###eois===16030###lif===18###soif===301###eoif===311###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_apis(struct api_array*)");return rc;

}

/**
 * Start the class 'cla' (start the apis that provide it).
 */
/** Instrumented function start_class(struct api_class*) */
static int start_class(struct api_class *cla)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/start_class(struct api_class*)");AKA_fCall++;
		AKA_mark("lis===739###sois===16150###eois===16191###lif===2###soif===49###eoif===90###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_class(struct api_class*)");return start_array_apis(&cla->providers);

}

/**
 * Start the classes of the 'array'
 */
/** Instrumented function start_array_classes(struct api_array*) */
static int start_array_classes(struct api_array *array)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/start_array_classes(struct api_array*)");AKA_fCall++;
		AKA_mark("lis===747###sois===16298###eois===16317###lif===2###soif===59###eoif===78###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_classes(struct api_array*)");int i, rc = 0, rc2;


		AKA_mark("lis===749###sois===16320###eois===16337###lif===4###soif===81###eoif===98###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_classes(struct api_array*)");i = array->count;

		while (AKA_mark("lis===750###sois===16346###eois===16347###lif===5###soif===107###eoif===108###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_classes(struct api_array*)") && (AKA_mark("lis===750###sois===16346###eois===16347###lif===5###soif===107###eoif===108###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_classes(struct api_array*)")&&i)) {
				AKA_mark("lis===751###sois===16353###eois===16392###lif===6###soif===114###eoif===153###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_classes(struct api_array*)");rc2 = start_class(array->classes[--i]);

				if (AKA_mark("lis===752###sois===16399###eois===16406###lif===7###soif===160###eoif===167###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_classes(struct api_array*)") && (AKA_mark("lis===752###sois===16399###eois===16406###lif===7###soif===160###eoif===167###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_classes(struct api_array*)")&&rc2 < 0)) {
						AKA_mark("lis===753###sois===16413###eois===16422###lif===8###soif===174###eoif===183###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_classes(struct api_array*)");rc = rc2;

		}
		else {AKA_mark("lis===-752-###sois===-16399-###eois===-163997-###lif===-7-###soif===-###eoif===-167-###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_classes(struct api_array*)");}

	}

		AKA_mark("lis===756###sois===16431###eois===16441###lif===11###soif===192###eoif===202###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_classes(struct api_array*)");return rc;

}

/**
 * Start the depends of the 'array'
 */
/** Instrumented function start_array_depends(struct api_array*) */
static int start_array_depends(struct api_array *array)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");AKA_fCall++;
		AKA_mark("lis===764###sois===16548###eois===16569###lif===2###soif===59###eoif===80###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");struct api_desc *api;

		AKA_mark("lis===765###sois===16571###eois===16590###lif===3###soif===82###eoif===101###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");int i, rc = 0, rc2;


		AKA_mark("lis===767###sois===16593###eois===16599###lif===5###soif===104###eoif===110###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");i = 0;

		while (AKA_mark("lis===768###sois===16608###eois===16624###lif===6###soif===119###eoif===135###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)") && (AKA_mark("lis===768###sois===16608###eois===16624###lif===6###soif===119###eoif===135###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)")&&i < array->count)) {
				AKA_mark("lis===769###sois===16630###eois===16695###lif===7###soif===141###eoif===206###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");api = searchrec(array->depends[i]->set, array->depends[i]->name);

				if (AKA_mark("lis===770###sois===16702###eois===16706###lif===8###soif===213###eoif===217###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)") && (AKA_mark("lis===770###sois===16702###eois===16706###lif===8###soif===213###eoif===217###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)")&&!api)) {
						AKA_mark("lis===771###sois===16713###eois===16721###lif===9###soif===224###eoif===232###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");rc = -1;

						AKA_mark("lis===772###sois===16725###eois===16729###lif===10###soif===236###eoif===240###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");i++;

		}
		else {
						AKA_mark("lis===774###sois===16744###eois===16762###lif===12###soif===255###eoif===273###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");rc2 = api->status;

						if (AKA_mark("lis===775###sois===16770###eois===16788###lif===13###soif===281###eoif===299###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)") && (AKA_mark("lis===775###sois===16770###eois===16788###lif===13###soif===281###eoif===299###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)")&&rc2 == NOT_STARTED)) {
								AKA_mark("lis===776###sois===16796###eois===16817###lif===14###soif===307###eoif===328###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");rc2 = start_api(api);

								if (AKA_mark("lis===777###sois===16826###eois===16833###lif===15###soif===337###eoif===344###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)") && (AKA_mark("lis===777###sois===16826###eois===16833###lif===15###soif===337###eoif===344###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)")&&rc2 < 0)) {
					AKA_mark("lis===778###sois===16840###eois===16849###lif===16###soif===351###eoif===360###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");rc = rc2;
				}
				else {AKA_mark("lis===-777-###sois===-16826-###eois===-168267-###lif===-15-###soif===-###eoif===-344-###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");}

								AKA_mark("lis===779###sois===16854###eois===16860###lif===17###soif===365###eoif===371###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");i = 0;

			}
			else {
								if (AKA_mark("lis===781###sois===16881###eois===16884###lif===19###soif===392###eoif===395###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)") && (AKA_mark("lis===781###sois===16881###eois===16884###lif===19###soif===392###eoif===395###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)")&&rc2)) {
					AKA_mark("lis===782###sois===16891###eois===16899###lif===20###soif===402###eoif===410###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");rc = -1;
				}
				else {AKA_mark("lis===-781-###sois===-16881-###eois===-168813-###lif===-19-###soif===-###eoif===-395-###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");}

								AKA_mark("lis===783###sois===16904###eois===16908###lif===21###soif===415###eoif===419###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");i++;

			}

		}

	}


		AKA_mark("lis===788###sois===16923###eois===16933###lif===26###soif===434###eoif===444###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_array_depends(struct api_array*)");return rc;

}

/**
 * Starts the service 'api'.
 * @param api the api
 * @return zero on success, -1 on error
 */
/** Instrumented function start_api(struct api_desc*) */
static int start_api(struct api_desc *api)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");AKA_fCall++;
		AKA_mark("lis===798###sois===17082###eois===17089###lif===2###soif===46###eoif===53###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");int rc;


		if (AKA_mark("lis===800###sois===17096###eois===17122###lif===4###soif===60###eoif===86###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)") && (AKA_mark("lis===800###sois===17096###eois===17122###lif===4###soif===60###eoif===86###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)")&&api->status != NOT_STARTED)) {
				if (AKA_mark("lis===801###sois===17132###eois===17147###lif===5###soif===96###eoif===111###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)") && (AKA_mark("lis===801###sois===17132###eois===17147###lif===5###soif===96###eoif===111###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)")&&api->status > 0)) {
						AKA_mark("lis===802###sois===17154###eois===17174###lif===6###soif===118###eoif===138###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");errno = api->status;

						AKA_mark("lis===803###sois===17178###eois===17188###lif===7###soif===142###eoif===152###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");return -1;

		}
		else {AKA_mark("lis===-801-###sois===-17132-###eois===-1713215-###lif===-5-###soif===-###eoif===-111-###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");}

				AKA_mark("lis===805###sois===17195###eois===17204###lif===9###soif===159###eoif===168###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");return 0;

	}
	else {AKA_mark("lis===-800-###sois===-17096-###eois===-1709626-###lif===-4-###soif===-###eoif===-86-###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");}


		AKA_mark("lis===808###sois===17210###eois===17250###lif===12###soif===174###eoif===214###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");NOTICE("API %s starting...", api->name);

		AKA_mark("lis===809###sois===17252###eois===17272###lif===13###soif===216###eoif===236###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");api->status = EBUSY;

		AKA_mark("lis===810###sois===17274###eois===17322###lif===14###soif===238###eoif===286###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");rc = start_array_classes(&api->require.classes);

		if (AKA_mark("lis===811###sois===17328###eois===17334###lif===15###soif===292###eoif===298###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)") && (AKA_mark("lis===811###sois===17328###eois===17334###lif===15###soif===292###eoif===298###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)")&&rc < 0)) {
		AKA_mark("lis===812###sois===17338###eois===17396###lif===16###soif===302###eoif===360###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");ERROR("Cannot start classes needed by api %s", api->name);
	}
	else {
				AKA_mark("lis===814###sois===17407###eois===17452###lif===18###soif===371###eoif===416###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");rc = start_array_depends(&api->require.apis);

				if (AKA_mark("lis===815###sois===17459###eois===17465###lif===19###soif===423###eoif===429###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)") && (AKA_mark("lis===815###sois===17459###eois===17465###lif===19###soif===423###eoif===429###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)")&&rc < 0)) {
			AKA_mark("lis===816###sois===17470###eois===17525###lif===20###soif===434###eoif===489###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");ERROR("Cannot start apis needed by api %s", api->name);
		}
		else {
			if (AKA_mark("lis===817###sois===17537###eois===17564###lif===21###soif===501###eoif===528###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)") && (AKA_mark("lis===817###sois===17537###eois===17564###lif===21###soif===501###eoif===528###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)")&&api->api.itf->service_start)) {
							AKA_mark("lis===818###sois===17571###eois===17622###lif===22###soif===535###eoif===586###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");rc = api->api.itf->service_start(api->api.closure);

							if (AKA_mark("lis===819###sois===17630###eois===17636###lif===23###soif===594###eoif===600###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)") && (AKA_mark("lis===819###sois===17630###eois===17636###lif===23###soif===594###eoif===600###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)")&&rc < 0)) {
					AKA_mark("lis===820###sois===17642###eois===17689###lif===24###soif===606###eoif===653###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");ERROR("The api %s failed to start", api->name);
				}
				else {AKA_mark("lis===-819-###sois===-17630-###eois===-176306-###lif===-23-###soif===-###eoif===-600-###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");}

		}
			else {AKA_mark("lis===-817-###sois===-17537-###eois===-1753727-###lif===-21-###soif===-###eoif===-528-###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");}
		}

	}

		if (AKA_mark("lis===823###sois===17702###eois===17708###lif===27###soif===666###eoif===672###ifc===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)") && (AKA_mark("lis===823###sois===17702###eois===17708###lif===27###soif===666###eoif===672###isc===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)")&&rc < 0)) {
				AKA_mark("lis===824###sois===17714###eois===17747###lif===28###soif===678###eoif===711###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");api->status = errno ?: ECANCELED;

				AKA_mark("lis===825###sois===17750###eois===17760###lif===29###soif===714###eoif===724###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");return -1;

	}
	else {AKA_mark("lis===-823-###sois===-17702-###eois===-177026-###lif===-27-###soif===-###eoif===-672-###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");}

		AKA_mark("lis===827###sois===17765###eois===17799###lif===31###soif===729###eoif===763###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");INFO("API %s started", api->name);

		AKA_mark("lis===828###sois===17801###eois===17817###lif===32###soif===765###eoif===781###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");api->status = 0;

		AKA_mark("lis===829###sois===17819###eois===17828###lif===33###soif===783###eoif===792###ins===true###function===./app-framework-binder/src/afb-apiset.c/start_api(struct api_desc*)");return 0;

}

/**
 * Get from the 'set' the API of 'name' in 'api'
 * @param set the set of API
 * @param name the name of the API to get
 * @param rec if not zero look also recursively in subsets
 * @return a pointer to the API item in case of success or NULL in case of error
 */
/** Instrumented function afb_apiset_lookup_started(struct afb_apiset*,const char*,int) */
const struct afb_api_item *afb_apiset_lookup_started(struct afb_apiset *set, const char *name, int rec)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup_started(struct afb_apiset*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===841###sois===18207###eois===18229###lif===2###soif===107###eoif===129###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup_started(struct afb_apiset*,const char*,int)");struct api_desc *desc;


		AKA_mark("lis===843###sois===18232###eois===18262###lif===4###soif===132###eoif===162###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup_started(struct afb_apiset*,const char*,int)");desc = lookup(set, name, rec);

		if (AKA_mark("lis===844###sois===18268###eois===18273###lif===5###soif===168###eoif===173###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup_started(struct afb_apiset*,const char*,int)") && (AKA_mark("lis===844###sois===18268###eois===18273###lif===5###soif===168###eoif===173###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup_started(struct afb_apiset*,const char*,int)")&&!desc)) {
				AKA_mark("lis===845###sois===18279###eois===18294###lif===6###soif===179###eoif===194###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup_started(struct afb_apiset*,const char*,int)");errno = ENOENT;

				AKA_mark("lis===846###sois===18297###eois===18309###lif===7###soif===197###eoif===209###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup_started(struct afb_apiset*,const char*,int)");return NULL;

	}
	else {AKA_mark("lis===-844-###sois===-18268-###eois===-182685-###lif===-5-###soif===-###eoif===-173-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup_started(struct afb_apiset*,const char*,int)");}

		if (AKA_mark("lis===848###sois===18318###eois===18333###lif===9###soif===218###eoif===233###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup_started(struct afb_apiset*,const char*,int)") && (AKA_mark("lis===848###sois===18318###eois===18333###lif===9###soif===218###eoif===233###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup_started(struct afb_apiset*,const char*,int)")&&start_api(desc))) {
				AKA_mark("lis===849###sois===18339###eois===18360###lif===10###soif===239###eoif===260###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup_started(struct afb_apiset*,const char*,int)");errno = desc->status;

				AKA_mark("lis===850###sois===18363###eois===18375###lif===11###soif===263###eoif===275###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup_started(struct afb_apiset*,const char*,int)");return NULL;

	}
	else {AKA_mark("lis===-848-###sois===-18318-###eois===-1831815-###lif===-9-###soif===-###eoif===-233-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup_started(struct afb_apiset*,const char*,int)");}

		AKA_mark("lis===852###sois===18380###eois===18398###lif===13###soif===280###eoif===298###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_lookup_started(struct afb_apiset*,const char*,int)");return &desc->api;

}

/**
 * Starts a service by its 'api' name.
 * @param set the api set
 * @param name name of the service to start
 * @return zero on success, -1 on error
 */
/** Instrumented function afb_apiset_start_service(struct afb_apiset*,const char*) */
int afb_apiset_start_service(struct afb_apiset *set, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_start_service(struct afb_apiset*,const char*)");AKA_fCall++;
		AKA_mark("lis===863###sois===18633###eois===18652###lif===2###soif===74###eoif===93###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_service(struct afb_apiset*,const char*)");struct api_desc *a;


		AKA_mark("lis===865###sois===18655###eois===18680###lif===4###soif===96###eoif===121###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_service(struct afb_apiset*,const char*)");a = searchrec(set, name);

		if (AKA_mark("lis===866###sois===18686###eois===18688###lif===5###soif===127###eoif===129###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_service(struct afb_apiset*,const char*)") && (AKA_mark("lis===866###sois===18686###eois===18688###lif===5###soif===127###eoif===129###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_service(struct afb_apiset*,const char*)")&&!a)) {
				AKA_mark("lis===867###sois===18694###eois===18731###lif===6###soif===135###eoif===172###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_service(struct afb_apiset*,const char*)");ERROR("can't find service %s", name);

				AKA_mark("lis===868###sois===18734###eois===18749###lif===7###soif===175###eoif===190###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_service(struct afb_apiset*,const char*)");errno = ENOENT;

				AKA_mark("lis===869###sois===18752###eois===18762###lif===8###soif===193###eoif===203###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_service(struct afb_apiset*,const char*)");return -1;

	}
	else {AKA_mark("lis===-866-###sois===-18686-###eois===-186862-###lif===-5-###soif===-###eoif===-129-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_service(struct afb_apiset*,const char*)");}


		AKA_mark("lis===872###sois===18768###eois===18788###lif===11###soif===209###eoif===229###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_service(struct afb_apiset*,const char*)");return start_api(a);

}

/**
 * Starts all possible services but stops at first error.
 * @param set the api set
 * @return 0 on success or a negative number when an error is found
 */
/** Instrumented function afb_apiset_start_all_services(struct afb_apiset*) */
int afb_apiset_start_all_services(struct afb_apiset *set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===882###sois===19013###eois===19040###lif===2###soif===61###eoif===88###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");struct afb_apiset *rootset;

		AKA_mark("lis===883###sois===19042###eois===19054###lif===3###soif===90###eoif===102###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");int rc, ret;

		AKA_mark("lis===884###sois===19056###eois===19062###lif===4###soif===104###eoif===110###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");int i;


		AKA_mark("lis===886###sois===19065###eois===19079###lif===6###soif===113###eoif===127###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");rootset = set;

		AKA_mark("lis===887###sois===19081###eois===19089###lif===7###soif===129###eoif===137###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");ret = 0;

		while (AKA_mark("lis===888###sois===19098###eois===19101###lif===8###soif===146###eoif===149###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)") && (AKA_mark("lis===888###sois===19098###eois===19101###lif===8###soif===146###eoif===149###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)")&&set)) {
				AKA_mark("lis===889###sois===19107###eois===19113###lif===9###soif===155###eoif===161###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");i = 0;

				while (AKA_mark("lis===890###sois===19123###eois===19142###lif===10###soif===171###eoif===190###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)") && (AKA_mark("lis===890###sois===19123###eois===19142###lif===10###soif===171###eoif===190###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)")&&i < set->apis.count)) {
						AKA_mark("lis===891###sois===19149###eois===19180###lif===11###soif===197###eoif===228###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");rc = set->apis.apis[i]->status;

						if (AKA_mark("lis===892###sois===19188###eois===19205###lif===12###soif===236###eoif===253###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)") && (AKA_mark("lis===892###sois===19188###eois===19205###lif===12###soif===236###eoif===253###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)")&&rc == NOT_STARTED)) {
								AKA_mark("lis===893###sois===19213###eois===19247###lif===13###soif===261###eoif===295###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");rc = start_api(set->apis.apis[i]);

								if (AKA_mark("lis===894###sois===19256###eois===19262###lif===14###soif===304###eoif===310###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)") && (AKA_mark("lis===894###sois===19256###eois===19262###lif===14###soif===304###eoif===310###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)")&&rc < 0)) {
					AKA_mark("lis===895###sois===19269###eois===19278###lif===15###soif===317###eoif===326###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");ret = rc;
				}
				else {AKA_mark("lis===-894-###sois===-19256-###eois===-192566-###lif===-14-###soif===-###eoif===-310-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");}

								AKA_mark("lis===896###sois===19283###eois===19297###lif===16###soif===331###eoif===345###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");set = rootset;

								AKA_mark("lis===897###sois===19302###eois===19308###lif===17###soif===350###eoif===356###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");i = 0;

			}
			else {
								if (AKA_mark("lis===899###sois===19329###eois===19331###lif===19###soif===377###eoif===379###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)") && (AKA_mark("lis===899###sois===19329###eois===19331###lif===19###soif===377###eoif===379###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)")&&rc)) {
					AKA_mark("lis===900###sois===19338###eois===19347###lif===20###soif===386###eoif===395###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");ret = -1;
				}
				else {AKA_mark("lis===-899-###sois===-19329-###eois===-193292-###lif===-19-###soif===-###eoif===-379-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");}

								AKA_mark("lis===901###sois===19352###eois===19356###lif===21###soif===400###eoif===404###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");i++;

			}

		}

				AKA_mark("lis===904###sois===19368###eois===19386###lif===24###soif===416###eoif===434###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");set = set->subset;

	}

		AKA_mark("lis===906###sois===19391###eois===19402###lif===26###soif===439###eoif===450###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_start_all_services(struct afb_apiset*)");return ret;

}

#if WITH_AFB_HOOK
/**
 * Ask to update the hook flags of the 'api'
 * @param set the api set
 * @param name the api to update (NULL updates all)
 */
void afb_apiset_update_hooks(struct afb_apiset *set, const char *name)
{
	struct api_desc **i, **e, *d;

	if (!name) {
		i = set->apis.apis;
		e = &set->apis.apis[set->apis.count];
		while (i != e) {
			d = *i++;
			if (d->api.itf->update_hooks)
				d->api.itf->update_hooks(d->api.closure);
		}
	} else {
		d = searchrec(set, name);
		if (d && d->api.itf->update_hooks)
			d->api.itf->update_hooks(d->api.closure);
	}
}
#endif

/**
 * Set the logmask of the 'api' to 'mask'
 * @param set the api set
 * @param name the api to set (NULL set all)
 */
/** Instrumented function afb_apiset_set_logmask(struct afb_apiset*,const char*,int) */
void afb_apiset_set_logmask(struct afb_apiset *set, const char *name, int mask)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===942###sois===20188###eois===20194###lif===2###soif===83###eoif===89###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)");int i;

		AKA_mark("lis===943###sois===20196###eois===20215###lif===3###soif===91###eoif===110###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)");struct api_desc *d;


		if (AKA_mark("lis===945###sois===20222###eois===20227###lif===5###soif===117###eoif===122###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)") && (AKA_mark("lis===945###sois===20222###eois===20227###lif===5###soif===117###eoif===122###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)")&&!name)) {
				AKA_mark("lis===946###sois===20238###eois===20245###lif===6###soif===133###eoif===140###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)");for (i = 0 ;AKA_mark("lis===946###sois===20246###eois===20265###lif===6###soif===141###eoif===160###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)") && AKA_mark("lis===946###sois===20246###eois===20265###lif===6###soif===141###eoif===160###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)")&&i < set->apis.count;({AKA_mark("lis===946###sois===20268###eois===20271###lif===6###soif===163###eoif===166###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)");i++;})) {
						AKA_mark("lis===947###sois===20278###eois===20300###lif===7###soif===173###eoif===195###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)")			;
d = set->apis.apis[i];
;
						if (AKA_mark("lis===948###sois===20309###eois===20332###lif===8###soif===204###eoif===227###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)") && (AKA_mark("lis===948###sois===20309###eois===20332###lif===8###soif===204###eoif===227###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)")&&d->api.itf->set_logmask)) {
				AKA_mark("lis===949###sois===20338###eois===20384###lif===9###soif===233###eoif===279###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)");d->api.itf->set_logmask(d->api.closure, mask);
			}
			else {AKA_mark("lis===-948-###sois===-20309-###eois===-2030923-###lif===-8-###soif===-###eoif===-227-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)");}

		}

	}
	else {
				AKA_mark("lis===952###sois===20401###eois===20426###lif===12###soif===296###eoif===321###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)");d = searchrec(set, name);

				if (AKA_mark("lis===953###sois===20433###eois===20461###lif===13###soif===328###eoif===356###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)") && ((AKA_mark("lis===953###sois===20433###eois===20434###lif===13###soif===328###eoif===329###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)")&&d)	&&(AKA_mark("lis===953###sois===20438###eois===20461###lif===13###soif===333###eoif===356###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)")&&d->api.itf->set_logmask))) {
			AKA_mark("lis===954###sois===20466###eois===20512###lif===14###soif===361###eoif===407###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)");d->api.itf->set_logmask(d->api.closure, mask);
		}
		else {AKA_mark("lis===-953-###sois===-20433-###eois===-2043328-###lif===-13-###soif===-###eoif===-356-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_set_logmask(struct afb_apiset*,const char*,int)");}

	}

}

/**
 * Get the logmask level of the 'api'
 * @param set the api set
 * @param name the api to get
 * @return the logmask level or -1 in case of error
 */
/** Instrumented function afb_apiset_get_logmask(struct afb_apiset*,const char*) */
int afb_apiset_get_logmask(struct afb_apiset *set, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_get_logmask(struct afb_apiset*,const char*)");AKA_fCall++;
		AKA_mark("lis===966###sois===20745###eois===20770###lif===2###soif===72###eoif===97###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_logmask(struct afb_apiset*,const char*)");const struct api_desc *i;


		AKA_mark("lis===968###sois===20773###eois===20812###lif===4###soif===100###eoif===139###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_logmask(struct afb_apiset*,const char*)");i = name ? searchrec(set, name) : NULL;

		if (AKA_mark("lis===969###sois===20818###eois===20820###lif===5###soif===145###eoif===147###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_logmask(struct afb_apiset*,const char*)") && (AKA_mark("lis===969###sois===20818###eois===20820###lif===5###soif===145###eoif===147###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_logmask(struct afb_apiset*,const char*)")&&!i)) {
				AKA_mark("lis===970###sois===20826###eois===20841###lif===6###soif===153###eoif===168###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_logmask(struct afb_apiset*,const char*)");errno = ENOENT;

				AKA_mark("lis===971###sois===20844###eois===20854###lif===7###soif===171###eoif===181###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_logmask(struct afb_apiset*,const char*)");return -1;

	}
	else {AKA_mark("lis===-969-###sois===-20818-###eois===-208182-###lif===-5-###soif===-###eoif===-147-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_logmask(struct afb_apiset*,const char*)");}


		if (AKA_mark("lis===974###sois===20864###eois===20888###lif===10###soif===191###eoif===215###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_logmask(struct afb_apiset*,const char*)") && (AKA_mark("lis===974###sois===20864###eois===20888###lif===10###soif===191###eoif===215###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_logmask(struct afb_apiset*,const char*)")&&!i->api.itf->get_logmask)) {
		AKA_mark("lis===975###sois===20892###eois===20907###lif===11###soif===219###eoif===234###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_logmask(struct afb_apiset*,const char*)");return logmask;
	}
	else {AKA_mark("lis===-974-###sois===-20864-###eois===-2086424-###lif===-10-###soif===-###eoif===-215-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_logmask(struct afb_apiset*,const char*)");}


		AKA_mark("lis===977###sois===20910###eois===20957###lif===13###soif===237###eoif===284###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_logmask(struct afb_apiset*,const char*)");return i->api.itf->get_logmask(i->api.closure);

}

/** Instrumented function afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*) */
void afb_apiset_describe(struct afb_apiset *set, const char *name, void (*describecb)(void *, struct json_object *), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)");AKA_fCall++;
		AKA_mark("lis===982###sois===21096###eois===21121###lif===2###soif===135###eoif===160###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)");const struct api_desc *i;

		AKA_mark("lis===983###sois===21123###eois===21145###lif===3###soif===162###eoif===184###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)");struct json_object *r;


		AKA_mark("lis===985###sois===21148###eois===21157###lif===5###soif===187###eoif===196###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)");r = NULL;

		if (AKA_mark("lis===986###sois===21163###eois===21167###lif===6###soif===202###eoif===206###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)") && (AKA_mark("lis===986###sois===21163###eois===21167###lif===6###soif===202###eoif===206###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)")&&name)) {
				AKA_mark("lis===987###sois===21173###eois===21198###lif===7###soif===212###eoif===237###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)");i = searchrec(set, name);

				if (AKA_mark("lis===988###sois===21205###eois===21206###lif===8###soif===244###eoif===245###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)") && (AKA_mark("lis===988###sois===21205###eois===21206###lif===8###soif===244###eoif===245###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)")&&i)) {
						if (AKA_mark("lis===989###sois===21217###eois===21237###lif===9###soif===256###eoif===276###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)") && (AKA_mark("lis===989###sois===21217###eois===21237###lif===9###soif===256###eoif===276###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)")&&i->api.itf->describe)) {
								AKA_mark("lis===990###sois===21245###eois===21303###lif===10###soif===284###eoif===342###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)");i->api.itf->describe(i->api.closure, describecb, closure);

								AKA_mark("lis===991###sois===21308###eois===21315###lif===11###soif===347###eoif===354###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)");return;

			}
			else {AKA_mark("lis===-989-###sois===-21217-###eois===-2121720-###lif===-9-###soif===-###eoif===-276-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)");}

		}
		else {AKA_mark("lis===-988-###sois===-21205-###eois===-212051-###lif===-8-###soif===-###eoif===-245-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)");}

	}
	else {AKA_mark("lis===-986-###sois===-21163-###eois===-211634-###lif===-6-###soif===-###eoif===-206-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)");}

		AKA_mark("lis===995###sois===21329###eois===21352###lif===15###soif===368###eoif===391###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_describe(struct afb_apiset*,const char*,void(*describecb)(void*, struct json_object*),void*)");describecb(closure, r);

}


struct get_names {
	union  {
		struct {
			size_t count;
			size_t size;
		};
		struct {
			const char **ptr;
			char *data;
		};
	};
	int type;
};

/** Instrumented function get_names_count(void*,struct afb_apiset*,const char*,int) */
static void get_names_count(void *closure, struct afb_apiset *set, const char *name, int isalias)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/get_names_count(void*,struct afb_apiset*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===1015###sois===21607###eois===21638###lif===2###soif===101###eoif===132###ins===true###function===./app-framework-binder/src/afb-apiset.c/get_names_count(void*,struct afb_apiset*,const char*,int)");struct get_names *gc = closure;

		if (AKA_mark("lis===1016###sois===21644###eois===21668###lif===3###soif===138###eoif===162###ifc===true###function===./app-framework-binder/src/afb-apiset.c/get_names_count(void*,struct afb_apiset*,const char*,int)") && (AKA_mark("lis===1016###sois===21644###eois===21668###lif===3###soif===138###eoif===162###isc===true###function===./app-framework-binder/src/afb-apiset.c/get_names_count(void*,struct afb_apiset*,const char*,int)")&&(1 + isalias) & gc->type)) {
				AKA_mark("lis===1017###sois===21674###eois===21699###lif===4###soif===168###eoif===193###ins===true###function===./app-framework-binder/src/afb-apiset.c/get_names_count(void*,struct afb_apiset*,const char*,int)");gc->size += strlen(name);

				AKA_mark("lis===1018###sois===21702###eois===21714###lif===5###soif===196###eoif===208###ins===true###function===./app-framework-binder/src/afb-apiset.c/get_names_count(void*,struct afb_apiset*,const char*,int)");gc->count++;

	}
	else {AKA_mark("lis===-1016-###sois===-21644-###eois===-2164424-###lif===-3-###soif===-###eoif===-162-###ins===true###function===./app-framework-binder/src/afb-apiset.c/get_names_count(void*,struct afb_apiset*,const char*,int)");}

}

/** Instrumented function get_names_value(void*,struct afb_apiset*,const char*,int) */
static void get_names_value(void *closure, struct afb_apiset *set, const char *name, int isalias)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/get_names_value(void*,struct afb_apiset*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===1024###sois===21822###eois===21853###lif===2###soif===101###eoif===132###ins===true###function===./app-framework-binder/src/afb-apiset.c/get_names_value(void*,struct afb_apiset*,const char*,int)");struct get_names *gc = closure;

		if (AKA_mark("lis===1025###sois===21859###eois===21883###lif===3###soif===138###eoif===162###ifc===true###function===./app-framework-binder/src/afb-apiset.c/get_names_value(void*,struct afb_apiset*,const char*,int)") && (AKA_mark("lis===1025###sois===21859###eois===21883###lif===3###soif===138###eoif===162###isc===true###function===./app-framework-binder/src/afb-apiset.c/get_names_value(void*,struct afb_apiset*,const char*,int)")&&(1 + isalias) & gc->type)) {
				AKA_mark("lis===1026###sois===21889###eois===21911###lif===4###soif===168###eoif===190###ins===true###function===./app-framework-binder/src/afb-apiset.c/get_names_value(void*,struct afb_apiset*,const char*,int)");*gc->ptr++ = gc->data;

				AKA_mark("lis===1027###sois===21914###eois===21952###lif===5###soif===193###eoif===231###ins===true###function===./app-framework-binder/src/afb-apiset.c/get_names_value(void*,struct afb_apiset*,const char*,int)");gc->data = stpcpy(gc->data, name) + 1;

	}
	else {AKA_mark("lis===-1025-###sois===-21859-###eois===-2185924-###lif===-3-###soif===-###eoif===-162-###ins===true###function===./app-framework-binder/src/afb-apiset.c/get_names_value(void*,struct afb_apiset*,const char*,int)");}

}

#if !defined(APISET_NO_SORT)
/** Instrumented function get_names_sortcb(const void*,const void*) */
static int get_names_sortcb(const void *a, const void *b)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/get_names_sortcb(const void*,const void*)");AKA_fCall++;
		AKA_mark("lis===1034###sois===22049###eois===22105###lif===2###soif===61###eoif===117###ins===true###function===./app-framework-binder/src/afb-apiset.c/get_names_sortcb(const void*,const void*)");return strcasecmp(*(const char **)a, *(const char **)b);

}
#endif

/**
 * Get the list of api names
 * @param set the api set
 * @param rec recursive
 * @param type expected type: 1 names, 3 names+aliases, 2 aliases
 * @return a NULL terminated array of api names. Must be freed.
 */
/** Instrumented function afb_apiset_get_names(struct afb_apiset*,int,int) */
const char **afb_apiset_get_names(struct afb_apiset *set, int rec, int type)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");AKA_fCall++;
		AKA_mark("lis===1047###sois===22413###eois===22433###lif===2###soif===80###eoif===100###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");struct get_names gc;

		AKA_mark("lis===1048###sois===22435###eois===22447###lif===3###soif===102###eoif===114###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");size_t size;

		AKA_mark("lis===1049###sois===22449###eois===22468###lif===4###soif===116###eoif===135###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");const char **names;


		AKA_mark("lis===1051###sois===22471###eois===22494###lif===6###soif===138###eoif===161###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");gc.count = gc.size = 0;

		AKA_mark("lis===1052###sois===22496###eois===22540###lif===7###soif===163###eoif===207###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");gc.type = type >= 1 && type <= 3 ? type : 1;

		AKA_mark("lis===1053###sois===22542###eois===22590###lif===8###soif===209###eoif===257###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");afb_apiset_enum(set, rec, get_names_count, &gc);


		AKA_mark("lis===1055###sois===22593###eois===22658###lif===10###soif===260###eoif===325###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");size = gc.size + gc.count * (1 + sizeof *names) + sizeof(*names);

		AKA_mark("lis===1056###sois===22660###eois===22681###lif===11###soif===327###eoif===348###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");names = malloc(size);


		if (AKA_mark("lis===1058###sois===22688###eois===22694###lif===13###soif===355###eoif===361###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)") && (AKA_mark("lis===1058###sois===22688###eois===22694###lif===13###soif===355###eoif===361###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)")&&!names)) {
		AKA_mark("lis===1059###sois===22698###eois===22713###lif===14###soif===365###eoif===380###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");errno = ENOMEM;
	}
	else {
				AKA_mark("lis===1061###sois===22724###eois===22762###lif===16###soif===391###eoif===429###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");gc.data = (char*)&names[gc.count + 1];

				AKA_mark("lis===1062###sois===22765###eois===22780###lif===17###soif===432###eoif===447###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");gc.ptr = names;

				AKA_mark("lis===1063###sois===22783###eois===22831###lif===18###soif===450###eoif===498###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");afb_apiset_enum(set, rec, get_names_value, &gc);

#if !defined(APISET_NO_SORT)
				AKA_mark("lis===1065###sois===22863###eois===22925###lif===20###soif===530###eoif===592###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");qsort(names, gc.ptr - names, sizeof *names, get_names_sortcb);

#endif
				AKA_mark("lis===1067###sois===22935###eois===22950###lif===22###soif===602###eoif===617###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");*gc.ptr = NULL;

	}

		AKA_mark("lis===1069###sois===22955###eois===22968###lif===24###soif===622###eoif===635###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_get_names(struct afb_apiset*,int,int)");return names;

}

/**
 * Enumerate the api names to a callback.
 * @param set the api set
 * @param rec should the enumeration be recursive
 * @param callback the function to call for each name
 * @param closure the closure for the callback
 */
/** Instrumented function afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*) */
void afb_apiset_enum(
	struct afb_apiset *set,
	int rec,
	void (*callback)(void *closure, struct afb_apiset *set, const char *name, int isalias),
	void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");AKA_fCall++;
		AKA_mark("lis===1085###sois===23364###eois===23370###lif===6###soif===165###eoif===171###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");int i;

		AKA_mark("lis===1086###sois===23372###eois===23396###lif===7###soif===173###eoif===197###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");struct afb_apiset *iset;

		AKA_mark("lis===1087###sois===23398###eois===23417###lif===8###soif===199###eoif===218###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");struct api_desc *d;

		AKA_mark("lis===1088###sois===23419###eois===23439###lif===9###soif===220###eoif===240###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");struct api_alias *a;


		AKA_mark("lis===1090###sois===23442###eois===23453###lif===11###soif===243###eoif===254###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");iset = set;

		while (AKA_mark("lis===1091###sois===23462###eois===23466###lif===12###soif===263###eoif===267###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)") && (AKA_mark("lis===1091###sois===23462###eois===23466###lif===12###soif===263###eoif===267###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)")&&iset)) {
				AKA_mark("lis===1092###sois===23477###eois===23484###lif===13###soif===278###eoif===285###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");for (i = 0 ;AKA_mark("lis===1092###sois===23485###eois===23504###lif===13###soif===286###eoif===305###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)") && AKA_mark("lis===1092###sois===23485###eois===23504###lif===13###soif===286###eoif===305###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)")&&i < set->apis.count;({AKA_mark("lis===1092###sois===23507###eois===23510###lif===13###soif===308###eoif===311###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");i++;})) {
						AKA_mark("lis===1093###sois===23517###eois===23539###lif===14###soif===318###eoif===340###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)")			;
d = set->apis.apis[i];
;
						if (AKA_mark("lis===1094###sois===23548###eois===23576###lif===15###soif===349###eoif===377###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)") && (AKA_mark("lis===1094###sois===23548###eois===23576###lif===15###soif===349###eoif===377###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)")&&searchrec(set, d->name) == d)) {
				AKA_mark("lis===1095###sois===23582###eois===23618###lif===16###soif===383###eoif===419###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");callback(closure, iset, d->name, 0);
			}
			else {AKA_mark("lis===-1094-###sois===-23548-###eois===-2354828-###lif===-15-###soif===-###eoif===-377-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");}

		}

				AKA_mark("lis===1097###sois===23625###eois===23643###lif===18###soif===426###eoif===444###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");a = iset->aliases;

				while (AKA_mark("lis===1098###sois===23653###eois===23654###lif===19###soif===454###eoif===455###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)") && (AKA_mark("lis===1098###sois===23653###eois===23654###lif===19###soif===454###eoif===455###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)")&&a)) {
						if (AKA_mark("lis===1099###sois===23665###eois===23698###lif===20###soif===466###eoif===499###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)") && (AKA_mark("lis===1099###sois===23665###eois===23698###lif===20###soif===466###eoif===499###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)")&&searchrec(set, a->name) == a->api)) {
				AKA_mark("lis===1100###sois===23704###eois===23740###lif===21###soif===505###eoif===541###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");callback(closure, iset, a->name, 1);
			}
			else {AKA_mark("lis===-1099-###sois===-23665-###eois===-2366533-###lif===-20-###soif===-###eoif===-499-###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");}

						AKA_mark("lis===1101###sois===23744###eois===23756###lif===22###soif===545###eoif===557###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");a = a->next;

		}

				AKA_mark("lis===1103###sois===23763###eois===23796###lif===24###soif===564###eoif===597###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_enum(struct afb_apiset*,int,void(*callback)(void*closure, struct afb_apiset*set, const char*name, int isalias),void*)");iset = rec ? iset->subset : NULL;

	}

}

/**
 * Declare that the api of 'name' requires the api of name 'required'.
 * The api is searched in the apiset 'set' and if 'rec' isn't null also in its subset.
 * Returns 0 if the declaration successed or -1 in case of failure
 * (ENOMEM: allocation failure, ENOENT: api name not found)
 */
/** Instrumented function afb_apiset_require(struct afb_apiset*,const char*,const char*) */
int afb_apiset_require(struct afb_apiset *set, const char *name, const char *required)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===1115###sois===24186###eois===24205###lif===2###soif===90###eoif===109###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)");struct api_desc *a;

		AKA_mark("lis===1116###sois===24207###eois===24228###lif===3###soif===111###eoif===132###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)");struct api_depend *d;

		AKA_mark("lis===1117###sois===24230###eois===24242###lif===4###soif===134###eoif===146###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)");int rc = -1;


		AKA_mark("lis===1119###sois===24245###eois===24270###lif===6###soif===149###eoif===174###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)");a = searchrec(set, name);

		if (AKA_mark("lis===1120###sois===24276###eois===24278###lif===7###soif===180###eoif===182###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)") && (AKA_mark("lis===1120###sois===24276###eois===24278###lif===7###soif===180###eoif===182###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)")&&!a)) {
		AKA_mark("lis===1121###sois===24282###eois===24297###lif===8###soif===186###eoif===201###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)");errno = ENOENT;
	}
	else {
				AKA_mark("lis===1123###sois===24308###eois===24353###lif===10###soif===212###eoif===257###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)");d = malloc(strlen(required) + 1 + sizeof *d);

				if (AKA_mark("lis===1124###sois===24360###eois===24362###lif===11###soif===264###eoif===266###ifc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)") && (AKA_mark("lis===1124###sois===24360###eois===24362###lif===11###soif===264###eoif===266###isc===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)")&&!d)) {
			AKA_mark("lis===1125###sois===24367###eois===24382###lif===12###soif===271###eoif===286###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)");errno = ENOMEM;
		}
		else {
						AKA_mark("lis===1127###sois===24395###eois===24408###lif===14###soif===299###eoif===312###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)");d->set = set;

						AKA_mark("lis===1128###sois===24412###eois===24438###lif===15###soif===316###eoif===342###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)");strcpy(d->name, required);

						AKA_mark("lis===1129###sois===24442###eois===24482###lif===16###soif===346###eoif===386###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)");rc = api_array_add(&a->require.apis, d);

		}

	}

		AKA_mark("lis===1132###sois===24491###eois===24501###lif===19###soif===395###eoif===405###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require(struct afb_apiset*,const char*,const char*)");return rc;

}

/**
 * Declare that the api of name 'apiname' requires the class of name 'classname'.
 * Returns 0 if the declaration successed or -1 in case of failure
 * (ENOMEM: allocation failure, ENOENT: api name not found)
 */
/** Instrumented function afb_apiset_require_class(struct afb_apiset*,const char*,const char*) */
int afb_apiset_require_class(struct afb_apiset *set, const char *apiname, const char *classname)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_require_class(struct afb_apiset*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===1142###sois===24822###eois===24867###lif===2###soif===100###eoif===145###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require_class(struct afb_apiset*,const char*,const char*)");struct api_desc *a = searchrec(set, apiname);

		AKA_mark("lis===1143###sois===24869###eois===24918###lif===3###soif===147###eoif===196###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require_class(struct afb_apiset*,const char*,const char*)");struct api_class *c = class_search(classname, 1);

		AKA_mark("lis===1144###sois===24920###eois===24997###lif===4###soif===198###eoif===275###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_require_class(struct afb_apiset*,const char*,const char*)");return a && c ? api_array_add(&a->require.classes, c) : (errno = ENOENT, -1);

}

/**
 * Declare that the api of name 'apiname' provides the class of name 'classname'
 * Returns 0 if the declaration successed or -1 in case of failure
 * (ENOMEM: allocation failure, ENOENT: api name not found)
 */
/** Instrumented function afb_apiset_provide_class(struct afb_apiset*,const char*,const char*) */
int afb_apiset_provide_class(struct afb_apiset *set, const char *apiname, const char *classname)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_provide_class(struct afb_apiset*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===1154###sois===25317###eois===25362###lif===2###soif===100###eoif===145###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_provide_class(struct afb_apiset*,const char*,const char*)");struct api_desc *a = searchrec(set, apiname);

		AKA_mark("lis===1155###sois===25364###eois===25413###lif===3###soif===147###eoif===196###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_provide_class(struct afb_apiset*,const char*,const char*)");struct api_class *c = class_search(classname, 1);

		AKA_mark("lis===1156###sois===25415###eois===25486###lif===4###soif===198###eoif===269###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_provide_class(struct afb_apiset*,const char*,const char*)");return a && c ? api_array_add(&c->providers, a) : (errno = ENOENT, -1);

}

/**
 * Start any API that provides the class of name 'classname'
 */
/** Instrumented function afb_apiset_class_start(const char*) */
int afb_apiset_class_start(const char *classname)
{AKA_mark("Calling: ./app-framework-binder/src/afb-apiset.c/afb_apiset_class_start(const char*)");AKA_fCall++;
		AKA_mark("lis===1164###sois===25612###eois===25663###lif===2###soif===53###eoif===104###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_class_start(const char*)");struct api_class *cla = class_search(classname, 0);

		AKA_mark("lis===1165###sois===25665###eois===25718###lif===3###soif===106###eoif===159###ins===true###function===./app-framework-binder/src/afb-apiset.c/afb_apiset_class_start(const char*)");return cla ? start_class(cla) : (errno = ENOENT, -1);

}


#endif

