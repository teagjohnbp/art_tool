/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_FDEV_SYSTEMD_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_FDEV_SYSTEMD_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>

#include <systemd/sd-event.h>

#define FDEV_PROVIDER
/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_H_
#define AKA_INCLUDE__FDEV_H_
#include "fdev.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_SYSTEMD_H_
#define AKA_INCLUDE__FDEV_SYSTEMD_H_
#include "fdev-systemd.akaignore.h"
#endif


/** Instrumented function handler(sd_event_source*,int,uint32_t,void*) */
static int handler(sd_event_source *s, int fd, uint32_t revents, void *userdata)
{AKA_mark("Calling: ./app-framework-binder/src/fdev-systemd.c/handler(sd_event_source*,int,uint32_t,void*)");AKA_fCall++;
		AKA_mark("lis===28###sois===845###eois===874###lif===2###soif===84###eoif===113###ins===true###function===./app-framework-binder/src/fdev-systemd.c/handler(sd_event_source*,int,uint32_t,void*)");struct fdev *fdev = userdata;

		AKA_mark("lis===29###sois===876###eois===905###lif===3###soif===115###eoif===144###ins===true###function===./app-framework-binder/src/fdev-systemd.c/handler(sd_event_source*,int,uint32_t,void*)");fdev_dispatch(fdev, revents);

		AKA_mark("lis===30###sois===907###eois===916###lif===4###soif===146###eoif===155###ins===true###function===./app-framework-binder/src/fdev-systemd.c/handler(sd_event_source*,int,uint32_t,void*)");return 0;

}

/** Instrumented function unref(void*) */
static void unref(void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/fdev-systemd.c/unref(void*)");AKA_fCall++;
		AKA_mark("lis===35###sois===956###eois===990###lif===2###soif===36###eoif===70###ins===true###function===./app-framework-binder/src/fdev-systemd.c/unref(void*)");sd_event_source *source = closure;

		AKA_mark("lis===36###sois===992###eois===1022###lif===3###soif===72###eoif===102###ins===true###function===./app-framework-binder/src/fdev-systemd.c/unref(void*)");sd_event_source_unref(source);

}

/** Instrumented function disable(void*,const struct fdev*) */
static void disable(void *closure, const struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/fdev-systemd.c/disable(void*,const struct fdev*)");AKA_fCall++;
		AKA_mark("lis===41###sois===1089###eois===1123###lif===2###soif===63###eoif===97###ins===true###function===./app-framework-binder/src/fdev-systemd.c/disable(void*,const struct fdev*)");sd_event_source *source = closure;

		AKA_mark("lis===42###sois===1125###eois===1175###lif===3###soif===99###eoif===149###ins===true###function===./app-framework-binder/src/fdev-systemd.c/disable(void*,const struct fdev*)");sd_event_source_set_enabled(source, SD_EVENT_OFF);

}

/** Instrumented function enable(void*,const struct fdev*) */
static void enable(void *closure, const struct fdev *fdev)
{AKA_mark("Calling: ./app-framework-binder/src/fdev-systemd.c/enable(void*,const struct fdev*)");AKA_fCall++;
		AKA_mark("lis===47###sois===1241###eois===1275###lif===2###soif===62###eoif===96###ins===true###function===./app-framework-binder/src/fdev-systemd.c/enable(void*,const struct fdev*)");sd_event_source *source = closure;

		AKA_mark("lis===48###sois===1277###eois===1334###lif===3###soif===98###eoif===155###ins===true###function===./app-framework-binder/src/fdev-systemd.c/enable(void*,const struct fdev*)");sd_event_source_set_io_events(source, fdev_events(fdev));

		AKA_mark("lis===49###sois===1336###eois===1385###lif===4###soif===157###eoif===206###ins===true###function===./app-framework-binder/src/fdev-systemd.c/enable(void*,const struct fdev*)");sd_event_source_set_enabled(source, SD_EVENT_ON);

}

static struct fdev_itf itf =
{
	.unref = unref,
	.disable = disable,
	.enable = enable,
	.update = enable
};

/** Instrumented function fdev_systemd_create(struct sd_event*,int) */
struct fdev *fdev_systemd_create(struct sd_event *eloop, int fd)
{AKA_mark("Calling: ./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)");AKA_fCall++;
		AKA_mark("lis===62###sois===1567###eois===1574###lif===2###soif===68###eoif===75###ins===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)");int rc;

		AKA_mark("lis===63###sois===1576###eois===1600###lif===3###soif===77###eoif===101###ins===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)");sd_event_source *source;

		AKA_mark("lis===64###sois===1602###eois===1620###lif===4###soif===103###eoif===121###ins===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)");struct fdev *fdev;


		AKA_mark("lis===66###sois===1623###eois===1646###lif===6###soif===124###eoif===147###ins===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)");fdev = fdev_create(fd);

		if (AKA_mark("lis===67###sois===1652###eois===1656###lif===7###soif===153###eoif===157###ifc===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)") && (AKA_mark("lis===67###sois===1652###eois===1656###lif===7###soif===153###eoif===157###isc===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)")&&fdev)) {
				AKA_mark("lis===68###sois===1662###eois===1721###lif===8###soif===163###eoif===222###ins===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)");rc = sd_event_add_io(eloop, &source, fd, 0, handler, fdev);

				if (AKA_mark("lis===69###sois===1728###eois===1734###lif===9###soif===229###eoif===235###ifc===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)") && (AKA_mark("lis===69###sois===1728###eois===1734###lif===9###soif===229###eoif===235###isc===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)")&&rc < 0)) {
						AKA_mark("lis===70###sois===1741###eois===1758###lif===10###soif===242###eoif===259###ins===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)");fdev_unref(fdev);

						AKA_mark("lis===71###sois===1762###eois===1771###lif===11###soif===263###eoif===272###ins===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)");fdev = 0;

						AKA_mark("lis===72###sois===1775###eois===1787###lif===12###soif===276###eoif===288###ins===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)");errno = -rc;

		}
		else {
						AKA_mark("lis===74###sois===1802###eois===1852###lif===14###soif===303###eoif===353###ins===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)");sd_event_source_set_enabled(source, SD_EVENT_OFF);

						AKA_mark("lis===75###sois===1856###eois===1889###lif===15###soif===357###eoif===390###ins===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)");fdev_set_itf(fdev, &itf, source);

		}

	}
	else {AKA_mark("lis===-67-###sois===-1652-###eois===-16524-###lif===-7-###soif===-###eoif===-157-###ins===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)");}

		AKA_mark("lis===78###sois===1898###eois===1910###lif===18###soif===399###eoif===411###ins===true###function===./app-framework-binder/src/fdev-systemd.c/fdev_systemd_create(struct sd_event*,int)");return fdev;

}


#endif

