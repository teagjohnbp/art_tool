/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_FDEV_H
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_FDEV_H
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <sys/epoll.h>

#ifndef FDEV_STRUCT
#define FDEV_STRUCT

struct fdev
{
	int fd;
	uint32_t events;
	unsigned refcount;
	struct fdev_itf *itf;
	void *closure_itf;
	void (*callback)(void*,uint32_t,struct fdev*);
	void *closure_callback;
};

#endif

#if defined(FDEV_PROVIDER)
struct fdev_itf
{
	void (*unref)(void *closure);
	void (*disable)(void *closure, const struct fdev *fdev);
	void (*enable)(void *closure, const struct fdev *fdev);
	void (*update)(void *closure, const struct fdev *fdev);
};

extern struct fdev *fdev_create(int fd);
extern void fdev_set_itf(struct fdev *fdev, struct fdev_itf *itf, void *closure_itf);
extern void fdev_dispatch(struct fdev *fdev, uint32_t events);
#endif

extern struct fdev *fdev_addref(struct fdev *fdev);
extern void fdev_unref(struct fdev *fdev);

extern int fdev_fd(const struct fdev *fdev);
extern uint32_t fdev_events(const struct fdev *fdev);
extern int fdev_autoclose(const struct fdev *fdev);

extern void fdev_set_callback(struct fdev *fdev, void (*callback)(void*,uint32_t,struct fdev*), void *closure);
extern void fdev_set_events(struct fdev *fdev, uint32_t events);
extern void fdev_set_autoclose(struct fdev *fdev, int autoclose);

#endif

