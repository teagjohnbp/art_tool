/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_API_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_API_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author "Fulup Ar Foll"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_H_
#define AKA_INCLUDE__AFB_API_H_
#include "afb-api.akaignore.h"
#endif


/**
 * Checks wether 'name' is a valid API name.
 * @return 1 if valid, 0 otherwise
 */
/** Instrumented function afb_api_is_valid_name(const char*) */
int afb_api_is_valid_name(const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");AKA_fCall++;
		AKA_mark("lis===34###sois===926###eois===942###lif===2###soif===47###eoif===63###ins===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");unsigned char c;


		AKA_mark("lis===36###sois===945###eois===970###lif===4###soif===66###eoif===91###ins===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");c = (unsigned char)*name;

		if (AKA_mark("lis===37###sois===976###eois===982###lif===5###soif===97###eoif===103###ifc===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)") && (AKA_mark("lis===37###sois===976###eois===982###lif===5###soif===97###eoif===103###isc===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)")&&c == 0)) {
		AKA_mark("lis===39###sois===1019###eois===1028###lif===7###soif===140###eoif===149###ins===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");return 0;
	}
	else {AKA_mark("lis===-37-###sois===-976-###eois===-9766-###lif===-5-###soif===-###eoif===-103-###ins===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");}


		do {
				if (AKA_mark("lis===42###sois===1042###eois===1067###lif===10###soif===163###eoif===188###ifc===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)") && (AKA_mark("lis===42###sois===1042###eois===1067###lif===10###soif===163###eoif===188###isc===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)")&&c < (unsigned char)'\x80')) {
						AKA_mark("lis===43###sois===1081###eois===1082###lif===11###soif===202###eoif===203###ins===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");switch(c){
							default: if(c != '"' && c != '#' && c != '%' && c != '&' && c != '\'' && c != '/' && c != '?' && c != '`' && c != '\\' && c != '\x7f')AKA_mark("lis===44###sois===1089###eois===1097###lif===12###soif===210###eoif===218###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");

								if (AKA_mark("lis===45###sois===1106###eois===1113###lif===13###soif===227###eoif===234###ifc===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)") && (AKA_mark("lis===45###sois===1106###eois===1113###lif===13###soif===227###eoif===234###isc===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)")&&c > ' ')) {
					AKA_mark("lis===46###sois===1120###eois===1126###lif===14###soif===241###eoif===247###ins===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");break;
				}
				else {AKA_mark("lis===-45-###sois===-1106-###eois===-11067-###lif===-13-###soif===-###eoif===-234-###ins===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");}

				/*@fallthrough@*/
							case '"': if(c == '"')AKA_mark("lis===48###sois===1152###eois===1161###lif===16###soif===273###eoif===282###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");

							case '#': if(c == '#')AKA_mark("lis===49###sois===1165###eois===1174###lif===17###soif===286###eoif===295###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");

							case '%': if(c == '%')AKA_mark("lis===50###sois===1178###eois===1187###lif===18###soif===299###eoif===308###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");

							case '&': if(c == '&')AKA_mark("lis===51###sois===1191###eois===1200###lif===19###soif===312###eoif===321###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");

							case '\'': if(c == '\'')AKA_mark("lis===52###sois===1204###eois===1214###lif===20###soif===325###eoif===335###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");

							case '/': if(c == '/')AKA_mark("lis===53###sois===1218###eois===1227###lif===21###soif===339###eoif===348###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");

							case '?': if(c == '?')AKA_mark("lis===54###sois===1231###eois===1240###lif===22###soif===352###eoif===361###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");

							case '`': if(c == '`')AKA_mark("lis===55###sois===1244###eois===1253###lif===23###soif===365###eoif===374###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");

							case '\\': if(c == '\\')AKA_mark("lis===56###sois===1257###eois===1267###lif===24###soif===378###eoif===388###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");

							case '\x7f': if(c == '\x7f')AKA_mark("lis===57###sois===1271###eois===1283###lif===25###soif===392###eoif===404###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");

								AKA_mark("lis===58###sois===1288###eois===1297###lif===26###soif===409###eoif===418###ins===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");return 0;

			}

		}
		else {AKA_mark("lis===-42-###sois===-1042-###eois===-104225-###lif===-10-###soif===-###eoif===-188-###ins===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");}

				AKA_mark("lis===61###sois===1309###eois===1336###lif===29###soif===430###eoif===457###ins===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");c = (unsigned char)*++name;

	}
	while (AKA_mark("lis===62###sois===1346###eois===1352###lif===30###soif===467###eoif===473###ifc===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)") && (AKA_mark("lis===62###sois===1346###eois===1352###lif===30###soif===467###eoif===473###isc===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)")&&c != 0));

		AKA_mark("lis===63###sois===1356###eois===1365###lif===31###soif===477###eoif===486###ins===true###function===./app-framework-binder/src/afb-api.c/afb_api_is_valid_name(const char*)");return 1;

}


#endif

