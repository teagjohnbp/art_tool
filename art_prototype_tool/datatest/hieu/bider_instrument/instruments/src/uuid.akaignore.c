/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_UUID_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_UUID_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <uuid/uuid.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__UUID_H_
#define AKA_INCLUDE__UUID_H_
#include "uuid.akaignore.h"
#endif


/**
 * generate a new fresh 'uuid'
 */
/** Instrumented function uuid_new_binary(uuid_binary_t) */
void uuid_new_binary(uuid_binary_t uuid)
{AKA_mark("Calling: ./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");AKA_fCall++;
#if defined(USE_UUID_GENERATE)
	uuid_generate(uuid);
#else
		AKA_mark("lis===35###sois===910###eois===929###lif===5###soif===103###eoif===122###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");struct timespec ts;

		AKA_mark("lis===36###sois===931###eois===951###lif===6###soif===124###eoif===144###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");static uint16_t pid;

		AKA_mark("lis===37###sois===953###eois===977###lif===7###soif===146###eoif===170###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");static uint16_t counter;

		AKA_mark("lis===38###sois===979###eois===1001###lif===8###soif===172###eoif===194###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");static char state[32];

		AKA_mark("lis===39###sois===1003###eois===1035###lif===9###soif===196###eoif===228###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");static struct random_data rdata;


		AKA_mark("lis===41###sois===1038###eois===1048###lif===11###soif===231###eoif===241###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");int32_t x;

		AKA_mark("lis===42###sois===1050###eois===1090###lif===12###soif===243###eoif===283###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");clock_gettime(CLOCK_MONOTONIC_RAW, &ts);

		if (AKA_mark("lis===43###sois===1096###eois===1104###lif===13###soif===289###eoif===297###ifc===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)") && (AKA_mark("lis===43###sois===1096###eois===1104###lif===13###soif===289###eoif===297###isc===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)")&&pid == 0)) {
				AKA_mark("lis===44###sois===1110###eois===1135###lif===14###soif===303###eoif===328###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");pid = (uint16_t)getpid();

				AKA_mark("lis===45###sois===1138###eois===1176###lif===15###soif===331###eoif===369###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");counter = (uint16_t)(ts.tv_nsec >> 8);

				AKA_mark("lis===46###sois===1179###eois===1198###lif===16###soif===372###eoif===391###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");rdata.state = NULL;

				AKA_mark("lis===47###sois===1201###eois===1294###lif===17###soif===394###eoif===487###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");initstate_r((((unsigned)pid) << 16) + ((unsigned)counter),
					state, sizeof state, &rdata);

	}
	else {AKA_mark("lis===-43-###sois===-1096-###eois===-10968-###lif===-13-###soif===-###eoif===-297-###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");}

		AKA_mark("lis===50###sois===1299###eois===1329###lif===20###soif===492###eoif===522###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");ts.tv_nsec ^= (long)ts.tv_sec;

		if (AKA_mark("lis===51###sois===1335###eois===1349###lif===21###soif===528###eoif===542###ifc===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)") && (AKA_mark("lis===51###sois===1335###eois===1349###lif===21###soif===528###eoif===542###isc===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)")&&++counter == 0)) {
		AKA_mark("lis===52###sois===1353###eois===1365###lif===22###soif===546###eoif===558###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");counter = 1;
	}
	else {AKA_mark("lis===-51-###sois===-1335-###eois===-133514-###lif===-21-###soif===-###eoif===-542-###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");}


		AKA_mark("lis===54###sois===1368###eois===1403###lif===24###soif===561###eoif===596###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[0] = (char)(ts.tv_nsec >> 24);

		AKA_mark("lis===55###sois===1405###eois===1440###lif===25###soif===598###eoif===633###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[1] = (char)(ts.tv_nsec >> 16);

		AKA_mark("lis===56###sois===1442###eois===1476###lif===26###soif===635###eoif===669###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[2] = (char)(ts.tv_nsec >> 8);

		AKA_mark("lis===57###sois===1478###eois===1507###lif===27###soif===671###eoif===700###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[3] = (char)(ts.tv_nsec);


		AKA_mark("lis===59###sois===1510###eois===1537###lif===29###soif===703###eoif===730###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[4] = (char)(pid >> 8);

		AKA_mark("lis===60###sois===1539###eois===1561###lif===30###soif===732###eoif===754###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[5] = (char)(pid);


		AKA_mark("lis===62###sois===1564###eois===1585###lif===32###soif===757###eoif===778###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");random_r(&rdata, &x);

		AKA_mark("lis===63###sois===1587###eois===1631###lif===33###soif===780###eoif===824###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[6] = (char)(((x >> 16) & 0x0f) | 0x40);
 /* pseudo-random version */
		AKA_mark("lis===64###sois===1661###eois===1686###lif===34###soif===854###eoif===879###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[7] = (char)(x >> 8);


		AKA_mark("lis===66###sois===1689###eois===1710###lif===36###soif===882###eoif===903###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");random_r(&rdata, &x);

		AKA_mark("lis===67###sois===1712###eois===1756###lif===37###soif===905###eoif===949###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[8] = (char)(((x >> 16) & 0x3f) | 0x80);
 /* variant RFC4122 */
		AKA_mark("lis===68###sois===1780###eois===1805###lif===38###soif===973###eoif===998###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[9] = (char)(x >> 8);


		AKA_mark("lis===70###sois===1808###eois===1829###lif===40###soif===1001###eoif===1022###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");random_r(&rdata, &x);

		AKA_mark("lis===71###sois===1831###eois===1858###lif===41###soif===1024###eoif===1051###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[10] = (char)(x >> 16);

		AKA_mark("lis===72###sois===1860###eois===1886###lif===42###soif===1053###eoif===1079###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[11] = (char)(x >> 8);


		AKA_mark("lis===74###sois===1889###eois===1910###lif===44###soif===1082###eoif===1103###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");random_r(&rdata, &x);

		AKA_mark("lis===75###sois===1912###eois===1939###lif===45###soif===1105###eoif===1132###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[12] = (char)(x >> 16);

		AKA_mark("lis===76###sois===1941###eois===1967###lif===46###soif===1134###eoif===1160###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[13] = (char)(x >> 8);


		AKA_mark("lis===78###sois===1970###eois===2002###lif===48###soif===1163###eoif===1195###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[14] = (char)(counter >> 8);

		AKA_mark("lis===79###sois===2004###eois===2031###lif===49###soif===1197###eoif===1224###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_binary(uuid_binary_t)");uuid[15] = (char)(counter);

#endif
}

/** Instrumented function uuid_new_stringz(uuid_stringz_t) */
void uuid_new_stringz(uuid_stringz_t uuid)
{AKA_mark("Calling: ./app-framework-binder/src/uuid.c/uuid_new_stringz(uuid_stringz_t)");AKA_fCall++;
		AKA_mark("lis===85###sois===2088###eois===2103###lif===2###soif===46###eoif===61###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_stringz(uuid_stringz_t)");uuid_t newuuid;

		AKA_mark("lis===86###sois===2105###eois===2130###lif===3###soif===63###eoif===88###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_stringz(uuid_stringz_t)");uuid_new_binary(newuuid);

		AKA_mark("lis===87###sois===2132###eois===2166###lif===4###soif===90###eoif===124###ins===true###function===./app-framework-binder/src/uuid.c/uuid_new_stringz(uuid_stringz_t)");uuid_unparse_lower(newuuid, uuid);

}

#endif

