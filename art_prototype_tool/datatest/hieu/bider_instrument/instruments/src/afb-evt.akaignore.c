/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_EVT_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_EVT_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <pthread.h>

#include <json-c/json.h>
#include <afb/afb-event-x2-itf.h>
#include <afb/afb-event-x1.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_EVT_H_
#define AKA_INCLUDE__AFB_EVT_H_
#include "afb-evt.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_HOOK_H_
#define AKA_INCLUDE__AFB_HOOK_H_
#include "afb-hook.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__JOBS_H_
#define AKA_INCLUDE__JOBS_H_
#include "jobs.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__UUID_H_
#define AKA_INCLUDE__UUID_H_
#include "uuid.akaignore.h"
#endif


struct afb_evt_watch;

/*
 * Structure for event listeners
 */
struct afb_evt_listener {

	/* chaining listeners */
	struct afb_evt_listener *next;

	/* interface for callbacks */
	const struct afb_evt_itf *itf;

	/* closure for the callback */
	void *closure;

	/* head of the list of events listened */
	struct afb_evt_watch *watchs;

	/* rwlock of the listener */
	pthread_rwlock_t rwlock;

	/* count of reference to the listener */
	uint16_t refcount;
};

/*
 * Structure for describing events
 */
struct afb_evtid {

	/* interface */
	struct afb_event_x2 eventid;

	/* next event */
	struct afb_evtid *next;

	/* head of the list of listeners watching the event */
	struct afb_evt_watch *watchs;

	/* rwlock of the event */
	pthread_rwlock_t rwlock;

#if WITH_AFB_HOOK
	/* hooking */
	int hookflags;
#endif

	/* refcount */
	uint16_t refcount;

	/* id of the event */
	uint16_t id;

	/* fullname of the event */
	char fullname[];
};

/*
 * Structure for associating events and listeners
 */
struct afb_evt_watch {

	/* the evtid */
	struct afb_evtid *evtid;

	/* link to the next watcher for the same evtid */
	struct afb_evt_watch *next_by_evtid;

	/* the listener */
	struct afb_evt_listener *listener;

	/* link to the next watcher for the same listener */
	struct afb_evt_watch *next_by_listener;
};

/*
 * structure for job of broadcasting events
 */
struct job_broadcast
{
	/** object atached to the event */
	struct json_object *object;

	/** the uuid of the event */
	uuid_binary_t  uuid;

	/** remaining hop */
	uint8_t hop;

	/** name of the event to broadcast */
	char event[];
};

/*
 * structure for job of broadcasting or pushing events
 */
struct job_evtid
{
	/** the event to broadcast */
	struct afb_evtid *evtid;

	/** object atached to the event */
	struct json_object *object;
};

/* the interface for events */
static struct afb_event_x2_itf afb_evt_event_x2_itf = {
	.broadcast = (void*)afb_evt_evtid_broadcast,
	.push = (void*)afb_evt_evtid_push,
	.unref = (void*)afb_evt_evtid_unref,
	.name = (void*)afb_evt_evtid_name,
	.addref = (void*)afb_evt_evtid_addref
};

#if WITH_AFB_HOOK
/* the interface for events */
static struct afb_event_x2_itf afb_evt_hooked_event_x2_itf = {
	.broadcast = (void*)afb_evt_evtid_hooked_broadcast,
	.push = (void*)afb_evt_evtid_hooked_push,
	.unref = (void*)afb_evt_evtid_hooked_unref,
	.name = (void*)afb_evt_evtid_hooked_name,
	.addref = (void*)afb_evt_evtid_hooked_addref
};
#endif

/* job groups for events push/broadcast */
#define BROADCAST_JOB_GROUP  (&afb_evt_event_x2_itf)
#define PUSH_JOB_GROUP       (&afb_evt_event_x2_itf)

/* head of the list of listeners */
static pthread_rwlock_t listeners_rwlock = PTHREAD_RWLOCK_INITIALIZER;
static struct afb_evt_listener *listeners = NULL;

/* handling id of events */
static pthread_rwlock_t events_rwlock = PTHREAD_RWLOCK_INITIALIZER;
static struct afb_evtid *evtids = NULL;
static uint16_t event_genid = 0;
static uint16_t event_count = 0;

/* head of uniqueness of events */
#if !defined(EVENT_BROADCAST_HOP_MAX)
#  define EVENT_BROADCAST_HOP_MAX  10
#endif
#if !defined(EVENT_BROADCAST_MEMORY_COUNT)
#  define EVENT_BROADCAST_MEMORY_COUNT  8
#endif

#if EVENT_BROADCAST_MEMORY_COUNT
static struct {
	pthread_mutex_t mutex;
	uint8_t base;
	uint8_t count;
	uuid_binary_t uuids[EVENT_BROADCAST_MEMORY_COUNT];
} uniqueness = {
	.mutex = PTHREAD_MUTEX_INITIALIZER,
	.base = 0,
	.count = 0
};
#endif

/*
 * Create structure for job of broadcasting string 'event' with 'object'
 * Returns the created structure or NULL if out of memory
 */
/** Instrumented function make_job_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t) */
static struct job_broadcast *make_job_broadcast(const char *event, struct json_object *object, const uuid_binary_t uuid, uint8_t hop)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/make_job_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");AKA_fCall++;
		AKA_mark("lis===203###sois===4643###eois===4673###lif===2###soif===137###eoif===167###ins===true###function===./app-framework-binder/src/afb-evt.c/make_job_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");size_t sz = 1 + strlen(event);

		AKA_mark("lis===204###sois===4675###eois===4726###lif===3###soif===169###eoif===220###ins===true###function===./app-framework-binder/src/afb-evt.c/make_job_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");struct job_broadcast *jb = malloc(sz + sizeof *jb);

		if (AKA_mark("lis===205###sois===4732###eois===4734###lif===4###soif===226###eoif===228###ifc===true###function===./app-framework-binder/src/afb-evt.c/make_job_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)") && (AKA_mark("lis===205###sois===4732###eois===4734###lif===4###soif===226###eoif===228###isc===true###function===./app-framework-binder/src/afb-evt.c/make_job_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)")&&jb)) {
				AKA_mark("lis===206###sois===4740###eois===4760###lif===5###soif===234###eoif===254###ins===true###function===./app-framework-binder/src/afb-evt.c/make_job_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");jb->object = object;

				AKA_mark("lis===207###sois===4763###eois===4803###lif===6###soif===257###eoif===297###ins===true###function===./app-framework-binder/src/afb-evt.c/make_job_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");memcpy(jb->uuid, uuid, sizeof jb->uuid);

				AKA_mark("lis===208###sois===4806###eois===4820###lif===7###soif===300###eoif===314###ins===true###function===./app-framework-binder/src/afb-evt.c/make_job_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");jb->hop = hop;

				AKA_mark("lis===209###sois===4823###eois===4852###lif===8###soif===317###eoif===346###ins===true###function===./app-framework-binder/src/afb-evt.c/make_job_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");memcpy(jb->event, event, sz);

	}
	else {AKA_mark("lis===-205-###sois===-4732-###eois===-47322-###lif===-4-###soif===-###eoif===-228-###ins===true###function===./app-framework-binder/src/afb-evt.c/make_job_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");}

		AKA_mark("lis===211###sois===4857###eois===4867###lif===10###soif===351###eoif===361###ins===true###function===./app-framework-binder/src/afb-evt.c/make_job_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");return jb;

}

/*
 * Destroy structure 'jb' for job of broadcasting string events
 */
/** Instrumented function destroy_job_broadcast(struct job_broadcast*) */
static void destroy_job_broadcast(struct job_broadcast *jb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/destroy_job_broadcast(struct job_broadcast*)");AKA_fCall++;
		AKA_mark("lis===219###sois===5005###eois===5033###lif===2###soif===63###eoif===91###ins===true###function===./app-framework-binder/src/afb-evt.c/destroy_job_broadcast(struct job_broadcast*)");json_object_put(jb->object);

		AKA_mark("lis===220###sois===5035###eois===5044###lif===3###soif===93###eoif===102###ins===true###function===./app-framework-binder/src/afb-evt.c/destroy_job_broadcast(struct job_broadcast*)");free(jb);

}

/*
 * Create structure for job of broadcasting or pushing 'evtid' with 'object'
 * Returns the created structure or NULL if out of memory
 */
/** Instrumented function make_job_evtid(struct afb_evtid*,struct json_object*) */
static struct job_evtid *make_job_evtid(struct afb_evtid *evtid, struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/make_job_evtid(struct afb_evtid*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===229###sois===5286###eois===5328###lif===2###soif===96###eoif===138###ins===true###function===./app-framework-binder/src/afb-evt.c/make_job_evtid(struct afb_evtid*,struct json_object*)");struct job_evtid *je = malloc(sizeof *je);

		if (AKA_mark("lis===230###sois===5334###eois===5336###lif===3###soif===144###eoif===146###ifc===true###function===./app-framework-binder/src/afb-evt.c/make_job_evtid(struct afb_evtid*,struct json_object*)") && (AKA_mark("lis===230###sois===5334###eois===5336###lif===3###soif===144###eoif===146###isc===true###function===./app-framework-binder/src/afb-evt.c/make_job_evtid(struct afb_evtid*,struct json_object*)")&&je)) {
				AKA_mark("lis===231###sois===5342###eois===5382###lif===4###soif===152###eoif===192###ins===true###function===./app-framework-binder/src/afb-evt.c/make_job_evtid(struct afb_evtid*,struct json_object*)");je->evtid = afb_evt_evtid_addref(evtid);

				AKA_mark("lis===232###sois===5385###eois===5405###lif===5###soif===195###eoif===215###ins===true###function===./app-framework-binder/src/afb-evt.c/make_job_evtid(struct afb_evtid*,struct json_object*)");je->object = object;

	}
	else {AKA_mark("lis===-230-###sois===-5334-###eois===-53342-###lif===-3-###soif===-###eoif===-146-###ins===true###function===./app-framework-binder/src/afb-evt.c/make_job_evtid(struct afb_evtid*,struct json_object*)");}

		AKA_mark("lis===234###sois===5410###eois===5420###lif===7###soif===220###eoif===230###ins===true###function===./app-framework-binder/src/afb-evt.c/make_job_evtid(struct afb_evtid*,struct json_object*)");return je;

}

/*
 * Destroy structure for job of broadcasting or pushing evtid
 */
/** Instrumented function destroy_job_evtid(struct job_evtid*) */
static void destroy_job_evtid(struct job_evtid *je)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/destroy_job_evtid(struct job_evtid*)");AKA_fCall++;
		AKA_mark("lis===242###sois===5548###eois===5579###lif===2###soif===55###eoif===86###ins===true###function===./app-framework-binder/src/afb-evt.c/destroy_job_evtid(struct job_evtid*)");afb_evt_evtid_unref(je->evtid);

		AKA_mark("lis===243###sois===5581###eois===5609###lif===3###soif===88###eoif===116###ins===true###function===./app-framework-binder/src/afb-evt.c/destroy_job_evtid(struct job_evtid*)");json_object_put(je->object);

		AKA_mark("lis===244###sois===5611###eois===5620###lif===4###soif===118###eoif===127###ins===true###function===./app-framework-binder/src/afb-evt.c/destroy_job_evtid(struct job_evtid*)");free(je);

}

/*
 * Broadcasts the 'event' of 'id' with its 'object'
 */
/** Instrumented function broadcast(struct job_broadcast*) */
static void broadcast(struct job_broadcast *jb)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/broadcast(struct job_broadcast*)");AKA_fCall++;
		AKA_mark("lis===252###sois===5734###eois===5768###lif===2###soif===51###eoif===85###ins===true###function===./app-framework-binder/src/afb-evt.c/broadcast(struct job_broadcast*)");struct afb_evt_listener *listener;


		AKA_mark("lis===254###sois===5771###eois===5812###lif===4###soif===88###eoif===129###ins===true###function===./app-framework-binder/src/afb-evt.c/broadcast(struct job_broadcast*)");pthread_rwlock_rdlock(&listeners_rwlock);

		AKA_mark("lis===255###sois===5814###eois===5835###lif===5###soif===131###eoif===152###ins===true###function===./app-framework-binder/src/afb-evt.c/broadcast(struct job_broadcast*)");listener = listeners;

		while (AKA_mark("lis===256###sois===5843###eois===5851###lif===6###soif===160###eoif===168###ifc===true###function===./app-framework-binder/src/afb-evt.c/broadcast(struct job_broadcast*)") && (AKA_mark("lis===256###sois===5843###eois===5851###lif===6###soif===160###eoif===168###isc===true###function===./app-framework-binder/src/afb-evt.c/broadcast(struct job_broadcast*)")&&listener)) {
				if (AKA_mark("lis===257###sois===5861###eois===5893###lif===7###soif===178###eoif===210###ifc===true###function===./app-framework-binder/src/afb-evt.c/broadcast(struct job_broadcast*)") && (AKA_mark("lis===257###sois===5861###eois===5893###lif===7###soif===178###eoif===210###isc===true###function===./app-framework-binder/src/afb-evt.c/broadcast(struct job_broadcast*)")&&listener->itf->broadcast != NULL)) {
			AKA_mark("lis===258###sois===5898###eois===6001###lif===8###soif===215###eoif===318###ins===true###function===./app-framework-binder/src/afb-evt.c/broadcast(struct job_broadcast*)");listener->itf->broadcast(listener->closure, jb->event, json_object_get(jb->object), jb->uuid, jb->hop);
		}
		else {AKA_mark("lis===-257-###sois===-5861-###eois===-586132-###lif===-7-###soif===-###eoif===-210-###ins===true###function===./app-framework-binder/src/afb-evt.c/broadcast(struct job_broadcast*)");}

				AKA_mark("lis===259###sois===6004###eois===6030###lif===9###soif===321###eoif===347###ins===true###function===./app-framework-binder/src/afb-evt.c/broadcast(struct job_broadcast*)");listener = listener->next;

	}

		AKA_mark("lis===261###sois===6035###eois===6076###lif===11###soif===352###eoif===393###ins===true###function===./app-framework-binder/src/afb-evt.c/broadcast(struct job_broadcast*)");pthread_rwlock_unlock(&listeners_rwlock);

}

/*
 * Jobs callback for broadcasting string asynchronously
 */
/** Instrumented function broadcast_job(int,void*) */
static void broadcast_job(int signum, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/broadcast_job(int,void*)");AKA_fCall++;
		AKA_mark("lis===269###sois===6199###eois===6234###lif===2###soif===56###eoif===91###ins===true###function===./app-framework-binder/src/afb-evt.c/broadcast_job(int,void*)");struct job_broadcast *jb = closure;


		if (AKA_mark("lis===271###sois===6241###eois===6252###lif===4###soif===98###eoif===109###ifc===true###function===./app-framework-binder/src/afb-evt.c/broadcast_job(int,void*)") && (AKA_mark("lis===271###sois===6241###eois===6252###lif===4###soif===98###eoif===109###isc===true###function===./app-framework-binder/src/afb-evt.c/broadcast_job(int,void*)")&&signum == 0)) {
		AKA_mark("lis===272###sois===6256###eois===6270###lif===5###soif===113###eoif===127###ins===true###function===./app-framework-binder/src/afb-evt.c/broadcast_job(int,void*)");broadcast(jb);
	}
	else {AKA_mark("lis===-271-###sois===-6241-###eois===-624111-###lif===-4-###soif===-###eoif===-109-###ins===true###function===./app-framework-binder/src/afb-evt.c/broadcast_job(int,void*)");}

		AKA_mark("lis===273###sois===6272###eois===6298###lif===6###soif===129###eoif===155###ins===true###function===./app-framework-binder/src/afb-evt.c/broadcast_job(int,void*)");destroy_job_broadcast(jb);

}

/*
 * Broadcasts the string 'event' with its 'object'
 */
/** Instrumented function unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t) */
static int unhooked_broadcast(const char *event, struct json_object *object, const uuid_binary_t uuid, uint8_t hop)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");AKA_fCall++;
		AKA_mark("lis===281###sois===6479###eois===6504###lif===2###soif===119###eoif===144###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");uuid_binary_t local_uuid;

		AKA_mark("lis===282###sois===6506###eois===6531###lif===3###soif===146###eoif===171###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");struct job_broadcast *jb;

		AKA_mark("lis===283###sois===6533###eois===6540###lif===4###soif===173###eoif===180###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");int rc;

#if EVENT_BROADCAST_MEMORY_COUNT
		AKA_mark("lis===285###sois===6575###eois===6591###lif===6###soif===215###eoif===231###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");int iter, count;

#endif

	/* check if lately sent */
		if (AKA_mark("lis===289###sois===6633###eois===6638###lif===10###soif===273###eoif===278###ifc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)") && (AKA_mark("lis===289###sois===6633###eois===6638###lif===10###soif===273###eoif===278###isc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)")&&!uuid)) {
				AKA_mark("lis===290###sois===6644###eois===6672###lif===11###soif===284###eoif===312###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");uuid_new_binary(local_uuid);

				AKA_mark("lis===291###sois===6675###eois===6693###lif===12###soif===315###eoif===333###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");uuid = local_uuid;

				AKA_mark("lis===292###sois===6696###eois===6726###lif===13###soif===336###eoif===366###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");hop = EVENT_BROADCAST_HOP_MAX;

#if EVENT_BROADCAST_MEMORY_COUNT
				AKA_mark("lis===294###sois===6762###eois===6800###lif===15###soif===402###eoif===440###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");pthread_mutex_lock(&uniqueness.mutex);

	}
	else {
				AKA_mark("lis===296###sois===6813###eois===6851###lif===17###soif===453###eoif===491###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");pthread_mutex_lock(&uniqueness.mutex);

				AKA_mark("lis===297###sois===6854###eois===6882###lif===18###soif===494###eoif===522###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");	AKA_mark("lis===309###sois===7154###eois===7182###lif===30###soif===794###eoif===822###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");iter = (int)uniqueness.base;


				AKA_mark("lis===298###sois===6885###eois===6915###lif===19###soif===525###eoif===555###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");count = (int)uniqueness.count;

				while (AKA_mark("lis===299###sois===6925###eois===6930###lif===20###soif===565###eoif===570###ifc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)") && (AKA_mark("lis===299###sois===6925###eois===6930###lif===20###soif===565###eoif===570###isc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)")&&count)) {
						if (AKA_mark("lis===300###sois===6941###eois===7005###lif===21###soif===581###eoif===645###ifc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)") && (AKA_mark("lis===300###sois===6941###eois===7005###lif===21###soif===581###eoif===645###isc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)")&&0 == memcmp(uuid, uniqueness.uuids[iter], sizeof(uuid_binary_t)))) {
								AKA_mark("lis===301###sois===7013###eois===7053###lif===22###soif===653###eoif===693###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");pthread_mutex_unlock(&uniqueness.mutex);

								AKA_mark("lis===302###sois===7058###eois===7067###lif===23###soif===698###eoif===707###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");return 0;

			}
			else {AKA_mark("lis===-300-###sois===-6941-###eois===-694164-###lif===-21-###soif===-###eoif===-645-###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");}

						if (AKA_mark("lis===304###sois===7080###eois===7118###lif===25###soif===720###eoif===758###ifc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)") && (AKA_mark("lis===304###sois===7080###eois===7118###lif===25###soif===720###eoif===758###isc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)")&&++iter == EVENT_BROADCAST_MEMORY_COUNT)) {
				AKA_mark("lis===305###sois===7124###eois===7133###lif===26###soif===764###eoif===773###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");iter = 0;
			}
			else {AKA_mark("lis===-304-###sois===-7080-###eois===-708038-###lif===-25-###soif===-###eoif===-758-###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");}

						AKA_mark("lis===306###sois===7137###eois===7145###lif===27###soif===777###eoif===785###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");count--;

		}

	}

	iter = (int)uniqueness.base;
		if (AKA_mark("lis===310###sois===7188###eois===7235###lif===31###soif===828###eoif===875###ifc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)") && (AKA_mark("lis===310###sois===7188###eois===7235###lif===31###soif===828###eoif===875###isc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)")&&uniqueness.count < EVENT_BROADCAST_MEMORY_COUNT)) {
		AKA_mark("lis===311###sois===7239###eois===7273###lif===32###soif===879###eoif===913###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");iter += (int)(uniqueness.count++);
	}
	else {
		if (AKA_mark("lis===312###sois===7284###eois===7333###lif===33###soif===924###eoif===973###ifc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)") && (AKA_mark("lis===312###sois===7284###eois===7333###lif===33###soif===924###eoif===973###isc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)")&&++uniqueness.base == EVENT_BROADCAST_MEMORY_COUNT)) {
			AKA_mark("lis===313###sois===7337###eois===7357###lif===34###soif===977###eoif===997###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");uniqueness.base = 0;
		}
		else {AKA_mark("lis===-312-###sois===-7284-###eois===-728449-###lif===-33-###soif===-###eoif===-973-###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");}
	}

		AKA_mark("lis===314###sois===7359###eois===7419###lif===35###soif===999###eoif===1059###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");memcpy(uniqueness.uuids[iter], uuid, sizeof(uuid_binary_t));

		AKA_mark("lis===315###sois===7421###eois===7461###lif===36###soif===1061###eoif===1101###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");pthread_mutex_unlock(&uniqueness.mutex);

#else
	}
#endif

	/* create the structure for the job */
		AKA_mark("lis===321###sois===7520###eois===7570###lif===42###soif===1160###eoif===1210###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");jb = make_job_broadcast(event, object, uuid, hop);

		if (AKA_mark("lis===322###sois===7576###eois===7586###lif===43###soif===1216###eoif===1226###ifc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)") && (AKA_mark("lis===322###sois===7576###eois===7586###lif===43###soif===1216###eoif===1226###isc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)")&&jb == NULL)) {
				AKA_mark("lis===323###sois===7592###eois===7698###lif===44###soif===1232###eoif===1338###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");ERROR("Cant't create broadcast string job item for %s(%s)",
			event, json_object_to_json_string(object));

				AKA_mark("lis===325###sois===7701###eois===7725###lif===46###soif===1341###eoif===1365###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");json_object_put(object);

				AKA_mark("lis===326###sois===7728###eois===7738###lif===47###soif===1368###eoif===1378###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");return -1;

	}
	else {AKA_mark("lis===-322-###sois===-7576-###eois===-757610-###lif===-43-###soif===-###eoif===-1226-###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");}


	/* queue the job */
		AKA_mark("lis===330###sois===7765###eois===7824###lif===51###soif===1405###eoif===1464###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");rc = jobs_queue(BROADCAST_JOB_GROUP, 0, broadcast_job, jb);

		if (AKA_mark("lis===331###sois===7830###eois===7832###lif===52###soif===1470###eoif===1472###ifc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)") && (AKA_mark("lis===331###sois===7830###eois===7832###lif===52###soif===1470###eoif===1472###isc===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)")&&rc)) {
				AKA_mark("lis===332###sois===7838###eois===7943###lif===53###soif===1478###eoif===1583###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");ERROR("cant't queue broadcast string job item for %s(%s)",
			event, json_object_to_json_string(object));

				AKA_mark("lis===334###sois===7946###eois===7972###lif===55###soif===1586###eoif===1612###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");destroy_job_broadcast(jb);

	}
	else {AKA_mark("lis===-331-###sois===-7830-###eois===-78302-###lif===-52-###soif===-###eoif===-1472-###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");}

		AKA_mark("lis===336###sois===7977###eois===7987###lif===57###soif===1617###eoif===1627###ins===true###function===./app-framework-binder/src/afb-evt.c/unhooked_broadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");return rc;

}

/*
 * Broadcasts the event 'evtid' with its 'object'
 * 'object' is released (like json_object_put)
 * Returns the count of listener that received the event.
 */
/** Instrumented function afb_evt_evtid_broadcast(struct afb_evtid*,struct json_object*) */
int afb_evt_evtid_broadcast(struct afb_evtid *evtid, struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_evtid_broadcast(struct afb_evtid*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===346###sois===8237###eois===8297###lif===2###soif===84###eoif===144###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_broadcast(struct afb_evtid*,struct json_object*)");return unhooked_broadcast(evtid->fullname, object, NULL, 0);

}

#if WITH_AFB_HOOK
/*
 * Broadcasts the event 'evtid' with its 'object'
 * 'object' is released (like json_object_put)
 * Returns the count of listener that received the event.
 */
int afb_evt_evtid_hooked_broadcast(struct afb_evtid *evtid, struct json_object *object)
{
	int result;

	json_object_get(object);

	if (evtid->hookflags & afb_hook_flag_evt_broadcast_before)
		afb_hook_evt_broadcast_before(evtid->fullname, evtid->id, object);

	result = afb_evt_evtid_broadcast(evtid, object);

	if (evtid->hookflags & afb_hook_flag_evt_broadcast_after)
		afb_hook_evt_broadcast_after(evtid->fullname, evtid->id, object, result);

	json_object_put(object);

	return result;
}
#endif

/** Instrumented function afb_evt_rebroadcast(const char*,struct json_object*,uuid_binary_t,uint8_t) */
int afb_evt_rebroadcast(const char *event, struct json_object *object, const uuid_binary_t uuid, uint8_t hop)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_rebroadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");AKA_fCall++;
		AKA_mark("lis===377###sois===9095###eois===9106###lif===2###soif===113###eoif===124###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_rebroadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");int result;


#if WITH_AFB_HOOK
	json_object_get(object);
	afb_hook_evt_broadcast_before(event, 0, object);
#endif

		AKA_mark("lis===384###sois===9211###eois===9265###lif===9###soif===229###eoif===283###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_rebroadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");result = unhooked_broadcast(event, object, uuid, hop);


#if WITH_AFB_HOOK
	afb_hook_evt_broadcast_after(event, 0, object, result);
	json_object_put(object);
#endif
		AKA_mark("lis===390###sois===9376###eois===9390###lif===15###soif===394###eoif===408###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_rebroadcast(const char*,struct json_object*,uuid_binary_t,uint8_t)");return result;

}

/*
 * Broadcasts the 'event' with its 'object'
 * 'object' is released (like json_object_put)
 * Returns the count of listener having receive the event.
 */
/** Instrumented function afb_evt_broadcast(const char*,struct json_object*) */
int afb_evt_broadcast(const char *event, struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_broadcast(const char*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===400###sois===9623###eois===9674###lif===2###soif===72###eoif===123###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_broadcast(const char*,struct json_object*)");return afb_evt_rebroadcast(event, object, NULL, 0);

}

/*
 * Pushes the event 'evtid' with 'obj' to its listeners
 * Returns the count of listener that received the event.
 */
/** Instrumented function push_evtid(struct afb_evtid*,struct json_object*) */
static void push_evtid(struct afb_evtid *evtid, struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/push_evtid(struct afb_evtid*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===409###sois===9878###eois===9906###lif===2###soif===79###eoif===107###ins===true###function===./app-framework-binder/src/afb-evt.c/push_evtid(struct afb_evtid*,struct json_object*)");struct afb_evt_watch *watch;

		AKA_mark("lis===410###sois===9908###eois===9942###lif===3###soif===109###eoif===143###ins===true###function===./app-framework-binder/src/afb-evt.c/push_evtid(struct afb_evtid*,struct json_object*)");struct afb_evt_listener *listener;


		AKA_mark("lis===412###sois===9945###eois===9983###lif===5###soif===146###eoif===184###ins===true###function===./app-framework-binder/src/afb-evt.c/push_evtid(struct afb_evtid*,struct json_object*)");pthread_rwlock_rdlock(&evtid->rwlock);

		AKA_mark("lis===413###sois===9985###eois===10007###lif===6###soif===186###eoif===208###ins===true###function===./app-framework-binder/src/afb-evt.c/push_evtid(struct afb_evtid*,struct json_object*)");watch = evtid->watchs;

		while (AKA_mark("lis===414###sois===10015###eois===10020###lif===7###soif===216###eoif===221###ifc===true###function===./app-framework-binder/src/afb-evt.c/push_evtid(struct afb_evtid*,struct json_object*)") && (AKA_mark("lis===414###sois===10015###eois===10020###lif===7###soif===216###eoif===221###isc===true###function===./app-framework-binder/src/afb-evt.c/push_evtid(struct afb_evtid*,struct json_object*)")&&watch)) {
				AKA_mark("lis===415###sois===10026###eois===10053###lif===8###soif===227###eoif===254###ins===true###function===./app-framework-binder/src/afb-evt.c/push_evtid(struct afb_evtid*,struct json_object*)");listener = watch->listener;

				AKA_mark("lis===416###sois===10056###eois===10092###lif===9###soif===257###eoif===293###ins===true###function===./app-framework-binder/src/afb-evt.c/push_evtid(struct afb_evtid*,struct json_object*)");assert(listener->itf->push != NULL);

				AKA_mark("lis===417###sois===10095###eois===10187###lif===10###soif===296###eoif===388###ins===true###function===./app-framework-binder/src/afb-evt.c/push_evtid(struct afb_evtid*,struct json_object*)");listener->itf->push(listener->closure, evtid->fullname, evtid->id, json_object_get(object));

				AKA_mark("lis===418###sois===10190###eois===10219###lif===11###soif===391###eoif===420###ins===true###function===./app-framework-binder/src/afb-evt.c/push_evtid(struct afb_evtid*,struct json_object*)");watch = watch->next_by_evtid;

	}

		AKA_mark("lis===420###sois===10224###eois===10262###lif===13###soif===425###eoif===463###ins===true###function===./app-framework-binder/src/afb-evt.c/push_evtid(struct afb_evtid*,struct json_object*)");pthread_rwlock_unlock(&evtid->rwlock);

}

/*
 * Jobs callback for pushing evtid asynchronously
 */
/** Instrumented function push_job_evtid(int,void*) */
static void push_job_evtid(int signum, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/push_job_evtid(int,void*)");AKA_fCall++;
		AKA_mark("lis===428###sois===10380###eois===10411###lif===2###soif===57###eoif===88###ins===true###function===./app-framework-binder/src/afb-evt.c/push_job_evtid(int,void*)");struct job_evtid *je = closure;


		if (AKA_mark("lis===430###sois===10418###eois===10429###lif===4###soif===95###eoif===106###ifc===true###function===./app-framework-binder/src/afb-evt.c/push_job_evtid(int,void*)") && (AKA_mark("lis===430###sois===10418###eois===10429###lif===4###soif===95###eoif===106###isc===true###function===./app-framework-binder/src/afb-evt.c/push_job_evtid(int,void*)")&&signum == 0)) {
		AKA_mark("lis===431###sois===10433###eois===10467###lif===5###soif===110###eoif===144###ins===true###function===./app-framework-binder/src/afb-evt.c/push_job_evtid(int,void*)");push_evtid(je->evtid, je->object);
	}
	else {AKA_mark("lis===-430-###sois===-10418-###eois===-1041811-###lif===-4-###soif===-###eoif===-106-###ins===true###function===./app-framework-binder/src/afb-evt.c/push_job_evtid(int,void*)");}

		AKA_mark("lis===432###sois===10469###eois===10491###lif===6###soif===146###eoif===168###ins===true###function===./app-framework-binder/src/afb-evt.c/push_job_evtid(int,void*)");destroy_job_evtid(je);

}

/*
 * Pushes the event 'evtid' with 'obj' to its listeners
 * 'obj' is released (like json_object_put)
 * Returns 1 if at least one listener exists or 0 if no listener exists or
 * -1 in case of error and the event can't be delivered
 */
/** Instrumented function afb_evt_evtid_push(struct afb_evtid*,struct json_object*) */
int afb_evt_evtid_push(struct afb_evtid *evtid, struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===443###sois===10812###eois===10833###lif===2###soif===79###eoif===100###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");struct job_evtid *je;

		AKA_mark("lis===444###sois===10835###eois===10842###lif===3###soif===102###eoif===109###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");int rc;


		if (AKA_mark("lis===446###sois===10849###eois===10863###lif===5###soif===116###eoif===130###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)") && (AKA_mark("lis===446###sois===10849###eois===10863###lif===5###soif===116###eoif===130###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)")&&!evtid->watchs)) {
		AKA_mark("lis===447###sois===10867###eois===10876###lif===6###soif===134###eoif===143###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");return 0;
	}
	else {AKA_mark("lis===-446-###sois===-10849-###eois===-1084914-###lif===-5-###soif===-###eoif===-130-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");}


		AKA_mark("lis===449###sois===10879###eois===10914###lif===8###soif===146###eoif===181###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");je = make_job_evtid(evtid, object);

		if (AKA_mark("lis===450###sois===10920###eois===10930###lif===9###soif===187###eoif===197###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)") && (AKA_mark("lis===450###sois===10920###eois===10930###lif===9###soif===187###eoif===197###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)")&&je == NULL)) {
				AKA_mark("lis===451###sois===10936###eois===11046###lif===10###soif===203###eoif===313###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");ERROR("Cant't create push evtid job item for %s(%s)",
			evtid->fullname, json_object_to_json_string(object));

				AKA_mark("lis===453###sois===11049###eois===11073###lif===12###soif===316###eoif===340###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");json_object_put(object);

				AKA_mark("lis===454###sois===11076###eois===11086###lif===13###soif===343###eoif===353###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");return -1;

	}
	else {AKA_mark("lis===-450-###sois===-10920-###eois===-1092010-###lif===-9-###soif===-###eoif===-197-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");}


		AKA_mark("lis===457###sois===11092###eois===11147###lif===16###soif===359###eoif===414###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");rc = jobs_queue(PUSH_JOB_GROUP, 0, push_job_evtid, je);

		if (AKA_mark("lis===458###sois===11153###eois===11160###lif===17###soif===420###eoif===427###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)") && (AKA_mark("lis===458###sois===11153###eois===11160###lif===17###soif===420###eoif===427###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)")&&rc == 0)) {
		AKA_mark("lis===459###sois===11164###eois===11171###lif===18###soif===431###eoif===438###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");rc = 1;
	}
	else {
				AKA_mark("lis===461###sois===11182###eois===11291###lif===20###soif===449###eoif===558###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");ERROR("cant't queue push evtid job item for %s(%s)",
			evtid->fullname, json_object_to_json_string(object));

				AKA_mark("lis===463###sois===11294###eois===11316###lif===22###soif===561###eoif===583###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");destroy_job_evtid(je);

	}


		AKA_mark("lis===466###sois===11322###eois===11332###lif===25###soif===589###eoif===599###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_push(struct afb_evtid*,struct json_object*)");return rc;

}

#if WITH_AFB_HOOK
/*
 * Pushes the event 'evtid' with 'obj' to its listeners
 * 'obj' is released (like json_object_put)
 * Emits calls to hooks.
 * Returns the count of listener taht received the event.
 */
int afb_evt_evtid_hooked_push(struct afb_evtid *evtid, struct json_object *obj)
{

	int result;

	/* lease the object */
	json_object_get(obj);

	/* hook before push */
	if (evtid->hookflags & afb_hook_flag_evt_push_before)
		afb_hook_evt_push_before(evtid->fullname, evtid->id, obj);

	/* push */
	result = afb_evt_evtid_push(evtid, obj);

	/* hook after push */
	if (evtid->hookflags & afb_hook_flag_evt_push_after)
		afb_hook_evt_push_after(evtid->fullname, evtid->id, obj, result);

	/* release the object */
	json_object_put(obj);
	return result;
}
#endif

/** Instrumented function unwatch(struct afb_evt_listener*,struct afb_evtid*,int) */
static void unwatch(struct afb_evt_listener *listener, struct afb_evtid *evtid, int remove)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/unwatch(struct afb_evt_listener*,struct afb_evtid*,int)");AKA_fCall++;
	/* notify listener if needed */
		if (AKA_mark("lis===504###sois===12238###eois===12277###lif===3###soif===132###eoif===171###ifc===true###function===./app-framework-binder/src/afb-evt.c/unwatch(struct afb_evt_listener*,struct afb_evtid*,int)") && ((AKA_mark("lis===504###sois===12238###eois===12244###lif===3###soif===132###eoif===138###isc===true###function===./app-framework-binder/src/afb-evt.c/unwatch(struct afb_evt_listener*,struct afb_evtid*,int)")&&remove)	&&(AKA_mark("lis===504###sois===12248###eois===12277###lif===3###soif===142###eoif===171###isc===true###function===./app-framework-binder/src/afb-evt.c/unwatch(struct afb_evt_listener*,struct afb_evtid*,int)")&&listener->itf->remove != NULL))) {
		AKA_mark("lis===505###sois===12281###eois===12350###lif===4###soif===175###eoif===244###ins===true###function===./app-framework-binder/src/afb-evt.c/unwatch(struct afb_evt_listener*,struct afb_evtid*,int)");listener->itf->remove(listener->closure, evtid->fullname, evtid->id);
	}
	else {AKA_mark("lis===-504-###sois===-12238-###eois===-1223839-###lif===-3-###soif===-###eoif===-171-###ins===true###function===./app-framework-binder/src/afb-evt.c/unwatch(struct afb_evt_listener*,struct afb_evtid*,int)");}

}

/** Instrumented function evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int) */
static void evtid_unwatch(struct afb_evtid *evtid, struct afb_evt_listener *listener, struct afb_evt_watch *watch, int remove)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)");AKA_fCall++;
		AKA_mark("lis===510###sois===12484###eois===12511###lif===2###soif===130###eoif===157###ins===true###function===./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)");struct afb_evt_watch **prv;


	/* notify listener if needed */
		AKA_mark("lis===513###sois===12547###eois===12580###lif===5###soif===193###eoif===226###ins===true###function===./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)");unwatch(listener, evtid, remove);


	/* unlink the watch for its event */
		AKA_mark("lis===516###sois===12621###eois===12662###lif===8###soif===267###eoif===308###ins===true###function===./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)");pthread_rwlock_wrlock(&listener->rwlock);

		AKA_mark("lis===517###sois===12664###eois===12688###lif===9###soif===310###eoif===334###ins===true###function===./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)");prv = &listener->watchs;

		while (AKA_mark("lis===518###sois===12696###eois===12700###lif===10###soif===342###eoif===346###ifc===true###function===./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)") && (AKA_mark("lis===518###sois===12696###eois===12700###lif===10###soif===342###eoif===346###isc===true###function===./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)")&&*prv)) {
				if (AKA_mark("lis===519###sois===12710###eois===12723###lif===11###soif===356###eoif===369###ifc===true###function===./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)") && (AKA_mark("lis===519###sois===12710###eois===12723###lif===11###soif===356###eoif===369###isc===true###function===./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)")&&*prv == watch)) {
						AKA_mark("lis===520###sois===12730###eois===12761###lif===12###soif===376###eoif===407###ins===true###function===./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)");*prv = watch->next_by_listener;

						AKA_mark("lis===521###sois===12765###eois===12771###lif===13###soif===411###eoif===417###ins===true###function===./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)");break;

		}
		else {AKA_mark("lis===-519-###sois===-12710-###eois===-1271013-###lif===-11-###soif===-###eoif===-369-###ins===true###function===./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)");}

				AKA_mark("lis===523###sois===12778###eois===12810###lif===15###soif===424###eoif===456###ins===true###function===./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)");prv = &(*prv)->next_by_listener;

	}

		AKA_mark("lis===525###sois===12815###eois===12856###lif===17###soif===461###eoif===502###ins===true###function===./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)");pthread_rwlock_unlock(&listener->rwlock);


	/* recycle memory */
		AKA_mark("lis===528###sois===12881###eois===12893###lif===20###soif===527###eoif===539###ins===true###function===./app-framework-binder/src/afb-evt.c/evtid_unwatch(struct afb_evtid*,struct afb_evt_listener*,struct afb_evt_watch*,int)");free(watch);

}

/** Instrumented function listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int) */
static void listener_unwatch(struct afb_evt_listener *listener, struct afb_evtid *evtid, struct afb_evt_watch *watch, int remove)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)");AKA_fCall++;
		AKA_mark("lis===533###sois===13030###eois===13057###lif===2###soif===133###eoif===160###ins===true###function===./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)");struct afb_evt_watch **prv;


	/* notify listener if needed */
		AKA_mark("lis===536###sois===13093###eois===13126###lif===5###soif===196###eoif===229###ins===true###function===./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)");unwatch(listener, evtid, remove);


	/* unlink the watch for its event */
		AKA_mark("lis===539###sois===13167###eois===13205###lif===8###soif===270###eoif===308###ins===true###function===./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)");pthread_rwlock_wrlock(&evtid->rwlock);

		AKA_mark("lis===540###sois===13207###eois===13228###lif===9###soif===310###eoif===331###ins===true###function===./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)");prv = &evtid->watchs;

		while (AKA_mark("lis===541###sois===13236###eois===13240###lif===10###soif===339###eoif===343###ifc===true###function===./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)") && (AKA_mark("lis===541###sois===13236###eois===13240###lif===10###soif===339###eoif===343###isc===true###function===./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)")&&*prv)) {
				if (AKA_mark("lis===542###sois===13250###eois===13263###lif===11###soif===353###eoif===366###ifc===true###function===./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)") && (AKA_mark("lis===542###sois===13250###eois===13263###lif===11###soif===353###eoif===366###isc===true###function===./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)")&&*prv == watch)) {
						AKA_mark("lis===543###sois===13270###eois===13298###lif===12###soif===373###eoif===401###ins===true###function===./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)");*prv = watch->next_by_evtid;

						AKA_mark("lis===544###sois===13302###eois===13308###lif===13###soif===405###eoif===411###ins===true###function===./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)");break;

		}
		else {AKA_mark("lis===-542-###sois===-13250-###eois===-1325013-###lif===-11-###soif===-###eoif===-366-###ins===true###function===./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)");}

				AKA_mark("lis===546###sois===13315###eois===13344###lif===15###soif===418###eoif===447###ins===true###function===./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)");prv = &(*prv)->next_by_evtid;

	}

		AKA_mark("lis===548###sois===13349###eois===13387###lif===17###soif===452###eoif===490###ins===true###function===./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)");pthread_rwlock_unlock(&evtid->rwlock);


	/* recycle memory */
		AKA_mark("lis===551###sois===13412###eois===13424###lif===20###soif===515###eoif===527###ins===true###function===./app-framework-binder/src/afb-evt.c/listener_unwatch(struct afb_evt_listener*,struct afb_evtid*,struct afb_evt_watch*,int)");free(watch);

}

/*
 * Creates an event of name 'fullname' and returns it or NULL on error.
 */
/** Instrumented function afb_evt_evtid_create(const char*) */
struct afb_evtid *afb_evt_evtid_create(const char *fullname)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");AKA_fCall++;
		AKA_mark("lis===559###sois===13571###eois===13582###lif===2###soif===64###eoif===75###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");size_t len;

		AKA_mark("lis===560###sois===13584###eois===13615###lif===3###soif===77###eoif===108###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");struct afb_evtid *evtid, *oevt;

		AKA_mark("lis===561###sois===13617###eois===13629###lif===4###soif===110###eoif===122###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");uint16_t id;


	/* allocates the event */
		AKA_mark("lis===564###sois===13659###eois===13682###lif===7###soif===152###eoif===175###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");len = strlen(fullname);

		AKA_mark("lis===565###sois===13684###eois===13725###lif===8###soif===177###eoif===218###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");evtid = malloc(len + 1 + sizeof * evtid);

		if (AKA_mark("lis===566###sois===13731###eois===13744###lif===9###soif===224###eoif===237###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)") && (AKA_mark("lis===566###sois===13731###eois===13744###lif===9###soif===224###eoif===237###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)")&&evtid == NULL)) {
		AKA_mark("lis===567###sois===13748###eois===13759###lif===10###soif===241###eoif===252###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");goto error;
	}
	else {AKA_mark("lis===-566-###sois===-13731-###eois===-1373113-###lif===-9-###soif===-###eoif===-237-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");}


	/* allocates the id */
		AKA_mark("lis===570###sois===13786###eois===13824###lif===13###soif===279###eoif===317###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");pthread_rwlock_wrlock(&events_rwlock);

		if (AKA_mark("lis===571###sois===13830###eois===13855###lif===14###soif===323###eoif===348###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)") && (AKA_mark("lis===571###sois===13830###eois===13855###lif===14###soif===323###eoif===348###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)")&&event_count == UINT16_MAX)) {
				AKA_mark("lis===572###sois===13861###eois===13899###lif===15###soif===354###eoif===392###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");pthread_rwlock_unlock(&events_rwlock);

				AKA_mark("lis===573###sois===13902###eois===13914###lif===16###soif===395###eoif===407###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");free(evtid);

				AKA_mark("lis===574###sois===13917###eois===13951###lif===17###soif===410###eoif===444###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");ERROR("Can't create more events");

				AKA_mark("lis===575###sois===13954###eois===13966###lif===18###soif===447###eoif===459###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");return NULL;

	}
	else {AKA_mark("lis===-571-###sois===-13830-###eois===-1383025-###lif===-14-###soif===-###eoif===-348-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");}

		AKA_mark("lis===577###sois===13971###eois===13985###lif===20###soif===464###eoif===478###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");event_count++;

		do {
		/* TODO add a guard (counting number of event created) */
				AKA_mark("lis===580###sois===14054###eois===14073###lif===23###soif===547###eoif===566###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");id = ++event_genid;

				if (AKA_mark("lis===581###sois===14080###eois===14083###lif===24###soif===573###eoif===576###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)") && (AKA_mark("lis===581###sois===14080###eois===14083###lif===24###soif===573###eoif===576###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)")&&!id)) {
			AKA_mark("lis===582###sois===14088###eois===14109###lif===25###soif===581###eoif===602###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");id = event_genid = 1;
		}
		else {AKA_mark("lis===-581-###sois===-14080-###eois===-140803-###lif===-24-###soif===-###eoif===-576-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");}

				AKA_mark("lis===583###sois===14112###eois===14126###lif===26###soif===605###eoif===619###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");oevt = evtids;

				while (AKA_mark("lis===584###sois===14135###eois===14165###lif===27###soif===628###eoif===658###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)") && ((AKA_mark("lis===584###sois===14135###eois===14147###lif===27###soif===628###eoif===640###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)")&&oevt != NULL)	&&(AKA_mark("lis===584###sois===14151###eois===14165###lif===27###soif===644###eoif===658###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)")&&oevt->id != id))) {
			AKA_mark("lis===585###sois===14170###eois===14188###lif===28###soif===663###eoif===681###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");oevt = oevt->next;
		}

	}
	while (AKA_mark("lis===586###sois===14199###eois===14211###lif===29###soif===692###eoif===704###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)") && (AKA_mark("lis===586###sois===14199###eois===14211###lif===29###soif===692###eoif===704###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)")&&oevt != NULL));


	/* initialize the event */
		AKA_mark("lis===589###sois===14244###eois===14287###lif===32###soif===737###eoif===780###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");memcpy(evtid->fullname, fullname, len + 1);

		AKA_mark("lis===590###sois===14289###eois===14310###lif===33###soif===782###eoif===803###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");evtid->next = evtids;

		AKA_mark("lis===591###sois===14312###eois===14332###lif===34###soif===805###eoif===825###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");evtid->refcount = 1;

		AKA_mark("lis===592###sois===14334###eois===14355###lif===35###soif===827###eoif===848###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");evtid->watchs = NULL;

		AKA_mark("lis===593###sois===14357###eois===14372###lif===36###soif===850###eoif===865###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");evtid->id = id;

		AKA_mark("lis===594###sois===14374###eois===14416###lif===37###soif===867###eoif===909###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");pthread_rwlock_init(&evtid->rwlock, NULL);

		AKA_mark("lis===595###sois===14418###eois===14433###lif===38###soif===911###eoif===926###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");evtids = evtid;

#if WITH_AFB_HOOK
	evtid->hookflags = afb_hook_flags_evt(evtid->fullname);
	evtid->eventid.itf = evtid->hookflags ? &afb_evt_hooked_event_x2_itf : &afb_evt_event_x2_itf;
	if (evtid->hookflags & afb_hook_flag_evt_create)
		afb_hook_evt_create(evtid->fullname, evtid->id);
#else
		AKA_mark("lis===602###sois===14712###eois===14755###lif===45###soif===1205###eoif===1248###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");evtid->eventid.itf = &afb_evt_event_x2_itf;

#endif
		AKA_mark("lis===604###sois===14764###eois===14802###lif===47###soif===1257###eoif===1295###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");pthread_rwlock_unlock(&events_rwlock);


	/* returns the event */
		AKA_mark("lis===607###sois===14830###eois===14843###lif===50###soif===1323###eoif===1336###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");return evtid;

	error:
	AKA_mark("lis===609###sois===14852###eois===14864###lif===52###soif===1345###eoif===1357###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create(const char*)");return NULL;

}

/*
 * Creates an event of name 'prefix'/'name' and returns it or NULL on error.
 */
/** Instrumented function afb_evt_evtid_create2(const char*,const char*) */
struct afb_evtid *afb_evt_evtid_create2(const char *prefix, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create2(const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===617###sois===15033###eois===15056###lif===2###soif===81###eoif===104###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create2(const char*,const char*)");size_t prelen, postlen;

		AKA_mark("lis===618###sois===15058###eois===15073###lif===3###soif===106###eoif===121###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create2(const char*,const char*)");char *fullname;


	/* makes the event fullname */
		AKA_mark("lis===621###sois===15108###eois===15132###lif===6###soif===156###eoif===180###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create2(const char*,const char*)");prelen = strlen(prefix);

		AKA_mark("lis===622###sois===15134###eois===15157###lif===7###soif===182###eoif===205###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create2(const char*,const char*)");postlen = strlen(name);

		AKA_mark("lis===623###sois===15159###eois===15199###lif===8###soif===207###eoif===247###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create2(const char*,const char*)");fullname = alloca(prelen + postlen + 2);

		AKA_mark("lis===624###sois===15201###eois===15234###lif===9###soif===249###eoif===282###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create2(const char*,const char*)");memcpy(fullname, prefix, prelen);

		AKA_mark("lis===625###sois===15236###eois===15259###lif===10###soif===284###eoif===307###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create2(const char*,const char*)");fullname[prelen] = '/';

		AKA_mark("lis===626###sois===15261###eois===15310###lif===11###soif===309###eoif===358###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create2(const char*,const char*)");memcpy(fullname + prelen + 1, name, postlen + 1);


	/* create the event */
		AKA_mark("lis===629###sois===15337###eois===15375###lif===14###soif===385###eoif===423###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_create2(const char*,const char*)");return afb_evt_evtid_create(fullname);

}

/*
 * increment the reference count of the event 'evtid'
 */
/** Instrumented function afb_evt_evtid_addref(struct afb_evtid*) */
struct afb_evtid *afb_evt_evtid_addref(struct afb_evtid *evtid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_evtid_addref(struct afb_evtid*)");AKA_fCall++;
		AKA_mark("lis===637###sois===15507###eois===15565###lif===2###soif===67###eoif===125###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_addref(struct afb_evtid*)");__atomic_add_fetch(&evtid->refcount, 1, __ATOMIC_RELAXED);

		AKA_mark("lis===638###sois===15567###eois===15580###lif===3###soif===127###eoif===140###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_addref(struct afb_evtid*)");return evtid;

}

#if WITH_AFB_HOOK
/*
 * increment the reference count of the event 'evtid'
 */
struct afb_evtid *afb_evt_evtid_hooked_addref(struct afb_evtid *evtid)
{
	if (evtid->hookflags & afb_hook_flag_evt_addref)
		afb_hook_evt_addref(evtid->fullname, evtid->id);
	return afb_evt_evtid_addref(evtid);
}
#endif

/*
 * decrement the reference count of the event 'evtid'
 * and destroy it when the count reachs zero
 */
/** Instrumented function afb_evt_evtid_unref(struct afb_evtid*) */
void afb_evt_evtid_unref(struct afb_evtid *evtid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");AKA_fCall++;
		AKA_mark("lis===659###sois===16043###eois===16072###lif===2###soif===53###eoif===82###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");struct afb_evtid **prv, *oev;

		AKA_mark("lis===660###sois===16074###eois===16111###lif===3###soif===84###eoif===121###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");struct afb_evt_watch *watch, *nwatch;


		if (AKA_mark("lis===662###sois===16118###eois===16176###lif===5###soif===128###eoif===186###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)") && (AKA_mark("lis===662###sois===16118###eois===16176###lif===5###soif===128###eoif===186###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)")&&!__atomic_sub_fetch(&evtid->refcount, 1, __ATOMIC_RELAXED))) {
		/* unlinks the event if valid! */
				AKA_mark("lis===664###sois===16218###eois===16256###lif===7###soif===228###eoif===266###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");pthread_rwlock_wrlock(&events_rwlock);

				AKA_mark("lis===665###sois===16259###eois===16273###lif===8###soif===269###eoif===283###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");prv = &evtids;

				for (;;) {
						AKA_mark("lis===667###sois===16289###eois===16300###lif===10###soif===299###eoif===310###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");oev = *prv;

						if (AKA_mark("lis===668###sois===16308###eois===16320###lif===11###soif===318###eoif===330###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)") && (AKA_mark("lis===668###sois===16308###eois===16320###lif===11###soif===318###eoif===330###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)")&&oev == evtid)) {
				AKA_mark("lis===669###sois===16326###eois===16332###lif===12###soif===336###eoif===342###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");break;
			}
			else {AKA_mark("lis===-668-###sois===-16308-###eois===-1630812-###lif===-11-###soif===-###eoif===-330-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");}

						if (AKA_mark("lis===670###sois===16340###eois===16344###lif===13###soif===350###eoif===354###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)") && (AKA_mark("lis===670###sois===16340###eois===16344###lif===13###soif===350###eoif===354###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)")&&!oev)) {
								AKA_mark("lis===671###sois===16352###eois===16378###lif===14###soif===362###eoif===388###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");ERROR("unexpected event");

								AKA_mark("lis===672###sois===16383###eois===16421###lif===15###soif===393###eoif===431###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");pthread_rwlock_unlock(&events_rwlock);

								AKA_mark("lis===673###sois===16426###eois===16433###lif===16###soif===436###eoif===443###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");return;

			}
			else {AKA_mark("lis===-670-###sois===-16340-###eois===-163404-###lif===-13-###soif===-###eoif===-354-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");}

						AKA_mark("lis===675###sois===16442###eois===16459###lif===18###soif===452###eoif===469###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");prv = &oev->next;

		}

				AKA_mark("lis===677###sois===16466###eois===16480###lif===20###soif===476###eoif===490###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");event_count--;

				AKA_mark("lis===678###sois===16483###eois===16502###lif===21###soif===493###eoif===512###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");*prv = evtid->next;

				AKA_mark("lis===679###sois===16505###eois===16543###lif===22###soif===515###eoif===553###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");pthread_rwlock_unlock(&events_rwlock);


		/* removes all watchers */
				AKA_mark("lis===682###sois===16576###eois===16614###lif===25###soif===586###eoif===624###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");pthread_rwlock_wrlock(&evtid->rwlock);

				AKA_mark("lis===683###sois===16617###eois===16639###lif===26###soif===627###eoif===649###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");watch = evtid->watchs;

				AKA_mark("lis===684###sois===16642###eois===16663###lif===27###soif===652###eoif===673###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");evtid->watchs = NULL;

				AKA_mark("lis===685###sois===16666###eois===16704###lif===28###soif===676###eoif===714###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");pthread_rwlock_unlock(&evtid->rwlock);

				while (AKA_mark("lis===686###sois===16713###eois===16718###lif===29###soif===723###eoif===728###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)") && (AKA_mark("lis===686###sois===16713###eois===16718###lif===29###soif===723###eoif===728###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)")&&watch)) {
						AKA_mark("lis===687###sois===16725###eois===16755###lif===30###soif===735###eoif===765###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");nwatch = watch->next_by_evtid;

						AKA_mark("lis===688###sois===16759###eois===16807###lif===31###soif===769###eoif===817###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");evtid_unwatch(evtid, watch->listener, watch, 1);

						AKA_mark("lis===689###sois===16811###eois===16826###lif===32###soif===821###eoif===836###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");watch = nwatch;

		}


		/* free */
				AKA_mark("lis===693###sois===16847###eois===16886###lif===36###soif===857###eoif===896###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");pthread_rwlock_destroy(&evtid->rwlock);

				AKA_mark("lis===694###sois===16889###eois===16901###lif===37###soif===899###eoif===911###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");free(evtid);

	}
	else {AKA_mark("lis===-662-###sois===-16118-###eois===-1611858-###lif===-5-###soif===-###eoif===-186-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_unref(struct afb_evtid*)");}

}

#if WITH_AFB_HOOK
/*
 * decrement the reference count of the event 'evtid'
 * and destroy it when the count reachs zero
 */
void afb_evt_evtid_hooked_unref(struct afb_evtid *evtid)
{
	if (evtid->hookflags & afb_hook_flag_evt_unref)
		afb_hook_evt_unref(evtid->fullname, evtid->id);
	afb_evt_evtid_unref(evtid);
}
#endif

/*
 * Returns the true name of the 'event'
 */
/** Instrumented function afb_evt_evtid_fullname(struct afb_evtid*) */
const char *afb_evt_evtid_fullname(struct afb_evtid *evtid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_evtid_fullname(struct afb_evtid*)");AKA_fCall++;
		AKA_mark("lis===716###sois===17339###eois===17362###lif===2###soif===63###eoif===86###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_fullname(struct afb_evtid*)");return evtid->fullname;

}

/*
 * Returns the name of the 'event'
 */
/** Instrumented function afb_evt_evtid_name(struct afb_evtid*) */
const char *afb_evt_evtid_name(struct afb_evtid *evtid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_evtid_name(struct afb_evtid*)");AKA_fCall++;
		AKA_mark("lis===724###sois===17467###eois===17515###lif===2###soif===59###eoif===107###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_name(struct afb_evtid*)");const char *name = strchr(evtid->fullname, '/');

		AKA_mark("lis===725###sois===17517###eois===17558###lif===3###soif===109###eoif===150###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_name(struct afb_evtid*)");return name ? name + 1 : evtid->fullname;

}

#if WITH_AFB_HOOK
/*
 * Returns the name associated to the event 'evtid'.
 */
const char *afb_evt_evtid_hooked_name(struct afb_evtid *evtid)
{
	const char *result = afb_evt_evtid_name(evtid);
	if (evtid->hookflags & afb_hook_flag_evt_name)
		afb_hook_evt_name(evtid->fullname, evtid->id, result);
	return result;
}
#endif

/*
 * Returns the id of the 'event'
 */
/** Instrumented function afb_evt_evtid_id(struct afb_evtid*) */
uint16_t afb_evt_evtid_id(struct afb_evtid *evtid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_evtid_id(struct afb_evtid*)");AKA_fCall++;
		AKA_mark("lis===746###sois===17979###eois===17996###lif===2###soif===54###eoif===71###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_evtid_id(struct afb_evtid*)");return evtid->id;

}

/*
 * Returns an instance of the listener defined by the 'send' callback
 * and the 'closure'.
 * Returns NULL in case of memory depletion.
 */
/** Instrumented function afb_evt_listener_create(const struct afb_evt_itf*,void*) */
struct afb_evt_listener *afb_evt_listener_create(const struct afb_evt_itf *itf, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");AKA_fCall++;
		AKA_mark("lis===756###sois===18242###eois===18276###lif===2###soif===98###eoif===132###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");struct afb_evt_listener *listener;


	/* search if an instance already exists */
		AKA_mark("lis===759###sois===18323###eois===18364###lif===5###soif===179###eoif===220###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");pthread_rwlock_wrlock(&listeners_rwlock);

		AKA_mark("lis===760###sois===18366###eois===18387###lif===6###soif===222###eoif===243###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");listener = listeners;

		while (AKA_mark("lis===761###sois===18396###eois===18412###lif===7###soif===252###eoif===268###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)") && (AKA_mark("lis===761###sois===18396###eois===18412###lif===7###soif===252###eoif===268###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)")&&listener != NULL)) {
				if (AKA_mark("lis===762###sois===18422###eois===18474###lif===8###soif===278###eoif===330###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)") && ((AKA_mark("lis===762###sois===18422###eois===18442###lif===8###soif===278###eoif===298###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)")&&listener->itf == itf)	&&(AKA_mark("lis===762###sois===18446###eois===18474###lif===8###soif===302###eoif===330###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)")&&listener->closure == closure))) {
						AKA_mark("lis===763###sois===18481###eois===18526###lif===9###soif===337###eoif===382###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");listener = afb_evt_listener_addref(listener);

						AKA_mark("lis===764###sois===18530###eois===18541###lif===10###soif===386###eoif===397###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");goto found;

		}
		else {AKA_mark("lis===-762-###sois===-18422-###eois===-1842252-###lif===-8-###soif===-###eoif===-330-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");}

				AKA_mark("lis===766###sois===18548###eois===18574###lif===12###soif===404###eoif===430###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");listener = listener->next;

	}


	/* allocates */
		AKA_mark("lis===770###sois===18597###eois===18636###lif===16###soif===453###eoif===492###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");listener = calloc(1, sizeof *listener);

		if (AKA_mark("lis===771###sois===18642###eois===18658###lif===17###soif===498###eoif===514###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)") && (AKA_mark("lis===771###sois===18642###eois===18658###lif===17###soif===498###eoif===514###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)")&&listener != NULL)) {
		/* init */
				AKA_mark("lis===773###sois===18677###eois===18697###lif===19###soif===533###eoif===553###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");listener->itf = itf;

				AKA_mark("lis===774###sois===18700###eois===18728###lif===20###soif===556###eoif===584###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");listener->closure = closure;

				AKA_mark("lis===775###sois===18731###eois===18755###lif===21###soif===587###eoif===611###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");listener->watchs = NULL;

				AKA_mark("lis===776###sois===18758###eois===18781###lif===22###soif===614###eoif===637###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");listener->refcount = 1;

				AKA_mark("lis===777###sois===18784###eois===18829###lif===23###soif===640###eoif===685###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");pthread_rwlock_init(&listener->rwlock, NULL);

				AKA_mark("lis===778###sois===18832###eois===18859###lif===24###soif===688###eoif===715###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");listener->next = listeners;

				AKA_mark("lis===779###sois===18862###eois===18883###lif===25###soif===718###eoif===739###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");listeners = listener;

	}
	else {AKA_mark("lis===-771-###sois===-18642-###eois===-1864216-###lif===-17-###soif===-###eoif===-514-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");}

 	found:
	AKA_mark("lis===782###sois===18896###eois===18937###lif===28###soif===752###eoif===793###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");pthread_rwlock_unlock(&listeners_rwlock);

		AKA_mark("lis===783###sois===18939###eois===18955###lif===29###soif===795###eoif===811###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_create(const struct afb_evt_itf*,void*)");return listener;

}

/*
 * Increases the reference count of 'listener' and returns it
 */
/** Instrumented function afb_evt_listener_addref(struct afb_evt_listener*) */
struct afb_evt_listener *afb_evt_listener_addref(struct afb_evt_listener *listener)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_listener_addref(struct afb_evt_listener*)");AKA_fCall++;
		AKA_mark("lis===791###sois===19115###eois===19176###lif===2###soif===87###eoif===148###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_addref(struct afb_evt_listener*)");__atomic_add_fetch(&listener->refcount, 1, __ATOMIC_RELAXED);

		AKA_mark("lis===792###sois===19178###eois===19194###lif===3###soif===150###eoif===166###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_addref(struct afb_evt_listener*)");return listener;

}

/*
 * Decreases the reference count of the 'listener' and destroys it
 * when no more used.
 */
/** Instrumented function afb_evt_listener_unref(struct afb_evt_listener*) */
void afb_evt_listener_unref(struct afb_evt_listener *listener)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");AKA_fCall++;
		AKA_mark("lis===801###sois===19360###eois===19397###lif===2###soif===66###eoif===103###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");struct afb_evt_listener **prv, *olis;


		if (AKA_mark("lis===803###sois===19404###eois===19477###lif===4###soif===110###eoif===183###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)") && ((AKA_mark("lis===803###sois===19404###eois===19412###lif===4###soif===110###eoif===118###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)")&&listener)	&&(AKA_mark("lis===803###sois===19416###eois===19477###lif===4###soif===122###eoif===183###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)")&&!__atomic_sub_fetch(&listener->refcount, 1, __ATOMIC_RELAXED)))) {

		/* unlink the listener */
				AKA_mark("lis===806###sois===19512###eois===19553###lif===7###soif===218###eoif===259###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");pthread_rwlock_wrlock(&listeners_rwlock);

				AKA_mark("lis===807###sois===19556###eois===19573###lif===8###soif===262###eoif===279###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");prv = &listeners;

				for (;;) {
						AKA_mark("lis===809###sois===19589###eois===19601###lif===10###soif===295###eoif===307###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");olis = *prv;

						if (AKA_mark("lis===810###sois===19609###eois===19625###lif===11###soif===315###eoif===331###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)") && (AKA_mark("lis===810###sois===19609###eois===19625###lif===11###soif===315###eoif===331###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)")&&olis == listener)) {
				AKA_mark("lis===811###sois===19631###eois===19637###lif===12###soif===337###eoif===343###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");break;
			}
			else {AKA_mark("lis===-810-###sois===-19609-###eois===-1960916-###lif===-11-###soif===-###eoif===-331-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");}

						if (AKA_mark("lis===812###sois===19645###eois===19650###lif===13###soif===351###eoif===356###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)") && (AKA_mark("lis===812###sois===19645###eois===19650###lif===13###soif===351###eoif===356###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)")&&!olis)) {
								AKA_mark("lis===813###sois===19658###eois===19687###lif===14###soif===364###eoif===393###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");ERROR("unexpected listener");

								AKA_mark("lis===814###sois===19692###eois===19733###lif===15###soif===398###eoif===439###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");pthread_rwlock_unlock(&listeners_rwlock);

								AKA_mark("lis===815###sois===19738###eois===19745###lif===16###soif===444###eoif===451###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");return;

			}
			else {AKA_mark("lis===-812-###sois===-19645-###eois===-196455-###lif===-13-###soif===-###eoif===-356-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");}

						AKA_mark("lis===817###sois===19754###eois===19772###lif===18###soif===460###eoif===478###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");prv = &olis->next;

		}

				AKA_mark("lis===819###sois===19779###eois===19801###lif===20###soif===485###eoif===507###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");*prv = listener->next;

				AKA_mark("lis===820###sois===19804###eois===19845###lif===21###soif===510###eoif===551###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");pthread_rwlock_unlock(&listeners_rwlock);


		/* remove the watchers */
				AKA_mark("lis===823###sois===19877###eois===19919###lif===24###soif===583###eoif===625###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");afb_evt_listener_unwatch_all(listener, 0);


		/* free the listener */
				AKA_mark("lis===826###sois===19949###eois===19991###lif===27###soif===655###eoif===697###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");pthread_rwlock_destroy(&listener->rwlock);

				AKA_mark("lis===827###sois===19994###eois===20009###lif===28###soif===700###eoif===715###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");free(listener);

	}
	else {AKA_mark("lis===-803-###sois===-19404-###eois===-1940473-###lif===-4-###soif===-###eoif===-183-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unref(struct afb_evt_listener*)");}

}

/*
 * Makes the 'listener' watching 'evtid'
 * Returns 0 in case of success or else -1.
 */
/** Instrumented function afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*) */
int afb_evt_listener_watch_evt(struct afb_evt_listener *listener, struct afb_evtid *evtid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");AKA_fCall++;
		AKA_mark("lis===837###sois===20202###eois===20230###lif===2###soif===94###eoif===122###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");struct afb_evt_watch *watch;


	/* check parameter */
		if (AKA_mark("lis===840###sois===20260###eois===20287###lif===5###soif===152###eoif===179###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)") && (AKA_mark("lis===840###sois===20260###eois===20287###lif===5###soif===152###eoif===179###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)")&&listener->itf->push == NULL)) {
				AKA_mark("lis===841###sois===20293###eois===20308###lif===6###soif===185###eoif===200###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");errno = EINVAL;

				AKA_mark("lis===842###sois===20311###eois===20321###lif===7###soif===203###eoif===213###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");return -1;

	}
	else {AKA_mark("lis===-840-###sois===-20260-###eois===-2026027-###lif===-5-###soif===-###eoif===-179-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");}


	/* search the existing watch for the listener */
		AKA_mark("lis===846###sois===20377###eois===20418###lif===11###soif===269###eoif===310###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");pthread_rwlock_wrlock(&listener->rwlock);

		AKA_mark("lis===847###sois===20420###eois===20445###lif===12###soif===312###eoif===337###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");watch = listener->watchs;

		while (AKA_mark("lis===848###sois===20453###eois===20466###lif===13###soif===345###eoif===358###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)") && (AKA_mark("lis===848###sois===20453###eois===20466###lif===13###soif===345###eoif===358###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)")&&watch != NULL)) {
				if (AKA_mark("lis===849###sois===20476###eois===20497###lif===14###soif===368###eoif===389###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)") && (AKA_mark("lis===849###sois===20476###eois===20497###lif===14###soif===368###eoif===389###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)")&&watch->evtid == evtid)) {
			AKA_mark("lis===850###sois===20502###eois===20511###lif===15###soif===394###eoif===403###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");goto end;
		}
		else {AKA_mark("lis===-849-###sois===-20476-###eois===-2047621-###lif===-14-###soif===-###eoif===-389-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");}

				AKA_mark("lis===851###sois===20514###eois===20546###lif===16###soif===406###eoif===438###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");watch = watch->next_by_listener;

	}


	/* not found, allocate a new */
		AKA_mark("lis===855###sois===20585###eois===20615###lif===20###soif===477###eoif===507###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");watch = malloc(sizeof *watch);

		if (AKA_mark("lis===856###sois===20621###eois===20634###lif===21###soif===513###eoif===526###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)") && (AKA_mark("lis===856###sois===20621###eois===20634###lif===21###soif===513###eoif===526###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)")&&watch == NULL)) {
				AKA_mark("lis===857###sois===20640###eois===20681###lif===22###soif===532###eoif===573###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");pthread_rwlock_unlock(&listener->rwlock);

				AKA_mark("lis===858###sois===20684###eois===20699###lif===23###soif===576###eoif===591###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");errno = ENOMEM;

				AKA_mark("lis===859###sois===20702###eois===20712###lif===24###soif===594###eoif===604###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");return -1;

	}
	else {AKA_mark("lis===-856-###sois===-20621-###eois===-2062113-###lif===-21-###soif===-###eoif===-526-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");}


	/* initialise and link */
		AKA_mark("lis===863###sois===20745###eois===20766###lif===28###soif===637###eoif===658###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");watch->evtid = evtid;

		AKA_mark("lis===864###sois===20768###eois===20795###lif===29###soif===660###eoif===687###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");watch->listener = listener;

		AKA_mark("lis===865###sois===20797###eois===20840###lif===30###soif===689###eoif===732###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");watch->next_by_listener = listener->watchs;

		AKA_mark("lis===866###sois===20842###eois===20867###lif===31###soif===734###eoif===759###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");listener->watchs = watch;

		AKA_mark("lis===867###sois===20869###eois===20907###lif===32###soif===761###eoif===799###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");pthread_rwlock_wrlock(&evtid->rwlock);

		AKA_mark("lis===868###sois===20909###eois===20946###lif===33###soif===801###eoif===838###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");watch->next_by_evtid = evtid->watchs;

		AKA_mark("lis===869###sois===20948###eois===20970###lif===34###soif===840###eoif===862###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");evtid->watchs = watch;

		AKA_mark("lis===870###sois===20972###eois===21010###lif===35###soif===864###eoif===902###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");pthread_rwlock_unlock(&evtid->rwlock);


		if (AKA_mark("lis===872###sois===21017###eois===21043###lif===37###soif===909###eoif===935###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)") && (AKA_mark("lis===872###sois===21017###eois===21043###lif===37###soif===909###eoif===935###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)")&&listener->itf->add != NULL)) {
		AKA_mark("lis===873###sois===21047###eois===21113###lif===38###soif===939###eoif===1005###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");listener->itf->add(listener->closure, evtid->fullname, evtid->id);
	}
	else {AKA_mark("lis===-872-###sois===-21017-###eois===-2101726-###lif===-37-###soif===-###eoif===-935-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");}

	end:
	AKA_mark("lis===875###sois===21120###eois===21161###lif===40###soif===1012###eoif===1053###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");pthread_rwlock_unlock(&listener->rwlock);

		AKA_mark("lis===876###sois===21163###eois===21172###lif===41###soif===1055###eoif===1064###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_evt(struct afb_evt_listener*,struct afb_evtid*)");return 0;

}

/*
 * Avoids the 'listener' to watch 'evtid'
 * Returns 0 in case of success or else -1.
 */
/** Instrumented function afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*) */
int afb_evt_listener_unwatch_evt(struct afb_evt_listener *listener, struct afb_evtid *evtid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");AKA_fCall++;
		AKA_mark("lis===885###sois===21365###eois===21403###lif===2###soif===96###eoif===134###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");struct afb_evt_watch *watch, **pwatch;


	/* search the existing watch */
		AKA_mark("lis===888###sois===21439###eois===21480###lif===5###soif===170###eoif===211###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");pthread_rwlock_wrlock(&listener->rwlock);

		AKA_mark("lis===889###sois===21482###eois===21509###lif===6###soif===213###eoif===240###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");pwatch = &listener->watchs;

		for (;;) {
				AKA_mark("lis===891###sois===21524###eois===21540###lif===8###soif===255###eoif===271###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");watch = *pwatch;

				if (AKA_mark("lis===892###sois===21547###eois===21553###lif===9###soif===278###eoif===284###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)") && (AKA_mark("lis===892###sois===21547###eois===21553###lif===9###soif===278###eoif===284###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)")&&!watch)) {
						AKA_mark("lis===893###sois===21560###eois===21601###lif===10###soif===291###eoif===332###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");pthread_rwlock_unlock(&listener->rwlock);

						AKA_mark("lis===894###sois===21605###eois===21620###lif===11###soif===336###eoif===351###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");errno = ENOENT;

						AKA_mark("lis===895###sois===21624###eois===21634###lif===12###soif===355###eoif===365###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");return -1;

		}
		else {AKA_mark("lis===-892-###sois===-21547-###eois===-215476-###lif===-9-###soif===-###eoif===-284-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");}

				if (AKA_mark("lis===897###sois===21645###eois===21666###lif===14###soif===376###eoif===397###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)") && (AKA_mark("lis===897###sois===21645###eois===21666###lif===14###soif===376###eoif===397###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)")&&evtid == watch->evtid)) {
						AKA_mark("lis===898###sois===21673###eois===21707###lif===15###soif===404###eoif===438###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");*pwatch = watch->next_by_listener;

						AKA_mark("lis===899###sois===21711###eois===21752###lif===16###soif===442###eoif===483###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");pthread_rwlock_unlock(&listener->rwlock);

						AKA_mark("lis===900###sois===21756###eois===21800###lif===17###soif===487###eoif===531###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");listener_unwatch(listener, evtid, watch, 1);

						AKA_mark("lis===901###sois===21804###eois===21813###lif===18###soif===535###eoif===544###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");return 0;

		}
		else {AKA_mark("lis===-897-###sois===-21645-###eois===-2164521-###lif===-14-###soif===-###eoif===-397-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");}

				AKA_mark("lis===903###sois===21820###eois===21854###lif===20###soif===551###eoif===585###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_evt(struct afb_evt_listener*,struct afb_evtid*)");pwatch = &watch->next_by_listener;

	}

}

/*
 * Avoids the 'listener' to watch 'eventid'
 * Returns 0 in case of success or else -1.
 */
/** Instrumented function afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t) */
int afb_evt_listener_unwatch_id(struct afb_evt_listener *listener, uint16_t eventid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");AKA_fCall++;
		AKA_mark("lis===913###sois===22044###eois===22082###lif===2###soif===88###eoif===126###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");struct afb_evt_watch *watch, **pwatch;

		AKA_mark("lis===914###sois===22084###eois===22108###lif===3###soif===128###eoif===152###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");struct afb_evtid *evtid;


	/* search the existing watch */
		AKA_mark("lis===917###sois===22144###eois===22185###lif===6###soif===188###eoif===229###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");pthread_rwlock_wrlock(&listener->rwlock);

		AKA_mark("lis===918###sois===22187###eois===22214###lif===7###soif===231###eoif===258###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");pwatch = &listener->watchs;

		for (;;) {
				AKA_mark("lis===920###sois===22229###eois===22245###lif===9###soif===273###eoif===289###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");watch = *pwatch;

				if (AKA_mark("lis===921###sois===22252###eois===22258###lif===10###soif===296###eoif===302###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)") && (AKA_mark("lis===921###sois===22252###eois===22258###lif===10###soif===296###eoif===302###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)")&&!watch)) {
						AKA_mark("lis===922###sois===22265###eois===22306###lif===11###soif===309###eoif===350###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");pthread_rwlock_unlock(&listener->rwlock);

						AKA_mark("lis===923###sois===22310###eois===22325###lif===12###soif===354###eoif===369###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");errno = ENOENT;

						AKA_mark("lis===924###sois===22329###eois===22339###lif===13###soif===373###eoif===383###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");return -1;

		}
		else {AKA_mark("lis===-921-###sois===-22252-###eois===-222526-###lif===-10-###soif===-###eoif===-302-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");}

				AKA_mark("lis===926###sois===22346###eois===22367###lif===15###soif===390###eoif===411###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");evtid = watch->evtid;

				if (AKA_mark("lis===927###sois===22374###eois===22394###lif===16###soif===418###eoif===438###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)") && (AKA_mark("lis===927###sois===22374###eois===22394###lif===16###soif===418###eoif===438###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)")&&evtid->id == eventid)) {
						AKA_mark("lis===928###sois===22401###eois===22435###lif===17###soif===445###eoif===479###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");*pwatch = watch->next_by_listener;

						AKA_mark("lis===929###sois===22439###eois===22480###lif===18###soif===483###eoif===524###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");pthread_rwlock_unlock(&listener->rwlock);

						AKA_mark("lis===930###sois===22484###eois===22528###lif===19###soif===528###eoif===572###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");listener_unwatch(listener, evtid, watch, 1);

						AKA_mark("lis===931###sois===22532###eois===22541###lif===20###soif===576###eoif===585###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");return 0;

		}
		else {AKA_mark("lis===-927-###sois===-22374-###eois===-2237420-###lif===-16-###soif===-###eoif===-438-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");}

				AKA_mark("lis===933###sois===22548###eois===22582###lif===22###soif===592###eoif===626###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_id(struct afb_evt_listener*,uint16_t)");pwatch = &watch->next_by_listener;

	}

}

/*
 * Avoids the 'listener' to watch any event, calling the callback
 * 'remove' of the interface if 'remoe' is not zero.
 */
/** Instrumented function afb_evt_listener_unwatch_all(struct afb_evt_listener*,int) */
void afb_evt_listener_unwatch_all(struct afb_evt_listener *listener, int remove)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_all(struct afb_evt_listener*,int)");AKA_fCall++;
		AKA_mark("lis===943###sois===22799###eois===22836###lif===2###soif===84###eoif===121###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_all(struct afb_evt_listener*,int)");struct afb_evt_watch *watch, *nwatch;


	/* search the existing watch */
		AKA_mark("lis===946###sois===22872###eois===22913###lif===5###soif===157###eoif===198###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_all(struct afb_evt_listener*,int)");pthread_rwlock_wrlock(&listener->rwlock);

		AKA_mark("lis===947###sois===22915###eois===22940###lif===6###soif===200###eoif===225###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_all(struct afb_evt_listener*,int)");watch = listener->watchs;

		AKA_mark("lis===948###sois===22942###eois===22966###lif===7###soif===227###eoif===251###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_all(struct afb_evt_listener*,int)");listener->watchs = NULL;

		AKA_mark("lis===949###sois===22968###eois===23009###lif===8###soif===253###eoif===294###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_all(struct afb_evt_listener*,int)");pthread_rwlock_unlock(&listener->rwlock);

		while (AKA_mark("lis===950###sois===23017###eois===23022###lif===9###soif===302###eoif===307###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_all(struct afb_evt_listener*,int)") && (AKA_mark("lis===950###sois===23017###eois===23022###lif===9###soif===302###eoif===307###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_all(struct afb_evt_listener*,int)")&&watch)) {
				AKA_mark("lis===951###sois===23028###eois===23061###lif===10###soif===313###eoif===346###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_all(struct afb_evt_listener*,int)");nwatch = watch->next_by_listener;

				AKA_mark("lis===952###sois===23064###eois===23120###lif===11###soif===349###eoif===405###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_all(struct afb_evt_listener*,int)");listener_unwatch(listener, watch->evtid, watch, remove);

				AKA_mark("lis===953###sois===23123###eois===23138###lif===12###soif===408###eoif===423###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_all(struct afb_evt_listener*,int)");watch = nwatch;

	}

}

#if WITH_AFB_HOOK
/*
 * update the hooks for events
 */
void afb_evt_update_hooks()
{
	struct afb_evtid *evtid;

	pthread_rwlock_rdlock(&events_rwlock);
	for (evtid = evtids ; evtid ; evtid = evtid->next) {
		evtid->hookflags = afb_hook_flags_evt(evtid->fullname);
		evtid->eventid.itf = evtid->hookflags ? &afb_evt_hooked_event_x2_itf : &afb_evt_event_x2_itf;
	}
	pthread_rwlock_unlock(&events_rwlock);
}
#endif

/** Instrumented function afb_evt_event_x2_to_evtid(struct afb_event_x2*) */
inline struct afb_evtid *afb_evt_event_x2_to_evtid(struct afb_event_x2 *eventid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_to_evtid(struct afb_event_x2*)");AKA_fCall++;
		AKA_mark("lis===976###sois===23643###eois===23677###lif===2###soif===84###eoif===118###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_to_evtid(struct afb_event_x2*)");return (struct afb_evtid*)eventid;

}

/** Instrumented function afb_evt_event_x2_from_evtid(struct afb_evtid*) */
inline struct afb_event_x2 *afb_evt_event_x2_from_evtid(struct afb_evtid *evtid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_from_evtid(struct afb_evtid*)");AKA_fCall++;
		AKA_mark("lis===981###sois===23765###eois===23788###lif===2###soif===84###eoif===107###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_from_evtid(struct afb_evtid*)");return &evtid->eventid;

}

/*
 * Creates an event of 'fullname' and returns it.
 * Returns an event with closure==NULL in case of error.
 */
/** Instrumented function afb_evt_event_x2_create(const char*) */
struct afb_event_x2 *afb_evt_event_x2_create(const char *fullname)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_create(const char*)");AKA_fCall++;
		AKA_mark("lis===990###sois===23976###eois===24043###lif===2###soif===70###eoif===137###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_create(const char*)");return afb_evt_event_x2_from_evtid(afb_evt_evtid_create(fullname));

}

/*
 * Creates an event of name 'prefix'/'name' and returns it.
 * Returns an event with closure==NULL in case of error.
 */
/** Instrumented function afb_evt_event_x2_create2(const char*,const char*) */
struct afb_event_x2 *afb_evt_event_x2_create2(const char *prefix, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_create2(const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===999###sois===24258###eois===24330###lif===2###soif===87###eoif===159###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_create2(const char*,const char*)");return afb_evt_event_x2_from_evtid(afb_evt_evtid_create2(prefix, name));

}

/*
 * Returns the fullname of the 'eventid'
 */
/** Instrumented function afb_evt_event_x2_fullname(struct afb_event_x2*) */
const char *afb_evt_event_x2_fullname(struct afb_event_x2 *eventid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_fullname(struct afb_event_x2*)");AKA_fCall++;
		AKA_mark("lis===1007###sois===24453###eois===24514###lif===2###soif===71###eoif===132###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_fullname(struct afb_event_x2*)");struct afb_evtid *evtid = afb_evt_event_x2_to_evtid(eventid);

		AKA_mark("lis===1008###sois===24516###eois===24554###lif===3###soif===134###eoif===172###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_fullname(struct afb_event_x2*)");return evtid ? evtid->fullname : NULL;

}

/*
 * Returns the id of the 'eventid'
 */
/** Instrumented function afb_evt_event_x2_id(struct afb_event_x2*) */
uint16_t afb_evt_event_x2_id(struct afb_event_x2 *eventid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_id(struct afb_event_x2*)");AKA_fCall++;
		AKA_mark("lis===1016###sois===24662###eois===24723###lif===2###soif===62###eoif===123###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_id(struct afb_event_x2*)");struct afb_evtid *evtid = afb_evt_event_x2_to_evtid(eventid);

		AKA_mark("lis===1017###sois===24725###eois===24754###lif===3###soif===125###eoif===154###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_id(struct afb_event_x2*)");return evtid ? evtid->id : 0;

}

/*
 * Makes the 'listener' watching 'eventid'
 * Returns 0 in case of success or else -1.
 */
/** Instrumented function afb_evt_listener_watch_x2(struct afb_evt_listener*,struct afb_event_x2*) */
int afb_evt_listener_watch_x2(struct afb_evt_listener *listener, struct afb_event_x2 *eventid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_x2(struct afb_evt_listener*,struct afb_event_x2*)");AKA_fCall++;
		AKA_mark("lis===1026###sois===24950###eois===25011###lif===2###soif===98###eoif===159###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_x2(struct afb_evt_listener*,struct afb_event_x2*)");struct afb_evtid *evtid = afb_evt_event_x2_to_evtid(eventid);


	/* check parameter */
		if (AKA_mark("lis===1029###sois===25041###eois===25047###lif===5###soif===189###eoif===195###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_x2(struct afb_evt_listener*,struct afb_event_x2*)") && (AKA_mark("lis===1029###sois===25041###eois===25047###lif===5###soif===189###eoif===195###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_x2(struct afb_evt_listener*,struct afb_event_x2*)")&&!evtid)) {
				AKA_mark("lis===1030###sois===25053###eois===25068###lif===6###soif===201###eoif===216###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_x2(struct afb_evt_listener*,struct afb_event_x2*)");errno = EINVAL;

				AKA_mark("lis===1031###sois===25071###eois===25081###lif===7###soif===219###eoif===229###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_x2(struct afb_evt_listener*,struct afb_event_x2*)");return -1;

	}
	else {AKA_mark("lis===-1029-###sois===-25041-###eois===-250416-###lif===-5-###soif===-###eoif===-195-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_x2(struct afb_evt_listener*,struct afb_event_x2*)");}


	/* search the existing watch for the listener */
		AKA_mark("lis===1035###sois===25137###eois===25188###lif===11###soif===285###eoif===336###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_watch_x2(struct afb_evt_listener*,struct afb_event_x2*)");return afb_evt_listener_watch_evt(listener, evtid);

}

/*
 * Avoids the 'listener' to watch 'eventid'
 * Returns 0 in case of success or else -1.
 */
/** Instrumented function afb_evt_listener_unwatch_x2(struct afb_evt_listener*,struct afb_event_x2*) */
int afb_evt_listener_unwatch_x2(struct afb_evt_listener *listener, struct afb_event_x2 *eventid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_x2(struct afb_evt_listener*,struct afb_event_x2*)");AKA_fCall++;
		AKA_mark("lis===1044###sois===25387###eois===25448###lif===2###soif===100###eoif===161###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_x2(struct afb_evt_listener*,struct afb_event_x2*)");struct afb_evtid *evtid = afb_evt_event_x2_to_evtid(eventid);


	/* check parameter */
		if (AKA_mark("lis===1047###sois===25478###eois===25484###lif===5###soif===191###eoif===197###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_x2(struct afb_evt_listener*,struct afb_event_x2*)") && (AKA_mark("lis===1047###sois===25478###eois===25484###lif===5###soif===191###eoif===197###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_x2(struct afb_evt_listener*,struct afb_event_x2*)")&&!evtid)) {
				AKA_mark("lis===1048###sois===25490###eois===25505###lif===6###soif===203###eoif===218###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_x2(struct afb_evt_listener*,struct afb_event_x2*)");errno = EINVAL;

				AKA_mark("lis===1049###sois===25508###eois===25518###lif===7###soif===221###eoif===231###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_x2(struct afb_evt_listener*,struct afb_event_x2*)");return -1;

	}
	else {AKA_mark("lis===-1047-###sois===-25478-###eois===-254786-###lif===-5-###soif===-###eoif===-197-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_x2(struct afb_evt_listener*,struct afb_event_x2*)");}


	/* search the existing watch */
		AKA_mark("lis===1053###sois===25557###eois===25610###lif===11###soif===270###eoif===323###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_listener_unwatch_x2(struct afb_evt_listener*,struct afb_event_x2*)");return afb_evt_listener_unwatch_evt(listener, evtid);

}

int afb_evt_event_x2_push(struct afb_event_x2 *eventid, struct json_object *object)
#if WITH_AFB_HOOK
{
	struct afb_evtid *evtid = afb_evt_event_x2_to_evtid(eventid);
	if (evtid)
		return afb_evt_evtid_hooked_push(evtid, object);
	json_object_put(object);
	return 0;
}
#else
	__attribute__((alias("afb_evt_event_x2_unhooked_push")));
#endif

/** Instrumented function afb_evt_event_x2_unhooked_push(struct afb_event_x2*,struct json_object*) */
int afb_evt_event_x2_unhooked_push(struct afb_event_x2 *eventid, struct json_object *object)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_unhooked_push(struct afb_event_x2*,struct json_object*)");AKA_fCall++;
		AKA_mark("lis===1071###sois===26052###eois===26113###lif===2###soif===96###eoif===157###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_unhooked_push(struct afb_event_x2*,struct json_object*)");struct afb_evtid *evtid = afb_evt_event_x2_to_evtid(eventid);

		if (AKA_mark("lis===1072###sois===26119###eois===26124###lif===3###soif===163###eoif===168###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_unhooked_push(struct afb_event_x2*,struct json_object*)") && (AKA_mark("lis===1072###sois===26119###eois===26124###lif===3###soif===163###eoif===168###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_unhooked_push(struct afb_event_x2*,struct json_object*)")&&evtid)) {
		AKA_mark("lis===1073###sois===26128###eois===26169###lif===4###soif===172###eoif===213###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_unhooked_push(struct afb_event_x2*,struct json_object*)");return afb_evt_evtid_push(evtid, object);
	}
	else {AKA_mark("lis===-1072-###sois===-26119-###eois===-261195-###lif===-3-###soif===-###eoif===-168-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_unhooked_push(struct afb_event_x2*,struct json_object*)");}

		AKA_mark("lis===1074###sois===26171###eois===26195###lif===5###soif===215###eoif===239###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_unhooked_push(struct afb_event_x2*,struct json_object*)");json_object_put(object);

		AKA_mark("lis===1075###sois===26197###eois===26206###lif===6###soif===241###eoif===250###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_unhooked_push(struct afb_event_x2*,struct json_object*)");return 0;

}

#if WITH_LEGACY_BINDING_V1 || WITH_LEGACY_BINDING_V2
struct afb_event_x1 afb_evt_event_from_evtid(struct afb_evtid *evtid)
{
	return evtid
#if WITH_AFB_HOOK
		? (struct afb_event_x1){ .itf = &afb_evt_hooked_event_x2_itf, .closure = &evtid->eventid }
#else
		? (struct afb_event_x1){ .itf = &afb_evt_event_x2_itf, .closure = &evtid->eventid }
#endif
		: (struct afb_event_x1){ .itf = NULL, .closure = NULL };
}
#endif

/** Instrumented function afb_evt_event_x2_unref(struct afb_event_x2*) */
void afb_evt_event_x2_unref(struct afb_event_x2 *eventid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_unref(struct afb_event_x2*)");AKA_fCall++;
		AKA_mark("lis===1093###sois===26689###eois===26750###lif===2###soif===61###eoif===122###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_unref(struct afb_event_x2*)");struct afb_evtid *evtid = afb_evt_event_x2_to_evtid(eventid);

		if (AKA_mark("lis===1094###sois===26756###eois===26761###lif===3###soif===128###eoif===133###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_unref(struct afb_event_x2*)") && (AKA_mark("lis===1094###sois===26756###eois===26761###lif===3###soif===128###eoif===133###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_unref(struct afb_event_x2*)")&&evtid)) {
		AKA_mark("lis===1095###sois===26765###eois===26792###lif===4###soif===137###eoif===164###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_unref(struct afb_event_x2*)");afb_evt_evtid_unref(evtid);
	}
	else {AKA_mark("lis===-1094-###sois===-26756-###eois===-267565-###lif===-3-###soif===-###eoif===-133-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_unref(struct afb_event_x2*)");}

}

/** Instrumented function afb_evt_event_x2_addref(struct afb_event_x2*) */
struct afb_event_x2 *afb_evt_event_x2_addref(struct afb_event_x2 *eventid)
{AKA_mark("Calling: ./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_addref(struct afb_event_x2*)");AKA_fCall++;
		AKA_mark("lis===1100###sois===26874###eois===26935###lif===2###soif===78###eoif===139###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_addref(struct afb_event_x2*)");struct afb_evtid *evtid = afb_evt_event_x2_to_evtid(eventid);

		if (AKA_mark("lis===1101###sois===26941###eois===26946###lif===3###soif===145###eoif===150###ifc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_addref(struct afb_event_x2*)") && (AKA_mark("lis===1101###sois===26941###eois===26946###lif===3###soif===145###eoif===150###isc===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_addref(struct afb_event_x2*)")&&evtid)) {
		AKA_mark("lis===1102###sois===26950###eois===26978###lif===4###soif===154###eoif===182###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_addref(struct afb_event_x2*)");afb_evt_evtid_addref(evtid);
	}
	else {AKA_mark("lis===-1101-###sois===-26941-###eois===-269415-###lif===-3-###soif===-###eoif===-150-###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_addref(struct afb_event_x2*)");}

		AKA_mark("lis===1103###sois===26980###eois===26995###lif===5###soif===184###eoif===199###ins===true###function===./app-framework-binder/src/afb-evt.c/afb_evt_event_x2_addref(struct afb_event_x2*)");return eventid;

}


#endif

