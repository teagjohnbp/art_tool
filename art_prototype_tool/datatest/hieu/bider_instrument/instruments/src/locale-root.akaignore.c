/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_LOCALE_ROOT_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_LOCALE_ROOT_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 Copyright (C) 2015-2020 "IoT.bzh"

 author: José Bollo <jose.bollo@iot.bzh>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#include <limits.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__LOCALE_ROOT_H_
#define AKA_INCLUDE__LOCALE_ROOT_H_
#include "locale-root.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__SUBPATH_H_
#define AKA_INCLUDE__SUBPATH_H_
#include "subpath.akaignore.h"
#endif


/*
 * Implementation of folder based localisation as described here:
 *
 *    https://www.w3.org/TR/widgets/#folder-based-localization
 */

#define LRU_COUNT 3

static const char locales[] = "locales/";

struct locale_folder {
	struct locale_folder *parent;
	size_t length;
	char name[];
};

struct locale_container {
	size_t maxlength;
	size_t count;
	struct locale_folder **folders;
};

struct locale_search_node {
	struct locale_search_node *next;
	struct locale_folder *folder;
};

struct locale_root;

struct locale_search {
	struct locale_root *root;
	struct locale_search_node *head;
	int refcount;
	char definition[];
};

struct locale_root {
	int refcount;
	int intcount;
	int rootfd;
	struct locale_container container;
	struct locale_search *lru[LRU_COUNT];
	struct locale_search *default_search;
};

/*
 * Clear a container content
 */
/** Instrumented function clear_container(struct locale_container*) */
static void clear_container(struct locale_container *container)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/clear_container(struct locale_container*)");AKA_fCall++;
		while (AKA_mark("lis===87###sois===1849###eois===1865###lif===2###soif===73###eoif===89###ifc===true###function===./app-framework-binder/src/locale-root.c/clear_container(struct locale_container*)") && (AKA_mark("lis===87###sois===1849###eois===1865###lif===2###soif===73###eoif===89###isc===true###function===./app-framework-binder/src/locale-root.c/clear_container(struct locale_container*)")&&container->count)) {
		AKA_mark("lis===88###sois===1869###eois===1914###lif===3###soif===93###eoif===138###ins===true###function===./app-framework-binder/src/locale-root.c/clear_container(struct locale_container*)");free(container->folders[--container->count]);
	}

		AKA_mark("lis===89###sois===1916###eois===1941###lif===4###soif===140###eoif===165###ins===true###function===./app-framework-binder/src/locale-root.c/clear_container(struct locale_container*)");free(container->folders);

}

/*
 * Adds a folder of name for the container
 */
/** Instrumented function add_folder(struct locale_container*,const char*) */
static int add_folder(struct locale_container *container, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");AKA_fCall++;
		AKA_mark("lis===97###sois===2074###eois===2095###lif===2###soif===79###eoif===100###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");size_t count, length;

		AKA_mark("lis===98###sois===2097###eois===2128###lif===3###soif===102###eoif===133###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");struct locale_folder **folders;


		AKA_mark("lis===100###sois===2131###eois===2156###lif===5###soif===136###eoif===161###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");count = container->count;

		AKA_mark("lis===101###sois===2158###eois===2227###lif===6###soif===163###eoif===232###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");folders = realloc(container->folders, (1 + count) * sizeof *folders);

		if (AKA_mark("lis===102###sois===2233###eois===2248###lif===7###soif===238###eoif===253###ifc===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)") && (AKA_mark("lis===102###sois===2233###eois===2248###lif===7###soif===238###eoif===253###isc===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)")&&folders != NULL)) {
				AKA_mark("lis===103###sois===2254###eois===2283###lif===8###soif===259###eoif===288###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");container->folders = folders;

				AKA_mark("lis===104###sois===2286###eois===2308###lif===9###soif===291###eoif===313###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");length = strlen(name);

				AKA_mark("lis===105###sois===2311###eois===2366###lif===10###soif===316###eoif===371###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");folders[count] = malloc(sizeof **folders + 1 + length);

				if (AKA_mark("lis===106###sois===2373###eois===2395###lif===11###soif===378###eoif===400###ifc===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)") && (AKA_mark("lis===106###sois===2373###eois===2395###lif===11###soif===378###eoif===400###isc===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)")&&folders[count] != NULL)) {
						AKA_mark("lis===107###sois===2402###eois===2432###lif===12###soif===407###eoif===437###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");folders[count]->parent = NULL;

						AKA_mark("lis===108###sois===2436###eois===2468###lif===13###soif===441###eoif===473###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");folders[count]->length = length;

						AKA_mark("lis===109###sois===2472###eois===2519###lif===14###soif===477###eoif===524###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");memcpy(folders[count]->name, name, 1 + length);

						AKA_mark("lis===110###sois===2523###eois===2552###lif===15###soif===528###eoif===557###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");container->count = count + 1;

						if (AKA_mark("lis===111###sois===2560###eois===2589###lif===16###soif===565###eoif===594###ifc===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)") && (AKA_mark("lis===111###sois===2560###eois===2589###lif===16###soif===565###eoif===594###isc===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)")&&length > container->maxlength)) {
				AKA_mark("lis===112###sois===2595###eois===2625###lif===17###soif===600###eoif===630###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");container->maxlength = length;
			}
			else {AKA_mark("lis===-111-###sois===-2560-###eois===-256029-###lif===-16-###soif===-###eoif===-594-###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");}

						AKA_mark("lis===113###sois===2629###eois===2638###lif===18###soif===634###eoif===643###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");return 0;

		}
		else {AKA_mark("lis===-106-###sois===-2373-###eois===-237322-###lif===-11-###soif===-###eoif===-400-###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");}

	}
	else {AKA_mark("lis===-102-###sois===-2233-###eois===-223315-###lif===-7-###soif===-###eoif===-253-###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");}

		AKA_mark("lis===116###sois===2647###eois===2674###lif===21###soif===652###eoif===679###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");clear_container(container);

		AKA_mark("lis===117###sois===2676###eois===2691###lif===22###soif===681###eoif===696###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");errno = ENOMEM;

		AKA_mark("lis===118###sois===2693###eois===2703###lif===23###soif===698###eoif===708###ins===true###function===./app-framework-binder/src/locale-root.c/add_folder(struct locale_container*,const char*)");return -1;

}

/*
 * Compare two folders for qsort
 */
/** Instrumented function compare_folders_for_qsort(const void*,const void*) */
static int compare_folders_for_qsort(const void *a, const void *b)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/compare_folders_for_qsort(const void*,const void*)");AKA_fCall++;
		AKA_mark("lis===126###sois===2817###eois===2877###lif===2###soif===70###eoif===130###ins===true###function===./app-framework-binder/src/locale-root.c/compare_folders_for_qsort(const void*,const void*)");const struct locale_folder * const *fa = a, * const *fb = b;

		AKA_mark("lis===127###sois===2879###eois===2923###lif===3###soif===132###eoif===176###ins===true###function===./app-framework-binder/src/locale-root.c/compare_folders_for_qsort(const void*,const void*)");return strcasecmp((*fa)->name, (*fb)->name);

}

/*
 * Search for a folder
 */
/** Instrumented function search_folder(struct locale_container*,const char*,size_t) */
static struct locale_folder *search_folder(struct locale_container *container, const char *name, size_t length)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)");AKA_fCall++;
		AKA_mark("lis===135###sois===3072###eois===3094###lif===2###soif===115###eoif===137###ins===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)");size_t low, high, mid;

		AKA_mark("lis===136###sois===3096###eois===3120###lif===3###soif===139###eoif===163###ins===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)");struct locale_folder *f;

		AKA_mark("lis===137###sois===3122###eois===3128###lif===4###soif===165###eoif===171###ins===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)");int c;


		AKA_mark("lis===139###sois===3131###eois===3139###lif===6###soif===174###eoif===182###ins===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)");low = 0;

		AKA_mark("lis===140###sois===3141###eois===3165###lif===7###soif===184###eoif===208###ins===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)");high = container->count;

		while (AKA_mark("lis===141###sois===3174###eois===3184###lif===8###soif===217###eoif===227###ifc===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)") && (AKA_mark("lis===141###sois===3174###eois===3184###lif===8###soif===217###eoif===227###isc===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)")&&low < high)) {
				AKA_mark("lis===142###sois===3190###eois===3214###lif===9###soif===233###eoif===257###ins===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)");mid = (low + high) >> 1;

				AKA_mark("lis===143###sois===3217###eois===3245###lif===10###soif===260###eoif===288###ins===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)");f = container->folders[mid];

				AKA_mark("lis===144###sois===3248###eois===3287###lif===11###soif===291###eoif===330###ins===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)");c = strncasecmp(f->name, name, length);

				if (AKA_mark("lis===145###sois===3294###eois===3324###lif===12###soif===337###eoif===367###ifc===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)") && ((AKA_mark("lis===145###sois===3294###eois===3300###lif===12###soif===337###eoif===343###isc===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)")&&c == 0)	&&(AKA_mark("lis===145###sois===3304###eois===3324###lif===12###soif===347###eoif===367###isc===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)")&&f->name[length] == 0))) {
			AKA_mark("lis===146###sois===3329###eois===3338###lif===13###soif===372###eoif===381###ins===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)");return f;
		}
		else {AKA_mark("lis===-145-###sois===-3294-###eois===-329430-###lif===-12-###soif===-###eoif===-367-###ins===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)");}

				if (AKA_mark("lis===147###sois===3345###eois===3351###lif===14###soif===388###eoif===394###ifc===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)") && (AKA_mark("lis===147###sois===3345###eois===3351###lif===14###soif===388###eoif===394###isc===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)")&&c >= 0)) {
			AKA_mark("lis===148###sois===3356###eois===3367###lif===15###soif===399###eoif===410###ins===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)");high = mid;
		}
		else {
			AKA_mark("lis===150###sois===3378###eois===3392###lif===17###soif===421###eoif===435###ins===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)");low = mid + 1;
		}

	}

		AKA_mark("lis===152###sois===3397###eois===3409###lif===19###soif===440###eoif===452###ins===true###function===./app-framework-binder/src/locale-root.c/search_folder(struct locale_container*,const char*,size_t)");return NULL;

}

/*
 * Init a container
 */
/** Instrumented function init_container(struct locale_container*,int) */
static int init_container(struct locale_container *container, int dirfd)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");AKA_fCall++;
		AKA_mark("lis===160###sois===3516###eois===3532###lif===2###soif===76###eoif===92###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");int rc = 0, sfd;

		AKA_mark("lis===161###sois===3534###eois===3543###lif===3###soif===94###eoif===103###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");DIR *dir;

		AKA_mark("lis===162###sois===3545###eois===3565###lif===4###soif===105###eoif===125###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");struct dirent *dent;

		AKA_mark("lis===163###sois===3567###eois===3582###lif===5###soif===127###eoif===142###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");struct stat st;

		AKA_mark("lis===164###sois===3584###eois===3596###lif===6###soif===144###eoif===156###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");size_t i, j;

		AKA_mark("lis===165###sois===3598###eois===3622###lif===7###soif===158###eoif===182###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");struct locale_folder *f;


	/* init the container */
		AKA_mark("lis===168###sois===3651###eois===3676###lif===10###soif===211###eoif===236###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");container->maxlength = 0;

		AKA_mark("lis===169###sois===3678###eois===3699###lif===11###soif===238###eoif===259###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");container->count = 0;

		AKA_mark("lis===170###sois===3701###eois===3727###lif===12###soif===261###eoif===287###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");container->folders = NULL;


	/* scan the subdirs */
		AKA_mark("lis===173###sois===3754###eois===3805###lif===15###soif===314###eoif===365###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");sfd = openat(dirfd, locales, O_DIRECTORY|O_RDONLY);

		if (AKA_mark("lis===174###sois===3811###eois===3820###lif===16###soif===371###eoif===380###ifc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)") && (AKA_mark("lis===174###sois===3811###eois===3820###lif===16###soif===371###eoif===380###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&sfd == -1)) {
		AKA_mark("lis===175###sois===3824###eois===3853###lif===17###soif===384###eoif===413###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");return (errno == ENOENT) - 1;
	}
	else {AKA_mark("lis===-174-###sois===-3811-###eois===-38119-###lif===-16-###soif===-###eoif===-380-###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");}


	/* get the directory data */
		AKA_mark("lis===178###sois===3886###eois===3907###lif===20###soif===446###eoif===467###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");dir = fdopendir(sfd);

		if (AKA_mark("lis===179###sois===3913###eois===3924###lif===21###soif===473###eoif===484###ifc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)") && (AKA_mark("lis===179###sois===3913###eois===3924###lif===21###soif===473###eoif===484###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&dir == NULL)) {
				AKA_mark("lis===180###sois===3930###eois===3941###lif===22###soif===490###eoif===501###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");close(sfd);

				AKA_mark("lis===181###sois===3944###eois===3954###lif===23###soif===504###eoif===514###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");return -1;

	}
	else {AKA_mark("lis===-179-###sois===-3913-###eois===-391311-###lif===-21-###soif===-###eoif===-484-###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");}


	/* enumerate the entries */
		for (;;) {
		/* next entry */
				AKA_mark("lis===187###sois===4020###eois===4030###lif===29###soif===580###eoif===590###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");errno = 0;

				AKA_mark("lis===188###sois===4033###eois===4053###lif===30###soif===593###eoif===613###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");dent = readdir(dir);

				if (AKA_mark("lis===189###sois===4060###eois===4072###lif===31###soif===620###eoif===632###ifc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)") && (AKA_mark("lis===189###sois===4060###eois===4072###lif===31###soif===620###eoif===632###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&dent == NULL)) {
			/* end of entries */
						AKA_mark("lis===191###sois===4103###eois===4117###lif===33###soif===663###eoif===677###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");closedir(dir);

						if (AKA_mark("lis===192###sois===4125###eois===4135###lif===34###soif===685###eoif===695###ifc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)") && (AKA_mark("lis===192###sois===4125###eois===4135###lif===34###soif===685###eoif===695###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&errno != 0)) {
				AKA_mark("lis===193###sois===4141###eois===4151###lif===35###soif===701###eoif===711###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");return -1;
			}
			else {AKA_mark("lis===-192-###sois===-4125-###eois===-412510-###lif===-34-###soif===-###eoif===-695-###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");}

						AKA_mark("lis===194###sois===4155###eois===4161###lif===36###soif===715###eoif===721###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");break;

		}
		else {AKA_mark("lis===-189-###sois===-4060-###eois===-406012-###lif===-31-###soif===-###eoif===-632-###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");}

				AKA_mark("lis===196###sois===4168###eois===4208###lif===38###soif===728###eoif===768###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");rc = fstatat(sfd, dent->d_name, &st, 0);

				if (AKA_mark("lis===197###sois===4215###eois===4245###lif===39###soif===775###eoif===805###ifc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)") && ((AKA_mark("lis===197###sois===4215###eois===4222###lif===39###soif===775###eoif===782###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&rc == 0)	&&(AKA_mark("lis===197###sois===4226###eois===4245###lif===39###soif===786###eoif===805###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&S_ISDIR(st.st_mode)))) {
			/* directory aka folder */
						if (AKA_mark("lis===199###sois===4286###eois===4386###lif===41###soif===846###eoif===946###ifc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)") && ((AKA_mark("lis===199###sois===4286###eois===4308###lif===41###soif===846###eoif===868###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&dent->d_name[0] == '.')	&&((AKA_mark("lis===199###sois===4313###eois===4333###lif===41###soif===873###eoif===893###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&dent->d_name[1] == 0)	||((AKA_mark("lis===199###sois===4338###eois===4360###lif===41###soif===898###eoif===920###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&dent->d_name[1] == '.')	&&(AKA_mark("lis===199###sois===4364###eois===4384###lif===41###soif===924###eoif===944###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&dent->d_name[2] == 0))))) {AKA_mark("lis===+199+###sois===+4286+###eois===+4286100+###lif===+41+###soif===+###eoif===+946+###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");}
			else {
								AKA_mark("lis===202###sois===4491###eois===4532###lif===44###soif===1051###eoif===1092###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");rc = add_folder(container, dent->d_name);

								if (AKA_mark("lis===203###sois===4541###eois===4547###lif===45###soif===1101###eoif===1107###ifc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)") && (AKA_mark("lis===203###sois===4541###eois===4547###lif===45###soif===1101###eoif===1107###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&rc < 0)) {
										AKA_mark("lis===204###sois===4556###eois===4570###lif===46###soif===1116###eoif===1130###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");closedir(dir);

										AKA_mark("lis===205###sois===4576###eois===4586###lif===47###soif===1136###eoif===1146###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");return rc;

				}
				else {AKA_mark("lis===-203-###sois===-4541-###eois===-45416-###lif===-45-###soif===-###eoif===-1107-###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");}

			}

		}
		else {AKA_mark("lis===-197-###sois===-4215-###eois===-421530-###lif===-39-###soif===-###eoif===-805-###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");}

	}


	/* sort the folders */
		if (AKA_mark("lis===212###sois===4635###eois===4651###lif===54###soif===1195###eoif===1211###ifc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)") && (AKA_mark("lis===212###sois===4635###eois===4651###lif===54###soif===1195###eoif===1211###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&container->count)) {
		AKA_mark("lis===213###sois===4655###eois===4754###lif===55###soif===1215###eoif===1314###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");qsort(container->folders, container->count, sizeof *container->folders, compare_folders_for_qsort);
	}
	else {AKA_mark("lis===-212-###sois===-4635-###eois===-463516-###lif===-54-###soif===-###eoif===-1211-###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");}


	/* build the parents links */
		AKA_mark("lis===216###sois===4788###eois===4809###lif===58###soif===1348###eoif===1369###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");i = container->count;

		while (AKA_mark("lis===217###sois===4818###eois===4824###lif===59###soif===1378###eoif===1384###ifc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)") && (AKA_mark("lis===217###sois===4818###eois===4824###lif===59###soif===1378###eoif===1384###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&i != 0)) {
				AKA_mark("lis===218###sois===4830###eois===4858###lif===60###soif===1390###eoif===1418###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");f = container->folders[--i];

				AKA_mark("lis===219###sois===4861###eois===4881###lif===61###soif===1421###eoif===1441###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");j = strlen(f->name);

				while (AKA_mark("lis===220###sois===4891###eois===4918###lif===62###soif===1451###eoif===1478###ifc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)") && ((AKA_mark("lis===220###sois===4891###eois===4897###lif===62###soif===1451###eoif===1457###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&j != 0)	&&(AKA_mark("lis===220###sois===4901###eois===4918###lif===62###soif===1461###eoif===1478###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&f->parent == NULL))) {
						if (AKA_mark("lis===221###sois===4929###eois===4948###lif===63###soif===1489###eoif===1508###ifc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)") && (AKA_mark("lis===221###sois===4929###eois===4948###lif===63###soif===1489###eoif===1508###isc===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)")&&f->name[--j] == '-')) {
				AKA_mark("lis===222###sois===4954###eois===5003###lif===64###soif===1514###eoif===1563###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");f->parent = search_folder(container, f->name, j);
			}
			else {AKA_mark("lis===-221-###sois===-4929-###eois===-492919-###lif===-63-###soif===-###eoif===-1508-###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");}

		}

	}


		AKA_mark("lis===226###sois===5013###eois===5023###lif===68###soif===1573###eoif===1583###ins===true###function===./app-framework-binder/src/locale-root.c/init_container(struct locale_container*,int)");return rc;

}

/*
 * Creates a locale root handler and returns it or return NULL
 * in case of memory depletion.
 */
/** Instrumented function locale_root_create(int) */
struct locale_root *locale_root_create(int dirfd)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/locale_root_create(int)");AKA_fCall++;
		AKA_mark("lis===235###sois===5182###eois===5207###lif===2###soif===53###eoif===78###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");struct locale_root *root;

		AKA_mark("lis===236###sois===5209###eois===5218###lif===3###soif===80###eoif===89###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");size_t i;


		AKA_mark("lis===238###sois===5221###eois===5253###lif===5###soif===92###eoif===124###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");root = calloc(1, sizeof * root);

		if (AKA_mark("lis===239###sois===5259###eois===5271###lif===6###soif===130###eoif===142###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)") && (AKA_mark("lis===239###sois===5259###eois===5271###lif===6###soif===130###eoif===142###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)")&&root == NULL)) {
		AKA_mark("lis===240###sois===5275###eois===5290###lif===7###soif===146###eoif===161###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");errno = ENOMEM;
	}
	else {
				if (AKA_mark("lis===242###sois===5305###eois===5349###lif===9###soif===176###eoif===220###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)") && (AKA_mark("lis===242###sois===5305###eois===5349###lif===9###soif===176###eoif===220###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)")&&init_container(&root->container, dirfd) == 0)) {
						AKA_mark("lis===243###sois===5356###eois===5377###lif===10###soif===227###eoif===248###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");root->rootfd = dirfd;

						AKA_mark("lis===244###sois===5381###eois===5400###lif===11###soif===252###eoif===271###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");root->refcount = 1;

						AKA_mark("lis===245###sois===5404###eois===5423###lif===12###soif===275###eoif===294###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");root->intcount = 1;

						AKA_mark("lis===246###sois===5431###eois===5438###lif===13###soif===302###eoif===309###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");for (i = 0 ;AKA_mark("lis===246###sois===5439###eois===5452###lif===13###soif===310###eoif===323###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)") && AKA_mark("lis===246###sois===5439###eois===5452###lif===13###soif===310###eoif===323###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)")&&i < LRU_COUNT;({AKA_mark("lis===246###sois===5455###eois===5458###lif===13###soif===326###eoif===329###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");i++;})) {
				AKA_mark("lis===247###sois===5464###eois===5484###lif===14###soif===335###eoif===355###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");root->lru[i] = NULL;
			}

						AKA_mark("lis===248###sois===5488###eois===5516###lif===15###soif===359###eoif===387###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");root->default_search = NULL;

						AKA_mark("lis===249###sois===5520###eois===5532###lif===16###soif===391###eoif===403###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");return root;

		}
		else {AKA_mark("lis===-242-###sois===-5305-###eois===-530544-###lif===-9-###soif===-###eoif===-220-###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");}

				AKA_mark("lis===251###sois===5539###eois===5550###lif===18###soif===410###eoif===421###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");free(root);

	}

		AKA_mark("lis===253###sois===5555###eois===5567###lif===20###soif===426###eoif===438###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create(int)");return NULL;

}

/*
 * Creates a locale root handler and returns it or return NULL
 * in case of memory depletion.
 */
/** Instrumented function locale_root_create_at(int,const char*) */
struct locale_root *locale_root_create_at(int dirfd, const char *path)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/locale_root_create_at(int,const char*)");AKA_fCall++;
		AKA_mark("lis===262###sois===5747###eois===5754###lif===2###soif===74###eoif===81###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create_at(int,const char*)");int fd;

		AKA_mark("lis===263###sois===5756###eois===5781###lif===3###soif===83###eoif===108###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create_at(int,const char*)");struct locale_root *root;


		AKA_mark("lis===265###sois===5784###eois===5829###lif===5###soif===111###eoif===156###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create_at(int,const char*)");fd = openat(dirfd, path, O_PATH|O_DIRECTORY);

		if (AKA_mark("lis===266###sois===5835###eois===5841###lif===6###soif===162###eoif===168###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_create_at(int,const char*)") && (AKA_mark("lis===266###sois===5835###eois===5841###lif===6###soif===162###eoif===168###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_create_at(int,const char*)")&&fd < 0)) {
		AKA_mark("lis===267###sois===5845###eois===5858###lif===7###soif===172###eoif===185###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create_at(int,const char*)");root =  NULL;
	}
	else {
				AKA_mark("lis===269###sois===5869###eois===5899###lif===9###soif===196###eoif===226###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create_at(int,const char*)");root = locale_root_create(fd);

				if (AKA_mark("lis===270###sois===5906###eois===5918###lif===10###soif===233###eoif===245###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_create_at(int,const char*)") && (AKA_mark("lis===270###sois===5906###eois===5918###lif===10###soif===233###eoif===245###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_create_at(int,const char*)")&&root == NULL)) {
			AKA_mark("lis===271###sois===5923###eois===5933###lif===11###soif===250###eoif===260###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create_at(int,const char*)");close(fd);
		}
		else {AKA_mark("lis===-270-###sois===-5906-###eois===-590612-###lif===-10-###soif===-###eoif===-245-###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create_at(int,const char*)");}

	}

		AKA_mark("lis===273###sois===5938###eois===5950###lif===13###soif===265###eoif===277###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_create_at(int,const char*)");return root;

}

/*
 * Adds a reference to 'root'
 */
/** Instrumented function locale_root_addref(struct locale_root*) */
struct locale_root *locale_root_addref(struct locale_root *root)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/locale_root_addref(struct locale_root*)");AKA_fCall++;
		AKA_mark("lis===281###sois===6059###eois===6116###lif===2###soif===68###eoif===125###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_addref(struct locale_root*)");__atomic_add_fetch(&root->refcount, 1, __ATOMIC_RELAXED);

		AKA_mark("lis===282###sois===6118###eois===6130###lif===3###soif===127###eoif===139###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_addref(struct locale_root*)");return root;

}

/*
 * Drops an internal reference to 'root' and destroys it
 * if not more referenced
 */
/** Instrumented function internal_unref(struct locale_root*) */
static void internal_unref(struct locale_root *root)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/internal_unref(struct locale_root*)");AKA_fCall++;
		if (AKA_mark("lis===291###sois===6284###eois===6341###lif===2###soif===60###eoif===117###ifc===true###function===./app-framework-binder/src/locale-root.c/internal_unref(struct locale_root*)") && (AKA_mark("lis===291###sois===6284###eois===6341###lif===2###soif===60###eoif===117###isc===true###function===./app-framework-binder/src/locale-root.c/internal_unref(struct locale_root*)")&&!__atomic_sub_fetch(&root->intcount, 1, __ATOMIC_RELAXED))) {
				AKA_mark("lis===292###sois===6347###eois===6381###lif===3###soif===123###eoif===157###ins===true###function===./app-framework-binder/src/locale-root.c/internal_unref(struct locale_root*)");clear_container(&root->container);

				AKA_mark("lis===293###sois===6384###eois===6404###lif===4###soif===160###eoif===180###ins===true###function===./app-framework-binder/src/locale-root.c/internal_unref(struct locale_root*)");close(root->rootfd);

				AKA_mark("lis===294###sois===6407###eois===6418###lif===5###soif===183###eoif===194###ins===true###function===./app-framework-binder/src/locale-root.c/internal_unref(struct locale_root*)");free(root);

	}
	else {AKA_mark("lis===-291-###sois===-6284-###eois===-628457-###lif===-2-###soif===-###eoif===-117-###ins===true###function===./app-framework-binder/src/locale-root.c/internal_unref(struct locale_root*)");}

}

/*
 * Drops a reference to 'root' and destroys it
 * if not more referenced
 */
/** Instrumented function locale_root_unref(struct locale_root*) */
void locale_root_unref(struct locale_root *root)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/locale_root_unref(struct locale_root*)");AKA_fCall++;
		AKA_mark("lis===304###sois===6557###eois===6566###lif===2###soif===52###eoif===61###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_unref(struct locale_root*)");size_t i;


		if (AKA_mark("lis===306###sois===6573###eois===6638###lif===4###soif===68###eoif===133###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_unref(struct locale_root*)") && ((AKA_mark("lis===306###sois===6573###eois===6577###lif===4###soif===68###eoif===72###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_unref(struct locale_root*)")&&root)	&&(AKA_mark("lis===306###sois===6581###eois===6638###lif===4###soif===76###eoif===133###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_unref(struct locale_root*)")&&!__atomic_sub_fetch(&root->refcount, 1, __ATOMIC_RELAXED)))) {
		/* clear circular references through searchs */
				AKA_mark("lis===308###sois===6699###eois===6706###lif===6###soif===194###eoif===201###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_unref(struct locale_root*)");for (i = 0 ;AKA_mark("lis===308###sois===6707###eois===6720###lif===6###soif===202###eoif===215###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_unref(struct locale_root*)") && AKA_mark("lis===308###sois===6707###eois===6720###lif===6###soif===202###eoif===215###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_unref(struct locale_root*)")&&i < LRU_COUNT;({AKA_mark("lis===308###sois===6723###eois===6726###lif===6###soif===218###eoif===221###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_unref(struct locale_root*)");i++;})) {
			AKA_mark("lis===309###sois===6731###eois===6765###lif===7###soif===226###eoif===260###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_unref(struct locale_root*)");locale_search_unref(root->lru[i]);
		}

		/* finalize if needed */
				AKA_mark("lis===311###sois===6795###eois===6816###lif===9###soif===290###eoif===311###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_unref(struct locale_root*)");internal_unref(root);

	}
	else {AKA_mark("lis===-306-###sois===-6573-###eois===-657365-###lif===-4-###soif===-###eoif===-133-###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_unref(struct locale_root*)");}

}

/*
 * Get the filedescriptor for the 'root' directory
 */
/** Instrumented function locale_root_get_dirfd(struct locale_root*) */
int locale_root_get_dirfd(struct locale_root *root)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/locale_root_get_dirfd(struct locale_root*)");AKA_fCall++;
		AKA_mark("lis===320###sois===6936###eois===6956###lif===2###soif===55###eoif===75###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_get_dirfd(struct locale_root*)");return root->rootfd;

}

/*
 * append, if needed, a folder to the search
 */
/** Instrumented function search_append_folder(struct locale_search*,struct locale_folder*) */
static int search_append_folder(struct locale_search *search, struct locale_folder *folder)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");AKA_fCall++;
		AKA_mark("lis===328###sois===7107###eois===7141###lif===2###soif===95###eoif===129###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");struct locale_search_node **p, *n;


	/* search an existing node */
		AKA_mark("lis===331###sois===7175###eois===7193###lif===5###soif===163###eoif===181###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");p = &search->head;

		AKA_mark("lis===332###sois===7195###eois===7212###lif===6###soif===183###eoif===200###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");n = search->head;

		while (AKA_mark("lis===333###sois===7220###eois===7229###lif===7###soif===208###eoif===217###ifc===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)") && (AKA_mark("lis===333###sois===7220###eois===7229###lif===7###soif===208###eoif===217###isc===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)")&&n != NULL)) {
				if (AKA_mark("lis===334###sois===7239###eois===7258###lif===8###soif===227###eoif===246###ifc===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)") && (AKA_mark("lis===334###sois===7239###eois===7258###lif===8###soif===227###eoif===246###isc===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)")&&n->folder == folder)) {
			AKA_mark("lis===335###sois===7263###eois===7272###lif===9###soif===251###eoif===260###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");return 0;
		}
		else {AKA_mark("lis===-334-###sois===-7239-###eois===-723919-###lif===-8-###soif===-###eoif===-246-###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");}

				AKA_mark("lis===336###sois===7275###eois===7288###lif===10###soif===263###eoif===276###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");p = &n->next;

				AKA_mark("lis===337###sois===7291###eois===7303###lif===11###soif===279###eoif===291###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");n = n->next;

	}


	/* allocates a new node */
		AKA_mark("lis===341###sois===7337###eois===7359###lif===15###soif===325###eoif===347###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");n = malloc(sizeof *n);

		if (AKA_mark("lis===342###sois===7365###eois===7374###lif===16###soif===353###eoif===362###ifc===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)") && (AKA_mark("lis===342###sois===7365###eois===7374###lif===16###soif===353###eoif===362###isc===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)")&&n == NULL)) {
				AKA_mark("lis===343###sois===7380###eois===7395###lif===17###soif===368###eoif===383###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");errno = ENOMEM;

				AKA_mark("lis===344###sois===7398###eois===7408###lif===18###soif===386###eoif===396###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");return -1;

	}
	else {AKA_mark("lis===-342-###sois===-7365-###eois===-73659-###lif===-16-###soif===-###eoif===-362-###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");}


	/* init the node */
		AKA_mark("lis===348###sois===7435###eois===7442###lif===22###soif===423###eoif===430###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");*p = n;

		AKA_mark("lis===349###sois===7444###eois===7463###lif===23###soif===432###eoif===451###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");n->folder = folder;

		AKA_mark("lis===350###sois===7465###eois===7480###lif===24###soif===453###eoif===468###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");n->next = NULL;

		AKA_mark("lis===351###sois===7482###eois===7491###lif===25###soif===470###eoif===479###ins===true###function===./app-framework-binder/src/locale-root.c/search_append_folder(struct locale_search*,struct locale_folder*)");return 1;

}

/*
 * construct a search for the given root and definition of length
 */
/** Instrumented function create_search(struct locale_root*,const char*,size_t,int) */
static struct locale_search *create_search(struct locale_root *root, const char *definition, size_t length, int immediate)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");AKA_fCall++;
		AKA_mark("lis===359###sois===7694###eois===7723###lif===2###soif===126###eoif===155###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");struct locale_search *search;

		AKA_mark("lis===360###sois===7725###eois===7743###lif===3###soif===157###eoif===175###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");size_t stop, back;

		AKA_mark("lis===361###sois===7745###eois===7774###lif===4###soif===177###eoif===206###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");struct locale_folder *folder;

		AKA_mark("lis===362###sois===7776###eois===7808###lif===5###soif===208###eoif===240###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");struct locale_search_node *node;


	/* allocate the structure */
		AKA_mark("lis===365###sois===7841###eois===7886###lif===8###soif===273###eoif===318###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");search = malloc(sizeof *search + 1 + length);

		if (AKA_mark("lis===366###sois===7892###eois===7906###lif===9###soif===324###eoif===338###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && (AKA_mark("lis===366###sois===7892###eois===7906###lif===9###soif===324###eoif===338###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&search == NULL)) {
				AKA_mark("lis===367###sois===7912###eois===7927###lif===10###soif===344###eoif===359###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");errno = ENOMEM;

	}
	else {
		/* init */
				AKA_mark("lis===370###sois===7953###eois===8010###lif===13###soif===385###eoif===442###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");__atomic_add_fetch(&root->intcount, 1, __ATOMIC_RELAXED);

				AKA_mark("lis===371###sois===8013###eois===8033###lif===14###soif===445###eoif===465###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");search->root = root;

				AKA_mark("lis===372###sois===8036###eois===8056###lif===15###soif===468###eoif===488###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");search->head = NULL;

				AKA_mark("lis===373###sois===8059###eois===8080###lif===16###soif===491###eoif===512###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");search->refcount = 1;

				AKA_mark("lis===374###sois===8083###eois===8130###lif===17###soif===515###eoif===562###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");memcpy(search->definition, definition, length);

				AKA_mark("lis===375###sois===8133###eois===8164###lif===18###soif===565###eoif===596###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");search->definition[length] = 0;


		/* build the search from the definition */
				while (AKA_mark("lis===378###sois===8219###eois===8229###lif===21###soif===651###eoif===661###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && (AKA_mark("lis===378###sois===8219###eois===8229###lif===21###soif===651###eoif===661###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&length > 0)) {
						AKA_mark("lis===379###sois===8236###eois===8245###lif===22###soif===668###eoif===677###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");stop = 0;

						while (AKA_mark("lis===380###sois===8255###eois===8322###lif===23###soif===687###eoif===754###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && (((AKA_mark("lis===380###sois===8255###eois===8268###lif===23###soif===687###eoif===700###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&stop < length)	&&(AKA_mark("lis===380###sois===8272###eois===8295###lif===23###soif===704###eoif===727###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&definition[stop] != ','))	&&(AKA_mark("lis===380###sois===8299###eois===8322###lif===23###soif===731###eoif===754###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&definition[stop] != ';'))) {
				AKA_mark("lis===381###sois===8328###eois===8335###lif===24###soif===760###eoif===767###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");stop++;
			}

						AKA_mark("lis===382###sois===8339###eois===8351###lif===25###soif===771###eoif===783###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");back = stop;

						while (AKA_mark("lis===383###sois===8362###eois===8427###lif===26###soif===794###eoif===859###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && ((AKA_mark("lis===383###sois===8362###eois===8370###lif===26###soif===794###eoif===802###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&back > 0)	&&((AKA_mark("lis===383###sois===8375###eois===8398###lif===26###soif===807###eoif===830###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&definition[back] == ' ')	||(AKA_mark("lis===383###sois===8402###eois===8426###lif===26###soif===834###eoif===858###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&definition[back] == '\t')))) {
				AKA_mark("lis===384###sois===8433###eois===8440###lif===27###soif===865###eoif===872###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");back--;
			}

						while (AKA_mark("lis===385###sois===8451###eois===8459###lif===28###soif===883###eoif===891###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && (AKA_mark("lis===385###sois===8451###eois===8459###lif===28###soif===883###eoif===891###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&back > 0)) {
								AKA_mark("lis===386###sois===8467###eois===8526###lif===29###soif===899###eoif===958###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");folder = search_folder(&root->container, definition, back);

								if (AKA_mark("lis===387###sois===8535###eois===8541###lif===30###soif===967###eoif===973###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && (AKA_mark("lis===387###sois===8535###eois===8541###lif===30###soif===967###eoif===973###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&folder)) {
										if (AKA_mark("lis===388###sois===8554###eois===8594###lif===31###soif===986###eoif===1026###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && (AKA_mark("lis===388###sois===8554###eois===8594###lif===31###soif===986###eoif===1026###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&search_append_folder(search, folder) < 0)) {
												AKA_mark("lis===389###sois===8604###eois===8632###lif===32###soif===1036###eoif===1064###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");locale_search_unref(search);

												AKA_mark("lis===390###sois===8639###eois===8651###lif===33###soif===1071###eoif===1083###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");return NULL;

					}
					else {AKA_mark("lis===-388-###sois===-8554-###eois===-855440-###lif===-31-###soif===-###eoif===-1026-###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");}

										if (AKA_mark("lis===392###sois===8668###eois===8678###lif===35###soif===1100###eoif===1110###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && (AKA_mark("lis===392###sois===8668###eois===8678###lif===35###soif===1100###eoif===1110###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&!immediate)) {
						AKA_mark("lis===393###sois===8686###eois===8692###lif===36###soif===1118###eoif===1124###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");break;
					}
					else {AKA_mark("lis===-392-###sois===-8668-###eois===-866810-###lif===-35-###soif===-###eoif===-1110-###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");}

				}
				else {AKA_mark("lis===-387-###sois===-8535-###eois===-85356-###lif===-30-###soif===-###eoif===-973-###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");}

								while (AKA_mark("lis===395###sois===8709###eois===8746###lif===38###soif===1141###eoif===1178###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && ((AKA_mark("lis===395###sois===8709###eois===8717###lif===38###soif===1141###eoif===1149###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&back > 0)	&&(AKA_mark("lis===395###sois===8721###eois===8746###lif===38###soif===1153###eoif===1178###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&definition[--back] != '-'))) {
					;
				}

								while (AKA_mark("lis===396###sois===8759###eois===8823###lif===39###soif===1191###eoif===1255###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && (((AKA_mark("lis===396###sois===8759###eois===8767###lif===39###soif===1191###eoif===1199###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&back > 0)	&&(AKA_mark("lis===396###sois===8771###eois===8794###lif===39###soif===1203###eoif===1226###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&definition[back] == '-'))	&&(AKA_mark("lis===396###sois===8798###eois===8823###lif===39###soif===1230###eoif===1255###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&definition[back-1] == '-'))) {
					AKA_mark("lis===396###sois===8825###eois===8832###lif===39###soif===1257###eoif===1264###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");back--;
				}

			}

						while (AKA_mark("lis===398###sois===8848###eois===8888###lif===41###soif===1280###eoif===1320###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && ((AKA_mark("lis===398###sois===8848###eois===8861###lif===41###soif===1280###eoif===1293###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&stop < length)	&&(AKA_mark("lis===398###sois===8865###eois===8888###lif===41###soif===1297###eoif===1320###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&definition[stop] != ','))) {
				AKA_mark("lis===399###sois===8894###eois===8901###lif===42###soif===1326###eoif===1333###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");stop++;
			}

						while (AKA_mark("lis===400###sois===8912###eois===9009###lif===43###soif===1344###eoif===1441###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && ((AKA_mark("lis===400###sois===8912###eois===8925###lif===43###soif===1344###eoif===1357###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&stop < length)	&&(((AKA_mark("lis===400###sois===8930###eois===8953###lif===43###soif===1362###eoif===1385###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&definition[stop] == ',')	||(AKA_mark("lis===400###sois===8957###eois===8980###lif===43###soif===1389###eoif===1412###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&definition[stop] == ' '))	||(AKA_mark("lis===400###sois===8984###eois===9008###lif===43###soif===1416###eoif===1440###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&definition[stop] == '\t')))) {
				AKA_mark("lis===401###sois===9015###eois===9022###lif===44###soif===1447###eoif===1454###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");stop++;
			}

						AKA_mark("lis===402###sois===9026###eois===9045###lif===45###soif===1458###eoif===1477###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");definition += stop;

						AKA_mark("lis===403###sois===9049###eois===9064###lif===46###soif===1481###eoif===1496###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");length -= stop;

		}


		/* fullfills the search */
				AKA_mark("lis===407###sois===9101###eois===9121###lif===50###soif===1533###eoif===1553###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");node = search->head;

				while (AKA_mark("lis===408###sois===9130###eois===9142###lif===51###soif===1562###eoif===1574###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && (AKA_mark("lis===408###sois===9130###eois===9142###lif===51###soif===1562###eoif===1574###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&node != NULL)) {
						AKA_mark("lis===409###sois===9149###eois===9179###lif===52###soif===1581###eoif===1611###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");folder = node->folder->parent;

						if (AKA_mark("lis===410###sois===9187###eois===9201###lif===53###soif===1619###eoif===1633###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && (AKA_mark("lis===410###sois===9187###eois===9201###lif===53###soif===1619###eoif===1633###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&folder != NULL)) {
								if (AKA_mark("lis===411###sois===9213###eois===9267###lif===54###soif===1645###eoif===1699###ifc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)") && (AKA_mark("lis===411###sois===9213###eois===9267###lif===54###soif===1645###eoif===1699###isc===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)")&&search_append_folder(search, node->folder->parent) < 0)) {
										AKA_mark("lis===412###sois===9276###eois===9304###lif===55###soif===1708###eoif===1736###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");locale_search_unref(search);

										AKA_mark("lis===413###sois===9310###eois===9322###lif===56###soif===1742###eoif===1754###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");return NULL;

				}
				else {AKA_mark("lis===-411-###sois===-9213-###eois===-921354-###lif===-54-###soif===-###eoif===-1699-###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");}

			}
			else {AKA_mark("lis===-410-###sois===-9187-###eois===-918714-###lif===-53-###soif===-###eoif===-1633-###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");}

						AKA_mark("lis===416###sois===9337###eois===9355###lif===59###soif===1769###eoif===1787###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");node = node->next;

		}

	}

		AKA_mark("lis===419###sois===9364###eois===9378###lif===62###soif===1796###eoif===1810###ins===true###function===./app-framework-binder/src/locale-root.c/create_search(struct locale_root*,const char*,size_t,int)");return search;

}

/*
 * Check if a possibly NUUL search matches the definition of length
 */
/** Instrumented function search_matches(struct locale_search*,const char*,size_t) */
static inline int search_matches(struct locale_search *search, const char *definition, size_t length)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/search_matches(struct locale_search*,const char*,size_t)");AKA_fCall++;
		AKA_mark("lis===427###sois===9562###eois===9682###lif===2###soif===105###eoif===225###ins===true###function===./app-framework-binder/src/locale-root.c/search_matches(struct locale_search*,const char*,size_t)");return search != NULL && strncasecmp(search->definition, definition, length) == 0 && search->definition[length] == '\0';

}

/*
 * Get an instance of search for the given root and definition
 * The flag immediate affects how the search is built.
 * For example, if the definition is "en-US,en-GB,en", the result differs depending on
 * immediate or not:
 *  when immediate==0 the search becomes "en-US,en-GB,en"
 *  when immediate==1 the search becomes "en-US,en,en-GB" because en-US is immediately downgraded to en
 */
/** Instrumented function locale_root_search(struct locale_root*,const char*,int) */
struct locale_search *locale_root_search(struct locale_root *root, const char *definition, int immediate)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===440###sois===10190###eois===10197###lif===2###soif===109###eoif===116###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");char c;

		AKA_mark("lis===441###sois===10199###eois===10216###lif===3###soif===118###eoif===135###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");size_t i, length;

		AKA_mark("lis===442###sois===10218###eois===10247###lif===4###soif===137###eoif===166###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");struct locale_search *search;


	/* normalize the definition */
		AKA_mark("lis===445###sois===10282###eois===10323###lif===7###soif===201###eoif===242###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");c = definition != NULL ? *definition : 0;

		while (AKA_mark("lis===446###sois===10332###eois===10365###lif===8###soif===251###eoif===284###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)") && (((AKA_mark("lis===446###sois===10332###eois===10340###lif===8###soif===251###eoif===259###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)")&&c == ' ')	||(AKA_mark("lis===446###sois===10344###eois===10353###lif===8###soif===263###eoif===272###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)")&&c == '\t'))	||(AKA_mark("lis===446###sois===10357###eois===10365###lif===8###soif===276###eoif===284###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)")&&c == ','))) {
		AKA_mark("lis===447###sois===10369###eois===10387###lif===9###soif===288###eoif===306###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");c = *++definition;
	}

		AKA_mark("lis===448###sois===10389###eois===10400###lif===10###soif===308###eoif===319###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");length = 0;

		while (AKA_mark("lis===449###sois===10409###eois===10410###lif===11###soif===328###eoif===329###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)") && (AKA_mark("lis===449###sois===10409###eois===10410###lif===11###soif===328###eoif===329###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)")&&c)) {
		AKA_mark("lis===450###sois===10414###eois===10439###lif===12###soif===333###eoif===358###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");c = definition[++length];
	}

		if (AKA_mark("lis===451###sois===10445###eois===10451###lif===13###soif===364###eoif===370###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)") && (AKA_mark("lis===451###sois===10445###eois===10451###lif===13###soif===364###eoif===370###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)")&&length)) {
				AKA_mark("lis===452###sois===10457###eois===10484###lif===14###soif===376###eoif===403###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");c = definition[length - 1];

				while (AKA_mark("lis===453###sois===10494###eois===10539###lif===15###soif===413###eoif===458###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)") && ((((AKA_mark("lis===453###sois===10495###eois===10503###lif===15###soif===414###eoif===422###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)")&&c == ' ')	||(AKA_mark("lis===453###sois===10507###eois===10516###lif===15###soif===426###eoif===435###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)")&&c == '\t'))	||(AKA_mark("lis===453###sois===10520###eois===10528###lif===15###soif===439###eoif===447###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)")&&c == ','))	&&(AKA_mark("lis===453###sois===10533###eois===10539###lif===15###soif===452###eoif===458###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)")&&length))) {
			AKA_mark("lis===454###sois===10544###eois===10573###lif===16###soif===463###eoif===492###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");c = definition[--length - 1];
		}

	}
	else {AKA_mark("lis===-451-###sois===-10445-###eois===-104456-###lif===-13-###soif===-###eoif===-370-###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");}


	/* search lru entry */
		AKA_mark("lis===458###sois===10603###eois===10609###lif===20###soif===522###eoif===528###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");i = 0;

		while (AKA_mark("lis===459###sois===10618###eois===10684###lif===21###soif===537###eoif===603###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)") && ((AKA_mark("lis===459###sois===10618###eois===10631###lif===21###soif===537###eoif===550###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)")&&i < LRU_COUNT)	&&(AKA_mark("lis===459###sois===10635###eois===10684###lif===21###soif===554###eoif===603###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)")&&!search_matches(root->lru[i], definition, length)))) {
		AKA_mark("lis===460###sois===10688###eois===10692###lif===22###soif===607###eoif===611###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");i++;
	}


	/* get the entry */
		if (AKA_mark("lis===463###sois===10720###eois===10733###lif===25###soif===639###eoif===652###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)") && (AKA_mark("lis===463###sois===10720###eois===10733###lif===25###soif===639###eoif===652###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)")&&i < LRU_COUNT)) {
		/* use an existing one */
				AKA_mark("lis===465###sois===10767###eois===10789###lif===27###soif===686###eoif===708###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");search = root->lru[i];

	}
	else {
		/* create a new one */
				AKA_mark("lis===468###sois===10827###eois===10887###lif===30###soif===746###eoif===806###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");search = create_search(root, definition, length, immediate);

				if (AKA_mark("lis===469###sois===10894###eois===10908###lif===31###soif===813###eoif===827###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)") && (AKA_mark("lis===469###sois===10894###eois===10908###lif===31###soif===813###eoif===827###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)")&&search == NULL)) {
			AKA_mark("lis===470###sois===10913###eois===10925###lif===32###soif===832###eoif===844###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");return NULL;
		}
		else {AKA_mark("lis===-469-###sois===-10894-###eois===-1089414-###lif===-31-###soif===-###eoif===-827-###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");}

		/* drop the oldest reference and update i */
				AKA_mark("lis===472###sois===10975###eois===11011###lif===34###soif===894###eoif===930###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");locale_search_unref(root->lru[--i]);

	}


	/* manage the LRU */
		while (AKA_mark("lis===476###sois===11046###eois===11051###lif===38###soif===965###eoif===970###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)") && (AKA_mark("lis===476###sois===11046###eois===11051###lif===38###soif===965###eoif===970###isc===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)")&&i > 0)) {
				AKA_mark("lis===477###sois===11057###eois===11089###lif===39###soif===976###eoif===1008###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");root->lru[i] = root->lru[i - 1];

				AKA_mark("lis===478###sois===11092###eois===11102###lif===40###soif===1011###eoif===1021###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");i = i - 1;

	}

		AKA_mark("lis===480###sois===11107###eois===11129###lif===42###soif===1026###eoif===1048###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");root->lru[0] = search;


	/* returns a new instance */
		AKA_mark("lis===483###sois===11162###eois===11198###lif===45###soif===1081###eoif===1117###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_search(struct locale_root*,const char*,int)");return locale_search_addref(search);

}

/*
 * Adds a reference to the search
 */
/** Instrumented function locale_search_addref(struct locale_search*) */
struct locale_search *locale_search_addref(struct locale_search *search)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/locale_search_addref(struct locale_search*)");AKA_fCall++;
		AKA_mark("lis===491###sois===11319###eois===11378###lif===2###soif===76###eoif===135###ins===true###function===./app-framework-binder/src/locale-root.c/locale_search_addref(struct locale_search*)");__atomic_add_fetch(&search->refcount, 1, __ATOMIC_RELAXED);

		AKA_mark("lis===492###sois===11380###eois===11394###lif===3###soif===137###eoif===151###ins===true###function===./app-framework-binder/src/locale-root.c/locale_search_addref(struct locale_search*)");return search;

}

/*
 * Removes a reference from the search
 */
/** Instrumented function locale_search_unref(struct locale_search*) */
void locale_search_unref(struct locale_search *search)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/locale_search_unref(struct locale_search*)");AKA_fCall++;
		AKA_mark("lis===500###sois===11502###eois===11537###lif===2###soif===58###eoif===93###ins===true###function===./app-framework-binder/src/locale-root.c/locale_search_unref(struct locale_search*)");struct locale_search_node *it, *nx;


		if (AKA_mark("lis===502###sois===11544###eois===11613###lif===4###soif===100###eoif===169###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_search_unref(struct locale_search*)") && ((AKA_mark("lis===502###sois===11544###eois===11550###lif===4###soif===100###eoif===106###isc===true###function===./app-framework-binder/src/locale-root.c/locale_search_unref(struct locale_search*)")&&search)	&&(AKA_mark("lis===502###sois===11554###eois===11613###lif===4###soif===110###eoif===169###isc===true###function===./app-framework-binder/src/locale-root.c/locale_search_unref(struct locale_search*)")&&!__atomic_sub_fetch(&search->refcount, 1, __ATOMIC_RELAXED)))) {
				AKA_mark("lis===503###sois===11619###eois===11637###lif===5###soif===175###eoif===193###ins===true###function===./app-framework-binder/src/locale-root.c/locale_search_unref(struct locale_search*)");it = search->head;

				while (AKA_mark("lis===504###sois===11646###eois===11656###lif===6###soif===202###eoif===212###ifc===true###function===./app-framework-binder/src/locale-root.c/locale_search_unref(struct locale_search*)") && (AKA_mark("lis===504###sois===11646###eois===11656###lif===6###soif===202###eoif===212###isc===true###function===./app-framework-binder/src/locale-root.c/locale_search_unref(struct locale_search*)")&&it != NULL)) {
						AKA_mark("lis===505###sois===11663###eois===11677###lif===7###soif===219###eoif===233###ins===true###function===./app-framework-binder/src/locale-root.c/locale_search_unref(struct locale_search*)");nx = it->next;

						AKA_mark("lis===506###sois===11681###eois===11690###lif===8###soif===237###eoif===246###ins===true###function===./app-framework-binder/src/locale-root.c/locale_search_unref(struct locale_search*)");free(it);

						AKA_mark("lis===507###sois===11694###eois===11702###lif===9###soif===250###eoif===258###ins===true###function===./app-framework-binder/src/locale-root.c/locale_search_unref(struct locale_search*)");it = nx;

		}

				AKA_mark("lis===509###sois===11709###eois===11738###lif===11###soif===265###eoif===294###ins===true###function===./app-framework-binder/src/locale-root.c/locale_search_unref(struct locale_search*)");internal_unref(search->root);

				AKA_mark("lis===510###sois===11741###eois===11754###lif===12###soif===297###eoif===310###ins===true###function===./app-framework-binder/src/locale-root.c/locale_search_unref(struct locale_search*)");free(search);

	}
	else {AKA_mark("lis===-502-###sois===-11544-###eois===-1154469-###lif===-4-###soif===-###eoif===-169-###ins===true###function===./app-framework-binder/src/locale-root.c/locale_search_unref(struct locale_search*)");}

}

/*
 * Set the default search of 'root' to 'search'.
 * This search is used as fallback when other search are failing.
 */
/** Instrumented function locale_root_set_default_search(struct locale_root*,struct locale_search*) */
void locale_root_set_default_search(struct locale_root *root, struct locale_search *search)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/locale_root_set_default_search(struct locale_root*,struct locale_search*)");AKA_fCall++;
		AKA_mark("lis===520###sois===11978###eois===12006###lif===2###soif===95###eoif===123###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_set_default_search(struct locale_root*,struct locale_search*)");struct locale_search *older;


		AKA_mark("lis===522###sois===12009###eois===12056###lif===4###soif===126###eoif===173###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_set_default_search(struct locale_root*,struct locale_search*)");assert(search == NULL || search->root == root);


		AKA_mark("lis===524###sois===12059###eois===12088###lif===6###soif===176###eoif===205###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_set_default_search(struct locale_root*,struct locale_search*)");older = root->default_search;

		AKA_mark("lis===525###sois===12090###eois===12158###lif===7###soif===207###eoif===275###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_set_default_search(struct locale_root*,struct locale_search*)");root->default_search = search ? locale_search_addref(search) : NULL;

		AKA_mark("lis===526###sois===12160###eois===12187###lif===8###soif===277###eoif===304###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_set_default_search(struct locale_root*,struct locale_search*)");locale_search_unref(older);

}

/*
 * Opens 'filename' for 'search' and 'root'.
 *
 * Returns the file descriptor as returned by openat
 * system call or -1 in case of error.
 */
/** Instrumented function do_open(struct locale_search*,const char*,int,struct locale_root*) */
static int do_open(struct locale_search *search, const char *filename, int flags, struct locale_root *root)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");AKA_fCall++;
		AKA_mark("lis===537###sois===12449###eois===12474###lif===2###soif===111###eoif===136###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");size_t maxlength, length;

		AKA_mark("lis===538###sois===12476###eois===12493###lif===3###soif===138###eoif===155###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");char *buffer, *p;

		AKA_mark("lis===539###sois===12495###eois===12527###lif===4###soif===157###eoif===189###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");struct locale_search_node *node;

		AKA_mark("lis===540###sois===12529###eois===12558###lif===5###soif===191###eoif===220###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");struct locale_folder *folder;

		AKA_mark("lis===541###sois===12560###eois===12575###lif===6###soif===222###eoif===237###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");int rootfd, fd;


	/* check the path and normalize it */
		AKA_mark("lis===544###sois===12617###eois===12652###lif===9###soif===279###eoif===314###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");filename = subpath_force(filename);

		if (AKA_mark("lis===545###sois===12658###eois===12674###lif===10###soif===320###eoif===336###ifc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)") && (AKA_mark("lis===545###sois===12658###eois===12674###lif===10###soif===320###eoif===336###isc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)")&&filename == NULL)) {
		AKA_mark("lis===546###sois===12678###eois===12689###lif===11###soif===340###eoif===351###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");goto inval;
	}
	else {AKA_mark("lis===-545-###sois===-12658-###eois===-1265816-###lif===-10-###soif===-###eoif===-336-###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");}


	/* no creation flags accepted */
		if (AKA_mark("lis===549###sois===12730###eois===12752###lif===14###soif===392###eoif===414###ifc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)") && (AKA_mark("lis===549###sois===12730###eois===12752###lif===14###soif===392###eoif===414###isc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)")&&(flags & O_CREAT) != 0)) {
		AKA_mark("lis===550###sois===12756###eois===12767###lif===15###soif===418###eoif===429###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");goto inval;
	}
	else {AKA_mark("lis===-549-###sois===-12730-###eois===-1273022-###lif===-14-###soif===-###eoif===-414-###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");}


	/* search for folders */
		AKA_mark("lis===553###sois===12796###eois===12818###lif===18###soif===458###eoif===480###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");rootfd = root->rootfd;

		AKA_mark("lis===554###sois===12820###eois===12856###lif===19###soif===482###eoif===518###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");node = search ? search->head : NULL;

		if (AKA_mark("lis===555###sois===12862###eois===12874###lif===20###soif===524###eoif===536###ifc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)") && (AKA_mark("lis===555###sois===12862###eois===12874###lif===20###soif===524###eoif===536###isc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)")&&node != NULL)) {
		/* allocates a buffer big enough */
				AKA_mark("lis===557###sois===12918###eois===12956###lif===22###soif===580###eoif===618###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");maxlength = root->container.maxlength;

				AKA_mark("lis===558###sois===12959###eois===12985###lif===23###soif===621###eoif===647###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");length = strlen(filename);

				if (AKA_mark("lis===559###sois===12992###eois===13009###lif===24###soif===654###eoif===671###ifc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)") && (AKA_mark("lis===559###sois===12992###eois===13009###lif===24###soif===654###eoif===671###isc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)")&&length > PATH_MAX)) {
			AKA_mark("lis===560###sois===13014###eois===13025###lif===25###soif===676###eoif===687###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");goto inval;
		}
		else {AKA_mark("lis===-559-###sois===-12992-###eois===-1299217-###lif===-24-###soif===-###eoif===-671-###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");}


		/* initialise the end of the buffer */
				AKA_mark("lis===563###sois===13070###eois===13127###lif===28###soif===732###eoif===789###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");buffer = alloca(length + maxlength + sizeof locales + 1);

				AKA_mark("lis===564###sois===13130###eois===13175###lif===29###soif===792###eoif===837###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");buffer[maxlength + sizeof locales - 1] = '/';

				AKA_mark("lis===565###sois===13178###eois===13244###lif===30###soif===840###eoif===906###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");memcpy(buffer + sizeof locales + maxlength, filename, length + 1);


		/* iterate the searched folder */
				while (AKA_mark("lis===568###sois===13291###eois===13303###lif===33###soif===953###eoif===965###ifc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)") && (AKA_mark("lis===568###sois===13291###eois===13303###lif===33###soif===953###eoif===965###isc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)")&&node != NULL)) {
						AKA_mark("lis===569###sois===13310###eois===13332###lif===34###soif===972###eoif===994###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");folder = node->folder;

						AKA_mark("lis===570###sois===13336###eois===13376###lif===35###soif===998###eoif===1038###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");p = buffer + maxlength - folder->length;

						AKA_mark("lis===571###sois===13380###eois===13419###lif===36###soif===1042###eoif===1081###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");memcpy(p, locales, sizeof locales - 1);

						AKA_mark("lis===572###sois===13423###eois===13484###lif===37###soif===1085###eoif===1146###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");memcpy(p + sizeof locales - 1, folder->name, folder->length);

						AKA_mark("lis===573###sois===13488###eois===13518###lif===38###soif===1150###eoif===1180###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");fd = openat(rootfd, p, flags);

						if (AKA_mark("lis===574###sois===13526###eois===13533###lif===39###soif===1188###eoif===1195###ifc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)") && (AKA_mark("lis===574###sois===13526###eois===13533###lif===39###soif===1188###eoif===1195###isc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)")&&fd >= 0)) {
				AKA_mark("lis===575###sois===13539###eois===13549###lif===40###soif===1201###eoif===1211###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");return fd;
			}
			else {AKA_mark("lis===-574-###sois===-13526-###eois===-135267-###lif===-39-###soif===-###eoif===-1195-###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");}

						AKA_mark("lis===576###sois===13553###eois===13571###lif===41###soif===1215###eoif===1233###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");node = node->next;

						if (AKA_mark("lis===577###sois===13579###eois===13625###lif===42###soif===1241###eoif===1287###ifc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)") && ((AKA_mark("lis===577###sois===13579###eois===13591###lif===42###soif===1241###eoif===1253###isc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)")&&node == NULL)	&&(AKA_mark("lis===577###sois===13595###eois===13625###lif===42###soif===1257###eoif===1287###isc===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)")&&search != root->default_search))) {
								AKA_mark("lis===578###sois===13633###eois===13663###lif===43###soif===1295###eoif===1325###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");search = root->default_search;

								AKA_mark("lis===579###sois===13668###eois===13704###lif===44###soif===1330###eoif===1366###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");node = search ? search->head : NULL;

			}
			else {AKA_mark("lis===-577-###sois===-13579-###eois===-1357946-###lif===-42-###soif===-###eoif===-1287-###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");}

		}

	}
	else {AKA_mark("lis===-555-###sois===-12862-###eois===-1286212-###lif===-20-###soif===-###eoif===-536-###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");}


	/* root search */
		AKA_mark("lis===585###sois===13738###eois===13777###lif===50###soif===1400###eoif===1439###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");return openat(rootfd, filename, flags);


	inval:
	AKA_mark("lis===588###sois===13787###eois===13802###lif===53###soif===1449###eoif===1464###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");errno = EINVAL;

		AKA_mark("lis===589###sois===13804###eois===13814###lif===54###soif===1466###eoif===1476###ins===true###function===./app-framework-binder/src/locale-root.c/do_open(struct locale_search*,const char*,int,struct locale_root*)");return -1;

}

/*
 * Opens 'filename' after search.
 *
 * Returns the file descriptor as returned by openat
 * system call or -1 in case of error.
 */
/** Instrumented function locale_search_open(struct locale_search*,const char*,int) */
int locale_search_open(struct locale_search *search, const char *filename, int flags)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/locale_search_open(struct locale_search*,const char*,int)");AKA_fCall++;
		AKA_mark("lis===600###sois===14043###eois===14097###lif===2###soif===89###eoif===143###ins===true###function===./app-framework-binder/src/locale-root.c/locale_search_open(struct locale_search*,const char*,int)");return do_open(search, filename, flags, search->root);

}

/*
 * Opens 'filename' for root with default search.
 *
 * Returns the file descriptor as returned by openat
 * system call or -1 in case of error.
 */
/** Instrumented function locale_root_open(struct locale_root*,const char*,int,const char*) */
int locale_root_open(struct locale_root *root, const char *filename, int flags, const char *locale)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/locale_root_open(struct locale_root*,const char*,int,const char*)");AKA_fCall++;
		AKA_mark("lis===611###sois===14356###eois===14367###lif===2###soif===103###eoif===114###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_open(struct locale_root*,const char*,int,const char*)");int result;

		AKA_mark("lis===612###sois===14369###eois===14398###lif===3###soif===116###eoif===145###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_open(struct locale_root*,const char*,int,const char*)");struct locale_search *search;


		AKA_mark("lis===614###sois===14401###eois===14470###lif===5###soif===148###eoif===217###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_open(struct locale_root*,const char*,int,const char*)");search = locale != NULL ? locale_root_search(root, locale, 0) : NULL;

		AKA_mark("lis===615###sois===14472###eois===14545###lif===6###soif===219###eoif===292###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_open(struct locale_root*,const char*,int,const char*)");result = do_open(search ? : root->default_search, filename, flags, root);

		AKA_mark("lis===616###sois===14547###eois===14575###lif===7###soif===294###eoif===322###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_open(struct locale_root*,const char*,int,const char*)");locale_search_unref(search);

		AKA_mark("lis===617###sois===14577###eois===14591###lif===8###soif===324###eoif===338###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_open(struct locale_root*,const char*,int,const char*)");return result;

}

/*
 * Resolves 'filename' for 'root' and 'search'.
 *
 * returns a copy of the filename after search or NULL if not found.
 * the returned string MUST be freed by the caller (using free).
 */
/** Instrumented function do_resolve(struct locale_search*,const char*,struct locale_root*) */
static char *do_resolve(struct locale_search *search, const char *filename, struct locale_root *root)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");AKA_fCall++;
		AKA_mark("lis===628###sois===14892###eois===14917###lif===2###soif===105###eoif===130###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");size_t maxlength, length;

		AKA_mark("lis===629###sois===14919###eois===14936###lif===3###soif===132###eoif===149###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");char *buffer, *p;

		AKA_mark("lis===630###sois===14938###eois===14970###lif===4###soif===151###eoif===183###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");struct locale_search_node *node;

		AKA_mark("lis===631###sois===14972###eois===15001###lif===5###soif===185###eoif===214###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");struct locale_folder *folder;

		AKA_mark("lis===632###sois===15003###eois===15014###lif===6###soif===216###eoif===227###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");int rootfd;


	/* check the path and normalize it */
		AKA_mark("lis===635###sois===15056###eois===15091###lif===9###soif===269###eoif===304###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");filename = subpath_force(filename);

		if (AKA_mark("lis===636###sois===15097###eois===15113###lif===10###soif===310###eoif===326###ifc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)") && (AKA_mark("lis===636###sois===15097###eois===15113###lif===10###soif===310###eoif===326###isc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)")&&filename == NULL)) {
		AKA_mark("lis===637###sois===15117###eois===15128###lif===11###soif===330###eoif===341###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");goto inval;
	}
	else {AKA_mark("lis===-636-###sois===-15097-###eois===-1509716-###lif===-10-###soif===-###eoif===-326-###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");}


	/* search for folders */
		AKA_mark("lis===640###sois===15157###eois===15179###lif===14###soif===370###eoif===392###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");rootfd = root->rootfd;

		AKA_mark("lis===641###sois===15181###eois===15217###lif===15###soif===394###eoif===430###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");node = search ? search->head : NULL;

		if (AKA_mark("lis===642###sois===15223###eois===15235###lif===16###soif===436###eoif===448###ifc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)") && (AKA_mark("lis===642###sois===15223###eois===15235###lif===16###soif===436###eoif===448###isc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)")&&node != NULL)) {
		/* allocates a buffer big enough */
				AKA_mark("lis===644###sois===15279###eois===15317###lif===18###soif===492###eoif===530###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");maxlength = root->container.maxlength;

				AKA_mark("lis===645###sois===15320###eois===15346###lif===19###soif===533###eoif===559###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");length = strlen(filename);

				if (AKA_mark("lis===646###sois===15353###eois===15370###lif===20###soif===566###eoif===583###ifc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)") && (AKA_mark("lis===646###sois===15353###eois===15370###lif===20###soif===566###eoif===583###isc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)")&&length > PATH_MAX)) {
			AKA_mark("lis===647###sois===15375###eois===15386###lif===21###soif===588###eoif===599###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");goto inval;
		}
		else {AKA_mark("lis===-646-###sois===-15353-###eois===-1535317-###lif===-20-###soif===-###eoif===-583-###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");}


		/* initialise the end of the buffer */
				AKA_mark("lis===650###sois===15431###eois===15488###lif===24###soif===644###eoif===701###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");buffer = alloca(length + maxlength + sizeof locales + 1);

				AKA_mark("lis===651###sois===15491###eois===15536###lif===25###soif===704###eoif===749###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");buffer[maxlength + sizeof locales - 1] = '/';

				AKA_mark("lis===652###sois===15539###eois===15605###lif===26###soif===752###eoif===818###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");memcpy(buffer + sizeof locales + maxlength, filename, length + 1);


		/* iterate the searched folder */
				while (AKA_mark("lis===655###sois===15652###eois===15664###lif===29###soif===865###eoif===877###ifc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)") && (AKA_mark("lis===655###sois===15652###eois===15664###lif===29###soif===865###eoif===877###isc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)")&&node != NULL)) {
						AKA_mark("lis===656###sois===15671###eois===15693###lif===30###soif===884###eoif===906###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");folder = node->folder;

						AKA_mark("lis===657###sois===15697###eois===15737###lif===31###soif===910###eoif===950###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");p = buffer + maxlength - folder->length;

						AKA_mark("lis===658###sois===15741###eois===15780###lif===32###soif===954###eoif===993###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");memcpy(p, locales, sizeof locales - 1);

						AKA_mark("lis===659###sois===15784###eois===15845###lif===33###soif===997###eoif===1058###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");memcpy(p + sizeof locales - 1, folder->name, folder->length);

						if (AKA_mark("lis===660###sois===15853###eois===15887###lif===34###soif===1066###eoif===1100###ifc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)") && (AKA_mark("lis===660###sois===15853###eois===15887###lif===34###soif===1066###eoif===1100###isc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)")&&0 == faccessat(rootfd, p, F_OK, 0))) {
								AKA_mark("lis===661###sois===15895###eois===15908###lif===35###soif===1108###eoif===1121###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");filename = p;

								AKA_mark("lis===662###sois===15913###eois===15924###lif===36###soif===1126###eoif===1137###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");goto found;

			}
			else {AKA_mark("lis===-660-###sois===-15853-###eois===-1585334-###lif===-34-###soif===-###eoif===-1100-###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");}

						AKA_mark("lis===664###sois===15933###eois===15951###lif===38###soif===1146###eoif===1164###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");node = node->next;

						if (AKA_mark("lis===665###sois===15959###eois===16005###lif===39###soif===1172###eoif===1218###ifc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)") && ((AKA_mark("lis===665###sois===15959###eois===15971###lif===39###soif===1172###eoif===1184###isc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)")&&node == NULL)	&&(AKA_mark("lis===665###sois===15975###eois===16005###lif===39###soif===1188###eoif===1218###isc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)")&&search != root->default_search))) {
								AKA_mark("lis===666###sois===16013###eois===16043###lif===40###soif===1226###eoif===1256###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");search = root->default_search;

								AKA_mark("lis===667###sois===16048###eois===16084###lif===41###soif===1261###eoif===1297###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");node = search ? search->head : NULL;

			}
			else {AKA_mark("lis===-665-###sois===-15959-###eois===-1595946-###lif===-39-###soif===-###eoif===-1218-###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");}

		}

	}
	else {AKA_mark("lis===-642-###sois===-15223-###eois===-1522312-###lif===-16-###soif===-###eoif===-448-###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");}


	/* root search */
		if (AKA_mark("lis===673###sois===16122###eois===16163###lif===47###soif===1335###eoif===1376###ifc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)") && (AKA_mark("lis===673###sois===16122###eois===16163###lif===47###soif===1335###eoif===1376###isc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)")&&0 != faccessat(rootfd, filename, F_OK, 0))) {
				AKA_mark("lis===674###sois===16169###eois===16184###lif===48###soif===1382###eoif===1397###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");errno = ENOENT;

				AKA_mark("lis===675###sois===16187###eois===16199###lif===49###soif===1400###eoif===1412###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");return NULL;

	}
	else {AKA_mark("lis===-673-###sois===-16122-###eois===-1612241-###lif===-47-###soif===-###eoif===-1376-###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");}


	found:
	AKA_mark("lis===679###sois===16212###eois===16233###lif===53###soif===1425###eoif===1446###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");p = strdup(filename);

		if (AKA_mark("lis===680###sois===16239###eois===16248###lif===54###soif===1452###eoif===1461###ifc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)") && (AKA_mark("lis===680###sois===16239###eois===16248###lif===54###soif===1452###eoif===1461###isc===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)")&&p == NULL)) {
		AKA_mark("lis===681###sois===16252###eois===16267###lif===55###soif===1465###eoif===1480###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");errno = ENOMEM;
	}
	else {AKA_mark("lis===-680-###sois===-16239-###eois===-162399-###lif===-54-###soif===-###eoif===-1461-###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");}

		AKA_mark("lis===682###sois===16269###eois===16278###lif===56###soif===1482###eoif===1491###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");return p;


	inval:
	AKA_mark("lis===685###sois===16288###eois===16303###lif===59###soif===1501###eoif===1516###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");errno = EINVAL;

		AKA_mark("lis===686###sois===16305###eois===16317###lif===60###soif===1518###eoif===1530###ins===true###function===./app-framework-binder/src/locale-root.c/do_resolve(struct locale_search*,const char*,struct locale_root*)");return NULL;

}

/*
 * Resolves 'filename' at 'root' after default search.
 *
 * returns a copy of the filename after search or NULL if not found.
 * the returned string MUST be freed by the caller (using free).
 */
/** Instrumented function locale_root_resolve(struct locale_root*,const char*,const char*) */
char *locale_root_resolve(struct locale_root *root, const char *filename, const char *locale)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/locale_root_resolve(struct locale_root*,const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===697###sois===16617###eois===16630###lif===2###soif===97###eoif===110###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_resolve(struct locale_root*,const char*,const char*)");char *result;

		AKA_mark("lis===698###sois===16632###eois===16661###lif===3###soif===112###eoif===141###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_resolve(struct locale_root*,const char*,const char*)");struct locale_search *search;


		AKA_mark("lis===700###sois===16664###eois===16733###lif===5###soif===144###eoif===213###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_resolve(struct locale_root*,const char*,const char*)");search = locale != NULL ? locale_root_search(root, locale, 0) : NULL;

		AKA_mark("lis===701###sois===16735###eois===16804###lif===6###soif===215###eoif===284###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_resolve(struct locale_root*,const char*,const char*)");result = do_resolve(search ? : root->default_search, filename, root);

		AKA_mark("lis===702###sois===16806###eois===16834###lif===7###soif===286###eoif===314###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_resolve(struct locale_root*,const char*,const char*)");locale_search_unref(search);

		AKA_mark("lis===703###sois===16836###eois===16850###lif===8###soif===316###eoif===330###ins===true###function===./app-framework-binder/src/locale-root.c/locale_root_resolve(struct locale_root*,const char*,const char*)");return result;

}

/*
 * Resolves 'filename' after 'search'.
 *
 * returns a copy of the filename after search or NULL if not found.
 * the returned string MUST be freed by the caller (using free).
 */
/** Instrumented function locale_search_resolve(struct locale_search*,const char*) */
char *locale_search_resolve(struct locale_search *search, const char *filename)
{AKA_mark("Calling: ./app-framework-binder/src/locale-root.c/locale_search_resolve(struct locale_search*,const char*)");AKA_fCall++;
		AKA_mark("lis===714###sois===17120###eois===17170###lif===2###soif===83###eoif===133###ins===true###function===./app-framework-binder/src/locale-root.c/locale_search_resolve(struct locale_search*,const char*)");return do_resolve(search, filename, search->root);

}

#if defined(TEST_locale_root)
int main(int ac,char**av)
{
	struct locale_root *root = locale_root_create(AT_FDCWD);
	struct locale_search *search = NULL;
	int fd, rc, i;
	char buffer[256];
	char *subpath;
	while (*++av) {
		if (**av == '@') {
			locale_search_unref(search);
			search = NULL;
			locale_root_unref(root);
			root = locale_root_create(AT_FDCWD, *av + 1);
			if (root == NULL)
				fprintf(stderr, "can't create root at %s: %m\n", *av + 1);
			else
				printf("root: %s\n", *av + 1);
		} else {
			if (root == NULL) {
				fprintf(stderr, "no valid root for %s\n", *av);
			} else if (**av == '-' || **av == '+') {
				locale_search_unref(search);
				search = locale_root_search(root, *av + 1, **av == '+');
				if (search == NULL)
					fprintf(stderr, "can't create search for %s: %m\n", *av + 1);
				else
					printf("search: %s\n", *av + 1);
			} else if (search == NULL) {
				fprintf(stderr, "no valid search for %s\n", *av);
			} else {
				fd = locale_search_open(search, *av, O_RDONLY);
				if (fd < 0)
					fprintf(stderr, "can't open file %s: %m\n", *av);
				else {
					subpath = locale_search_resolve(search, *av);
					if (subpath == NULL)
						fprintf(stderr, "can't resolve file %s: %m\n", *av);
					else {
						rc = (int)read(fd, buffer, sizeof buffer - 1);
						if (rc < 0)
							fprintf(stderr, "can't read file %s: %m\n", *av);
						else {
							buffer[rc] = 0;
							*strchrnul(buffer, '\n') = 0;
							printf("%s -> %s [%s]\n", *av, subpath, buffer);
						}
						free(subpath);
					}
					close(fd);
				}
			}
		}
	}
	locale_search_unref(search); search = NULL;
	locale_root_unref(root); root = NULL;
}
#endif


#endif

