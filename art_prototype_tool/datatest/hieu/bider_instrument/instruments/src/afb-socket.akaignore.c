/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_SOCKET_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_SOCKET_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <endian.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_FDEV_H_
#define AKA_INCLUDE__AFB_FDEV_H_
#include "afb-fdev.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_SOCKET_H_
#define AKA_INCLUDE__AFB_SOCKET_H_
#include "afb-socket.akaignore.h"
#endif


/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__SYSTEMD_H_
#define AKA_INCLUDE__SYSTEMD_H_
#include "systemd.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__FDEV_H_
#define AKA_INCLUDE__FDEV_H_
#include "fdev.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif


#define BACKLOG  5

/******************************************************************************/

/**
 * known types
 */
enum type {
	/** type internet */
	Type_Inet,

	/** type systemd */
	Type_Systemd,

	/** type Unix */
	Type_Unix
};

/**
 * Structure for known entries
 */
struct entry
{
	/** the known prefix */
	const char *prefix;

	/** the type of the entry */
	unsigned type: 2;

	/** should not set SO_REUSEADDR for servers */
	unsigned noreuseaddr: 1;

	/** should not call listen for servers */
	unsigned nolisten: 1;
};

/**
 * The known entries with the default one at the first place
 */
static struct entry entries[] = {
	{
		.prefix = "tcp:",
		.type = Type_Inet
	},
	{
		.prefix = "sd:",
		.type = Type_Systemd,
		.noreuseaddr = 1,
		.nolisten = 1
	},
	{
		.prefix = "unix:",
		.type = Type_Unix
	}
};

/**
 * It is possible to set explicit api name instead of using the
 * default one.
 */
static const char as_api[] = "?as-api=";

/******************************************************************************/

/**
 * open a unix domain socket for client or server
 *
 * @param spec the specification of the path (prefix with @ for abstract)
 * @param server 0 for client, server otherwise
 *
 * @return the file descriptor number of the socket or -1 in case of error
 */
/** Instrumented function open_unix(const char*,int) */
static int open_unix(const char *spec, int server)
{AKA_mark("Calling: ./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");AKA_fCall++;
		AKA_mark("lis===113###sois===2349###eois===2370###lif===2###soif===54###eoif===75###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");int fd, rc, abstract;

		AKA_mark("lis===114###sois===2372###eois===2396###lif===3###soif===77###eoif===101###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");struct sockaddr_un addr;

		AKA_mark("lis===115###sois===2398###eois===2412###lif===4###soif===103###eoif===117###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");size_t length;


		AKA_mark("lis===117###sois===2415###eois===2441###lif===6###soif===120###eoif===146###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");abstract = spec[0] == '@';


	/* check the length */
		AKA_mark("lis===120###sois===2468###eois===2490###lif===9###soif===173###eoif===195###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");length = strlen(spec);

		if (AKA_mark("lis===121###sois===2496###eois===2509###lif===10###soif===201###eoif===214###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)") && (AKA_mark("lis===121###sois===2496###eois===2509###lif===10###soif===201###eoif===214###isc===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)")&&length >= 108)) {
				AKA_mark("lis===122###sois===2515###eois===2536###lif===11###soif===220###eoif===241###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");errno = ENAMETOOLONG;

				AKA_mark("lis===123###sois===2539###eois===2549###lif===12###soif===244###eoif===254###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");return -1;

	}
	else {AKA_mark("lis===-121-###sois===-2496-###eois===-249613-###lif===-10-###soif===-###eoif===-214-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");}


	/* remove the file on need */
		if (AKA_mark("lis===127###sois===2590###eois===2609###lif===16###soif===295###eoif===314###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)") && ((AKA_mark("lis===127###sois===2590###eois===2596###lif===16###soif===295###eoif===301###isc===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)")&&server)	&&(AKA_mark("lis===127###sois===2600###eois===2609###lif===16###soif===305###eoif===314###isc===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)")&&!abstract))) {
		AKA_mark("lis===128###sois===2613###eois===2626###lif===17###soif===318###eoif===331###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");unlink(spec);
	}
	else {AKA_mark("lis===-127-###sois===-2590-###eois===-259019-###lif===-16-###soif===-###eoif===-314-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");}


	/* create a  socket */
		AKA_mark("lis===131###sois===2653###eois===2690###lif===20###soif===358###eoif===395###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");fd = socket(AF_UNIX, SOCK_STREAM, 0);

		if (AKA_mark("lis===132###sois===2696###eois===2702###lif===21###soif===401###eoif===407###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)") && (AKA_mark("lis===132###sois===2696###eois===2702###lif===21###soif===401###eoif===407###isc===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)")&&fd < 0)) {
		AKA_mark("lis===133###sois===2706###eois===2716###lif===22###soif===411###eoif===421###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");return fd;
	}
	else {AKA_mark("lis===-132-###sois===-2696-###eois===-26966-###lif===-21-###soif===-###eoif===-407-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");}


	/* prepare address  */
		AKA_mark("lis===136###sois===2743###eois===2773###lif===25###soif===448###eoif===478###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");memset(&addr, 0, sizeof addr);

		AKA_mark("lis===137###sois===2775###eois===2801###lif===26###soif===480###eoif===506###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");addr.sun_family = AF_UNIX;

		AKA_mark("lis===138###sois===2803###eois===2831###lif===27###soif===508###eoif===536###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");strcpy(addr.sun_path, spec);

		if (AKA_mark("lis===139###sois===2837###eois===2845###lif===28###soif===542###eoif===550###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)") && (AKA_mark("lis===139###sois===2837###eois===2845###lif===28###soif===542###eoif===550###isc===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)")&&abstract)) {
		AKA_mark("lis===140###sois===2849###eois===2870###lif===29###soif===554###eoif===575###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");addr.sun_path[0] = 0;
	}
	else {AKA_mark("lis===-139-###sois===-2837-###eois===-28378-###lif===-28-###soif===-###eoif===-550-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");}
 /* implement abstract sockets */

		if (AKA_mark("lis===142###sois===2910###eois===2916###lif===31###soif===615###eoif===621###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)") && (AKA_mark("lis===142###sois===2910###eois===2916###lif===31###soif===615###eoif===621###isc===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)")&&server)) {
				AKA_mark("lis===143###sois===2922###eois===2989###lif===32###soif===627###eoif===694###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");rc = bind(fd, (struct sockaddr *) &addr, (socklen_t)(sizeof addr));

	}
	else {
				AKA_mark("lis===145###sois===3002###eois===3072###lif===34###soif===707###eoif===777###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");rc = connect(fd, (struct sockaddr *) &addr, (socklen_t)(sizeof addr));

	}

		if (AKA_mark("lis===147###sois===3081###eois===3087###lif===36###soif===786###eoif===792###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)") && (AKA_mark("lis===147###sois===3081###eois===3087###lif===36###soif===786###eoif===792###isc===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)")&&rc < 0)) {
				AKA_mark("lis===148###sois===3093###eois===3103###lif===37###soif===798###eoif===808###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");close(fd);

				AKA_mark("lis===149###sois===3106###eois===3116###lif===38###soif===811###eoif===821###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");return rc;

	}
	else {AKA_mark("lis===-147-###sois===-3081-###eois===-30816-###lif===-36-###soif===-###eoif===-792-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");}

		AKA_mark("lis===151###sois===3121###eois===3131###lif===40###soif===826###eoif===836###ins===true###function===./app-framework-binder/src/afb-socket.c/open_unix(const char*,int)");return fd;

}

/**
 * open a tcp socket for client or server
 *
 * @param spec the specification of the host:port/...
 * @param server 0 for client, server otherwise
 *
 * @return the file descriptor number of the socket or -1 in case of error
 */
/** Instrumented function open_tcp(const char*,int,int) */
static int open_tcp(const char *spec, int server, int reuseaddr)
{AKA_mark("Calling: ./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");AKA_fCall++;
		AKA_mark("lis===164###sois===3436###eois===3447###lif===2###soif===68###eoif===79###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");int rc, fd;

		AKA_mark("lis===165###sois===3449###eois===3483###lif===3###soif===81###eoif===115###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");const char *service, *host, *tail;

		AKA_mark("lis===166###sois===3485###eois===3518###lif===4###soif===117###eoif===150###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");struct addrinfo hint, *rai, *iai;


	/* scan the uri */
		AKA_mark("lis===169###sois===3541###eois===3569###lif===7###soif===173###eoif===201###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");tail = strchrnul(spec, '/');

		AKA_mark("lis===170###sois===3571###eois===3599###lif===8###soif===203###eoif===231###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");service = strchr(spec, ':');

		if (AKA_mark("lis===171###sois===3605###eois===3654###lif===9###soif===237###eoif===286###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)") && (((AKA_mark("lis===171###sois===3605###eois===3617###lif===9###soif===237###eoif===249###isc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)")&&tail == NULL)	||(AKA_mark("lis===171###sois===3621###eois===3636###lif===9###soif===253###eoif===268###isc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)")&&service == NULL))	||(AKA_mark("lis===171###sois===3640###eois===3654###lif===9###soif===272###eoif===286###isc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)")&&tail < service))) {
				AKA_mark("lis===172###sois===3660###eois===3675###lif===10###soif===292###eoif===307###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");errno = EINVAL;

				AKA_mark("lis===173###sois===3678###eois===3688###lif===11###soif===310###eoif===320###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");return -1;

	}
	else {AKA_mark("lis===-171-###sois===-3605-###eois===-360549-###lif===-9-###soif===-###eoif===-286-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");}

		AKA_mark("lis===175###sois===3693###eois===3733###lif===13###soif===325###eoif===365###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");host = strndupa(spec, service++ - spec);

		AKA_mark("lis===176###sois===3735###eois===3779###lif===14###soif===367###eoif===411###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");service = strndupa(service, tail - service);


	/* get addr */
		AKA_mark("lis===179###sois===3798###eois===3828###lif===17###soif===430###eoif===460###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");memset(&hint, 0, sizeof hint);

		AKA_mark("lis===180###sois===3830###eois===3855###lif===18###soif===462###eoif===487###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");hint.ai_family = AF_INET;

		AKA_mark("lis===181###sois===3857###eois===3888###lif===19###soif===489###eoif===520###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");hint.ai_socktype = SOCK_STREAM;

		if (AKA_mark("lis===182###sois===3894###eois===3900###lif===20###soif===526###eoif===532###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)") && (AKA_mark("lis===182###sois===3894###eois===3900###lif===20###soif===526###eoif===532###isc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)")&&server)) {
				AKA_mark("lis===183###sois===3906###eois===3933###lif===21###soif===538###eoif===565###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");hint.ai_flags = AI_PASSIVE;

				if (AKA_mark("lis===184###sois===3940###eois===3988###lif===22###soif===572###eoif===620###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)") && ((AKA_mark("lis===184###sois===3940###eois===3952###lif===22###soif===572###eoif===584###isc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)")&&host[0] == 0)	||((AKA_mark("lis===184###sois===3957###eois===3971###lif===22###soif===589###eoif===603###isc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)")&&host[0] == '*')	&&(AKA_mark("lis===184###sois===3975###eois===3987###lif===22###soif===607###eoif===619###isc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)")&&host[1] == 0)))) {
			AKA_mark("lis===185###sois===3993###eois===4005###lif===23###soif===625###eoif===637###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");host = NULL;
		}
		else {AKA_mark("lis===-184-###sois===-3940-###eois===-394048-###lif===-22-###soif===-###eoif===-620-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");}

	}
	else {AKA_mark("lis===-182-###sois===-3894-###eois===-38946-###lif===-20-###soif===-###eoif===-532-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");}

		AKA_mark("lis===187###sois===4010###eois===4055###lif===25###soif===642###eoif===687###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");rc = getaddrinfo(host, service, &hint, &rai);

		if (AKA_mark("lis===188###sois===4061###eois===4068###lif===26###soif===693###eoif===700###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)") && (AKA_mark("lis===188###sois===4061###eois===4068###lif===26###soif===693###eoif===700###isc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)")&&rc != 0)) {
				AKA_mark("lis===189###sois===4074###eois===4089###lif===27###soif===706###eoif===721###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");errno = EINVAL;

				AKA_mark("lis===190###sois===4092###eois===4102###lif===28###soif===724###eoif===734###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");return -1;

	}
	else {AKA_mark("lis===-188-###sois===-4061-###eois===-40617-###lif===-26-###soif===-###eoif===-700-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");}


	/* get the socket */
		AKA_mark("lis===194###sois===4130###eois===4140###lif===32###soif===762###eoif===772###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");iai = rai;

		while (AKA_mark("lis===195###sois===4149###eois===4160###lif===33###soif===781###eoif===792###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)") && (AKA_mark("lis===195###sois===4149###eois===4160###lif===33###soif===781###eoif===792###isc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)")&&iai != NULL)) {
				AKA_mark("lis===196###sois===4166###eois===4230###lif===34###soif===798###eoif===862###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");fd = socket(iai->ai_family, iai->ai_socktype, iai->ai_protocol);

				if (AKA_mark("lis===197###sois===4237###eois===4244###lif===35###soif===869###eoif===876###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)") && (AKA_mark("lis===197###sois===4237###eois===4244###lif===35###soif===869###eoif===876###isc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)")&&fd >= 0)) {
						if (AKA_mark("lis===198###sois===4255###eois===4261###lif===36###soif===887###eoif===893###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)") && (AKA_mark("lis===198###sois===4255###eois===4261###lif===36###soif===887###eoif===893###isc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)")&&server)) {
								if (AKA_mark("lis===199###sois===4273###eois===4282###lif===37###soif===905###eoif===914###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)") && (AKA_mark("lis===199###sois===4273###eois===4282###lif===37###soif===905###eoif===914###isc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)")&&reuseaddr)) {
										AKA_mark("lis===200###sois===4291###eois===4298###lif===38###soif===923###eoif===930###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");rc = 1;

										AKA_mark("lis===201###sois===4304###eois===4361###lif===39###soif===936###eoif===993###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &rc, sizeof rc);

				}
				else {AKA_mark("lis===-199-###sois===-4273-###eois===-42739-###lif===-37-###soif===-###eoif===-914-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");}

								AKA_mark("lis===203###sois===4372###eois===4417###lif===41###soif===1004###eoif===1049###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");rc = bind(fd, iai->ai_addr, iai->ai_addrlen);

			}
			else {
								AKA_mark("lis===205###sois===4434###eois===4482###lif===43###soif===1066###eoif===1114###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");rc = connect(fd, iai->ai_addr, iai->ai_addrlen);

			}

						if (AKA_mark("lis===207###sois===4495###eois===4502###lif===45###soif===1127###eoif===1134###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)") && (AKA_mark("lis===207###sois===4495###eois===4502###lif===45###soif===1127###eoif===1134###isc===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)")&&rc == 0)) {
								AKA_mark("lis===208###sois===4510###eois===4528###lif===46###soif===1142###eoif===1160###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");	AKA_mark("lis===215###sois===4593###eois===4611###lif===53###soif===1225###eoif===1243###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");freeaddrinfo(rai);


								AKA_mark("lis===209###sois===4533###eois===4543###lif===47###soif===1165###eoif===1175###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");return fd;

			}
			else {AKA_mark("lis===-207-###sois===-4495-###eois===-44957-###lif===-45-###soif===-###eoif===-1134-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");}

						AKA_mark("lis===211###sois===4552###eois===4562###lif===49###soif===1184###eoif===1194###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");close(fd);

		}
		else {AKA_mark("lis===-197-###sois===-4237-###eois===-42377-###lif===-35-###soif===-###eoif===-876-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");}

				AKA_mark("lis===213###sois===4569###eois===4588###lif===51###soif===1201###eoif===1220###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");iai = iai->ai_next;

	}

	freeaddrinfo(rai);
		AKA_mark("lis===216###sois===4613###eois===4623###lif===54###soif===1245###eoif===1255###ins===true###function===./app-framework-binder/src/afb-socket.c/open_tcp(const char*,int,int)");return -1;

}

/**
 * open a systemd socket for server
 *
 * @param spec the specification of the systemd name
 *
 * @return the file descriptor number of the socket or -1 in case of error
 */
/** Instrumented function open_systemd(const char*) */
static int open_systemd(const char *spec)
{AKA_mark("Calling: ./app-framework-binder/src/afb-socket.c/open_systemd(const char*)");AKA_fCall++;
#if defined(NO_SYSTEMD_ACTIVATION)
	errno = EAFNOSUPPORT;
	return -1;
#else
		AKA_mark("lis===232###sois===4926###eois===4955###lif===6###soif===121###eoif===150###ins===true###function===./app-framework-binder/src/afb-socket.c/open_systemd(const char*)");return systemd_fds_for(spec);

#endif
}

/******************************************************************************/

/**
 * Get the entry of the uri by searching to its prefix
 *
 * @param uri the searched uri
 * @param offset where to store the prefix length
 * @param scheme the default scheme to use if none is set in uri (can be NULL)
 *
 * @return the found entry or the default one
 */
/** Instrumented function get_entry(const char*,int*,const char*) */
static struct entry *get_entry(const char *uri, int *offset, const char *scheme)
{AKA_mark("Calling: ./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");AKA_fCall++;
		AKA_mark("lis===249###sois===5407###eois===5426###lif===2###soif===84###eoif===103###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");int len, i, deflen;


	/* search as prefix of URI */
		AKA_mark("lis===252###sois===5460###eois===5505###lif===5###soif===137###eoif===182###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");i = (int)(sizeof entries / sizeof * entries);

		while (AKA_mark("lis===253###sois===5514###eois===5515###lif===6###soif===191###eoif===192###ifc===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)") && (AKA_mark("lis===253###sois===5514###eois===5515###lif===6###soif===191###eoif===192###isc===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)")&&i)) {
				AKA_mark("lis===254###sois===5521###eois===5525###lif===7###soif===198###eoif===202###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");i--;

				AKA_mark("lis===255###sois===5528###eois===5565###lif===8###soif===205###eoif===242###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");len = (int)strlen(entries[i].prefix);

				if (AKA_mark("lis===256###sois===5572###eois===5609###lif===9###soif===249###eoif===286###ifc===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)") && (AKA_mark("lis===256###sois===5572###eois===5609###lif===9###soif===249###eoif===286###isc===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)")&&!strncmp(uri, entries[i].prefix, len))) {
			AKA_mark("lis===257###sois===5614###eois===5623###lif===10###soif===291###eoif===300###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");goto end;
		}
		else {AKA_mark("lis===-256-###sois===-5572-###eois===-557237-###lif===-9-###soif===-###eoif===-286-###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");}
 /* found */
	}


	/* not a prefix of uri */
		AKA_mark("lis===261###sois===5668###eois===5676###lif===14###soif===345###eoif===353###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");len = 0;


	/* search default scheme if given and valid */
		if (AKA_mark("lis===264###sois===5731###eois===5748###lif===17###soif===408###eoif===425###ifc===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)") && ((AKA_mark("lis===264###sois===5731###eois===5737###lif===17###soif===408###eoif===414###isc===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)")&&scheme)	&&(AKA_mark("lis===264###sois===5741###eois===5748###lif===17###soif===418###eoif===425###isc===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)")&&*scheme))) {
				AKA_mark("lis===265###sois===5754###eois===5783###lif===18###soif===431###eoif===460###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");deflen = (int)strlen(scheme);

				AKA_mark("lis===266###sois===5786###eois===5824###lif===19###soif===463###eoif===501###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");deflen += (scheme[deflen - 1] != ':');
 /* add virtual trailing colon */
				AKA_mark("lis===267###sois===5860###eois===5905###lif===20###soif===537###eoif===582###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");i = (int)(sizeof entries / sizeof * entries);

				while (AKA_mark("lis===268###sois===5915###eois===5916###lif===21###soif===592###eoif===593###ifc===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)") && (AKA_mark("lis===268###sois===5915###eois===5916###lif===21###soif===592###eoif===593###isc===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)")&&i)) {
						AKA_mark("lis===269###sois===5923###eois===5927###lif===22###soif===600###eoif===604###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");i--;

						if (AKA_mark("lis===270###sois===5935###eois===6030###lif===23###soif===612###eoif===707###ifc===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)") && ((AKA_mark("lis===270###sois===5935###eois===5975###lif===23###soif===612###eoif===652###isc===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)")&&deflen == (int)strlen(entries[i].prefix))	&&(AKA_mark("lis===271###sois===5983###eois===6030###lif===24###soif===660###eoif===707###isc===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)")&&!strncmp(scheme, entries[i].prefix, deflen - 1)))) {
				AKA_mark("lis===272###sois===6036###eois===6045###lif===25###soif===713###eoif===722###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");goto end;
			}
			else {AKA_mark("lis===-270-###sois===-5935-###eois===-593595-###lif===-23-###soif===-###eoif===-707-###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");}
 /* found */
		}

	}
	else {AKA_mark("lis===-264-###sois===-5731-###eois===-573117-###lif===-17-###soif===-###eoif===-425-###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");}


	end:
	AKA_mark("lis===277###sois===6072###eois===6086###lif===30###soif===749###eoif===763###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");*offset = len;

		AKA_mark("lis===278###sois===6088###eois===6107###lif===31###soif===765###eoif===784###ins===true###function===./app-framework-binder/src/afb-socket.c/get_entry(const char*,int*,const char*)");return &entries[i];

}

/**
 * open socket for client or server
 *
 * @param uri the specification of the socket
 * @param server 0 for client, server otherwise
 * @param scheme the default scheme to use if none is set in uri (can be NULL)
 *
 * @return the file descriptor number of the socket or -1 in case of error
 */
/** Instrumented function open_uri(const char*,int,const char*) */
static int open_uri(const char *uri, int server, const char *scheme)
{AKA_mark("Calling: ./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");AKA_fCall++;
		AKA_mark("lis===292###sois===6481###eois===6496###lif===2###soif===72###eoif===87###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");int fd, offset;

		AKA_mark("lis===293###sois===6498###eois===6514###lif===3###soif===89###eoif===105###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");struct entry *e;

		AKA_mark("lis===294###sois===6516###eois===6532###lif===4###soif===107###eoif===123###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");const char *api;


	/* search for the entry */
		AKA_mark("lis===297###sois===6563###eois===6599###lif===7###soif===154###eoif===190###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");e = get_entry(uri, &offset, scheme);


	/* get the names */
		AKA_mark("lis===300###sois===6623###eois===6637###lif===10###soif===214###eoif===228###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");uri += offset;

		AKA_mark("lis===301###sois===6639###eois===6665###lif===11###soif===230###eoif===256###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");api = strstr(uri, as_api);

		if (AKA_mark("lis===302###sois===6671###eois===6674###lif===12###soif===262###eoif===265###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)") && (AKA_mark("lis===302###sois===6671###eois===6674###lif===12###soif===262###eoif===265###isc===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)")&&api)) {
		AKA_mark("lis===303###sois===6678###eois===6709###lif===13###soif===269###eoif===300###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");uri = strndupa(uri, api - uri);
	}
	else {AKA_mark("lis===-302-###sois===-6671-###eois===-66713-###lif===-12-###soif===-###eoif===-265-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");}


	/* open the socket */
		AKA_mark("lis===306###sois===6743###eois===6750###lif===16###soif===334###eoif===341###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");switch(e->type){
			case Type_Unix: if(e->type == Type_Unix)AKA_mark("lis===307###sois===6755###eois===6770###lif===17###soif===346###eoif===361###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");

				AKA_mark("lis===308###sois===6773###eois===6801###lif===18###soif===364###eoif===392###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");fd = open_unix(uri, server);

				AKA_mark("lis===309###sois===6804###eois===6810###lif===19###soif===395###eoif===401###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");break;

			case Type_Inet: if(e->type == Type_Inet)AKA_mark("lis===310###sois===6812###eois===6827###lif===20###soif===403###eoif===418###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");

				AKA_mark("lis===311###sois===6830###eois===6874###lif===21###soif===421###eoif===465###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");fd = open_tcp(uri, server, !e->noreuseaddr);

				AKA_mark("lis===312###sois===6877###eois===6883###lif===22###soif===468###eoif===474###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");break;

			case Type_Systemd: if(e->type == Type_Systemd)AKA_mark("lis===313###sois===6885###eois===6903###lif===23###soif===476###eoif===494###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");

				if (AKA_mark("lis===314###sois===6910###eois===6916###lif===24###soif===501###eoif===507###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)") && (AKA_mark("lis===314###sois===6910###eois===6916###lif===24###soif===501###eoif===507###isc===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)")&&server)) {
			AKA_mark("lis===315###sois===6921###eois===6944###lif===25###soif===512###eoif===535###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");fd = open_systemd(uri);
		}
		else {
						AKA_mark("lis===317###sois===6957###eois===6972###lif===27###soif===548###eoif===563###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");errno = EINVAL;

						AKA_mark("lis===318###sois===6976###eois===6984###lif===28###soif===567###eoif===575###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");fd = -1;

		}

				AKA_mark("lis===320###sois===6991###eois===6997###lif===30###soif===582###eoif===588###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");break;

			default: if(e->type != Type_Unix && e->type != Type_Inet && e->type != Type_Systemd)AKA_mark("lis===321###sois===6999###eois===7007###lif===31###soif===590###eoif===598###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");

				AKA_mark("lis===322###sois===7010###eois===7031###lif===32###soif===601###eoif===622###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");errno = EAFNOSUPPORT;

				AKA_mark("lis===323###sois===7034###eois===7042###lif===33###soif===625###eoif===633###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");fd = -1;

				AKA_mark("lis===324###sois===7045###eois===7051###lif===34###soif===636###eoif===642###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");break;

	}

		if (AKA_mark("lis===326###sois===7060###eois===7066###lif===36###soif===651###eoif===657###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)") && (AKA_mark("lis===326###sois===7060###eois===7066###lif===36###soif===651###eoif===657###isc===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)")&&fd < 0)) {
		AKA_mark("lis===327###sois===7070###eois===7080###lif===37###soif===661###eoif===671###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");return -1;
	}
	else {AKA_mark("lis===-326-###sois===-7060-###eois===-70606-###lif===-36-###soif===-###eoif===-657-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");}


	/* set it up */
		AKA_mark("lis===330###sois===7100###eois===7131###lif===40###soif===691###eoif===722###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");fcntl(fd, F_SETFD, FD_CLOEXEC);

		AKA_mark("lis===331###sois===7133###eois===7164###lif===41###soif===724###eoif===755###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");fcntl(fd, F_SETFL, O_NONBLOCK);

		if (AKA_mark("lis===332###sois===7170###eois===7176###lif===42###soif===761###eoif===767###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)") && (AKA_mark("lis===332###sois===7170###eois===7176###lif===42###soif===761###eoif===767###isc===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)")&&server)) {
				if (AKA_mark("lis===333###sois===7186###eois===7198###lif===43###soif===777###eoif===789###ifc===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)") && (AKA_mark("lis===333###sois===7186###eois===7198###lif===43###soif===777###eoif===789###isc===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)")&&!e->nolisten)) {
			AKA_mark("lis===334###sois===7203###eois===7223###lif===44###soif===794###eoif===814###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");listen(fd, BACKLOG);
		}
		else {AKA_mark("lis===-333-###sois===-7186-###eois===-718612-###lif===-43-###soif===-###eoif===-789-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");}

	}
	else {AKA_mark("lis===-332-###sois===-7170-###eois===-71706-###lif===-42-###soif===-###eoif===-767-###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");}

		AKA_mark("lis===336###sois===7228###eois===7238###lif===46###soif===819###eoif===829###ins===true###function===./app-framework-binder/src/afb-socket.c/open_uri(const char*,int,const char*)");return fd;

}

/**
 * open socket for client or server
 *
 * @param uri the specification of the socket
 * @param server 0 for client, server otherwise
 * @param scheme the default scheme to use if none is set in uri (can be NULL)
 *
 * @return the file descriptor number of the socket or -1 in case of error
 */
/** Instrumented function afb_socket_open_scheme(const char*,int,const char*) */
int afb_socket_open_scheme(const char *uri, int server, const char *scheme)
{AKA_mark("Calling: ./app-framework-binder/src/afb-socket.c/afb_socket_open_scheme(const char*,int,const char*)");AKA_fCall++;
		AKA_mark("lis===350###sois===7619###eois===7658###lif===2###soif===79###eoif===118###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_scheme(const char*,int,const char*)");int fd = open_uri(uri, server, scheme);

		if (AKA_mark("lis===351###sois===7664###eois===7670###lif===3###soif===124###eoif===130###ifc===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_scheme(const char*,int,const char*)") && (AKA_mark("lis===351###sois===7664###eois===7670###lif===3###soif===124###eoif===130###isc===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_scheme(const char*,int,const char*)")&&fd < 0)) {
		AKA_mark("lis===352###sois===7674###eois===7746###lif===4###soif===134###eoif===206###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_scheme(const char*,int,const char*)");ERROR("can't open %s socket for %s", server ? "server" : "client", uri);
	}
	else {AKA_mark("lis===-351-###sois===-7664-###eois===-76646-###lif===-3-###soif===-###eoif===-130-###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_scheme(const char*,int,const char*)");}

		AKA_mark("lis===353###sois===7748###eois===7758###lif===5###soif===208###eoif===218###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_scheme(const char*,int,const char*)");return fd;

}

/**
 * open socket for client or server
 *
 * @param uri the specification of the socket
 * @param server 0 for client, server otherwise
 * @param scheme the default scheme to use if none is set in uri (can be NULL)
 *
 * @return the fdev of the socket or NULL in case of error
 */
/** Instrumented function afb_socket_open_fdev_scheme(const char*,int,const char*) */
struct fdev *afb_socket_open_fdev_scheme(const char *uri, int server, const char *scheme)
{AKA_mark("Calling: ./app-framework-binder/src/afb-socket.c/afb_socket_open_fdev_scheme(const char*,int,const char*)");AKA_fCall++;
		AKA_mark("lis===367###sois===8137###eois===8155###lif===2###soif===93###eoif===111###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_fdev_scheme(const char*,int,const char*)");struct fdev *fdev;

		AKA_mark("lis===368###sois===8157###eois===8164###lif===3###soif===113###eoif===120###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_fdev_scheme(const char*,int,const char*)");int fd;


		AKA_mark("lis===370###sois===8167###eois===8216###lif===5###soif===123###eoif===172###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_fdev_scheme(const char*,int,const char*)");fd = afb_socket_open_scheme(uri, server, scheme);

		if (AKA_mark("lis===371###sois===8222###eois===8228###lif===6###soif===178###eoif===184###ifc===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_fdev_scheme(const char*,int,const char*)") && (AKA_mark("lis===371###sois===8222###eois===8228###lif===6###soif===178###eoif===184###isc===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_fdev_scheme(const char*,int,const char*)")&&fd < 0)) {
		AKA_mark("lis===372###sois===8232###eois===8244###lif===7###soif===188###eoif===200###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_fdev_scheme(const char*,int,const char*)");fdev = NULL;
	}
	else {
				AKA_mark("lis===374###sois===8255###eois===8282###lif===9###soif===211###eoif===238###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_fdev_scheme(const char*,int,const char*)");fdev = afb_fdev_create(fd);

				if (AKA_mark("lis===375###sois===8289###eois===8294###lif===10###soif===245###eoif===250###ifc===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_fdev_scheme(const char*,int,const char*)") && (AKA_mark("lis===375###sois===8289###eois===8294###lif===10###soif===245###eoif===250###isc===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_fdev_scheme(const char*,int,const char*)")&&!fdev)) {
						AKA_mark("lis===376###sois===8301###eois===8311###lif===11###soif===257###eoif===267###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_fdev_scheme(const char*,int,const char*)");close(fd);

						AKA_mark("lis===377###sois===8315###eois===8387###lif===12###soif===271###eoif===343###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_fdev_scheme(const char*,int,const char*)");ERROR("can't make %s socket for %s", server ? "server" : "client", uri);

		}
		else {AKA_mark("lis===-375-###sois===-8289-###eois===-82895-###lif===-10-###soif===-###eoif===-250-###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_fdev_scheme(const char*,int,const char*)");}

	}

		AKA_mark("lis===380###sois===8396###eois===8408###lif===15###soif===352###eoif===364###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_open_fdev_scheme(const char*,int,const char*)");return fdev;

}

/**
 * Get the api name of the uri
 *
 * @param uri the specification of the socket
 *
 * @return the api name or NULL if none can be deduced
 */
/** Instrumented function afb_socket_api(const char*) */
const char *afb_socket_api(const char *uri)
{AKA_mark("Calling: ./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");AKA_fCall++;
		AKA_mark("lis===392###sois===8605###eois===8616###lif===2###soif===47###eoif===58###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");int offset;

		AKA_mark("lis===393###sois===8618###eois===8634###lif===3###soif===60###eoif===76###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");const char *api;

		AKA_mark("lis===394###sois===8636###eois===8656###lif===4###soif===78###eoif===98###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");struct entry *entry;


		AKA_mark("lis===396###sois===8659###eois===8697###lif===6###soif===101###eoif===139###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");entry = get_entry(uri, &offset, NULL);

		AKA_mark("lis===397###sois===8699###eois===8713###lif===7###soif===141###eoif===155###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");uri += offset;

		AKA_mark("lis===398###sois===8715###eois===8764###lif===8###soif===157###eoif===206###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");uri += (entry->type == Type_Unix && *uri == '@');

		AKA_mark("lis===399###sois===8766###eois===8792###lif===9###soif===208###eoif===234###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");api = strstr(uri, as_api);

		if (AKA_mark("lis===400###sois===8798###eois===8801###lif===10###soif===240###eoif===243###ifc===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)") && (AKA_mark("lis===400###sois===8798###eois===8801###lif===10###soif===240###eoif===243###isc===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)")&&api)) {
		AKA_mark("lis===401###sois===8805###eois===8830###lif===11###soif===247###eoif===272###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");api += sizeof as_api - 1;
	}
	else {
				AKA_mark("lis===403###sois===8841###eois===8865###lif===13###soif===283###eoif===307###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");api = strrchr(uri, '/');

				if (AKA_mark("lis===404###sois===8872###eois===8875###lif===14###soif===314###eoif===317###ifc===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)") && (AKA_mark("lis===404###sois===8872###eois===8875###lif===14###soif===314###eoif===317###isc===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)")&&api)) {
			AKA_mark("lis===405###sois===8880###eois===8886###lif===15###soif===322###eoif===328###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");api++;
		}
		else {
			AKA_mark("lis===407###sois===8897###eois===8907###lif===17###soif===339###eoif===349###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");api = uri;
		}

				if (AKA_mark("lis===408###sois===8914###eois===8930###lif===18###soif===356###eoif===372###ifc===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)") && (AKA_mark("lis===408###sois===8914###eois===8930###lif===18###soif===356###eoif===372###isc===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)")&&strchr(api, ':'))) {
			AKA_mark("lis===409###sois===8935###eois===8946###lif===19###soif===377###eoif===388###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");api = NULL;
		}
		else {AKA_mark("lis===-408-###sois===-8914-###eois===-891416-###lif===-18-###soif===-###eoif===-372-###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");}

	}

		AKA_mark("lis===411###sois===8951###eois===8962###lif===21###soif===393###eoif===404###ins===true###function===./app-framework-binder/src/afb-socket.c/afb_socket_api(const char*)");return api;

}

#endif

