/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_SYSTEMD_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_SYSTEMD_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <unistd.h>
#include <errno.h>

#include <systemd/sd-event.h>
#include <systemd/sd-bus.h>
#include <systemd/sd-daemon.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__SYSTEMD_H_
#define AKA_INCLUDE__SYSTEMD_H_
#include "systemd.akaignore.h"
#endif


/** Instrumented function sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**)) */
static struct sd_bus *sdbusopen(struct sd_bus **p, int (*f)(struct sd_bus **))
{AKA_mark("Calling: ./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");AKA_fCall++;
		AKA_mark("lis===31###sois===898###eois===905###lif===2###soif===82###eoif===89###ins===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");int rc;

		AKA_mark("lis===32###sois===907###eois===924###lif===3###soif===91###eoif===108###ins===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");struct sd_bus *r;


		AKA_mark("lis===34###sois===927###eois===934###lif===5###soif===111###eoif===118###ins===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");r = *p;

		if (AKA_mark("lis===35###sois===940###eois===942###lif===6###soif===124###eoif===126###ifc===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))") && (AKA_mark("lis===35###sois===940###eois===942###lif===6###soif===124###eoif===126###isc===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))")&&!r)) {
				AKA_mark("lis===36###sois===948###eois===959###lif===7###soif===132###eoif===143###ins===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");rc = f(&r);

				if (AKA_mark("lis===37###sois===966###eois===973###lif===8###soif===150###eoif===157###ifc===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))") && (AKA_mark("lis===37###sois===966###eois===973###lif===8###soif===150###eoif===157###isc===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))")&&rc >= 0)) {
						AKA_mark("lis===38###sois===980###eois===1037###lif===9###soif===164###eoif===221###ins===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");rc = sd_bus_attach_event(r, systemd_get_event_loop(), 0);

						if (AKA_mark("lis===39###sois===1045###eois===1051###lif===10###soif===229###eoif===235###ifc===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))") && (AKA_mark("lis===39###sois===1045###eois===1051###lif===10###soif===229###eoif===235###isc===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))")&&rc < 0)) {
								AKA_mark("lis===40###sois===1059###eois===1075###lif===11###soif===243###eoif===259###ins===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");sd_bus_unref(r);

								AKA_mark("lis===41###sois===1080###eois===1086###lif===12###soif===264###eoif===270###ins===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");r = 0;

			}
			else {AKA_mark("lis===-39-###sois===-1045-###eois===-10456-###lif===-10-###soif===-###eoif===-235-###ins===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");}

		}
		else {AKA_mark("lis===-37-###sois===-966-###eois===-9667-###lif===-8-###soif===-###eoif===-157-###ins===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");}

				if (AKA_mark("lis===44###sois===1102###eois===1108###lif===15###soif===286###eoif===292###ifc===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))") && (AKA_mark("lis===44###sois===1102###eois===1108###lif===15###soif===286###eoif===292###isc===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))")&&rc < 0)) {
			AKA_mark("lis===45###sois===1113###eois===1125###lif===16###soif===297###eoif===309###ins===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");errno = -rc;
		}
		else {AKA_mark("lis===-44-###sois===-1102-###eois===-11026-###lif===-15-###soif===-###eoif===-292-###ins===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");}

				AKA_mark("lis===46###sois===1128###eois===1135###lif===17###soif===312###eoif===319###ins===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");*p = r;

	}
	else {AKA_mark("lis===-35-###sois===-940-###eois===-9402-###lif===-6-###soif===-###eoif===-126-###ins===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");}

		AKA_mark("lis===48###sois===1140###eois===1149###lif===19###soif===324###eoif===333###ins===true###function===./app-framework-binder/src/systemd.c/sdbusopen(struct sd_bus**,int(*f)(struct sd_bus**))");return r;

}

/** Instrumented function systemd_get_event_loop() */
struct sd_event *systemd_get_event_loop()
{AKA_mark("Calling: ./app-framework-binder/src/systemd.c/systemd_get_event_loop()");AKA_fCall++;
		AKA_mark("lis===53###sois===1198###eois===1233###lif===2###soif===45###eoif===80###ins===true###function===./app-framework-binder/src/systemd.c/systemd_get_event_loop()");static struct sd_event *result = 0;

		AKA_mark("lis===54###sois===1235###eois===1242###lif===3###soif===82###eoif===89###ins===true###function===./app-framework-binder/src/systemd.c/systemd_get_event_loop()");int rc;


		if (AKA_mark("lis===56###sois===1249###eois===1256###lif===5###soif===96###eoif===103###ifc===true###function===./app-framework-binder/src/systemd.c/systemd_get_event_loop()") && (AKA_mark("lis===56###sois===1249###eois===1256###lif===5###soif===96###eoif===103###isc===true###function===./app-framework-binder/src/systemd.c/systemd_get_event_loop()")&&!result)) {
				AKA_mark("lis===57###sois===1262###eois===1289###lif===6###soif===109###eoif===136###ins===true###function===./app-framework-binder/src/systemd.c/systemd_get_event_loop()");rc = sd_event_new(&result);

				if (AKA_mark("lis===58###sois===1296###eois===1302###lif===7###soif===143###eoif===149###ifc===true###function===./app-framework-binder/src/systemd.c/systemd_get_event_loop()") && (AKA_mark("lis===58###sois===1296###eois===1302###lif===7###soif===143###eoif===149###isc===true###function===./app-framework-binder/src/systemd.c/systemd_get_event_loop()")&&rc < 0)) {
						AKA_mark("lis===59###sois===1309###eois===1321###lif===8###soif===156###eoif===168###ins===true###function===./app-framework-binder/src/systemd.c/systemd_get_event_loop()");errno = -rc;

						AKA_mark("lis===60###sois===1325###eois===1339###lif===9###soif===172###eoif===186###ins===true###function===./app-framework-binder/src/systemd.c/systemd_get_event_loop()");result = NULL;

		}
		else {AKA_mark("lis===-58-###sois===-1296-###eois===-12966-###lif===-7-###soif===-###eoif===-149-###ins===true###function===./app-framework-binder/src/systemd.c/systemd_get_event_loop()");}

	}
	else {AKA_mark("lis===-56-###sois===-1249-###eois===-12497-###lif===-5-###soif===-###eoif===-103-###ins===true###function===./app-framework-binder/src/systemd.c/systemd_get_event_loop()");}

		AKA_mark("lis===63###sois===1348###eois===1362###lif===12###soif===195###eoif===209###ins===true###function===./app-framework-binder/src/systemd.c/systemd_get_event_loop()");return result;

}

/** Instrumented function systemd_get_user_bus() */
struct sd_bus *systemd_get_user_bus()
{AKA_mark("Calling: ./app-framework-binder/src/systemd.c/systemd_get_user_bus()");AKA_fCall++;
		AKA_mark("lis===68###sois===1407###eois===1440###lif===2###soif===41###eoif===74###ins===true###function===./app-framework-binder/src/systemd.c/systemd_get_user_bus()");static struct sd_bus *result = 0;

		AKA_mark("lis===69###sois===1442###eois===1500###lif===3###soif===76###eoif===134###ins===true###function===./app-framework-binder/src/systemd.c/systemd_get_user_bus()");return sdbusopen((void*)&result, (void*)sd_bus_open_user);

}

/** Instrumented function systemd_get_system_bus() */
struct sd_bus *systemd_get_system_bus()
{AKA_mark("Calling: ./app-framework-binder/src/systemd.c/systemd_get_system_bus()");AKA_fCall++;
		AKA_mark("lis===74###sois===1547###eois===1580###lif===2###soif===43###eoif===76###ins===true###function===./app-framework-binder/src/systemd.c/systemd_get_system_bus()");static struct sd_bus *result = 0;

		AKA_mark("lis===75###sois===1582###eois===1642###lif===3###soif===78###eoif===138###ins===true###function===./app-framework-binder/src/systemd.c/systemd_get_system_bus()");return sdbusopen((void*)&result, (void*)sd_bus_open_system);

}

/** Instrumented function fds_names() */
static char **fds_names()
{AKA_mark("Calling: ./app-framework-binder/src/systemd.c/fds_names()");AKA_fCall++;
		AKA_mark("lis===80###sois===1675###eois===1693###lif===2###soif===29###eoif===47###ins===true###function===./app-framework-binder/src/systemd.c/fds_names()");static char *null;

		AKA_mark("lis===81###sois===1695###eois===1715###lif===3###soif===49###eoif===69###ins===true###function===./app-framework-binder/src/systemd.c/fds_names()");static char **names;


		AKA_mark("lis===83###sois===1718###eois===1725###lif===5###soif===72###eoif===79###ins===true###function===./app-framework-binder/src/systemd.c/fds_names()");int rc;


		if (AKA_mark("lis===85###sois===1732###eois===1738###lif===7###soif===86###eoif===92###ifc===true###function===./app-framework-binder/src/systemd.c/fds_names()") && (AKA_mark("lis===85###sois===1732###eois===1738###lif===7###soif===86###eoif===92###isc===true###function===./app-framework-binder/src/systemd.c/fds_names()")&&!names)) {
				AKA_mark("lis===86###sois===1744###eois===1785###lif===8###soif===98###eoif===139###ins===true###function===./app-framework-binder/src/systemd.c/fds_names()");rc = sd_listen_fds_with_names(1, &names);

				if (AKA_mark("lis===87###sois===1792###eois===1799###lif===9###soif===146###eoif===153###ifc===true###function===./app-framework-binder/src/systemd.c/fds_names()") && (AKA_mark("lis===87###sois===1792###eois===1799###lif===9###soif===146###eoif===153###isc===true###function===./app-framework-binder/src/systemd.c/fds_names()")&&rc <= 0)) {
						AKA_mark("lis===88###sois===1806###eois===1818###lif===10###soif===160###eoif===172###ins===true###function===./app-framework-binder/src/systemd.c/fds_names()");errno = -rc;

						AKA_mark("lis===89###sois===1822###eois===1836###lif===11###soif===176###eoif===190###ins===true###function===./app-framework-binder/src/systemd.c/fds_names()");names = &null;

		}
		else {AKA_mark("lis===-87-###sois===-1792-###eois===-17927-###lif===-9-###soif===-###eoif===-153-###ins===true###function===./app-framework-binder/src/systemd.c/fds_names()");}

	}
	else {AKA_mark("lis===-85-###sois===-1732-###eois===-17326-###lif===-7-###soif===-###eoif===-92-###ins===true###function===./app-framework-binder/src/systemd.c/fds_names()");}

		AKA_mark("lis===92###sois===1845###eois===1858###lif===14###soif===199###eoif===212###ins===true###function===./app-framework-binder/src/systemd.c/fds_names()");return names;

}

/** Instrumented function systemd_fds_init() */
int systemd_fds_init()
{AKA_mark("Calling: ./app-framework-binder/src/systemd.c/systemd_fds_init()");AKA_fCall++;
		AKA_mark("lis===97###sois===1888###eois===1898###lif===2###soif===26###eoif===36###ins===true###function===./app-framework-binder/src/systemd.c/systemd_fds_init()");errno = 0;

		AKA_mark("lis===98###sois===1900###eois===1912###lif===3###soif===38###eoif===50###ins===true###function===./app-framework-binder/src/systemd.c/systemd_fds_init()");fds_names();

		AKA_mark("lis===99###sois===1914###eois===1930###lif===4###soif===52###eoif===68###ins===true###function===./app-framework-binder/src/systemd.c/systemd_fds_init()");return -!!errno;

}

/** Instrumented function systemd_fds_for(const char*) */
int systemd_fds_for(const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/systemd.c/systemd_fds_for(const char*)");AKA_fCall++;
		AKA_mark("lis===104###sois===1975###eois===1983###lif===2###soif===41###eoif===49###ins===true###function===./app-framework-binder/src/systemd.c/systemd_fds_for(const char*)");int idx;

		AKA_mark("lis===105###sois===1985###eois===1998###lif===3###soif===51###eoif===64###ins===true###function===./app-framework-binder/src/systemd.c/systemd_fds_for(const char*)");char **names;


		AKA_mark("lis===107###sois===2001###eois===2021###lif===5###soif===67###eoif===87###ins===true###function===./app-framework-binder/src/systemd.c/systemd_fds_for(const char*)");names = fds_names();

		AKA_mark("lis===108###sois===2028###eois===2037###lif===6###soif===94###eoif===103###ins===true###function===./app-framework-binder/src/systemd.c/systemd_fds_for(const char*)");for (idx = 0 ;AKA_mark("lis===108###sois===2038###eois===2056###lif===6###soif===104###eoif===122###ifc===true###function===./app-framework-binder/src/systemd.c/systemd_fds_for(const char*)") && AKA_mark("lis===108###sois===2038###eois===2056###lif===6###soif===104###eoif===122###isc===true###function===./app-framework-binder/src/systemd.c/systemd_fds_for(const char*)")&&names[idx] != NULL;({AKA_mark("lis===108###sois===2059###eois===2064###lif===6###soif===125###eoif===130###ins===true###function===./app-framework-binder/src/systemd.c/systemd_fds_for(const char*)");idx++;})) {
		if (AKA_mark("lis===109###sois===2072###eois===2097###lif===7###soif===138###eoif===163###ifc===true###function===./app-framework-binder/src/systemd.c/systemd_fds_for(const char*)") && (AKA_mark("lis===109###sois===2072###eois===2097###lif===7###soif===138###eoif===163###isc===true###function===./app-framework-binder/src/systemd.c/systemd_fds_for(const char*)")&&!strcmp(name, names[idx]))) {
			AKA_mark("lis===110###sois===2102###eois===2135###lif===8###soif===168###eoif===201###ins===true###function===./app-framework-binder/src/systemd.c/systemd_fds_for(const char*)");return idx + SD_LISTEN_FDS_START;
		}
		else {AKA_mark("lis===-109-###sois===-2072-###eois===-207225-###lif===-7-###soif===-###eoif===-163-###ins===true###function===./app-framework-binder/src/systemd.c/systemd_fds_for(const char*)");}
	}


		AKA_mark("lis===112###sois===2138###eois===2153###lif===10###soif===204###eoif===219###ins===true###function===./app-framework-binder/src/systemd.c/systemd_fds_for(const char*)");errno = ENOENT;

		AKA_mark("lis===113###sois===2155###eois===2165###lif===11###soif===221###eoif===231###ins===true###function===./app-framework-binder/src/systemd.c/systemd_fds_for(const char*)");return -1;

}


#endif

