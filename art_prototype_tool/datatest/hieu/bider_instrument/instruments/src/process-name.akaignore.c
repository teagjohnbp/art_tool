/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_PROCESS_NAME_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_PROCESS_NAME_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/prctl.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__PROCESS_NAME_H_
#define AKA_INCLUDE__PROCESS_NAME_H_
#include "process-name.akaignore.h"
#endif


/** Instrumented function process_name_set_name(const char*) */
int process_name_set_name(const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/process-name.c/process_name_set_name(const char*)");AKA_fCall++;
		AKA_mark("lis===26###sois===758###eois===790###lif===2###soif===47###eoif===79###ins===true###function===./app-framework-binder/src/process-name.c/process_name_set_name(const char*)");return prctl(PR_SET_NAME, name);

}

/** Instrumented function process_name_replace_cmdline(char**,const char*) */
int process_name_replace_cmdline(char **argv, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");AKA_fCall++;
		AKA_mark("lis===31###sois===861###eois===886###lif===2###soif===67###eoif===92###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");char *beg, *end, **av, c;


	/* update the command line */
		AKA_mark("lis===34###sois===920###eois===930###lif===5###soif===126###eoif===136###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");av = argv;

		if (AKA_mark("lis===35###sois===936###eois===939###lif===6###soif===142###eoif===145###ifc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)") && (AKA_mark("lis===35###sois===936###eois===939###lif===6###soif===142###eoif===145###isc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)")&&!av)) {
				AKA_mark("lis===36###sois===945###eois===960###lif===7###soif===151###eoif===166###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");errno = EINVAL;

				AKA_mark("lis===37###sois===963###eois===973###lif===8###soif===169###eoif===179###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");return -1;
 /* no command line update required */
	}
	else {AKA_mark("lis===-35-###sois===-936-###eois===-9363-###lif===-6-###soif===-###eoif===-145-###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");}


	/* longest prefix */
		AKA_mark("lis===41###sois===1039###eois===1055###lif===12###soif===245###eoif===261###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");end = beg = *av;

		while (AKA_mark("lis===42###sois===1064###eois===1067###lif===13###soif===270###eoif===273###ifc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)") && (AKA_mark("lis===42###sois===1064###eois===1067###lif===13###soif===270###eoif===273###isc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)")&&*av)) {
		if (AKA_mark("lis===43###sois===1075###eois===1087###lif===14###soif===281###eoif===293###ifc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)") && (AKA_mark("lis===43###sois===1075###eois===1087###lif===14###soif===281###eoif===293###isc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)")&&*av++ == end)) {
			while (AKA_mark("lis===44###sois===1098###eois===1104###lif===15###soif===304###eoif===310###ifc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)") && (AKA_mark("lis===44###sois===1098###eois===1104###lif===15###soif===304###eoif===310###isc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)")&&*end++)) {
				;
			}
		}
		else {AKA_mark("lis===-43-###sois===-1075-###eois===-107512-###lif===-14-###soif===-###eoif===-293-###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");}
	}

		if (AKA_mark("lis===46###sois===1117###eois===1127###lif===17###soif===323###eoif===333###ifc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)") && (AKA_mark("lis===46###sois===1117###eois===1127###lif===17###soif===323###eoif===333###isc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)")&&end == beg)) {
				AKA_mark("lis===47###sois===1133###eois===1148###lif===18###soif===339###eoif===354###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");errno = EINVAL;

				AKA_mark("lis===48###sois===1151###eois===1161###lif===19###soif===357###eoif===367###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");return -1;
 /* nothing to change */
	}
	else {AKA_mark("lis===-46-###sois===-1117-###eois===-111710-###lif===-17-###soif===-###eoif===-333-###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");}


	/* patch the command line */
		AKA_mark("lis===52###sois===1221###eois===1235###lif===23###soif===427###eoif===441###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");av = &argv[1];

		AKA_mark("lis===53###sois===1237###eois===1243###lif===24###soif===443###eoif===449###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");end--;

		while (AKA_mark("lis===54###sois===1252###eois===1279###lif===25###soif===458###eoif===485###ifc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)") && ((AKA_mark("lis===54###sois===1252###eois===1262###lif===25###soif===458###eoif===468###isc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)")&&beg != end)	&&(c=(*name++) && AKA_mark("lis===54###sois===1267###eois===1278###lif===25###soif===473###eoif===484###isc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)")))) {
				if (AKA_mark("lis===55###sois===1289###eois===1305###lif===26###soif===495###eoif===511###ifc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)") && ((AKA_mark("lis===55###sois===1289###eois===1297###lif===26###soif===495###eoif===503###isc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)")&&c != ' ')	||(AKA_mark("lis===55###sois===1301###eois===1305###lif===26###soif===507###eoif===511###isc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)")&&!*av))) {
			AKA_mark("lis===56###sois===1310###eois===1321###lif===27###soif===516###eoif===527###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");*beg++ = c;
		}
		else {
						AKA_mark("lis===58###sois===1334###eois===1345###lif===29###soif===540###eoif===551###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");*beg++ = 0;

						AKA_mark("lis===59###sois===1349###eois===1361###lif===30###soif===555###eoif===567###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");*av++ = beg;

		}

	}

	/* terminate last arg */
		if (AKA_mark("lis===63###sois===1400###eois===1410###lif===34###soif===606###eoif===616###ifc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)") && (AKA_mark("lis===63###sois===1400###eois===1410###lif===34###soif===606###eoif===616###isc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)")&&beg != end)) {
		AKA_mark("lis===64###sois===1414###eois===1425###lif===35###soif===620###eoif===631###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");*beg++ = 0;
	}
	else {AKA_mark("lis===-63-###sois===-1400-###eois===-140010-###lif===-34-###soif===-###eoif===-616-###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");}

	/* update remaining args (for keeping initial length correct) */
		while (AKA_mark("lis===66###sois===1500###eois===1503###lif===37###soif===706###eoif===709###ifc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)") && (AKA_mark("lis===66###sois===1500###eois===1503###lif===37###soif===706###eoif===709###isc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)")&&*av)) {
		AKA_mark("lis===67###sois===1507###eois===1519###lif===38###soif===713###eoif===725###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");*av++ = beg;
	}

	/* fulfill last arg with spaces */
		while (AKA_mark("lis===69###sois===1564###eois===1574###lif===40###soif===770###eoif===780###ifc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)") && (AKA_mark("lis===69###sois===1564###eois===1574###lif===40###soif===770###eoif===780###isc===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)")&&beg != end)) {
		AKA_mark("lis===70###sois===1578###eois===1591###lif===41###soif===784###eoif===797###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");*beg++ = ' ';
	}

		AKA_mark("lis===71###sois===1593###eois===1602###lif===42###soif===799###eoif===808###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");*beg = 0;


		AKA_mark("lis===73###sois===1605###eois===1614###lif===44###soif===811###eoif===820###ins===true###function===./app-framework-binder/src/process-name.c/process_name_replace_cmdline(char**,const char*)");return 0;

}


#endif

