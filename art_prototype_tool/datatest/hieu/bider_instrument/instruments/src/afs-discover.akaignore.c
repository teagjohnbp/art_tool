/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFS_DISCOVER_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFS_DISCOVER_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>

/** Instrumented function afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*) */
void afs_discover(const char *pattern, void (*callback)(void *closure, pid_t pid), void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");AKA_fCall++;
		AKA_mark("lis===31###sois===927###eois===938###lif===2###soif===101###eoif===112###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");intmax_t n;

		AKA_mark("lis===32###sois===940###eois===949###lif===3###soif===114###eoif===123###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");DIR *dir;

		AKA_mark("lis===33###sois===951###eois===970###lif===4###soif===125###eoif===144###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");struct dirent *ent;

		AKA_mark("lis===34###sois===972###eois===983###lif===5###soif===146###eoif===157###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");char *name;

		AKA_mark("lis===35###sois===985###eois===1019###lif===6###soif===159###eoif===193###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");char exe[PATH_MAX], lnk[PATH_MAX];


		AKA_mark("lis===37###sois===1022###eois===1045###lif===8###soif===196###eoif===219###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");dir = opendir("/proc");

		while (AKA_mark("lis===38###sois===1054###eois===1074###lif===9###soif===228###eoif===248###ifc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)") && (ent=(readdir(dir)) && AKA_mark("lis===38###sois===1055###eois===1073###lif===9###soif===229###eoif===247###isc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)"))) {
				AKA_mark("lis===39###sois===1080###eois===1099###lif===10###soif===254###eoif===273###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");name = ent->d_name;

				while (AKA_mark("lis===40###sois===1109###eois===1123###lif===11###soif===283###eoif===297###ifc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)") && (AKA_mark("lis===40###sois===1109###eois===1123###lif===11###soif===283###eoif===297###isc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)")&&isdigit(*name))) {
			AKA_mark("lis===41###sois===1128###eois===1135###lif===12###soif===302###eoif===309###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");name++;
		}

				if (AKA_mark("lis===42###sois===1142###eois===1147###lif===13###soif===316###eoif===321###ifc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)") && (AKA_mark("lis===42###sois===1142###eois===1147###lif===13###soif===316###eoif===321###isc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)")&&*name)) {
			AKA_mark("lis===43###sois===1152###eois===1161###lif===14###soif===326###eoif===335###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");continue;
		}
		else {AKA_mark("lis===-42-###sois===-1142-###eois===-11425-###lif===-13-###soif===-###eoif===-321-###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");}

				AKA_mark("lis===44###sois===1164###eois===1223###lif===15###soif===338###eoif===397###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");n = snprintf(exe, sizeof exe, "/proc/%s/exe", ent->d_name);

				if (AKA_mark("lis===45###sois===1230###eois===1262###lif===16###soif===404###eoif===436###ifc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)") && ((AKA_mark("lis===45###sois===1230###eois===1235###lif===16###soif===404###eoif===409###isc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)")&&n < 0)	||(AKA_mark("lis===45###sois===1239###eois===1262###lif===16###soif===413###eoif===436###isc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)")&&(size_t)n >= sizeof exe))) {
			AKA_mark("lis===46###sois===1267###eois===1276###lif===17###soif===441###eoif===450###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");continue;
		}
		else {AKA_mark("lis===-45-###sois===-1230-###eois===-123032-###lif===-16-###soif===-###eoif===-436-###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");}

				AKA_mark("lis===47###sois===1279###eois===1314###lif===18###soif===453###eoif===488###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");n = readlink(exe, lnk, sizeof lnk);

				if (AKA_mark("lis===48###sois===1321###eois===1353###lif===19###soif===495###eoif===527###ifc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)") && ((AKA_mark("lis===48###sois===1321###eois===1326###lif===19###soif===495###eoif===500###isc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)")&&n < 0)	||(AKA_mark("lis===48###sois===1330###eois===1353###lif===19###soif===504###eoif===527###isc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)")&&(size_t)n >= sizeof lnk))) {
			AKA_mark("lis===49###sois===1358###eois===1367###lif===20###soif===532###eoif===541###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");continue;
		}
		else {AKA_mark("lis===-48-###sois===-1321-###eois===-132132-###lif===-19-###soif===-###eoif===-527-###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");}

				AKA_mark("lis===50###sois===1370###eois===1381###lif===21###soif===544###eoif===555###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");lnk[n] = 0;

				AKA_mark("lis===51###sois===1384###eois===1395###lif===22###soif===558###eoif===569###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");name = lnk;

				while (AKA_mark("lis===52###sois===1404###eois===1409###lif===23###soif===578###eoif===583###ifc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)") && (AKA_mark("lis===52###sois===1404###eois===1409###lif===23###soif===578###eoif===583###isc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)")&&*name)) {
						while (AKA_mark("lis===53###sois===1422###eois===1434###lif===24###soif===596###eoif===608###ifc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)") && (AKA_mark("lis===53###sois===1422###eois===1434###lif===24###soif===596###eoif===608###isc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)")&&*name == '/')) {
				AKA_mark("lis===54###sois===1440###eois===1447###lif===25###soif===614###eoif===621###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");name++;
			}

						if (AKA_mark("lis===55###sois===1455###eois===1460###lif===26###soif===629###eoif===634###ifc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)") && (AKA_mark("lis===55###sois===1455###eois===1460###lif===26###soif===629###eoif===634###isc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)")&&*name)) {
								if (AKA_mark("lis===56###sois===1472###eois===1494###lif===27###soif===646###eoif===668###ifc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)") && (AKA_mark("lis===56###sois===1472###eois===1494###lif===27###soif===646###eoif===668###isc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)")&&!strcmp(name, pattern))) {
										AKA_mark("lis===57###sois===1503###eois===1547###lif===28###soif===677###eoif===721###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");callback(closure, (pid_t)atoi(ent->d_name));

										AKA_mark("lis===58###sois===1553###eois===1559###lif===29###soif===727###eoif===733###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");break;

				}
				else {AKA_mark("lis===-56-###sois===-1472-###eois===-147222-###lif===-27-###soif===-###eoif===-668-###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");}

								while (AKA_mark("lis===60###sois===1576###eois===1599###lif===31###soif===750###eoif===773###ifc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)") && ((AKA_mark("lis===60###sois===1576###eois===1583###lif===31###soif===750###eoif===757###isc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)")&&*++name)	&&(AKA_mark("lis===60###sois===1587###eois===1599###lif===31###soif===761###eoif===773###isc===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)")&&*name != '/'))) {
					;
				}

			}
			else {AKA_mark("lis===-55-###sois===-1455-###eois===-14555-###lif===-26-###soif===-###eoif===-634-###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");}

		}

	}

		AKA_mark("lis===64###sois===1615###eois===1629###lif===35###soif===789###eoif===803###ins===true###function===./app-framework-binder/src/afs-discover.c/afs_discover(const char*,void(*callback)(void*closure, pid_t pid),void*)");closedir(dir);

}


#endif

