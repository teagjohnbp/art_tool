/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_JOBS_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_JOBS_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <sys/syscall.h>
#include <pthread.h>
#include <errno.h>
#include <assert.h>
#include <sys/eventfd.h>

#include <systemd/sd-event.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__JOBS_H_
#define AKA_INCLUDE__JOBS_H_
#include "jobs.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__EVMGR_H_
#define AKA_INCLUDE__EVMGR_H_
#include "evmgr.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__SIG_MONITOR_H_
#define AKA_INCLUDE__SIG_MONITOR_H_
#include "sig-monitor.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__SYSTEMD_H_
#define AKA_INCLUDE__SYSTEMD_H_
#include "systemd.akaignore.h"
#endif


#define EVENT_TIMEOUT_TOP  	((uint64_t)-1)
#define EVENT_TIMEOUT_CHILD	((uint64_t)10000)

/** Internal shortcut for callback */
typedef void (*job_cb_t)(int, void*);

/** starting mode for jobs */
enum start_mode
{
	Start_Default,  /**< Start a thread if more than one jobs is pending */
	Start_Urgent,   /**< Always start a thread */
	Start_Lazy      /**< Never start a thread */
};

/** Description of a pending job */
struct job
{
	struct job *next;    /**< link to the next job enqueued */
	const void *group;   /**< group of the request */
	job_cb_t callback;   /**< processing callback */
	void *arg;           /**< argument */
	int timeout;         /**< timeout in second for processing the request */
	unsigned blocked: 1; /**< is an other request blocking this one ? */
	unsigned dropped: 1; /**< is removed ? */
};

/** Description of threads */
struct thread
{
	struct thread *next;   /**< next thread of the list */
	struct thread *upper;  /**< upper same thread */
	struct thread *nholder;/**< next holder for evloop */
	pthread_cond_t *cwhold;/**< condition wait for holding */
	struct job *job;       /**< currently processed job */
	pthread_t tid;         /**< the thread id */
	volatile unsigned stop: 1;      /**< stop requested */
	volatile unsigned waits: 1;     /**< is waiting? */
	volatile unsigned leaved: 1;    /**< was leaved? */
};

/**
 * Description of synchronous callback
 */
struct sync
{
	struct thread thread;	/**< thread loop data */
	union {
		void (*callback)(int, void*);	/**< the synchronous callback */
		void (*enter)(int signum, void *closure, struct jobloop *jobloop);
				/**< the entering synchronous routine */
	};
	void *arg;		/**< the argument of the callback */
};

/* synchronisation of threads */
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t  cond = PTHREAD_COND_INITIALIZER;

/* counts for threads */
static int allowed_thread_count = 0; /** allowed count of threads */
static int started_thread_count = 0; /** started count of threads */
static int busy_thread_count = 0;    /** count of busy threads */

/* list of threads */
static struct thread *threads;
static _Thread_local struct thread *current_thread;

/* counts for jobs */
static int remaining_job_count = 0;  /** count of job that can be created */
static int allowed_job_count = 0;    /** allowed count of pending jobs */

/* queue of pending jobs */
static struct job *first_pending_job;
static struct job *first_free_job;

/* event loop */
static struct evmgr *evmgr;

static void (*exit_handler)();

/**
 * Create a new job with the given parameters
 * @param group    the group of the job
 * @param timeout  the timeout of the job (0 if none)
 * @param callback the function that achieves the job
 * @param arg      the argument of the callback
 * @return the created job unblock or NULL when no more memory
 */
/** Instrumented function job_create(const void*,int,job_cb_t,void*) */
static struct job *job_create(
		const void *group,
		int timeout,
		job_cb_t callback,
		void *arg)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");AKA_fCall++;
		AKA_mark("lis===134###sois===3999###eois===4015###lif===6###soif===104###eoif===120###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");struct job *job;


	/* try recyle existing job */
		AKA_mark("lis===137###sois===4049###eois===4070###lif===9###soif===154###eoif===175###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");job = first_free_job;

		if (AKA_mark("lis===138###sois===4076###eois===4079###lif===10###soif===181###eoif===184###ifc===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)") && (AKA_mark("lis===138###sois===4076###eois===4079###lif===10###soif===181###eoif===184###isc===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)")&&job)) {
		AKA_mark("lis===139###sois===4083###eois===4110###lif===11###soif===188###eoif===215###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");first_free_job = job->next;
	}
	else {
		/* allocation without blocking */
				AKA_mark("lis===142###sois===4157###eois===4186###lif===14###soif===262###eoif===291###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");pthread_mutex_unlock(&mutex);

				AKA_mark("lis===143###sois===4189###eois===4215###lif===15###soif===294###eoif===320###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");job = malloc(sizeof *job);

				AKA_mark("lis===144###sois===4218###eois===4245###lif===16###soif===323###eoif===350###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");pthread_mutex_lock(&mutex);

				if (AKA_mark("lis===145###sois===4252###eois===4256###lif===17###soif===357###eoif===361###ifc===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)") && (AKA_mark("lis===145###sois===4252###eois===4256###lif===17###soif===357###eoif===361###isc===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)")&&!job)) {
						AKA_mark("lis===146###sois===4263###eois===4286###lif===18###soif===368###eoif===391###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");ERROR("out of memory");

						AKA_mark("lis===147###sois===4290###eois===4305###lif===19###soif===395###eoif===410###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");errno = ENOMEM;

						AKA_mark("lis===148###sois===4309###eois===4318###lif===20###soif===414###eoif===423###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");goto end;

		}
		else {AKA_mark("lis===-145-###sois===-4252-###eois===-42524-###lif===-17-###soif===-###eoif===-361-###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");}

	}

	/* initialises the job */
		AKA_mark("lis===152###sois===4354###eois===4373###lif===24###soif===459###eoif===478###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");job->group = group;

		AKA_mark("lis===153###sois===4375###eois===4398###lif===25###soif===480###eoif===503###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");job->timeout = timeout;

		AKA_mark("lis===154###sois===4400###eois===4425###lif===26###soif===505###eoif===530###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");job->callback = callback;

		AKA_mark("lis===155###sois===4427###eois===4442###lif===27###soif===532###eoif===547###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");job->arg = arg;

		AKA_mark("lis===156###sois===4444###eois===4461###lif===28###soif===549###eoif===566###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");job->blocked = 0;

		AKA_mark("lis===157###sois===4463###eois===4480###lif===29###soif===568###eoif===585###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");job->dropped = 0;

	end:
	AKA_mark("lis===159###sois===4487###eois===4498###lif===31###soif===592###eoif===603###ins===true###function===./app-framework-binder/src/jobs.c/job_create(const void*,int,job_cb_t,void*)");return job;

}

/**
 * Adds 'job' at the end of the list of jobs, marking it
 * as blocked if an other job with the same group is pending.
 * @param job the job to add
 */
/** Instrumented function job_add(struct job*) */
static void job_add(struct job *job)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/job_add(struct job*)");AKA_fCall++;
		AKA_mark("lis===169###sois===4698###eois===4716###lif===2###soif===40###eoif===58###ins===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)");const void *group;

		AKA_mark("lis===170###sois===4718###eois===4743###lif===3###soif===60###eoif===85###ins===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)");struct job *ijob, **pjob;


	/* prepare to add */
		AKA_mark("lis===173###sois===4768###eois===4787###lif===6###soif===110###eoif===129###ins===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)");group = job->group;

		AKA_mark("lis===174###sois===4789###eois===4806###lif===7###soif===131###eoif===148###ins===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)");job->next = NULL;


	/* search end and blockers */
		AKA_mark("lis===177###sois===4840###eois===4866###lif===10###soif===182###eoif===208###ins===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)");pjob = &first_pending_job;

		AKA_mark("lis===178###sois===4868###eois===4893###lif===11###soif===210###eoif===235###ins===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)");ijob = first_pending_job;

		while (AKA_mark("lis===179###sois===4902###eois===4906###lif===12###soif===244###eoif===248###ifc===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)") && (AKA_mark("lis===179###sois===4902###eois===4906###lif===12###soif===244###eoif===248###isc===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)")&&ijob)) {
				if (AKA_mark("lis===180###sois===4916###eois===4945###lif===13###soif===258###eoif===287###ifc===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)") && ((AKA_mark("lis===180###sois===4916###eois===4921###lif===13###soif===258###eoif===263###isc===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)")&&group)	&&(AKA_mark("lis===180###sois===4925###eois===4945###lif===13###soif===267###eoif===287###isc===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)")&&ijob->group == group))) {
			AKA_mark("lis===181###sois===4950###eois===4967###lif===14###soif===292###eoif===309###ins===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)");job->blocked = 1;
		}
		else {AKA_mark("lis===-180-###sois===-4916-###eois===-491629-###lif===-13-###soif===-###eoif===-287-###ins===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)");}

				AKA_mark("lis===182###sois===4970###eois===4989###lif===15###soif===312###eoif===331###ins===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)");pjob = &ijob->next;

				AKA_mark("lis===183###sois===4992###eois===5010###lif===16###soif===334###eoif===352###ins===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)");ijob = ijob->next;

	}


	/* queue the jobs */
		AKA_mark("lis===187###sois===5038###eois===5050###lif===20###soif===380###eoif===392###ins===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)");*pjob = job;

		AKA_mark("lis===188###sois===5052###eois===5074###lif===21###soif===394###eoif===416###ins===true###function===./app-framework-binder/src/jobs.c/job_add(struct job*)");remaining_job_count--;

}

/**
 * Get the next job to process or NULL if none.
 * @return the first job that isn't blocked or NULL
 */
/** Instrumented function job_get() */
static inline struct job *job_get()
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/job_get()");AKA_fCall++;
		AKA_mark("lis===197###sois===5225###eois===5261###lif===2###soif===39###eoif===75###ins===true###function===./app-framework-binder/src/jobs.c/job_get()");struct job *job = first_pending_job;

		while (AKA_mark("lis===198###sois===5270###eois===5289###lif===3###soif===84###eoif===103###ifc===true###function===./app-framework-binder/src/jobs.c/job_get()") && ((AKA_mark("lis===198###sois===5270###eois===5273###lif===3###soif===84###eoif===87###isc===true###function===./app-framework-binder/src/jobs.c/job_get()")&&job)	&&(AKA_mark("lis===198###sois===5277###eois===5289###lif===3###soif===91###eoif===103###isc===true###function===./app-framework-binder/src/jobs.c/job_get()")&&job->blocked))) {
		AKA_mark("lis===199###sois===5293###eois===5309###lif===4###soif===107###eoif===123###ins===true###function===./app-framework-binder/src/jobs.c/job_get()");job = job->next;
	}

		if (AKA_mark("lis===200###sois===5315###eois===5318###lif===5###soif===129###eoif===132###ifc===true###function===./app-framework-binder/src/jobs.c/job_get()") && (AKA_mark("lis===200###sois===5315###eois===5318###lif===5###soif===129###eoif===132###isc===true###function===./app-framework-binder/src/jobs.c/job_get()")&&job)) {
		AKA_mark("lis===201###sois===5322###eois===5344###lif===6###soif===136###eoif===158###ins===true###function===./app-framework-binder/src/jobs.c/job_get()");remaining_job_count++;
	}
	else {AKA_mark("lis===-200-###sois===-5315-###eois===-53153-###lif===-5-###soif===-###eoif===-132-###ins===true###function===./app-framework-binder/src/jobs.c/job_get()");}

		AKA_mark("lis===202###sois===5346###eois===5357###lif===7###soif===160###eoif===171###ins===true###function===./app-framework-binder/src/jobs.c/job_get()");return job;

}

/**
 * Releases the processed 'job': removes it
 * from the list of jobs and unblock the first
 * pending job of the same group if any.
 * @param job the job to release
 */
/** Instrumented function job_release(struct job*) */
static inline void job_release(struct job *job)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/job_release(struct job*)");AKA_fCall++;
		AKA_mark("lis===213###sois===5585###eois===5610###lif===2###soif===51###eoif===76###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");struct job *ijob, **pjob;

		AKA_mark("lis===214###sois===5612###eois===5630###lif===3###soif===78###eoif===96###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");const void *group;


	/* first unqueue the job */
		AKA_mark("lis===217###sois===5662###eois===5688###lif===6###soif===128###eoif===154###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");pjob = &first_pending_job;

		AKA_mark("lis===218###sois===5690###eois===5715###lif===7###soif===156###eoif===181###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");ijob = first_pending_job;

		while (AKA_mark("lis===219###sois===5724###eois===5735###lif===8###soif===190###eoif===201###ifc===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)") && (AKA_mark("lis===219###sois===5724###eois===5735###lif===8###soif===190###eoif===201###isc===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)")&&ijob != job)) {
				AKA_mark("lis===220###sois===5741###eois===5760###lif===9###soif===207###eoif===226###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");pjob = &ijob->next;

				AKA_mark("lis===221###sois===5763###eois===5781###lif===10###soif===229###eoif===247###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");ijob = ijob->next;

	}

		AKA_mark("lis===223###sois===5786###eois===5804###lif===12###soif===252###eoif===270###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");*pjob = job->next;


	/* then unblock jobs of the same group */
		AKA_mark("lis===226###sois===5850###eois===5869###lif===15###soif===316###eoif===335###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");group = job->group;

		if (AKA_mark("lis===227###sois===5875###eois===5880###lif===16###soif===341###eoif===346###ifc===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)") && (AKA_mark("lis===227###sois===5875###eois===5880###lif===16###soif===341###eoif===346###isc===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)")&&group)) {
				AKA_mark("lis===228###sois===5886###eois===5903###lif===17###soif===352###eoif===369###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");ijob = job->next;

				while (AKA_mark("lis===229###sois===5913###eois===5941###lif===18###soif===379###eoif===407###ifc===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)") && ((AKA_mark("lis===229###sois===5913###eois===5917###lif===18###soif===379###eoif===383###isc===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)")&&ijob)	&&(AKA_mark("lis===229###sois===5921###eois===5941###lif===18###soif===387###eoif===407###isc===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)")&&ijob->group != group))) {
			AKA_mark("lis===230###sois===5946###eois===5964###lif===19###soif===412###eoif===430###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");ijob = ijob->next;
		}

				if (AKA_mark("lis===231###sois===5971###eois===5975###lif===20###soif===437###eoif===441###ifc===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)") && (AKA_mark("lis===231###sois===5971###eois===5975###lif===20###soif===437###eoif===441###isc===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)")&&ijob)) {
			AKA_mark("lis===232###sois===5980###eois===5998###lif===21###soif===446###eoif===464###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");ijob->blocked = 0;
		}
		else {AKA_mark("lis===-231-###sois===-5971-###eois===-59714-###lif===-20-###soif===-###eoif===-441-###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");}

	}
	else {AKA_mark("lis===-227-###sois===-5875-###eois===-58755-###lif===-16-###soif===-###eoif===-346-###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");}


	/* recycle the job */
		AKA_mark("lis===236###sois===6027###eois===6054###lif===25###soif===493###eoif===520###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");job->next = first_free_job;

		AKA_mark("lis===237###sois===6056###eois===6077###lif===26###soif===522###eoif===543###ins===true###function===./app-framework-binder/src/jobs.c/job_release(struct job*)");first_free_job = job;

}

/**
 * Monitored cancel callback for a job.
 * This function is called by the monitor
 * to cancel the job when the safe environment
 * is set.
 * @param signum 0 on normal flow or the number
 *               of the signal that interrupted the normal
 *               flow, isn't used
 * @param arg    the job to run
 */
/** Instrumented function job_cancel(int,void*) */
__attribute__((unused))
static void job_cancel(int signum, void *arg)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/job_cancel(int,void*)");AKA_fCall++;
		AKA_mark("lis===253###sois===6475###eois===6497###lif===3###soif===73###eoif===95###ins===true###function===./app-framework-binder/src/jobs.c/job_cancel(int,void*)");struct job *job = arg;

		AKA_mark("lis===254###sois===6499###eois===6532###lif===4###soif===97###eoif===130###ins===true###function===./app-framework-binder/src/jobs.c/job_cancel(int,void*)");job->callback(SIGABRT, job->arg);

}

/**
 * wakeup the event loop if needed by sending
 * an event.
 */
/** Instrumented function evloop_wakeup() */
static void evloop_wakeup()
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/evloop_wakeup()");AKA_fCall++;
		if (AKA_mark("lis===263###sois===6638###eois===6643###lif===2###soif===35###eoif===40###ifc===true###function===./app-framework-binder/src/jobs.c/evloop_wakeup()") && (AKA_mark("lis===263###sois===6638###eois===6643###lif===2###soif===35###eoif===40###isc===true###function===./app-framework-binder/src/jobs.c/evloop_wakeup()")&&evmgr)) {
		AKA_mark("lis===264###sois===6647###eois===6667###lif===3###soif===44###eoif===64###ins===true###function===./app-framework-binder/src/jobs.c/evloop_wakeup()");evmgr_wakeup(evmgr);
	}
	else {AKA_mark("lis===-263-###sois===-6638-###eois===-66385-###lif===-2-###soif===-###eoif===-40-###ins===true###function===./app-framework-binder/src/jobs.c/evloop_wakeup()");}

}

/**
 * Release the currently held event loop
 */
/** Instrumented function evloop_release() */
static void evloop_release()
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/evloop_release()");AKA_fCall++;
		AKA_mark("lis===272###sois===6752###eois===6792###lif===2###soif===32###eoif===72###ins===true###function===./app-framework-binder/src/jobs.c/evloop_release()");struct thread *nh, *ct = current_thread;


		if (AKA_mark("lis===274###sois===6799###eois===6841###lif===4###soif===79###eoif===121###ifc===true###function===./app-framework-binder/src/jobs.c/evloop_release()") && (((AKA_mark("lis===274###sois===6799###eois===6801###lif===4###soif===79###eoif===81###isc===true###function===./app-framework-binder/src/jobs.c/evloop_release()")&&ct)	&&(AKA_mark("lis===274###sois===6805###eois===6810###lif===4###soif===85###eoif===90###isc===true###function===./app-framework-binder/src/jobs.c/evloop_release()")&&evmgr))	&&(AKA_mark("lis===274###sois===6814###eois===6841###lif===4###soif===94###eoif===121###isc===true###function===./app-framework-binder/src/jobs.c/evloop_release()")&&evmgr_release_if(evmgr, ct)))) {
				AKA_mark("lis===275###sois===6847###eois===6864###lif===5###soif===127###eoif===144###ins===true###function===./app-framework-binder/src/jobs.c/evloop_release()");nh = ct->nholder;

				AKA_mark("lis===276###sois===6867###eois===6883###lif===6###soif===147###eoif===163###ins===true###function===./app-framework-binder/src/jobs.c/evloop_release()");ct->nholder = 0;

				if (AKA_mark("lis===277###sois===6890###eois===6892###lif===7###soif===170###eoif===172###ifc===true###function===./app-framework-binder/src/jobs.c/evloop_release()") && (AKA_mark("lis===277###sois===6890###eois===6892###lif===7###soif===170###eoif===172###isc===true###function===./app-framework-binder/src/jobs.c/evloop_release()")&&nh)) {
						AKA_mark("lis===278###sois===6899###eois===6925###lif===8###soif===179###eoif===205###ins===true###function===./app-framework-binder/src/jobs.c/evloop_release()");evmgr_try_hold(evmgr, nh);

						AKA_mark("lis===279###sois===6929###eois===6961###lif===9###soif===209###eoif===241###ins===true###function===./app-framework-binder/src/jobs.c/evloop_release()");pthread_cond_signal(nh->cwhold);

		}
		else {AKA_mark("lis===-277-###sois===-6890-###eois===-68902-###lif===-7-###soif===-###eoif===-172-###ins===true###function===./app-framework-binder/src/jobs.c/evloop_release()");}

	}
	else {AKA_mark("lis===-274-###sois===-6799-###eois===-679942-###lif===-4-###soif===-###eoif===-121-###ins===true###function===./app-framework-binder/src/jobs.c/evloop_release()");}

}

/**
 * get the eventloop for the current thread
 */
/** Instrumented function evloop_get() */
static int evloop_get()
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/evloop_get()");AKA_fCall++;
		AKA_mark("lis===289###sois===7051###eois===7105###lif===2###soif===27###eoif===81###ins===true###function===./app-framework-binder/src/jobs.c/evloop_get()");return evmgr && evmgr_try_hold(evmgr, current_thread);

}

/**
 * acquire the eventloop for the current thread
 */
/** Instrumented function evloop_acquire() */
static void evloop_acquire()
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/evloop_acquire()");AKA_fCall++;
		AKA_mark("lis===297###sois===7197###eois===7223###lif===2###soif===32###eoif===58###ins===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()");struct thread *pwait, *ct;

		AKA_mark("lis===298###sois===7225###eois===7245###lif===3###soif===60###eoif===80###ins===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()");pthread_cond_t cond;


	/* try to get the evloop */
		if (AKA_mark("lis===301###sois===7281###eois===7294###lif===6###soif===116###eoif===129###ifc===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()") && (AKA_mark("lis===301###sois===7281###eois===7294###lif===6###soif===116###eoif===129###isc===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()")&&!evloop_get())) {
		/* failed, init waiting state */
				AKA_mark("lis===303###sois===7335###eois===7355###lif===8###soif===170###eoif===190###ins===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()");ct = current_thread;

				AKA_mark("lis===304###sois===7358###eois===7377###lif===9###soif===193###eoif===212###ins===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()");ct->nholder = NULL;

				AKA_mark("lis===305###sois===7380###eois===7399###lif===10###soif===215###eoif===234###ins===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()");ct->cwhold = &cond;

				AKA_mark("lis===306###sois===7402###eois===7433###lif===11###soif===237###eoif===268###ins===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()");pthread_cond_init(&cond, NULL);


		/* queue current thread in holder list */
				AKA_mark("lis===309###sois===7481###eois===7509###lif===14###soif===316###eoif===344###ins===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()");pwait = evmgr_holder(evmgr);

				while (AKA_mark("lis===310###sois===7519###eois===7533###lif===15###soif===354###eoif===368###ifc===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()") && (AKA_mark("lis===310###sois===7519###eois===7533###lif===15###soif===354###eoif===368###isc===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()")&&pwait->nholder)) {
			AKA_mark("lis===311###sois===7538###eois===7561###lif===16###soif===373###eoif===396###ins===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()");pwait = pwait->nholder;
		}

				AKA_mark("lis===312###sois===7564###eois===7584###lif===17###soif===399###eoif===419###ins===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()");pwait->nholder = ct;


		/* wake up the evloop */
				AKA_mark("lis===315###sois===7615###eois===7631###lif===20###soif===450###eoif===466###ins===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()");evloop_wakeup();


		/* wait to acquire the evloop */
				AKA_mark("lis===318###sois===7670###eois===7703###lif===23###soif===505###eoif===538###ins===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()");pthread_cond_wait(&cond, &mutex);

				AKA_mark("lis===319###sois===7706###eois===7734###lif===24###soif===541###eoif===569###ins===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()");pthread_cond_destroy(&cond);

	}
	else {AKA_mark("lis===-301-###sois===-7281-###eois===-728113-###lif===-6-###soif===-###eoif===-129-###ins===true###function===./app-framework-binder/src/jobs.c/evloop_acquire()");}

}

/**
 * Enter the thread
 * @param me the description of the thread to enter
 */
/** Instrumented function thread_enter(volatile struct thread*) */
static void thread_enter(volatile struct thread *me)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/thread_enter(volatile struct thread*)");AKA_fCall++;
		AKA_mark("lis===329###sois===7877###eois===7894###lif===2###soif===56###eoif===73###ins===true###function===./app-framework-binder/src/jobs.c/thread_enter(volatile struct thread*)");evloop_release();

	/* initialize description of itself and link it in the list */
		AKA_mark("lis===331###sois===7960###eois===7985###lif===4###soif===139###eoif===164###ins===true###function===./app-framework-binder/src/jobs.c/thread_enter(volatile struct thread*)");me->tid = pthread_self();

		AKA_mark("lis===332###sois===7987###eois===8000###lif===5###soif===166###eoif===179###ins===true###function===./app-framework-binder/src/jobs.c/thread_enter(volatile struct thread*)");me->stop = 0;

		AKA_mark("lis===333###sois===8002###eois===8016###lif===6###soif===181###eoif===195###ins===true###function===./app-framework-binder/src/jobs.c/thread_enter(volatile struct thread*)");me->waits = 0;

		AKA_mark("lis===334###sois===8018###eois===8033###lif===7###soif===197###eoif===212###ins===true###function===./app-framework-binder/src/jobs.c/thread_enter(volatile struct thread*)");me->leaved = 0;

		AKA_mark("lis===335###sois===8035###eois===8051###lif===8###soif===214###eoif===230###ins===true###function===./app-framework-binder/src/jobs.c/thread_enter(volatile struct thread*)");me->nholder = 0;

		AKA_mark("lis===336###sois===8053###eois===8080###lif===9###soif===232###eoif===259###ins===true###function===./app-framework-binder/src/jobs.c/thread_enter(volatile struct thread*)");me->upper = current_thread;

		AKA_mark("lis===337###sois===8082###eois===8101###lif===10###soif===261###eoif===280###ins===true###function===./app-framework-binder/src/jobs.c/thread_enter(volatile struct thread*)");me->next = threads;

		AKA_mark("lis===338###sois===8103###eois===8132###lif===11###soif===282###eoif===311###ins===true###function===./app-framework-binder/src/jobs.c/thread_enter(volatile struct thread*)");threads = (struct thread*)me;

		AKA_mark("lis===339###sois===8134###eois===8170###lif===12###soif===313###eoif===349###ins===true###function===./app-framework-binder/src/jobs.c/thread_enter(volatile struct thread*)");current_thread = (struct thread*)me;

}

/**
 * leave the thread
 * @param me the description of the thread to leave
 */
/** Instrumented function thread_leave() */
static void thread_leave()
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/thread_leave()");AKA_fCall++;
		AKA_mark("lis===348###sois===8284###eois===8309###lif===2###soif===30###eoif===55###ins===true###function===./app-framework-binder/src/jobs.c/thread_leave()");struct thread **prv, *me;


	/* unlink the current thread and cleanup */
		AKA_mark("lis===351###sois===8357###eois===8377###lif===5###soif===103###eoif===123###ins===true###function===./app-framework-binder/src/jobs.c/thread_leave()");me = current_thread;

		AKA_mark("lis===352###sois===8379###eois===8394###lif===6###soif===125###eoif===140###ins===true###function===./app-framework-binder/src/jobs.c/thread_leave()");prv = &threads;

		while (AKA_mark("lis===353###sois===8403###eois===8413###lif===7###soif===149###eoif===159###ifc===true###function===./app-framework-binder/src/jobs.c/thread_leave()") && (AKA_mark("lis===353###sois===8403###eois===8413###lif===7###soif===149###eoif===159###isc===true###function===./app-framework-binder/src/jobs.c/thread_leave()")&&*prv != me)) {
		AKA_mark("lis===354###sois===8417###eois===8437###lif===8###soif===163###eoif===183###ins===true###function===./app-framework-binder/src/jobs.c/thread_leave()");prv = &(*prv)->next;
	}

		AKA_mark("lis===355###sois===8439###eois===8455###lif===9###soif===185###eoif===201###ins===true###function===./app-framework-binder/src/jobs.c/thread_leave()");*prv = me->next;


		AKA_mark("lis===357###sois===8458###eois===8485###lif===11###soif===204###eoif===231###ins===true###function===./app-framework-binder/src/jobs.c/thread_leave()");current_thread = me->upper;

}

/**
 * Main processing loop of internal threads with processing jobs.
 * The loop must be called with the mutex locked
 * and it returns with the mutex locked.
 * @param me the description of the thread to use
 * TODO: how are timeout handled when reentering?
 */
/** Instrumented function thread_run_internal(volatile struct thread*) */
static void thread_run_internal(volatile struct thread *me)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");AKA_fCall++;
		AKA_mark("lis===369###sois===8816###eois===8832###lif===2###soif===63###eoif===79###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");struct job *job;


	/* enter thread */
		AKA_mark("lis===372###sois===8855###eois===8872###lif===5###soif===102###eoif===119###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");thread_enter(me);


	/* loop until stopped */
		while (AKA_mark("lis===375###sois===8908###eois===8917###lif===8###soif===155###eoif===164###ifc===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)") && (AKA_mark("lis===375###sois===8908###eois===8917###lif===8###soif===155###eoif===164###isc===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)")&&!me->stop)) {
		/* release the current event loop */
				AKA_mark("lis===377###sois===8962###eois===8979###lif===10###soif===209###eoif===226###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");evloop_release();


		/* get a job */
				AKA_mark("lis===380###sois===9001###eois===9017###lif===13###soif===248###eoif===264###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");job = job_get();

				if (AKA_mark("lis===381###sois===9024###eois===9027###lif===14###soif===271###eoif===274###ifc===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)") && (AKA_mark("lis===381###sois===9024###eois===9027###lif===14###soif===271###eoif===274###isc===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)")&&job)) {
			/* prepare running the job */
						AKA_mark("lis===383###sois===9067###eois===9084###lif===16###soif===314###eoif===331###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");job->blocked = 1;
 /* mark job as blocked */
						AKA_mark("lis===384###sois===9114###eois===9128###lif===17###soif===361###eoif===375###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");me->job = job;
 /* record the job (only for terminate) */

			/* run the job */
						AKA_mark("lis===387###sois===9196###eois===9225###lif===20###soif===443###eoif===472###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");pthread_mutex_unlock(&mutex);

						AKA_mark("lis===388###sois===9229###eois===9280###lif===21###soif===476###eoif===527###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");sig_monitor(job->timeout, job->callback, job->arg);

						AKA_mark("lis===389###sois===9284###eois===9311###lif===22###soif===531###eoif===558###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");pthread_mutex_lock(&mutex);


			/* release the run job */
						AKA_mark("lis===392###sois===9345###eois===9362###lif===25###soif===592###eoif===609###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");job_release(job);

		/* no job, check event loop wait */
		}
		else {
			if (AKA_mark("lis===394###sois===9414###eois===9426###lif===27###soif===661###eoif===673###ifc===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)") && (AKA_mark("lis===394###sois===9414###eois===9426###lif===27###soif===661###eoif===673###isc===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)")&&evloop_get())) {
							if (AKA_mark("lis===395###sois===9437###eois===9458###lif===28###soif===684###eoif===705###ifc===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)") && (AKA_mark("lis===395###sois===9437###eois===9458###lif===28###soif===684###eoif===705###isc===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)")&&!evmgr_can_run(evmgr))) {
				/* busy ? */
									AKA_mark("lis===397###sois===9483###eois===9535###lif===30###soif===730###eoif===782###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");CRITICAL("Can't enter dispatch while in dispatch!");

									AKA_mark("lis===398###sois===9540###eois===9548###lif===31###soif===787###eoif===795###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");abort();

			}
				else {AKA_mark("lis===-395-###sois===-9437-###eois===-943721-###lif===-28-###soif===-###eoif===-705-###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");}

			/* run the events */
							AKA_mark("lis===401###sois===9581###eois===9606###lif===34###soif===828###eoif===853###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");evmgr_prepare_run(evmgr);

							AKA_mark("lis===402###sois===9610###eois===9639###lif===35###soif===857###eoif===886###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");pthread_mutex_unlock(&mutex);

							AKA_mark("lis===403###sois===9643###eois===9700###lif===36###soif===890###eoif===947###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");sig_monitor(0, (void(*)(int,void*))evmgr_job_run, evmgr);

							AKA_mark("lis===404###sois===9704###eois===9731###lif===37###soif===951###eoif===978###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");pthread_mutex_lock(&mutex);

		}
			else {
			/* no job and no event loop */
							AKA_mark("lis===407###sois===9780###eois===9800###lif===40###soif===1027###eoif===1047###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");busy_thread_count--;

							if (AKA_mark("lis===408###sois===9808###eois===9826###lif===41###soif===1055###eoif===1073###ifc===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)") && (AKA_mark("lis===408###sois===9808###eois===9826###lif===41###soif===1055###eoif===1073###isc===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)")&&!busy_thread_count)) {
					AKA_mark("lis===409###sois===9832###eois===9887###lif===42###soif===1079###eoif===1134###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");ERROR("Entering job deep sleep! Check your bindings.");
				}
				else {AKA_mark("lis===-408-###sois===-9808-###eois===-980818-###lif===-41-###soif===-###eoif===-1073-###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");}

							AKA_mark("lis===410###sois===9891###eois===9905###lif===43###soif===1138###eoif===1152###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");me->waits = 1;

							AKA_mark("lis===411###sois===9909###eois===9942###lif===44###soif===1156###eoif===1189###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");pthread_cond_wait(&cond, &mutex);

							AKA_mark("lis===412###sois===9946###eois===9960###lif===45###soif===1193###eoif===1207###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");me->waits = 0;

							AKA_mark("lis===413###sois===9964###eois===9984###lif===46###soif===1211###eoif===1231###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");busy_thread_count++;

		}
		}

	}

	/* cleanup */
		AKA_mark("lis===417###sois===10008###eois===10025###lif===50###soif===1255###eoif===1272###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");evloop_release();

		AKA_mark("lis===418###sois===10027###eois===10042###lif===51###soif===1274###eoif===1289###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_internal(volatile struct thread*)");thread_leave();

}

/**
 * Main processing loop of external threads.
 * The loop must be called with the mutex locked
 * and it returns with the mutex locked.
 * @param me the description of the thread to use
 */
/** Instrumented function thread_run_external(volatile struct thread*) */
static void thread_run_external(volatile struct thread *me)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/thread_run_external(volatile struct thread*)");AKA_fCall++;
	/* enter thread */
		AKA_mark("lis===430###sois===10322###eois===10339###lif===3###soif===83###eoif===100###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_external(volatile struct thread*)");thread_enter(me);


	/* loop until stopped */
		AKA_mark("lis===433###sois===10368###eois===10382###lif===6###soif===129###eoif===143###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_external(volatile struct thread*)");me->waits = 1;

		while (AKA_mark("lis===434###sois===10391###eois===10400###lif===7###soif===152###eoif===161###ifc===true###function===./app-framework-binder/src/jobs.c/thread_run_external(volatile struct thread*)") && (AKA_mark("lis===434###sois===10391###eois===10400###lif===7###soif===152###eoif===161###isc===true###function===./app-framework-binder/src/jobs.c/thread_run_external(volatile struct thread*)")&&!me->stop)) {
		AKA_mark("lis===435###sois===10404###eois===10437###lif===8###soif===165###eoif===198###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_external(volatile struct thread*)");pthread_cond_wait(&cond, &mutex);
	}

		AKA_mark("lis===436###sois===10439###eois===10453###lif===9###soif===200###eoif===214###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_external(volatile struct thread*)");me->waits = 0;

		AKA_mark("lis===437###sois===10455###eois===10470###lif===10###soif===216###eoif===231###ins===true###function===./app-framework-binder/src/jobs.c/thread_run_external(volatile struct thread*)");thread_leave();

}

/**
 * Root for created threads.
 */
/** Instrumented function thread_main() */
static void thread_main()
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/thread_main()");AKA_fCall++;
		AKA_mark("lis===445###sois===10540###eois===10557###lif===2###soif===29###eoif===46###ins===true###function===./app-framework-binder/src/jobs.c/thread_main()");struct thread me;


		AKA_mark("lis===447###sois===10560###eois===10580###lif===4###soif===49###eoif===69###ins===true###function===./app-framework-binder/src/jobs.c/thread_main()");busy_thread_count++;

		AKA_mark("lis===448###sois===10582###eois===10605###lif===5###soif===71###eoif===94###ins===true###function===./app-framework-binder/src/jobs.c/thread_main()");started_thread_count++;

		AKA_mark("lis===449###sois===10607###eois===10635###lif===6###soif===96###eoif===124###ins===true###function===./app-framework-binder/src/jobs.c/thread_main()");sig_monitor_init_timeouts();

		AKA_mark("lis===450###sois===10637###eois===10662###lif===7###soif===126###eoif===151###ins===true###function===./app-framework-binder/src/jobs.c/thread_main()");thread_run_internal(&me);

		AKA_mark("lis===451###sois===10664###eois===10693###lif===8###soif===153###eoif===182###ins===true###function===./app-framework-binder/src/jobs.c/thread_main()");sig_monitor_clean_timeouts();

		AKA_mark("lis===452###sois===10695###eois===10718###lif===9###soif===184###eoif===207###ins===true###function===./app-framework-binder/src/jobs.c/thread_main()");started_thread_count--;

		AKA_mark("lis===453###sois===10720###eois===10740###lif===10###soif===209###eoif===229###ins===true###function===./app-framework-binder/src/jobs.c/thread_main()");busy_thread_count--;

}

/**
 * Entry point for created threads.
 * @param data not used
 * @return NULL
 */
/** Instrumented function thread_starter(void*) */
static void *thread_starter(void *data)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/thread_starter(void*)");AKA_fCall++;
		AKA_mark("lis===463###sois===10871###eois===10898###lif===2###soif===43###eoif===70###ins===true###function===./app-framework-binder/src/jobs.c/thread_starter(void*)");pthread_mutex_lock(&mutex);

		AKA_mark("lis===464###sois===10900###eois===10914###lif===3###soif===72###eoif===86###ins===true###function===./app-framework-binder/src/jobs.c/thread_starter(void*)");thread_main();

		AKA_mark("lis===465###sois===10916###eois===10945###lif===4###soif===88###eoif===117###ins===true###function===./app-framework-binder/src/jobs.c/thread_starter(void*)");pthread_mutex_unlock(&mutex);

		AKA_mark("lis===466###sois===10947###eois===10959###lif===5###soif===119###eoif===131###ins===true###function===./app-framework-binder/src/jobs.c/thread_starter(void*)");return NULL;

}

/**
 * Starts a new thread
 * @return 0 in case of success or -1 in case of error
 */
/** Instrumented function start_one_thread() */
static int start_one_thread()
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/start_one_thread()");AKA_fCall++;
		AKA_mark("lis===475###sois===11082###eois===11096###lif===2###soif===33###eoif===47###ins===true###function===./app-framework-binder/src/jobs.c/start_one_thread()");pthread_t tid;

		AKA_mark("lis===476###sois===11098###eois===11105###lif===3###soif===49###eoif===56###ins===true###function===./app-framework-binder/src/jobs.c/start_one_thread()");int rc;


		AKA_mark("lis===478###sois===11108###eois===11162###lif===5###soif===59###eoif===113###ins===true###function===./app-framework-binder/src/jobs.c/start_one_thread()");rc = pthread_create(&tid, NULL, thread_starter, NULL);

		if (AKA_mark("lis===479###sois===11168###eois===11175###lif===6###soif===119###eoif===126###ifc===true###function===./app-framework-binder/src/jobs.c/start_one_thread()") && (AKA_mark("lis===479###sois===11168###eois===11175###lif===6###soif===119###eoif===126###isc===true###function===./app-framework-binder/src/jobs.c/start_one_thread()")&&rc != 0)) {
		/* errno = rc; */
				AKA_mark("lis===481###sois===11201###eois===11241###lif===8###soif===152###eoif===192###ins===true###function===./app-framework-binder/src/jobs.c/start_one_thread()");WARNING("not able to start thread: %m");

				AKA_mark("lis===482###sois===11244###eois===11252###lif===9###soif===195###eoif===203###ins===true###function===./app-framework-binder/src/jobs.c/start_one_thread()");rc = -1;

	}
	else {AKA_mark("lis===-479-###sois===-11168-###eois===-111687-###lif===-6-###soif===-###eoif===-126-###ins===true###function===./app-framework-binder/src/jobs.c/start_one_thread()");}

		AKA_mark("lis===484###sois===11257###eois===11267###lif===11###soif===208###eoif===218###ins===true###function===./app-framework-binder/src/jobs.c/start_one_thread()");return rc;

}

/**
 * Queues a new asynchronous job represented by 'callback' and 'arg'
 * for the 'group' and the 'timeout'.
 * Jobs are queued FIFO and are possibly executed in parallel
 * concurrently except for job of the same group that are
 * executed sequentially in FIFO order.
 * @param group    The group of the job or NULL when no group.
 * @param timeout  The maximum execution time in seconds of the job
 *                 or 0 for unlimited time.
 * @param callback The function to execute for achieving the job.
 *                 Its first parameter is either 0 on normal flow
 *                 or the signal number that broke the normal flow.
 *                 The remaining parameter is the parameter 'arg1'
 *                 given here.
 * @param arg      The second argument for 'callback'
 * @param start    The start mode for threads
 * @return 0 in case of success or -1 in case of error
 */
/** Instrumented function queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode) */
static int queue_job_internal(
		const void *group,
		int timeout,
		void (*callback)(int, void*),
		void *arg,
		enum start_mode start_mode)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");AKA_fCall++;
		AKA_mark("lis===512###sois===12319###eois===12335###lif===7###soif===145###eoif===161###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");struct job *job;

		AKA_mark("lis===513###sois===12337###eois===12350###lif===8###soif===163###eoif===176###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");int rc, busy;


	/* check availability */
		if (AKA_mark("lis===516###sois===12383###eois===12407###lif===11###soif===209###eoif===233###ifc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)") && (AKA_mark("lis===516###sois===12383###eois===12407###lif===11###soif===209###eoif===233###isc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)")&&remaining_job_count <= 0)) {
				AKA_mark("lis===517###sois===12413###eois===12468###lif===12###soif===239###eoif===294###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");ERROR("can't process job with threads: too many jobs");

				AKA_mark("lis===518###sois===12471###eois===12485###lif===13###soif===297###eoif===311###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");errno = EBUSY;

				AKA_mark("lis===519###sois===12488###eois===12499###lif===14###soif===314###eoif===325###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");goto error;

	}
	else {AKA_mark("lis===-516-###sois===-12383-###eois===-1238324-###lif===-11-###soif===-###eoif===-233-###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");}


	/* allocates the job */
		AKA_mark("lis===523###sois===12530###eois===12578###lif===18###soif===356###eoif===404###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");job = job_create(group, timeout, callback, arg);

		if (AKA_mark("lis===524###sois===12584###eois===12588###lif===19###soif===410###eoif===414###ifc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)") && (AKA_mark("lis===524###sois===12584###eois===12588###lif===19###soif===410###eoif===414###isc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)")&&!job)) {
		AKA_mark("lis===525###sois===12592###eois===12603###lif===20###soif===418###eoif===429###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");goto error;
	}
	else {AKA_mark("lis===-524-###sois===-12584-###eois===-125844-###lif===-19-###soif===-###eoif===-414-###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");}


	/* start a thread if needed */
		AKA_mark("lis===528###sois===12638###eois===12687###lif===23###soif===464###eoif===513###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");busy = busy_thread_count == started_thread_count;

		if (AKA_mark("lis===529###sois===12693###eois===12876###lif===24###soif===519###eoif===702###ifc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)") && ((((AKA_mark("lis===529###sois===12693###eois===12717###lif===24###soif===519###eoif===543###isc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)")&&start_mode != Start_Lazy)	&&(AKA_mark("lis===530###sois===12723###eois===12727###lif===25###soif===549###eoif===553###isc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)")&&busy))	&&((AKA_mark("lis===531###sois===12734###eois===12760###lif===26###soif===560###eoif===586###isc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)")&&start_mode == Start_Urgent)	||(AKA_mark("lis===531###sois===12764###eois===12826###lif===26###soif===590###eoif===652###isc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)")&&remaining_job_count + started_thread_count < allowed_job_count)))	&&(AKA_mark("lis===532###sois===12833###eois===12876###lif===27###soif===659###eoif===702###isc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)")&&started_thread_count < allowed_thread_count))) {
		/* all threads are busy and a new can be started */
				AKA_mark("lis===534###sois===12936###eois===12960###lif===29###soif===762###eoif===786###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");rc = start_one_thread();

				if (AKA_mark("lis===535###sois===12967###eois===13002###lif===30###soif===793###eoif===828###ifc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)") && ((AKA_mark("lis===535###sois===12967###eois===12973###lif===30###soif===793###eoif===799###isc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)")&&rc < 0)	&&(AKA_mark("lis===535###sois===12977###eois===13002###lif===30###soif===803###eoif===828###isc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)")&&started_thread_count == 0))) {
						AKA_mark("lis===536###sois===13009###eois===13049###lif===31###soif===835###eoif===875###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");ERROR("can't start initial thread: %m");

						AKA_mark("lis===537###sois===13053###eois===13065###lif===32###soif===879###eoif===891###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");goto error2;

		}
		else {AKA_mark("lis===-535-###sois===-12967-###eois===-1296735-###lif===-30-###soif===-###eoif===-828-###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");}

				AKA_mark("lis===539###sois===13072###eois===13081###lif===34###soif===898###eoif===907###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");busy = 0;

	}
	else {AKA_mark("lis===-529-###sois===-12693-###eois===-12693183-###lif===-24-###soif===-###eoif===-702-###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");}


	/* queues the job */
		AKA_mark("lis===543###sois===13109###eois===13122###lif===38###soif===935###eoif===948###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");job_add(job);


	/* wakeup an evloop if needed */
		if (AKA_mark("lis===546###sois===13163###eois===13167###lif===41###soif===989###eoif===993###ifc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)") && (AKA_mark("lis===546###sois===13163###eois===13167###lif===41###soif===989###eoif===993###isc===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)")&&busy)) {
		AKA_mark("lis===547###sois===13171###eois===13187###lif===42###soif===997###eoif===1013###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");evloop_wakeup();
	}
	else {AKA_mark("lis===-546-###sois===-13163-###eois===-131634-###lif===-41-###soif===-###eoif===-993-###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");}


		AKA_mark("lis===549###sois===13190###eois===13217###lif===44###soif===1016###eoif===1043###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");pthread_cond_signal(&cond);

		AKA_mark("lis===550###sois===13219###eois===13228###lif===45###soif===1045###eoif===1054###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");return 0;


	error2:
	AKA_mark("lis===553###sois===13239###eois===13266###lif===48###soif===1065###eoif===1092###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");job->next = first_free_job;

		AKA_mark("lis===554###sois===13268###eois===13289###lif===49###soif===1094###eoif===1115###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");first_free_job = job;

	error:
	AKA_mark("lis===556###sois===13298###eois===13308###lif===51###soif===1124###eoif===1134###ins===true###function===./app-framework-binder/src/jobs.c/queue_job_internal(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");return -1;

}

/**
 * Queues a new asynchronous job represented by 'callback' and 'arg'
 * for the 'group' and the 'timeout'.
 * Jobs are queued FIFO and are possibly executed in parallel
 * concurrently except for job of the same group that are
 * executed sequentially in FIFO order.
 * @param group    The group of the job or NULL when no group.
 * @param timeout  The maximum execution time in seconds of the job
 *                 or 0 for unlimited time.
 * @param callback The function to execute for achieving the job.
 *                 Its first parameter is either 0 on normal flow
 *                 or the signal number that broke the normal flow.
 *                 The remaining parameter is the parameter 'arg1'
 *                 given here.
 * @param arg      The second argument for 'callback'
 * @param start    The start mode for threads
 * @return 0 in case of success or -1 in case of error
 */
/** Instrumented function queue_job(const void*,int,void(*callback)(int, void*),void*,enum start_mode) */
static int queue_job(
		const void *group,
		int timeout,
		void (*callback)(int, void*),
		void *arg,
		enum start_mode start_mode)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/queue_job(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");AKA_fCall++;
		AKA_mark("lis===584###sois===14351###eois===14358###lif===7###soif===136###eoif===143###ins===true###function===./app-framework-binder/src/jobs.c/queue_job(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");int rc;


		AKA_mark("lis===586###sois===14361###eois===14388###lif===9###soif===146###eoif===173###ins===true###function===./app-framework-binder/src/jobs.c/queue_job(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");pthread_mutex_lock(&mutex);

		AKA_mark("lis===587###sois===14390###eois===14457###lif===10###soif===175###eoif===242###ins===true###function===./app-framework-binder/src/jobs.c/queue_job(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");rc = queue_job_internal(group, timeout, callback, arg, start_mode);

		AKA_mark("lis===588###sois===14459###eois===14488###lif===11###soif===244###eoif===273###ins===true###function===./app-framework-binder/src/jobs.c/queue_job(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");pthread_mutex_unlock(&mutex);

		AKA_mark("lis===589###sois===14490###eois===14500###lif===12###soif===275###eoif===285###ins===true###function===./app-framework-binder/src/jobs.c/queue_job(const void*,int,void(*callback)(int, void*),void*,enum start_mode)");return rc;


}

/**
 * Queues a new asynchronous job represented by 'callback' and 'arg'
 * for the 'group' and the 'timeout'.
 * Jobs are queued FIFO and are possibly executed in parallel
 * concurrently except for job of the same group that are
 * executed sequentially in FIFO order.
 * @param group    The group of the job or NULL when no group.
 * @param timeout  The maximum execution time in seconds of the job
 *                 or 0 for unlimited time.
 * @param callback The function to execute for achieving the job.
 *                 Its first parameter is either 0 on normal flow
 *                 or the signal number that broke the normal flow.
 *                 The remaining parameter is the parameter 'arg1'
 *                 given here.
 * @param arg      The second argument for 'callback'
 * @return 0 in case of success or -1 in case of error
 */
/** Instrumented function jobs_queue(const void*,int,void(*callback)(int, void*),void*) */
int jobs_queue(
		const void *group,
		int timeout,
		void (*callback)(int, void*),
		void *arg)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/jobs_queue(const void*,int,void(*callback)(int, void*),void*)");AKA_fCall++;
		AKA_mark("lis===616###sois===15462###eois===15525###lif===6###soif===100###eoif===163###ins===true###function===./app-framework-binder/src/jobs.c/jobs_queue(const void*,int,void(*callback)(int, void*),void*)");return queue_job(group, timeout, callback, arg, Start_Default);

}

/**
 * Queues lazyly a new asynchronous job represented by 'callback' and 'arg'
 * for the 'group' and the 'timeout'.
 * Jobs are queued FIFO and are possibly executed in parallel
 * concurrently except for job of the same group that are
 * executed sequentially in FIFO order.
 * @param group    The group of the job or NULL when no group.
 * @param timeout  The maximum execution time in seconds of the job
 *                 or 0 for unlimited time.
 * @param callback The function to execute for achieving the job.
 *                 Its first parameter is either 0 on normal flow
 *                 or the signal number that broke the normal flow.
 *                 The remaining parameter is the parameter 'arg1'
 *                 given here.
 * @param arg      The second argument for 'callback'
 * @return 0 in case of success or -1 in case of error
 */
/** Instrumented function jobs_queue_lazy(const void*,int,void(*callback)(int, void*),void*) */
int jobs_queue_lazy(
		const void *group,
		int timeout,
		void (*callback)(int, void*),
		void *arg)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/jobs_queue_lazy(const void*,int,void(*callback)(int, void*),void*)");AKA_fCall++;
		AKA_mark("lis===642###sois===16498###eois===16558###lif===6###soif===105###eoif===165###ins===true###function===./app-framework-binder/src/jobs.c/jobs_queue_lazy(const void*,int,void(*callback)(int, void*),void*)");return queue_job(group, timeout, callback, arg, Start_Lazy);

}

/**
 * Queues urgently a new asynchronous job represented by 'callback' and 'arg'
 * for the 'group' and the 'timeout'.
 * Jobs are queued FIFO and are possibly executed in parallel
 * concurrently except for job of the same group that are
 * executed sequentially in FIFO order.
 * @param group    The group of the job or NULL when no group.
 * @param timeout  The maximum execution time in seconds of the job
 *                 or 0 for unlimited time.
 * @param callback The function to execute for achieving the job.
 *                 Its first parameter is either 0 on normal flow
 *                 or the signal number that broke the normal flow.
 *                 The remaining parameter is the parameter 'arg1'
 *                 given here.
 * @param arg      The second argument for 'callback'
 * @return 0 in case of success or -1 in case of error
 */
/** Instrumented function jobs_queue_urgent(const void*,int,void(*callback)(int, void*),void*) */
int jobs_queue_urgent(
		const void *group,
		int timeout,
		void (*callback)(int, void*),
		void *arg)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/jobs_queue_urgent(const void*,int,void(*callback)(int, void*),void*)");AKA_fCall++;
		AKA_mark("lis===668###sois===17535###eois===17597###lif===6###soif===107###eoif===169###ins===true###function===./app-framework-binder/src/jobs.c/jobs_queue_urgent(const void*,int,void(*callback)(int, void*),void*)");return queue_job(group, timeout, callback, arg, Start_Urgent);

}

/**
 * Internal helper function for 'jobs_enter'.
 * @see jobs_enter, jobs_leave
 */
/** Instrumented function enter_cb(int,void*) */
static void enter_cb(int signum, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/enter_cb(int,void*)");AKA_fCall++;
		AKA_mark("lis===677###sois===17737###eois===17765###lif===2###soif===51###eoif===79###ins===true###function===./app-framework-binder/src/jobs.c/enter_cb(int,void*)");struct sync *sync = closure;

		AKA_mark("lis===678###sois===17767###eois===17820###lif===3###soif===81###eoif===134###ins===true###function===./app-framework-binder/src/jobs.c/enter_cb(int,void*)");sync->enter(signum, sync->arg, (void*)&sync->thread);

}

/**
 * Internal helper function for 'jobs_call'.
 * @see jobs_call
 */
/** Instrumented function call_cb(int,void*) */
static void call_cb(int signum, void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/call_cb(int,void*)");AKA_fCall++;
		AKA_mark("lis===687###sois===17945###eois===17973###lif===2###soif===50###eoif===78###ins===true###function===./app-framework-binder/src/jobs.c/call_cb(int,void*)");struct sync *sync = closure;

		AKA_mark("lis===688###sois===17975###eois===18009###lif===3###soif===80###eoif===114###ins===true###function===./app-framework-binder/src/jobs.c/call_cb(int,void*)");sync->callback(signum, sync->arg);

		AKA_mark("lis===689###sois===18011###eois===18044###lif===4###soif===116###eoif===149###ins===true###function===./app-framework-binder/src/jobs.c/call_cb(int,void*)");jobs_leave((void*)&sync->thread);

}

/**
 * Internal helper for synchronous jobs. It enters
 * a new thread loop for evaluating the given job
 * as recorded by the couple 'sync_cb' and 'sync'.
 * @see jobs_call, jobs_enter, jobs_leave
 */
/** Instrumented function do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*) */
static int do_sync(
		const void *group,
		int timeout,
		void (*sync_cb)(int signum, void *closure),
		struct sync *sync
)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)");AKA_fCall++;
		AKA_mark("lis===705###sois===18377###eois===18384###lif===7###soif===127###eoif===134###ins===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)");int rc;


		AKA_mark("lis===707###sois===18387###eois===18414###lif===9###soif===137###eoif===164###ins===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)");pthread_mutex_lock(&mutex);


		AKA_mark("lis===709###sois===18417###eois===18487###lif===11###soif===167###eoif===237###ins===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)");rc = queue_job_internal(group, timeout, sync_cb, sync, Start_Default);

		if (AKA_mark("lis===710###sois===18493###eois===18500###lif===12###soif===243###eoif===250###ifc===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)") && (AKA_mark("lis===710###sois===18493###eois===18500###lif===12###soif===243###eoif===250###isc===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)")&&rc == 0)) {
		/* run until stopped */
				if (AKA_mark("lis===712###sois===18536###eois===18550###lif===14###soif===286###eoif===300###ifc===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)") && (AKA_mark("lis===712###sois===18536###eois===18550###lif===14###soif===286###eoif===300###isc===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)")&&current_thread)) {
			AKA_mark("lis===713###sois===18555###eois===18590###lif===15###soif===305###eoif===340###ins===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)");thread_run_internal(&sync->thread);
		}
		else {
			AKA_mark("lis===715###sois===18601###eois===18636###lif===17###soif===351###eoif===386###ins===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)");thread_run_external(&sync->thread);
		}

				if (AKA_mark("lis===716###sois===18643###eois===18663###lif===18###soif===393###eoif===413###ifc===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)") && (AKA_mark("lis===716###sois===18643###eois===18663###lif===18###soif===393###eoif===413###isc===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)")&&!sync->thread.leaved)) {
						AKA_mark("lis===717###sois===18670###eois===18684###lif===19###soif===420###eoif===434###ins===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)");errno = EINTR;

						AKA_mark("lis===718###sois===18688###eois===18696###lif===20###soif===438###eoif===446###ins===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)");rc = -1;

		}
		else {AKA_mark("lis===-716-###sois===-18643-###eois===-1864320-###lif===-18-###soif===-###eoif===-413-###ins===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)");}

	}
	else {AKA_mark("lis===-710-###sois===-18493-###eois===-184937-###lif===-12-###soif===-###eoif===-250-###ins===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)");}

		AKA_mark("lis===721###sois===18705###eois===18734###lif===23###soif===455###eoif===484###ins===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)");pthread_mutex_unlock(&mutex);

		AKA_mark("lis===722###sois===18736###eois===18746###lif===24###soif===486###eoif===496###ins===true###function===./app-framework-binder/src/jobs.c/do_sync(const void*,int,void(*sync_cb)(int signum, void*closure),struct sync*)");return rc;

}

/**
 * Enter a synchronisation point: activates the job given by 'callback'
 * and 'closure' using 'group' and 'timeout' to control sequencing and
 * execution time.
 * @param group the group for sequencing jobs
 * @param timeout the time in seconds allocated to the job
 * @param callback the callback that will handle the job.
 *                 it receives 3 parameters: 'signum' that will be 0
 *                 on normal flow or the catched signal number in case
 *                 of interrupted flow, the context 'closure' as given and
 *                 a 'jobloop' reference that must be used when the job is
 *                 terminated to unlock the current execution flow.
 * @param closure the argument to the callback
 * @return 0 on success or -1 in case of error
 */
/** Instrumented function jobs_enter(const void*,int,void(*callback)(int signum, void*closure, struct jobloop*jobloop),void*) */
int jobs_enter(
		const void *group,
		int timeout,
		void (*callback)(int signum, void *closure, struct jobloop *jobloop),
		void *closure
)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/jobs_enter(const void*,int,void(*callback)(int signum, void*closure, struct jobloop*jobloop),void*)");AKA_fCall++;
		AKA_mark("lis===747###sois===19680###eois===19697###lif===7###soif===145###eoif===162###ins===true###function===./app-framework-binder/src/jobs.c/jobs_enter(const void*,int,void(*callback)(int signum, void*closure, struct jobloop*jobloop),void*)");struct sync sync;


		AKA_mark("lis===749###sois===19700###eois===19722###lif===9###soif===165###eoif===187###ins===true###function===./app-framework-binder/src/jobs.c/jobs_enter(const void*,int,void(*callback)(int signum, void*closure, struct jobloop*jobloop),void*)");sync.enter = callback;

		AKA_mark("lis===750###sois===19724###eois===19743###lif===10###soif===189###eoif===208###ins===true###function===./app-framework-binder/src/jobs.c/jobs_enter(const void*,int,void(*callback)(int signum, void*closure, struct jobloop*jobloop),void*)");sync.arg = closure;

		AKA_mark("lis===751###sois===19745###eois===19793###lif===11###soif===210###eoif===258###ins===true###function===./app-framework-binder/src/jobs.c/jobs_enter(const void*,int,void(*callback)(int signum, void*closure, struct jobloop*jobloop),void*)");return do_sync(group, timeout, enter_cb, &sync);

}

/**
 * Unlocks the execution flow designed by 'jobloop'.
 * @param jobloop indication of the flow to unlock
 * @return 0 in case of success of -1 on error
 */
/** Instrumented function jobs_leave(struct jobloop*) */
int jobs_leave(struct jobloop *jobloop)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)");AKA_fCall++;
		AKA_mark("lis===761###sois===19999###eois===20016###lif===2###soif===43###eoif===60###ins===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)");struct thread *t;


		AKA_mark("lis===763###sois===20019###eois===20046###lif===4###soif===63###eoif===90###ins===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)");pthread_mutex_lock(&mutex);

		AKA_mark("lis===764###sois===20048###eois===20060###lif===5###soif===92###eoif===104###ins===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)");t = threads;

		while (AKA_mark("lis===765###sois===20069###eois===20102###lif===6###soif===113###eoif===146###ifc===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)") && ((AKA_mark("lis===765###sois===20069###eois===20070###lif===6###soif===113###eoif===114###isc===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)")&&t)	&&(AKA_mark("lis===765###sois===20074###eois===20102###lif===6###soif===118###eoif===146###isc===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)")&&t != (struct thread*)jobloop))) {
		AKA_mark("lis===766###sois===20106###eois===20118###lif===7###soif===150###eoif===162###ins===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)");t = t->next;
	}

		if (AKA_mark("lis===767###sois===20124###eois===20126###lif===8###soif===168###eoif===170###ifc===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)") && (AKA_mark("lis===767###sois===20124###eois===20126###lif===8###soif===168###eoif===170###isc===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)")&&!t)) {
				AKA_mark("lis===768###sois===20132###eois===20147###lif===9###soif===176###eoif===191###ins===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)");errno = EINVAL;

	}
	else {
				AKA_mark("lis===770###sois===20160###eois===20174###lif===11###soif===204###eoif===218###ins===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)");t->leaved = 1;

				AKA_mark("lis===771###sois===20177###eois===20189###lif===12###soif===221###eoif===233###ins===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)");t->stop = 1;

				if (AKA_mark("lis===772###sois===20196###eois===20204###lif===13###soif===240###eoif===248###ifc===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)") && (AKA_mark("lis===772###sois===20196###eois===20204###lif===13###soif===240###eoif===248###isc===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)")&&t->waits)) {
			AKA_mark("lis===773###sois===20209###eois===20239###lif===14###soif===253###eoif===283###ins===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)");pthread_cond_broadcast(&cond);
		}
		else {
			AKA_mark("lis===775###sois===20250###eois===20266###lif===16###soif===294###eoif===310###ins===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)");evloop_wakeup();
		}

	}

		AKA_mark("lis===777###sois===20271###eois===20300###lif===18###soif===315###eoif===344###ins===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)");pthread_mutex_unlock(&mutex);

		AKA_mark("lis===778###sois===20302###eois===20313###lif===19###soif===346###eoif===357###ins===true###function===./app-framework-binder/src/jobs.c/jobs_leave(struct jobloop*)");return -!t;

}

/**
 * Calls synchronously the job represented by 'callback' and 'arg1'
 * for the 'group' and the 'timeout' and waits for its completion.
 * @param group    The group of the job or NULL when no group.
 * @param timeout  The maximum execution time in seconds of the job
 *                 or 0 for unlimited time.
 * @param callback The function to execute for achieving the job.
 *                 Its first parameter is either 0 on normal flow
 *                 or the signal number that broke the normal flow.
 *                 The remaining parameter is the parameter 'arg1'
 *                 given here.
 * @param arg      The second argument for 'callback'
 * @return 0 in case of success or -1 in case of error
 */
/** Instrumented function jobs_call(const void*,int,void(*callback)(int, void*),void*) */
int jobs_call(
		const void *group,
		int timeout,
		void (*callback)(int, void*),
		void *arg)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/jobs_call(const void*,int,void(*callback)(int, void*),void*)");AKA_fCall++;
		AKA_mark("lis===801###sois===21141###eois===21158###lif===6###soif===99###eoif===116###ins===true###function===./app-framework-binder/src/jobs.c/jobs_call(const void*,int,void(*callback)(int, void*),void*)");struct sync sync;


		AKA_mark("lis===803###sois===21161###eois===21186###lif===8###soif===119###eoif===144###ins===true###function===./app-framework-binder/src/jobs.c/jobs_call(const void*,int,void(*callback)(int, void*),void*)");sync.callback = callback;

		AKA_mark("lis===804###sois===21188###eois===21203###lif===9###soif===146###eoif===161###ins===true###function===./app-framework-binder/src/jobs.c/jobs_call(const void*,int,void(*callback)(int, void*),void*)");sync.arg = arg;


		AKA_mark("lis===806###sois===21206###eois===21253###lif===11###soif===164###eoif===211###ins===true###function===./app-framework-binder/src/jobs.c/jobs_call(const void*,int,void(*callback)(int, void*),void*)");return do_sync(group, timeout, call_cb, &sync);

}

/**
 * Ensure that the current running thread can control the event loop.
 */
/** Instrumented function jobs_acquire_event_manager() */
void jobs_acquire_event_manager()
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");AKA_fCall++;
		AKA_mark("lis===814###sois===21372###eois===21389###lif===2###soif===37###eoif===54###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");struct thread lt;


	/* ensure an existing thread environment */
		if (AKA_mark("lis===817###sois===21441###eois===21456###lif===5###soif===106###eoif===121###ifc===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()") && (AKA_mark("lis===817###sois===21441###eois===21456###lif===5###soif===106###eoif===121###isc===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()")&&!current_thread)) {
				AKA_mark("lis===818###sois===21462###eois===21488###lif===6###soif===127###eoif===153###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");memset(&lt, 0, sizeof lt);

				AKA_mark("lis===819###sois===21491###eois===21512###lif===7###soif===156###eoif===177###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");current_thread = &lt;

	}
	else {AKA_mark("lis===-817-###sois===-21441-###eois===-2144115-###lif===-5-###soif===-###eoif===-121-###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");}


	/* lock */
		AKA_mark("lis===823###sois===21530###eois===21557###lif===11###soif===195###eoif===222###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");pthread_mutex_lock(&mutex);


	/* creates the evloop on need */
		if (AKA_mark("lis===826###sois===21598###eois===21604###lif===14###soif===263###eoif===269###ifc===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()") && (AKA_mark("lis===826###sois===21598###eois===21604###lif===14###soif===263###eoif===269###isc===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()")&&!evmgr)) {
		AKA_mark("lis===827###sois===21608###eois===21629###lif===15###soif===273###eoif===294###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");evmgr_create(&evmgr);
	}
	else {AKA_mark("lis===-826-###sois===-21598-###eois===-215986-###lif===-14-###soif===-###eoif===-269-###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");}


	/* acquire the event loop under lock */
		if (AKA_mark("lis===830###sois===21677###eois===21682###lif===18###soif===342###eoif===347###ifc===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()") && (AKA_mark("lis===830###sois===21677###eois===21682###lif===18###soif===342###eoif===347###isc===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()")&&evmgr)) {
		AKA_mark("lis===831###sois===21686###eois===21703###lif===19###soif===351###eoif===368###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");evloop_acquire();
	}
	else {AKA_mark("lis===-830-###sois===-21677-###eois===-216775-###lif===-18-###soif===-###eoif===-347-###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");}


	/* unlock */
		AKA_mark("lis===834###sois===21720###eois===21749###lif===22###soif===385###eoif===414###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");pthread_mutex_unlock(&mutex);


	/* release the faked thread environment if needed */
		if (AKA_mark("lis===837###sois===21810###eois===21831###lif===25###soif===475###eoif===496###ifc===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()") && (AKA_mark("lis===837###sois===21810###eois===21831###lif===25###soif===475###eoif===496###isc===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()")&&current_thread == &lt)) {
		/*
		 * Releasing it is needed because there is no way to guess
		 * when it has to be released really. But here is where it is
		 * hazardous: if the caller modifies the eventloop when it
		 * is waiting, there is no way to make the change effective.
		 * A workaround to achieve that goal is for the caller to
		 * require the event loop a second time after having modified it.
		 */
				AKA_mark("lis===846###sois===22225###eois===22312###lif===34###soif===890###eoif===977###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");NOTICE("Requiring event manager/loop from outside of binder's callback is hazardous!");

				AKA_mark("lis===847###sois===22319###eois===22348###lif===35###soif===984###eoif===1013###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");if (verbose_wants(Log_Level_Info)){
			AKA_mark("lis===848###sois===22353###eois===22377###lif===36###soif===1018###eoif===1042###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");sig_monitor_dumpstack();
		}
		else {AKA_mark("");}

				AKA_mark("lis===849###sois===22380###eois===22397###lif===37###soif===1045###eoif===1062###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");evloop_release();

				AKA_mark("lis===850###sois===22400###eois===22422###lif===38###soif===1065###eoif===1087###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");current_thread = NULL;

	}
	else {AKA_mark("lis===-837-###sois===-21810-###eois===-2181021-###lif===-25-###soif===-###eoif===-496-###ins===true###function===./app-framework-binder/src/jobs.c/jobs_acquire_event_manager()");}

}

/**
 * Enter the jobs processing loop.
 * @param allowed_count Maximum count of thread for jobs including this one
 * @param start_count   Count of thread to start now, must be lower.
 * @param waiter_count  Maximum count of jobs that can be waiting.
 * @param start         The start routine to activate (can't be NULL)
 * @return 0 in case of success or -1 in case of error.
 */
/** Instrumented function jobs_start(int,int,int,void(*start)(int signum, void*arg),void*) */
int jobs_start(
	int allowed_count,
	int start_count,
	int waiter_count,
	void (*start)(int signum, void* arg),
	void *arg)
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");AKA_fCall++;
		AKA_mark("lis===869###sois===22937###eois===22954###lif===7###soif===127###eoif===144###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");int rc, launched;

		AKA_mark("lis===870###sois===22956###eois===22972###lif===8###soif===146###eoif===162###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");struct job *job;


		AKA_mark("lis===872###sois===22975###eois===23002###lif===10###soif===165###eoif===192###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");assert(allowed_count >= 1);

		AKA_mark("lis===873###sois===23004###eois===23029###lif===11###soif===194###eoif===219###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");assert(start_count >= 0);

		AKA_mark("lis===874###sois===23031###eois===23056###lif===12###soif===221###eoif===246###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");assert(waiter_count > 0);

		AKA_mark("lis===875###sois===23058###eois===23095###lif===13###soif===248###eoif===285###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");assert(start_count <= allowed_count);


		AKA_mark("lis===877###sois===23098###eois===23106###lif===15###soif===288###eoif===296###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");rc = -1;

		AKA_mark("lis===878###sois===23108###eois===23135###lif===16###soif===298###eoif===325###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");pthread_mutex_lock(&mutex);


	/* check whether already running */
		if (AKA_mark("lis===881###sois===23179###eois===23217###lif===19###soif===369###eoif===407###ifc===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)") && ((AKA_mark("lis===881###sois===23179###eois===23193###lif===19###soif===369###eoif===383###isc===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)")&&current_thread)	||(AKA_mark("lis===881###sois===23197###eois===23217###lif===19###soif===387###eoif===407###isc===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)")&&allowed_thread_count))) {
				AKA_mark("lis===882###sois===23223###eois===23255###lif===20###soif===413###eoif===445###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");ERROR("thread already started");

				AKA_mark("lis===883###sois===23258###eois===23273###lif===21###soif===448###eoif===463###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");errno = EINVAL;

				AKA_mark("lis===884###sois===23276###eois===23287###lif===22###soif===466###eoif===477###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");goto error;

	}
	else {AKA_mark("lis===-881-###sois===-23179-###eois===-2317938-###lif===-19-###soif===-###eoif===-407-###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");}


	/* records the allowed count */
		AKA_mark("lis===888###sois===23326###eois===23363###lif===26###soif===516###eoif===553###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");allowed_thread_count = allowed_count;

		AKA_mark("lis===889###sois===23365###eois===23390###lif===27###soif===555###eoif===580###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");started_thread_count = 0;

		AKA_mark("lis===890###sois===23392###eois===23414###lif===28###soif===582###eoif===604###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");busy_thread_count = 0;

		AKA_mark("lis===891###sois===23416###eois===23451###lif===29###soif===606###eoif===641###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");remaining_job_count = waiter_count;

		AKA_mark("lis===892###sois===23453###eois===23486###lif===30###soif===643###eoif===676###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");allowed_job_count = waiter_count;


	/* start at least one thread: the current one */
		AKA_mark("lis===895###sois===23539###eois===23552###lif===33###soif===729###eoif===742###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");launched = 1;

		while (AKA_mark("lis===896###sois===23561###eois===23583###lif===34###soif===751###eoif===773###ifc===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)") && (AKA_mark("lis===896###sois===23561###eois===23583###lif===34###soif===751###eoif===773###isc===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)")&&launched < start_count)) {
				if (AKA_mark("lis===897###sois===23593###eois===23616###lif===35###soif===783###eoif===806###ifc===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)") && (AKA_mark("lis===897###sois===23593###eois===23616###lif===35###soif===783###eoif===806###isc===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)")&&start_one_thread() != 0)) {
						AKA_mark("lis===898###sois===23623###eois===23663###lif===36###soif===813###eoif===853###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");ERROR("Not all threads can be started");

						AKA_mark("lis===899###sois===23667###eois===23678###lif===37###soif===857###eoif===868###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");goto error;

		}
		else {AKA_mark("lis===-897-###sois===-23593-###eois===-2359323-###lif===-35-###soif===-###eoif===-806-###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");}

				AKA_mark("lis===901###sois===23685###eois===23696###lif===39###soif===875###eoif===886###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");launched++;

	}


	/* queue the start job */
		AKA_mark("lis===905###sois===23729###eois===23767###lif===43###soif===919###eoif===957###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");job = job_create(NULL, 0, start, arg);

		if (AKA_mark("lis===906###sois===23773###eois===23777###lif===44###soif===963###eoif===967###ifc===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)") && (AKA_mark("lis===906###sois===23773###eois===23777###lif===44###soif===963###eoif===967###isc===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)")&&!job)) {
		AKA_mark("lis===907###sois===23781###eois===23792###lif===45###soif===971###eoif===982###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");goto error;
	}
	else {AKA_mark("lis===-906-###sois===-23773-###eois===-237734-###lif===-44-###soif===-###eoif===-967-###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");}

		AKA_mark("lis===908###sois===23794###eois===23807###lif===46###soif===984###eoif===997###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");job_add(job);


	/* run until end */
		AKA_mark("lis===911###sois===23831###eois===23845###lif===49###soif===1021###eoif===1035###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");thread_main();

		AKA_mark("lis===912###sois===23847###eois===23854###lif===50###soif===1037###eoif===1044###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");rc = 0;

	error:
	AKA_mark("lis===914###sois===23863###eois===23892###lif===52###soif===1053###eoif===1082###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");pthread_mutex_unlock(&mutex);

		if (AKA_mark("lis===915###sois===23898###eois===23910###lif===53###soif===1088###eoif===1100###ifc===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)") && (AKA_mark("lis===915###sois===23898###eois===23910###lif===53###soif===1088###eoif===1100###isc===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)")&&exit_handler)) {
		AKA_mark("lis===916###sois===23914###eois===23929###lif===54###soif===1104###eoif===1119###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");exit_handler();
	}
	else {AKA_mark("lis===-915-###sois===-23898-###eois===-2389812-###lif===-53-###soif===-###eoif===-1100-###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");}

		AKA_mark("lis===917###sois===23931###eois===23941###lif===55###soif===1121###eoif===1131###ins===true###function===./app-framework-binder/src/jobs.c/jobs_start(int,int,int,void(*start)(int signum, void*arg),void*)");return rc;

}

/**
 * Exit jobs threads and call handler if not NULL.
 */
/** Instrumented function jobs_exit(void(*handler)()) */
void jobs_exit(void (*handler)())
{AKA_mark("Calling: ./app-framework-binder/src/jobs.c/jobs_exit(void(*handler)())");AKA_fCall++;
		AKA_mark("lis===925###sois===24041###eois===24058###lif===2###soif===37###eoif===54###ins===true###function===./app-framework-binder/src/jobs.c/jobs_exit(void(*handler)())");struct thread *t;


	/* request all threads to stop */
		AKA_mark("lis===928###sois===24096###eois===24123###lif===5###soif===92###eoif===119###ins===true###function===./app-framework-binder/src/jobs.c/jobs_exit(void(*handler)())");pthread_mutex_lock(&mutex);


	/* set the handler */
		AKA_mark("lis===931###sois===24149###eois===24172###lif===8###soif===145###eoif===168###ins===true###function===./app-framework-binder/src/jobs.c/jobs_exit(void(*handler)())");exit_handler = handler;


	/* stops the threads */
		AKA_mark("lis===934###sois===24200###eois===24212###lif===11###soif===196###eoif===208###ins===true###function===./app-framework-binder/src/jobs.c/jobs_exit(void(*handler)())");t = threads;

		while (AKA_mark("lis===935###sois===24221###eois===24222###lif===12###soif===217###eoif===218###ifc===true###function===./app-framework-binder/src/jobs.c/jobs_exit(void(*handler)())") && (AKA_mark("lis===935###sois===24221###eois===24222###lif===12###soif===217###eoif===218###isc===true###function===./app-framework-binder/src/jobs.c/jobs_exit(void(*handler)())")&&t)) {
				AKA_mark("lis===936###sois===24228###eois===24240###lif===13###soif===224###eoif===236###ins===true###function===./app-framework-binder/src/jobs.c/jobs_exit(void(*handler)())");t->stop = 1;

				AKA_mark("lis===937###sois===24243###eois===24255###lif===14###soif===239###eoif===251###ins===true###function===./app-framework-binder/src/jobs.c/jobs_exit(void(*handler)())");t = t->next;

	}


	/* wake up the threads */
		AKA_mark("lis===941###sois===24288###eois===24304###lif===18###soif===284###eoif===300###ins===true###function===./app-framework-binder/src/jobs.c/jobs_exit(void(*handler)())");evloop_wakeup();

		AKA_mark("lis===942###sois===24306###eois===24336###lif===19###soif===302###eoif===332###ins===true###function===./app-framework-binder/src/jobs.c/jobs_exit(void(*handler)())");pthread_cond_broadcast(&cond);


	/* leave */
		AKA_mark("lis===945###sois===24352###eois===24381###lif===22###soif===348###eoif===377###ins===true###function===./app-framework-binder/src/jobs.c/jobs_exit(void(*handler)())");pthread_mutex_unlock(&mutex);

}

#endif

