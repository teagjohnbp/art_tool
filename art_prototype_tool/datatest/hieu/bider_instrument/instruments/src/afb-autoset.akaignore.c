/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_AUTOSET_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_AUTOSET_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__VERBOSE_H_
#define AKA_INCLUDE__VERBOSE_H_
#include "verbose.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_WS_H_
#define AKA_INCLUDE__AFB_API_WS_H_
#include "afb-api-ws.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_API_SO_H_
#define AKA_INCLUDE__AFB_API_SO_H_
#include "afb-api-so.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_APISET_H_
#define AKA_INCLUDE__AFB_APISET_H_
#include "afb-apiset.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_AUTOSET_H_
#define AKA_INCLUDE__AFB_AUTOSET_H_
#include "afb-autoset.akaignore.h"
#endif


/** Instrumented function cleanup(void*) */
static void cleanup(void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/afb-autoset.c/cleanup(void*)");AKA_fCall++;
		AKA_mark("lis===39###sois===1024###eois===1062###lif===2###soif===38###eoif===76###ins===true###function===./app-framework-binder/src/afb-autoset.c/cleanup(void*)");struct afb_apiset *call_set = closure;

		AKA_mark("lis===40###sois===1064###eois===1091###lif===3###soif===78###eoif===105###ins===true###function===./app-framework-binder/src/afb-autoset.c/cleanup(void*)");afb_apiset_unref(call_set);

}

/** Instrumented function onlack(void*,struct afb_apiset*,const char*,int(*create)(const char*path, struct afb_apiset*declare_set, struct afb_apiset*call_set)) */
static int onlack(void *closure, struct afb_apiset *set, const char *name, int (*create)(const char *path, struct afb_apiset *declare_set, struct afb_apiset *call_set))
{AKA_mark("Calling: ./app-framework-binder/src/afb-autoset.c/onlack(void*,struct afb_apiset*,const char*,int(*create)(const char*path, struct afb_apiset*declare_set, struct afb_apiset*call_set))");AKA_fCall++;
		AKA_mark("lis===45###sois===1267###eois===1305###lif===2###soif===172###eoif===210###ins===true###function===./app-framework-binder/src/afb-autoset.c/onlack(void*,struct afb_apiset*,const char*,int(*create)(const char*path, struct afb_apiset*declare_set, struct afb_apiset*call_set))");struct afb_apiset *call_set = closure;

		AKA_mark("lis===46###sois===1307###eois===1318###lif===3###soif===212###eoif===223###ins===true###function===./app-framework-binder/src/afb-autoset.c/onlack(void*,struct afb_apiset*,const char*,int(*create)(const char*path, struct afb_apiset*declare_set, struct afb_apiset*call_set))");char *path;

		AKA_mark("lis===47###sois===1320###eois===1337###lif===4###soif===225###eoif===242###ins===true###function===./app-framework-binder/src/afb-autoset.c/onlack(void*,struct afb_apiset*,const char*,int(*create)(const char*path, struct afb_apiset*declare_set, struct afb_apiset*call_set))");const char *base;

		AKA_mark("lis===48###sois===1339###eois===1359###lif===5###soif===244###eoif===264###ins===true###function===./app-framework-binder/src/afb-autoset.c/onlack(void*,struct afb_apiset*,const char*,int(*create)(const char*path, struct afb_apiset*declare_set, struct afb_apiset*call_set))");size_t lbase, lname;


		AKA_mark("lis===50###sois===1362###eois===1390###lif===7###soif===267###eoif===295###ins===true###function===./app-framework-binder/src/afb-autoset.c/onlack(void*,struct afb_apiset*,const char*,int(*create)(const char*path, struct afb_apiset*declare_set, struct afb_apiset*call_set))");base = afb_apiset_name(set);

		AKA_mark("lis===51###sois===1392###eois===1413###lif===8###soif===297###eoif===318###ins===true###function===./app-framework-binder/src/afb-autoset.c/onlack(void*,struct afb_apiset*,const char*,int(*create)(const char*path, struct afb_apiset*declare_set, struct afb_apiset*call_set))");lbase = strlen(base);

		AKA_mark("lis===52###sois===1415###eois===1436###lif===9###soif===320###eoif===341###ins===true###function===./app-framework-binder/src/afb-autoset.c/onlack(void*,struct afb_apiset*,const char*,int(*create)(const char*path, struct afb_apiset*declare_set, struct afb_apiset*call_set))");lname = strlen(name);


		AKA_mark("lis===54###sois===1439###eois===1472###lif===11###soif===344###eoif===377###ins===true###function===./app-framework-binder/src/afb-autoset.c/onlack(void*,struct afb_apiset*,const char*,int(*create)(const char*path, struct afb_apiset*declare_set, struct afb_apiset*call_set))");path = alloca(2 + lbase + lname);

		AKA_mark("lis===55###sois===1474###eois===1500###lif===12###soif===379###eoif===405###ins===true###function===./app-framework-binder/src/afb-autoset.c/onlack(void*,struct afb_apiset*,const char*,int(*create)(const char*path, struct afb_apiset*declare_set, struct afb_apiset*call_set))");memcpy(path, base, lbase);

		AKA_mark("lis===56###sois===1502###eois===1520###lif===13###soif===407###eoif===425###ins===true###function===./app-framework-binder/src/afb-autoset.c/onlack(void*,struct afb_apiset*,const char*,int(*create)(const char*path, struct afb_apiset*declare_set, struct afb_apiset*call_set))");path[lbase] = '/';

		AKA_mark("lis===57###sois===1522###eois===1564###lif===14###soif===427###eoif===469###ins===true###function===./app-framework-binder/src/afb-autoset.c/onlack(void*,struct afb_apiset*,const char*,int(*create)(const char*path, struct afb_apiset*declare_set, struct afb_apiset*call_set))");memcpy(&path[lbase + 1], name, lname + 1);


		AKA_mark("lis===59###sois===1567###eois===1602###lif===16###soif===472###eoif===507###ins===true###function===./app-framework-binder/src/afb-autoset.c/onlack(void*,struct afb_apiset*,const char*,int(*create)(const char*path, struct afb_apiset*declare_set, struct afb_apiset*call_set))");return create(path, set, call_set);

}

/** Instrumented function add(const char*,struct afb_apiset*,struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*)) */
static int add(const char *path, struct afb_apiset *declare_set, struct afb_apiset *call_set, int (*callback)(void *, struct afb_apiset *, const char*))
{AKA_mark("Calling: ./app-framework-binder/src/afb-autoset.c/add(const char*,struct afb_apiset*,struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*))");AKA_fCall++;
		AKA_mark("lis===64###sois===1762###eois===1788###lif===2###soif===156###eoif===182###ins===true###function===./app-framework-binder/src/afb-autoset.c/add(const char*,struct afb_apiset*,struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*))");struct afb_apiset *ownset;


	/* create a sub-apiset */
		AKA_mark("lis===67###sois===1818###eois===1882###lif===5###soif===212###eoif===276###ins===true###function===./app-framework-binder/src/afb-autoset.c/add(const char*,struct afb_apiset*,struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*))");ownset = afb_apiset_create_subset_last(declare_set, path, 3600);

		if (AKA_mark("lis===68###sois===1888###eois===1895###lif===6###soif===282###eoif===289###ifc===true###function===./app-framework-binder/src/afb-autoset.c/add(const char*,struct afb_apiset*,struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*))") && (AKA_mark("lis===68###sois===1888###eois===1895###lif===6###soif===282###eoif===289###isc===true###function===./app-framework-binder/src/afb-autoset.c/add(const char*,struct afb_apiset*,struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*))")&&!ownset)) {
				AKA_mark("lis===69###sois===1901###eois===1950###lif===7###soif===295###eoif===344###ins===true###function===./app-framework-binder/src/afb-autoset.c/add(const char*,struct afb_apiset*,struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*))");ERROR("Can't create apiset autoset-ws %s", path);

				AKA_mark("lis===70###sois===1953###eois===1963###lif===8###soif===347###eoif===357###ins===true###function===./app-framework-binder/src/afb-autoset.c/add(const char*,struct afb_apiset*,struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*))");return -1;

	}
	else {AKA_mark("lis===-68-###sois===-1888-###eois===-18887-###lif===-6-###soif===-###eoif===-289-###ins===true###function===./app-framework-binder/src/afb-autoset.c/add(const char*,struct afb_apiset*,struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*))");}


	/* set the onlack behaviour on this set */
		AKA_mark("lis===74###sois===2013###eois===2091###lif===12###soif===407###eoif===485###ins===true###function===./app-framework-binder/src/afb-autoset.c/add(const char*,struct afb_apiset*,struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*))");afb_apiset_onlack_set(ownset, callback, afb_apiset_addref(call_set), cleanup);

		AKA_mark("lis===75###sois===2093###eois===2102###lif===13###soif===487###eoif===496###ins===true###function===./app-framework-binder/src/afb-autoset.c/add(const char*,struct afb_apiset*,struct afb_apiset*,int(*callback)(void*, struct afb_apiset*, const char*))");return 0;

}

/*******************************************************************/

/** Instrumented function create_ws(const char*,struct afb_apiset*,struct afb_apiset*) */
static int create_ws(const char *path, struct afb_apiset *declare_set, struct afb_apiset *call_set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-autoset.c/create_ws(const char*,struct afb_apiset*,struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===82###sois===2280###eois===2346###lif===2###soif===103###eoif===169###ins===true###function===./app-framework-binder/src/afb-autoset.c/create_ws(const char*,struct afb_apiset*,struct afb_apiset*)");return afb_api_ws_add_client(path, declare_set, call_set, 0) >= 0;

}

/** Instrumented function onlack_ws(void*,struct afb_apiset*,const char*) */
static int onlack_ws(void *closure, struct afb_apiset *set, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-autoset.c/onlack_ws(void*,struct afb_apiset*,const char*)");AKA_fCall++;
		AKA_mark("lis===87###sois===2431###eois===2476###lif===2###soif===81###eoif===126###ins===true###function===./app-framework-binder/src/afb-autoset.c/onlack_ws(void*,struct afb_apiset*,const char*)");return onlack(closure, set, name, create_ws);

}

/** Instrumented function afb_autoset_add_ws(const char*,struct afb_apiset*,struct afb_apiset*) */
int afb_autoset_add_ws(const char *path, struct afb_apiset *declare_set, struct afb_apiset *call_set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-autoset.c/afb_autoset_add_ws(const char*,struct afb_apiset*,struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===92###sois===2585###eois===2636###lif===2###soif===105###eoif===156###ins===true###function===./app-framework-binder/src/afb-autoset.c/afb_autoset_add_ws(const char*,struct afb_apiset*,struct afb_apiset*)");return add(path, declare_set, call_set, onlack_ws);

}

/*******************************************************************/

#if WITH_DYNAMIC_BINDING
static int create_so(const char *path, struct afb_apiset *declare_set, struct afb_apiset *call_set)
{
	return afb_api_so_add_binding(path, declare_set, call_set) >= 0;
}

static int onlack_so(void *closure, struct afb_apiset *set, const char *name)
{
	return onlack(closure, set, name, create_so);
}

int afb_autoset_add_so(const char *path, struct afb_apiset *declare_set, struct afb_apiset *call_set)
{
	return add(path, declare_set, call_set, onlack_so);
}
#endif

/*******************************************************************/

/** Instrumented function create_any(const char*,struct afb_apiset*,struct afb_apiset*) */
static int create_any(const char *path, struct afb_apiset *declare_set, struct afb_apiset *call_set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===118###sois===3379###eois===3386###lif===2###soif===104###eoif===111###ins===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");int rc;

		AKA_mark("lis===119###sois===3388###eois===3403###lif===3###soif===113###eoif===128###ins===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");struct stat st;

		AKA_mark("lis===120###sois===3405###eois===3433###lif===4###soif===130###eoif===158###ins===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");char sockname[PATH_MAX + 7];


		AKA_mark("lis===122###sois===3436###eois===3457###lif===6###soif===161###eoif===182###ins===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");rc = stat(path, &st);

		if (AKA_mark("lis===123###sois===3463###eois===3466###lif===7###soif===188###eoif===191###ifc===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)") && (AKA_mark("lis===123###sois===3463###eois===3466###lif===7###soif===188###eoif===191###isc===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)")&&!rc)) {
				AKA_mark("lis===124###sois===3479###eois===3498###lif===8###soif===204###eoif===223###ins===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");switch(st.st_mode & S_IFMT){
#if WITH_DYNAMIC_BINDING
		case S_IFREG:
			rc = afb_api_so_add_binding(path, declare_set, call_set);
			break;
#endif
					case S_IFSOCK: if(st.st_mode & S_IFMT == S_IFSOCK)AKA_mark("lis===130###sois===3623###eois===3637###lif===14###soif===348###eoif===362###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");

						AKA_mark("lis===131###sois===3641###eois===3694###lif===15###soif===366###eoif===419###ins===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");snprintf(sockname, sizeof sockname, "unix:%s", path);

						AKA_mark("lis===132###sois===3698###eois===3761###lif===16###soif===423###eoif===486###ins===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");rc = afb_api_ws_add_client(sockname, declare_set, call_set, 0);

						AKA_mark("lis===133###sois===3765###eois===3771###lif===17###soif===490###eoif===496###ins===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");break;

					default: if(st.st_mode & S_IFMT != S_IFSOCK)AKA_mark("lis===134###sois===3774###eois===3782###lif===18###soif===499###eoif===507###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");

						AKA_mark("lis===135###sois===3786###eois===3831###lif===19###soif===511###eoif===556###ins===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");NOTICE("Unexpected autoset entry: %s", path);

						AKA_mark("lis===136###sois===3835###eois===3843###lif===20###soif===560###eoif===568###ins===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");rc = -1;

						AKA_mark("lis===137###sois===3847###eois===3853###lif===21###soif===572###eoif===578###ins===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");break;

		}

	}
	else {AKA_mark("lis===-123-###sois===-3463-###eois===-34633-###lif===-7-###soif===-###eoif===-191-###ins===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");}

		AKA_mark("lis===140###sois===3862###eois===3877###lif===24###soif===587###eoif===602###ins===true###function===./app-framework-binder/src/afb-autoset.c/create_any(const char*,struct afb_apiset*,struct afb_apiset*)");return rc >= 0;

}

/** Instrumented function onlack_any(void*,struct afb_apiset*,const char*) */
static int onlack_any(void *closure, struct afb_apiset *set, const char *name)
{AKA_mark("Calling: ./app-framework-binder/src/afb-autoset.c/onlack_any(void*,struct afb_apiset*,const char*)");AKA_fCall++;
		AKA_mark("lis===145###sois===3963###eois===4009###lif===2###soif===82###eoif===128###ins===true###function===./app-framework-binder/src/afb-autoset.c/onlack_any(void*,struct afb_apiset*,const char*)");return onlack(closure, set, name, create_any);

}

/** Instrumented function afb_autoset_add_any(const char*,struct afb_apiset*,struct afb_apiset*) */
int afb_autoset_add_any(const char *path, struct afb_apiset *declare_set, struct afb_apiset *call_set)
{AKA_mark("Calling: ./app-framework-binder/src/afb-autoset.c/afb_autoset_add_any(const char*,struct afb_apiset*,struct afb_apiset*)");AKA_fCall++;
		AKA_mark("lis===150###sois===4119###eois===4171###lif===2###soif===106###eoif===158###ins===true###function===./app-framework-binder/src/afb-autoset.c/afb_autoset_add_any(const char*,struct afb_apiset*,struct afb_apiset*)");return add(path, declare_set, call_set, onlack_any);

}

#endif

