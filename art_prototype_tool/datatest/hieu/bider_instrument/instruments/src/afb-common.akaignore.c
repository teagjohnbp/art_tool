/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_COMMON_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_AFB_COMMON_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__AFB_COMMON_H_
#define AKA_INCLUDE__AFB_COMMON_H_
#include "afb-common.akaignore.h"
#endif

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__LOCALE_ROOT_H_
#define AKA_INCLUDE__LOCALE_ROOT_H_
#include "locale-root.akaignore.h"
#endif


static const char *default_locale = NULL;
static struct locale_root *rootdir = NULL;

/** Instrumented function afb_common_default_locale_set(const char*) */
void afb_common_default_locale_set(const char *locale)
{AKA_mark("Calling: ./app-framework-binder/src/afb-common.c/afb_common_default_locale_set(const char*)");AKA_fCall++;
		AKA_mark("lis===33###sois===943###eois===967###lif===2###soif===58###eoif===82###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_default_locale_set(const char*)");default_locale = locale;

}

/** Instrumented function afb_common_default_locale_get() */
const char *afb_common_default_locale_get()
{AKA_mark("Calling: ./app-framework-binder/src/afb-common.c/afb_common_default_locale_get()");AKA_fCall++;
		AKA_mark("lis===38###sois===1018###eois===1040###lif===2###soif===47###eoif===69###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_default_locale_get()");return default_locale;

}

/** Instrumented function afb_common_rootdir_set(const char*) */
int afb_common_rootdir_set(const char *dirname)
{AKA_mark("Calling: ./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");AKA_fCall++;
		AKA_mark("lis===43###sois===1095###eois===1109###lif===2###soif===51###eoif===65###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");int dirfd, rc;

		AKA_mark("lis===44###sois===1111###eois===1136###lif===3###soif===67###eoif===92###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");struct locale_root *root;

		AKA_mark("lis===45###sois===1138###eois===1167###lif===4###soif===94###eoif===123###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");struct locale_search *search;


		AKA_mark("lis===47###sois===1170###eois===1178###lif===6###soif===126###eoif===134###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");rc = -1;

		AKA_mark("lis===48###sois===1180###eois===1234###lif===7###soif===136###eoif===190###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");dirfd = openat(AT_FDCWD, dirname, O_PATH|O_DIRECTORY);

		if (AKA_mark("lis===49###sois===1240###eois===1249###lif===8###soif===196###eoif===205###ifc===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)") && (AKA_mark("lis===49###sois===1240###eois===1249###lif===8###soif===196###eoif===205###isc===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)")&&dirfd < 0)) {AKA_mark("lis===+49+###sois===+1240+###eois===+12409+###lif===+8+###soif===+###eoif===+205+###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");}
	else {
				AKA_mark("lis===52###sois===1286###eois===1319###lif===11###soif===242###eoif===275###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");root = locale_root_create(dirfd);

				if (AKA_mark("lis===53###sois===1326###eois===1338###lif===12###soif===282###eoif===294###ifc===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)") && (AKA_mark("lis===53###sois===1326###eois===1338###lif===12###soif===282###eoif===294###isc===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)")&&root == NULL)) {
			/* TODO message */
						AKA_mark("lis===55###sois===1367###eois===1380###lif===14###soif===323###eoif===336###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");close(dirfd);

		}
		else {
						AKA_mark("lis===57###sois===1395###eois===1402###lif===16###soif===351###eoif===358###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");rc = 0;

						if (AKA_mark("lis===58###sois===1410###eois===1432###lif===17###soif===366###eoif===388###ifc===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)") && (AKA_mark("lis===58###sois===1410###eois===1432###lif===17###soif===366###eoif===388###isc===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)")&&default_locale != NULL)) {
								AKA_mark("lis===59###sois===1440###eois===1493###lif===18###soif===396###eoif===449###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");search = locale_root_search(root, default_locale, 0);

								if (AKA_mark("lis===60###sois===1502###eois===1516###lif===19###soif===458###eoif===472###ifc===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)") && (AKA_mark("lis===60###sois===1502###eois===1516###lif===19###soif===458###eoif===472###isc===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)")&&search == NULL)) {AKA_mark("lis===+60+###sois===+1502+###eois===+150214+###lif===+19+###soif===+###eoif===+472+###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");}
				else {
										AKA_mark("lis===63###sois===1562###eois===1607###lif===22###soif===518###eoif===563###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");locale_root_set_default_search(root, search);

										AKA_mark("lis===64###sois===1613###eois===1641###lif===23###soif===569###eoif===597###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");locale_search_unref(search);

				}

			}
			else {AKA_mark("lis===-58-###sois===-1410-###eois===-141022-###lif===-17-###soif===-###eoif===-388-###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");}

						AKA_mark("lis===67###sois===1656###eois===1683###lif===26###soif===612###eoif===639###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");locale_root_unref(rootdir);

						AKA_mark("lis===68###sois===1687###eois===1702###lif===27###soif===643###eoif===658###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");rootdir = root;

		}

	}

		AKA_mark("lis===71###sois===1711###eois===1721###lif===30###soif===667###eoif===677###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_set(const char*)");return rc;

}

/** Instrumented function afb_common_rootdir_get_fd() */
int afb_common_rootdir_get_fd()
{AKA_mark("Calling: ./app-framework-binder/src/afb-common.c/afb_common_rootdir_get_fd()");AKA_fCall++;
		AKA_mark("lis===76###sois===1760###eois===1798###lif===2###soif===35###eoif===73###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_get_fd()");return locale_root_get_dirfd(rootdir);

}

/** Instrumented function afb_common_rootdir_open_locale(const char*,int,const char*) */
int afb_common_rootdir_open_locale(const char *filename, int flags, const char *locale)
{AKA_mark("Calling: ./app-framework-binder/src/afb-common.c/afb_common_rootdir_open_locale(const char*,int,const char*)");AKA_fCall++;
		AKA_mark("lis===81###sois===1893###eois===1951###lif===2###soif===91###eoif===149###ins===true###function===./app-framework-binder/src/afb-common.c/afb_common_rootdir_open_locale(const char*,int,const char*)");return locale_root_open(rootdir, filename, flags, locale);

}



#endif

