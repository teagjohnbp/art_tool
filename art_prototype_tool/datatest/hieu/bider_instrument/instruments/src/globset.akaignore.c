/** Guard statement to avoid multiple declaration */
#ifndef AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_GLOBSET_C
#define AKA_SRC__HOME_LAMNT_AKAUTAUTO_DATATEST_THESIS_APP_FRAMEWORK_BINDER_SRC_GLOBSET_C
extern int strcmp(const char * str1, const char * str2);
extern int AKA_mark(char* str);
extern void AKA_assert(char* actualName, int actualVal, char* expectedName, int expectedVal);
extern int AKA_assert_double(char* actualName, double actualVal, char* expectedName, double expectedVal);
extern int AKA_assert_ptr(char* actualName, void* actualVal, char* expectedName, void* expectedVal);
extern int AKA_fCall;
extern char* AKA_test_case_name;



/*
 * Copyright (C) 2015-2020 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <errno.h>

/** Guard statement to avoid multiple declaration */
#ifndef AKA_INCLUDE__GLOBSET_H_
#define AKA_INCLUDE__GLOBSET_H_
#include "globset.akaignore.h"
#endif


#define GLOB '*'

/*************************************************************************
 * internal types
 ************************************************************************/

/**
 * internal structure for handling patterns
 */
struct pathndl
{
	/* link to the next handler of the list */
	struct pathndl *next;

	/* the hash value for the pattern */
	size_t hash;

	/* the handler */
	struct globset_handler handler;
};

/**
 * Structure that handles a set of global pattern handlers
 */
struct globset
{
	/** linked list of global patterns */
	struct pathndl *globs;

	/** hash dictionary of exact matches */
	struct pathndl **exacts;

	/** mask used for accessing dictionary of exact matches */
	unsigned gmask;

	/** count of handlers stored in the dictionary of exact matches */
	unsigned count;
};

/**
 * Matches whether the string 'str' matches the pattern 'pat'
 * and returns its matching score.
 *
 * @param pat the glob pattern
 * @param str the string to match
 * @return 0 if no match or number representing the matching score
 */
/** Instrumented function match(const char*,const char*) */
static unsigned match(const char *pat, const char *str)
{AKA_mark("Calling: ./app-framework-binder/src/globset.c/match(const char*,const char*)");AKA_fCall++;
		AKA_mark("lis===75###sois===1862###eois===1881###lif===2###soif===59###eoif===78###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");unsigned r, rs, rr;

		AKA_mark("lis===76###sois===1883###eois===1890###lif===3###soif===80###eoif===87###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");char c;


	/* scan the prefix without glob */
		AKA_mark("lis===79###sois===1929###eois===1935###lif===6###soif===126###eoif===132###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");r = 1;

		while (AKA_mark("lis===80###sois===1944###eois===1964###lif===7###soif===141###eoif===161###ifc===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)") && (AKA_mark("lis===80###sois===1944###eois===1964###lif===7###soif===141###eoif===161###isc===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)")&&(c = *pat++) != GLOB)) {
				if (AKA_mark("lis===81###sois===1974###eois===1985###lif===8###soif===171###eoif===182###ifc===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)") && (AKA_mark("lis===81###sois===1974###eois===1985###lif===8###soif===171###eoif===182###isc===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)")&&c != *str++)) {
			AKA_mark("lis===82###sois===1990###eois===1999###lif===9###soif===187###eoif===196###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");return 0;
		}
		else {AKA_mark("lis===-81-###sois===-1974-###eois===-197411-###lif===-8-###soif===-###eoif===-182-###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");}
 /* no match */
				if (AKA_mark("lis===83###sois===2021###eois===2023###lif===10###soif===218###eoif===220###ifc===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)") && (AKA_mark("lis===83###sois===2021###eois===2023###lif===10###soif===218###eoif===220###isc===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)")&&!c)) {
			AKA_mark("lis===84###sois===2028###eois===2037###lif===11###soif===225###eoif===234###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");return r;
		}
		else {AKA_mark("lis===-83-###sois===-2021-###eois===-20212-###lif===-10-###soif===-###eoif===-220-###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");}
 /* match up to end */
				AKA_mark("lis===85###sois===2062###eois===2066###lif===12###soif===259###eoif===263###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");r++;

	}


	/* glob found */
		AKA_mark("lis===89###sois===2090###eois===2101###lif===16###soif===287###eoif===298###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");c = *pat++;

		if (AKA_mark("lis===90###sois===2107###eois===2109###lif===17###soif===304###eoif===306###ifc===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)") && (AKA_mark("lis===90###sois===2107###eois===2109###lif===17###soif===304###eoif===306###isc===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)")&&!c)) {
		AKA_mark("lis===91###sois===2113###eois===2122###lif===18###soif===310###eoif===319###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");return r;
	}
	else {AKA_mark("lis===-90-###sois===-2107-###eois===-21072-###lif===-17-###soif===-###eoif===-306-###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");}
 /* not followed by pattern */

	/* evaluate the best score for following pattern */
		AKA_mark("lis===94###sois===2208###eois===2215###lif===21###soif===405###eoif===412###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");rs = 0;

		while (AKA_mark("lis===95###sois===2224###eois===2228###lif===22###soif===421###eoif===425###ifc===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)") && (AKA_mark("lis===95###sois===2224###eois===2228###lif===22###soif===421###eoif===425###isc===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)")&&*str)) {
				if (AKA_mark("lis===96###sois===2238###eois===2249###lif===23###soif===435###eoif===446###ifc===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)") && (AKA_mark("lis===96###sois===2238###eois===2249###lif===23###soif===435###eoif===446###isc===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)")&&*str++ == c)) {
			/* first char matches, check remaining string */
						AKA_mark("lis===98###sois===2308###eois===2329###lif===25###soif===505###eoif===526###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");rr = match(pat, str);

						if (AKA_mark("lis===99###sois===2337###eois===2344###lif===26###soif===534###eoif===541###ifc===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)") && (AKA_mark("lis===99###sois===2337###eois===2344###lif===26###soif===534###eoif===541###isc===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)")&&rr > rs)) {
				AKA_mark("lis===100###sois===2350###eois===2358###lif===27###soif===547###eoif===555###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");rs = rr;
			}
			else {AKA_mark("lis===-99-###sois===-2337-###eois===-23377-###lif===-26-###soif===-###eoif===-541-###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");}

		}
		else {AKA_mark("lis===-96-###sois===-2238-###eois===-223811-###lif===-23-###soif===-###eoif===-446-###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");}

	}


	/* best score or not match if rs == 0 */
		AKA_mark("lis===105###sois===2410###eois===2433###lif===32###soif===607###eoif===630###ins===true###function===./app-framework-binder/src/globset.c/match(const char*,const char*)");return rs ? rs + r : 0;

}

/**
 * Normalize the string 'from' to the string 'to' and computes the hash code.
 * The normalization translates upper characters to lower characters.
 * The returned hash code is greater than zero for exact patterns or zero
 * for global patterns.
 *
 * @param from string to normalize
 * @param to where to store the normalization
 * @return 0 if 'from' is a glob pattern or the hash code for exacts patterns
 */
/** Instrumented function normhash(const char*,char*) */
static unsigned normhash(const char *from, char *to)
{AKA_mark("Calling: ./app-framework-binder/src/globset.c/normhash(const char*,char*)");AKA_fCall++;
		AKA_mark("lis===120###sois===2909###eois===2926###lif===2###soif===56###eoif===73###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");unsigned i, hash;

		AKA_mark("lis===121###sois===2928###eois===2939###lif===3###soif===75###eoif===86###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");int isglob;

		AKA_mark("lis===122###sois===2941###eois===2948###lif===4###soif===88###eoif===95###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");char c;


		AKA_mark("lis===124###sois===2951###eois===2962###lif===6###soif===98###eoif===109###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");isglob = 0;
 /* is glob? */
		AKA_mark("lis===125###sois===2979###eois===2992###lif===7###soif===126###eoif===139###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");i = hash = 0;
 /* index and hash code */

	/* copy the normalized string and compute isglob and hash */
		while (AKA_mark("lis===128###sois===3090###eois===3103###lif===10###soif===237###eoif===250###ifc===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)") && (c=(from[i]) && AKA_mark("lis===128###sois===3091###eois===3102###lif===10###soif===238###eoif===249###isc===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)"))) {
				if (AKA_mark("lis===129###sois===3113###eois===3122###lif===11###soif===260###eoif===269###ifc===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)") && (AKA_mark("lis===129###sois===3113###eois===3122###lif===11###soif===260###eoif===269###isc===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)")&&c == GLOB)) {
			/* it is a glob pattern */
						AKA_mark("lis===131###sois===3159###eois===3170###lif===13###soif===306###eoif===317###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");isglob = 1;

						AKA_mark("lis===132###sois===3174###eois===3183###lif===14###soif===321###eoif===330###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");hash = 0;

		}
		else {
			/* normalize to lower */
						if (AKA_mark("lis===135###sois===3230###eois===3250###lif===17###soif===377###eoif===397###ifc===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)") && ((AKA_mark("lis===135###sois===3230###eois===3238###lif===17###soif===377###eoif===385###isc===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)")&&c >= 'A')	&&(AKA_mark("lis===135###sois===3242###eois===3250###lif===17###soif===389###eoif===397###isc===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)")&&c <= 'Z'))) {
				AKA_mark("lis===136###sois===3256###eois===3282###lif===18###soif===403###eoif===429###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");c = (char)(c + 'a' - 'A');
			}
			else {AKA_mark("lis===-135-###sois===-3230-###eois===-323020-###lif===-17-###soif===-###eoif===-397-###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");}

			/* compute hash if not glob */
						if (AKA_mark("lis===138###sois===3324###eois===3331###lif===20###soif===471###eoif===478###ifc===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)") && (AKA_mark("lis===138###sois===3324###eois===3331###lif===20###soif===471###eoif===478###isc===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)")&&!isglob)) {
								AKA_mark("lis===139###sois===3339###eois===3374###lif===21###soif===486###eoif===521###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");hash += (unsigned)(unsigned char)c;

								AKA_mark("lis===140###sois===3379###eois===3398###lif===22###soif===526###eoif===545###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");hash += hash << 10;

								AKA_mark("lis===141###sois===3403###eois===3421###lif===23###soif===550###eoif===568###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");hash ^= hash >> 6;

			}
			else {AKA_mark("lis===-138-###sois===-3324-###eois===-33247-###lif===-20-###soif===-###eoif===-478-###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");}

		}

				AKA_mark("lis===144###sois===3433###eois===3445###lif===26###soif===580###eoif===592###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");to[i++] = c;

	}

		AKA_mark("lis===146###sois===3450###eois===3460###lif===28###soif===597###eoif===607###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");to[i] = c;

		if (AKA_mark("lis===147###sois===3466###eois===3473###lif===29###soif===613###eoif===620###ifc===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)") && (AKA_mark("lis===147###sois===3466###eois===3473###lif===29###soif===613###eoif===620###isc===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)")&&!isglob)) {
		/* finalize hash if not glob */
				AKA_mark("lis===149###sois===3513###eois===3523###lif===31###soif===660###eoif===670###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");hash += i;

				AKA_mark("lis===150###sois===3526###eois===3544###lif===32###soif===673###eoif===691###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");hash += hash << 3;

				AKA_mark("lis===151###sois===3547###eois===3566###lif===33###soif===694###eoif===713###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");hash ^= hash >> 11;

				AKA_mark("lis===152###sois===3569###eois===3588###lif===34###soif===716###eoif===735###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");hash += hash << 15;

				if (AKA_mark("lis===153###sois===3595###eois===3604###lif===35###soif===742###eoif===751###ifc===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)") && (AKA_mark("lis===153###sois===3595###eois===3604###lif===35###soif===742###eoif===751###isc===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)")&&hash == 0)) {
			AKA_mark("lis===154###sois===3609###eois===3622###lif===36###soif===756###eoif===769###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");hash = 1 + i;
		}
		else {AKA_mark("lis===-153-###sois===-3595-###eois===-35959-###lif===-35-###soif===-###eoif===-751-###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");}

	}
	else {AKA_mark("lis===-147-###sois===-3466-###eois===-34667-###lif===-29-###soif===-###eoif===-620-###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");}

		AKA_mark("lis===156###sois===3627###eois===3639###lif===38###soif===774###eoif===786###ins===true###function===./app-framework-binder/src/globset.c/normhash(const char*,char*)");return hash;

}

/**
 * Ensure that there is enough place to store one more exact pattern
 * in the dictionary.
 *
 * @param set the set that has to grow if needed
 * @return 0 in case of success or -1 on case of error
 */
/** Instrumented function grow(struct globset*) */
static int grow(struct globset *set)
{AKA_mark("Calling: ./app-framework-binder/src/globset.c/grow(struct globset*)");AKA_fCall++;
		AKA_mark("lis===168###sois===3889###eois===3922###lif===2###soif===40###eoif===73###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");unsigned old_gmask, new_gmask, i;

		AKA_mark("lis===169###sois===3924###eois===3981###lif===3###soif===75###eoif===132###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");struct pathndl **new_exacts, **old_exacts, *ph, *next_ph;


	/* test if grow is needed */
		AKA_mark("lis===172###sois===4014###eois===4037###lif===6###soif===165###eoif===188###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");old_gmask = set->gmask;

		if (AKA_mark("lis===173###sois===4043###eois===4088###lif===7###soif===194###eoif===239###ifc===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)") && (AKA_mark("lis===173###sois===4043###eois===4088###lif===7###soif===194###eoif===239###isc===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)")&&old_gmask - (old_gmask >> 2) > set->count + 1)) {
		AKA_mark("lis===174###sois===4092###eois===4101###lif===8###soif===243###eoif===252###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");return 0;
	}
	else {AKA_mark("lis===-173-###sois===-4043-###eois===-404345-###lif===-7-###soif===-###eoif===-239-###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");}


	/* compute the new mask */
		AKA_mark("lis===177###sois===4132###eois===4166###lif===11###soif===283###eoif===317###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");new_gmask = (old_gmask << 1) | 15;

		if (AKA_mark("lis===178###sois===4172###eois===4198###lif===12###soif===323###eoif===349###ifc===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)") && (AKA_mark("lis===178###sois===4172###eois===4198###lif===12###soif===323###eoif===349###isc===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)")&&new_gmask + 1 <= new_gmask)) {
				AKA_mark("lis===179###sois===4204###eois===4222###lif===13###soif===355###eoif===373###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");errno = EOVERFLOW;

				AKA_mark("lis===180###sois===4225###eois===4235###lif===14###soif===376###eoif===386###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");return -1;

	}
	else {AKA_mark("lis===-178-###sois===-4172-###eois===-417226-###lif===-12-###soif===-###eoif===-349-###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");}


	/* allocates the new array */
		AKA_mark("lis===184###sois===4272###eois===4335###lif===18###soif===423###eoif===486###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");new_exacts = calloc(1 + (size_t)new_gmask, sizeof *new_exacts);

		if (AKA_mark("lis===185###sois===4341###eois===4352###lif===19###soif===492###eoif===503###ifc===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)") && (AKA_mark("lis===185###sois===4341###eois===4352###lif===19###soif===492###eoif===503###isc===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)")&&!new_exacts)) {
		AKA_mark("lis===186###sois===4356###eois===4366###lif===20###soif===507###eoif===517###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");return -1;
	}
	else {AKA_mark("lis===-185-###sois===-4341-###eois===-434111-###lif===-19-###soif===-###eoif===-503-###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");}


	/* copy values */
		AKA_mark("lis===189###sois===4388###eois===4413###lif===23###soif===539###eoif===564###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");old_exacts = set->exacts;

		AKA_mark("lis===190###sois===4415###eois===4440###lif===24###soif===566###eoif===591###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");set->exacts = new_exacts;

		AKA_mark("lis===191###sois===4442###eois===4465###lif===25###soif===593###eoif===616###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");set->gmask = new_gmask;

		if (AKA_mark("lis===192###sois===4471###eois===4480###lif===26###soif===622###eoif===631###ifc===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)") && (AKA_mark("lis===192###sois===4471###eois===4480###lif===26###soif===622###eoif===631###isc===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)")&&old_gmask)) {
				AKA_mark("lis===193###sois===4490###eois===4497###lif===27###soif===641###eoif===648###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");for (i = 0 ;AKA_mark("lis===193###sois===4498###eois===4512###lif===27###soif===649###eoif===663###ifc===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)") && AKA_mark("lis===193###sois===4498###eois===4512###lif===27###soif===649###eoif===663###isc===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)")&&i <= old_gmask;({AKA_mark("lis===193###sois===4515###eois===4518###lif===27###soif===666###eoif===669###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");i++;})) {
						AKA_mark("lis===194###sois===4525###eois===4544###lif===28###soif===676###eoif===695###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");ph = old_exacts[i];

						while (AKA_mark("lis===195###sois===4555###eois===4557###lif===29###soif===706###eoif===708###ifc===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)") && (AKA_mark("lis===195###sois===4555###eois===4557###lif===29###soif===706###eoif===708###isc===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)")&&ph)) {
								AKA_mark("lis===196###sois===4565###eois===4584###lif===30###soif===716###eoif===735###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");next_ph = ph->next;

								AKA_mark("lis===197###sois===4589###eois===4633###lif===31###soif===740###eoif===784###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");ph->next = new_exacts[ph->hash & new_gmask];

								AKA_mark("lis===198###sois===4638###eois===4676###lif===32###soif===789###eoif===827###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");new_exacts[ph->hash & new_gmask] = ph;

								AKA_mark("lis===199###sois===4681###eois===4694###lif===33###soif===832###eoif===845###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");ph = next_ph;

			}

		}

				AKA_mark("lis===202###sois===4706###eois===4723###lif===36###soif===857###eoif===874###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");free(old_exacts);

	}
	else {AKA_mark("lis===-192-###sois===-4471-###eois===-44719-###lif===-26-###soif===-###eoif===-631-###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");}


	/* release the previous values */
		AKA_mark("lis===206###sois===4764###eois===4773###lif===40###soif===915###eoif===924###ins===true###function===./app-framework-binder/src/globset.c/grow(struct globset*)");return 0;

}

/**
 * Search in set the handler for the normalized pattern 'normal' of 'has' code.
 *
 * @param set the set to search
 * @param normal the normalized pattern
 * @param hash hash code of the normalized pattern
 * @param pprev pointer where to store the pointer pointing to the returned result
 * @return the handler found, can be NULL
 */
/** Instrumented function search(struct globset*,const char*,unsigned,struct pathndl***) */
static struct pathndl *search(
		struct globset *set,
		const char *normal,
		unsigned hash,
		struct pathndl ***pprev)
{AKA_mark("Calling: ./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)");AKA_fCall++;
		AKA_mark("lis===224###sois===5239###eois===5265###lif===6###soif===123###eoif===149###ins===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)");struct pathndl *ph, **pph;


		if (AKA_mark("lis===226###sois===5272###eois===5277###lif===8###soif===156###eoif===161###ifc===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)") && (AKA_mark("lis===226###sois===5272###eois===5277###lif===8###soif===156###eoif===161###isc===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)")&&!hash)) {
		AKA_mark("lis===227###sois===5281###eois===5299###lif===9###soif===165###eoif===183###ins===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)");pph = &set->globs;
	}
	else {
		if (AKA_mark("lis===228###sois===5310###eois===5321###lif===10###soif===194###eoif===205###ifc===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)") && (AKA_mark("lis===228###sois===5310###eois===5321###lif===10###soif===194###eoif===205###isc===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)")&&set->exacts)) {
			AKA_mark("lis===229###sois===5325###eois===5363###lif===11###soif===209###eoif===247###ins===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)");pph = &set->exacts[hash & set->gmask];
		}
		else {
					AKA_mark("lis===231###sois===5374###eois===5388###lif===13###soif===258###eoif===272###ins===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)");*pprev = NULL;

					AKA_mark("lis===232###sois===5391###eois===5403###lif===14###soif===275###eoif===287###ins===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)");return NULL;

	}
	}

		while (AKA_mark("lis===234###sois===5415###eois===5465###lif===16###soif===299###eoif===349###ifc===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)") && ((ph=(*pph) && AKA_mark("lis===234###sois===5416###eois===5425###lif===16###soif===300###eoif===309###isc===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)"))	&&(AKA_mark("lis===234###sois===5430###eois===5465###lif===16###soif===314###eoif===349###isc===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)")&&strcmp(normal, ph->handler.pattern)))) {
		AKA_mark("lis===235###sois===5469###eois===5485###lif===17###soif===353###eoif===369###ins===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)");pph = &ph->next;
	}

		AKA_mark("lis===236###sois===5487###eois===5500###lif===18###soif===371###eoif===384###ins===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)");*pprev = pph;

		AKA_mark("lis===237###sois===5502###eois===5512###lif===19###soif===386###eoif===396###ins===true###function===./app-framework-binder/src/globset.c/search(struct globset*,const char*,unsigned,struct pathndl***)");return ph;

}

/**
 * Allocates a new set of handlers
 *
 * @return the new allocated global pattern set of handlers or NULL on failure
 */
/** Instrumented function globset_create() */
struct globset *globset_create()
{AKA_mark("Calling: ./app-framework-binder/src/globset.c/globset_create()");AKA_fCall++;
		AKA_mark("lis===247###sois===5677###eois===5718###lif===2###soif===36###eoif===77###ins===true###function===./app-framework-binder/src/globset.c/globset_create()");return calloc(1, sizeof(struct globset));

}

/**
 * Destroy the set
 * @param set the set to destroy
 */
/** Instrumented function globset_destroy(struct globset*) */
void globset_destroy(struct globset *set)
{AKA_mark("Calling: ./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");AKA_fCall++;
		AKA_mark("lis===256###sois===5827###eois===5838###lif===2###soif===45###eoif===56###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");unsigned i;

		AKA_mark("lis===257###sois===5840###eois===5869###lif===3###soif===58###eoif===87###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");struct pathndl *ph, *next_ph;


	/* free exact pattern handlers */
		if (AKA_mark("lis===260###sois===5911###eois===5921###lif===6###soif===129###eoif===139###ifc===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)") && (AKA_mark("lis===260###sois===5911###eois===5921###lif===6###soif===129###eoif===139###isc===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)")&&set->gmask)) {
				AKA_mark("lis===261###sois===5931###eois===5938###lif===7###soif===149###eoif===156###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");for (i = 0 ;AKA_mark("lis===261###sois===5939###eois===5954###lif===7###soif===157###eoif===172###ifc===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)") && AKA_mark("lis===261###sois===5939###eois===5954###lif===7###soif===157###eoif===172###isc===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)")&&i <= set->gmask;({AKA_mark("lis===261###sois===5957###eois===5960###lif===7###soif===175###eoif===178###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");i++;})) {
						AKA_mark("lis===262###sois===5967###eois===5987###lif===8###soif===185###eoif===205###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");ph = set->exacts[i];

						while (AKA_mark("lis===263###sois===5998###eois===6000###lif===9###soif===216###eoif===218###ifc===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)") && (AKA_mark("lis===263###sois===5998###eois===6000###lif===9###soif===216###eoif===218###isc===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)")&&ph)) {
								AKA_mark("lis===264###sois===6008###eois===6027###lif===10###soif===226###eoif===245###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");next_ph = ph->next;

								AKA_mark("lis===265###sois===6032###eois===6041###lif===11###soif===250###eoif===259###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");free(ph);

								AKA_mark("lis===266###sois===6046###eois===6059###lif===12###soif===264###eoif===277###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");ph = next_ph;

			}

		}

				AKA_mark("lis===269###sois===6071###eois===6089###lif===15###soif===289###eoif===307###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");free(set->exacts);

	}
	else {AKA_mark("lis===-260-###sois===-5911-###eois===-591110-###lif===-6-###soif===-###eoif===-139-###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");}


	/* free global pattern handlers */
		AKA_mark("lis===273###sois===6131###eois===6147###lif===19###soif===349###eoif===365###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");ph = set->globs;

		while (AKA_mark("lis===274###sois===6156###eois===6158###lif===20###soif===374###eoif===376###ifc===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)") && (AKA_mark("lis===274###sois===6156###eois===6158###lif===20###soif===374###eoif===376###isc===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)")&&ph)) {
				AKA_mark("lis===275###sois===6164###eois===6183###lif===21###soif===382###eoif===401###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");next_ph = ph->next;

				AKA_mark("lis===276###sois===6186###eois===6195###lif===22###soif===404###eoif===413###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");free(ph);

				AKA_mark("lis===277###sois===6198###eois===6211###lif===23###soif===416###eoif===429###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");ph = next_ph;

	}


	/* free the set */
		AKA_mark("lis===281###sois===6237###eois===6247###lif===27###soif===455###eoif===465###ins===true###function===./app-framework-binder/src/globset.c/globset_destroy(struct globset*)");free(set);

}

/**
 * Add a handler for the given pattern
 * @param set the set
 * @param pattern the pattern of the handler
 * @param callback the handler's callback
 * @param closure the handler's closure
 * @return 0 in case of success or -1 in case of error (ENOMEM, EEXIST)
 */
/** Instrumented function globset_add(struct globset*,const char*,void*,void*) */
int globset_add(
		struct globset *set,
		const char *pattern,
		void *callback,
		void *closure)
{AKA_mark("Calling: ./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");AKA_fCall++;
		AKA_mark("lis===298###sois===6620###eois===6646###lif===6###soif===101###eoif===127###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");struct pathndl *ph, **pph;

		AKA_mark("lis===299###sois===6648###eois===6662###lif===7###soif===129###eoif===143###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");unsigned hash;

		AKA_mark("lis===300###sois===6664###eois===6675###lif===8###soif===145###eoif===156###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");size_t len;

		AKA_mark("lis===301###sois===6677###eois===6687###lif===9###soif===158###eoif===168###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");char *pat;


	/* normalize */
		AKA_mark("lis===304###sois===6707###eois===6729###lif===12###soif===188###eoif===210###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");len = strlen(pattern);

		AKA_mark("lis===305###sois===6731###eois===6753###lif===13###soif===212###eoif===234###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");pat = alloca(1 + len);

		AKA_mark("lis===306###sois===6755###eois===6785###lif===14###soif===236###eoif===266###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");hash = normhash(pattern, pat);


	/* grows if needed */
		if (AKA_mark("lis===309###sois===6815###eois===6832###lif===17###soif===296###eoif===313###ifc===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)") && ((AKA_mark("lis===309###sois===6815###eois===6819###lif===17###soif===296###eoif===300###isc===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)")&&hash)	&&(AKA_mark("lis===309###sois===6823###eois===6832###lif===17###soif===304###eoif===313###isc===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)")&&grow(set)))) {
		AKA_mark("lis===310###sois===6836###eois===6846###lif===18###soif===317###eoif===327###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");return -1;
	}
	else {AKA_mark("lis===-309-###sois===-6815-###eois===-681517-###lif===-17-###soif===-###eoif===-313-###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");}


	/* search */
		AKA_mark("lis===313###sois===6863###eois===6897###lif===21###soif===344###eoif===378###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");ph = search(set, pat, hash, &pph);

		if (AKA_mark("lis===314###sois===6903###eois===6905###lif===22###soif===384###eoif===386###ifc===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)") && (AKA_mark("lis===314###sois===6903###eois===6905###lif===22###soif===384###eoif===386###isc===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)")&&ph)) {
		/* found */
				AKA_mark("lis===316###sois===6925###eois===6940###lif===24###soif===406###eoif===421###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");errno = EEXIST;

				AKA_mark("lis===317###sois===6943###eois===6953###lif===25###soif===424###eoif===434###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");return -1;

	}
	else {AKA_mark("lis===-314-###sois===-6903-###eois===-69032-###lif===-22-###soif===-###eoif===-386-###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");}


	/* not found, create it */
		AKA_mark("lis===321###sois===6987###eois===7021###lif===29###soif===468###eoif===502###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");ph = malloc(1 + len + sizeof *ph);

		if (AKA_mark("lis===322###sois===7027###eois===7030###lif===30###soif===508###eoif===511###ifc===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)") && (AKA_mark("lis===322###sois===7027###eois===7030###lif===30###soif===508###eoif===511###isc===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)")&&!ph)) {
		AKA_mark("lis===323###sois===7034###eois===7044###lif===31###soif===515###eoif===525###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");return -1;
	}
	else {AKA_mark("lis===-322-###sois===-7027-###eois===-70273-###lif===-30-###soif===-###eoif===-511-###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");}


	/* initialize it */
		AKA_mark("lis===326###sois===7068###eois===7084###lif===34###soif===549###eoif===565###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");ph->next = NULL;

		AKA_mark("lis===327###sois===7086###eois===7102###lif===35###soif===567###eoif===583###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");ph->hash = hash;

		AKA_mark("lis===328###sois===7104###eois===7136###lif===36###soif===585###eoif===617###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");ph->handler.callback = callback;

		AKA_mark("lis===329###sois===7138###eois===7168###lif===37###soif===619###eoif===649###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");ph->handler.closure = closure;

		AKA_mark("lis===330###sois===7170###eois===7203###lif===38###soif===651###eoif===684###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");strcpy(ph->handler.pattern, pat);

		AKA_mark("lis===331###sois===7205###eois===7215###lif===39###soif===686###eoif===696###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");*pph = ph;

		if (AKA_mark("lis===332###sois===7221###eois===7225###lif===40###soif===702###eoif===706###ifc===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)") && (AKA_mark("lis===332###sois===7221###eois===7225###lif===40###soif===702###eoif===706###isc===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)")&&hash)) {
		AKA_mark("lis===333###sois===7229###eois===7242###lif===41###soif===710###eoif===723###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");set->count++;
	}
	else {AKA_mark("lis===-332-###sois===-7221-###eois===-72214-###lif===-40-###soif===-###eoif===-706-###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");}

		AKA_mark("lis===334###sois===7244###eois===7253###lif===42###soif===725###eoif===734###ins===true###function===./app-framework-binder/src/globset.c/globset_add(struct globset*,const char*,void*,void*)");return 0;

}

/**
 * Delete the handler for the pattern
 * @param set the set
 * @param pattern the pattern to delete
 * @param closure where to put the closure if not NULL
 * @return 0 in case of success or -1 in case of error (ENOENT)
 */
/** Instrumented function globset_del(struct globset*,const char*,void**) */
int globset_del(
			struct globset *set,
			const char *pattern,
			void **closure)
{AKA_mark("Calling: ./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");AKA_fCall++;
		AKA_mark("lis===349###sois===7571###eois===7597###lif===5###soif===87###eoif===113###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");struct pathndl *ph, **pph;

		AKA_mark("lis===350###sois===7599###eois===7613###lif===6###soif===115###eoif===129###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");unsigned hash;

		AKA_mark("lis===351###sois===7615###eois===7625###lif===7###soif===131###eoif===141###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");char *pat;


	/* normalize */
		AKA_mark("lis===354###sois===7645###eois===7679###lif===10###soif===161###eoif===195###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");pat = alloca(1 + strlen(pattern));

		AKA_mark("lis===355###sois===7681###eois===7711###lif===11###soif===197###eoif===227###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");hash = normhash(pattern, pat);


	/* search */
		AKA_mark("lis===358###sois===7728###eois===7762###lif===14###soif===244###eoif===278###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");ph = search(set, pat, hash, &pph);

		if (AKA_mark("lis===359###sois===7768###eois===7771###lif===15###soif===284###eoif===287###ifc===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)") && (AKA_mark("lis===359###sois===7768###eois===7771###lif===15###soif===284###eoif===287###isc===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)")&&!ph)) {
		/* not found */
				AKA_mark("lis===361###sois===7795###eois===7810###lif===17###soif===311###eoif===326###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");errno = ENOENT;

				AKA_mark("lis===362###sois===7813###eois===7823###lif===18###soif===329###eoif===339###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");return -1;

	}
	else {AKA_mark("lis===-359-###sois===-7768-###eois===-77683-###lif===-15-###soif===-###eoif===-287-###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");}


	/* found, remove it */
		if (AKA_mark("lis===366###sois===7857###eois===7861###lif===22###soif===373###eoif===377###ifc===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)") && (AKA_mark("lis===366###sois===7857###eois===7861###lif===22###soif===373###eoif===377###isc===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)")&&hash)) {
		AKA_mark("lis===367###sois===7865###eois===7878###lif===23###soif===381###eoif===394###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");set->count--;
	}
	else {AKA_mark("lis===-366-###sois===-7857-###eois===-78574-###lif===-22-###soif===-###eoif===-377-###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");}

		AKA_mark("lis===368###sois===7880###eois===7896###lif===24###soif===396###eoif===412###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");*pph = ph->next;


	/* store the closure back */
		if (AKA_mark("lis===371###sois===7933###eois===7940###lif===27###soif===449###eoif===456###ifc===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)") && (AKA_mark("lis===371###sois===7933###eois===7940###lif===27###soif===449###eoif===456###isc===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)")&&closure)) {
		AKA_mark("lis===372###sois===7944###eois===7975###lif===28###soif===460###eoif===491###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");*closure = ph->handler.closure;
	}
	else {AKA_mark("lis===-371-###sois===-7933-###eois===-79337-###lif===-27-###soif===-###eoif===-456-###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");}


	/* free the memory */
		AKA_mark("lis===375###sois===8001###eois===8010###lif===31###soif===517###eoif===526###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");free(ph);

		AKA_mark("lis===376###sois===8012###eois===8021###lif===32###soif===528###eoif===537###ins===true###function===./app-framework-binder/src/globset.c/globset_del(struct globset*,const char*,void**)");return 0;

}

/**
 * Search the handler of 'pattern'
 * @param set the set
 * @param pattern the pattern to search
 * @return the handler found or NULL
 */
/** Instrumented function globset_search(struct globset*,const char*) */
struct globset_handler *globset_search(
			struct globset *set,
			const char *pattern)
{AKA_mark("Calling: ./app-framework-binder/src/globset.c/globset_search(struct globset*,const char*)");AKA_fCall++;
		AKA_mark("lis===389###sois===8258###eois===8284###lif===4###soif===91###eoif===117###ins===true###function===./app-framework-binder/src/globset.c/globset_search(struct globset*,const char*)");struct pathndl *ph, **pph;

		AKA_mark("lis===390###sois===8286###eois===8300###lif===5###soif===119###eoif===133###ins===true###function===./app-framework-binder/src/globset.c/globset_search(struct globset*,const char*)");unsigned hash;

		AKA_mark("lis===391###sois===8302###eois===8312###lif===6###soif===135###eoif===145###ins===true###function===./app-framework-binder/src/globset.c/globset_search(struct globset*,const char*)");char *pat;


	/* local normalization */
		AKA_mark("lis===394###sois===8342###eois===8376###lif===9###soif===175###eoif===209###ins===true###function===./app-framework-binder/src/globset.c/globset_search(struct globset*,const char*)");pat = alloca(1 + strlen(pattern));

		AKA_mark("lis===395###sois===8378###eois===8408###lif===10###soif===211###eoif===241###ins===true###function===./app-framework-binder/src/globset.c/globset_search(struct globset*,const char*)");hash = normhash(pattern, pat);


	/* search */
		AKA_mark("lis===398###sois===8425###eois===8459###lif===13###soif===258###eoif===292###ins===true###function===./app-framework-binder/src/globset.c/globset_search(struct globset*,const char*)");ph = search(set, pat, hash, &pph);

		AKA_mark("lis===399###sois===8461###eois===8493###lif===14###soif===294###eoif===326###ins===true###function===./app-framework-binder/src/globset.c/globset_search(struct globset*,const char*)");return ph ? &ph->handler : NULL;

}

/**
 * Search a handler for the string 'text'
 * @param set the set
 * @param text the text to match
 * @return the handler found or NULL
 */
/** Instrumented function globset_match(struct globset*,const char*) */
const struct globset_handler *globset_match(
			struct globset *set,
			const char *text)
{AKA_mark("Calling: ./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");AKA_fCall++;
		AKA_mark("lis===412###sois===8732###eois===8757###lif===4###soif===93###eoif===118###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");struct pathndl *ph, *iph;

		AKA_mark("lis===413###sois===8759###eois===8779###lif===5###soif===120###eoif===140###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");unsigned hash, g, s;

		AKA_mark("lis===414###sois===8781###eois===8791###lif===6###soif===142###eoif===152###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");char *txt;


	/* local normalization */
		AKA_mark("lis===417###sois===8821###eois===8852###lif===9###soif===182###eoif===213###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");txt = alloca(1 + strlen(text));

		AKA_mark("lis===418###sois===8854###eois===8881###lif===10###soif===215###eoif===242###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");hash = normhash(text, txt);


	/* first, look in dictionary of exact matches */
		AKA_mark("lis===421###sois===8934###eois===8944###lif===13###soif===295###eoif===305###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");ph = NULL;

		if (AKA_mark("lis===422###sois===8950###eois===8969###lif===14###soif===311###eoif===330###ifc===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)") && ((AKA_mark("lis===422###sois===8950###eois===8954###lif===14###soif===311###eoif===315###isc===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)")&&hash)	&&(AKA_mark("lis===422###sois===8958###eois===8969###lif===14###soif===319###eoif===330###isc===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)")&&set->exacts))) {
				AKA_mark("lis===423###sois===8975###eois===9011###lif===15###soif===336###eoif===372###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");ph = set->exacts[hash & set->gmask];

				while (AKA_mark("lis===424###sois===9020###eois===9080###lif===16###soif===381###eoif===441###ifc===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)") && ((AKA_mark("lis===424###sois===9020###eois===9022###lif===16###soif===381###eoif===383###isc===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)")&&ph)	&&((AKA_mark("lis===424###sois===9027###eois===9043###lif===16###soif===388###eoif===404###isc===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)")&&ph->hash != hash)	||(AKA_mark("lis===424###sois===9047###eois===9079###lif===16###soif===408###eoif===440###isc===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)")&&strcmp(txt, ph->handler.pattern))))) {
			AKA_mark("lis===425###sois===9085###eois===9099###lif===17###soif===446###eoif===460###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");ph = ph->next;
		}

	}
	else {AKA_mark("lis===-422-###sois===-8950-###eois===-895019-###lif===-14-###soif===-###eoif===-330-###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");}


	/* then if not found */
		if (AKA_mark("lis===429###sois===9134###eois===9144###lif===21###soif===495###eoif===505###ifc===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)") && (AKA_mark("lis===429###sois===9134###eois===9144###lif===21###soif===495###eoif===505###isc===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)")&&ph == NULL)) {
		/* look in glob patterns for the best match */
				AKA_mark("lis===431###sois===9199###eois===9205###lif===23###soif===560###eoif===566###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");s = 0;

				AKA_mark("lis===432###sois===9208###eois===9225###lif===24###soif===569###eoif===586###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");iph = set->globs;

				while (AKA_mark("lis===433###sois===9234###eois===9237###lif===25###soif===595###eoif===598###ifc===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)") && (AKA_mark("lis===433###sois===9234###eois===9237###lif===25###soif===595###eoif===598###isc===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)")&&iph)) {
						AKA_mark("lis===434###sois===9244###eois===9281###lif===26###soif===605###eoif===642###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");g = match(iph->handler.pattern, txt);

						if (AKA_mark("lis===435###sois===9289###eois===9294###lif===27###soif===650###eoif===655###ifc===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)") && (AKA_mark("lis===435###sois===9289###eois===9294###lif===27###soif===650###eoif===655###isc===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)")&&g > s)) {
								AKA_mark("lis===436###sois===9302###eois===9308###lif===28###soif===663###eoif===669###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");s = g;

								AKA_mark("lis===437###sois===9313###eois===9322###lif===29###soif===674###eoif===683###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");ph = iph;

			}
			else {AKA_mark("lis===-435-###sois===-9289-###eois===-92895-###lif===-27-###soif===-###eoif===-655-###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");}

						AKA_mark("lis===439###sois===9331###eois===9347###lif===31###soif===692###eoif===708###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");iph = iph->next;

		}

	}
	else {AKA_mark("lis===-429-###sois===-9134-###eois===-913410-###lif===-21-###soif===-###eoif===-505-###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");}

		AKA_mark("lis===442###sois===9356###eois===9388###lif===34###soif===717###eoif===749###ins===true###function===./app-framework-binder/src/globset.c/globset_match(struct globset*,const char*)");return ph ? &ph->handler : NULL;

}


#endif

